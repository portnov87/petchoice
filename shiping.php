<?php
$properties = $modx->resource->getOne('Template')->getProperties();



if(!empty($properties['tpl'])){
    $tpl = $properties['tpl'];
}
else{
    $tpl = 'shipping.tpl';
}

if (($_SERVER['HTTP_HOST']=='m.petchoice.pp.ua')||($_SERVER['HTTP_HOST']=='m.petchoice.ua') || ($_SERVER['HTTP_HOST']=='m.petchoice.test')) $tpl = 'm/shipping.tpl';
if ($modx->resource->cacheable != '1') {
    $modx->smarty->caching = false;
}

if(!empty($properties['phptemplates.non-cached'])){
    $modx->smarty->compile_check = false;
    $modx->smarty->force_compile = true;
}

/*
echo '<pre>';
print_r($_SERVER);
echo '</pre>';*/

return preg_replace("/[ \n\t\r]+$/sm", "\r", $modx->smarty->fetch("tpl/{$tpl}"));


