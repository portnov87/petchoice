const gulp = require('gulp');

const requireDir = require('require-dir');
const tasks = requireDir('./gulp_tasks');

exports.styles = tasks.styles;
exports.libs_style = tasks.libs_style;
exports.svg_css = tasks.svg_css;
exports.rastr = tasks.rastr;

exports.ttf = tasks.ttf;
exports.fonts = tasks.fonts;

//js
exports.build_js = tasks.build_js;
exports.dev_js = tasks.dev_js;

exports.libs_js = tasks.libs_js;


