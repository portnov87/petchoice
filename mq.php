{snippet name=msCartKey params="id=`{$attr.idproduct}`&option_id=`{$attr.id_1c}`&weight=``&size=`{$attr.weight} {$attr.weight_prefix}`&price=`{$attr.price}`" assign=key}
{snippet name=msCartKey params="id=`{$attr.idproduct}`&option_id=`{$attr.id_1c}`&weight=``&size=`{$attr.weight} {$attr.weight_prefix}`&price=`{$attr.price}`" assign=msCartKey}


<li>
    <div class="weight_wrap">
        <div class="buy-type">{$attr.weight} {$attr.weight_prefix}</div>

        <div class="sale-from-type">
            {if ($attr.markdown_text!='')}
                <span class="grey small">{$attr.markdown_text}</span>
            {else}
                {if (($modx->resource->parent==15)||($modx->resource->parent==19)||($modx->resource->parent==59)||($modx->resource->parent==57))}
                    {if ($attr.price_kg!='')}<span class="grey small" style="float:right;">{$attr.price_kg}  грн / кг</span>{/if}
                {/if}
            {/if}
        </div>
    </div>

<!--    <div class="buy-type">{$attr.weight} {$attr.weight_prefix}</div>
 style="margin-left:100px;"-->

    <div class="icon_wrap">
        {if ($attr.markdown)}
            <div class="col-xs-2 equal-height"><span style="" class="icon markdown-icon"></span></div>
        {/if}
        {if ($attr.by_weight)}
            <div class="col-xs-2 equal-height"><span style="" class="icon by_weight-icon"></span></div>
        {/if}
        {if ($attr.dostavka>0)}
            <div class="col-xs-3 equal-height"><span style="" class="icon free-icon"></span></div>

        {/if}

    <!--style="margin-left:90px;"-->
        <!-- style="margin-left:5px;margin-right:5px;padding:2px 1px; background-color: #a0489c;color:#fff;font-size:13px;"-->
        {if (($attr.action==1)&&($attr.action_number>0))}
            <div class="col-xs-2 equal-height action_number" >
                <span>-{$attr.action_number}%</span>
            </div>
        {/if}

    </div>

    {if ($attr.instock==0)}
        <div class="product-none">
            <strong>{$attr.price} грн</strong>
            <a href="#boxStatus" data-article="{$attr.articleproduct}"
               data-product="{$attr.nameproduct}, {$attr.weight} {$attr.weight_prefix}" data-id="{$attr.id}">[[%polylang_site_product_button_askqty]]</a>
        </div>
    {else}
        <div class="product-buy-wrapper">
            {if ($attr.action==1)}
                <div class="wrapper_price_product_one oldpr">
                    <div class="old-price uah">{$attr.oldprice}</div>
                    <div style="float:right;clear:none;margin-top:8px;" class="price uah">{$attr.price}</div>
                </div>
            {else}
                <div class="wrapper_price_product_one">
                    <div style="float:right;clear:none;" class="price uah">{$attr.price}</div>
                </div>
            {/if}

            <div class="btn-buy">
                {snippet name=inCartCount params="key=`{$msCartKey}`" assign=inCartCount}
                <input name="sub-product-count{$attr.idx}" class="form-control custom-inpt count-products" type="hidden" min="1"
                       value="{$inCartCount}"/>
                <button data-product-type="page"
                        data-product-vendor="{$attr.product_vendor}"
                        data-product-category="{$attr.category_ec}"
                        data-product-price="{$attr.price}"
                        data-option-id="{$attr.id_1c}"
                        data-product-name="{$attr.nameproduct}"
                        data-product-id="{$attr.idproduct}" data-size="{$attr.weight} {$attr.weight_prefix}"
                        data-key="{$msCartKey}" data-count="1" data-action="cart/add" href="#product-quantity-pu" type="button"
                        class="btn-product-buy ">[[%polylang_site_product_button_buy]]
                </button>
            </div>
        </div>
    {/if}
</li>
           
		   
		  