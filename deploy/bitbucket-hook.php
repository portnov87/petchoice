<?php
/**
 * @module bitbucket-hook
 * @version 2018.10.21, 02:50
 *
 * Bitbucket webhook interface.
 *
 * Based on 'Automated git deployment' script by Jonathan Nicoal:
 * http://jonathannicol.com/blog/2013/11/19/automated-git-deployments-from-bitbucket/
 *
 * See README.md and config.sample.php
 *
 * ---
 * Igor Lilliputten
 * mailto: igor at lilliputten dot ru
 * http://lilliputten.ru/
 *
 * Ivan Pushkin
 * mailto: iv dot pushk at gmail dot com
 */
echo 'start';
// Initalize:
require_once('log.php');
require_once('bitbucket.php');

// Load config:
include('config.php');
echo $REPO.' '.$REPO_NAME;
// Let's go:
initConfig(); // Initialize repo configs
echo 'finish init config';
initLog(); // Initialize log variables
echo 'initlog';

initPayload(); // Get posted data
echo 'init payload';
fetchParams(); // Get parameters from bitbucket payload (REPO)
echo 'finish fetch';
checkPaths(); // Check repository and project paths; create them if necessary
echo 'finish check';
placeVerboseInfo(); // Place verbose log information if specified in config
echo 'finish placeVerboseInfo';

fetchRepository(); // Fetch or clone repository
echo $REPO.' '.$REPO_NAME;
checkoutProject(); // Checkout project into target folder
echo 'no error';
