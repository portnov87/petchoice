<?php
$properties = $modx->resource->getOne('Template')->getProperties();


$alias = $modx->context->getOption('request_param_alias', 'q');


$request = $_REQUEST[$alias];
$tmp = explode('/', $request);
$tmp=array_filter($tmp);


if(!empty($properties['tpl'])){
    $tpl = $properties['tpl'];
}
else{
    $tpl = 'discounts.tpl';
}
if (($_SERVER['HTTP_HOST']=='m.petchoice.pp.ua')||($_SERVER['HTTP_HOST']=='m.petchoice.ua')||($_SERVER['HTTP_HOST']=='m.petchoice.test'))
{
    $tpl = 'm/discounts.tpl';
}





if ($modx->resource->cacheable != '1') {
    $modx->smarty->caching = false;
}

if(!empty($properties['phptemplates.non-cached'])){
    $modx->smarty->compile_check = false;
    $modx->smarty->force_compile = true;
}

return preg_replace("/[ \n\t\r]+$/sm", "\r", $modx->smarty->fetch("tpl/{$tpl}"));


