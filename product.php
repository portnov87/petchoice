<?php
$properties = $modx->resource->getOne('Template')->getProperties();

if (!empty($properties['tpl'])) {
    $tpl = $properties['tpl'];
} else {
    $tpl = 'product.tpl';
}
/*
if ($_SERVER['REMOTE_ADDR']=='188.130.177.18'){
echo '<pre>server';
print_r($_SERVER);
echo '</pre>';
}*/
$product = $modx->resource;

$hit = $modx->getObject('msProduct', array('id' => $product->id));

$hits = intval($hit->get('hits')) + 1;
$hit->set('hits', $hits);
$hit->save();

if (($_SERVER['HTTP_HOST'] == 'm.petchoice.ua')||($_SERVER['HTTP_HOST'] == 'm.petchoice.test')||($_SERVER['HTTP_HOST'] == 'm.petchoice.pp.ua')) $tpl = 'm/product.tpl';

if ($modx->resource->cacheable != '1') {
    $modx->smarty->caching = false;
}

if (!empty($properties['phptemplates.non-cached'])) {
    $modx->smarty->compile_check = false;
    $modx->smarty->force_compile = true;
}

$_SERVER['SCRIPT_URI'] = $_SERVER['REQUEST_URI'];


$modx->smarty->assign('type', 'product');


// получим данные по производителю

//if ($_SERVER['REMOTE_ADDR']=='188.130.177.18'){

$polylang = $modx->getService('polylang', 'Polylang');
$polyLangTools = $polylang->getTools();
$language = false;
$language = $polyLangTools->detectLanguage();
$vendor = $product->Data->vendor;

$nameproduct='';
if ($language->get('culture_key') == 'ua') {
    $sql = "SELECT 
IF(name_ua!='',name_ua,name) as vendorname,
url,
IF(country_ua!='',country_ua,country) as vendorcountry
 FROM `modx_ms2_vendors` 
WHERE `id` ='$vendor' LIMIT 1";
} else {
    $sql = "SELECT 
name as vendorname,
url,
country as vendorcountry FROM `modx_ms2_vendors` 
WHERE `id` ='$vendor' LIMIT 1";
}

$q = $modx->prepare($sql);
$q->execute();
$res = $q->fetchAll(PDO::FETCH_ASSOC);

foreach ($res as $v) {
    $modx->smarty->assign('vendorname', $v['vendorname']);
    if ($language->get('culture_key') == 'ua') {
        $vendorUrl='/brands/'.$v['url'];
    }else{
        $vendorUrl='/ru/brands/'.$v['url'];
    }
    $modx->smarty->assign('vendorurl', $vendorUrl);
    $modx->smarty->assign('vendorcountry', $v['vendorcountry']);
    $modx->smarty->assign('vendorname_ec', addslashes($v['name']));
}
$modx->smarty->assign('productname_ec', addslashes($product->pagetitle));

// получим название категории
$parent = $product->parent;
$parent_cat = '';

if ($language->get('culture_key')=='ua') {


    $sql = 'SELECT 
IF(polylang_content.pagetitle!="",polylang_content.pagetitle,modx_site_content.pagetitle) as pagetitleLang,
parent,
`modx_site_content`.id as catid,
uri 
FROM `modx_site_content` 
LEFT JOIN modx_polylang_content as polylang_content ON (modx_site_content.id=polylang_content.content_id and polylang_content.culture_key="ua") 
WHERE `modx_site_content`.`id` ="'.$parent.'" LIMIT 1';

}else{
    $sql = "SELECT pagetitle as pagetitleLang,parent,id,uri FROM `modx_site_content` WHERE `id` ='$parent' LIMIT 1";

}



$q = $modx->prepare($sql);
$q->execute();
$res = $q->fetchAll(PDO::FETCH_ASSOC);
$uri = '';



$idcategory = false;
foreach ($res as $v) {
    $namecategory = $v['pagetitleLang'];

    if ($language->get('culture_key') == 'ru') {
        $uri = $language->makeUrl($v['catid'], 'full');//$v['uri'];
    } else {
        $uri = $v['uri'];
    }
    $idcategory = $v['id'];
    $parent_cat = $v['parent'];
    $modx->smarty->assign('namecategory', $namecategory);


}
$namecategory_ec = $namecategory;
if ($parent_cat != '') {
    $cat_model_parent = $modx->getObject('msCategory', $parent_cat);
    if ($cat_model_parent) {
        $namecategory_ec = $namecategory . ' - ' . $cat_model_parent->get('pagetitle');
        $url_category =
        $category = $cat_model_parent->get('pagetitle') . ' - ' . $category;

    }

}
$modx->smarty->assign('namecategory_ec', $namecategory_ec);
//echo '$namecategory_ec'.$namecategory_ec. ' '.$parent_cat;


if ($language->get('culture_key') == 'ua') {

    $sql = "SELECT attribute.key,attribute.id,attribute.sef, group.id as groupid,
IF(attribute.name_ua!='',attribute.name_ua,attribute.name) as attributename,  
IF(group.name_ua!='',group.name_ua,group.name) as groupname,
group.sort as sortatr 
FROM `modx_ms2_attributes` as `attribute`
INNER JOIN `modx_ms2_attribute_groups` as `group` ON (`attribute`.`attribute_group`=`group`.`id`)
INNER JOIN `modx_ms2_attributes_product` as `prod_atr` ON (`attribute`.`id`=`prod_atr`.`attribute_id`)

WHERE `prod_atr`.`product_id` ='" . $product->id . "' ORDER BY sortatr ASC";

} else {
    $sql = "SELECT attribute.key,attribute.id,attribute.sef, group.id as groupid, 
attribute.name as attributename, 
group.name as groupname,
group.sort as sortatr 
FROM `modx_ms2_attributes` as `attribute`
INNER JOIN `modx_ms2_attribute_groups` as `group` ON (`attribute`.`attribute_group`=`group`.`id`)
INNER JOIN `modx_ms2_attributes_product` as `prod_atr` ON (`attribute`.`id`=`prod_atr`.`attribute_id`)

WHERE `prod_atr`.`product_id` ='" . $product->id . "' ORDER BY sortatr ASC";
}


//получим остальные атрибуты


$q = $modx->prepare($sql);
$q->execute();
$res = $q->fetchAll(PDO::FETCH_ASSOC);

$attributes = '';
foreach ($res as $re) {
    if (($uri != '') && ($re['sef'] == 1)) {

        //$url=
        $name_at = '<a href="' . $uri . '/' . $re['key'] . '">' . $re['attributename'] . '</a>';
    } else $name_at = $re['attributename'];
    $selectedatr[$re['groupname']][] = $name_at;
}
foreach ($selectedatr as $key => $atr) {
    $attributes .= '<p>
            <span class="grey">' . $key . ': </span>';

    $attributes .= implode(', ', $atr);

    $attributes .= '</p>';
}
$modx->smarty->assign('attributes', $attributes);

// выборка отзывов
//if ($_SERVER['REMOTE_ADDR']=='188.130.177.18'){

$sql = "SELECT `C`.`id`,`C`.`deleted`,`breed`.`name` as `pet_type_user`,`pets`.`name` as `pet_user`,`C`.`rating`, `C`.`name`, `profile`.`fullname`, `C`.`text`, `C`.`createdby`,`C`.`pet`,`C`.`pet_type`,`C`.`rating`,`C`.`createdon` 
 FROM `modx_tickets_comments` as `C`
 INNER JOIN `modx_tickets_threads` AS `T` ON (`C`.`thread`=`T`.`id`)
 LEFT JOIN `modx_users` as `user` ON (`C`.`createdby`=`user`.`id`)
 LEFT JOIN `modx_user_attributes` as `profile` ON (`C`.`createdby`=`profile`.`internalKey`)
 LEFT JOIN `modx_ms2_pets` as `pets` ON (`user`.`id`=`pets`.`user_id`)
 LEFT JOIN `modx_ms2_breeds` as `breed` ON (`pets`.`breed`=`breed`.`id`)
 WHERE `C`.`published`=1 AND `C`.`deleted`=0 AND `T`.`resource`='" . $product->id . "'
 GROUP BY  `C`.`id`
 ORDER BY `C`.`createdon` DESC";


$q = $modx->prepare($sql);
$q->execute(array(0));
$arr = $q->fetchAll(PDO::FETCH_ASSOC);
//$arr =array();
$countcomments = count($arr);

$limit3 = array();
$i = 0;
$comments = $commentslimit = array();

$all = array();
$num = 1;
foreach ($arr as $com) {
    $comment = array();
    $create = explode(' ', $com['createdon']);
    $comment['created'] = $com['createdon'];//$create[0] ;
    $comment['text'] = $com['text'];
    $comment['num'] = $num;


    if ($com['fullname'] == '') $comment['name'] = $com['fullname'];
    else $comment['name'] = $com['name'];
    $rat = '';
    $comment['rat'] = $rat;
    if ($com['rating'] != 0) {

        for ($d = 1; $d <= $com['rating']; $d++) {
            $rat .= '<span class="icon star-little-icon star-little-icon-check"></span>';
        }
        $noact = 5 - (int)$com['rating'];
        for ($d = 1; $d <= $noact; $d++) {
            $rat .= '<span class="icon star-little-icon"></span>';
        }
        $comment['rat'] = $rat;
    }
    if ($com['pet_type_user'] != '') $comment['pet_type'] = $com['pet_type_user'];
    elseif ($com['pet_type'] != '') $comment['pet_type'] = $com['pet_type'];
    else $comment['pet_type'] = '';

    if ($com['pet_user'] != '') $comment['pet'] = $com['pet_user'];
    elseif ($com['pet'] != '') $comment['pet'] = $com['pet'];
    else $comment['pet'] = '';


    $comment['id'] = $com['id'];

    $all[] = $com['rating'];

    $comments[$comment['id']] = $comment;
    $i++;
    if ($i <= 3) $commentslimit[] = $comment;

}
//echo count($commentslimit);


$middle = number_format(array_sum($all) / count($all), 1, '.', '');

$middle = round($middle, 0);

if (count($all) == 0) {
    $result_rating = "";
    $countcom = "";
    $noact = 5;
    for ($d = 1; $d <= $noact; $d++) {
        $rat .= '<span class="icon star-big-icon"></span>';
    }
    $result_rating = $rat;
} else {

    $result_rating = '';
    $middle1 = $middle;

    if ($middle != 0) {
        $rat = '';
        for ($d = 1; $d <= $middle; $d++) {
            $rat .= '<span class="icon star-big-icon star-big-icon-check"></span>';
        }
        $noact = 5 - $middle1;
        for ($d = 1; $d <= $noact; $d++) {
            $rat .= '<span class="icon star-big-icon"></span>';
        }
        $result_rating = $rat;
    }else{
        $noact = 5;
        for ($d = 1; $d <= $noact; $d++) {
            $rat .= '<span class="icon star-big-icon"></span>';
        }
        $result_rating = $rat;
    }


    $countcom = "<span class='tabcountsreview'>(<span itemprop='ratingCount'>" . count($all) . "</span>)</span>";
}


$reviewCount = count($all);
$ratingValue = $middle;
$modx->smarty->assign('rating', $result_rating);
$modx->smarty->assign('ratingValue', $ratingValue);
$modx->smarty->assign('ratingCount', $countcom);

if (isset($modx->user->id)) $modx->smarty->assign('fullname', $modx->user->name);

$modx->smarty->assign('commentslimit', $commentslimit);
$modx->smarty->assign('comments', $comments);
$modx->smarty->assign('countcomments', count($comments));


// получаем инфу по отзывам
/*
$sql = "SELECT `contentvalues`.`value`
 FROM `modx_site_tmplvars` as `tmpl`
 LEFT JOIN `modx_site_tmplvar_contentvalues` as `contentvalues` ON (`tmpl`.`id`=`contentvalues`.`tmplvarid`)
 WHERE `contentvalues`.`contentid`='" . $product->id . "'
 AND `tmpl`.`id`='61'
 LIMIT 1";

$q = $modx->prepare($sql);
$q->execute(array(0));
$arr = $q->fetchAll(PDO::FETCH_ASSOC);
$infoaction = '';
foreach ($arr as $act) $infoaction = $act['value'];
$modx->smarty->assign('infoaction', $infoaction);*/

return preg_replace("/[ \n\t\r]+$/sm", "\r", $modx->smarty->fetch("tpl/{$tpl}"));