// PRELOADER
//===============================================================
$(window).on('load', function () {
    "use strict";
    var $preloader = $('#page-preloader'),
        $spinner = $preloader.find('.spinner');
    $spinner.fadeOut();
    $preloader.delay(350).fadeOut('slow');
});


jQuery(document).ready(function () {
    "use strict";

    jQuery('.choose_filter').on('click',  function (event) {
        event.preventDefault();
        var filter_pos = jQuery('#filters-pu').offset().top - 100;
        jQuery('body,html').animate({scrollTop: filter_pos+'px'},800);
    });
//NUMERIC LIST
    //===============================================================
    var numeric_list = 0;
    $(".list-v3 li").each(function () {
        numeric_list++;
        $(this).append("<sub class='num'>" + numeric_list + "</sub>");
    });
    // SWIPER 1
    //===============================================================



    // ALIGN
    //===============================================================
    var height_va, width_ha;
    Align();
    $(window).on('resize load ready', Align);

    function Align() {
        // vertical align
        //===============================================================
        $(".align-v, .align-all").each(function () {
            height_va = '-' + $(this).outerHeight() / 2 + 'px';
            $(this).css({top: '50%', marginTop: height_va});
        });
        // horisontal align
        //===============================================================
        $(".align-h, .align-all").each(function () {
            width_ha = $(this).innerWidth() / 2;
            $(this).css({left: '50%', marginLeft: '-' + width_ha + 'px'});
        });

        var img_w, img_h, img_c_w, img_c_h, other_size;
        $('.imgfit').each(function () {
            other_size = $(this).css('borderWidth').replace(/\D+/g, "") * 2;
            img_w = $(this).children('img').outerWidth();
            img_h = $(this).children('img').outerHeight();
            img_c_w = $(this).outerWidth() - other_size;
            img_c_h = $(this).outerHeight() - other_size;
            $(this).children('img').css({
                position: 'absolute',
            });
            if (img_c_w <= img_c_h) {
                if (img_w <= img_h) {
                    if (img_w <= img_c_w) {
                        $(this).children('img').css({
                            width: img_c_w,
                            height: 'auto'
                        });
                    }
                    else {
                        $(this).children('img').css({
                            width: 'auto',
                            height: img_c_h
                        });
                    }

                }
                else {
                    $(this).children('img').css({
                        width: 'auto',
                        height: '100%'
                    });
                }
            }
            else {
                if (img_w > img_h) {
                    if (img_w <= img_c_w) {
                        $(this).children('img').css({
                            width: img_c_w,
                            height: 'auto'
                        });
                    }
                    else {
                        $(this).children('img').css({
                            width: 'auto',
                            height: img_c_h
                        });
                    }
                }
                else {
                    $(this).children('img').css({
                        width: '100%',
                        height: 'auto'
                    });
                }
            }
        });

        // del action
        //===============================================================
        //$('.h-btn-menu, .main-menu, .h-btn-search, .main-search, .h-btn-user, .main-login-registration').removeClass('active');

    }

    // FOOTER HEIGHT
    //===============================================================
    var footer_h;
    // $(window).on('resize load ready', footer_in);
    //
    // function footer_in() {
    //     footer_h = $('.main-footer').innerHeight();
    //     $('#main-container').css({
    //         paddingBottom: footer_h
    //     });
    // }

    // JACOR
    //===============================================================
    $(document).ready(function () {

    });

    // MENU LIST ACCORDEON
    //===============================================================
    /*$('.main-menu-list > a').click(function(){
        $(this).parent('li').toggleClass('active').siblings().removeClass('active');
    });*/

    // OTHER PHONES
    //===============================================================
    $('.btn-more-phones').click(function () {
        $(this).prevAll('div').toggleClass('active');
        footer_in();
    });

    // MAIN MENU & SEARCH & USER
    //===============================================================
    $('.h-btn-user').click(function () {
        $(this).toggleClass('active');
        $('.main-login-registration').toggleClass('active');
        $('.h-btn-search, .main-search, .h-btn-menu, .main-menu').removeClass('active');
        return false;
    });
    $('#registrcomment').click(function () {
        $('.h-btn-user').toggleClass('active');
        $('.main-login-registration').toggleClass('active');
        $('.h-btn-search, .main-search, .h-btn-menu, .main-menu').removeClass('active');
        return false;
    });

    // $('#logincomment').click(function () {
    //     $('.h-btn-user').toggleClass('active');
    //     $('.main-login-registration').toggleClass('active');
    //     $('.h-btn-search, .main-search, .h-btn-menu, .main-menu').removeClass('active');
    //     return false;
    // });


    $('.h-btn-menu').click(function () {
        $(this).toggleClass('active');
        $('.main-menu').toggleClass('active');
        $('.h-btn-search, .main-search, .h-btn-user, .main-login-registration').removeClass('active');
        return false;
    });

    $('.h-btn-search').click(function () {
        $(this).toggleClass('active');
        $('.main-search').toggleClass('active');
        setTimeout(function () {
            $('form.search-form input').focus();
        }, 200);

        $('.h-btn-menu, .main-menu, .h-btn-user, .main-login-registration').removeClass('active');
        return false;
    });

    //POPUP PRODUCT QUALITY
    //===============================================================


    $('body').on('click', '.clickquickorder', function () {
        $('.quickorder' +
            '').fadeIn(200);
        return false;
    });


    $('body').on('click', '.product-none a', function () {

        var product = $(this).data('product');
        var article = $(this).data('article');
        //alert(article);
        $('#boxStatus').find('#productask').val(product);
        $('#boxStatus').find('#articleask').val(article);


        $('.boxStatus' +
            '').fadeIn(200);
        return false;
    });


    // close btn
    //===============================================================


    $(".attention-top .btn-close").click(function () {
        $(this).parents(".attention-top").fadeOut(200);
        setCookie('modal', '1', {expires: 86400});
        $('.main-header').css('top', '0px');

    });


    $('#office-auth-form form').submit(function () {

    });
    $('body').on('click', 'a.forgotpassword', function () {


        $('div.forgotpassword' +
            '').fadeIn(200);
        return false;
    });
    $('body').on('click', 'button.showResetModal', function () {
        $('div.forgotpassword' +
            '').fadeIn(200);
        return false;
    });


    $('#forgotpassword .btn-close').click(function () {
        $(this).parents('.windows-popup-container').fadeOut(300);
    });

    $('.windows-popup-container .btn-close').click(function () {
        $(this).parents('.windows-popup-container').fadeOut(300);
    });
    // tabs btns
    //===============================================================
    var dataTab;
    $('.tabs-btn > *').click(function () {
        $(this).addClass('active').siblings().removeClass('active');
        dataTab = $(this).data('tabBtn');
        $(".tabs li[data-tab='" + dataTab + "']").fadeIn(0).siblings().fadeOut(0);

    });
    // order form - change password
    //===============================================================
    /*$('.ac-btn').click(function(){
        $(this).toggleClass('active').next('.ac-hide').slideToggle(200);
    });*/

    // input-quantity - change
    //===============================================================
    // PLUS\MINUS NUMBER PRODUCT
    //===============================================================
    var number, p_price, tp_price, tps_price, all_price = 0;


    $('.windows-popup-container .quantity span').click(function () {

        number = $(this).prevAll('input').val();
        if ($(this).hasClass('number-minus') && number > 1) {
            number--;
        }
        else if ($(this).hasClass('number-plus') && number <= 999) {
            number++;
        }
        $(this).prevAll('input').val(number);
        /*p_price = $(this).parents('.basket-table-product-tool2').children('li').children('.product-price').html().replace(/\D/g, '');
        tp_price = p_price*number;
        $(this).parents('.basket-table-product-tool2').children('li').children('.product-total-price').html(tp_price);
        QuantityChange();*/

    });

    $('.windows-popup-container .input-quantity').change(function () {
        number = $(this).val();
        /*p_price = $(this).parents('.basket-table-product-tool2').children('li').children('.product-price').html().replace(/\D/g, '');
        tp_price = p_price*number;
        $(this).parents('.basket-table-product-tool2').children('li').children('.product-total-price').html(tp_price);
        QuantityChange();*/
    });


    $('.basket-table-product-tool2 .quantity span').click(function () {

        number = $(this).prevAll('input').val();
        if ($(this).hasClass('number-minus') && number > 1) {
            number--;
        }
        else if ($(this).hasClass('number-plus') && number <= 999) {
            number++;
        }
        $(this).prevAll('input').val(number);
        p_price = $(this).parents('.basket-table-product-tool2').children('li').children('.product-price').html().replace(/\D/g, '');
        tp_price = p_price * number;
        $(this).parents('.basket-table-product-tool2').children('li').children('.product-total-price').html(tp_price);
        //alert($(this).attr('data-key'));

        QuantityChange($(this).attr('data-key'), number);
        if (number == 0) {
            var input_quantity_cart = $(this).parent().find('.input-quantity');

            var product_id = input_quantity_cart.data('product-id');

            var product_size = input_quantity_cart.data('size');
            var product_name = input_quantity_cart.data('product-name');
            var product_price = input_quantity_cart.data('product-price');
            var product_category = input_quantity_cart.data('product-category');
            var product_vendor = input_quantity_cart.data('product-vendor');
            dataLayer.push({
                'event': 'removeFromCart',
                'ecommerce': {
                    'remove': {                               // 'remove' actionFieldObject measures.
                        'products': [{                          //  removing a product to a shopping cart.
                            'name': product_name,
                            'id': product_id,
                            'price': product_price,
                            'brand': product_vendor,
                            'category': product_category,
                            'variant': product_size,
                            'quantity': 1
                        }]
                    }
                }
            });
        }

        /*



                // alert(product_name);
                dataLayer.push({
                    'event': 'addToCart',
                    'ecommerce': {
                        'currencyCode': 'UAH',
                        'add': {                                // 'add' actionFieldObject measures.
                            'products': [{                        //  adding a product to a shopping cart.
                                'name': product_name,
                                'id': product_id,
                                'price': product_price,
                                'brand': product_vendor,
                                'category': product_category,
                                'variant': product_size,
                                'quantity': number
                            }]
                        }
                    }
                });*/

    });

    $('.basket-table-product-tool2 .input-quantity').change(function () {
        number = $(this).val();
        p_price = $(this).parents('.basket-table-product-tool2').children('li').children('.product-price').html().replace(/\D/g, '');
        tp_price = p_price * number;
        $(this).parents('.basket-table-product-tool2').children('li').children('.product-total-price').html(tp_price);

        //key=$(this).data('key');
        //	alert($(this).data('key'));

        var input_quantity_cart = $(this);
        var product_id = input_quantity_cart.data('product-id');
        var product_size = input_quantity_cart.data('size');
        var product_name = input_quantity_cart.data('product-name');
        var product_price = input_quantity_cart.data('product-price');
        var product_category = input_quantity_cart.data('product-category');
        var product_vendor = input_quantity_cart.data('product-vendor');
        if (number == 0) {
            dataLayer.push({
                'event': 'removeFromCart',
                'ecommerce': {
                    'remove': {                               // 'remove' actionFieldObject measures.
                        'products': [{                          //  removing a product to a shopping cart.
                            'name': product_name,
                            'id': product_id,
                            'price': product_price,
                            'brand': product_vendor,
                            'category': product_category,
                            'variant': product_size,
                            'quantity': 1
                        }]
                    }
                }
            });
        }
        // alert(product_name);
        /*dataLayer.push({
            'event': 'addToCart',
            'ecommerce': {
                'currencyCode': 'UAH',
                'add': {                                // 'add' actionFieldObject measures.
                    'products': [{                        //  adding a product to a shopping cart.
                        'name': product_name,
                        'id': product_id,
                        'price': product_price,
                        'brand': product_vendor,
                        'category': product_category,
                        'variant': product_size,
                        'quantity': number
                    }]
                }
            }
        });*/

        QuantityChange($(this).attr('data-key'), number);
    });

    //$(window).on('load ready', QuantityChange);
    function QuantityChange(key, count) {
        cartChangeMobile(key, count);
        //alert(key);
        /*all_price=0;
        $(".product-total-price").each(function(){
            tps_price = $(this).html().replace(/\D/g, '');
            all_price = tps_price*1+all_price;
        });
        $(".orders-total-price div").html(all_price);*/
    }

    /*$('.add-cart').click(function () {
        //alert()
        var product_id = $('#idproductcart').val();
        var optionId=$('#optionId').val();
        var size = $('#productsize').val();
        //var count = $(this).next().children('.count-products').val();
        var count = $(this).parent().parent().find('.input-quantity').val();
        //next().children('.count-products').val();

        //alert(product_id);

        addToCartMobile(product_id, size, count, optionId);
    });*/
    $('body').on('change', '.prevcatalogselect', function () {
        //id=$(this).data('size');
        var optionSelected = $("option:selected", this);
        //alert($(optionSelected).attr('data-size'));

        $(this).parent().parent().parent().find('.optionweight').hide();
        $(this).parent().parent().parent().find('.optionweight').addClass('hide');
        $('#' + $(optionSelected).attr('data-size')).show();
        $('#' + $(optionSelected).attr('data-size')).removeClass('hide');
        var price = $(optionSelected).attr('data-price');

        $(this).parent().parent().parent().find('.icn-free').addClass('hide');
        if (price > 499) $(this).parent().parent().parent().find('.icn-free').removeClass('hide');
    });


    $(".btn_answer_vis a").on('click', function () {

        var id = $(this).data('parent');
        $('.comment_answer_wr').html('');
        var clonehtml = jQuery('#answer-form-wrapper').clone();
        clonehtml.find('form').show();
        clonehtml.find('input[name="parent"]').val(id);
        $('#comment_answer_' + id).html(clonehtml.html());
    });
    $(document).on('click', ".btn_cancel", function () {
        $(this).parents(".answer_footer_container").find(".answer_footer_form").slideToggle(200);
    })

}); // jQuery ready - end

