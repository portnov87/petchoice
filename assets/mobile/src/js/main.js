

jQuery(document).ready(function(){
//#queryword
    jQuery('.main-header .search-form input').on('focus',function(){
        jQuery('#autocomplete_popup').addClass('active');
        // jQuery(this).click();
        // jQuery(this).focus();
        jQuery('#autocomplete_popup form input').click();
        jQuery('#autocomplete_popup form input').focus();
        console.log('focus autocomplete');
    });

    jQuery('#autocomplete_popup .close_modal, .close_autocomplete_popup').on('click',function(){
        jQuery('#autocomplete_popup').removeClass('active');
    });


    //jQuery('#queryword')
    var swiper = new Swiper(".logos_galerey", {
        slidesPerView: 2,
        spaceBetween: 10,
        grabCursor: true,
        navigation: {
            nextEl: ".swiper-button-next",
            prevEl: ".swiper-button-prev",
        },
    });
    var swiper2 = new Swiper('.gallery-product', {
        //pagination: '.swiper-pagination',
        nextButton: '.swiper-button-next',
        prevButton: '.swiper-button-prev',
        //pagination: '.swiper-pagination',
        //effect: 'flip',
        grabCursor: true,
        paginationClickable: true,
        navigation: {
            nextEl: ".swiper-button-next",
            prevEl: ".swiper-button-prev",
        }
        /* autoplay: 6000,
         slidesPerView:1,
         spaceBetween:20,
         autoplayDisableOnInteraction: false,
         breakpoints: {
             880: {
                 slidesPerView: 1,
                 spaceBetween: 10
             },
             520: {
                 slidesPerView: 1,
                 spaceBetween: 10
             }
         }*/
    });


    var swiper1 = new Swiper('.gallery-1', {
        pagination: '.swiper-pagination',
        nextButton: '.swiper-button-next',
        prevButton: '.swiper-button-prev',
        paginationClickable: true,
        autoplay: 6000,
        slidesPerView: 4,
        spaceBetween: 20,
        autoplayDisableOnInteraction: false,
        breakpoints: {
            880: {
                slidesPerView: 4,
                spaceBetween: 10
            },
            520: {
                slidesPerView: 2,
                spaceBetween: 10
            }
        }
    });




    console.log('swiper navtabs');
    /*
    var swipersnav = new Swiper(".all_options ul.nav.nav-tabs", {
        //pagination: '.swiper-pagination',
        nextButton: '.next_nav',
        prevButton: '.prev_nav',
        //paginationClickable: true,
        //autoplay: 6000,
        slidesPerView: 1,
        spaceBetween: 0,
        //autoplayDisableOnInteraction: false,
    });*/

    // FILTERS

    jQuery('body').on('click', 'a.choose_filter', function () {

        var parent =jQuery(this).parent();
        //jQuery('ul.filters-accordion.accordion > li').removeClass('active');
        parent.toggleClass('active');//.siblings().removeClass('active');;//addClass('active');

        return false;
    });
    jQuery('.orders-history-accordion li > a').on('click',function(){

        if (jQuery(this).parent().hasClass('active'))
        {
            //jQuery('.orders-history-accordion li').removeClass('active');

            jQuery(this).parent().removeClass('active');
        }
        else {
            //jQuery('.orders-history-accordion li').removeClass('active');
            jQuery(this).parent().addClass('active');
        }

        return false;
    });

    jQuery('.product-info-accordion li > a').on('click',function(){

        if (jQuery(this).parent().hasClass('active'))
        {
            jQuery('.product-info-accordion li').removeClass('active');

        }
        else {
            jQuery('.product-info-accordion li').removeClass('active');
            jQuery(this).parent().addClass('active');
        }

        return false;
    });

//===============================================================
    jQuery('body').on('click', '.block_sort a', function () {
        jQuery('.sort-pu').fadeIn(200);
        return false;
    });

    //===============================================================
    jQuery('body').on('click', '.btn-filters a', function () {
        jQuery('.filters-pu').fadeIn(200);
        return false;
    });


    jQuery('body').on('click', '.btn-breadcrumb a', function () {
        jQuery('.breadcrumb-pu').fadeIn(200);
        return false;
    });

    // ACCORION
    //===============================================================
    jQuery('.main_items .accordion > li > a').click(function () {
        jQuery(this).hide();
        jQuery(this).parent().toggleClass('active').siblings().removeClass('active');


        jQuery('.main_items.active .accordion li').hide();

        jQuery(this).parent().show();

        jQuery('.current_open_menu_child').addClass('active');

        var textMenu=jQuery(this).text();
        jQuery('.current_open_menu_child').html('<a href="javascript:void(0)" class="backAllMenuParent">'+textMenu+'</a>');
        return false;
    });



    jQuery('.wrapper-burger').on('click','.backAllMenuParent',function(){

        jQuery('li.main_items.active ul.accordion li').removeClass('active');
        jQuery('li.main_items.active ul.accordion li').show();
        jQuery('li.main_items.active ul.accordion li > a').show();
        jQuery(this).parent().html('');
    });

    jQuery('.wrapper-burger').on('click','.backAllMenu',function(){

        jQuery('.main_menu_items li.main_items').removeClass('active');

        jQuery('li.main_items.active ul.accordion li').removeClass('active');
        jQuery('.main_menu_items li.main_items').show();

        jQuery('.current_open_menu_child').removeClass('active');
        jQuery(this).parent().html('');
    });

    jQuery('.main_items_parent').on('click',function(){

        var indexMenu=jQuery(this).data('menu');
        jQuery('.main_items').hide();

        jQuery('#mm-p'+indexMenu).show();
        jQuery('#mm-p'+indexMenu).addClass('active');
        jQuery('.current_open_menu').addClass('active');

        var textMenu=jQuery(this).text();
        jQuery('.current_open_menu').html('<a href="javascript:void(0)" class="backAllMenu">'+textMenu+'</a>');

    });

    // main menu
    jQuery('.main-header__menu__burger').on('click',function(){
        jQuery('#main_menu').addClass('active');
    });

    jQuery('#main_menu .close_modal').on('click',function(){
        jQuery('#main_menu').removeClass('active');
    });

    //menu_catalog
    jQuery('.open_menu_catalog').on('click',function(){
        jQuery('#menu_catalog').addClass('active');
    });

    jQuery('#menu_catalog .close_modal').on('click',function(){
        jQuery('#menu_catalog').removeClass('active');
    });

    jQuery('.forgotpassword').on('click',function(){

        jQuery('#forgotpas_profile').addClass('active');
        jQuery('#reg_profile').removeClass('active');
        jQuery('#login_profile').removeClass('active');

    });

    jQuery('#forgotpas_profile .close_modal').on('click',function(){
        jQuery('#forgotpas_profile').removeClass('active');


    });
    // jQuery('.registration-block .link_login').on('click',function(){
    //     jQuery('.login-registration-block.registration-block').removeClass('active');
    //     jQuery('.login-registration-block.login-block').addClass('active');
    // });

    jQuery('.link_login').on('click',function(){

        jQuery('#login_profile').addClass('active');
        jQuery('#main_menu').removeClass('active');
        jQuery('#reg_profile').removeClass('active');

        jQuery('.login-registration-block.registration-block').removeClass('active');
        jQuery('.login-registration-block.login-block').addClass('active');

    });

    jQuery('#login_profile .close_modal').on('click',function(){
        jQuery('#login_profile').removeClass('active');


    });

    jQuery('.link_registr').on('click',function(){
        jQuery('#reg_profile').addClass('active');
        jQuery('#main_menu').removeClass('active');

        jQuery('.login-registration-block.registration-block').addClass('active');
        jQuery('.login-registration-block.login-block').removeClass('active');


    });

    //
    // jQuery('.login-registration-block .link_registr').on('click',function(){
    //
    //
    //     jQuery('.login-registration-block.registration-block').addClass('active');
    //     jQuery('.login-registration-block.login-block').removeClass('active');
    //     //jQuery('.login-registration-block.registration-block').addClass('active');
    //
    //     //.login-block
    //
    // });

    jQuery('#reg_profile .close_modal').on('click',function(){
        jQuery('#reg_profile').removeClass('active');
    });

    //
    // jQuery('#reg_profile .close_modal').on('click',function(){
    //     jQuery('#reg_profile').removeClass('active');
    // });

    jQuery('#quickorder .btn-close').on('click',function(){
        //jQuery('.quickorder').hide();
        jQuery('.windows-popup-container').fadeOut(300);
    });





    // product options
    // jQuery('.all_options .prev_nav').on('click',function(){
    //     var parent=jQuery(this).parent();
    //     var oldActive=jQuery(parent).find('ul.nav-tabs li.active');
    //
    //     var prev=oldActive.prev();
    //     if (prev.length>0) {
    //         oldActive.removeClass('active');
    //         prev.addClass('active');
    //         console.log(prev);
    //     }
    //
    // });

    jQuery('.user-accordion > li > a').on('click',function(){
        var parent=jQuery(this).parent();
        if (parent.hasClass('active'))
        {
            parent.removeClass('active');
        }else{
            jQuery('.user-accordion > li').removeClass('active');
            parent.addClass('active');
        }


    });


    jQuery('.enter-promocode > li > a').on('click',function(){
        var parent=jQuery(this).parent();
        if (parent.hasClass('active'))
        {
            parent.removeClass('active');
        }else{
            jQuery('.enter-promocode > li').removeClass('active');
            parent.addClass('active');
        }


    });

    jQuery('.add-cart').click(function () {
        //alert()
        var product_id = jQuery('#idproductcart').val();
        var optionId=jQuery('#optionId').val();
        var size = jQuery('#productsize').val();
        var count = jQuery(this).parent().parent().find('.input-quantity').val();


        addToCartMobile(product_id, size, count, optionId);
    });

    jQuery('body').on('click', '.clickquickorder', function () {
        jQuery('.quickorder').fadeIn(200);
        return false;
    });





    jQuery('body').on('click', '.btn-product-buy', function () {
        jQuery('.product-quantity-pu').fadeIn(200);

        var el = jQuery(this).attr('href');

        jQuery('#keycart').val(jQuery(this).data('key'));
        //alert($(this).data('product-id'));

        jQuery('#idproductcart').val(jQuery(this).data('product-id'));
        jQuery('#optionId').val(jQuery(this).data('option-id'));




        jQuery('#productsize').val(jQuery(this).data('size'));

        jQuery('#productprice').val(jQuery(this).data('product-price'));

        jQuery('#action').val(jQuery(this).data('action'));


        jQuery('#product_category').val(jQuery(this).data('product-category'));
        jQuery('#product_vendor').val(jQuery(this).data('product-vendor'));
        jQuery('#product_name').val(jQuery(this).data('product-name'));
        jQuery('#product_price').val(jQuery(this).data('product-price'));

        jQuery('.product-quantity-pu').find('input[name="product_id"]').val(jQuery(this).data('product-id'));

        jQuery('.input-quantity').val(1);
        jQuery('body,html').animate({scrollTop: jQuery(el).offset().top - 50}, 400);

        return false;
    });

    jQuery('body').on('click','.all_options .next_nav', function(){

        var parent=jQuery(this).parent();
        var oldActive=jQuery(parent).find('ul.nav-tabs li.active');

        var index = oldActive.data('index');

        var next = oldActive.next();

        if (next.length<=0) {

            index=1;
            next=jQuery(parent).find('[data-index="'+index+'"]');
        }


        if (next.length>0) {



            oldActive.removeClass('active');
            next.addClass('active');

            var btn_product_buy = parent.parent().find('.btn-product-buy');
            var optionId =next.find('input[data-name="option_id"]').val();
            var productId =next.find('input[data-name="product_id"]').val();
            var size =next.find('input[data-name="size"]').val();
            var key = next.find('input[data-name="key"]').val();
            var price = next.find('input[data-name="price"]').val();
            var category = next.find('input[data-name="category"]').val();
            var product_name = next.find('input[data-name="product_name"]').val();
            var vendor = next.find('input[data-name="product_vendor"]').val();
            var list = next.find('input[data-name="list"]').val();

            jQuery(this).parent().parent().find('.category_option_block').hide();
            jQuery(this).parent().parent().find('#category_product_'+productId+'_'+optionId).show();
            console.log('next_nav #category_product_'+productId+'_'+optionId);



            btn_product_buy.attr('data-option-id', optionId);
            btn_product_buy.attr('data-product-id', productId);
            btn_product_buy.attr('data-size', size);
            btn_product_buy.attr('data-key', key);
            btn_product_buy.attr('data-product-type', list);
            btn_product_buy.attr('data-product-vendor', vendor);
            btn_product_buy.attr('data-product-category', category);
            btn_product_buy.attr('data-product-price', price);
            btn_product_buy.attr('data-product-name', product_name);

            jQuery('#keycart').val(key);
        }

    });


    jQuery('body').on('click', '[href^="#"]', function () {
        var el = jQuery(this).attr('href');
        console.log('el');
        console.log(el);
        console.log(jQuery(this));
        if (el == '#product-quantity-pu') {
            jQuery('#keycart').val(jQuery(this).data('key'));
//alert($(this).data('product-id'));
            console.log('product-quantity-pu _ product_id'+jQuery(this).data('product-id'));
            jQuery('#idproductcart').val(jQuery(this).data('product-id'));

            jQuery('#optionId').val(jQuery(this).data('option-id'));

            jQuery('#productsize').val(jQuery(this).data('size'));

            jQuery('#productprice').val(jQuery(this).data('product-price'));

            jQuery('#action').val(jQuery(this).data('action'));


            jQuery('#product_category').val(jQuery(this).data('product-category'));
            jQuery('#product_vendor').val(jQuery(this).data('product-vendor'));
            jQuery('#product_name').val(jQuery(this).data('product-name'));
            jQuery('#product_price').val(jQuery(this).data('product-price'));


            jQuery('.input-quantity').val(1);
            //alert(el);
            //$('body').animate({scrollTop: $(el).offset().top-50}, 400);
            jQuery('body,html').animate({scrollTop: jQuery(el).offset().top - 50}, 400);
            return false;
        }

        //alert(el);

    });


    jQuery('body').on('click','.all_options .prev_nav', function(){

        var parent=jQuery(this).parent();
        var oldActive=jQuery(parent).find('ul.nav-tabs li.active');

        var lenli=jQuery(parent).find('ul.nav-tabs li');
        var index = oldActive.attr('data-index');//data('index');
        var newindex= index - 1;

        if (newindex<=0)
            newindex=lenli.length;

        var next=jQuery(parent).find('[data-index="'+newindex+'"]');

        if (next.length>0) {

            oldActive.removeClass('active');
            next.addClass('active');

            var btn_product_buy = parent.parent().find('.btn-product-buy');
            var optionId =next.find('input[data-name="option_id"]').val();
            var productId =next.find('input[data-name="product_id"]').val();
            var size =next.find('input[data-name="size"]').val();
            var key = next.find('input[data-name="key"]').val();
            var price = next.find('input[data-name="price"]').val();
            var category = next.find('input[data-name="category"]').val();
            var product_name = next.find('input[data-name="product_name"]').val();
            var vendor = next.find('input[data-name="product_vendor"]').val();
            var list = next.find('input[data-name="list"]').val();



            jQuery(this).parent().parent().find('.category_option_block').hide();
            jQuery(this).parent().parent().find('#category_product_'+productId+'_'+optionId).show();
            console.log('prev_nav #category_product_'+productId+'_'+optionId);


            btn_product_buy.attr('data-option-id', optionId);
            btn_product_buy.attr('data-product-id', productId);
            btn_product_buy.attr('data-size', size);
            btn_product_buy.attr('data-key', key);
            btn_product_buy.attr('data-product-type', list);
            btn_product_buy.attr('data-product-vendor', vendor);
            btn_product_buy.attr('data-product-category', category);
            btn_product_buy.attr('data-product-price', price);
            btn_product_buy.attr('data-product-name', product_name);

            jQuery('#keycart').val(key);
        }

    });

});


function addToCartMobile(product_id, size, count, optionId) {
    if (!count) count = 1;

    var productData = {
        count: count,
        ctx: "web",
        id: product_id,
        option_id: optionId,
        options: {
            size: size
        },
        option_id: optionId,
        ms2_action: 'cart/add'
    };
    //9652b1e93d62b9ea86042b7682bacd52


    if ($('#product_category').val() != undefined) {
        var product_vendor = $('#product_vendor').val();
        var product_category = $('#product_category').val();
        var product_name = $('#product_name').val();
        var product_price = $('#product_price').val();
    } else {
        var cartadd = $('.product.add-cart');
        var product_vendor = cartadd.data('product-vendor');
        var product_category = cartadd.data('product-category');

        var product_price = $('#productprice').val();//cartadd.data('product-price');

        var product_name = cartadd.data('product-name');
    }
    console.log(' addToCartMobile productData');
    console.log(productData);
    if (count == 1) {
        dataLayer.push({

            'ecommerce': {
                'currencyCode': 'EUR',
                'add': {                                // 'add' actionFieldObject measures.
                    'products': [{                        //  adding a product to a shopping cart.
                        'name': product_name,
                        'id': product_id,
                        'price': product_price,
                        'brand': product_vendor,
                        'category': product_category,
                        'variant': size,
                        'quantity': count
                    }]
                }
            },
            'event': 'gtm-ee-event',
            'gtm-ee-event-category': 'Enhanced Ecommerce',
            'gtm-ee-event-action': 'Adding to Shopping Cart',
            'gtm-ee-event-non-interaction': 'False',
        });




    }

    $.ajax({
        type: "POST",
        url: action_url,
        data: productData,
        success: function (data) {
            data = $.parseJSON(data);


            console.log('data.data1');
            console.log(data.data);
            console.log('eSputnikProducts');
            console.log(data.data.eSputnikProducts);
            console.log('guest_key');
            console.log(data.data.guest_key);
            eS('sendEvent', 'StatusCart', {
                'StatusCart': data.data.eSputnikProducts,
                'GUID': data.data.guest_key
            });

            if (data.success === true) {
                if (data.data.total_count > 0) {
                    $('.main-header__menu__cart').addClass('active');
                    $('.products-on-basket').html(data.data.total_count);//#msMiniCart
                    //$('#msMiniCart').html('<a href="/order?edit_cart=1"><span class="products-on-basket">' + data.data.total_count + '</span></a>');
                } else {
                    $('.products-on-basket').html(data.data.total_count);//#msMiniCart
                    $('.main-header__menu__cart').removeClass('active');
                }
                $('.windows-popup-container').fadeOut(300);


                //eSputnikProducts

            //     $eSputnikProducts[]="{
            //     'productKey': '".$cart_items['option_id']."',
            //         'price': '".$cart_items['price']."',
            //         'quantity': '".$cart_items['count']."',
            //         'currency': 'UAH'
            // }";
            //     [
            //         ".implode(', ',$eSputnikProducts)."
            //     ],




            } else if (data.success === false) {

            }
        }
    });
}




$('.basket-table-product-tool2 .quantity span').click(function () {

    var number = $(this).prevAll('input').val();
    if ($(this).hasClass('number-minus') && number > 1) {
        number--;
    }
    else if ($(this).hasClass('number-plus') && number <= 999) {
        number++;
    }
    $(this).prevAll('input').val(number);
    var p_price = $(this).parents('.basket-table-product-tool2').children('li').children('.product-price').html().replace(/\D/g, '');
    var tp_price = p_price * number;
    $(this).parents('.basket-table-product-tool2').children('li').children('.product-total-price').html(tp_price);
    //alert($(this).attr('data-key'));

    QuantityChange($(this).attr('data-key'), number);
    if (number == 0) {
        var input_quantity_cart = $(this).parent().find('.input-quantity');

        var product_id = input_quantity_cart.data('product-id');

        var product_size = input_quantity_cart.data('size');
        var product_name = input_quantity_cart.data('product-name');
        var product_price = input_quantity_cart.data('product-price');
        var product_category = input_quantity_cart.data('product-category');
        var product_vendor = input_quantity_cart.data('product-vendor');
        dataLayer.push({
            'event': 'removeFromCart',
            'ecommerce': {
                'remove': {                               // 'remove' actionFieldObject measures.
                    'products': [{                          //  removing a product to a shopping cart.
                        'name': product_name,
                        'id': product_id,
                        'price': product_price,
                        'brand': product_vendor,
                        'category': product_category,
                        'variant': product_size,
                        'quantity': 1
                    }]
                }
            }
        });
    }

    /*



            // alert(product_name);
            dataLayer.push({
                'event': 'addToCart',
                'ecommerce': {
                    'currencyCode': 'UAH',
                    'add': {                                // 'add' actionFieldObject measures.
                        'products': [{                        //  adding a product to a shopping cart.
                            'name': product_name,
                            'id': product_id,
                            'price': product_price,
                            'brand': product_vendor,
                            'category': product_category,
                            'variant': product_size,
                            'quantity': number
                        }]
                    }
                }
            });*/

});

$('.basket-table-product-tool2 .input-quantity').change(function () {
    number = $(this).val();
    p_price = $(this).parents('.basket-table-product-tool2').children('li').children('.product-price').html().replace(/\D/g, '');
    tp_price = p_price * number;
    $(this).parents('.basket-table-product-tool2').children('li').children('.product-total-price').html(tp_price);

    //key=$(this).data('key');
    //	alert($(this).data('key'));

    var input_quantity_cart = $(this);
    var product_id = input_quantity_cart.data('product-id');
    var product_size = input_quantity_cart.data('size');
    var product_name = input_quantity_cart.data('product-name');
    var product_price = input_quantity_cart.data('product-price');
    var product_category = input_quantity_cart.data('product-category');
    var product_vendor = input_quantity_cart.data('product-vendor');
    if (number == 0) {
        dataLayer.push({
            'event': 'removeFromCart',
            'ecommerce': {
                'remove': {                               // 'remove' actionFieldObject measures.
                    'products': [{                          //  removing a product to a shopping cart.
                        'name': product_name,
                        'id': product_id,
                        'price': product_price,
                        'brand': product_vendor,
                        'category': product_category,
                        'variant': product_size,
                        'quantity': 1
                    }]
                }
            }
        });
    }
    // alert(product_name);
    /*dataLayer.push({
        'event': 'addToCart',
        'ecommerce': {
            'currencyCode': 'UAH',
            'add': {                                // 'add' actionFieldObject measures.
                'products': [{                        //  adding a product to a shopping cart.
                    'name': product_name,
                    'id': product_id,
                    'price': product_price,
                    'brand': product_vendor,
                    'category': product_category,
                    'variant': product_size,
                    'quantity': number
                }]
            }
        }
    });*/

    QuantityChange($(this).attr('data-key'), number);
});

//$(window).on('load ready', QuantityChange);
function QuantityChange(key, count) {
    cartChangeMobile(key, count);
    //alert(key);
    /*all_price=0;
    $(".product-total-price").each(function(){
        tps_price = $(this).html().replace(/\D/g, '');
        all_price = tps_price*1+all_price;
    });
    $(".orders-total-price div").html(all_price);*/
}


