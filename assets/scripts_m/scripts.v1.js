/*global $:false */



//режим отображения шапки
var min = false;//изначально шапка в обычном режиме
//var scroll=false;
$(document).scroll(function () {
    if (scroll == true) {

        if ($(document).scrollTop() > 30 && !min) {
            //изменение свойств шапки
            //$(".line2, .line2_b").css('height','35px');
            $(".basket-header").css('position', 'fixed');
            $(".basket-header").css('top', '0');
            $(".basket-header").css('width', '100%');
            $(".basket-header").css('border-bottom', '1px solid #c5d4e4');
            $(".basket-header").css('z-index', '99999');

            //$(".line2_b ul").css('margin-top','-87px');
            min = true;//шапка в мини режиме
        }
        if ($(document).scrollTop() <= 30 && min) {
            //изменение свойств шапки
            //$(".line2, .line2_b").css('height','80px');
            $(".basket-header").css('position', 'relative');
            //$(".logo").css('margin-top','15px');
            //увеличение логотипа
            $(".basket-header").css('border-bottom', 'none');
            //$(".logo").css('background-position','0px 0px');
            //$(".line2_b ul").css('margin-top','-75px');
            min = false;//шапка в обычном режиме
        }
    }
});


$('form').each(function () {
    var form=$(this);
    $(this).validate({
        errorElement: "div",
        highlight: function (element, errorClass, validClass) {
            $(element).parent('.form-group').addClass('has-error');
        },
        unhighlight: function (element, errorClass, validClass) {
            $(element).parent('.form-group').removeClass('has-error');
        },
        rules: {
            password: {
                required: true,
                minlength: 6
            },
            password_confirm: {
                required: true,
                equalTo: form.find("#password"),//"#password",
                minlength: 6
            },
            specifiedpassword: {
                minlength: 6
            },
            confirmpassword: {
                equalTo: "#inputNewPassword",
                minlength: 6
            }
        },
        messages: {
            password: {
                //required: "Please provide a password",
                minlength: "Пароль должен содержать не меньше 6 символов"
            },
            password_confirm: {
                //required: "Please provide a password",
                minlength: "Пароль должен содержать не меньше 6 символов",
                equalTo: "Пароли не совпадают"
            },
            specifiedpassword: {
                //required: "Please provide a password",
                minlength: "Пароль должен содержать не меньше 6 символов"
            },
            confirmpassword: {
                //required: "Please provide a password",
                minlength: "Пароль должен содержать не меньше 6 символов",
                equalTo: "Пароли не совпадают"
            }
        }
        //,errorClass: "help-block with-errors"
    });
});

$("input[name='username']").mask("+389999999999");
$("input[name='phone']").mask("+389999999999");
//$("input[name='phone_search']").mask("+380999999999");

$(function () {
    'use strict';
    if ($('#mse2_selected span').size() > 1)
        $('#mse2_selected').removeClass('hidden');
    else
        $('#mse2_selected').addClass('hidden');

    if ($('#petlist form').size() == 0)
        $('#petlist .info-msg').show();
    else
        $('#petlist .info-msg').hide();
});

vendor = document.getElementById('mse2_ms|vendor');
$(vendor).children('.checkbox:gt(10)').addClass('hidden');

$(function () {
    'use strict';
    $('[data-toggle="popover"]').popover({
        html: true,
        content: function () {
            var content = $(this).attr('data-popover-content');
            return $(content).children('.popover-body').html();
        },
        title: function () {
            var title = $(this).attr('data-popover-content');
            return $(title).children('.popover-heading').html();
        }
    });
});

$(document).on('click', 'a.upload', function (e) {
    $(this).next('input[type=file]').click();
    return false;
});
$(document).on('click', 'div.upload div.file-upload-btn', function (e) {
    $(this).next('input[type=file]').click();
    return false;
});


$(document).on('change', '.pet_type', function (e) {
    var type_id = this.value;
    var petData = {
        pet_type: type_id
    };
    var $this = $(this);
    $.ajax({
        type: "GET",
        url: '/assets/components/pets/getpetdata.php',
        data: petData,
        success: function (data) {
            data = $.parseJSON(data);
            if (data.success === true) {
                response = data.data;
                another = $(response).filter(function () {
                    return $(this).html() == "Другая";
                }).val();
                $pet_breed = $this.closest('form').find('.pet_breed');
                $pet_breed.html(data.data);
                $pet_breed.children('option:first').attr('selected', 'selected');
                var selected_text = $pet_breed.children('option:first').text();
                $pet_breed.next('.custom-select').find('span span').text(selected_text);
                $pet_breed.find('option[value="' + another + '"]').remove();
                $pet_breed.append($(response).filter(function () {
                    return $(this).html() == "Другая";
                }));
            } else if (data.success === false) {

            }
        }
    });
});


$('#shownextcomment').on('click', function (e) {
    var Data = {
        id: $(this).data('id'),
        thread: '463',
        //	thread	resource-303',//
        action: 'comment/getnext'//comment/get'
    };

    $.ajax({
        type: "POST",
        url: '/assets/components/tickets/action.php',//nextticket.php',
        data: Data,
        success: function (data) {
            data = $.parseJSON(data);
            if (data.success === true) {
                //results.append(data.data.results);
                dataLayer.push({
                    'event': 'event-GA',
                    'eventCategory': 'product',
                    'eventAction': 'review'
                });
                //ga('send', 'event', 'product', 'review');
                $('.callback-single .user-photo img').attr('src', data.data.avatar);
                //.src="../template/save.png";
                //$('.callback-single .user-photo').attr('src',data.data.avatar);
                $('.callback-popup .greener b').html(data.data.fullname);
                $('.callback-popup .small.grey').html(data.data.date_ago);
                $('.callback-popup p.textcom').html(data.data.text);
                $('#shownextcomment').data('id', data.data.id);
                //.attr('data-id',data.data.id);
                //alert(data.data.id);
            } else if (data.success === false) {

                alert(data.message);
            }

        }
    });

});


$('body').on('click', function (e) {
    'use strict';
    $('[data-toggle="popover"]').each(function () {
        if (!$(this).is(e.target) && $(this).has(e.target).length === 0 && $('.popover').has(e.target).length === 0) {
            $(this).popover('hide');
        }
    });
});


$('#showmore').on('click', function (e) {
    'use strict';

    var next_page = $('#mse2_pagination li.active').next().children('a');
    var tmp = $(next_page).prop('href').match(/page[=|\/](\d+)/);
    var page = tmp && tmp[1] ? Number(tmp[1]) : 1;
    var results = $('#mse2_results');
    var pagination = $('#mse2_pagination');
    var params = mSearch2.Hash.get();
    params['action'] = 'filter';
    params['pageId'] = mse2Config.pageId;
    params['key'] = mse2Config.key;
    params['page'] = page;


    vendor = document.getElementById('mse2_ms|vendor');

    //alert($(vendor).find('.checkbox input:checked').val());
    if ($(vendor).find('.checkbox input:checked').val() != 'undefined') {

        params['ms|vendor'] = $(vendor).find('.checkbox input:checked').val();
    }

    //=19
    $.ajax({
        type: "GET",
        url: '/assets/components/msearch2/action.php',
        data: params,
        success: function (data) {
            data = $.parseJSON(data);
            if (data.success === true) {
                results.append(data.data.results);
                pagination.html(data.data.pagination);
            } else if (data.success === false) {
                results.html(data.message);
            }

            if (parseInt(data.data.ostatok) < 0) {
                $('#showmore').hide();
            }
            else if (data.data.ostatok < 24) {
                var ost = parseInt(data.data.ostatok);//24 -
                $('#showmore .count').html(ost);
                //$('#showmore').hide();
            }
            else {
                $('#showmore .count').html(24);
                $('#showmore').show();
            }
            /*
             if (data.data.ostatok<=0){
             $('#showmore .count').html(data.data.ostatok);
             $('#showmore').hide();
             }else {
             $('#showmore .count').html(24);
             $('#showmore').show();
             }*/
            mSearch2.Hash.set(params);
        }
    });
});

$(function () {
    'use strict';
    $.prettyLoader();
});
/*
 // AJAX activity indicator bound to ajax start/stop document events
 $(document).ajaxStart(function () {
 $.prettyLoader.show();
 }).ajaxStop(function () {
 $.prettyLoader.hide();
 });
 */
$(function () {
    'use strict';
    $('.open-comment').click(function (e) {
        e.preventDefault();
        $(this).parents('.row').siblings('.comment-text-inpt').toggleClass('hide');
    });
});

$(function () {
    'use strict';
    $('.pay-btn').click(function () {
        //$(this).addClass('hidden').siblings('.count-box').removeClass('hidden');
    });
});

$(function () {
    'use strict';
    $(document).on("click", ".pay-product-btn", function (e) {
        var imgEl = $(this).parents('.product-item-tabs').siblings('.product-item-img').children().children('img');
        var imgClone = $(this).parents('.product-item-tabs').siblings('.product-item-img').children().children('img').clone();
        var paddLeft = document.documentElement.clientWidth;
        var paddEl = imgEl.offset().left + 260;
        paddEl = paddLeft - paddEl;
        imgClone.appendTo($(this).parents('.product-item-tabs').siblings('.product-item-img')).css({
            'position': 'absolute',
            'top': 0,
            'left': '17%',
            'z-index': 50,
            'width': '62%'
        });
        imgClone.animate({
            width: 0,
            height: 0,
            opacity: 0,
            left: '+=' + paddEl,
            top: '-=350'
        }, 1000);
        // $(this).addClass('hidden').siblings('.count-box').removeClass('hidden');
    });
});


/* bxslider for brands */
$(function () {
    'use strict';

    var slideActions = $('.slider-actions').bxSlider({
        minSlides: 1,
        maxSlides: 5,
        infiniteLoop: false,
        slideWidth: 176,
        slideMargin: 22,
        moveSlides: 1,
        nextText: '',
        prevText: ''
    });

    if (slideActions.length > 0) {
        var slideQt = slideActions.getSlideCount();

        if (slideQt < 6) {
            slideActions.parents().parents('.bx-wrapper').addClass('non-align');
        }
    }
    ;

    $('.brands-bx').bxSlider({
        minSlides: 1,
        maxSlides: 5,
        infiniteLoop: false,
        slideWidth: 176,
        slideMargin: 22,
        moveSlides: 1,
        nextText: '',
        prevText: ''
    });

    /*
     $('.foto-product').bxSlider({
     minSlides: 1,
     maxSlides: 5,
     infiniteLoop: false,
     slideWidth: 176,
     slideMargin: 22,
     moveSlides: 1,
     nextText: '',
     prevText: ''
     });*/


    /* product card slider */
    $('.product-complex-bx').bxSlider({
        minSlides: 1,
        maxSlides: 1,
        slideWidth: 1015,
        moveSlides: 1,
        nextText: '',
        prevText: ''
    });


    /* Prooduct card gallery */
    var product_slider = $('.product-gallery-bx').bxSlider({
        pager: false,
        //captions: false,
        pagerCustom: '#bx-pager',
        //nextSelector: '#bx-next-prod',
        //prevSelector: '#bx-prev-prod',
        nextText: 'Следующий',
        prevText: 'Предыдущий',
        // controls:false
    });
    /*
     var product_slider = $('.product-gallery-bx').bxSlider({
     pagerCustom: '#bx-pager'
     });
     */


    $('a[href="#new-products-pane"]').one('shown.bs.tab', function () {
        $('.slider-new-products').bxSlider({
            minSlides: 1,
            infiniteLoop: false,
            maxSlides: 5,
            slideWidth: 176,
            slideMargin: 22,
            moveSlides: 1,
            nextText: '',
            prevText: ''
        });
    });

});

/* Stars rating */
$(function () {
    'use strict';

    $('.stars-rating').barrating({
        showSelectedRating: false
    });
});


/* Slider range */
/*$(function() {
 'use strict';

 $('#slider-range').slider({
 range: true,
 min: 0,
 max: 10000,
 values: [325, 4620],
 slide: function(event, ui) {
 $('#amount1').val(ui.values[0]);
 $('#amount2').val(ui.values[1]);
 }
 });
 $('#amount1').val($('#slider-range').slider('values', 0));
 $('#amount2').val($('#slider-range').slider('values', 1));
 });*/

$(document).on("click", ".plus-count", function (e) {
    e.preventDefault();
    var qtty = $(this).prev('.count-products');
    var parent = $(this).closest('.ms2_product');
    var product_id = $(parent).find('input[name="product_id"]').val();
    var size = $(this).closest('.card-costs-item').find('.weight').text();
    var key = $(this).closest('.product-option').data('product-key');
    $(qtty).get(0).value++;
    //alert('d');
    $(qtty).parent('.count-box').find('.minus-countoff').addClass('minus-count');
    $(qtty).parent('.count-box').find('.minus-countoff').removeClass('minus-countoff');


    product_vendor = $(qtty).data('product-vendor');
    product_category = $(qtty).data('product-category');

    product_price = $(qtty).data('product-price');
    product_name = $(qtty).data('product-name');
    if ($(qtty).val() == 1) {
        dataLayer.push({
            'event': 'addToCart',
            'ecommerce': {
                'currencyCode': 'EUR',
                'add': {                                // 'add' actionFieldObject measures.
                    'products': [{                        //  adding a product to a shopping cart.
                        'name': product_name,
                        'id': product_id,
                        'price': product_price,
                        'brand': product_vendor,
                        'category': product_category,
                        'variant': size,
                        'quantity': 1
                    }]
                }
            },
            'event': 'gtm-ee-event',
            'gtm-ee-event-category': 'Enhanced Ecommerce',
            'gtm-ee-event-action': 'Adding to Shopping Cart',
            'gtm-ee-event-non-interaction': 'False',
        });
    }
    cartChange(key, $(qtty).val());

});
$(document).on("click", ".minus-countoff", function (e) {
    return false;
});
$(document).on("click", ".basket-item .minus-count", function (e) {
    e.preventDefault();

    var qtty = $(this).next('.count-products');
    var parent = $(this).closest('.ms2_product');
    var product_id = $(parent).find('input[name="product_id"]').val();
    var size = $(this).closest('.card-costs-item').find('.weight').text();
    var btn_cart = $(qtty).parent('.count-box').prev('.add-cart');
    var key = $(this).closest('.product-option').data('product-key');
    if ($(qtty).get(0).value - 1 > 0) {
        $(qtty).get(0).value--;
        cartChange(key, $(qtty).val());
    } else {
        $(qtty).parent('.count-box').find('.minus-count').addClass('minus-countoff');
        $(qtty).parent('.count-box').find('.minus-count').removeClass('minus-count');
        $(qtty).get(0).value = 1;
        $(btn_cart).removeClass('hidden');
        cartChange(key, $(qtty).val());
    }


    product_vendor = $(qtty).data('product-vendor');
    product_category = $(qtty).data('product-category');
    product_price = $(qtty).data('product-price');
    product_name = $(qtty).data('product-name');


    dataLayer.push({
        'ecommerce': {
            'remove': {                               // 'remove' actionFieldObject measures.
                'products': [{                          //  removing a product to a shopping cart.
                    'name': product_name,
                    'id': product_id,
                    'price': product_price,
                    'brand': product_vendor,
                    'category': product_category,
                    'variant': size,
                    'quantity': $(qtty).val()
                }]
            }
        },
        'event': 'gtm-ee-event',
        'gtm-ee-event-category': 'Enhanced Ecommerce',
        'gtm-ee-event-action': 'Removing from Shopping Cart',
        'gtm-ee-event-non-interaction': 'False',
    });


});

$(document).on("click", ".card-button .minus-count", function (e) {
    e.preventDefault();
    var qtty = $(this).next('.count-products');
    var parent = $(this).closest('.ms2_product');
    var product_id = $(parent).find('input[name="product_id"]').val();
    var size = $(this).closest('.card-costs-item').find('.weight').text();
    var btn_cart = $(qtty).parent('.count-box').prev('.add-cart');
    var key = $(this).closest('.product-option').data('product-key');
    if ($(qtty).get(0).value - 1 > 0) {
        $(qtty).get(0).value--;
        cartChange(key, $(qtty).val());
    } else {
        //alert('dd');
        $(qtty).parent('.card-button .count-box').addClass('hidden');////.card-button
        $(qtty).parent('.count-box').find('.minus-count').addClass('minus-countoff');
        $(qtty).parent('.count-box').find('.minus-count').removeClass('minus-count');
        $(qtty).get(0).value = 0;
        $(btn_cart).removeClass('hidden');
        cartChange(key, $(qtty).val());
        // $(qtty).get(0).value = 1;
    }


    product_vendor = $(qtty).data('product-vendor');
    product_category = $(qtty).data('product-category');
    product_price = $(qtty).data('product-price');
    product_name = $(qtty).data('product-name');
    product_id = $(qtty).data('product-id');

    dataLayer.push({
        'ecommerce': {
            'remove': {                               // 'remove' actionFieldObject measures.
                'products': [{                          //  removing a product to a shopping cart.
                    'name': product_name,
                    'id': product_id,
                    'price': product_price,
                    'brand': product_vendor,
                    'category': product_category,
                    'variant': size,
                    'quantity': $(qtty).val()
                }]
            }
        },
        'event': 'gtm-ee-event',
        'gtm-ee-event-category': 'Enhanced Ecommerce',
        'gtm-ee-event-action': 'Removing from Shopping Cart',
        'gtm-ee-event-non-interaction': 'False',
    });

});
$(document).on("click", ".product-item.small-item .minus-count", function (e) {
    e.preventDefault();
    var qtty = $(this).next('.count-products');
    var parent = $(this).closest('.ms2_product');
    var product_id = $(parent).find('input[name="product_id"]').val();
    var size = $(this).closest('.card-costs-item').find('.weight').text();
    var btn_cart = $(qtty).parent('.count-box').prev('.add-cart');
    var key = $(this).closest('.product-option').data('product-key');
    if ($(qtty).get(0).value - 1 > 0) {
        $(qtty).get(0).value--;
        cartChange(key, $(qtty).val());
    } else {
        //alert('dd');

        $(qtty).parent('.product-item.small-item .count-box').addClass('hidden');////.card-button
        $(qtty).parent('.count-box').find('.minus-count').addClass('minus-countoff');
        $(qtty).parent('.count-box').find('.minus-count').removeClass('minus-count');
        $(qtty).get(0).value = 0;
        $(btn_cart).removeClass('hidden');
        cartChange(key, $(qtty).val());
        // $(qtty).get(0).value = 1;
    }


    product_vendor = $(qtty).data('product-vendor');
    product_category = $(qtty).data('product-category');
    product_price = $(qtty).data('product-price');
    product_name = $(qtty).data('product-name');


    dataLayer.push({
        'ecommerce': {
            'remove': {                               // 'remove' actionFieldObject measures.
                'products': [{                          //  removing a product to a shopping cart.
                    'name': product_name,
                    'id': product_id,
                    'price': product_price,
                    'brand': product_vendor,
                    'category': product_category,
                    'variant': size,
                    'quantity': $(qtty).val()
                }]
            }
        },
        'event': 'gtm-ee-event',
        'gtm-ee-event-category': 'Enhanced Ecommerce',
        'gtm-ee-event-action': 'Removing from Shopping Cart',
        'gtm-ee-event-non-interaction': 'False',

    });
    //alert($(qtty).val());

});

$('#msOrder').keydown(function (event) {
    if (event.keyCode == 13) {
        event.preventDefault();
        return false;
    }
});

$("input[name='suggest_locality']").blur(function () {
    setTimeout(function () {
        if ($("input[name='suggest_locality']").val() == '') {
            $("input[name='city']").val('');
            $("input[name='extended[city_id]'").val('');

        }
    }, 2000);

});
$("input[name='email_search'], input[name='suggest_locality']").on('click', function () {

    $(this).select();
});

/*
 $( "input[name='suggest_locality']" ).focusin(function() {//focus(function() {
 setTimeout(function(){
 $( "input[name='suggest_locality']" ).select();
 },500);
 // $('.typeahead.dropdown-menu').show();
 //return true;
 });
 */
$(document).on("blur", ".count-products", function () {
    var qtty = $(this).val();
    var key = $(this).closest('.product-option').data('product-key');
    cartChange(key, qtty);
});
// сортировка в фильтре
$('.sort').on("click", ".mSort", function (e) {
    var data = $(this).data('sort');

    switch (data) {
        case 'ms_popular,desc':
            $(this).addClass('up-sort');
            $(this).data('sort', 'ms_popular,asc');
            break;
        case 'ms_popular,asc':
            $(this).removeClass('up-sort');
            $(this).data('sort', 'ms_popular,desc');
            break;
        case 'ms_price,desc':
            $(this).addClass('up-sort');
            $(this).data('sort', 'ms_price,asc');
            break;
        case 'ms_price,asc':
            $(this).removeClass('up-sort');
            $(this).data('sort', 'ms_price,desc');
            break;
        case 'name,desc':
            $(this).addClass('up-sort');
            $(this).data('sort', 'name,asc');
            break;
        case 'name,asc':
            $(this).removeClass('up-sort');
            $(this).data('sort', 'name,desc');
            break;
        default:
            console.log('Received unknown sort "' + data + '"');
    }
});


/* Fancybox */
$(function () {
    'use strict';

    $(document).ready(function () {
        $('.reserresultfilter').click(function (e) {


            var url = $(this).data('url');
            if (url != '') {
                //alert(url);
                location.href = url;
                return;
            }
            $('#mse2_filters').trigger('reset').submit();
            $(this).parents('.windows-popup-container').fadeOut(300);
            //return
        });

        /*$('#filters-pu .btn-close').click(function(e) {
         // url=$(this).data('url');
         //if (url!='')location.href=url;
         e.preventDefault();




         });*/

        $('body').on('click', '#showmore_mobile', function (e) {
            'use strict';
            //$('#showmore_mobile').click(function() {//.on('click', function(e) {
            //alert('dfdfdf');


            // var next_page = $('#mse2_pagination li.active').next().children('a');
            // var tmp = $(next_page).prop('href').match(/page[=|\/](\d+)/);
            var page = $('#paginatorpage').val();//tmp && tmp[1] ? Number(tmp[1]) : 1;
            var results = $('#mse2_results');//.big-prevs-list');

            mSearch2_m.results = results;
            // var pagination = $('#mse2_pagination');
            var params = mSearch2_m.Hash.get();
            //$('.big-prevs-container').addClass('loadingpag');
            $('.catalogfilter').show();

            params['action'] = 'filter';
            params['pageId'] = mse2Config.pageId;
            params['key'] = mse2Config.key;
            params['page'] = page;

            vendor = document.getElementById('mse2_ms|vendor');
            //alert($(vendor).find('input:checked').val());
            if ($(vendor).find('input:checked').val() != 'undefined') {
                params['ms|vendor'] = $(vendor).find('input:checked').val();
                //params['vendor'] = $(vendor).find('.checkbox input:checked').val();
            }
            mSearch2_m.beforeLoad();


            $.ajax({
                type: "GET",
                url: '/assets/components/msearch2/action.php',
                data: params,
                success: function (data) {
                    data = $.parseJSON(data);
                    if (data.success === true) {
                        var pa = parseInt($('#paginatorpage').val());
                        results.append('<div class="result'+pa+'">'+data.data.results+'</div>');
                        //results.scrollTop('300px');
                        //$('body,html').animate({scrollTop: results.offset().top-50}, 400);
                        $('html, body').animate({
                            scrollTop: $('.result'+pa).offset().top-350
                        }, 0);



                        // currentpage=$('#paginatorpage').val();

                        //parseInt()+1;
                        pa = pa + 1;

                        $('#paginatorpage').val(pa);
                        // paginatorpage
                        // pagination.html(data.data.pagination);
                    } else if (data.success === false) {
                        results.html(data.message);
                    }

                    if (parseInt(data.data.ostatok) < 0) {
                        $('#showmore_mobile').hide();

                        $('.number-products-vis').html(data.data.total);//data.data.limit2);
                    }
                    else if (parseInt(data.data.ostatok) == 0) {
                        $('#showmore_mobile').hide();

                        $('.number-products-vis').html(data.data.total);//data.data.limit2);
                    }
                    else if (data.data.ostatok < 10) {
                        var ost = parseInt(data.data.ostatok);//24 -
                        //$('#showmore_mobile .count').html(1111);//data.data.total);
                        $('#showmore_mobile .count').html(ost);
                        //$('#showmore').hide();number-products-vis

                        var showproduct = data.data.total - data.data.ostatok;
                        $('.number-products-vis').html(showproduct);//data.data.limit2);


                    }
                    else {
                        $('#showmore_mobile .count').html(10);
                        $('#showmore_mobile').show();
                        var showproduct = data.data.total - data.data.ostatok;
                        $('.number-products-vis').html(showproduct);//data.data.limit2);
                    }
                    $('#mse2_results').removeClass('loading');
                    $('#mse2_results').css('opacity', '1');


                    mSearch2_m.Hash.set(params);
                    mSearch2_m.afterLoad();
                    $('.catalogfilter').hide();//removeClass('loadingpag');
                    // alert(data.data.total+' '+data.data.ostatok);

                }
            });
        });


        $('.fancybox').fancybox({
            "frameWidth": 700,
            "imageScale": true,
//"opacity":true,
//"modal":true,
            "frameHeight": 600,
            //		"overlayShow" : true,
            //"overlayOpacity" : 0.8,
        });
        /*
         $('input[name=suggest_locality]').typeahead({
         source: function (query, process) {
         return $.post('/assets/components/suggest/getLocalitiesForSuggestJSON.php', {'name':query},
         function (response) {

         var data = new Array();


         var records = response.content.records;
         $.each(response.content.records, function(i) {
         $.each(response.content.records[i], function(key,val) {
         data.push(key+'_'+val);
         });

         return process(data);
         });

         },
         'json'
         );
         },
         matcher: function (item) {
         if (item.toLowerCase().indexOf(this.query.trim().toLowerCase()) != -1) {
         return true;
         }
         },
         sorter: function (items) {
         return items.sort();
         },
         highlighter: function (item) {
         var parts = item.split('_');
         parts.shift();
         return parts.join('_');
         },
         updater: function (item) {
         var parts = item.split('_');

         var cityId = parts.shift();
         $.post('/assets/components/delivery/getdeliverydata.php', {'city_id':cityId},
         function (deliveries) {
         $('#deliveries').html(deliveries);
         var getFirstDelivery = $("#deliveries option:first").val();
         //$('#deliveries').prop('selectedIndex',getFirstDelivery);
         $('#deliveries option:first').prop('selected', true);
         $('#deliveries').trigger('render');
         getWarehouses(cityId,getFirstDelivery);
         var getFirstWarehouse = $("#deliveries option:first").val();
         //$('#warehouses').prop('selectedIndex',getFirstWarehouse);
         $('#warehouses option:first').attr('selected', 'selected');
         //alert(getFirstWarehouse);

         },
         'json'
         );

         $("input[name=city]").val(parts);
         $("input[name='extended[city_id]']").val(cityId);
         return parts.join('_');
         },
         autoSelect: true,
         minLength: 3,
         delay: 500,
         items: 10,
         });*/
    });
});


$(document).ready(function () {

    $('.showresultfilter').click(function () {

        $('.windows-popup-container').fadeOut(300);
    });


    $('#office-profile-form .js-form-expand').hide();
    $('body').on('click', '.js-form-trigger', function () {
        $(this).toggleClass('is-active');
        $(this).parents('.js-form').find('.js-form-expand').slideToggle('fast');
    });

    //$('select').not('.stars-rating,#sort-by,#mse2_sort, .prevcatalogselect').customSelect();
    //alert($('#discountsumma').html());
    if ($('#discountsumma').html() == '') $('.js-form-expand').hide();
    /*$('#msOrder').on('click','.js-form-trigger',function() {
     $(this).toggleClass('is-active');
     $(this).parents('.js-form').find('.js-form-expand').slideToggle('fast');
     });*/

    $('.brand-expandall').click(function () {
        vendor1 = $('.allbrands');//$('#mse2_ms|vendor_all');//$(this).parent('.js-form');
        vendor2 = $('.commonbrands');//$('#mse2_ms|vendor');
        vendor2.css('display', 'block');//show();
        vendor1.css('display', 'none');//hide();

        // vendor2.children('.checkbox:gt(10)').toggleClass('hidden',300);
        /* $(this).toggleClass('is-active');
         if ($(this).text()=='Показать все бренды') {
         $(this).text('Показать основные бренды');
         } else {
         $(this).text('Показать все бренды');
         }*/
    });
    /*$(vendor).children('.checkbox:gt(9)').addClass('hidden');
     checkedvendor=$('#mse2_filters .checkbox.hidden').find('input').prop("checked");
     if (checkedvendor==true){
     $("input:checkbox:checked")
     checkedvendor.parent().removeClass('hidden');
     //$('#mse2_filters .checkbox.hidden').find('input:checked').removeClass('hidden');
     }*/
    $(".checkbox.hidden input:checkbox:checked").parent().removeClass('hidden');
    $('.brand-expand').click(function () {
        vendor = $(this).parent('.js-form');
        vendor.children('.checkbox:gt(10)').toggleClass('hidden', 300);
        $(this).toggleClass('is-active');
        // setTimeout(function(){
        if ($(this).text() == 'Показать все бренды') {
            $(this).text('Показать основные бренды');
        } else {
            $(this).text('Показать все бренды');
        }
        // },100);
        /*
         vendor1 = $('.commonbrands');//$('#mse2_ms|vendor');//$(this).parent('.js-form');
         //vendor1.html('dddd');
         vendor2 = $('.allbrands');//#mse2_ms|vendor_all');
         vendor2.css('display','block');//show();
         vendor1.css('display','none');//.hide();
         // vendor1.children('.checkbox:gt(10)').toggleClass('hidden',300);
         */
    });

    $('.js-form-link').click(function () {
        var input = $(this).parents('.js-form-wrapper').find('.js-form-input');
        if (input.is(':disabled')) {
            input.prop("disabled", false);
        } else {
            input.prop("disabled", true);
        }
    });

    $('.checkbox').click(function () {
        var filter = $(this).children('input');
        var filter_name = $(filter).val();
        var filter_id = $(filter).attr('id');
        $('<div class="choose-el" data-filter="' + filter_id + '"><span class="icon del-icon"></span>' + filter_name + '</div>').appendTo('.filter-selected-items');
        if ($(this).hasClass('disabled')) {
            $(this).find('input[type="checkbox"]').prop('disabled', true);
        }
    });

    $(document).on('change', $('#mse2_selected').size(), function (e) {
        if ($('#mse2_selected span').size() > 1) {
            $('#mse2_selected').removeClass('hidden');
        } else {
            $('#mse2_selected').addClass('hidden');
        }
    });

    /*$(document).on("click", '.del-icon', function(e) {
     var filter_id = $(this).parent().data('filter');
     $(this).parent().remove();
     console.log(filter_id);
     });*/
    /*$('.delete-filters-m').click(function(e) {
     url=$(this).data('url');
     if (url!='')location.href=url;
     e.preventDefault();
     $('#mse2_filters').trigger('reset').submit();
     $('.filter-selected-items').empty();
     $('#mse2_selected span').empty();
     $('#mse2_selected').addClass('hidden');
     });*/
    $('.delete-filters').click(function (e) {
        url = $(this).data('url');
        if (url != '') location.href = url;
        e.preventDefault();
        $('#mse2_filters').trigger('reset').submit();
        $('.filter-selected-items').empty();
        $('#mse2_selected span').empty();
        $('#mse2_selected').addClass('hidden');
    });

    $(document).on("click", "a,button", function (e) {
        var action = $(this).data('action');
        //alert('fdfdfdf'+action);
        if (action) {
            switch (action) {
                case 'cart/remove':
                    var cart_row = $(this).parent('.product-option');
                    var key = cart_row.data('product-key');
                    cart_row.fadeOut(300, function () {
                        $(this).remove();
                    });
                    removeFromCart(key);


                    product_name = $(this).data('product-name');
                    product_id = $(this).data('product-id');
                    product_price = $(this).data('product-price');
                    product_vendor = $(this).data('product-vendor');
                    product_category = $(this).data('product-category');
                    size = $(this).data('product-size');
                    countcart = $(this).data('product-count');


                    dataLayer.push({
                        'ecommerce': {
                            'remove': {                               // 'remove' actionFieldObject measures.
                                'products': [{                          //  removing a product to a shopping cart.
                                    'name': product_name,
                                    'id': product_id,
                                    'price': product_price,
                                    'brand': product_vendor,
                                    'category': product_category,
                                    'variant': size,
                                    'quantity': countcart
                                }]
                            }
                        },
                        'event': 'gtm-ee-event',
                        'gtm-ee-event-category': 'Enhanced Ecommerce',
                        'gtm-ee-event-action': 'Removing from Shopping Cart',
                        'gtm-ee-event-non-interaction': 'False',

                    });

                    break;
                case 'cart/add':
                    var product_id = $(this).data('product-id');
                    var size = $(this).data('size');
                    var count = $(this).next().children('.count-products').val();

                    var optionId = $(this).data('option-id');
                    addToCart(product_id, size, 1,optionId);
                    if (count < 1) {
                        $(this).next().children('.count-products').val(1);
                    }
                    var product_type = $(this).data('product-type');
                    if (product_type == 'page') {
                        dataLayer.push({
                            'event': 'event-GA',
                            'eventCategory': 'product',
                            'eventAction': 'buy-product-page'
                        });
                    }


                    var product_name = $(this).data('product-name');
                    var product_price = $(this).data('product-price');
                    var product_category = $(this).data('product-category');
                    var product_vendor = $(this).data('product-vendor');


                    // alert(product_name);
                    if (count == 1) {
                        dataLayer.push({
                            //'event': 'addToCart',
                            'ecommerce': {
                                'currencyCode': 'UAH',
                                'add': {                                // 'add' actionFieldObject measures.
                                    'products': [{                        //  adding a product to a shopping cart.
                                        'name': product_name,
                                        'id': product_id,
                                        'price': product_price,
                                        'brand': product_vendor,
                                        'category': product_category,
                                        'variant': size,
                                        'quantity': count
                                    }]
                                }
                            },
                            'event': 'gtm-ee-event',
                            'gtm-ee-event-category': 'Enhanced Ecommerce',
                            'gtm-ee-event-action': 'Adding to Shopping Cart',
                            'gtm-ee-event-non-interaction': 'False',


                        });
                    }
                    break;
                case 'cart/update':
                    break;
                case 'cart/get':
                    break;
                case 'pet/update':
                    e.preventDefault();
                    var form = $(this).closest('form');
                    var postData = new FormData(form[0]);
                    //petUpdate(postData);
                    $.ajax({
                        type: "POST",
                        url: '/assets/components/pets/updatepetdata.php',
                        enctype: 'multipart/form-data',
                        data: postData,
                        processData: false,  // tell jQuery not to process the data
                        contentType: false   // tell jQuery not to set contentType
                    }).done(function (data) {
                        data = $.parseJSON(data);
                        var photo = form.find('.img-circle');
                        if (data.newphoto)
                            photo.attr('src', data.newphoto);
                    });
                    break;
                case 'pet/remove':
                    e.preventDefault();
                    var pet_id = $(this).closest('form').find('input[name="pet_id"]').val();
                    $(this).closest('form').remove();
                    petRemove(pet_id);
                    if ($('#petlist form').size() == 0)
                        $('#petlist .info-msg').show();
                    else
                        $('#petlist .info-msg').hide();
                    break;
                default:
                    console.log('Received unknown action "' + action + '"');
            }
        }
    });
    $('#warehouses').on('change', function (e) {
        var warehouse_obj = $(this).find(':selected');
        var val = warehouse_obj.val();
        var textval = warehouse_obj.text();
        //alert(textval);
        $("#textwarehouse").val(textval);
    });
    $('#deliveries').on('change', function (e) {
        var delivery_obj = $(this).find(':selected');
        var delivery_payments = $(this).find(':selected').data('payments');
        var delivery_id = delivery_obj.val();
        var city_id = $('input[name="extended[city_id]"]').val();
        $('#warehouse-block .with-errors').hide();
        $('#warehouse-block .custom-select-container').show();
        $('#warehouses').prop('disabled', false);
        //alert($('#payments').prop('selectedIndex').val());
        //if ($('#payments').prop('selectedIndex').val()!=1){
        $('#payments').prop('disabled', false);
        //}

        if ($('#delivery_id').val() == 2) $('#infodostavkaorder').html('');

        if (delivery_id == 5){// курьер НП
            $('.fathername').removeClass('hide');
            $("#msOrder").validate(); //sets up the validator

            //console.log(delivery_id+' fathername required ');
            $('#fathername').rules('add', { required: true });
            $('#fathername').prop('required', true);//addAttribute();
            $('.form-group.fathername').addClass('has-error');

            $('body,html').animate({scrollTop: $('.fathername').offset().top - 150}, 400);
        }else{
            $('.fathername').addClass('hide');
            $('#fathername').prop('required', false);
            $('.form-group.fathername').removeClass('has-error');
            //$('#fathername').rules('remove', 'required');
        }

        getPayments(delivery_id);
        $('#payments option').each(function () {
            var payment = $(this).val();
            if ($.inArray(payment, delivery_payments) == -1) {
                $(this).prop('disabled', 'disabled').addClass('hidden');
                $('.payment-descr').addClass('hidden');
            } else {
                $(this).prop('disabled', false).removeClass('hidden');

            }
        });


        if (($(this).val() == 5) || ($(this).val() == 4)) {
            $('#warehouse-block').addClass('hide');
            $('#courier-block').removeClass('hide');
            $('#samovivoz-block').addClass('hide');

            if ($(this).val() == 5) $('.showdopkurier').hide();
        }
        else if ($(this).val() == 1) {
            $('#warehouse-block').addClass('hide');
            $('#samovivoz-block').removeClass('hide');
            $('#courier-block').addClass('hide');
        }
        else {
            $('#courier-block').addClass('hide');
            $('#samovivoz-block').addClass('hide');
            $('#warehouse-block').removeClass('hide');
            $('#warehouse-block .col-xs-8 .custom-select-container').addClass('loading');
            getWarehouses(city_id, delivery_id);
        }

        if ($(this).val() == 5){
            $('#street_ext').hide();
            $('#street_np').show();



            $('#courier-block .show_np').show();

            $('#street_ext').hide();
            $('#street_ext').val('');

            $('#street_np').show();

            var cityId=$('#cityidorder').val();
            var cityName=$('input[name=city]').val();
            $('input#street_np').typeahead(
                {
                    source: function (query, process) {

                        return $.post('/assets/components/suggest/getStreetsForSuggestJSON.php', {
                            'name': query,
                            'city_id':cityId,
                            'city_name':cityName,
                        }, function (response) {
                            $('input#street_np').focus();
                            var data = new Array();
                            var records = response.content.records;
                            $('#street_np_ref').val('');
                            $.each(response.content.records, function (i) {
                                $.each(response.content.records[i], function (key, val) {
                                    data.push(key + '_' + val);
                                });
                                return process(data);
                            });
                        }, 'json');
                    },
                    matcher: function (item) {
                        return true;
                        if (item.toLowerCase().indexOf(this.query.trim().toLowerCase()) != -1) {
                            return true;
                        }
                    },
                    sorter: function (items) {
                        return items.sort();
                    },
                    highlighter: function (item) {
                        var parts = item.split('_');
                        parts.shift();
                        return parts.join('_');
                    },
                    updater: function (item) {
                        var parts = item.split('_');

                        console.log(parts);

                        $('#street_np_ref').val(parts[0]);//parts[0]);
                        $('#street_np').val(parts);//[1]);

                        return parts[1];//parts.join('_');

                    },
                    autoSelect: false, highlight: false, minLength: 0, delay: 400, items: 10
                });
            $('#courier-block .hidenp').hide();
            $('#courier-block .hidenp').hide();



        }




        // $('#payments').prop('selectedIndex',1);
        /*if ($(this).val() == 4) {

            $('#warehouse-block').addClass('hide');
            $('#courier-block').removeClass('hide');
            $('#samovivoz-block').addClass('hide');
        }
        else if ($(this).val() == 1) {
            $('#warehouse-block').addClass('hide');
            $('#samovivoz-block').removeClass('hide');
            $('#courier-block').addClass('hide');
        }
        else {
            $('#courier-block').addClass('hide');
            $('#samovivoz-block').addClass('hide');
            $('#warehouse-block').removeClass('hide');
            $('#warehouse-block .col-xs-8 .custom-select-container').addClass('loading');
            getWarehouses(city_id, delivery_id);

        }*/
    });

   /*if ($('#deliveries').val() == 4) {
        $('#warehouse-block').addClass('hide');
        $('#courier-block').removeClass('hide');
    }
    else if ($('#deliveries').val() == 1) {
        $('#samovivoz-block').removeClass('hide');
        $('#courier-block').addClass('hide');
        $('#warehouse-block').addClass('hide');

    } else {
        $('#courier-block').addClass('hide');
        $('#samovivoz-block').addClass('hide');
        $('#warehouse-block').removeClass('hide');
    }*/
    if (($('#deliveries').val() == 5) || ($('#deliveries').val() == 4)) {
        $('#warehouse-block').addClass('hide');
        $('#courier-block').removeClass('hide');
    }
    else if ($('#deliveries').val() == 1) {
        $('#samovivoz-block').removeClass('hide');
        $('#courier-block').addClass('hide');
        $('#warehouse-block').addClass('hide');
    } else {
        $('#courier-block').addClass('hide');
        $('#samovivoz-block').addClass('hide');
        $('#warehouse-block').removeClass('hide');
    }


    $(".payment-descr[data-payment-id='" + $('#payments').find(':selected').val() + "']").removeClass('hidden');

    $('#payments').on('change', function (e) {
        $('.payment-descr').addClass('hidden');
        $(".payment-descr[data-payment-id='" + $(this).val() + "']").removeClass('hidden');
        // if ($(this).val() == 6) {
        //     if ($('#summadostavkaorder').html() == 'Бесплатно') $('#descdeliveryorder').html('Внимание! Перевод денег осуществляется за Ваш счет');
        //     else $('#descdeliveryorder').html('');
        // }
        // else $('#descdeliveryorder').html('');


        if ($(this).val() == 6) {
            if ($('#summadostavkaorder').html() == 'Бесплатно') {
                $('#descdeliveryorder').html(miniShop2Config.textNalosh);
            }
            else if (total_cost_order < miniShop2Config.amountNalosh) {
                payment_desc = miniShop2Config.textNaloshBeforeAmount;//'Заказы до 199 грн наложенным платежом не отправляются';
                //$(".payment-descr[data-payment-id='" + $(this).val() + "']").html()+'<br/>Заказы до 199 грн наложенным платежом не отправляются';
                $(".payment-descr[data-payment-id='" + $(this).val() + "']").html(payment_desc);
            }
            else {
                $('#descdeliveryorder').html('');
                $(".payment-descr[data-payment-id='" + $(this).val() + "']").html(miniShop2Config.naloshPay);
                //'Перевод денег осуществляется за Ваш счет');

            }
        } else $('#descdeliveryorder').html('');

    });

});
$('.payorder').on('click', function () {

    if ($(this).data('value') != '') location.href = $(this).data('value');
    return false;
});

$('#getorder input[name="msorder"]').on('keyup', function () {
    var msorder = $(this).val();
    var name = $('#getorder').find('.name');
    $.ajax({
        type: "GET",
        url: '/assets/components/minishop2/getorder.php',//getorder',
        data: {
            msorder: msorder
        },
        success: function (data) {
            data = $.parseJSON(data);
            if (data.success === true) {
                $('.errorliqpay').addClass('hide');
                $('.errorliqpay').removeClass('show');
                $(name).text(data.data.name);
                if ((data.data.paymentid != 3) && (data.data.paymentid != 4)) {
                    $('#getorder').find('.orange-btn').addClass('hidden');
                    $('#getorder').find('.pull-left').css('width', '100%');
                }
                else {
                    $('#getorder').find('.orange-btn').removeClass('hidden');
                    if (data.data.link != '') {
                        $('.payorder').data('value', data.data.link);

                    }
                    if (data.data.form != '') $('#getorder').append(data.data.form);
                }

                $('#getorder').find('.payment').removeClass('hidden');
                $('#getorder').find('.deliveryorderfooter').html(data.data.delivery);
                $('#getorder').find('.payment').html(data.data.payment + '<br/>' + data.data.text);
                $('#getorder').find('.green-check-icon').removeClass('hidden');
                $('#getorder').find('.red-check-icon').addClass('hidden');
                $('#getorder').find('.product-price').html(data.data.totalcost);//data.data.total);
                $('#getorder').find('.modal-footer').removeClass('hidden');

            } else {
                //alert(data.message);
                $('.errorliqpay').removeClass('hide');
                $('.errorliqpay').addClass('show');
                $('.errorliqpay').html(data.message);
                $('#getorder').find('.payment').addClass('hidden');
                $('#getorder').find('.green-check-icon').addClass('hidden');
                $('#getorder').find('.red-check-icon').removeClass('hidden');
                $('#getorder').find('.modal-footer').addClass('hidden');
            }
        }
    });
});

// $(document).on("click", "#order_submit", function (e) {
//     $('#msOrder').submit();
//
// });


//$('#payment_liqpay_submit').on('click', function () {
//on('click', '#payment_liqpay_submit', function (e) {
    /*$('.error-phone').html('');
    $('.error-phone').html('');
    e.preventDefault();
    var $form = $('#msOrder');//'#ms2_form');
    //ms2_action_payment
    var action = 'order/submitpayment';//$form.find(miniShop2.actionNamePayment).val();
    //alert(action);
    //var action_payment = $form.find(miniShop2.action_payment).val();
    //console.log(e);
    //alert(action_payment);

    //if (action) {
    var formData = $form.serializeArray();
    formData.push({name: miniShop2.actionNamePayment, value: action});
    formData.push({name: miniShop2.actionName, value: action});

    //action_payment
    miniShop2.sendData = {$form: $form, action: action, formData: formData};
    // console.log(miniShop2.sendData);
    miniShop2.controller();*/
    // }
//});


//$(document).on("click", "#order_submit", function (e) {
    //$('#msOrder').submit();

    /*on('submit', miniShop2.form, function (e) {
        $('.error-phone').html('');
        $('.error-phone').html('');*/
    /*e.preventDefault();
    var $form = $(this);
    var action = $form.find(miniShop2.action).val();
    var action_payment = $form.find(miniShop2.action_payment).val();
    console.log(e);
    //alert(action_payment);

    if (action) {
        var formData = $form.serializeArray();
        formData.push({name: miniShop2.actionName, value: action});

        //action_payment
        miniShop2.sendData = {$form: $form, action: action, formData: formData};
        miniShop2.controller();
    }*/
    //})
    //$("#msOrder").ajaxSubmit({url: action_url, type: 'post'})
//});


$(".count-products").each(function (index) {
    if ($(this).val() > 0) {
        $(this).closest('.count-box').removeClass('hidden');
        $(this).closest('.card-button').children('.add-cart');//.addClass('hidden');
    }
});

action_url = '/assets/components/minishop2/action.php';

function addToCart(product_id, size, count,optionId) {

    if (!count) count = 1;
    var productData = {
        count: count,
        ctx: "web",
        id: product_id,
        option_id: optionId,
        options: {
            size: size
        },
        ms2_action: 'cart/add'
    };
    $.ajax({
        type: "POST",
        url: action_url,
        data: productData,
        success: function (data) {
            data = $.parseJSON(data);
            if (data.success === true) {
                if (data.data.total_count > 0) {
                    $('.basket-header .total').html(data.data.total_count + ' товаров - ' + data.data.total_cost + ' грн.');
                    if ($('#msMiniCart .basket-btn').hasClass('hidden'))
                        $('#msMiniCart .basket-btn').removeClass('hidden');


                    console.log('data.data5');
                    console.log(data.data);
                    console.log('eSputnikProducts');
                    console.log(data.data.eSputnikProducts);
                    console.log('guest_key');
                    console.log(data.data.guest_key);
                    eS('sendEvent', 'StatusCart', {
                        'StatusCart': data.data.eSputnikProducts,
                        'GUID': data.data.guest_key
                    });
                } else {
                    $('.basket-header .total').html('Ваша корзина пуста');
                    $('#msMiniCart .basket-btn').addClass('hidden');
                }
            } else if (data.success === false) {

            }
        }
    });
}


function addToCartMobile(product_id, size, count,optionId) {
    if (!count) count = 1;

    var productData = {
        count: count,
        ctx: "web",
        id: product_id,
        options: {
            size: size
        },
        option_id: optionId,
        ms2_action: 'cart/add'
    };


    if ($('#product_category').val() != undefined) {
        product_vendor = $('#product_vendor').val();
        product_category = $('#product_category').val();
        product_name = $('#product_name').val();
        product_price = $('#product_price').val();
    } else {
        cartadd = $('.product.add-cart');
        product_vendor = cartadd.data('product-vendor');
        product_category = cartadd.data('product-category');

        product_price = $('#productprice').val();//cartadd.data('product-price');

        product_name = cartadd.data('product-name');
    }
    if (count == 1) {
        dataLayer.push({

            'ecommerce': {
                'currencyCode': 'EUR',
                'add': {                                // 'add' actionFieldObject measures.
                    'products': [{                        //  adding a product to a shopping cart.
                        'name': product_name,
                        'id': product_id,
                        'price': product_price,
                        'brand': product_vendor,
                        'category': product_category,
                        'variant': size,
                        'quantity': count
                    }]
                }
            },
            'event': 'gtm-ee-event',
            'gtm-ee-event-category': 'Enhanced Ecommerce',
            'gtm-ee-event-action': 'Adding to Shopping Cart',
            'gtm-ee-event-non-interaction': 'False',
        });
    }

    $.ajax({
        type: "POST",
        url: action_url,
        data: productData,
        success: function (data) {
            data = $.parseJSON(data);
            if (data.success === true) {
                if (data.data.total_count > 0) {
                    /*$('.basket-header .total').html(data.data.total_count + ' товаров - ' + data.data.total_cost + ' грн.');
                     if ($('#msMiniCart .basket-btn').hasClass('hidden'))
                     $('#msMiniCart .basket-btn').removeClass('hidden');*/
                    $('.h-btn-basket').addClass('active');
                    $('#msMiniCart').html('<a href="/order?edit_cart=1"><span class="products-on-basket">' + data.data.total_count + '</span></a>');
                } else {
                    /*$('.basket-header .total').html('Ваша корзина пуста');
                     $('#msMiniCart .basket-btn').addClass('hidden');*/
                    $('#msMiniCart').html('<span class="products-on-basket">' + data.data.total_count + '</span>');
                    $('.h-btn-basket').removeClass('active');

                }


                console.log('data.data1');
                console.log(data.data);
                console.log('eSputnikProducts');
                console.log(data.data.eSputnikProducts);
                console.log('guest_key');
                console.log(data.data.guest_key);
                eS('sendEvent', 'StatusCart', {
                    'StatusCart': data.data.eSputnikProducts,
                    'GUID': data.data.guest_key
                });

                $('.windows-popup-container').fadeOut(300);
            } else if (data.success === false) {

            }
        }
    });
}


function cartChangeMobile(key, count, type) {
//alert($('#deliveries option:selected').val());
//alert($('#cityidorder').val());
    if (type == 'plus') {

    }
    else {

    }

    var productData = {
        count: count,
        ctx: "web",
        key: key,
        cityid: $('#cityidorder').val(),
        delivery: $('#deliveries option:selected').val(),
        payment: $('#payments option:selected').val(),
        ms2_action: 'cart/change'
    };
    $.ajax({
        async: "false",
        type: "POST",
        url: action_url,
        data: productData,
        success: function (data) {
            data = $.parseJSON(data);
            if (data.success === true) {
                var total = data.data.total_cost;
                if (data.data.cost_delivery) {
                    //alert(data.data.cost_delivery);
                    if (typeof data.data.cost_delivery == "number") {
                        cost = data.data.cost_delivery + ' грн.';
                    } else {
                        cost = data.data.cost_delivery;
                    }
                    //typeof a) // "number"
                    //if (data.data.cost_delivery!='Бесплатно') cost=data.data.cost_delivery+' грн.';
                    //else cost=data.data.cost_delivery;
                    $('#summadostavkaorder').html(cost);
                }
                if (data.data.infodelivery) {
                    $('#infodeliveryorder').html(data.data.infodelivery);
                }
                if (data.data.descdelivery) {
                    $('#descdeliveryorder').html(data.data.descdelivery);
                    //$('#summadostavkaorder').html(data.data.descdelivery);//descdeliveryorder
                }
                if ((data.data.economy !== false) && (data.data.economy !== undefined)) {

                    economy = data.data.economy.toString().split('.', 2);
                    if (economy[1] == undefined) economylit = '00';
                    else economylit = economy[1].toString();
                    eco = economy[0] + '<sup>' + economylit.toString() + '</sup>';
                    $('#discountsumma').html(eco);
                } else {

                    eco = '<b>0</b><sup>00</sup>';
                    $('#discountsumma').html(eco);
                }
                if ((data.data.message_promocode !== false) && (data.data.message_promocode !== undefined)) {
                    //$('#messagepromocode').html(data.data.message_promocode);

                }
                //$('#messagepromocode').html('<strong>Внимание!</strong> Нажмите еще раз Применить чтобы использовать промо-код');

                //alert(data.data.economy);
                //totalcart
                //total_cost_order
                $.each(data.data.items, function (i, val) {

                    var product = $('.basket-list').find('*[data-product-key=' + val.key + ']');

                    cost = val.cost;
                    if (val.count == 1) {
                        minuscount = $(product).find('.minus-count');
                        minuscount.addClass('minus-countoff');
                        minuscount.removeClass('minus-count');
                    }
                    cost = cost.toString().split('.', 2);
                    //cost.split('.',2);
                    $(product).find('.price-large b').text(cost[0]);//val.cost);
                    $(product).find('.count-products').val(val.count);
                    if (cost[1] == undefined) $(product).find('.price-large sup.small').text('00');
                    else $(product).find('.price-large sup.small').text(cost[1]);

                    if (key == val.key) {
                        if (val.keynew != '') {
                            keypodarok = val.keynew;
                            if (val.htmlimage != '') {
                                var newproduct1 = $('.basket-list').find('*[data-product-key=' + keypodarok + ']');//.data('product-key', keypodarok);
                                if (newproduct1.html() == undefined) $('.basket-list').append(val.htmlimage);
                            }
                            else {
                                if (parseInt(val.count) < parseInt(val.plusonecondition)) {
                                    var newproduct1 = $('.basket-list').find('*[data-product-key=' + keypodarok + ']');//.data('product-key', keypodarok);
                                    newproduct1.remove();
                                }
                            }
                        }

                    }
                });

                cost = data.data.total_cost_withdelivery.toString().split('.', 2);

                if (cost[1] == undefined) costlit = '00';
                else costlit = cost[1].toString();

                co = cost[0] + '<sup>' + costlit.toString() + '</sup>';
                $('.price-summary .price-xlarge').html(co);
                $('.total_cost_order').val(total);
                //alert(total);

                if (data.data.total_count > 0) {
                    $('.basket-header .total').html(data.data.total_count + ' товаров - ' + data.data.total_cost + ' грн.');
                    if ($('#msMiniCart .basket-btn').hasClass('hidden'))
                        $('#msMiniCart .basket-btn').removeClass('hidden');
                } else {
                    $('.basket-header .total').html('Ваша корзина пуста');
                    $('#msMiniCart .basket-btn').addClass('hidden');
                }
            } else if (data.success === false) {
                //alert(data.message);
            }
        }
    });
}


function cartChange(key, count) {
//alert($('#deliveries option:selected').val());
//alert($('#cityidorder').val());

    var productData = {
        count: count,
        ctx: "web",
        key: key,
        cityid: $('#cityidorder').val(),
        delivery: $('#deliveries option:selected').val(),
        payment: $('#payments option:selected').val(),
        ms2_action: 'cart/change'
    };
    $.ajax({
        async: "false",
        type: "POST",
        url: action_url,
        data: productData,
        success: function (data) {
            data = $.parseJSON(data);
            if (data.success === true) {
                var total = data.data.total_cost;
                if (data.data.cost_delivery) {
                    //alert(data.data.cost_delivery);
                    if (typeof data.data.cost_delivery == "number") {
                        cost = data.data.cost_delivery + ' грн.';
                    } else {
                        cost = data.data.cost_delivery;
                    }
                    //typeof a) // "number"
                    //if (data.data.cost_delivery!='Бесплатно') cost=data.data.cost_delivery+' грн.';
                    //else cost=data.data.cost_delivery;
                    $('#summadostavkaorder').html(cost);
                }
                if (data.data.infodelivery) {
                    $('#infodeliveryorder').html(data.data.infodelivery);
                }
                if (data.data.descdelivery) {
                    $('#descdeliveryorder').html(data.data.descdelivery);
                    //$('#summadostavkaorder').html(data.data.descdelivery);//descdeliveryorder
                }
                if ((data.data.economy !== false) && (data.data.economy !== undefined)) {

                    economy = data.data.economy.toString().split('.', 2);
                    if (economy[1] == undefined) economylit = '00';
                    else economylit = economy[1].toString();
                    eco = economy[0] + '<sup>' + economylit.toString() + '</sup>';
                    $('#discountsumma').html(eco);
                } else {

                    eco = '<b>0</b><sup>00</sup>';
                    $('#discountsumma').html(eco);
                }
                if ((data.data.message_promocode !== false) && (data.data.message_promocode !== undefined)) {
                    //$('#messagepromocode').html(data.data.message_promocode);

                }
                //$('#messagepromocode').html('<strong>Внимание!</strong> Нажмите еще раз Применить чтобы использовать промо-код');

                //alert(data.data.economy);
                //totalcart
                //total_cost_order
                $.each(data.data.items, function (i, val) {

                    var product = $('.basket-list').find('*[data-product-key=' + val.key + ']');

                    cost = val.cost;
                    if (val.count == 1) {
                        minuscount = $(product).find('.minus-count');
                        minuscount.addClass('minus-countoff');
                        minuscount.removeClass('minus-count');
                    }
                    cost = cost.toString().split('.', 2);
                    //cost.split('.',2);
                    $(product).find('.price-large b').text(cost[0]);//val.cost);
                    $(product).find('.count-products').val(val.count);
                    if (cost[1] == undefined) $(product).find('.price-large sup.small').text('00');
                    else $(product).find('.price-large sup.small').text(cost[1]);

                    if (key == val.key) {
                        if (val.keynew != '') {
                            keypodarok = val.keynew;
                            if (val.htmlimage != '') {
                                var newproduct1 = $('.basket-list').find('*[data-product-key=' + keypodarok + ']');//.data('product-key', keypodarok);
                                if (newproduct1.html() == undefined) $('.basket-list').append(val.htmlimage);
                            }
                            else {
                                if (parseInt(val.count) < parseInt(val.plusonecondition)) {
                                    var newproduct1 = $('.basket-list').find('*[data-product-key=' + keypodarok + ']');//.data('product-key', keypodarok);
                                    newproduct1.remove();
                                }
                            }
                        }

                    }
                });

                cost = data.data.total_cost_withdelivery.toString().split('.', 2);

                if (cost[1] == undefined) costlit = '00';
                else costlit = cost[1].toString();

                co = cost[0] + '<sup>' + costlit.toString() + '</sup>';
                $('.price-summary .price-xlarge').html(co);
                $('.total_cost_order').val(total);
                //alert(total);

                if (data.data.total_count > 0) {
                    $('.basket-header .total').html(data.data.total_count + ' товаров - ' + data.data.total_cost + ' грн.');
                    if ($('#msMiniCart .basket-btn').hasClass('hidden'))
                        $('#msMiniCart .basket-btn').removeClass('hidden');
                } else {
                    $('.basket-header .total').html('Ваша корзина пуста');
                    $('#msMiniCart .basket-btn').addClass('hidden');
                }
            } else if (data.success === false) {
                //alert(data.message);
            }
        }
    });
}


function cartChangeMobile(key, count) {

    var productData = {
        count: count,
        ctx: "web",
        key: key,
        //cityid:$('#cityidorder').val(),
        //delivery:$('#deliveries option:selected').val(),
        //payment:$('#payments option:selected').val(),
        ms2_action: 'cart/change'
    };
    $.ajax({
        async: "false",
        type: "POST",
        url: action_url,
        data: productData,
        success: function (data) {
            data = $.parseJSON(data);
            if (data.success === true) {
                var total = data.data.total_cost;


                /*if ( data.data.cost_delivery)
                 {
                 //alert(data.data.cost_delivery);
                 if(typeof data.data.cost_delivery=="number")
                 {
                 cost=data.data.cost_delivery+' грн.';
                 }else
                 {
                 cost=data.data.cost_delivery;
                 }

                 $('#summadostavkaorder').html(cost);
                 }
                 if ( data.data.infodelivery)
                 {
                 $('#infodeliveryorder').html(data.data.infodelivery);
                 }
                 if ( data.data.descdelivery)
                 {
                 $('#descdeliveryorder').html(data.data.descdelivery);

                 }*/
                /*if ((data.data.economy!==false)&&(data.data.economy!==undefined))
                 {

                 economy=data.data.economy.toString().split('.',2);
                 //if (economy[1]==undefined) economylit='00';
                 //else economylit=economy[1].toString();
                 //	eco=economy[0]+'<sup>'+economylit.toString()+'</sup>';
                 //$('#discountsumma').html(eco);

                 $('.basket-table-product-sale div.sale').html();

                 }else{

                 eco='<b>0</b><sup>00</sup>';
                 $('#discountsumma').html(eco);
                 }*/

                $.each(data.data.items, function (i, val) {

                    var product = $('.basket-table-list').find('*[data-product-key=' + val.key + ']');

                    cost = val.cost;
                    if (val.count == 1) {
                        minuscount = $(product).find('.minus-count');
                        minuscount.addClass('minus-countoff');
                        minuscount.removeClass('minus-count');
                    }
                    cost = cost.toString();//.split('.',2);
                    //cost.split('.',2);
                    // $(product).find('.price-large b').text(cost[0]);//val.cost);
                    //$(product).find('.count-products').val(val.count);
                    //if (cost[1]==undefined) $(product).find('.price-large sup.small').text('00');
                    //else $(product).find('.price-large sup.small').text(cost[1]);

                    //()
                    $(product).find('.product-total-price').html(cost);//+' грн.');
                    if (key == val.key) {
                        if (val.keynew != '') {
                            keypodarok = val.keynew;
                            if (val.htmlimage != '') {
                                var newproduct1 = $('.basket-table-list').find('*[data-product-key=' + keypodarok + ']');//.data('product-key', keypodarok);
                                if (newproduct1.html() == undefined) $('.basket-table-list').append(val.htmlimage);
                            }
                            else {
                                if (parseInt(val.count) < parseInt(val.plusonecondition)) {
                                    var newproduct1 = $('.basket-table-list').find('*[data-product-key=' + keypodarok + ']');//.data('product-key', keypodarok);
                                    newproduct1.remove();
                                }
                            }
                        }

                    }
                });

                cost = data.data.total_cost_withdelivery.toString();//.split('.',2);

                //	//if (cost[1]==undefined) costlit='00';
                //else costlit=cost[1].toString();

                $('.orders-total-price div').html(cost);//+' грн.');
                /*
                 co=cost[0]+'<sup>'+costlit.toString()+'</sup>';
                 $('.price-summary .price-xlarge').html(co);
                 $('.total_cost_order').val(total);

                 if (data.data.total_count > 0) {
                 $('.basket-header .total').html(data.data.total_count + ' товаров - ' + data.data.total_cost + ' грн.');
                 if ($('#msMiniCart .basket-btn').hasClass('hidden'))
                 $('#msMiniCart .basket-btn').removeClass('hidden');
                 } else {
                 $('.basket-header .total').html('Ваша корзина пуста');
                 $('#msMiniCart .basket-btn').addClass('hidden');
                 }*/
            } else if (data.success === false) {
                //alert(data.message);
            }
        }
    });
}


function removeFromCart(key) {
    var productData = {
        key: key,
        cityid: $('#cityidorder').val(),
        delivery: $('#deliveries option:selected').val(),
        payment: $('#payments option:selected').val(),
        ctx: "web",
        ms2_action: 'cart/remove'
    };
    $.ajax({
        type: "POST",
        url: action_url,
        data: productData,
        success: function (data) {
            data = $.parseJSON(data);
            if (data.success === true) {
                if (data.data.total_count > 0) {
                    $('.basket-header .total').html(data.data.total_count + ' товаров - ' + data.data.total_cost + ' грн.');
                    //total_cost
                    cost = data.data.total_cost_withdelivery.toString().split('.', 2);
                    if (cost[1] == undefined) costlit = '00';
                    else costlit = cost[1];
                    co = cost[0] + '<sup>' + costlit + '</sup>';
                    $('.price-summary .price-xlarge').html(co);
                    $('.total_cost_order').val(data.data.total_cost);


                    if (data.data.keydelete) {
                        //alert(data.data.keydelete);
                        $('.basket-list').find('*[data-product-key=' + data.data.keydelete + ']').remove();
                    }


                    var total = data.data.total_cost;
                    if (data.data.cost_delivery) {

                        if (typeof data.data.cost_delivery == "number") {
                            cost = data.data.cost_delivery + ' грн.';
                        } else {
                            cost = data.data.cost_delivery;
                        }

                        //if (data.data.cost_delivery!='Бесплатно') cost=data.data.cost_delivery+' грн.';else cost=data.data.cost_delivery;
                        $('#summadostavkaorder').html(cost);//data.data.cost_delivery+' грн.');
                    }
                    if (data.data.infodelivery) {
                        $('#infodeliveryorder').html(data.data.infodelivery);
                    }
                    if (data.data.descdelivery) {
                        $('#descdeliveryorder').html(data.data.descdelivery);
                        //$('#summadostavkaorder').html(data.data.descdelivery);//descdeliveryorder
                    }
                    //$('#messagepromocode').html('Внимание! Нажмите еще раз Применить чтобы использовать промо-код');
                }
                else {
                    $('.basket-header .total').html('Ваша корзина пуста');
                    $('.product-option.basket-item').remove();
                    $('.price-summary .price-xlarge').text(0);
                    $('.total_cost_order').val(0);
                }
            } else if (data.success === false) {

            }
        }
    });
}


function removeFromCartMobile(key, button) {
    var productData = {
        key: key,
        //cityid:$('#cityidorder').val(),key
        //delivery:$('#deliveries option:selected').val(),
        //payment:$('#payments option:selected').val(),
        ctx: "web",
        ms2_action: 'cart/remove'
    };


    product_id = $(button).data('product-id');

    product_vendor = $(button).data('product-vendor');
    product_category = $(button).data('product-category');
    product_price = $(button).data('product-price');
    product_size = $(button).data('size');
    product_name = $(button).data('product-name');
    /*
     alert(product_name);
     alert(product_id);
     alert(product_price);
     alert(product_vendor);
     alert(product_category);
     alert(product_size);*/
    dataLayer.push({
        'ecommerce': {
            'remove': {                               // 'remove' actionFieldObject measures.
                'products': [{                          //  removing a product to a shopping cart.
                    'name': product_name,
                    'id': product_id,
                    'price': product_price,
                    'brand': product_vendor,
                    'category': product_category,
                    'variant': product_size,
                    'quantity': 1
                }]
            }
        },
        'event': 'gtm-ee-event',
        'gtm-ee-event-category': 'Enhanced Ecommerce',
        'gtm-ee-event-action': 'Removing from Shopping Cart',
        'gtm-ee-event-non-interaction': 'False',

    });


    $.ajax({
        type: "POST",
        url: action_url,
        data: productData,
        success: function (data) {
            data = $.parseJSON(data);
            //alert(data.data.total_counts);
            if (data.success === true) {
                if (data.data.total_count > 0) {
                    //$('.basket-header .total').html(data.data.total_count + ' товаров - ' + data.data.total_cost + ' грн.');
                    //total_cost
                    /*cost=data.data.total_cost_withdelivery.toString().split('.',2);
                     if (cost[1]==undefined) costlit='00';
                     else costlit=cost[1];
                     co=cost[0]+'<sup>'+costlit+'</sup>';
                     // $('.price-summary .price-xlarge').html(co);
                     //  $('.total_cost_order').val(data.data.total_cost);
                     */

                    if (data.data.total_count > 0) {

                        $('#msMiniCart').html('<a href="/order?edit_cart=1"><span class="products-on-basket">' + data.data.total_count + '</span></a>');
                    } else {


                    }

                    //$('#msMiniCart span').html('<a href="/order?edit_cart=1">'+data.data.total_count+'</a>');
                    //<a href="/order?edit_cart=1">5</a>

                    // if (data.data.keydelete) {
                    //alert(data.data.keydelete);
                    $('.basket-table-list').find('*[data-product-key=' + key + ']').remove();
                    // }


                    var total = data.data.total_cost;

                    $('.orders-total-price div').html(total + ' грн.');
                    /*
                     if ( data.data.cost_delivery)
                     {

                     if(typeof data.data.cost_delivery=="number")
                     {
                     cost=data.data.cost_delivery+' грн.';
                     }else
                     {
                     cost=data.data.cost_delivery;
                     }

                     //if (data.data.cost_delivery!='Бесплатно') cost=data.data.cost_delivery+' грн.';else cost=data.data.cost_delivery;
                     $('#summadostavkaorder').html(cost);//data.data.cost_delivery+' грн.');
                     }
                     if ( data.data.infodelivery)
                     {
                     $('#infodeliveryorder').html(data.data.infodelivery);
                     }
                     if ( data.data.descdelivery)
                     {
                     $('#descdeliveryorder').html(data.data.descdelivery);
                     //$('#summadostavkaorder').html(data.data.descdelivery);//descdeliveryorder
                     }*/

                }
                else {
                    /*$('.basket-header .total').html('Ваша корзина пуста');
                     $('.product-option.basket-item').remove();
                     $('.price-summary .price-xlarge').text(0);$('.total_cost_order').val(0);*/
                    $('#msMiniCart').html('<span class="products-on-basket">' + data.data.total_count + '</span>');
                    $('.h-btn-basket').removeClass('active');
                    $('.basket-table-list').find('*[data-product-key=' + key + ']').remove();
                    location.reload();
                }
            } else if (data.success === false) {

            }
        }
    });
}


function getCart() {
    var productData = {
        ctx: "web",
        ms2_action: 'cart/get'
    };
    $.ajax({
        async: "false",
        type: "GET",
        url: action_url,
        data: productData,
        success: function (data) {
            data = $.parseJSON(data);
            if (data.success === true) {
                if (data.data.total_count > 0) {
                    $('.basket-header .total').html(data.data.total_count + ' товаров - ' + data.data.total_cost + ' грн.');
                    if ($('#msMiniCart .basket-btn').hasClass('hidden'))
                        $('#msMiniCart .basket-btn').removeClass('hidden');
                } else {
                    $('.basket-header .total').html('Ваша корзина пуста');
                    $('#msMiniCart .basket-btn').addClass('hidden');
                }
            } else if (data.success === false) {

            }
        }
    });
}

function cleanCart() {
    var productData = {
        ctx: "web",
        ms2_action: 'cart/clean'
    };
    $.ajax({
        type: "GET",
        url: action_url,
        data: productData
    });
}
// добавляем товар со страницы продукта
$('.product.add-cart').on('click', function (e) {
    var parent = $(this).closest('.ms2_product');
    var product_id = $(parent).find('input[name="product_id"]').val();
    var size = $(this).closest('.card-costs-item').find('.weight').text();
    var count = $(this).next().children('.count-products').val();
    if (count < 1) {
        $(this).next().children('.count-products').val(1);
    }
    //addToCart(product_id, size, 1);
});

function appendPet(data) {
    //$('#add_pet_form').clone().appendTo('#petlist');
    var clone = $('#add_pet_form').clone();
    clone.removeAttr('id');
    //data = $.parseJSON(data);
    /*if(data.newphoto) {
     clone.find('.img-circle').attr('src',data.newphoto);
     }
     clone.find('select[name="type"]').val(data.type);
     clone.find('select[name="breed"]').val(data.breed);
     clone.find('select[name="genre"]').val(data.genre);
     clone.find('input[name="name"]').val(data.name);
     clone.find('input[name="pet_id"]').val(data.pet_id);
     clone.find('input[name="af_action"]').val(data.af_action);
     clone.find('textarea').text(data.comment);
     clone.find('.col-xs-2').removeClass('hide');*/
    //data.appendTo('#petlist');
    $("#petlist").append(data);
    $('#add_pet_form')[0].reset();
    //$('select').customSelect();
}
function petRemove(id) {
    var petData = {
        id: id
    };
    $.ajax({
        type: "POST",
        url: '/assets/components/pets/removepetdata.php',
        data: petData
    });
}
function petUpdate(data) {
    $.ajax({
        type: "POST",
        url: '/assets/components/pets/updatepetdata.php',
        enctype: 'multipart/form-data',
        data: data,
        processData: false,  // tell jQuery not to process the data
        contentType: false   // tell jQuery not to set contentType
    }).done(function (data) {
        console.log("PHP Output:");
        console.log(data);
    });
}

function getPayments(delivery_id) {

    var paymentData = {
        id: delivery_id,
        total_order: $('.total_cost_order').val(),
        cityidorder: $('#cityidorder').val(),
    };

    $.ajax({
        async: "false",
        type: "GET",
        url: '/assets/components/delivery/getpaymentdata.php',
        data: paymentData,
        success: function (data) {
            data = $.parseJSON(data);
            if (data.success === true) {
                $('#payments').html(data.data);
                //$('#payments').html(data.data);

                $('#infodeliveryorder').html(data.infodelivery);
                //$('#descdeliveryorder').html(data.descdelivery);
                //alert(data.cost_delivery);
                //if (data.cost_delivery!='Бесплатно') cost=data.cost_delivery+' грн.';else cost=data.cost_delivery;
                if (typeof data.cost_delivery == "number") {
                    cost = data.cost_delivery + ' грн.';
                } else {
                    cost = data.cost_delivery;
                }
                //if (data.cost_delivery==0) $('#summadostavkaorder').html('Бесплатно');
                //else
                if (data.cost_delivery != '') {
                    if (data.cost_delivery == 0) $('#summadostavkaorder').html('Бесплатно');
                    else $('#summadostavkaorder').html(cost);//data.cost_delivery+' грн.');
                }
                else {

                    $('#summadostavkaorder').html(data.descdelivery);
                    if (data.descdelivery == null) $('#summadostavkaorder').html('Бесплатно');
                }
                $('#totalcart').html(data.total_order);
                //$('#total_cost_order').html(data.total_order);

                $('.total_cost_order').val(data.total_order_without_delivery);//total_order);

                /*setTimeout(function(){
                 $('#payments').next().remove();
                 // $('#payments').prop('selectedIndex',1);
                 $('.payment-descr').addClass('hidden');
                 $(".payment-descr[data-payment-id='" + $('#payments').prop('selectedIndex',1).val() +"']").removeClass('hidden');
                 $('#payments').customSelect('update');
                 },1000);*/

            } else if (data.success === false) {

            }
        }
    });
}


/*
 var frm = $('#comment-form');
 frm.submit(function (ev) {
 $.ajax({
 type: frm.attr('method'),
 url: frm.attr('action'),
 data: frm.serialize(),
 success: function (data) {
 frm[0].reset();
 ga('send', 'event', 'product', 'review');
 //form.find('.modal-body').html('<div class="thanks-text" style="text-align:center;">Заявка успешно отправлена. <br/>Менеджер свяжется с Вами когда товар появится в наличии</div>');
 //	form.find('.modal-footer').html('');

 $('#feedbackModal .modal-body').html('<div class="thanks-text" style="text-align:center;">Спасибо! <br/>Ваш отзыв успешно отправлен</div>');
 $('#feedbackModal .modal-footer').html('');
 //.modal('hide');
 setTimeout(function(){$('#feedbackModal').modal('hide');},3000);

 }
 });

 ev.preventDefault();
 });
 */


$('#petlist').bind('DOMSubtreeModified', function () {
    if ($('#petlist form').size() == 0)
        $('#petlist .info-msg').show();
    else
        $('#petlist .info-msg').hide();
});

var formHasChanged = false;
var submitted = false;

/*$(document).on('change', '#office-profile-form input, #office-profile-form select, #office-profile-form textarea', function (e) {
 formHasChanged = true;
 });

 $(document).ready(function () {
 window.onbeforeunload = function (e) {
 if (formHasChanged && !submitted) {
 var message = "Вы не сохранили изменения.", e = e || window.event;
 if (e) {
 e.returnValue = message;
 }
 return message;
 }
 }
 });*/

$(document).on("change", '#petlist input[name="newphoto"]', function () {

    $(this).parent().find('.small.upload').html('Изображение загружено');
    //$('#petlist form.form-info-pets').

    $(this).parent().parent().parent().find('button[type="submit"]').trigger('click');

});


$(document).on("change", '.modal-body input[name="newphoto"]', function () {

    $(this).parent().find('.small.upload').html('Изображение загружено<br/> Сохраните изменения');
    //$('#petlist form.form-info-pets').

    //$(this).parent().parent().parent().find('button[type="submit"]').trigger('click');

});

$(document).on("change", '.upload input[name="newphoto"]', function () {

    $(this).parent().find('.upload .file-upload-btn').html('Изображение загружено<br/> Сохраните изменения');
    //$('#petlist form.form-info-pets').

    //$(this).parent().parent().parent().find('button[type="submit"]').trigger('click');

});

function getWarehouses(city_id, delivery_id, idwar) {
    $.get('/assets/components/delivery/getwarehouses.php', {
            'city_id': city_id,
            'delivery_id': delivery_id,
            'idwar': idwar
        },
        function (warehouses) {


            $('#warehouses').html(warehouses);
            setTimeout(function () {
                $('#deliveries').next().remove();
                //$('#deliveries').customSelect('update');
                $('#warehouses').next().remove();
                //$('#warehouses').customSelect('update');
                if (warehouses.length > 0) {
                    $('#warehouse-block .custom-select-container').show();
                    $('#warehouses').prop('disabled', false);
                    $('#warehouse-block .with-errors').html('');
                }
                else {
                    $('#warehouse-block .custom-select-container').hide();//
                    $('#warehouses').prop('disabled', true);
                    $('#warehouse-block .with-errors').show();
                    $('#warehouse-block .with-errors').html('В Вашем населенном пункте нет отделений. Выберите ближайший город');
                }
                $('.deliverieswrap .col-xs-8 .custom-select-container').removeClass('loading');
//setTimeout(function(){
                $('#warehouse-block .col-xs-8 .custom-select-container').removeClass('loading');
                //},500);
            }, 1300);

        },
        'json'
    );
}

$(document).ready(function () {

    $('input[name=suggest_locality]')//.typeahead
        .on('typeahead:open', function ($e) {

            }
        );

    /*$('input[name=suggest_locality]').bind('typeahead:select', function(ev, suggestion) {
     console.log('Selection: ' + suggestion);
     alert('suggestion'+suggestion);
     });*/

    $('.seeallcom').on('click', function () {
        $('#productCardTab li').removeClass('active');
        $('.allcomments').addClass('active');


    });


    $('body').on('click', '.card-costs-item a.askpr', function () {
        product = $(this).data('product');
        article = $(this).data('article');
        //alert(article);
        $('#boxStatus').find('#productask').val(product);
        $('#boxStatus').find('#articleask').val(article);


    });

    $('body').on('click', '.product-item-tab-content a', function () {
        product = $(this).data('product');
        article = $(this).data('article');
        $('#boxStatus').find('#productask').val(product);
        $('#boxStatus').find('#articleask').val(article);
    });

    $('#boxlogin').on('submit', 'form.ajaxloginbox', function () {
        // alert('dddd');
        $.ajax({
            type: "POST",
            cache: false,
            url: "/account/login",
            data: $(this).serializeArray(),
            success: function (data) {
                var errMessage = $(data).find(".has-error .with-errors").text();
                //alert(errMessage);
                if (errMessage == "") {
                    //alert(data);
                    $(".form-group").removeClass('has-error');
                    $(".ajaxloginbox .with-errors").html('');
                    $(".ajaxloginbox .with-errors").removeClass('show');
                    location.reload();
                } else {
                    //alert(errMessage);
                    $(".ajaxloginbox .form-group").addClass('has-error');
                    $(".ajaxloginbox .with-errors").html('<p class="error">' + errMessage + '</p>');
                    $(".ajaxloginbox .with-errors").addClass('show');
                }
            }
        });
        return false;
    });


    //'#buttonsubscribe' click
    $('.buttonsubscribe, #buttonsubscribe').on('click', function () {

        action = $(this).parent().attr('action');//'.formsubscribe')

        //alert($(this).serialize());//serializeArray());
        $.ajax({
            type: "POST",
            //datatype:'json',
            cache: false,
            url: action,//"/account/login",
            data: $(this).parent().serialize(),
            success: function (response) {
                data = $.parseJSON(response);
                //alert(response);
                //alert(data.message);
                $(this).parent().parent().find('.messagesubscribe').html('');
                if (data.success) {
                    dataLayer.push({
                        'event': 'event-GA',
                        'eventCategory': 'subscribe',
                        'eventAction': 'footer'
                    });
                    $('#subscribeSuccessModal .modal-body').html(data.message);
                    $('#subscribeSuccessModal').modal('show');
                } else {
                    $('.messagesubscribe').html(data.message);
                }


            }
        });
        return false;
    });


//'#boxregistr'
//,form.ajaxregistrbox


    /*$(document).on('submit', 'form.formaregistration1', function () {
        haser = false;

        $.each($('form.formaregistration1 input'), function (i) {

            if ($(this).parent().find('.with-errors').html() != '') {

                $(this).parent().addClass('has-error');
                //if ($(this).parent().hasClass("has-error")) {
                $('.formaregistration1 input.submit-btn').addClass('disabled');

                haser = true;
            } else {

            }
        });
        if (haser) {
            return false;
            //$('.formaregistration1 input.submit-btn').removeClass('disabled');
        }//else return false;
        dataForm = $(this).serialize();
        //alert(dataForm);
        $.ajax({
            type: "POST",
            cache: false,
            url: "/account/register",
            data: dataForm,//$(this).serializeArray(),
            success: function (data) {
                var errMessage = $(data).find(".has-error .with-errors").text();
                //alert(errMessage);
                if (errMessage == "") {
                    //alert(data);
                    $(".form-group").removeClass('has-error');
                    $(".ajaxregistrbox .with-errors").html('');
                    $(".ajaxregistrbox .with-errors").removeClass('show');
                    location.reload();
                } else {
                    var textform = $(data).find(".formaregistration .row .col-xs-8").html();
                    //alert(errMessage);
                    $(".ajaxregistrbox .modal-body").html(textform);


                    $("input[name='username']").mask("+389999999999");
                    $("input[name='phone']").mask("+389999999999");

                }
            }
        });
        //alert('submit');
        return false;
    });
    */


    $(document).on('submit', 'form.formaregistration2', function () {
        haser = false;
        $.each($('form.formaregistration2 input'), function (i) {
            if ($(this).parent().hasClass("has-error")) {
                $('.formaregistration2 input.submit-btn').addClass('disabled');
                haser = true;
            }
        });
        if (!haser) {
            $('.formaregistration2 input.submit-btn').removeClass('disabled');
        } else return false;
        dataForm = $(this).serialize();
        $.ajax({
            type: "POST",
            cache: false,
            url: "/account/register",
            data: dataForm,//$(this).serializeArray(),
            success: function (data) {
                var errMessage = $(data).find(".has-error .with-errors").text();
                if (errMessage == "") {
                    $(".form-group").removeClass('has-error');
                    $(".ajaxregistrbox .with-errors").html('');
                    $(".ajaxregistrbox .with-errors").removeClass('show');
                    location.reload();
                } else {
                    var textform = $(data).find(".formaregistration .row .col-xs-8").html();
                    $(".ajaxregistrbox .modal-body").html(textform);


                    $("input[name='username']").mask("+389999999999");
                    $("input[name='phone']").mask("+389999999999");

                }
            }
        });
        //alert('submit');
        return false;
    });


    $('.formreglabel').on('click', function () {
        if ($('.loginform').css('display') == 'none') {
            $('.loginform').show();
            $('.regformorder').hide();
            $('.formreglabel').html('Оформить без авторизации');
        } else {

            $('.regformorder').show();
            $('.loginform').hide();
            $('.formreglabel').html('Я зарегистрирован');
        }

    });


});