<?php
define('MODX_API_MODE', true);
require_once dirname(dirname(dirname(dirname(__FILE__)))) . '/index.php';

$modx->getService('error', 'error.modError');
$modx->setLogLevel(modX::LOG_LEVEL_ERROR);
$modx->setLogTarget('FILE');

/** @var ModSliderRev $modSliderRev */
$modSliderRev = $modx->getService('modSliderRev ', 'ModSliderRev', $modx->getOption('modsliderrevolution.core_path', null, $modx->getOption('core_path') . 'components/modsliderrevolution/') . 'model/modsliderrevolution/', array());

if (trim($modx->getOption('server_protocol')) === 'https') {
    $_SERVER['HTTPS'] = 'on';
}

define('MODX_SLIDER_REV_BASE_URL', $modSliderRev->getSliderBaseUrl());

include $modSliderRev->config['revSliderPath'] . 'embed.php';

if (!isset($_GET['slider'])) return '';
$jQueryInclude = isset($_GET['jq']) ? $_GET['jq'] : 1;

RevSliderEmbedder::jsIncludes($jQueryInclude);
RevSliderEmbedder::cssIncludes();
RevSliderEmbedder::putRevSlider($_GET['slider']);