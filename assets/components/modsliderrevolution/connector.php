<?php
/**
 * modSliderRevolution Connector
 * @package modsliderrevolution
 */

require_once dirname(dirname(dirname(dirname(__FILE__)))).'/config.core.php';
require_once MODX_CORE_PATH.'config/'.MODX_CONFIG_KEY.'.inc.php';
require_once MODX_CONNECTORS_PATH.'index.php';

$corePath = $modx->getOption('modsliderrevolution.core_path',null,$modx->getOption('core_path').'components/modsliderrevolution/');
require_once $corePath.'model/modsliderrevolution/modsliderrev.class.php';
$modx->modsliderrev = new ModSliderRev($modx);

$modx->lexicon->load('modsliderrevolution:default');

/* handle request */
$path = $modx->getOption('processorsPath',$modx->modsliderrev->config,$corePath.'processors/');
$modx->request->handleRequest(array(
    'processors_path' => $path,
    'location' => '',
));
