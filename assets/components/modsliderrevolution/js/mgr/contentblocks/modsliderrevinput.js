(function ($, ContentBlocks) {
    ContentBlocks.fieldTypes.modsliderrevinput = function (dom, data) {
        var input = {
            properties: {}
        };

        input.init = function () {
            input.properties = data.properties;

            dom.find('.contentblocks-field-modsliderrevinput-preview').addClass('slider-size-' + data.properties.thumb_size);

            if (data.alias) {
                input.selectSlider(data);
            }
        };

        dom.find('.contentblocks-field-modsliderrevinput-update').on('click', function (e) {
            e.preventDefault();
            var data = input.getData();
            window.open('index.php?a=index&namespace=modsliderrevolution&id=' + data.id);
        });
        dom.find('.contentblocks-field-modsliderrevinput-refresh').on('click', function (e) {
            e.preventDefault();
            input.selectSlider(input.getData());
        });
        dom.find('.contentblocks-field-modsliderrevinput-remove').on('click', function (e) {
            e.preventDefault();
            dom.find('.modsliderrev_id').val('');
            dom.find('.modsliderrev_alias').val('');
            dom.find('.contentblocks-field-modsliderrevinput-preview').empty();
            dom.removeClass('hasModSliderRev');
            ContentBlocks.fireChange();
        });

        dom.find('.contentblocks-field-modsliderrevinput-add').on('click', function (e) {
            e.preventDefault();
            var w = Ext.getCmp('modsliderrev-sliders-window');
            if (w) w.close();
            w = MODx.load({
                xtype: 'modsliderrev-sliders-window',
                id: 'modsliderrev-sliders-window',
                listeners: {
                    'slider.insert': {
                        fn: function (data) {
                            if (!data || !data.alias) return;
                            input.selectSlider(data);
                        }, scope: this
                    }
                }
            });
            w.show();

        });

        input.selectSlider = function (data) {
            dom.addClass('hasModSliderRev');
            dom.find('.modsliderrev_id').val(data.id);
            dom.find('.modsliderrev_alias').val(data.alias);

            var preview = dom.find('.contentblocks-field-modsliderrevinput-preview');
            if (parseInt(input.properties.cover)) {

            } else {
                preview.html('<iframe class="modsliderrev-slider" src="' + ModSliderRev.config.assetsUrl + 'embed.php?slider=' + data.alias + '" frameborder="0">');
            }
        };

        input.getData = function () {
            return {
                id: dom.find('.modsliderrev_id').val(),
                alias: dom.find('.modsliderrev_alias').val()
            }
        };

        return input;
    }
})(vcJquery, ContentBlocks);