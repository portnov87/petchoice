Ext.onReady(function() {
    MODx.load({ xtype: 'modsliderrev-page-revslidermodtemplates'});
});

ModSliderRev.page.RevsliderModTemplates = function(config) {
    config = config || {};
    Ext.applyIf(config,{
        components: [{
            xtype: 'modsliderrev-panel-revslidermodtemplates'
            ,renderTo: 'modsliderrev-panel-revslidermodtemplates-div'
        }]
    });
    ModSliderRev.page.RevsliderModTemplates.superclass.constructor.call(this,config);
};
Ext.extend(ModSliderRev.page.RevsliderModTemplates,MODx.Component);
Ext.reg('modsliderrev-page-revslidermodtemplates',ModSliderRev.page.RevsliderModTemplates);

