ModSliderRev.panel.RevsliderModTemplates = function (config) {
    config = config || {};
    Ext.apply(config, {
        border: false
        , baseCls: 'modx-formpanel'
        , cls: 'container'
        , items: [{
            xtype: 'modx-tabs'
            , id: 'modsliderrev-revslidermodtemplates-tabs'
            , defaults: {border: true, autoHeight: true}
            , stateEvents: ['tabchange']
            , getState: function () {
                return {activeTab: this.items.indexOf(this.getActiveTab())};
            }
            , items: [{
                title: _('modsliderrevolution.tab.revslidermodtemplates')
                , defaults: {autoHeight: true}
                , items: [{
                    xtype: 'modsliderrev-templates-panel'
                    , cls: 'main-wrapper'
                    , pageSize: 20
                }]
            }]
        }]
    });
    ModSliderRev.panel.RevsliderModTemplates.superclass.constructor.call(this, config);
};
Ext.extend(ModSliderRev.panel.RevsliderModTemplates, MODx.Panel);
Ext.reg('modsliderrev-panel-revslidermodtemplates', ModSliderRev.panel.RevsliderModTemplates);