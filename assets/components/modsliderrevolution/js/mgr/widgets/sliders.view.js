ModSliderRev.panel.Sliders = function (config) {
    config = config || {};

    this.view = MODx.load({
        xtype: 'modsliderrev-sliders-view',
        id: 'modsliderrev-sliders-view',
        cls: 'modsliderrev-sliders',
        containerScroll: true,
        pageSize: parseInt(config.pageSize) || MODx.config.default_per_page,
        emptyText: _('modsliderrevolution.sliders.msg.view_empty'),
        listeners: {
            'slider.insert': {
                fn: function (data) {
                    this.fireEvent('slider.insert', data);
                }, scope: this
            },
            click: {
                fn: function (view, index, node) {
                    var data = view.lookup[node.id];
                    if (!data) return;
                    this.fireEvent('slider.select', data);


                }, scope: this
            },
            dblclick: {
                fn: function (view, index, node) {
                    var data = view.lookup[node.id];
                    if (!data) return;
                    this.fireEvent('slider.insert', data);
                }, scope: this
            }
        }
    });

    Ext.applyIf(config, {
        id: 'modsliderrev-sliders',
        cls: 'browser-view',
        border: false,
        items: [this.view],
        tbar: this.getTopBar(config),
        bbar: this.getBottomBar(config),
    });
    ModSliderRev.panel.Sliders.superclass.constructor.call(this, config);
    this.addEvents('slider.select', 'slider.insert');
};
Ext.extend(ModSliderRev.panel.Sliders, MODx.Panel, {

    Search: function (tf) {
        this.view.getStore().baseParams.query = tf.getValue();
        this.getBottomToolbar().changePage(1);
    },

    clearSearch: function (tf) {
        tf.setValue('');
        this.view.getStore().baseParams.query = '';
        this.getBottomToolbar().changePage(1);
    },

    getTopBar: function (config) {
        return new Ext.Toolbar({
            items: [{
                xtype: 'modsliderrev-combo-search',
                width: 300,
                listeners: {
                    search: {
                        fn: this.Search
                        , scope: this
                    },
                    clear: {
                        fn: this.clearSearch
                        , scope: this
                    },
                },
            }, {
                text: '<i class="icon icon-arrow-right"></i>  ' + _('modsliderrevolution.sliders.btn.editor')
                , cls: 'primary-button'
                , handler: function () {
                    window.open('index.php?a=index&namespace=modsliderrevolution');
                }
            }]
        })
    },

    getBottomBar: function (config) {
        return new Ext.PagingToolbar({
            pageSize: parseInt(config.pageSize) || MODx.config.default_per_page,
            store: this.view.store,
            displayInfo: true,
            autoLoad: true,
            items: ['-',
                _('per_page') + ':',
                {
                    xtype: 'textfield',
                    value: parseInt(config.pageSize) || MODx.config.default_per_page,
                    width: 50,
                    listeners: {
                        change: {
                            fn: function (tf, nv, ov) {
                                if (Ext.isEmpty(nv)) {
                                    return;
                                }
                                nv = parseInt(nv);
                                this.getBottomToolbar().pageSize = nv;
                                this.view.getStore().load({params: {start: 0, limit: nv}});
                            }, scope: this
                        },
                        render: {
                            fn: function (cmp) {
                                new Ext.KeyMap(cmp.getEl(), {
                                    key: Ext.EventObject.ENTER,
                                    fn: function () {
                                        this.fireEvent('change', this.getValue());
                                        this.blur();
                                        return true;
                                    },
                                    scope: cmp
                                });
                            }, scope: this
                        }
                    }
                }
            ]
        });
    },

});
Ext.reg('modsliderrev-sliders-panel', ModSliderRev.panel.Sliders);


ModSliderRev.view.Sliders = function (config) {
    config = config || {};
    this._initSliders();
    Ext.applyIf(config, {
        url: ModSliderRev.config.connectorUrl,
        fields: ['id', 'title', 'alias', 'thumb_url', 'thumb_class', 'thumb_style', 'actions'],
        id: 'modsliderrev-sliders-view',
        baseParams: {
            action: 'mgr/sliders/getlist',
            limit: config.pageSize || MODx.config.default_per_page
        },
        tpl: this.sliders.thumb,
        itemSelector: 'div.modx-browser-thumb-wrap',
        prepareData: this.formatData.createDelegate(this)
    });
    ModSliderRev.view.Sliders.superclass.constructor.call(this, config);

    this.addEvents('slider.insert');

    var widget = this;
    this.getStore().on('beforeload', function () {
        widget.getEl().mask(_('loading'), 'x-mask-loading');
    });
    this.getStore().on('load', function () {
        widget.getEl().unmask();
    });
};
Ext.extend(ModSliderRev.view.Sliders, MODx.DataView, {
    sliders: {},
    run: function (p) {
        p = p || {};
        var v = {};
        Ext.apply(v, this.store.baseParams);
        Ext.apply(v, p);
        this.changePage(1);
        this.store.baseParams = v;
        this.store.load();
    },
    formatData: function (data) {
        this.lookup['modsliderrev-slider-' + data.id] = data;
        return data;
    },
    _initSliders: function () {
        this.sliders.thumb = new Ext.XTemplate(
            '<tpl for=".">\
                <div class="modx-browser-thumb-wrap modx-pb-thumb-wrap modsliderrev-thumb-wrap modsliderrev-thumb-wrap-slide {class}" id="modsliderrev-slider-{id}">\
                    <div class="modx-browser-thumb modx-pb-thumb modsliderrev-thumb modsliderrev-thumb-slide">\
                        <span class="{thumb_class}" style="{thumb_style} "></span>\
					</div>\
					<div><small><span class="modsliderrev-thumb-title" title="{alias}"><strong>#{id}</strong> {title}</span></small></div>\
				</div>\
			</tpl>'
        );
        this.sliders.thumb.compile();
    },
    _showContextMenu: function (v, i, n, e) {
        e.preventDefault();
        var data = this.lookup[n.id];
        var m = this.cm;
        m.removeAll();

        var menu = ModSliderRev.utils.getMenu(data.actions, this, this._getSelectedIds());
        for (var item in menu) {
            if (!menu.hasOwnProperty(item)) {
                continue;
            }
            m.add(menu[item]);
        }

        m.show(n, 'tl-c?');
        m.activeNode = n;
    },
    _getSelectedIds: function () {
        var ids = [];
        var selected = this.getSelectedRecords();

        for (var i in selected) {
            if (!selected.hasOwnProperty(i)) {
                continue;
            }
            ids.push(selected[i]['id']);
        }

        return ids;
    },
    update: function (btn, e) {
        var node = this.cm.activeNode;
        var data = this.lookup[node.id];
        if (!data) return;
        window.open('index.php?a=index&namespace=modsliderrevolution&id=' + data.id);
    },
    insert: function (btn, e) {
        var node = this.cm.activeNode;
        var data = this.lookup[node.id];
        if (!data) return;
        this.fireEvent('slider.insert', data);
    }

});
Ext.reg('modsliderrev-sliders-view', ModSliderRev.view.Sliders);

ModSliderRev.window.Sliders = function (config) {
    config = config || {};
    Ext.applyIf(config, {
        title: _('modsliderrevolution.sliders.win.title')
        , modal: true
        , width: 1000
        , height: 600
        , autoHeight: false
        , autoScroll: true
        , buttons: [{
            text: _('cancel')
            , scope: this
            , handler: function () {
                this.hide();
            }
        }, {
            text: '<i class="icon icon-code"></i> ' + _('modsliderrevolution.sliders.btn.insert')
            , scope: this
            , cls: 'primary-button'
            , handler: function () {
                if (Ext.isEmpty(this.selectSliderData)) return;
                this.insert(this.selectSliderData);
            }

        }]
        , fields: [{
            xtype: 'modsliderrev-sliders-panel',
            listeners: {
                'slider.insert': {
                    fn: this.insert, scope: this
                },
                'slider.select': {
                    fn: function (data) {
                        this.selectSliderData = data;
                    }, scope: this
                }
            }
        }]
    });
    ModSliderRev.window.Sliders.superclass.constructor.call(this, config);
    this.addEvents('slider.insert');
};
Ext.extend(ModSliderRev.window.Sliders, MODx.Window, {
    selectSliderData: {},
    insert: function (data) {
        this.fireEvent('slider.insert', data);
        this.hide();
    }
});
Ext.reg('modsliderrev-sliders-window', ModSliderRev.window.Sliders);

