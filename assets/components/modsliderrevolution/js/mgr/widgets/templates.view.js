ModSliderRev.panel.Templates = function (config) {
    config = config || {};

    this.view = MODx.load({
        xtype: 'modsliderrev-templates-view',
        id: 'modsliderrev-templates-view',
        cls: 'modsliderrev-templates',
        containerScroll: true,
        pageSize: parseInt(config.pageSize) || MODx.config.default_per_page,
        emptyText: _('modsliderrev.msg.empty')
    });

    Ext.applyIf(config, {
        id: 'modsliderrev-templates',
        //cls: 'browser-view',
        border: false,
        items: [this.view],
        tbar: this.getTopBar(config),
        bbar: this.getBottomBar(config),
    });
    ModSliderRev.panel.Templates.superclass.constructor.call(this, config);
};
Ext.extend(ModSliderRev.panel.Templates, MODx.Panel, {

    filter: function (tf) {
        var s = this.view.getStore();
        s.baseParams.filter = tf.getValue();
        this.getBottomToolbar().changePage(1);
    },

    clearFilter: function () {
        var s = this.view.getStore();
        s.baseParams.filter = '';
        this.getBottomToolbar().changePage(1);
    },

    Search: function (tf) {
        this.view.getStore().baseParams.query = tf.getValue();
        this.getBottomToolbar().changePage(1);
    },

    clearSearch: function (tf) {
        tf.setValue('');
        this.view.getStore().baseParams.query = '';
        this.getBottomToolbar().changePage(1);
    },

    getTopBar: function (config) {
        return new Ext.Toolbar({
            items: [{
                xtype: 'modsliderrev-combo-filter',
                width: 210,
                custm: true,
                clear: true,
                addall: true,
                value: 1,
                listeners: {
                    select: {
                        fn: this.filter,
                        scope: this
                    }/*,
                     afterrender: {
                         fn: this.filter,
                         scope: this
                     }*/
                }
            },{
                xtype: 'modsliderrev-combo-search',
                width: 300,
                listeners: {
                    search: {
                        fn: this.Search
                        , scope: this
                    },
                    clear: {
                        fn: this.clearSearch
                        , scope: this
                    },
                },
            },{
                text: '<i class="icon icon-arrow-right"></i>  ' + _('modsliderrevolution.btn.editor')
                , cls: 'primary-button'
                ,handler: function () {
                    window.open('index.php?a=index&namespace=modsliderrevolution');
                }
            }]
        })
    },

    getBottomBar: function (config) {
        return new Ext.PagingToolbar({
            pageSize: parseInt(config.pageSize) || MODx.config.default_per_page,
            store: this.view.store,
            displayInfo: true,
            autoLoad: true,
            items: ['-',
                _('per_page') + ':',
                {
                    xtype: 'textfield',
                    value: parseInt(config.pageSize) || MODx.config.default_per_page,
                    width: 50,
                    listeners: {
                        change: {
                            fn: function (tf, nv, ov) {
                                if (Ext.isEmpty(nv)) {
                                    return;
                                }
                                nv = parseInt(nv);
                                this.getBottomToolbar().pageSize = nv;
                                this.view.getStore().load({params: {start: 0, limit: nv}});
                            }, scope: this
                        },
                        render: {
                            fn: function (cmp) {
                                new Ext.KeyMap(cmp.getEl(), {
                                    key: Ext.EventObject.ENTER,
                                    fn: function () {
                                        this.fireEvent('change', this.getValue());
                                        this.blur();
                                        return true;
                                    },
                                    scope: cmp
                                });
                            }, scope: this
                        }
                    }
                }
            ]
        });
    },

});
Ext.reg('modsliderrev-templates-panel', ModSliderRev.panel.Templates);


ModSliderRev.view.Templates = function (config) {
    config = config || {};
    this._initTemplates();
    Ext.applyIf(config, {
        url: ModSliderRev.config.connectorUrl,
        fields: ['id', 'title', 'description', 'preview', 'img', 'zip'],
        id: 'modsliderrev-templates-view',
        singleSelect:false,
        baseParams: {
            action: 'mgr/revslidermodtemplates/getlist',
            limit: config.pageSize || MODx.config.default_per_page
        },
        tpl: this.templates.thumb,
        //itemSelector: 'div.modx-browser-thumb-wrap',
        listeners: {},
        prepareData: this.formatData.createDelegate(this)
    });
    ModSliderRev.view.Templates.superclass.constructor.call(this, config);
    this.addEvents('select');
    var widget = this;
    this.getStore().on('beforeload', function () {
        widget.getEl().mask(_('loading'), 'x-mask-loading');
    });
    this.getStore().on('load', function () {
        widget.getEl().unmask();
    });
};
Ext.extend(ModSliderRev.view.Templates, MODx.DataView, {
    templates: {},
    run: function (p) {
        p = p || {};
        var v = {};
        Ext.apply(v, this.store.baseParams);
        Ext.apply(v, p);
        this.changePage(1);
        this.store.baseParams = v;
        this.store.load();
    },
    formatData: function (data) {
        this.lookup['modsliderrev-template-' + data.id] = data;
        return data;
    },
    _initTemplates: function () {
        this.templates.thumb = new Ext.XTemplate(
            '<tpl for=".">\
                <div class="modx-browser-thumb-wrap modx-pb-thumb-wrap modsliderrev-thumb-wrap modsliderrev-thumb-wrap-template {class}" id="modsliderrev-template-{id}">\
                    <div class="modx-browser-thumb modx-pb-thumb modsliderrev-thumb modsliderrev-thumb-template">\
                        <img src="{img}" title="{title}" />\
                        <div class="action-group">\
                        <a href="{preview}" title="' + _('modsliderrevolution.btn.preview') + '" class="btn-action" target="_blank"><i class="fa fa-search"></i></a>\
						<a href="#" onclick="Ext.getCmp(\'modsliderrev-templates-view\').install(\'{id}\'); return false;" title="' + _('modsliderrevolution.btn.install') + '" class="btn-action" target="_blank"><i class="fa fa-plug"></i></a>\
						<a href="{zip}" title="' + _('modsliderrevolution.btn.download') + '" class="btn-action" target="_blank"><i class="fa fa-download"></i></a>\
						</div>\
					</div>\
					<small><span class="modsliderrev-thumb-title" title="{title}">{title}</span></small>\
				</div>\
			</tpl>'
        );
        this.templates.thumb.compile();
    },
    install: function (id) {
        MODx.Ajax.request({
            url: ModSliderRev.config.connectorUrl
            , params: {
                action: 'mgr/revslidermodtemplates/install'
                , id: id
            }
            , scope: this
            , listeners: {
                success: {
                    fn: function (r) {
                        MODx.msg.status({
                            title: _('success'),
                            message: r.message,
                            dontHide: false
                        });
                        //this.store.reload();
                    }, scope: this
                },
                failure: {
                    fn: function (e) {
                        MODx.msg.alert(_('error'), e.message);
                    }, scope: this
                },
            }
        });
    }

});
Ext.reg('modsliderrev-templates-view', ModSliderRev.view.Templates);