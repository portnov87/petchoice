Ext.onReady(function () {
    if (!MODx.loadRTE) return;
    CKEDITOR.plugins.add('modsliderrev',
        {
            init: function (editor) {
                editor.addCommand('sliders',
                    {
                        exec: function (editor) {
                            var w = Ext.getCmp('modsliderrev-sliders-window');
                            if (w) w.close();
                            w = MODx.load({
                                xtype: 'modsliderrev-sliders-window',
                                id: 'modsliderrev-sliders-window',
                                listeners: {
                                    'slider.insert': {
                                        fn: function (data) {
                                            if (!data || !data.alias) return;
                                            var code = "[[modSliderRevolution? &slider=`" + data.alias + "`]]";
                                            editor.insertHtml(code);
                                        }, scope: this
                                    }
                                }
                            });
                            w.show();
                        }
                    });
                editor.ui.addButton('modsliderrev',
                    {
                        label: _('modsliderrevolution.sliders.btn.rtf'),
                        command: 'sliders',
                        // icon: this.path + 'icon.png'
                    });
            }
        });
});