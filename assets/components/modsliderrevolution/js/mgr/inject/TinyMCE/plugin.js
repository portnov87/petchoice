Ext.onReady(function () {
    if (!MODx.loadRTE) return;
    tinymce.PluginManager.add('modsliderrev', function (editor, url) {
        editor.addButton('modsliderrev', {
            title: _('modsliderrevolution.sliders.btn.rtf'),
            icon: false,
            onclick: function () {
                var w = Ext.getCmp('modsliderrev-sliders-window');
                if (w) w.close();
                w = MODx.load({
                    xtype: 'modsliderrev-sliders-window',
                    id: 'modsliderrev-sliders-window',
                    listeners: {
                        'slider.insert': {
                            fn: function (data) {
                                if (!data || !data.alias) return;
                                var code = "[[modSliderRevolution? &slider=`" + data.alias + "`]]";
                                tinymce.execCommand('mceInsertContent', 0, code);
                            }, scope: this
                        }
                    }
                });
                w.show();
            }
        });
    });
});