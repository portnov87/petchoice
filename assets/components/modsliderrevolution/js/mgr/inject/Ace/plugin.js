Ext.onReady(function () {
    if (MODx.ux.Ace.replaceTextAreas) {
        var items = Ext.select('.ace_editor');
        items.each(function (item) {
            var btn = Ext.DomHelper.append(item, '<span class="btn-modsliderrev" title="' + _('modsliderrevolution.sliders.btn.rtf') + '"></span>', true);
            btn.on('click', function (e) {
                if (Ext.isEmpty(item.getAttribute('env')) || Ext.isEmpty(item.getAttribute('env').editor)) return;
                var editor = item.getAttribute('env').editor,
                    w = Ext.getCmp('modsliderrev-sliders-window');
                if (w) w.close();
                w = MODx.load({
                    xtype: 'modsliderrev-sliders-window',
                    id: 'modsliderrev-sliders-window',
                    listeners: {
                        'slider.insert': {
                            fn: function (data) {
                                if (!data || !data.alias) return;
                                var code = "[[modSliderRevolution? &slider=`" + data.alias + "`]]";
                                editor.session.insert(editor.getCursorPosition(), code);
                            }, scope: this
                        }
                    }
                });
                w.show();

            }, this);
        });
    }
});