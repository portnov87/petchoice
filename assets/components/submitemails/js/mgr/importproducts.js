var importproducts = function(config) {
    config = config || {};
    importproducts.superclass.constructor.call(this,config);
};
Ext.extend(importproducts,Ext.Component,{
    page:{},window:{},grid:{},tree:{},panel:{},combo:{},config: {},view: {}
});
Ext.reg('importproducts',importproducts);

importproducts = new importproducts();