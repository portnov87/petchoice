Ext.onReady(function() {
    MODx.load({ xtype: 'modextra-page-home'});
});

submitemails.page.Home = function(config) {
    config = config || {};
    Ext.applyIf(config,{
        components: [{
            xtype: 'modextra-panel-home'
            ,renderTo: 'modextra-panel-home-div'
        }]
    }); 
    submitemails.page.Home.superclass.constructor.call(this,config);
};
Ext.extend(submitemails.page.Home,MODx.Component);
Ext.reg('modextra-page-home',submitemails.page.Home);