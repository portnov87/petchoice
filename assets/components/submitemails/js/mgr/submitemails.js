var submitemails = function(config) {
    config = config || {};
    submitemails.superclass.constructor.call(this,config);
};
Ext.extend(submitemails,Ext.Component,{
    page:{},window:{},grid:{},tree:{},panel:{},combo:{},config: {},view: {}
});
Ext.reg('submitemails',submitemails);

submitemails = new submitemails();