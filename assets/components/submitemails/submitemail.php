<?php
error_reporting(E_ALL | E_STRICT);
ini_set('display_errors', 1);
define('MODX_API_MODE', true);
require_once dirname(dirname(dirname(dirname(__FILE__)))) . '/index.php';


$output['success'] = false;
$output['message'] ='Ошибка подписки';
$must_subscribe=false;

if (isset($_POST['email'])&&isset($_POST['name'])&&($_POST['name']!='')&&($_POST['email']!=''))
{
	$name=$_POST['name'];
	$email=$_POST['email'];
	//пользователя необходимо проверить по 2м таблицам пользователи и подписчики
	
	$profile=$modx->getObject('modProfile', array('email'=>$email));
	if ($profile)// значит пользователь есть в базе
	{
		$user=$modx->getObject('modUser',$profile->get('id'));
		if (($user->get('subscribe')==1)||($user->get('subscribe')=='Подписан')){
			$output['success'] = false;
			$output['message'] = 'Вы уже были подписаны.<br/><br/>Пользователь с таким email уже подписан на рассылку Pet Choice!';
		}
		else{
			//$must_subscribe=true;// надо подписать пользователя
			$user->set('subscribe',1);
			if ($user->save())
			{
				$must_subscribe=true;
				
			}
		}
	}else{
		$subscribe=$modx->getObject('UsersSubscribe', array('email'=>$email));
		if ($subscribe)
		{
			if ($subscribe->get('subscribe')==1)
			{
				$output['success'] = false;
				$output['message'] = "<span style='color:red;margin-top:5px; font-size:12px;'>Пользователь с таким email уже подписан</span>";
				//"Вы уже были подписаны.<br/><br/>Пользователь с таким email уже подписан на рассылку Pet Choice!";
			}
			else{
				$must_subscribe=true;
				//$output['success'] = true;
				//$output['message'] = 'Спасибо! Вы успешно подписались на рассылку Pet Choice';
			}			
		}else{
			$must_subscribe=true;
			$subscribe=$modx->newObject('UsersSubscribe');
			$subscribe->set('name',$name);
			$subscribe->set('email',$email);
			if (($modx->user->get('email')==$email)&&($isAuthenticated = $modx->user->isAuthenticated())) 
				$subscribe->set('user_id',$modx->user->get('id'));//$id_cms=$modx->user->get('id');			
			//else $id_cms=0; 
			//if ($isAuthenticated = $modx->user->isAuthenticated()) {
			//$subscribe->set('user_id',$modx->user->get('id'));
			//}
			$subscribe->set('subscribe',1);
			$subscribe->save();
			
		}
	}
	
	
	if ($must_subscribe) // оформляем подписку
	{
		//if ($isAuthenticated = $modx->user->isAuthenticated()) {
			$unisender =  $modx->getService('unisender','Unisender',$modx->getOption('core_path').'components/unisender/model/unisender/');				 
			//$subscr=$unisender->SubscribeUserId( $this->user->id, $dataunisender);
			$list_id=$unisender->list_subscribe;
						
			$api_path = $modx->getOption('unisender.api_path');
			require_once($api_path . 'User.php');
			require_once($api_path . 'ListBook.php');
			$api_key = $modx->getOption('unisender_api');
			
			$imports = new ListBook($api_key);//User($api_key);//
			$data=array();
			$field_names = array(
			'email', 'email_list_ids', 'phone', 'phone_list_ids', 
			'Name','total_order','date_registr',
			'last_order','registration','podpiska','id_cms','categories');
			
			if (($modx->user->get('email')==$email)&&($isAuthenticated = $modx->user->isAuthenticated())) $id_cms=$modx->user->get('id');			
			else $id_cms=0; 
			$data[]=array($email,$list_id,'',$list_id,$name,'0','','',0,1,$id_cms,'');			
			$list_res = $imports->importContacts($field_names, $data, 3, 1, 1);
			
			
			if ($list_res['error'] != 1) {
				$output['success'] = true;
				$output['message'] = 'Спасибо!<br/><br/>Вы успешно подписались на рассылку Pet Choice!';
				
				
				$tpl='mailsubscribe';
									
									$body = $modx->getChunk($tpl,										
											array(										
												'name'=>$name,'fullname'=>$name
											)										
									);
									$miniShop2 = $modx->getService('minishop2');
						$miniShop2->initialize($modx->context->key);
						
						$subject=$modx->getOption('subject_subscribe');
									$miniShop2->sendEmail($email, $subject, $body);
									
									//echo $content;
									//echo $tpl.' '.$dataprofile['fullname'].' '.$dataprofile['extended']['lastname'].'</br>';
									/*$send = $user->sendEmail(
										$content
										,array(
											'subject' => $modx->getOption('subject_subscribe')//'Увеличилась скидка'//$this->modx->getOption('subject_password')//$this->modx->lexicon('office_auth_email_subject')
										)
									);*/
				
			}
			
			
			
			
			//mailsubscribe
			
			/*$list_ids=array($list_id);
			$fields=array(
			'email'=>$email,
			'name'=>$name);*/
		//}
		
	}
	
	
	//$output['success'] = false;
	//$output['message'] = 'Произошла ошибка при выполнении запроса.';
}else{
	$output['success'] = false;
	$output['message'] = "<span style='color:red;margin-top:5px; font-size:12px;'>Введите имя и email</span>";
}
$response = $modx->toJSON(array('success' => $output['success'], 'message' =>$output['message']));

@session_write_close();
exit($response);