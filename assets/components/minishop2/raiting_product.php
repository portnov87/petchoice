<?php
/**
 * Created by PhpStorm.
 * User: portnovvit
 * Date: 04.01.2019
 * Time: 16:01
 */

set_time_limit(0);
error_reporting(E_ALL | E_STRICT);
ini_set('display_errors', 1);
ini_set('MAX_EXECUTION_TIME', -1);

define('MODX_API_MODE', true);
require_once dirname(dirname(dirname(dirname(__FILE__)))) . '/index.php';
/*
ob_implicit_flush(1);

echo str_pad('', 1024);
ob_start();
@ob_flush();
flush();*/
$miniShop2 = $modx->getService('minishop2');

if (isset($_POST['product_id']) && (isset($_POST['type']))) {
    $product_id = $_POST['product_id'];
    $type = $_POST['type'];

    $product_data = $modx->getObject('msProductData', $product_id);
    if ($product_data) {
        switch ($type) {
            case 'shared':
                $count_shared_old = (int)$product_data->get('count_shared');
                $count_shared_new = $count_shared_old + 1;
                $product_data->set('count_shared', (int)$count_shared_new);
                $product_data->save();
                break;
            case 'like':
                $count_likes_old = (int)$product_data->get('count_likes');
                $count_likes_new = $count_likes_old + 1;
                $product_data->set('count_likes', (int)$count_likes_new);
                $product_data->save();
                break;
            case 'dislike':
                $count_likes_old = (int)$product_data->get('count_likes');
                $count_likes_new = $count_likes_old - 1;
                $product_data->set('count_likes', (int)$count_likes_new);
                $product_data->save();
                break;

        }
        $miniShop2->recountpoints_of_product($product_data);

        echo json_encode(['result' => true, 'message' => 'ok']);
        die();
    }
}
echo json_encode(['result' => false, 'message' => 'error']);
die();