<?php
define('MODX_API_MODE', true);
require_once dirname(dirname(dirname(dirname(__FILE__)))) . '/index.php';


?>
    <form method="post" style="width:1000px;margin:30px auto;">
        <h1>Выгрузить покупателей которые покупали продукт</h1>
        <div>
            <label style="width:48%;float:left;font-size:14px;">Название продукта</label>
            <input type="text" name="name_product"/>
        </div>
        <div>
            <label style="width:48%;float:left;font-size:14px;">Ид опции</label>
            <input type="text" name="id_option"/>
        </div>

        <br style="clear:both;"/>
        <div style="width:100%; float:left;margin-top:20px; text-align:center;">
            <button type="submit" name="submit_users"
                    style="background:blue;color:#fff; text-transform:uppercase;text-align:center; padding:5px 20px;">
                Обработать
            </button>
        </div>

    </form>
<?php

if (isset($_POST['submit_users'])) {


    $name_product = (isset($_POST['name_product']) ? $_POST['name_product'] : false);
    $id_option = (isset($_POST['id_option']) ? $_POST['id_option'] : false);
    $q = $modx->newQuery('modUser');//
    /*$day7=86400*80;
    $daydate=date("Y-m-d",time()-$day7);
    Данные:
- имя
- телефон
- имейл
    $daydatebegin=$daydate.' 00:00:00';
    $daydateend=$daydate.' 23:59:59';
    *///modx_ms2_order_products
    /*$where = array(
        'modUserProfile.email:='=>'',
        'msProductData.vendor:='=>$id_vendor,
    );*/
    /*- имя
- телефон
- имейл
- лояльность
- зарегистрирован / не зарегистрирован
- сумма покупок*/
    //if ($user_id) $where['modUser.id:=']=$user_id;
    //$where['modUser.registr:=']=1;

    // $q->where("order.createdon > CAST('" . $daydate . "' AS DATETIME) " , xPDOQuery::SQL_AND);

    $q->select(array(
        'modUserProfile.email',
        'modUser.username',
        'modUserProfile.phone',
        'modUserProfile.mobilephone',
        'modUser.id',
        'modUser.registr',
        'modUserProfile.fullname',
        'modUser.bonuses',
        'modUserProfile.extended',
        'modUser.last_order',
        //'msProductData.vendor'
    ));

    //msOrder');
    //	$q->leftJoin('msOrder', 'msOrder', 'msOrder.id=msOrderProduct.order_id');
    //$q->leftJoin('msOrderProduct', 'msOrderProduct', 'msOrder.id=msOrderProduct.order_id');
    //$q->innerJoin('msProductData', 'msProductData', 'msOrderProduct.product_id=msProductData.id');
    //$q->innerJoin('modUser', 'modUser', 'msOrder.user_id=modUser.id');
    $q->innerJoin('modUserProfile', 'modUserProfile', 'modUser.id=modUserProfile.internalKey');
    //$q->limit(10);
    //$q->sortby('msOrder.createdon', 'DESC');
    $q->groupby('modUser.id');//modUser.id');//msOrder.id');
    //$q->where($where);
    $q->prepare();
    $q->stmt->execute();

    $result = $q->stmt->fetchAll(PDO::FETCH_ASSOC);
    /*echo "<pre>result";
    print_r($result);
    echo "</pre>";*/

    $user_csv = "имя~телефон~доптелефон~город~улица~email~лояльность~зарегистрирован/не зарегистрирован~сумма покупок~количество заказов~доставка~дата последнего заказа~бонусы~бренды;\r\n";
    //~имя~цена~производитель;\r\n";
    foreach ($result as $res) {
        $totalorder = 0;
        $countorder = 0;
        $q = $modx->newQuery('msOrder');
        $q->select('*');
        $q->where(array('msOrder.user_id:=' => $res['id'], 'msOrder.status:=' => 2));

        $brandtag = '';
        $brands = [];
        $q->prepare();
        $q->stmt->execute();


        $resultorders = $q->stmt->fetchAll(PDO::FETCH_ASSOC);

        $findOption = false;
        if (count($resultorders) > 0) {
            foreach ($resultorders as $order) {
                $totalorder = $totalorder + $order['msOrder_cost'];
                $countorder++;
                if (($name_product) || ($id_option)) {

                    $q = $modx->newQuery('msOrderProduct');
                    $where = array(
                        'order_id' => $order['msOrder_id']
                    );

                    if ($name_product) {
                        $where[] = [
                            'msProduct.pagetitle:like' => "%$name_product%"
                        ];
                    }


                    $q->leftJoin('msProduct', 'msProduct', array(
                        '`msOrderProduct`.`product_id` = `msProduct`.`id`'
                    ));
                    $q->leftJoin('msProductData', 'msProductData', array(
                        '`msOrderProduct`.`product_id` = `msProductData`.`id`'
                    ));


                    $q->leftJoin('msVendor', 'msVendor', array(
                        '`msProductData`.`vendor` = `msVendor`.`id`'
                    ));

                    if ($id_option) {
                        $where[] = [
                            'msOrderProduct.id_1c:=' => $id_option
                        ];
                        $q->leftJoin('modTemplateVarResource', 'modTemplateVarResource', array(
                            '`modTemplateVarResource`.`contentid` = `msProductData`.`id` AND modTemplateVarResource.tmplvarid=1'
                        ));
                        $q->select(array(
                            'msVendor.name',
                            'modTemplateVarResource.value',
                            'msVendor.id',
                            'msProduct.pagetitle',
                            'msOrderProduct.id_1c'
                        ));
                    } else {
                        $q->select(array(
                            'msVendor.name',
                            'msVendor.id',
                            'msProduct.pagetitle',
                            'msOrderProduct.id_1c'
                        ));
                    }

                    $q->where($where);
                    //$id_option

                    $q->prepare();
                    $sql = $q->toSQL();


                    $query = $modx->prepare($sql);
                    $query->execute();

                    $result_products = $query->fetchAll(PDO::FETCH_ASSOC);

                    if (count($result_products) > 0) {

                        foreach ($result_products as $_product) {
                            $vendor = $_product['name'];
                            $product_name = $name_product['pagetitle'];
                            $product_id1c = $name_product['id_1c'];

                            if ($id_option) {
                                //if ($product_id1c == $id_option) {
                                    $findOption = true;
                                //}

                               /* $valueOptions = $_product['value'];
                                $options = $modx->fromJson($valueOptions);

                                foreach ($options as $option) {
                                    if ($option['id_1c'] == $id_option) {
                                        $findOption = true;
                                    }
                                }*/


                            }

                            if (!empty(trim($vendor))) {
                                if (!isset($brands[$vendor]))
                                    $brands[$vendor] = $vendor;
                            }
                        }



                    } else {
                        if (($name_product) || ($id_option))
                            continue;
                    }
                }
            }


        } else {
            if (($name_product) || ($id_option))
                continue;
        }


        if ($id_option) {
            if (!$findOption) {
                continue;
            }
        }

        if (count($brands) > 0) {


            $brandtag = implode(', ', $brands);
        }


        if (
            (($name_product || $id_option) && (count($brands) > 0))
            || (!$name_product && !$id_option)
        ) {


            $extended = $modx->fromJson($res['extended']);
            if (isset($extended['discount']))
                $discount = $extended['discount'];
            else $discount = 0;
            if (isset($extended['delivery'])) {
                $delivery_id = $extended['delivery'];
                switch ($delivery_id) {
                    case '1':
                        $delivery = 'Самовывоз';
                        break;
                    case '2':
                        $delivery = 'Новая почта';
                        break;
                    case '3':
                        $delivery = 'Интайм';
                        break;
                    case '4':
                        $delivery = 'Курьерская доставка';
                        break;
                    case '5':
                        $delivery = 'Курьерская доставка Новой почты';
                        break;

                }
            } else {
                $delivery = '';
            }


            /*echo '<pre>';
            print_r($res);
            echo '';*/
            $street = '';
            if (isset($extended['street'])) $street = $extended['street'];
            $city_name = '';
            if (isset($extended['city_id'])) {

                $q = $modx->newQuery('msNpCities');//Localities
                $q->select('`msNpCities`.`DescriptionRu`');
                $q->where(array('msNpCities.Ref' => $extended['city_id']));
                if ($q->prepare() && $q->stmt->execute()) {
                    $city = $q->stmt->fetchAll(PDO::FETCH_ASSOC);
                    if (isset($city[0])) {

                        $city_name = $city[0]['DescriptionRu'];
                    }
                }
                //if ($city[0]['kyrier']==1) return false;

            }
            $last_order = $res['last_order'];
            $bonuses = $res['bonuses'];

            $user_csv .= $res['fullname'] . "~" . $res['username'] . "~" . $res['mobilephone'] . "~" . $city_name . "~" . $street . "~" . $res['email'] . "~" . $discount . "~" . $res['registr'] . "~" . $totalorder . "~" . $countorder . "~" . $delivery . "~" . $last_order . "~" . $bonuses . "~" . $brandtag . "\r\n";
        }


    }


    //echo $products_csv;//'количество товаров '.count($products_csv);
    $file = 'usersall.csv';
    //echo $_SERVER['DOCUMENT_ROOT'].'/'.$file;
    $handle = fopen($_SERVER['DOCUMENT_ROOT'] . '/' . $file, "w+");

    fwrite($handle, header('Content-Type: text/html; charset=utf8'));
    fwrite($handle, $user_csv);
    fclose($handle);
    /*echo '<pre>$result';
    print_r($result);
    echo '</pre>';*/
} else {

}

?>