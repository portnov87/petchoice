miniShop2.grid.Orders = function (config) {
    config = config || {};

    this.exp = new Ext.grid.RowExpander({
        expandOnDblClick: false
        , tpl: new Ext.Template('<p class="desc">{comment}</p>')
        , renderer: function (v, p, record) {
            return record.data.comment != '' && record.data.comment != null ? '<div class="x-grid3-row-expander">&#160;</div>' : '&#160;';
        }
    });
    this.sm = new Ext.grid.CheckboxSelectionModel();

    Ext.applyIf(config, {
        id: 'minishop2-grid-orders'
        , url: miniShop2.config.connector_url
        , baseParams: {
            action: 'mgr/orders/getlist'
        }
        , fields: miniShop2.config.order_grid_fields
        , autoHeight: true
        , paging: true
        , remoteSort: true
        , bodyCssClass: 'grid-with-buttons'
        , sm: this.sm
        , cls: 'minishop2-grid'
        , plugins: this.exp
        , columns: this.getColumns()

        /*  ,bbar:[
         {
         xtype: 'button'
         ,id: 'minishop2-orders-review'
         ,text: 'Сформировать отчёт'
         ,listeners: {
         click: {fn: this.clearFilter, scope: this}
         }
         }
         ]
         ,paging: true*/
        , tbar: [/*{
         text: '<i class="'+ (MODx.modx23 ? 'icon icon-list' : 'bicon-list') + '"></i> ' + _('ms2_bulk_actions')
         ,menu: [
         {
         text: _('ms2_product_selected_status')
         ,handler: this.changeStatus
         ,scope: this
         },'-',{
         text: _('ms2_menu_remove_multiple')
         ,handler: this.removeSelected
         ,scope: this
         }]
         }
         ,*/
            {
                xtype: 'button'
                , id: 'minishop2-orders-report'
                , text: 'Отчёт'
                , listeners: {
                    click: {fn: this.createReport, scope: this}
                }
            }
            , {
                xtype: 'button'
                , id: 'minishop2-orders-ttn'
                , text: 'ТТН'
                , listeners: {
                    click: {fn: this.createTtn, scope: this}
                }
            }
            , {
                xtype: 'minishop2-combo-status'
                , id: 'tbar-minishop2-combo-status'
                , width: 100
                , emptyText: 'Статус'
                , addall: true
                , listeners: {
                    select: {fn: this.filterByStatus, scope: this}
                }
            }
            // , {
            //     xtype: 'minishop2-combo-paymentstatus'
            //     , id: 'tbar-minishop2-combo-paymentstatus'
            //     , width: 100
            //     , emptyText: 'Статус оплаты'
            //     , addall: true
            //     , listeners: {
            //         select: {fn: this.filterByPaymentStatus, scope: this}
            //     }
            // }
            ,{
                xtype: 'minishop2-combo-delivery'
                , id: 'tbar-minishop2-combo-delivery'
                , width: 150
                , emptyText: 'Доставка'
                , addall: true
                , listeners: {
                    select: {fn: this.filterByDelivery, scope: this}
                }
            }
            , {
                xtype: 'minishop2-combo-payment'
                , id: 'tbar-minishop2-combo-payment'
                //,fieldLabel: 'Оплата'
                , emptyText: 'Оплата'
                , width: 150
                , addall: true
                , listeners: {
                    select: {fn: this.filterByPayment, scope: this}
                }
            }
            , {xtype: 'label', text: 'Доставка'}
            , {
                xtype: 'xdatetime'
                , id: 'tbar-minishop2-date-shipping'
                , width: 230
                // ,allowBlank: true
                , fieldLabel: 'Доставка'

                , allowBlank: false

                , emptyText: 'Доставка'
                , addall: true
                , dateFormat: MODx.config.manager_date_format
                , timeFormat: MODx.config.manager_time_format
                , hideTime: true
                //,addall: true
                , listeners: {
                    change: {
                        fn: function (tf) {
                            this.filterByDateshipping(tf);
                        }, scope: this
                    }
                }
            }
            , {
                xtype: 'xdatetime'
                , id: 'tbar-minishop2-date-shipping-to'
                , width: 230
                // ,allowBlank: true
                //,fieldLabel : 'Дата доставки'
                , allowBlank: false
                //,emptyText: 'Дата доставки'
                , addall: true
                , dateFormat: MODx.config.manager_date_format
                , timeFormat: MODx.config.manager_time_format
                , hideTime: true
                //,addall: true
                , listeners: {
                    change: {
                        fn: function (tf) {
                            this.filterByDateshipping_to(tf);
                        }, scope: this
                    }
                }
            }
            , {xtype: 'label', text: 'Изменение'}
            , {
                xtype: 'xdatetime'
                , id: 'tbar-minishop2-updatedon'
                , width: 230
                , allowBlank: true
                , emptyText: 'Дата обновления'
                , dateFormat: MODx.config.manager_date_format
                , timeFormat: MODx.config.manager_time_format
                , hideTime: true
                , listeners: {
                    change: {
                        fn: function (tf) {
                            this.filterByUpdatedon(tf);
                        }, scope: this
                    }
                }
            }
            , {
                xtype: 'xdatetime'
                , id: 'tbar-minishop2-updatedon-to'
                , width: 230
                , allowBlank: true
                , emptyText: 'Дата обновления'
                , dateFormat: MODx.config.manager_date_format
                , timeFormat: MODx.config.manager_time_format
                , hideTime: true
                , listeners: {
                    change: {
                        fn: function (tf) {
                            this.filterByUpdatedonto(tf);
                        }, scope: this
                    }
                }
            }
            /* ,{
             xtype: 'xdatetime'
             ,id: 'tbar-minishop2-time-shipping'
             ,width: 250
             ,emptyText: 'Время доставки'
             ,hideDate:true
             //,addall: true
             ,listeners: {
             //select: {fn: this.filterByTimeshipping, scope:this}
             change: {fn:function(tf) {
             this.filterByTimeshipping(tf);
             },scope:this}
             }
             }*/

            /*,{xtype: 'spacer',style: 'width:5px;'}*/
            //,'->'
            , {
                xtype: 'textfield'
                , name: 'query'
                , width: 150
                , id: 'minishop2-orders-search'
                , emptyText: _('ms2_search')
                , listeners: {
                    render: {
                        fn: function (tf) {
                            tf.getEl().addKeyListener(Ext.EventObject.ENTER, function () {
                                this.FilterByQuery(tf);
                            }, this);
                        }, scope: this
                    }
                }
            }, {
                xtype: 'button'
                ,
                id: 'minishop2-orders-clear'
                ,
                text: '<i class="' + (MODx.modx23 ? 'icon icon-times' : 'bicon-remove-sign') + '"></i>'/* + _('ms2_search_clear')*/
                ,
                listeners: {
                    click: {fn: this.clearFilter, scope: this}
                }
            }
        ]
        , listeners: {
            rowDblClick: function (grid, rowIndex, e) {
                var row = grid.store.getAt(rowIndex);
                this.updateOrder(grid, e, row);
            }
        }
    });
    miniShop2.grid.Orders.superclass.constructor.call(this, config);
    this.changed = false;
    this._makeTemplates();
};
Ext.extend(miniShop2.grid.Orders, MODx.grid.Grid, {
    windows: {}

    , getMenu: function () {
        var m = [];
        m.push({
            text: _('ms2_menu_update')
            , handler: this.updateOrder
        });
        m.push('-');
        m.push({
            text: ('Печать накладной')
            , handler: this.printOrder
        });
        m.push({
            text: ('Печать ТТН Наклейка')
            , handler: this.printttnmark
        });

        m.push({
            text: ('Печать ТТН А4')
            , handler: this.printttn
        });
        m.push({
            text: ('Выписать товары')
            , handler: this.printOrderKyrier
        });
        m.push('-');

        /*m.push({
         text: _('ms2_menu_remove')
         ,handler: this.removeOrder
         });*/
        this.addContextMenuItem(m);
    }

    , FilterByQuery: function (tf, nv, ov) {
        var s = this.getStore();
        s.baseParams.query = tf.getValue();
        this.getBottomToolbar().changePage(1);
        this.refresh();
    }
    , createTtn: function (btn, e) {
        //if (!this.menu.record) return false;
        //var id = this.menu.record.id;
        var cs = this.getSelectedAsList();
        if (cs === false) return false;


        var printurl = '../core/components/minishop2/custom/order/printttn.php?orders=' + cs;
        window.open(printurl, "_blank")
    }
    , createReport: function (tf, nv, ov) {

        var cs = this.getSelectedAsList();
        if (cs === false) return false;


        MODx.Ajax.request({
            url: miniShop2.config.connector_url
            , params: {
                action: 'mgr/orders/getreport',
                ids: cs
            }
            , listeners: {
                success: {
                    fn: function (r) {

                        // alert(r.results.sales);
                        var w = Ext.getCmp('minishop2-window-order-report');
                        if (w) {
                            w.hide().getEl().remove();
                        }

                        w = MODx.load({
                            xtype: 'minishop2-window-order-report'
                            , id: 'minishop2-window-order-report'
                            , cs: cs
                            , record: r.results
                            , listeners: {
                                success: {
                                    fn: function () {
                                        this.refresh();

                                    }, scope: this
                                }
                                , hide: {
                                    fn: function () {
                                    }
                                }
                            }

                        });
                        w.reset();
                        w.fp.getForm().reset();
                        w.setValues({
                            sales: r.results.sales
                            , amounts: r.results.amounts
                            , cache: r.results.cache
                            , carts: r.results.carts
                            , nal: r.results.nal
                        });

                        w.setValues(r.results);

                        w.setWidth('400');
                        w.setPosition('500');
                        w.show();
                    }, scope: this
                }
            }
        });

        //this.refresh();


    }
    , clearFilter: function (btn, e) {
        var s = this.getStore();
        s.baseParams.query = '';
        Ext.getCmp('minishop2-orders-search').setValue('');
        this.getBottomToolbar().changePage(1);
        this.refresh();
    }

    , filterByDelivery: function (cb) {
        this.getStore().baseParams['delivery'] = cb.value;
        this.getBottomToolbar().changePage(1);
        this.refresh();
    }
    , filterByPayment: function (cb) {
        this.getStore().baseParams['payment'] = cb.value;
        this.getBottomToolbar().changePage(1);
        this.refresh();
    }
    , filterByDateshipping: function (cb) {
        //alert(cb.value);
        //alert(cb);
        this.getStore().baseParams['date_shipping'] = cb.dateValue;//value;
        this.getBottomToolbar().changePage(1);
        this.refresh();
    }
    , filterByDateshipping_to: function (cb) {
        //alert(cb.value);
        //alert(cb);
        this.getStore().baseParams['date_shipping_to'] = cb.dateValue;//value;
        this.getBottomToolbar().changePage(1);
        this.refresh();
    }
    , filterByUpdatedon: function (cb) {
        //alert(cb.value);
        //alert(cb);
        this.getStore().baseParams['updatedon'] = cb.dateValue;//value;
        this.getBottomToolbar().changePage(1);
        this.refresh();
    }
    , filterByUpdatedonto: function (cb) {
        //alert(cb.value);
        //alert(cb);
        this.getStore().baseParams['updatedonto'] = cb.dateValue;//value;
        this.getBottomToolbar().changePage(1);
        this.refresh();
    }

    /* ,filterByTimeshipping: function(cb) {
     this.getStore().baseParams['time_shipping'] = cb.dateValue;
     this.getBottomToolbar().changePage(1);
     this.refresh();
     }*/
    , filterByStatus: function (cb) {
        this.getStore().baseParams['status'] = cb.value;
        this.getBottomToolbar().changePage(1);
        this.refresh();
    }
    , filterByPaymentStatus: function (cb) {
        this.getStore().baseParams['payment_status'] = cb.value;
        this.getBottomToolbar().changePage(1);
        this.refresh();
    }
    , _makeTemplates: function () {
        var userPage = MODx.action ? MODx.action['security/user/update'] : 'security/user/update';
        this.tplCustomer = new Ext.XTemplate(''
            + '<tpl for="."><div class="order-title-column {cls}">'
            + '<h3 class="main-column"><span class="title">' + _('ms2_order') + ' #{num}</span></h3>'
            + '<tpl if="actions">'
            + '<ul class="actions">'
            + '<tpl for="actions">'
            + '<li><a href="#" class="controlBtn {className}">{text}</a></li>'
            + '</tpl>'
            + '</ul>'
            + '</tpl>'
            + '</div></tpl>', {
            compiled: true
        });
    }

    , _renderCustomer: function (v, md, rec) {
        return this.tplCustomer.apply(rec.data);
    }

    , _renderCost: function (v, md, rec) {
        return rec.data.type && rec.data.type == 1
            ? '-' + v
            : v;
    }

    , onClick: function (e) {
        var t = e.getTarget();
        var elm = t.className.split(' ')[0];
        if (elm == 'controlBtn') {
            var action = t.className.split(' ')[1];
            this.menu.record = this.getSelectionModel().getSelected().data;
            switch (action) {
                case 'update':
                    this.updateOrder(this, e);
                    break;
                /*case 'delete':
                 this.removeOrder(this,e);
                 break;*/
            }
        }
        this.processEvent('click', e);
    }


    , getColumns: function () {
        var all = {
            id: {width: 35}
            , customer: {width: 100, sortable: true, renderer: miniShop2.utils.userLink}
            , num: {width: 100, sortable: true, renderer: {fn: this._renderCustomer, scope: this}, id: 'main'}
            , receiver: {width: 100, sortable: true}
            , createdon: {width: 75, sortable: true, renderer: miniShop2.utils.formatDate}
            , date_shipping: {
                header: 'Дата доставки', width: 75, sortable: true, renderer: function (string) {

                    if (string && string != '0000-00-00' && string != '1970-01-01' && string != '' && string != '00:00:00' && string != 0) {
                        var date = /^[0-9]+$/.test(string)
                            ? new Date(string * 1000)
                            : new Date(string.replace(/(\d+)-(\d+)-(\d+)/, '$2/$3/$1'));

                        return date.strftime(MODx.config.ms2_date_format);
                    }
                    else {
                        return '&nbsp;';
                    }
                }
            }

            , time_shipping: {header: 'Время доставки', width: 75, sortable: true}
            , updatedon: {width: 75, sortable: true, renderer: miniShop2.utils.formatDate}

            /*,time_shipping_from: {width: 75, sortable: true, renderer: function(string) {

             if (string && string != '1970-01-01 00:00:00'&& string != '-1-11-30 00:00:00' && string != '00:00:00' && string != 0) {
             var date = /^[0-9]+$/.test(string)
             ? new Date(string * 1000)
             : new Date(string.replace(/(\d+)-(\d+)-(\d+)/, '$2/$3/$1'));

             return date.strftime(MODx.config.ms2_date_format);
             }
             else {
             return '&nbsp;';
             }
             }}
             ,time_shipping_to: {width: 75, sortable: true, renderer: function(string) {

             if (string && string != '1970-01-01 00:00:00'&& string != '-1-11-30 00:00:00' && string != '00:00:00' && string != 0) {
             var date = /^[0-9]+$/.test(string)
             ? new Date(string * 1000)
             : new Date(string.replace(/(\d+)-(\d+)-(\d+)/, '$2/$3/$1'));

             return date.strftime(MODx.config.ms2_date_format);
             }
             else {
             return '&nbsp;';
             }
             }}*/
            //,time_shipping_from: {width: 75, sortable: true, renderer: miniShop2.utils.formatDate}
            , cost: {width: 75, sortable: true, renderer: this._renderCost}
            , cart_cost: {width: 75, sortable: true}
            //,delivery_cost: {width: 75, sortable: true}
            //,weight: {width: 50, sortable: true}
            , status: {width: 75, sortable: true}
            , payment_status: {width: 75, sortable: true}
            , delivery: {width: 75, sortable: true}
            /* , delivery: {
                 header: 'оставка', width: 75, sortable: true,
                 renderer: function (string) {

                 }
             }
             date_update_status
             status_np*/


            , payment: {width: 75, sortable: true}
            //,address: {width: 50, sortable: true}
            , context: {width: 50, sortable: true}
        };

        var columns = [this.sm, this.exp];
        for (var i = 0; i < miniShop2.config.order_grid_fields.length; i++) {
            var field = miniShop2.config.order_grid_fields[i];
            /* if (field=='time_shipping_from')
             {
             Ext.applyIf(all[field], {
             header: _('ms2_' + field)
             ,dataIndex: field
             });
             columns.push(all[field]);

             }else */
            if (all[field]) {
                Ext.applyIf(all[field], {
                    header: _('ms2_' + field)
                    , dataIndex: field
                });
                columns.push(all[field]);
            }
        }

        return columns;
    }
    , createreport: function (btn, e, row) {
        if (typeof(row) != 'undefined') {
            this.menu.record = row.data;
        }
        var id = this.menu.record.id;


        /*
         MODx.Ajax.request({
         url: miniShop2.config.connector_url
         ,params: {
         action: 'mgr/orders/report'
         ,id: id
         }
         ,listeners: {
         success: {fn:function(r) {
         var w = Ext.getCmp('minishop2-window-order-report');
         if (w) {w.hide().getEl().remove();}
         alert('dd');
         w = MODx.load({
         xtype: 'minishop2-window-order-report'
         ,id: 'minishop2-window-order-report'
         ,record:r.object
         ,listeners: {
         success: {fn:function() {this.refresh();},scope:this}
         ,hide: {fn: function() {

         }}
         }
         });
         w.fp.getForm().reset();
         w.fp.getForm().setValues(r.object);
         w.show(e.target,function() {w.setPosition(null,100)},this);

         },scope:this}
         }
         });*/


    }
    , updateOrder: function (btn, e, row) {
        if (typeof(row) != 'undefined') {
            this.menu.record = row.data;
        }
        var id = this.menu.record.id;

        MODx.Ajax.request({
            url: miniShop2.config.connector_url
            , params: {
                action: 'mgr/orders/get'
                , id: id
            }
            , listeners: {
                success: {
                    fn: function (r) {
                        var w = Ext.getCmp('minishop2-window-order-update');
                        if (w) {
                            w.hide().getEl().remove();
                        }

                        w = MODx.load({
                            xtype: 'minishop2-window-order-update'
                            , id: 'minishop2-window-order-update'
                            , record: r.object
                            , listeners: {
                                success: {
                                    fn: function () {
                                        this.refresh();
                                    }, scope: this
                                }
                                , hide: {
                                    fn: function () {
                                        if (miniShop2.grid.Orders.changed === true) {
                                            Ext.getCmp('minishop2-grid-orders').getStore().reload();
                                            miniShop2.grid.Orders.changed = false;
                                        }
                                        this.getEl().remove();
                                    }
                                }
                            }
                        });
                        w.fp.getForm().reset();
                        w.fp.getForm().setValues(r.object);
                        w.show(e.target, function () {
                            w.setPosition(null, 100)
                        }, this);
                        /* Need to refresh grids with goods and logs */
                    }, scope: this
                }
            }
        });
    }

    , removeOrder: function (btn, e) {
        if (!this.menu.record) return;
        return;
        /*
         MODx.msg.confirm({
         title: _('ms2_menu_remove') + ' ' + _('ms2_order') + ' #' + this.menu.record.num
         ,text: _('ms2_menu_remove_confirm')
         ,url: miniShop2.config.connector_url
         ,params: {
         action: 'mgr/orders/remove'
         ,id: this.menu.record.id
         }
         ,listeners: {
         success: {fn:function(r) { this.refresh();}, scope:this}
         }
         });*/
    }
    , printOrderKyrier: function (btn, e) {
        if (!this.menu.record) return false;
        var id = this.menu.record.id;
        var printurl = '../core/components/minishop2/custom/order/print.php?kurier=1&order_id=' + id;
        window.open(printurl, "_blank")
    }

    , printttn: function (btn, e) {
        if (!this.menu.record) return false;
        var id = this.menu.record.id;
        var printurl = '../core/components/minishop2/custom/order/printttn_np.php?order_id=' + id;
        window.open(printurl, "_blank")
    }
    , printttnmark: function (btn, e) {
        if (!this.menu.record) return false;
        var id = this.menu.record.id;
        var printurl = '../core/components/minishop2/custom/order/printttn_np_mark.php?order_id=' + id;
        window.open(printurl, "_blank")
    }
    , printOrder: function (btn, e) {
        if (!this.menu.record) return false;
        var id = this.menu.record.id;
        var printurl = '../core/components/minishop2/custom/order/print.php?order_id=' + id;
        window.open(printurl, "_blank")
    }
    , removeSelected: function (btn, e) {
        var cs = this.getSelectedAsList();
        if (cs === false) return false;

        MODx.msg.confirm({
            title: _('ms2_menu_remove_multiple')
            , text: _('ms2_menu_remove_multiple_confirm')
            , url: miniShop2.config.connector_url
            , params: {
                action: 'mgr/orders/remove_multiple'
                , ids: cs
            }
            , listeners: {
                success: {
                    fn: function (r) {
                        this.getSelectionModel().clearSelections(true);
                        this.refresh();
                    }, scope: this
                }
            }
        });
        return true;
    }
});
Ext.reg('minishop2-grid-orders', miniShop2.grid.Orders);


miniShop2.window.UpdateOrder = function (config) {
    config = config || {};

    this.ident = config.ident || 'meuitem' + Ext.id();
    Ext.applyIf(config, {
        title: _('ms2_menu_update')
        , id: this.ident
        , width: '70%'//750
        , autoHeight: true
        , labelAlign: 'top'
        , url: miniShop2.config.connector_url
        , action: 'mgr/orders/update'
        , fields: {
            xtype: 'modx-tabs'
            //,border: true
            , activeTab: config.activeTab || 0
            , bodyStyle: {background: 'transparent'}
            , deferredRender: false
            , autoHeight: true
            , stateful: true
            , stateId: 'minishop2-window-order-update'
            , stateEvents: ['tabchange']
            , getState: function () {
                return {activeTab: this.items.indexOf(this.getActiveTab())};
            }
            , items: this.getTabs(config)
        }
        , keys: [{
            key: Ext.EventObject.ENTER, shift: true, fn: function () {
                this.submit()
            }, scope: this
        }]
    });
    miniShop2.window.UpdateOrder.superclass.constructor.call(this, config);

};
Ext.extend(miniShop2.window.UpdateOrder, MODx.Window, {

    getOrderId: function (config) {
        order_id = Ext.get('order_id').getValue();
        alert('order' + order_id);
        return order_id;
    },
    getTabs: function (config) {
        var tabs = [{
            title: _('ms2_order')
            , hideMode: 'offsets'
            , bodyStyle: 'padding:5px 0;'
            , defaults: {msgTarget: 'under', border: false}
            , items: this.getOrderFields(config)
        }, {
            xtype: 'minishop2-grid-order-products'
            , title: _('ms2_order_products')
            , order_id: config.record.id
        }];

        var address = this.getAddressFields(config);
        if (address.length > 0) {
            tabs.push({
                layout: 'form'
                , title: _('ms2_address')
                , hideMode: 'offsets'
                , bodyStyle: 'padding:5px 0;'
                , defaults: {msgTarget: 'under', border: false}
                , items: address
            });
        }
        var sms = this.getSmsFields(config);
        tabs.push({
            layout: 'form'
            , title: 'Отправка СМС'
            , hideMode: 'offsets'
            , bodyStyle: 'padding:5px 0;'
            , defaults: {msgTarget: 'under', border: false}
            , items: sms
        });

        tabs.push({
            xtype: 'minishop2-grid-order-logs'
            , title: 'История заказа'
            , order_id: config.record.id
        });

        /*tabs.push({
         xtype: 'minishop2-grid-order-np'
         , title: 'Новая почта'
         , order_id: config.record.id
         });*/
        var nova_poshta = this.getNovaposhtaFields(config);
        tabs.push({
            layout: 'form'
            , title: 'Новая почта'
            , hideMode: 'offsets'
            , bodyStyle: 'padding:5px 0;'
            , defaults: {msgTarget: 'under', border: false}
            , items: nova_poshta
        });

        return tabs;
    }

    , getOrderFields: function (config) {
        return [{
            xtype: 'hidden'
            , name: 'id'
            , id: 'order_id'
        }, {
            layout: 'column'
            , defaults: {msgTarget: 'under', border: false}
            , style: 'padding:15px 5px;text-align:center;'
            , items: [{
                columnWidth: .3
                ,
                layout: 'form'
                ,
                items: [{
                    xtype: 'displayfield',
                    name: 'fullname',
                    fieldLabel: _('ms2_user'),
                    renderer: miniShop2.utils.userLink,
                    anchor: '100%',
                    style: 'font-size:1.1em;'
                }]
            }, {
                columnWidth: .3
                ,
                layout: 'form'
                ,
                items: [{
                    xtype: 'textfield',
                    id: 'bonuses',
                    name: 'bonuses',
                    fieldLabel: 'Бонусы',//_('ms2_order_cost'),
                    anchor: '100%',
                    style: 'font-size:1.1em;'
                }]
            }, /* {
             columnWidth: .25
             ,
             layout: 'form'
             ,
             items: [
             {
             xtype: 'xcheckbox',
             fieldLabel: 'Применить бонусы',
             name: 'with_bonuses',
             anchor: '99%',
             id: 'minishop2-city-with_bonuses'
             }
             ]
             },*/{
                columnWidth: .3
                ,
                layout: 'form'
                ,
                items: [{
                    xtype: 'displayfield',
                    name: 'promocode',
                    fieldLabel: 'Промокод',
                    anchor: '100%',
                    style: 'font-size:1.1em;'
                }]
            }]
        }, {
            xtype: 'fieldset'
            , layout: 'column'
            , style: 'padding:15px 5px;text-align:center;'
            , defaults: {msgTarget: 'under', border: false}
            , items: [{
                columnWidth: .33
                , layout: 'form'
                , items: [
                    {
                        xtype: 'displayfield',
                        name: 'num',
                        fieldLabel: _('ms2_num'),
                        anchor: '100%',
                        style: 'font-size:1.1em;'
                    }
                    , {
                        xtype: 'displayfield',
                        name: 'cart_cost_new',
                        id: 'cart_cost_new',
                        fieldLabel: _('ms2_cart_cost'),
                        anchor: '100%'
                    }
                    , {
                        xtype: 'hidden',
                        name: 'cost',
                        id: 'cost',
                        fieldLabel: _('ms2_cart_cost'),
                        anchor: '100%'
                    }
                ]
            }, {
                columnWidth: .33
                , layout: 'form'
                , items: [
                    {xtype: 'displayfield', name: 'createdon', fieldLabel: _('ms2_createdon'), anchor: '100%'}
                    , {
                        xtype: 'minishop2-combo-costdelivery',
                        name: 'delivery_cost',
                        fieldLabel: _('ms2_delivery_cost'),
                        anchor: '80%'
                    }
                    //,{xtype: 'textfield', name: 'delivery_cost', fieldLabel: _('ms2_delivery_cost'), anchor: '100%'}
                ]
            }, {
                columnWidth: .33
                , layout: 'form'
                , items: [
                    {xtype: 'displayfield', name: 'discount_customer', fieldLabel: 'Дисконт клиента', anchor: '100%'}
                    , {xtype: 'displayfield', name: 'bonuses_user', fieldLabel: 'Бонусов у клиента', anchor: '100%'}
                    , {xtype: 'displayfield', name: 'skidka', fieldLabel: 'Скидка', anchor: '100%'}//weight_('ms2_weight')
                    //,{xtype: 'displayfield', name: 'discount', fieldLabel: 'Дисконт(Скидка)', anchor: '100%'}//weight_('ms2_weight')

                ]
            }]
        }, {
            layout: 'column'
            , defaults: {msgTarget: 'under', border: false}
            , anchor: '100%'
            , items: [{
                columnWidth: .48
                , layout: 'form'
                , items: [
                    {
                        xtype: 'minishop2-combo-status',
                        name: 'status',
                        fieldLabel: _('ms2_status'),
                        anchor: '100%',
                        order_id: config.record.id
                    }
                    , {
                        xtype: 'minishop2-combo-paymentstatus',
                        name: 'payment_status',
                        fieldLabel: 'Статус оплаты',
                        anchor: '100%',
                        order_id: config.record.id
                    }
                    , {
                        xtype: 'minishop2-combo-delivery',
                        name: 'delivery',
                        fieldLabel: _('ms2_delivery'),
                        anchor: '100%'
                    }
                    , {
                        xtype: 'minishop2-combo-pickup',
                        name: 'pickup_id',
                        fieldLabel: 'Магазин для самовывоза',//_('ms2_delivery'),
                        anchor: '100%'
                    }
                    , {
                        xtype: 'minishop2-combo-payment',
                        name: 'payment',
                        fieldLabel: _('ms2_payment'),
                        anchor: '100%',
                        delivery_id: config.record.delivery
                    }
                ]
            },

                {
                    columnWidth: .5
                    , layout: 'form'
                    , items: [
                        {xtype: 'textarea', name: 'comment', fieldLabel: _('ms2_comment'), anchor: '100%', height: 82},
                        {xtype: 'textarea', name: 'addr_comment', fieldLabel: 'Комментарий клиента', anchor: '100%', height: 82},
                            /*{
                                xtype: 'displayfield',
                                name: 'addr_comment',
                                fieldLabel: 'Комментарий клиента',//_('ms2_comment'),
                                anchor: '98%',
                                style: 'min-height: 50px;border:1px solid #efefef;width:95%;'
                            },*/

                        {
                            columnWidth: .100
                            , layout: 'column'
                            , fieldLabel: 'Дата доставки'
                            , items: [
                                {

                                    columnWidth: .33
                                    , layout: 'form'
                                    , items: [
                                        {
                                            xtype: 'xdatetime'
                                            , name: 'date_shipping'
                                            , allowBlank: true
                                            , dateFormat: MODx.config.manager_date_format
                                            , timeFormat: ''//MODx.config.manager_time_format
                                            // ,startDay: parseInt(MODx.config.manager_week_start)
                                            , dateWidth: 200
                                            , hideTime: true
                                            , timePosition: 'below'
                                            , timeWidth: 200
                                        }
                                    ]
                                },
                                {

                                    columnWidth: .33
                                    , layout: 'form'
                                    , items: [
                                        {
                                            xtype: 'xdatetime',
                                            name: 'time_shipping_from',
                                            timeIncrement: 30,
                                            style: 'width:100%;',
                                            timeWidth: 200,
                                            allowBlank: true,
                                            hideTime: false,
                                            anchor: '100%',
                                            timeFormat: 'H:i',
                                            dateFormat: '',
                                            hideDate: true
                                        }//G:i
                                    ]
                                },
                                {

                                    columnWidth: .33
                                    , layout: 'form'
                                    , items: [
                                        {
                                            xtype: 'xdatetime',
                                            name: 'time_shipping_to',
                                            timeIncrement: 30,
                                            style: 'width:100%;',
                                            hideTime: false,
                                            allowBlank: true,
                                            timeWidth: 200,
                                            anchor: '100%',
                                            timeFormat: 'H:i',
                                            dateFormat: '',
                                            hideDate: true
                                        }//g:i A
                                    ]
                                }
                            ]
                        }

                        /*{
                         layout: 'column'
                         ,defaults: {msgTarget: 'under',border: false}
                         ,style: 'padding:15px 5px;'
                         ,fieldLabel: 'Дата доставки'
                         ,anchor: '100%'
                         ,items: [
                         {
                         columnWidth: .90
                         ,layout: 'column'
                         //,style: 'w;'
                         ,items: [
                         {xtype: 'xdatetime', name: 'date_shipping', style: 'width:90%;',hideTime:true, anchor: '100%',dateFormat: MODx.config.manager_date_format,startDay: parseInt(MODx.config.manager_week_start)}
                         ]
                         },
                         {
                         layout: 'column'
                         ,defaults: {msgTarget: 'under',border: false}
                         ,style: 'padding:15px 5px;'
                         ,fieldLabel: 'Дата доставки'
                         ,anchor: '100%'
                         ,items: [
                         {
                         columnWidth: .48
                         ,layout: 'column'
                         ,items: [
                         {xtype: 'xdatetime', name: 'time_shipping_from', style: 'width:90%;',hideTime:false, anchor: '100%',timeFormat: MODx.config.manager_time_format,dateFormat:'',hideDate:true}
                         ]
                         },
                         {
                         columnWidth: .48
                         ,layout: 'column'
                         ,items: [
                         {xtype: 'xdatetime', name: 'time_shipping_to', hideTime:false,anchor: '100%',timeFormat: MODx.config.manager_time_format,dateFormat:'',hideDate:true}
                         ]
                         }
                         ]
                         }

                         ]
                         }  */
                    ]
                }]
        }
        ];
    }
    /*,indexClear: function() {
        var el = this.getEl();
        el.mask(_('loading'),'x-mask-loading');
        MODx.Ajax.request({
            url: mSearch2.config.connector_url
            ,params: {
                action: 'mgr/index/remove'
            }
            ,listeners: {
                success: {fn:function(r) {
                    el.unmask();
                    this.getStat();
                },scope: this}
                ,failure: {fn:function(r) {
                    el.unmask();
                }, scope:this}
            }
        })
    }*/
    , printnpttn: function () {
        var order_id = Ext.getCmp('order_id').getValue();
        var printurl = '../core/components/minishop2/custom/order/printttn_np.php?order_id=' + order_id;
        window.open(printurl, "_blank")
    }
    , printnpttnmarker: function (btn, e) {
        var order_id = Ext.getCmp('order_id').getValue();
        var printurl = '../core/components/minishop2/custom/order/printttn_np_mark.php?order_id=' + order_id;
        window.open(printurl, "_blank")

        /*if (!this.menu.record) return false;
        var id = this.menu.record.id;
        var printurl = '../core/components/minishop2/custom/order/printttn_np_mark.php?order_id=' + id;
        window.open(printurl, "_blank")*/
    }
    , submitnovaposhta: function () {

        var order_id = Ext.getCmp('order_id').getValue();
        var np_address_building = Ext.getCmp('np_address_building').getValue();
        var np_address_flat = Ext.getCmp('np_address_flat').getValue();

        var np_address_street = Ext.getCmp('minishop2-combo-np_street').getValue();
        var address_street = Ext.getCmp('np_address_street').getValue();

        var np_phone = Ext.getCmp('np_phone').getValue();
        var np_lastname = Ext.getCmp('np_lastname').getValue();
        var np_firstname = Ext.getCmp('np_firstname').getValue();

        var np_receiving = Ext.getCmp('minishop2-combo-np_receiving-combo').getValue();
        var np_city = Ext.getCmp('minishop2-combo-np_city').getValue();

        var np_warehouse = Ext.getCmp('minishop2-combo-np_warehouse').getValue();
        var np_whopay = Ext.getCmp('minishop2-combo-np_whopay').getValue();

        var np_method_payment = Ext.getCmp('minishop2-combo-np_method_payment').getValue();
        var np_summa = Ext.getCmp('np_summa').getValue();
        var np_date_shipping = Ext.getCmp('np_date_shipping').getValue();

        var np_weight = Ext.getCmp('np_weight').getValue();
        var np_volume = Ext.getCmp('np_volume').getValue();
        var np_countmesta = Ext.getCmp('np_countmesta').getValue();
//Ext.getBody()//Ext.getCmp('minishop2-window-order-update')
        var myMask = new Ext.LoadMask('minishop2-window-order-update', {
            msg: "Формируем накладную",
            style: 'z-index:999999!important',
            target: Ext.getCmp('minishop2-window-order-update'),
            onBeforeLoad: function () {
                // if (!this.disabled) {
                this.el.mask(this.msg, this.msgCls, false);
                //}
            }
        });
        myMask.show();
        MODx.Ajax.request({
            url: miniShop2.config.connector_url
            , params: {
                action: 'mgr/orders/np/submit'
                , order_id: order_id
                , np_address_street: np_address_street
                , address_street: address_street
                , np_address_flat: np_address_flat
                , np_address_building: np_address_building
                , np_phone: np_phone
                , np_lastname: np_lastname
                , np_firstname: np_firstname
                , np_receiving: np_receiving
                , np_city: np_city
                , np_warehouse: np_warehouse
                , np_whopay: np_whopay
                , np_method_payment: np_method_payment
                , np_summa: np_summa
                , np_date_shipping: np_date_shipping
                , np_weight: np_weight
                , np_volume: np_volume
                , np_countmesta: np_countmesta
            }
            , listeners: {
                success: {
                    fn: function (r) {

                        myMask.hide();

                        if (r.results.errors != '') {
                            alert(r.results.errors);
                        }
                        else {
                            //Ext.getCmp('buttonprintttn').setStyle({display:'block'});

                            Ext.getCmp('number_ttn').setValue(r.results.results.number_ttn);
                            Ext.getCmp('cost_shipping_np').setValue(r.results.results.cost_shipping_np);
                            Ext.getCmp('date_shipping_np').setValue(r.results.results.date_shipping_np);
                            Ext.getCmp('date_create_ttn').setValue(r.results.results.date_create_ttn);
                        }

                    }, scope: this
                }
            }
        });
    }
    , submitmessage: function () {
        var phone_sms = Ext.getCmp('phone_sms').getValue();
        var order_id = Ext.getCmp('order_id').getValue();
        var submitsmsform = Ext.getCmp('submitsmsform').getValue();
        var submitviberform = Ext.getCmp('submitviberform').getValue();
        //alert(submitsmsform);

        MODx.Ajax.request({
            url: miniShop2.config.connector_url
            , params: {
                action: 'mgr/orders/sms/submit'
                , text: submitsmsform
                , text_viber: submitviberform
                , phone_sms: phone_sms
                , order_id: order_id
            }
            , listeners: {
                success: {
                    fn: function (r) {
                        alert(r.results.result);


                    }, scope: this
                }
            }
        });
    }
    , selecttemplate: function (combo, row, e) {

        var id = row.id;
        var option = row.name;
        //alert(id+' '+option);
        /*
         var id_option=row.data.id_option;

         combo.reset();*/

        cart_cost = Ext.getCmp('cost').getValue();
        //alert(cart_cost);
        MODx.Ajax.request({
            url: miniShop2.config.connector_url
            , params: {
                action: 'mgr/orders/sms/gettemplatesmessage'
                , id: id//id
                , order_id: Ext.getCmp('order_id').getValue()
                , cart_cost: cart_cost
                //,option: option
            }
            , listeners: {
                success: {
                    fn: function (r) {

                        Ext.getCmp('submitsmsform').setValue(r.results.text_sms);
                        Ext.getCmp('submitviberform').setValue(r.results.text);
                        //Ext.getCmp('id_notification').setValue(r.results.id_notification);


                    }, scope: this
                }
            }
        });

    }
    /*, selectstock: function (combo, row, e) {

        var id = row.id;
        var option = row.name;

        //cart_cost = Ext.getCmp('cost').getValue();
        //alert(cart_cost);
        MODx.Ajax.request({
            url: miniShop2.config.connector_url
            , params: {
                action: 'mgr/orders/getstocks'
                , id: id
            }
            , listeners: {
                success: {
                    fn: function (r) {

                      //  Ext.getCmp('submitsmsform').setValue(r.results.text);

                    }, scope: this
                }
            }
        });

    }*/


    , selectreceiving: function (combo, row, e) {

        var id = row.id;
        var option = row.name;
        cart_cost = Ext.getCmp('cost').getValue();
        //alert(cart_cost);
        MODx.Ajax.request({
            url: miniShop2.config.connector_url
            , params: {
                action: 'mgr/orders/np/getreceiving'
                , id: id
                , cart_cost: cart_cost
            }
            , listeners: {
                success: {
                    fn: function (r) {

                        //Ext.getCmp('submitsmsform').setValue(r.results.text);

                    }, scope: this
                }
            }
        });

    }

    , getNovaposhtaFields: function (config) {

        var fields = [], tmp = [];


        tmp.push(
            {
                columnWidth: .100,
                layout: 'column'
                , defaults: {msgTarget: 'under', border: false}
                //, anchor: '100%'
                , items: [
                    {
                        xtype: 'fieldset'
                        , columnWidth: .48

                        , layout: 'form'
                        , items: [
                            {
                                columnWidth: .100
                                , layout: 'column'
                                //, fieldLabel: 'Получатель'
                                , items: [
                                    {
                                        columnWidth: .5
                                        , layout: 'form'
                                        , items: [
                                            {
                                                xtype: 'textfield', name: 'np_firstname', fieldLabel: 'Имя',
                                                anchor: '100%',
                                                id: 'np_firstname',
                                                //style: 'min-height: 20px;padding: 5px;border: 1px solid;border-radius: 2px;border-color: #CCCCCC!important;',
                                            },
                                        ]
                                    },

                                    {
                                        columnWidth: .5
                                        , layout: 'form'
                                        , items: [
                                            {
                                                xtype: 'textfield', name: 'np_lastname', fieldLabel: 'Фамилия',
                                                anchor: '100%',
                                                id: 'np_lastname',
                                                //style: 'min-height: 20px;padding: 5px;border: 1px solid;border-radius: 2px;border-color: #CCCCCC!important;',

                                            },
                                        ]
                                    },
                                ]
                            },
                            {
                                columnWidth: .100
                                , layout: 'column'
                                //, fieldLabel: 'Получатель'
                                , items: [
                                    {

                                        columnWidth: .5
                                        , layout: 'form'
                                        , items: [
                                            {
                                                xtype: 'textfield', name: 'np_phone', fieldLabel: 'Телефон',
                                                anchor: '100%',
                                                id: 'np_phone',
                                                //style: 'min-height: 20px;padding: 5px;border: 1px solid;border-radius: 2px;border-color: #CCCCCC!important;',

                                            }
                                        ]


                                    },
                                    {

                                        columnWidth: .5
                                        , layout: 'form'
                                        , items: [
                                            {
                                                xtype: 'textfield', name: 'np_address_building', fieldLabel: 'Дом',
                                                anchor: '100%',
                                                id: 'np_address_building',
                                                // style: 'min-height: 20px;padding: 5px;border: 1px solid;border-radius: 2px;border-color: #CCCCCC!important;',

                                            }
                                        ]


                                    },
                                ]
                            },
                            {
                                columnWidth: .100
                                , layout: 'column'
                                //, fieldLabel: 'Получатель'
                                , items: [
                                    {
                                        columnWidth: .5
                                        , layout: 'form'
                                        , items: [
                                            {
                                                xtype: 'textfield', name: 'np_address_street', fieldLabel: 'Улица',
                                                anchor: '100%',
                                                id: 'np_address_street',
                                            }


                                        ]
                                    },
                                    {
                                        columnWidth: .5
                                        , layout: 'form'
                                        , items: [
                                            {
                                                xtype: 'textfield', name: 'np_address_flat', fieldLabel: 'Квартира',
                                                anchor: '100%',
                                                id: 'np_address_flat',
                                            }

                                        ]
                                    },
                                ]
                            },


                            //           ]
                            // },

                        ]
                    },

                    {

                        xtype: 'fieldset'
                        , columnWidth: .48
                        , layout: 'form'
                        , items: [


                            {
                                columnWidth: .100
                                , layout: 'column'
                                //, fieldLabel: 'Новая почта'
                                , items: [

                                    {
                                        columnWidth: .5
                                        , layout: 'form'
                                        , items: [
                                            {
                                                xtype: 'minishop2-combo-np_receiving',
                                                fieldLabel: 'Способ получения',
                                                name: 'np_receiving',
                                                anchor: '100%',
                                                id: 'minishop2-combo-np_receiving-combo',
                                            },
                                        ]
                                    },
                                    {
                                        columnWidth: .5
                                        , layout: 'form'
                                        , items: [

                                            {
                                                xtype: 'minishop2-combo-np_city'//minishop2-combo-product'
                                                , allowBlank: true
                                                , width: '100%'
                                                , anchor: '100%'
                                                , name: 'np_city'
                                                , id: 'minishop2-combo-np_city'
                                                , order_id: config.record.id//this.getOrderId
                                                , listeners: {
                                                    'select': {
                                                        fn: function (data) {
                                                            order_id = Ext.getCmp('order_id').getValue();
                                                            MODx.Ajax.request({
                                                                url: miniShop2.config.connector_url
                                                                , params: {
                                                                    action: 'mgr/orders/np/savecity'
                                                                    , order_id: order_id
                                                                    , city_id: data.value
                                                                }

                                                            });

                                                        }, scope: this
                                                    }
                                                }
                                            }


                                        ]
                                    }
                                ]
                            },
                            {
                                columnWidth: .100
                                , layout: 'column'
                                //, fieldLabel: 'Новая почта'
                                , items: [

                                    {
                                        columnWidth: .5
                                        , layout: 'form'
                                        , items: [
                                            {
                                                xtype: 'minishop2-combo-np_street'//minishop2-combo-product'
                                                , allowBlank: true
                                                , name: 'np_street'
                                                , id: 'minishop2-combo-np_street'
                                                , width: '100%'
                                                , anchor: '100%'
                                                , order_id: config.record.id
                                            }

                                        ]
                                    },

                                    {
                                        columnWidth: .5
                                        , layout: 'form'
                                        , items: [

                                            {
                                                xtype: 'minishop2-combo-np_warehouse'
                                                , allowBlank: true
                                                , width: '100%'
                                                , anchor: '100%'
                                                , name: 'np_warehouse'
                                                , id: 'minishop2-combo-np_warehouse'
                                                , order_id: config.record.id

                                            },
                                        ]
                                    },
                                ]
                            },
                            {
                                columnWidth: .100
                                , layout: 'column'
                                //, fieldLabel: 'Новая почта'
                                , items: [

                                    {
                                        columnWidth: .5
                                        , layout: 'form'
                                        , items: [
                                            {
                                                xtype: 'minishop2-combo-np_whopay',
                                                fieldLabel: 'Плательщик',
                                                name: 'np_whopay', anchor: '100%',
                                                id: 'minishop2-combo-np_whopay',
                                            },
                                        ]
                                    },

                                    {
                                        columnWidth: .5
                                        , layout: 'form'
                                        , items: [
                                            {
                                                xtype: 'minishop2-combo-np_method_payment',
                                                fieldLabel: 'Оплата',
                                                name: 'np_method_payment', anchor: '100%',
                                                id: 'minishop2-combo-np_method_payment',
                                            },
                                        ]
                                    },
                                ]
                            },

                            {
                                columnWidth: .100
                                , layout: 'column'
                                //, fieldLabel: 'Новая почта'
                                , items: [

                                    {
                                        columnWidth: .5
                                        , layout: 'form'
                                        , items: [
                                            {
                                                xtype: 'textfield',
                                                name: 'np_summa',
                                                fieldLabel: 'Денежный перевод',
                                                anchor: '100%',
                                                id: 'np_summa',
                                            },
                                        ]
                                    },
                                    {
                                        columnWidth: .5
                                        , layout: 'form'
                                        , items: [
                                            {
                                                xtype: 'xdatetime',
                                                name: 'np_date_shipping',
                                                id: 'np_date_shipping',
                                                fieldLabel: 'Дата отправки',
                                                // timeIncrement: 30,
                                                style: 'width:100%;',
                                                width: '100%',
                                                allowBlank: true,
                                                anchor: '100%',
                                                dateFormat: MODx.config.manager_date_format
                                                , timeFormat: MODx.config.manager_time_format
                                                , hideTime: true
                                            }
                                        ]
                                    },
                                ]
                            },


                        ]
                    },


                ]
            }
        );

        tmp.push(
            {
                xtype: 'fieldset'
                , layout: 'column'
                , style: 'padding:15px 5px;text-align:center;'
                , defaults: {msgTarget: 'under', border: false}
                //columnWidth: .100,
                //layout: 'column'
                //, defaults: {msgTarget: 'under', border: false}
                //, anchor: '100%'
                , items: [

                    {
                        columnWidth: .3
                        , layout: 'form'
                        , items: [
                            {
                                xtype: 'textfield', name: 'np_weight', fieldLabel: 'Вес, кг', anchor: '90%',
                                id: 'np_weight',
                                //style: 'min-height: 20px;padding: 5px;border: 1px solid;border-radius: 2px;border-color: #CCCCCC!important;',

                            }
                        ]
                    }

                    , {
                        columnWidth: .3
                        , layout: 'form'
                        , items: [
                            {
                                xtype: 'textfield', name: 'np_volume', fieldLabel: 'Объем, м3', anchor: '90%',
                                id: 'np_volume',
                                //style: 'min-height: 20px;padding: 5px;border: 1px solid;border-radius: 2px;border-color: #CCCCCC!important;',

                            }
                        ]
                    }
                    , {
                        columnWidth: .3
                        , layout: 'form'
                        , items: [
                            {
                                xtype: 'textfield', name: 'np_countmesta', fieldLabel: 'Количество мест', anchor: '90%',
                                id: 'np_countmesta',
                                //style: 'min-height: 20px;padding: 5px;border: 1px solid;border-radius: 2px;border-color: #CCCCCC!important;',
                            }
                        ]
                    }

                ]
            });
        tmp.push(
            {
                xtype: 'fieldset'
                , layout: 'column'
                , style: 'padding:10px 0px 5px 0px;text-align:center;'
                //, id: 'resultprocessttn1'
                , defaults: {msgTarget: 'under', border: false}
                //columnWidth: .100,
                //layout: 'column'
                //, defaults: {msgTarget: 'under', border: false}
                //, anchor: '100%'
                , items: [
                    {
                        columnWidth: .9
                        , layout: 'form'
                        , style: 'text-align:center;'
                        , items: [
                            {
                                xtype: 'button', name: 'button',
                                text: 'Сформировать',
                                //style: 'padding:5px;',
                                id: 'buttoncreatettn',
                                //anchor: '10%',
                                listeners: {
                                    click: {fn: this.submitnovaposhta, scope: this}
                                },

                            }
                        ]
                    }
                ]
            }
            /*{
                xtype: 'button', name: 'button',
                text: 'Сформировать',
                style: 'margin-left:1px;',
                id:'buttoncreatettn',
                anchor: '10%',
                listeners: {
                    click: {fn: this.submitnovaposhta, scope: this}
                },

            }*/
        );


        tmp.push(
            {
                xtype: 'fieldset'
                , layout: 'column'
                , style: 'padding:15px 5px;text-align:center;'
                , id: 'resultprocessttn'
                , defaults: {msgTarget: 'under', border: false}
                //columnWidth: .100,
                //layout: 'column'
                //, defaults: {msgTarget: 'under', border: false}
                //, anchor: '100%'
                , items: [


                    {
                        columnWidth: .3
                        , layout: 'form'
                        , items: [
                            {
                                xtype: 'displayfield', name: 'number_ttn', fieldLabel: 'ТТН', anchor: '90%',
                                id: 'number_ttn',
                                // style: 'min-height: 20px;padding: 5px;border: 1px solid;border-radius: 2px;border-color: #CCCCCC!important;',

                            }
                        ]
                    }
                    , {
                        columnWidth: .3
                        , layout: 'form'
                        , items: [
                            {
                                xtype: 'displayfield',
                                name: 'date_create_ttn',
                                fieldLabel: 'Дата создания ТТН',
                                anchor: '90%',
                                id: 'date_create_ttn',
                                // style: 'min-height: 20px;padding: 5px;border: 1px solid;border-radius: 2px;border-color: #CCCCCC!important;',

                            }
                        ]
                    }
                    , {
                        columnWidth: .3
                        , layout: 'form'
                        , items: [
                            {
                                xtype: 'displayfield', name: 'date_shipping_np', fieldLabel: 'Дата доставки',
                                anchor: '90%',
                                id: 'date_shipping_np',
                                //style: 'min-height: 20px;padding: 5px;border: 1px solid;border-radius: 2px;border-color: #CCCCCC!important;',

                            },
                        ]
                    }
                    , {
                        columnWidth: .3
                        , layout: 'form'
                        , items: [
                            {
                                xtype: 'displayfield', name: 'cost_shipping_np',
                                fieldLabel: 'Стоимость доставки',
                                anchor: '90%',
                                id: 'cost_shipping_np',
                                // style: 'min-height: 20px;padding: 5px;border: 1px solid;border-radius: 2px;border-color: #CCCCCC!important;',

                            }
                        ]
                    }
                    , {
                        columnWidth: .3
                        , layout: 'form'
                        , items: [
                            {
                                xtype: 'button',
                                name: 'buttonprintttn',
                                text: 'Печатать A4',
                                id: 'buttonprintttn',
                                anchor: '90%',
                                style: 'margin-top:20px',
                                listeners: {

                                    click: {fn: this.printnpttn, scope: this}
                                    //handler: {fn:this.submitnovaposhta(), scope: this}
                                }

                            },
                            {
                                xtype: 'button',
                                name: 'buttonprintttn_marker',
                                text: 'Печатать Наклейку',
                                id: 'buttonprintttn_marker',
                                anchor: '90%',
                                style: 'margin-top:20px',
                                listeners: {
                                    click: {fn: this.printnpttnmarker, scope: this}
                                }

                            }
                        ]
                    }
                    , {
                        columnWidth: .3
                        , layout: 'form'
                        , items: []
                    }
                ]
            });

        var n;
        if (tmp.length > 0) {
            for (i = 0; i < tmp.length; i++) {
                /*if (i == 0) fields.push(addx(.7,.3));
                 else if (i == 2) fields.push(addx(.3,.7));
                 else if (i % 2 == 0) fields.push(addx());

                 if (i <= 1) {n = 0;}
                 else {n = Math.floor(i / 2);}*/
                //fields[i].items[i % 2].items.push(tmp[i]);
                //fields[i].items[i].items.
                fields.push(tmp[i]);
            }


        }

        return fields;
    }
    , getSmsFields: function (config) {
        var all = {phone_sms: {}, templatesms: {}, message: {}, message_viber: {}};
        var fields = [], tmp = [];

        tmp.push(
            {
                xtype: 'textfield', name: 'phone_sms', fieldLabel: 'Получатель', anchor: '40%',
                id: 'phone_sms',
                style: 'min-height: 20px;padding: 5px;border: 1px solid;border-radius: 2px;border-color: #CCCCCC!important;',

            }
        );
        tmp.push(
            //{xtype: 'textfield', name: 'templatesms', fieldLabel: 'Выберите шаблон', anchor: '40%',style: 'min-height: 20px;padding: 5px;border: 1px solid;border-radius: 2px;border-color: #CCCCCC!important;'}

            {
                xtype: 'minishop2-combo-templatesms', fieldLabel: 'Выберите шаблон',
                name: 'templatesms', anchor: '40%',
                id: 'minishop2-combo-templatesms-combo',
                listeners: {
                    select: {fn: this.selecttemplate, scope: this}
                }
            }
        );
        //var name = Ext.getCmp('firstName').setValue('JohnRambo');

        tmp.push(
            {
                xtype: 'textarea',
                name: 'message_viber',
                fieldLabel: 'Текст сообщения Ввйбер',
                id: 'submitviberform',
                anchor: '40%',
                style: 'min-height: 20px;padding: 5px;border: 1px solid;border-radius: 2px;border-color: #CCCCCC!important;margin-bottom:20px;'
            }
        );

        tmp.push(
            {
                xtype: 'textarea',
                name: 'message',
                fieldLabel: 'Текст сообщения СМС',
                id: 'submitsmsform',
                anchor: '40%',
                style: 'min-height: 20px;padding: 5px;border: 1px solid;border-radius: 2px;border-color: #CCCCCC!important;margin-bottom:20px;'
            }
        );
        /*tmp.push(
         {xtype: 'displayfield', name: 'warehouse', fieldLabel: 'Отделение', anchor: '98%',style: 'min-height: 20px;padding: 5px;border: 1px solid;border-radius: 2px;border-color: #CCCCCC!important;'}
         );*/

        tmp.push(
            {
                xtype: 'button', name: 'button',
                text: 'Отправить',
                style: 'margin-left:1px;',
                anchor: '10%',
                listeners: {

                    click: {fn: this.submitmessage, scope: this}
                },

            }
        );


        var n;
        if (tmp.length > 0) {
            for (i = 0; i < tmp.length; i++) {
                /*if (i == 0) fields.push(addx(.7,.3));
                 else if (i == 2) fields.push(addx(.3,.7));
                 else if (i % 2 == 0) fields.push(addx());

                 if (i <= 1) {n = 0;}
                 else {n = Math.floor(i / 2);}*/
                //fields[i].items[i % 2].items.push(tmp[i]);
                //fields[i].items[i].items.
                fields.push(tmp[i]);
            }

            /*
             if (miniShop2.config.order_address_fields.in_array('comment')) {
             fields.push(
             {xtype: 'displayfield', name: 'addr_comment', fieldLabel: _('ms2_comment'), anchor: '98%',style: 'min-height: 50px;border:1px solid #efefef;width:95%;'}
             );
             }*/


        }

        return fields;
    }
    , getAddressFields: function (config) {
        var all = {
            receiver: {},
            lastname: {},
            phone: {},
            index: {},
            country: {},
            region: {},
            metro: {},
            building: {},
            city: {},
            street: {},
            room: {},
            housing: {},
            parade: {},
            doorphone: {},
            floor: {},
            email: {}
        };
        var fields = [], tmp = [];
        /*tmp.push(
         {xtype: 'textfield', name: 'addr_lastname', fieldLabel: 'Фамилия', anchor: '98%'}
         );*/
        for (var i = 0; i < miniShop2.config.order_address_fields.length; i++) {
            var field = miniShop2.config.order_address_fields[i];
            if (all[field]) {
                Ext.applyIf(all[field], {
                    xtype: 'textfield'
                    , name: 'addr_' + field
                    , fieldLabel: _('ms2_' + field)
                });
                all[field].anchor = '100%';
                tmp.push(all[field]);
            }
        }


        /*
         tmp.push(
         {xtype: 'textfield', name: 'street', fieldLabel: 'Улица', anchor: '98%'}
         );
         tmp.push(
         {xtype: 'textfield', name: 'building', fieldLabel: 'Дом', anchor: '98%'}
         );
         tmp.push(
         {xtype: 'textfield', name: 'room', fieldLabel: 'Квартира', anchor: '98%'}
         );

         */
        tmp.push(
            {xtype: 'textfield', name: 'addr_warehouse', fieldLabel: 'Отделение', anchor: '98%'}
        );


        var addx = function (w1, w2) {
            if (!w1) {
                w1 = .5;
            }
            if (!w2) {
                w2 = .5;
            }
            return {
                layout: 'column'
                , defaults: {msgTarget: 'under', border: false}
                , items: [
                    {columnWidth: w1, layout: 'form', items: []}
                    , {columnWidth: w2, layout: 'form', items: []}
                ]
            };
        }

        var n;
        if (tmp.length > 0) {
            for (i = 0; i < tmp.length; i++) {
                if (i == 0) fields.push(addx(.7, .3));
                else if (i == 2) fields.push(addx(.3, .7));
                else if (i % 2 == 0) fields.push(addx());

                if (i <= 1) {
                    n = 0;
                }
                else {
                    n = Math.floor(i / 2);
                }
                fields[n].items[i % 2].items.push(tmp[i]);
            }


            if (miniShop2.config.order_address_fields.in_array('comment')) {
                /*fields.push(
                    {
                        xtype: 'displayfield',
                        name: 'addr_comment',
                        fieldLabel: _('ms2_comment'),
                        anchor: '98%',
                        style: 'min-height: 50px;border:1px solid #efefef;width:95%;'
                    }
                );*/
            }


        }

        return fields;
    }
});
Ext.reg('minishop2-window-order-update', miniShop2.window.UpdateOrder);


/*------------------------------------*/
miniShop2.grid.Logs = function (config) {
    config = config || {};

    Ext.applyIf(config, {
        id: this.ident
        , url: miniShop2.config.connector_url
        , baseParams: {
            action: 'mgr/orders/getlog'
            , order_id: config.order_id
            //,type: 'status'
        }
        , fields: ['id', 'user_id', 'username', 'fullname', 'timestamp', 'action', 'entry']
        , pageSize: Math.round(MODx.config.default_per_page / 2)
        , autoHeight: true
        , paging: true
        , remoteSort: true
        , columns: [
            {header: _('ms2_id'), dataIndex: 'id', hidden: true, sortable: true, width: 50}
            , {header: 'Дата', dataIndex: 'timestamp', sortable: true, renderer: miniShop2.utils.formatDate, width: 75}
            , {header: 'Статус', dataIndex: 'action', width: 50}
            , {header: 'Пользователь', dataIndex: 'username', width: 75, renderer: miniShop2.utils.userLink}
            //,{header: _('ms2_fullname'),dataIndex: 'fullname', width: 100}

            , {header: 'Примечание', dataIndex: 'entry', width: 150}
        ]
    });
    miniShop2.grid.Logs.superclass.constructor.call(this, config);
};
Ext.extend(miniShop2.grid.Logs, MODx.grid.Grid);
Ext.reg('minishop2-grid-order-logs', miniShop2.grid.Logs);


/*------------------------------------*/
miniShop2.grid.Np = function (config) {
    config = config || {};

    Ext.applyIf(config, {
        id: this.ident
        , url: miniShop2.config.connector_url
        , baseParams: {
            action: 'mgr/orders/getnp'
            , order_id: config.order_id
        }
        , fields: ['id', 'user_id', 'username', 'fullname', 'timestamp', 'action', 'entry']
        , pageSize: Math.round(MODx.config.default_per_page / 2)
        , autoHeight: true
        , paging: true
        , remoteSort: true
        , columns: [
            {header: _('ms2_id'), dataIndex: 'id', hidden: true, sortable: true, width: 50}
            , {header: 'Дата', dataIndex: 'timestamp', sortable: true, renderer: miniShop2.utils.formatDate, width: 75}
            , {header: 'Статус', dataIndex: 'action', width: 50}
            , {header: 'Пользователь', dataIndex: 'username', width: 75, renderer: miniShop2.utils.userLink}
            , {header: 'Примечание', dataIndex: 'entry', width: 150}
        ]
    });
    miniShop2.grid.Np.superclass.constructor.call(this, config);
};
Ext.extend(miniShop2.grid.Np, MODx.grid.Grid);
Ext.reg('minishop2-grid-order-np', miniShop2.grid.Np);


miniShop2.grid.Products = function (config) {
    config = config || {};
    Ext.applyIf(config, {
        id: this.ident
        , url: miniShop2.config.connector_url
        , baseParams: {
            action: 'mgr/orders/product/getlist'
            , order_id: config.order_id
            , type: 'status'
        }
        , fields: miniShop2.config.order_product_fields
        //['id','product_id','pagetitle','article','weight','count','price','cost']
        , pageSize: Math.round(MODx.config.default_per_page / 2)
        , autoHeight: true
        , paging: true
        , width: '90%'
        , 'class': 'productorder'
        , remoteSort: true
        , columns: this.getColumns()
        /*[
         {header: _('ms2_id'),dataIndex: 'id', hidden: true, sortable: true, width: 40}
         ,{header: _('ms2_product_id'), dataIndex: 'product_id', hidden: true, sortable: true, width: 40}
         ,{header: _('ms2_product_pagetitle'),dataIndex: 'pagetitle', width: 100, renderer: miniShop2.utils.productLink}
         ,{header: _('ms2_product_article'),dataIndex: 'article', width: 50}
         ,{header: _('ms2_product_weight'),dataIndex: 'weight', sortable: true, width: 50}
         ,{header: _('ms2_product_price'),dataIndex: 'price', sortable: true, width: 50}
         ,{header: _('ms2_count'),dataIndex: 'count', sortable: true, width: 50}
         ,{header: _('ms2_cost'),dataIndex: 'cost', width: 50}
         ]*/
        , tbar: [{
            xtype: 'minishop2-combo-productorder'//minishop2-combo-product'
            , allowBlank: true
            , width: '90%'

            , listeners: {
                select: {fn: this.addOrderProduct, scope: this}
            }
        }]
        , listeners: {
            rowDblClick: function (grid, rowIndex, e) {
                var row = grid.store.getAt(rowIndex);
                this.updateOrderProduct(grid, e, row);
            }
        }
    });
    miniShop2.grid.Products.superclass.constructor.call(this, config);
};


Ext.extend(miniShop2.grid.Products, MODx.grid.Grid, {

    getMenu: function () {
        var m = [];
        m.push({
            text: _('ms2_menu_update')
            , handler: this.updateOrderProduct
        });
        m.push('-');
        m.push({
            text: _('ms2_menu_remove')
            , handler: this.removeOrderProduct
        });
        this.addContextMenuItem(m);
    }

    , getColumns: function () {
        var fields = {
            id: {hidden: true, sortable: true, width: 40}
            , product_id: {hidden: true, sortable: true, width: 40}
            , action: {hidden: true, sortable: true, width: 0}
            , name: {header: _('ms2_name'), width: 100, renderer: miniShop2.utils.productLink}
            , option_size: {header: 'Опция', width: 60}
            , stock: {header: 'Склад', width: 60}
            , available: {header: 'Наличие', width: 60}
            , price_old: {header: _('ms2_product_price'), width: 50}
            //,skidka: {header: 'Скидка по дисконту', width: 50}
            //,discount: {header: 'Дисконт(доп)', width: 50}
            , product_article: {width: 50}
            , option_artman: {header: 'Артикл производителя', width: 100}
            , option_artpost: {header: 'Артикл поставщика', width: 100}
            , weight: {sortable: true, width: 50}
            , price: {sortable: true, header: _('ms2_product_price'), width: 50}
            , count: {sortable: true, width: 50}
            , cost: {width: 50}
            , promocode: {width: 50}

        };

        var columns = [];
        for (var i = 0; i < miniShop2.config.order_product_fields.length; i++) {
            var field = miniShop2.config.order_product_fields[i];
            if (fields[field]) {
                Ext.applyIf(fields[field], {
                    header: _('ms2_' + field)
                    , dataIndex: field
                });
                columns.push(fields[field]);
            }
            else if (/^option_/.test(field)) {
                columns.push(
                    {header: _(field.replace(/^option_/, 'ms2_')), dataIndex: field, width: 50}
                );
            }
            else if (/^product_/.test(field)) {
                columns.push(
                    {header: _(field.replace(/^product_/, 'ms2_')), dataIndex: field, width: 75}
                );
            }
        }

        return columns;
    }

    , selectCityNp: function (combo, row, e) {
        var id = row.id;
        //alert(id);
        alert('d');
    }
    , addOrderProduct: function (combo, row, e) {
        var id = row.id;
        var option = row.data.option;

        var id_option = row.data.id_option;

        combo.reset();

        MODx.Ajax.request({
            url: miniShop2.config.connector_url
            , params: {
                action: 'mgr/product/get'
                , id: id_option//id
                , option: option
            }
            , listeners: {
                success: {
                    fn: function (r) {
                        var w = Ext.getCmp('minishop2-window-orderproduct-update');
                        if (w) {
                            w.hide().getEl().remove();
                        }

                        r.object.order_id = this.config.order_id;
                        r.object.count = 1;
                        w = MODx.load({
                            xtype: 'minishop2-window-orderproduct-update'
                            , id: 'minishop2-window-orderproduct-update'
                            , record: r.object
                            , action: 'mgr/orders/product/create'
                            , listeners: {
                                success: {
                                    fn: function () {
                                        miniShop2.grid.Orders.changed = true;
                                        this.refresh();
                                    }, scope: this
                                }
                                , hide: {
                                    fn: function () {
                                        this.getEl().remove();
                                    }
                                }
                            }
                        });
                        w.fp.getForm().reset();
                        w.fp.getForm().setValues(r.object);
                        w.show(e.target, function () {
                            w.setPosition(null, 100)
                        }, this);
                    }, scope: this
                }
            }
        });
    }

    , updateOrderProduct: function (btn, e, row) {
        if (typeof(row) != 'undefined') {
            this.menu.record = row.data;
        }
        var id = this.menu.record.id;

        MODx.Ajax.request({
            url: miniShop2.config.connector_url
            , params: {
                action: 'mgr/orders/product/get'
                , id: id
            }
            , listeners: {
                success: {
                    fn: function (r) {
                        var w = Ext.getCmp('minishop2-window-orderproduct-update');
                        if (w) {
                            w.hide().getEl().remove();
                        }

                        r.object.order_id = this.config.order_id;
                        w = MODx.load({
                            xtype: 'minishop2-window-orderproduct-update'
                            , id: 'minishop2-window-orderproduct-update'
                            , record: r.object
                            , action: 'mgr/orders/product/update'
                            , listeners: {
                                success: {
                                    fn: function () {
                                        miniShop2.grid.Orders.changed = true;
                                        this.refresh();
                                    }, scope: this
                                }
                                , hide: {
                                    fn: function () {
                                        this.getEl().remove();
                                    }
                                }
                            }
                        });
                        w.fp.getForm().reset();
                        w.fp.getForm().setValues(r.object);
                        w.show(e.target, function () {
                            w.setPosition(null, 100)
                        }, this);
                    }, scope: this
                }
            }
        });
    }

    , removeOrderProduct: function (btn, e) {
        if (!this.menu.record) return false;

        MODx.msg.confirm({
            title: _('ms2_menu_remove')
            , text: _('ms2_menu_remove_confirm')
            , url: miniShop2.config.connector_url
            , params: {
                action: 'mgr/orders/product/remove'
                , id: this.menu.record.id
            }
            , listeners: {
                success: {
                    fn: function (r) {
                        this.refresh();
                    }, scope: this
                }
            }
        });
        return true;
    }
});
Ext.reg('minishop2-grid-order-products', miniShop2.grid.Products);


miniShop2.window.OrderProduct = function (config) {
    config = config || {};
    this.ident = config.ident || 'meuitem' + Ext.id();

    Ext.applyIf(config, {
        title: _('ms2_menu_update')
        , autoHeight: true
        , width: 600
        , autoHeight: true
        , url: miniShop2.config.connector_url
        , action: config.action || 'mgr/orders/product/update'
        , fields: [
            {xtype: 'hidden', name: 'id'}
            , {xtype: 'hidden', name: 'order_id'}
            , {
                layout: 'column'
                , border: false
                , anchor: '100%'
                , items: [
                    {
                        columnWidth: .3, layout: 'form', defaults: {msgTarget: 'under'}, border: false, items: [
                            {
                                xtype: 'numberfield',
                                fieldLabel: _('ms2_product_count'),
                                name: 'count',
                                anchor: '100%',
                                allowNegative: false,
                                allowBlank: false
                            }
                        ]
                    }
                    , {
                        columnWidth: .7, layout: 'form', defaults: {msgTarget: 'under'}, border: false, items: [
                            {xtype: 'textfield', fieldLabel: _('ms2_name'), name: 'pagetitle', anchor: '100%'}
                        ]
                    }
                ]
            }
            , {
                layout: 'column'
                , border: false
                , anchor: '100%'
                , items: [
                    {
                        columnWidth: .5, layout: 'form', defaults: {msgTarget: 'under'}, border: false, items: [
                            {
                                xtype: 'numberfield',
                                decimalPrecision: 2,
                                fieldLabel: _('ms2_product_price'),
                                name: 'price',
                                anchor: '100%'
                            },
                            {
                                xtype: 'numberfield',
                                decimalPrecision: 2,
                                fieldLabel: 'Цена без скидки',
                                name: 'price_old',
                                anchor: '100%'
                            },
                            {
                                xtype: 'numberfield',
                                decimalPrecision: 2,
                                fieldLabel: 'Скидка',
                                name: 'skidka',
                                anchor: '100%'
                            },
                            {
                                xtype: 'numberfield',
                                decimalPrecision: 2,
                                fieldLabel: 'Установите скидку',
                                name: 'discount',
                                anchor: '100%'
                            },
                            /*{
                                xtype: 'numberfield',
                                decimalPrecision: 2,
                                fieldLabel: 'Установите скидку',
                                name: 'stock',
                                anchor: '100%'
                            }
*/
                           /* {
                                xtype: 'minishop2-combo-stock', fieldLabel: 'Склад',
                                name: 'stock', anchor: '40%',
                                id: 'minishop2-combo-stock-combo',
                                listeners: {
                                    select: {fn: this.selecttemplate, scope: this}
                                }
                            }*/
                            {
                                xtype: 'minishop2-combo-getstocks', fieldLabel: 'Выберите cклад',
                                name: 'stock', anchor: '40%',
                                id: 'minishop2-combo-getstocks-combo',
                                listeners: {
                                    //select: {fn: this.selecttemplate, scope: this}
                                }
                            }
/*
                            Магазин
                            Склад
                            Экватор
                            Поставщик
                            Комбинированный
*/
                        ]
                    }
                    , {
                        columnWidth: .5, layout: 'form', defaults: {msgTarget: 'under'}, border: false, items: [
                            //{xtype: 'numberfield', decimalPrecision: 3, fieldLabel: _('ms2_product_weight'), name: 'weight', anchor: '100%'}
                            {
                                xtype: 'textfield',
                                decimalPrecision: 3,
                                fieldLabel: 'Опция',
                                name: 'options',
                                anchor: '100%'
                            }

                        ]
                    }
                ]
            },
            //{xtype: 'textarea',fieldLabel: '4555'+_('ms2_product_options'), name: 'options', height: 100, anchor: '100%'}
            /*{xtype: 'modx-superbox-options',fieldLabel: _('ms2_product_options'),

             name: 'options',id: 'modx-'+this.ident+'-options',anchor: '99%'}*/
            /*
             ,{
             xtype : 'combo'
             ,editable : false
             ,width : 40
             ,fieldLabel : _('ms2_product_options')
             ,name : 'options'
             ,valueField : 'id'
             ,hiddenName: 'options'
             ,displayField : 'options'
             ,triggerAction:'all'
             ,allowBlank : false
             ,mode:'local'
             ,anchor: '50%'
             ,store: new Ext.data.ArrayStore({
             id: this.ident+'-options'
             ,fields: ['id','options']
             ,data: [['static','Статический'],['dinamic','Динамический']]
             })
             }*/

            //,{xtype: 'textarea',fieldLabel: '4555'+_('ms2_product_options'), name: 'options1', height: 100, anchor: '100%'}
            //,{xtype: 'minishop2-combo-status', name: 'options', fieldLabel: _('ms2_product_options', anchor: '100%'}

        ]
        , keys: [{
            key: Ext.EventObject.ENTER, shift: true, fn: function () {
                this.submit()
            }, scope: this
        }]
    });
    miniShop2.window.OrderProduct.superclass.constructor.call(this, config);
};
Ext.extend(miniShop2.window.OrderProduct, MODx.Window);
Ext.reg('minishop2-window-orderproduct-update', miniShop2.window.OrderProduct);


MODx.combo.Options = function (config) {
    config = config || {};
    Ext.applyIf(config, {
        name: 'options'
        , hiddenName: 'option'
        , displayField: 'options'
        , valueField: 'id'
        , fields: ['id', 'options']
        , pageSize: 10
        , hideMode: 'options'
        , url: MODx.config.connectors_url
        , baseParams: {
            action: 'mgr/orders/product/getlistoptions'
            , product: this.product_id
        }
    });
    MODx.combo.Options.superclass.constructor.call(this, config);
};
Ext.extend(MODx.combo.Options, MODx.combo.ComboBox);
Ext.reg('modx-superbox-options', MODx.combo.Options);


MODx.combo.Deliverycost = function (config) {
    config = config || {};
    Ext.applyIf(config, {
        store: new Ext.data.SimpleStore({
            fields: ['name', 'delivery_cost']
            , data: this.getTypes()
        })
        , emptyText: _('ms2_combo_select')
        , displayField: 'delivery_cost'
        , valueField: 'delivery_cost'
        , hiddenName: 'delivery_cost'
        , mode: 'local'
        , triggerAction: 'all'
        /*,editable: false
         ,selectOnFocus: false
         ,preventRender: true
         ,forceSelection: true
         ,enableKeyEvents: true
         */
        //,itemSelector: 'div.delivery-cost-item11'
    });
    MODx.combo.Deliverycost.superclass.constructor.call(this, config);
};
Ext.extend(MODx.combo.Deliverycost, MODx.combo.ComboBox, {

    getTypes: function () {
        var array = [];
        var types = [];
        types[0] = 0;
        types[1] = 45;
        types[2] = 70;
        array.push([0, 0]);
        array.push([45, 45]);//35);//[t, t]);
        array.push([70, 70]);//50);

        /*for(var i = 0; i < types.length; i++) {
         var t = types[i];
         array.push([t, t]);
         }*/
        return array;
    }
});
Ext.reg('minishop2-combo-costdelivery', MODx.combo.Deliverycost);

//


miniShop2.window.ReportOrder = function (config) {
    config = config || {};
    this.ident = config.ident || 'meuitem' + Ext.id();
    Ext.applyIf(config, {
        id: this.ident
        , url: miniShop2.config.connector_url
        , params: {
            action: 'mgr/orders/getreport'
            , ids: config.cs
        }
        , width: '70%'
        , autoHeight: true
        , fields: [{xtype: 'displayfield', name: 'sales', fieldLabel: 'Количество продаж'},
            {xtype: 'displayfield', name: 'amounts', fieldLabel: 'Оборот'},
            {xtype: 'displayfield', name: 'cache', fieldLabel: 'Наличные'},
            {xtype: 'displayfield', name: 'carts', fieldLabel: 'Карта'},
            {xtype: 'displayfield', name: 'nal', fieldLabel: 'Наложенный'}

        ]

    });

    miniShop2.window.ReportOrder.superclass.constructor.call(this, config);

};
Ext.extend(miniShop2.window.ReportOrder, MODx.Window, {

    /*getTabs: function(config) {


     return tabs;
     }*/

});
Ext.reg('minishop2-window-order-report', miniShop2.window.ReportOrder);


miniShop2.combo.StreetNovaposhta = function (config) {
    config = config || {};


    //alert(order_id);
    //alert(config.order_id);
//alert(Ext.getCmp('minishop2-combo-np_city').getValue());

    Ext.applyIf(config, {
        id: 'minishop2-combo-np_street'
        , fieldLabel: 'Улица'//_('ms2_product_name')
        , fields: ['title', 'id']
        , valueField: 'id'//id'
        , displayField: 'title'
        , name: 'city_street_np'
        , hiddenName: 'city_street_np'
        , allowBlank: false
        , width: 600
        , url: miniShop2.config.connector_url
        , baseParams: {
            action: 'mgr/orders/np/getstreet'
            , combo: 1
            , id: config.value
            , order_id: config.order_id
        }
        , tpl: new Ext.XTemplate(''
            + '<tpl for="."><div title="{title}" class="x-combo-list-item minishop2-np_street">'
            + '{title}</div></tpl>', {
            compiled: true
        })
        , itemSelector: 'div.minishop2-np_street'
        , pageSize: 20
        , emptyText: _('ms2_combo_select')
        , typeAhead: true
        , editable: true
    });


    //
    // Ext.applyIf(config, {
    //     id: 'minishop2-combo-np_steet'
    //     , fieldLabel: 'Улица'
    //     , fields: ['title', 'id']
    //     , valueField: 'id'
    //     , displayField: 'title'
    //     , name: 'city_street_np'
    //     , hiddenName: 'city_street_np'
    //     , allowBlank: false
    //     , size: 40
    //     , width: 600
    //     , url: miniShop2.config.connector_url
    //     , baseParams: {
    //         action: 'mgr/orders/np/getstreet'
    //         , combo: 1
    //         , id: config.value
    //         , order_id: config.order_id//config.order_id
    //         //,city_id: config.city_id//Ext.get('city_np').getValue()
    //     }
    //     , tpl: new Ext.XTemplate(''
    //         + '<tpl for="."><div title="{title}" class="x-combo-list-item minishop2-np_steet">'
    //         + '{title}</div></tpl>', {
    //         compiled: true
    //     })
    //     , itemSelector: 'div.minishop2-np_street'
    //     , pageSize: 20
    //     //, hideMode: 'offsets'
    //     , emptyText: _('ms2_combo_select')
    //     , typeAhead: true
    //     , editable: true
    //
    // });
    miniShop2.combo.StreetNovaposhta.superclass.constructor.call(this, config);
};
/*beforequery:function( queryEvent ){
    alert('d');
}*/
Ext.extend(miniShop2.combo.StreetNovaposhta, MODx.combo.ComboBox);
Ext.reg('minishop2-combo-np_street', miniShop2.combo.StreetNovaposhta);


miniShop2.combo.CityNovaposhta = function (config) {
    config = config || {};

    Ext.applyIf(config, {
        id: 'minishop2-combo-np_city'
        , fieldLabel: 'Город'//_('ms2_product_name')
        , fields: ['title', 'id']
        , valueField: 'id'//id'
        , displayField: 'title'
        , name: 'addr_city_id_np'
        , hiddenName: 'addr_city_id_np'
        , allowBlank: false
        , width: 600
        , url: miniShop2.config.connector_url
        , baseParams: {
            action: 'mgr/orders/np/getcity'
            , combo: 1
            , id: config.value
            , order_id: config.order_id
        }
        , tpl: new Ext.XTemplate(''
            + '<tpl for="."><div title="{title}" class="x-combo-list-item minishop2-np_cities">'
            + '{title}</div></tpl>', {
            compiled: true
        })
        , itemSelector: 'div.minishop2-np_cities'
        , pageSize: 20
        , emptyText: _('ms2_combo_select')
        , typeAhead: true
        , editable: true
    });
    miniShop2.combo.CityNovaposhta.superclass.constructor.call(this, config);
};
Ext.extend(miniShop2.combo.CityNovaposhta, MODx.combo.ComboBox);
Ext.reg('minishop2-combo-np_city', miniShop2.combo.CityNovaposhta);


miniShop2.combo.WarehouseNovaposhta = function (config) {
    config = config || {};
//alert(config.city_id);
    Ext.applyIf(config, {
        id: 'minishop2-combo-np_warehouse'
        , fieldLabel: 'Отделение'//_('ms2_product_name')
        , fields: ['title', 'id']
        , valueField: 'id'//id'
        , displayField: 'title'
        , name: 'addr_warehouse_id'
        , hiddenName: 'addr_warehouse_id'
        , allowBlank: false
        , width: 600
        , url: miniShop2.config.connector_url
        , baseParams: {
            action: 'mgr/orders/np/getwarehouse'
            , combo: 1
            , id: config.value
            , order_id: config.order_id
        }
        , pageSize: 20
        , emptyText: _('ms2_combo_select')
        , typeAhead: true
        , editable: true
    });
    miniShop2.combo.WarehouseNovaposhta.superclass.constructor.call(this, config);
};
Ext.extend(miniShop2.combo.WarehouseNovaposhta, MODx.combo.ComboBox);
Ext.reg('minishop2-combo-np_warehouse', miniShop2.combo.WarehouseNovaposhta);