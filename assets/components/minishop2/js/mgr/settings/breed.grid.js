miniShop2.grid.Breed = function(config) {
	config = config || {};

	this.exp = new Ext.grid.RowExpander({
		expandOnDblClick: false
		,tpl : new Ext.Template('<p class="desc">{description}</p>')
		,renderer : function(v, p, record){return record.data.description != '' && record.data.description != null ? '<div class="x-grid3-row-expander">&#160;</div>' : '&#160;';}
	});

	Ext.applyIf(config,{
		id: 'minishop2-grid-breed'
		,url: miniShop2.config.connector_url
		,baseParams: {
			action: 'mgr/settings/breed/getlist'
		}
		,fields: ['id','name','pet_type']
		,autoHeight: true
		,paging: true
		,remoteSort: true
		,save_action: 'mgr/settings/breed/updatefromgrid'
		,autosave: true
		,plugins: this.exp
		,columns: [this.exp
			,{header: _('ms2_id'),dataIndex: 'id',width: 50, sortable: true}
			,{header: _('ms2_name'),dataIndex: 'name',width: 100, editor: {xtype: 'textfield', allowBlank: false}, sortable: true}
			,{header: _('ms2_pet_types'),dataIndex: 'pet_type',width: 100, editor: {xtype: 'minishop2-combo-pet_type'}, sortable: true}
		]
		,tbar: [{
			text: _('ms2_btn_create')
			,handler: this.createBreed
			,scope: this
		}]
	});
	miniShop2.grid.Breed.superclass.constructor.call(this,config);
};
Ext.extend(miniShop2.grid.Breed,MODx.grid.Grid,{
	windows: {}

	,getMenu: function() {
		var m = [];
		m.push({
			text: _('ms2_menu_update')
			,handler: this.updateBreed
		});
		m.push('-');
		m.push({
			text: _('ms2_menu_remove')
			,handler: this.removeBreed
		});
		this.addContextMenuItem(m);
	}

	,createBreed: function(btn,e) {
		if (!this.windows.createBreed) {
			this.windows.createBreed = MODx.load({
				xtype: 'minishop2-window-breed-create'
				,fields: this.getBreedFields('create')
				,listeners: {
					success: {fn:function() { this.refresh(); },scope:this}
				}
			});
		}
		this.windows.createBreed.fp.getForm().reset();
		this.windows.createBreed.show(e.target);
	}

	,updateBreed: function(btn,e) {
		if (!this.menu.record || !this.menu.record.id) return false;
		var r = this.menu.record;

		if (!this.windows.updateBreed) {
			this.windows.updateBreed = MODx.load({
				xtype: 'minishop2-window-breed-update'
				,record: r
				,fields: this.getBreedFields('update')
				,listeners: {
					success: {fn:function() { this.refresh(); },scope:this}
				}
			});
		}
		this.windows.updateBreed.fp.getForm().reset();
		this.windows.updateBreed.fp.getForm().setValues(r);
		this.windows.updateBreed.show(e.target);
	}

	,removeBreed: function(btn,e) {
		if (!this.menu.record) return false;

		MODx.msg.confirm({
			title: _('ms2_menu_remove') + '"' + this.menu.record.name + '"'
			,text: _('ms2_menu_remove_confirm')
			,url: this.config.url
			,params: {
				action: 'mgr/settings/breed/remove'
				,id: this.menu.record.id
			}
			,listeners: {
				success: {fn:function(r) {this.refresh();}, scope:this}
			}
		});
	}

	,getBreedFields: function(type) {
		return [
			{xtype: 'hidden',name: 'id', id: 'minishop2-breed-id-'+type}
			,{xtype: 'textfield',fieldLabel: _('ms2_name'), name: 'name', allowBlank: false, anchor: '99%', id: 'minishop2-breed-name-'+type}
			,{xtype: 'minishop2-combo-pet_type',fieldLabel: _('ms2_pet_types'), name: 'pet_type', anchor: '99%', id: 'minishop2-breed-pet_type-'+type}
		];
	}

});
Ext.reg('minishop2-grid-breed',miniShop2.grid.Breed);




miniShop2.window.CreateBreed = function(config) {
	config = config || {};
	this.ident = config.ident || 'mecitem'+Ext.id();
	Ext.applyIf(config,{
		title: _('ms2_menu_create')
		,id: this.ident
		,width: 600
		,autoHeight: true
		,labelAlign: 'left'
		,labelWidth: 180
		,url: miniShop2.config.connector_url
		,action: 'mgr/settings/breed/create'
		,fields: config.fields
		,keys: [{key: Ext.EventObject.ENTER,shift: true,fn: function() {this.submit() },scope: this}]
	});
	miniShop2.window.CreateBreed.superclass.constructor.call(this,config);
};
Ext.extend(miniShop2.window.CreateBreed,MODx.Window);
Ext.reg('minishop2-window-breed-create',miniShop2.window.CreateBreed);


miniShop2.window.UpdateBreed = function(config) {
	config = config || {};
	this.ident = config.ident || 'meuitem'+Ext.id();
	Ext.applyIf(config,{
		title: _('ms2_menu_update')
		,id: this.ident
		,width: 600
		,autoHeight: true
		,labelAlign: 'left'
		,labelWidth: 180
		,url: miniShop2.config.connector_url
		,action: 'mgr/settings/breed/update'
		,fields: config.fields
		,keys: [{key: Ext.EventObject.ENTER,shift: true,fn: function() {this.submit() },scope: this}]
	});
	miniShop2.window.UpdateBreed.superclass.constructor.call(this,config);
};
Ext.extend(miniShop2.window.UpdateBreed,MODx.Window);
Ext.reg('minishop2-window-breed-update',miniShop2.window.UpdateBreed);
