miniShop2.grid.City = function(config) {
	config = config || {};

	this.exp = new Ext.grid.RowExpander({
		expandOnDblClick: false
		,tpl : new Ext.Template('<p class="desc"></p>')
		,renderer : function(v, p, record){return record.data.description != '' && record.data.description != null ? '<div class="x-grid3-row-expander">&#160;</div>' : '&#160;';}
	});
	this.dd = function(grid) {
		this.dropTarget = new Ext.dd.DropTarget(grid.container, {
			ddGroup : 'dd',
			copy:false,
			notifyDrop : function(dd, e, data) {
				var store = grid.store.data.items;
				var target = store[dd.getDragData(e).rowIndex].id;
				var source = store[data.rowIndex].id;
				if (target != source) {
					dd.el.mask(_('loading'),'x-mask-loading');
					MODx.Ajax.request({
						url: miniShop2.config.connector_url
						,params: {
							action: config.action || 'mgr/settings/City/sort'
							,source: source
							,target: target
						}
						,listeners: {
							success: {fn:function(r) {dd.el.unmask();grid.refresh();},scope:grid}
							,failure: {fn:function(r) {dd.el.unmask();},scope:grid}
						}
					});
				}
			}
		});
	};
	Ext.applyIf(config,{
		id: 'minishop2-grid-city'
		,url: miniShop2.config.connector_url
		,baseParams: {
			action: 'mgr/settings/city/getlist'
		}
		,fields: ['id','Description','DescriptionRu','AreaDescriptionRu','SettlementTypeDescriptionRu','kyrier','samov','created','updated','poshtomat']
		,autoHeight: true
		,paging: true
		,remoteSort: true
		,save_action: 'mgr/settings/City/updatefromgrid'
		,autosave: true
		,plugins: this.exp
		,columns: [this.exp
			,{header: _('ms2_id'),dataIndex: 'id',width: 50}
			//,{header: _('ms2_name'),dataIndex: 'Ref',width: 100, editor: {xtype: 'textfield'}, sortable: true}
			,{header: 'Название укр',dataIndex: 'Description',width: 50, editor: {xtype: 'textfield'}, sortable: true}
			,{header: 'Название',dataIndex: 'DescriptionRu',width: 50, editor: {xtype: 'textfield'}, sortable: true}
            ,{header: 'Регион',dataIndex: 'AreaDescriptionRu',width: 50, editor: {xtype: 'textfield'}, sortable: true}
            ,{header: 'Тип',dataIndex: 'SettlementTypeDescriptionRu',width: 50, editor: {xtype: 'textfield'}, sortable: true}
            ,{header: 'Создание',dataIndex: 'created',width: 50,  sortable: true}
            ,{header: 'Обновление',dataIndex: 'updated',width: 50,  sortable: true}

			,{header: _('ms2_kyrier'),dataIndex: 'kyrier',width: 50, editor: {xtype: 'xcheckbox'}, sortable: true}
			,{header: _('ms2_samov'),dataIndex: 'samov',width: 50, editor: {xtype: 'xcheckbox'}, sortable: true}
            ,{header: 'Почтоматы',dataIndex: 'poshtomat',width: 50, editor: {xtype: 'xcheckbox'}, sortable: true}
			
			
			
		]
		,tbar: [{
			text: _('ms2_btn_create')
			,handler: this.createCity
			,scope: this
		}]
		,ddGroup: 'dd'
		,enableDragDrop: true
		,listeners: {render: {fn: this.dd, scope: this}}
	});
	miniShop2.grid.City.superclass.constructor.call(this,config);
};
Ext.extend(miniShop2.grid.City,MODx.grid.Grid,{
	windows: {}

	,getMenu: function() {
		var m = [];
		m.push({
			text: _('ms2_menu_update')
			,handler: this.updateCity
		});
		m.push('-');
		m.push({
			text: _('ms2_menu_remove')
			,handler: this.removeCity
		});
		this.addContextMenuItem(m);
	}

	,renderLogo: function(value) {
		if (/(jpg|png|gif|jpeg)$/i.test(value)) {
			if (!/^\//.test(value)) {value = '/' + value;}
			return '<img src="'+value+'" height="35" />';
		}
		else {
			return '';
		}
	}

	,createCity: function(btn,e) {
		var w = Ext.getCmp('minishop2-window-City-create');
		if (w) {w.hide().getEl().remove();}
		//if (!this.windows.createCity) {
			this.windows.createCity = MODx.load({
				xtype: 'minishop2-window-City-create'
				,id: 'minishop2-window-City-create'
				,fields: this.getCityFields('create', {})
				,listeners: {
					success: {fn:function() { this.refresh(); },scope:this}
					,hide: {fn:function() {this.getEl().remove()}}
				}
			});
		//}
		this.windows.createCity.fp.getForm().reset().setValues({
			price: 0
			,weight_price: 0
			,distance_price: 0
		});
		this.windows.createCity.show(e.target);
	}
	,updateCity: function(btn,e) {
		if (!this.menu.record || !this.menu.record.id) return false;
		var r = this.menu.record;

		var w = Ext.getCmp('minishop2-window-City-update');
		if (w) {w.hide().getEl().remove();}
		//if (!this.windows.updateCity) {
			this.windows.updateCity = MODx.load({
				xtype: 'minishop2-window-City-update'
				,id: 'minishop2-window-City-update'
				,record: r
				,fields: this.getCityFields('update', r)
				,listeners: {
					success: {fn:function() {this.refresh();},scope:this}
					,hide: {fn:function() {this.getEl().remove()}}
				}
			});
		//}
		this.windows.updateCity.fp.getForm().reset();
		this.windows.updateCity.fp.getForm().setValues(r);
		this.windows.updateCity.show(e.target);
		//this.enablePayments(r.payments);
	}

	,removeCity: function(btn,e) {
		if (!this.menu.record) return false;

		MODx.msg.confirm({
			title: _('ms2_menu_remove') + '"' + this.menu.record.name + '"'
			,text: _('ms2_menu_remove_confirm')
			,url: this.config.url
			,params: {
				action: 'mgr/settings/City/remove'
				,id: this.menu.record.id
			}
			,listeners: {
				success: {fn:function(r) {this.refresh();}, scope:this}
			}
		});
	}

	,getCityFields: function(type) {
		var fields = [];
		fields.push({xtype: 'hidden',name: 'id', id: 'minishop2-City-id-'+type}
			,{xtype: 'textfield',fieldLabel: _('ms2_name'), name: 'DescriptionRu', allowBlank: false, anchor: '99%', id: 'minishop2-City-descriptionru-'+type}
            ,{xtype: 'textfield',fieldLabel: 'Название на укр', name: 'Description', allowBlank: false, anchor: '99%', id: 'minishop2-City-description-'+type}
			,{xtype: 'textfield',fieldLabel: _('ms2_region'), description: _('ms2_region'), name: 'AreaDescriptionRu', allowBlank: true, anchor: '50%', id: 'minishop2-City-region_id-'+type}
			//,{xtype: 'textfield',fieldLabel: _('ms2_district'), description: _('ms2_district'), name: 'district', allowBlank: true, anchor: '50%', id: 'minishop2-City-phone_code-'+type}
			,{xtype: 'xcheckbox',fieldLabel: _('ms2_kyrier'), name: 'kyrier', anchor: '99%', id: 'minishop2-city-kyrier-'+type}
			//,{xtype: 'xcheckbox',fieldLabel: _('ms2_show_all'), name: 'show_all', anchor: '99%', id: 'minishop2-city-show_all-'+type}
			,{xtype: 'xcheckbox',fieldLabel: _('ms2_samov'), name: 'samov', anchor: '99%', id: 'minishop2-city-samov-'+type}
            ,{xtype: 'xcheckbox',fieldLabel: 'Почтоматы', name: 'poshtomat', anchor: '99%', id: 'minishop2-city-poshtomat-'+type}
		);
		
		
		/*var payments = this.getAvailablePayments();
		if (payments.length > 0) {
			fields.push(
				{xtype: 'checkboxgroup'
					,fieldLabel: _('ms2_payments')
					,columns: 2
					,items: payments
					,id: 'minishop2-City-payments-'+type
				}
			);
		}
		fields.push(
			{xtype: 'textfield',fieldLabel: _('ms2_class'), name: 'class', anchor: '99%', id: 'minishop2-City-class-'+type}
			,{xtype: 'xcheckbox', fieldLabel: '', boxLabel: _('ms2_active'), name: 'active', id: 'minishop2-City-active-'+type}
		);*/

		return fields;
	}
/*
	,getAvailablePayments: function() {
		var payments = [];
		var items = miniShop2.PaymentsArray;
		for (i in items) {
			if (items.hasOwnProperty(i)) {
				var id = items[i].id;
				payments.push({
					xtype: 'xcheckbox'
					,boxLabel: items[i].name
					,name: 'payments['+id+']'
					,payment_id: id
				});
			}
		}
		return payments;
	}

	,enablePayments: function(payments) {
		if (payments.length < 1) {return;}
		var checkboxgroup = Ext.getCmp('minishop2-City-payments-update');
		Ext.each(checkboxgroup.items.items, function(item) {
			if (payments[item.payment_id] == 1) {
				item.setValue(true);
			}
			else {
				item.setValue(false);
			}
		});
	}*/

	,beforeDestroy: function() {
		if (this.rendered) {
			this.dropTarget.destroy();
		}
		miniShop2.grid.City.superclass.beforeDestroy.call(this);
	}
});
Ext.reg('minishop2-grid-city',miniShop2.grid.City);




miniShop2.window.CreateCity = function(config) {
	config = config || {};
	this.ident = config.ident || 'mecitem'+Ext.id();
	Ext.applyIf(config,{
		title: _('ms2_menu_create')
		,id: this.ident
		,width: 600
		,autoHeight: true
		,labelAlign: 'left'
		,labelWidth: 180
		,url: miniShop2.config.connector_url
		,action: 'mgr/settings/City/create'
		,fields: config.fields
		,keys: [{key: Ext.EventObject.ENTER,shift: true,fn: function() {this.submit() },scope: this}]
	});
	miniShop2.window.CreateCity.superclass.constructor.call(this,config);
};
Ext.extend(miniShop2.window.CreateCity,MODx.Window);
Ext.reg('minishop2-window-City-create',miniShop2.window.CreateCity);


miniShop2.window.UpdateCity = function(config) {
	config = config || {};
	this.ident = config.ident || 'meuitem'+Ext.id();
	Ext.applyIf(config,{
		title: _('ms2_menu_update')
		,id: this.ident
		,width: 600
		,autoHeight: true
		,labelAlign: 'left'
		,labelWidth: 180
		,url: miniShop2.config.connector_url
		,action: 'mgr/settings/City/update'
		,fields: config.fields
		,keys: [{key: Ext.EventObject.ENTER,shift: true,fn: function() {this.submit() },scope: this}]
	});
	miniShop2.window.UpdateCity.superclass.constructor.call(this,config);
};
Ext.extend(miniShop2.window.UpdateCity,MODx.Window);
Ext.reg('minishop2-window-City-update',miniShop2.window.UpdateCity);