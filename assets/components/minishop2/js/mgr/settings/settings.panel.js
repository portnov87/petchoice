miniShop2.page.Settings = function (config) {
    config = config || {};
    Ext.applyIf(config, {
        components: [{
            xtype: 'minishop2-panel-settings'
            , renderTo: 'minishop2-panel-settings-div'
        }]
    });
    miniShop2.page.Settings.superclass.constructor.call(this, config);
};
Ext.extend(miniShop2.page.Settings, MODx.Component);
Ext.reg('minishop2-page-settings', miniShop2.page.Settings);

miniShop2.panel.Settings = function (config) {
    config = config || {};
    Ext.apply(config, {
        border: false
        , deferredRender: true
        , baseCls: 'modx-formpanel'
        , items: [{
            html: '<h2>' + _('minishop2') + ' :: ' + _('ms2_settings') + '</h2>'
            , border: false
            , cls: 'modx-page-header container'
        }, {
            xtype: 'modx-tabs'
            , bodyStyle: 'padding: 5px'
            , defaults: {border: false, autoHeight: true}
            , border: true
            , hideMode: 'offsets'
            , stateful: true
            , stateId: 'minishop2-settings-tabpanel'
            , stateEvents: ['tabchange']
            , getState: function () {
                return {activeTab: this.items.indexOf(this.getActiveTab())};
            }
            , items: [{
                title: _('ms2_deliveries')
                , deferredRender: true
                , items: [{
                    html: '<p>' + _('ms2_deliveries_intro') + '</p>'
                    , border: false
                    , bodyCssClass: 'panel-desc'
                    , bodyStyle: 'margin-bottom: 10px'
                }, {
                    xtype: 'minishop2-grid-delivery'
                }]
            }, {
                title: _('ms2_cities')
                , deferredRender: true
                , items: [{
                    html: '<p></p>'
                    , border: false
                    , bodyCssClass: 'panel-desc'
                    , bodyStyle: 'margin-bottom: 10px'
                }, {
                    xtype: 'minishop2-grid-city'
                }]
            }, {
                title: 'Нотификации'
                , deferredRender: true
                , items: [{
                    html: '<p></p>'
                    , border: false
                    , bodyCssClass: 'panel-desc'
                    , bodyStyle: 'margin-bottom: 10px'
                }, {
                    xtype: 'minishop2-grid-notifications'
                }]
            },
                {
                    title: _('ms2_payments')
                    , deferredRender: true
                    , items: [{
                        html: '<p>' + _('ms2_payments_intro') + '</p>'
                        , border: false
                        , bodyCssClass: 'panel-desc'
                        , bodyStyle: 'margin-bottom: 10px'
                    }, {
                        xtype: 'minishop2-grid-payment'
                    }]
                }, {
                    title: _('ms2_statuses')
                    , deferredRender: true
                    , items: [{
                        html: '<p>' + _('ms2_statuses_intro') + '</p>'
                        , border: false
                        , bodyCssClass: 'panel-desc'
                        , bodyStyle: 'margin-bottom: 10px'
                    }, {
                        xtype: 'minishop2-grid-status'
                    }]
                }, {
                    title: _('ms2_paymentstatuses')
                    , deferredRender: true
                    , items: [{
                        html: '<p>' + _('ms2_paymentstatuses_intro') + '</p>'
                        , border: false
                        , bodyCssClass: 'panel-desc'
                        , bodyStyle: 'margin-bottom: 10px'
                    }, {
                        //xtype: 'minishop2-grid-status'
                        xtype: 'minishop2-grid-payment-status'
                    }]
                }, {
                    title: _('ms2_vendors')
                    , deferredRender: true
                    , items: [{
                        html: '<p>' + _('ms2_vendors_intro') + '</p>'
                        , border: false
                        , bodyCssClass: 'panel-desc'
                        , bodyStyle: 'margin-bottom: 10px'
                    }, {
                        xtype: 'minishop2-grid-vendor'
                    }]
                }, {
                    title: _('ms2_attributes')
                    , deferredRender: true
                    , items: [{
                        html: '<p>' + _('ms2_attributes_intro') + '</p>'
                        , border: false
                        , bodyCssClass: 'panel-desc'
                        , bodyStyle: 'margin-bottom: 10px'
                    }, {
                        xtype: 'minishop2-grid-attribute'
                    }]
                }, {
                    title: _('ms2_attribute_groups')
                    , deferredRender: true
                    , items: [{
                        html: '<p>' + _('ms2_attribute_groups_intro') + '</p>'
                        , border: false
                        , bodyCssClass: 'panel-desc'
                        , bodyStyle: 'margin-bottom: 10px'
                    }, {
                        xtype: 'minishop2-grid-attribute_group'
                    }]
                }, {
                    title: _('ms2_breeds')
                    , deferredRender: true
                    , items: [{
                        html: '<p>' + _('ms2_breeds_intro') + '</p>'
                        , border: false
                        , bodyCssClass: 'panel-desc'
                        , bodyStyle: 'margin-bottom: 10px'
                    }, {
                        xtype: 'minishop2-grid-breed'
                    }]
                }, {
                    title: _('ms2_suppliers')
                    , deferredRender: true
                    , items: [{
                        html: '<p>' + _('ms2_suppliers_intro') + '</p>'
                        , border: false
                        , bodyCssClass: 'panel-desc'
                        , bodyStyle: 'margin-bottom: 10px'
                    }, {
                        xtype: 'minishop2-grid-suppliers'
                    }]
                }, {
                    title: _('ms2_pet_types')
                    , deferredRender: true
                    , items: [{
                        html: '<p>' + _('ms2_pet_types_intro') + '</p>'
                        , border: false
                        , bodyCssClass: 'panel-desc'
                        , bodyStyle: 'margin-bottom: 10px'
                    }, {
                        xtype: 'minishop2-grid-pet_type'
                    }]
                }, {
                    title: _('ms2_links')
                    , deferredRender: true
                    , items: [{
                        html: '<p>' + _('ms2_links_intro') + '</p>'
                        , border: false
                        , bodyCssClass: 'panel-desc'
                        , bodyStyle: 'margin-bottom: 10px'
                    }, {
                        xtype: 'minishop2-grid-link'
                    }]
                }, {
                    title: _('ms2_discount')
                    , deferredRender: true
                    , items: [{
                        html: '<p>' + _('ms2_discount_intro') + '</p>'
                        , border: false
                        , bodyCssClass: 'panel-desc'
                        , bodyStyle: 'margin-bottom: 10px'
                    }, {
                        xtype: 'minishop2-grid-user_discount'
                    }]
                }]
        }]
    });
    miniShop2.panel.Settings.superclass.constructor.call(this, config);
};
Ext.extend(miniShop2.panel.Settings, MODx.Panel);
Ext.reg('minishop2-panel-settings', miniShop2.panel.Settings);