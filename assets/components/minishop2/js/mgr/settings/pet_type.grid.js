miniShop2.grid.PetType = function(config) {
	config = config || {};

	this.exp = new Ext.grid.RowExpander({
		expandOnDblClick: false
		,tpl : new Ext.Template('<p class="desc">{description}</p>')
		,renderer : function(v, p, record){return record.data.description != '' && record.data.description != null ? '<div class="x-grid3-row-expander">&#160;</div>' : '&#160;';}
	});

	Ext.applyIf(config,{
		id: 'minishop2-grid-pet_type'
		,url: miniShop2.config.connector_url
		,baseParams: {
			action: 'mgr/settings/pet_type/getlist'
		}
		,fields: ['id','name']
		,autoHeight: true
		,paging: true
		,remoteSort: true
		,save_action: 'mgr/settings/pet_type/updatefromgrid'
		,autosave: true
		,plugins: this.exp
		,columns: [this.exp
			,{header: _('ms2_id'),dataIndex: 'id',width: 50, sortable: true}
			,{header: _('ms2_name'),dataIndex: 'name',width: 100, editor: {xtype: 'textfield', allowBlank: false}, sortable: true}
		]
		,tbar: [{
			text: _('ms2_btn_create')
			,handler: this.createPetType
			,scope: this
		}]
	});
	miniShop2.grid.PetType.superclass.constructor.call(this,config);
};
Ext.extend(miniShop2.grid.PetType,MODx.grid.Grid,{
	windows: {}

	,getMenu: function() {
		var m = [];
		m.push({
			text: _('ms2_menu_update')
			,handler: this.updatePetType
		});
		m.push('-');
		m.push({
			text: _('ms2_menu_remove')
			,handler: this.removePetType
		});
		this.addContextMenuItem(m);
	}

	,createPetType: function(btn,e) {
		if (!this.windows.createPetType) {
			this.windows.createPetType = MODx.load({
				xtype: 'minishop2-window-pet_type-create'
				,fields: this.getPetTypeFields('create')
				,listeners: {
					success: {fn:function() { this.refresh(); },scope:this}
				}
			});
		}
		this.windows.createPetType.fp.getForm().reset();
		this.windows.createPetType.show(e.target);
	}

	,updatePetType: function(btn,e) {
		if (!this.menu.record || !this.menu.record.id) return false;
		var r = this.menu.record;

		if (!this.windows.updatePetType) {
			this.windows.updatePetType = MODx.load({
				xtype: 'minishop2-window-pet_type-update'
				,record: r
				,fields: this.getPetTypeFields('update')
				,listeners: {
					success: {fn:function() { this.refresh(); },scope:this}
				}
			});
		}
		this.windows.updatePetType.fp.getForm().reset();
		this.windows.updatePetType.fp.getForm().setValues(r);
		this.windows.updatePetType.show(e.target);
	}

	,removePetType: function(btn,e) {
		if (!this.menu.record) return false;

		MODx.msg.confirm({
			title: _('ms2_menu_remove') + '"' + this.menu.record.name + '"'
			,text: _('ms2_menu_remove_confirm')
			,url: this.config.url
			,params: {
				action: 'mgr/settings/pet_type/remove'
				,id: this.menu.record.id
			}
			,listeners: {
				success: {fn:function(r) {this.refresh();}, scope:this}
			}
		});
	}

	,getPetTypeFields: function(type) {
		return [
			{xtype: 'hidden',name: 'id', id: 'minishop2-pet_type-id-'+type}
			,{xtype: 'textfield',fieldLabel: _('ms2_name'), name: 'name', allowBlank: false, anchor: '99%', id: 'minishop2-pet_type-name-'+type}
		];
	}

});
Ext.reg('minishop2-grid-pet_type',miniShop2.grid.PetType);




miniShop2.window.CreatePetType = function(config) {
	config = config || {};
	this.ident = config.ident || 'mecitem'+Ext.id();
	Ext.applyIf(config,{
		title: _('ms2_menu_create')
		,id: this.ident
		,width: 600
		,autoHeight: true
		,labelAlign: 'left'
		,labelWidth: 180
		,url: miniShop2.config.connector_url
		,action: 'mgr/settings/pet_type/create'
		,fields: config.fields
		,keys: [{key: Ext.EventObject.ENTER,shift: true,fn: function() {this.submit() },scope: this}]
	});
	miniShop2.window.CreatePetType.superclass.constructor.call(this,config);
};
Ext.extend(miniShop2.window.CreatePetType,MODx.Window);
Ext.reg('minishop2-window-pet_type-create',miniShop2.window.CreatePetType);


miniShop2.window.UpdatePetType = function(config) {
	config = config || {};
	this.ident = config.ident || 'meuitem'+Ext.id();
	Ext.applyIf(config,{
		title: _('ms2_menu_update')
		,id: this.ident
		,width: 600
		,autoHeight: true
		,labelAlign: 'left'
		,labelWidth: 180
		,url: miniShop2.config.connector_url
		,action: 'mgr/settings/pet_type/update'
		,fields: config.fields
		,keys: [{key: Ext.EventObject.ENTER,shift: true,fn: function() {this.submit() },scope: this}]
	});
	miniShop2.window.UpdatePetType.superclass.constructor.call(this,config);
};
Ext.extend(miniShop2.window.UpdatePetType,MODx.Window);
Ext.reg('minishop2-window-pet_type-update',miniShop2.window.UpdatePetType);
