miniShop2.grid.Suppliers = function(config) {
	config = config || {};

	this.exp = new Ext.grid.RowExpander({
		expandOnDblClick: false
		,tpl : new Ext.Template('<p class="desc">{description}</p>')
		,renderer : function(v, p, record){return record.data.description != '' && record.data.description != null ? '<div class="x-grid3-row-expander">&#160;</div>' : '&#160;';}
	});

	Ext.applyIf(config,{
		id: 'minishop2-grid-suppliers'
		,url: miniShop2.config.connector_url
		,baseParams: {
			action: 'mgr/settings/suppliers/getlist'
		}
		,fields: ['id','name','code','status']
		,autoHeight: true
		,paging: true
		,remoteSort: true
		,save_action: 'mgr/settings/suppliers/updatefromgrid'
		,autosave: true
		,plugins: this.exp
		,columns: [this.exp
			,{header: _('ms2_id'),dataIndex: 'id',width: 50, sortable: true}
			,{header: _('ms2_name'),dataIndex: 'name',width: 100, editor: {xtype: 'textfield', allowBlank: false}, sortable: true}
			,{header: _('ms2_code'),dataIndex: 'code',width: 100, editor: {xtype: 'textfield', allowBlank: false}, sortable: true}
            ,{header: _('ms2_active'),dataIndex: 'status',width: 50, editor: {xtype: 'combo-boolean', renderer: 'boolean'}}
            //,{header: _('ms2_status'),dataIndex: 'status',width: 100, editor: {xtype: 'textfield', allowBlank: false}, sortable: true}
		]
		,tbar: [{
			text: _('ms2_btn_create')
			,handler: this.createSuppliers
			,scope: this
		}]
	});
	miniShop2.grid.Suppliers.superclass.constructor.call(this,config);
};
Ext.extend(miniShop2.grid.Suppliers,MODx.grid.Grid,{
	windows: {}

	,getMenu: function() {
		var m = [];
		m.push({
			text: _('ms2_menu_update')
			,handler: this.updateSuppliers
		});
		m.push('-');
		m.push({
			text: _('ms2_menu_remove')
			,handler: this.removeSuppliers
		});
		this.addContextMenuItem(m);
	}

	,createSuppliers: function(btn,e) {
		if (!this.windows.createSuppliers) {
			this.windows.createSuppliers = MODx.load({
				xtype: 'minishop2-window-suppliers-create'
				,fields: this.getSuppliersFields('create')
				,listeners: {
					success: {fn:function() { this.refresh(); },scope:this}
				}
			});
		}
		this.windows.createSuppliers.fp.getForm().reset();
		this.windows.createSuppliers.show(e.target);
	}

	,updateSuppliers: function(btn,e) {
		if (!this.menu.record || !this.menu.record.id) return false;
		var r = this.menu.record;

		if (!this.windows.updateSuppliers) {
			this.windows.updateSuppliers = MODx.load({
				xtype: 'minishop2-window-suppliers-update'
				,record: r
				,fields: this.getSuppliersFields('update')
				,listeners: {
					success: {fn:function() { this.refresh(); },scope:this}
				}
			});
		}
		this.windows.updateSuppliers.fp.getForm().reset();
		this.windows.updateSuppliers.fp.getForm().setValues(r);
		this.windows.updateSuppliers.show(e.target);
	}

	,removeSuppliers: function(btn,e) {
		if (!this.menu.record) return false;

		MODx.msg.confirm({
			title: _('ms2_menu_remove') + '"' + this.menu.record.name + '"'
			,text: _('ms2_menu_remove_confirm')
			,url: this.config.url
			,params: {
				action: 'mgr/settings/suppliers/remove'
				,id: this.menu.record.id
			}
			,listeners: {
				success: {fn:function(r) {this.refresh();}, scope:this}
			}
		});
	}

	,getSuppliersFields: function(type) {
		return [
			{xtype: 'hidden',name: 'id', id: 'minishop2-suppliers-id-'+type}
			,{xtype: 'textfield',fieldLabel: _('ms2_name'), name: 'name', allowBlank: false, anchor: '99%', id: 'minishop2-suppliers-name-'+type}
            //,{xtype: 'xcheckbox', fieldLabel: '', boxLabel: _('ms2_active'), name: 'active', id: 'minishop2-suppliers-active-'+type}
            ,{xtype: 'textfield',fieldLabel: 'Код', name: 'code', allowBlank: false, anchor: '99%', id: 'minishop2-suppliers-code-'+type}
            //,{header: _('ms2_active'),dataIndex: 'status',width: 50, editor: {xtype: 'combo-boolean', renderer: 'boolean'}}
            ,{xtype: 'xcheckbox', fieldLabel: '', boxLabel: _('ms2_active'), name: 'status', id: 'minishop2-suppliers-status-'+type}
			//,{xtype: 'minishop2-combo-status',fieldLabel: _('ms2_active'), name: 'status', anchor: '99%', id: 'minishop2-suppliers-status-'+type}


		];
	}

});
Ext.reg('minishop2-grid-suppliers',miniShop2.grid.Suppliers);




miniShop2.window.CreateSuppliers = function(config) {
	config = config || {};
	this.ident = config.ident || 'mecitem'+Ext.id();
	Ext.applyIf(config,{
		title: _('ms2_menu_create')
		,id: this.ident
		,width: 600
		,autoHeight: true
		,labelAlign: 'left'
		,labelWidth: 180
		,url: miniShop2.config.connector_url
		,action: 'mgr/settings/suppliers/create'
		,fields: config.fields
		,keys: [{key: Ext.EventObject.ENTER,shift: true,fn: function() {this.submit() },scope: this}]
	});
	miniShop2.window.CreateSuppliers.superclass.constructor.call(this,config);
};
Ext.extend(miniShop2.window.CreateSuppliers,MODx.Window);
Ext.reg('minishop2-window-suppliers-create',miniShop2.window.CreateSuppliers);


miniShop2.window.UpdateSuppliers = function(config) {
	config = config || {};
	this.ident = config.ident || 'meuitem'+Ext.id();
	console.log(config.fields);
	Ext.applyIf(config,{
		title: _('ms2_menu_update')
		,id: this.ident
		,width: 600
		,autoHeight: true
		,labelAlign: 'left'
		,labelWidth: 180
		,url: miniShop2.config.connector_url
		,action: 'mgr/settings/suppliers/update'
		,fields: config.fields
		,keys: [{key: Ext.EventObject.ENTER,shift: true,fn: function() {this.submit() },scope: this}]
	});
	miniShop2.window.UpdateSuppliers.superclass.constructor.call(this,config);
};
Ext.extend(miniShop2.window.UpdateSuppliers,MODx.Window);
Ext.reg('minishop2-window-suppliers-update',miniShop2.window.UpdateSuppliers);
