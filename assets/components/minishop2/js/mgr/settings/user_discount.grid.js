miniShop2.grid.UserDiscount = function(config) {
	config = config || {};

	this.exp = new Ext.grid.RowExpander({
		expandOnDblClick: false
		,tpl : new Ext.Template('<p class="desc">{description}</p>')
		,renderer : function(v, p, record){return record.data.description != '' && record.data.description != null ? '<div class="x-grid3-row-expander">&#160;</div>' : '&#160;';}
	});

	Ext.applyIf(config,{
		id: 'minishop2-grid-user_discount'
		,url: miniShop2.config.connector_url
		,baseParams: {
			action: 'mgr/settings/user_discount/getlist'
		}
		,fields: ['id','percent','total']
		,autoHeight: true
		,paging: true
		,remoteSort: true
		,save_action: 'mgr/settings/user_discount/updatefromgrid'
		,autosave: true
		,plugins: this.exp
		,columns: [this.exp
			,{header: _('ms2_id'),dataIndex: 'id',width: 50, sortable: true}
			,{header: _('ms2_discount_percent'),dataIndex: 'percent',width: 100, editor: {xtype: 'textfield', allowBlank: false}, sortable: true}
			,{header: _('ms2_discount_total'),dataIndex: 'total',width: 100, editor: {xtype: 'textfield', allowBlank: false}, sortable: true}
		]
		,tbar: [{
			text: _('ms2_btn_create')
			,handler: this.createUserDiscount
			,scope: this
		}]
	});
	miniShop2.grid.UserDiscount.superclass.constructor.call(this,config);
};
Ext.extend(miniShop2.grid.UserDiscount,MODx.grid.Grid,{
	windows: {}

	,getMenu: function() {
		var m = [];
		m.push({
			text: _('ms2_menu_update')
			,handler: this.updateUserDiscount
		});
		m.push('-');
		m.push({
			text: _('ms2_menu_remove')
			,handler: this.removeUserDiscount
		});
		this.addContextMenuItem(m);
	}

	,createUserDiscount: function(btn,e) {
		if (!this.windows.createUserDiscount) {
			this.windows.createUserDiscount = MODx.load({
				xtype: 'minishop2-window-user_discount-create'
				,fields: this.getUserDiscountFields('create')
				,listeners: {
					success: {fn:function() { this.refresh(); },scope:this}
				}
			});
		}
		this.windows.createUserDiscount.fp.getForm().reset();
		this.windows.createUserDiscount.show(e.target);
	}

	,updateUserDiscount: function(btn,e) {
		if (!this.menu.record || !this.menu.record.id) return false;
		var r = this.menu.record;

		if (!this.windows.updateUserDiscount) {
			this.windows.updateUserDiscount = MODx.load({
				xtype: 'minishop2-window-user_discount-update'
				,record: r
				,fields: this.getUserDiscountFields('update')
				,listeners: {
					success: {fn:function() { this.refresh(); },scope:this}
				}
			});
		}
		this.windows.updateUserDiscount.fp.getForm().reset();
		this.windows.updateUserDiscount.fp.getForm().setValues(r);
		this.windows.updateUserDiscount.show(e.target);
	}

	,removeUserDiscount: function(btn,e) {
		if (!this.menu.record) return false;

		MODx.msg.confirm({
			title: _('ms2_menu_remove') + '"' + this.menu.record.name + '"'
			,text: _('ms2_menu_remove_confirm')
			,url: this.config.url
			,params: {
				action: 'mgr/settings/user_discount/remove'
				,id: this.menu.record.id
			}
			,listeners: {
				success: {fn:function(r) {this.refresh();}, scope:this}
			}
		});
	}

	,getUserDiscountFields: function(type) {
		return [
			{xtype: 'hidden',name: 'id', id: 'minishop2-user_discount-id-'+type}
			,{xtype: 'textfield',fieldLabel: _('ms2_discount_percent'), name: 'percent', allowBlank: false, anchor: '99%', id: 'minishop2-user_discount-percent-'+type}
			,{xtype: 'textfield',fieldLabel: _('ms2_discount_total'), name: 'total', allowBlank: false, anchor: '99%', id: 'minishop2-user_discount-total-'+type}
		];
	}

});
Ext.reg('minishop2-grid-user_discount',miniShop2.grid.UserDiscount);




miniShop2.window.CreateUserDiscount = function(config) {
	config = config || {};
	this.ident = config.ident || 'mecitem'+Ext.id();
	Ext.applyIf(config,{
		title: _('ms2_menu_create')
		,id: this.ident
		,width: 600
		,autoHeight: true
		,labelAlign: 'left'
		,labelWidth: 180
		,url: miniShop2.config.connector_url
		,action: 'mgr/settings/user_discount/create'
		,fields: config.fields
		,keys: [{key: Ext.EventObject.ENTER,shift: true,fn: function() {this.submit() },scope: this}]
	});
	miniShop2.window.CreateUserDiscount.superclass.constructor.call(this,config);
};
Ext.extend(miniShop2.window.CreateUserDiscount,MODx.Window);
Ext.reg('minishop2-window-user_discount-create',miniShop2.window.CreateUserDiscount);


miniShop2.window.UpdateUserDiscount = function(config) {
	config = config || {};
	this.ident = config.ident || 'meuitem'+Ext.id();
	Ext.applyIf(config,{
		title: _('ms2_menu_update')
		,id: this.ident
		,width: 600
		,autoHeight: true
		,labelAlign: 'left'
		,labelWidth: 180
		,url: miniShop2.config.connector_url
		,action: 'mgr/settings/user_discount/update'
		,fields: config.fields
		,keys: [{key: Ext.EventObject.ENTER,shift: true,fn: function() {this.submit() },scope: this}]
	});
	miniShop2.window.UpdateUserDiscount.superclass.constructor.call(this,config);
};
Ext.extend(miniShop2.window.UpdateUserDiscount,MODx.Window);
Ext.reg('minishop2-window-user_discount-update',miniShop2.window.UpdateUserDiscount);
