miniShop2.grid.PaymentStatus = function(config) {
	config = config || {};

	this.exp = new Ext.grid.RowExpander({
		expandOnDblClick: false
		,tpl : new Ext.Template('<p class="desc">{description}</p>')
		,renderer : function(v, p, record){return record.data.description != '' && record.data.description != null ? '<div class="x-grid3-row-expander">&#160;</div>' : '&#160;';}
	});
	this.dd = function(grid) {
		this.dropTarget = new Ext.dd.DropTarget(grid.container, {
			ddGroup : 'dd',
			copy:false,
			notifyDrop : function(dd, e, data) {
				var store = grid.store.data.items;
				var target = store[dd.getDragData(e).rowIndex].id;
				var source = store[data.rowIndex].id;
				if (target != source) {
					dd.el.mask(_('loading'),'x-mask-loading');
					MODx.Ajax.request({
						url: miniShop2.config.connector_url
						,params: {
							action: config.action || 'mgr/settings/paymentstatus/sort'
							,source: source
							,target: target
						}
						,listeners: {
							success: {fn:function(r) {dd.el.unmask();grid.refresh();},scope:grid}
							,failure: {fn:function(r) {dd.el.unmask();},scope:grid}
						}
					});
				}
			}
		});
	};

	Ext.applyIf(config,{
		id: 'minishop2-grid-payment-status'
		,url: miniShop2.config.connector_url
		,baseParams: {
			action: 'mgr/settings/paymentstatus/getlist'
		}
		,fields: ['id','name','description','active','color']
		,autoHeight: true
		,paging: true
		,remoteSort: true
		,save_action: 'mgr/settings/paymentstatus/updatefromgrid'
		,autosave: true
		,plugins: this.exp
		,columns: [this.exp
			,{header: _('ms2_id'),dataIndex: 'id',width: 50}
			,{header: _('ms2_name'),dataIndex: 'name',width: 150, editor: {xtype: 'textfield', allowBlank: false}}
			,{header: _('ms2_active'),dataIndex: 'active',width: 50, editor: {xtype: 'combo-boolean', renderer: 'boolean'}}
            ,{header: _('ms2_color'),dataIndex: 'color',renderer: this.renderColor, width: 50}
		]
		,tbar: [{
			text: _('ms2_btn_create')
			,handler: this.createPaymentStatus
			,scope: this
		}]
		,ddGroup: 'dd'
		,enableDragDrop: true
		,listeners: {render: {fn: this.dd, scope: this}}
	});
	miniShop2.grid.PaymentStatus.superclass.constructor.call(this,config);
};
Ext.extend(miniShop2.grid.PaymentStatus,MODx.grid.Grid,{
	windows: {}

	,getMenu: function() {
		var m = [];
		m.push({
			text: _('ms2_menu_update')
			,handler: this.updatePaymentStatus
		});
		if (this.menu.record.editable) {
			m.push('-');
			m.push({
				text: _('ms2_menu_remove')
				,handler: this.removePaymentStatus
			});
		}
		this.addContextMenuItem(m);
	}

	,renderColor: function(value) {
		return '<div style="width: 30px; height: 20px; border-radius: 3px; background: #' + value + '">&nbsp;</div>'
	}

	,renderBoolean: function(value) {
		if (value == 1) {return _('yes');}
		else {return _('no');}
	}

	,createPaymentStatus: function(btn,e) {
		if (!this.windows.createPaymentStatus) {
			this.windows.createPaymentStatus = MODx.load({
				xtype: 'minishop2-window-paymentstatus-create'
				,fields: this.getPaymentStatusFields('create')
				,listeners: {
					success: {fn:function() { this.refresh(); },scope:this}
				}
			});
		}
		this.windows.createPaymentStatus.fp.getForm().reset();
		this.windows.createPaymentStatus.fp.getForm().setValues({color:'000000'});
		this.windows.createPaymentStatus.show(e.target);
	}

	,updatePaymentStatus: function(btn,e) {
		if (!this.menu.record || !this.menu.record.id) return false;
		var r = this.menu.record;

		if (!this.windows.updatePaymentStatus) {
			this.windows.updatePaymentStatus = MODx.load({
				xtype: 'minishop2-window-paymentstatus-update'
				,record: r
				,fields: this.getPaymentStatusFields('update')
				,listeners: {
					success: {fn:function() { this.refresh(); },scope:this}
				}
			});
		}
		this.windows.updatePaymentStatus.fp.getForm().reset();
		this.windows.updatePaymentStatus.show(e.target);
		this.windows.updatePaymentStatus.fp.getForm().setValues(r);
	}

	,removePaymentStatus: function(btn,e) {
		if (!this.menu.record) return false;

		MODx.msg.confirm({
			title: _('ms2_menu_remove') + '"' + this.menu.record.name + '"'
			,text: _('ms2_menu_remove_confirm')
			,url: this.config.url
			,params: {
				action: 'mgr/settings/paymentstatus/remove'
				,id: this.menu.record.id
			}
			,listeners: {
				success: {fn:function(r) {this.refresh();}, scope:this}
			}
		});
	}

	,getPaymentStatusFields: function(type) {
		return [
			{xtype: 'hidden',name: 'id', id: 'minishop2-paymentstatus-id-'+type}
            ,{xtype: 'hidden',name: 'color',id: 'minishop2-newpaymentstatus-color-'+type}
            ,{xtype: 'colorpalette', fieldLabel: _('ms2_color'), id: 'minishop2-status-colorpalette-'+type
                ,listeners: {
                    select: function(palette, setColor) {
                        Ext.getCmp('minishop2-newpaymentstatus-color-'+type).setValue(setColor)
                    }
                    ,beforerender: function(palette) {
                        var color = Ext.getCmp('minishop2-newpaymentstatus-color-'+type).value;
                        if (color != 'undefined') {
                            palette.value = color;
                        }
                    }
                }
            }
			,{xtype: 'textfield',fieldLabel: _('ms2_name'), name: 'name', allowBlank: false, anchor: '99%', id: 'minishop2-paymentstatus-name-'+type}
			,{xtype: 'textarea', fieldLabel: _('ms2_description'), name: 'description', anchor: '99%', id: 'minishop2-paymentstatus-description-'+type}
			,{xtype: 'checkboxgroup'
				,fieldLabel: _('ms2_options')
				,columns: 1
				,items: [
					{xtype: 'xcheckbox', boxLabel: _('ms2_active'), name: 'active', id: 'minishop2-paymentstatus-active-'+type}
				]
			}
		];
	}

	,handleStatusFields: function(v) {

	}

	,beforeDestroy: function() {
		if (this.rendered) {
			this.dropTarget.destroy();
		}
		miniShop2.grid.PaymentStatus.superclass.beforeDestroy.call(this);
	}
});
Ext.reg('minishop2-grid-payment-status',miniShop2.grid.PaymentStatus);




miniShop2.window.CreatePaymentStatus = function(config) {
	config = config || {};
	this.ident = config.ident || 'mecitem'+Ext.id();
	Ext.applyIf(config,{
		title: _('ms2_menu_create')
		,id: this.ident
		,width: 600
		,autoHeight: true
		,labelAlign: 'left'
		,labelWidth: 180
		,url: miniShop2.config.connector_url
		,action: 'mgr/settings/paymentstatus/create'
		,fields: config.fields
		,keys: [{key: Ext.EventObject.ENTER,shift: true,fn: function() {this.submit() },scope: this}]
	});
	miniShop2.window.CreatePaymentStatus.superclass.constructor.call(this,config);
};
Ext.extend(miniShop2.window.CreatePaymentStatus,MODx.Window);
Ext.reg('minishop2-window-paymentstatus-create',miniShop2.window.CreatePaymentStatus);


miniShop2.window.UpdatePaymentStatus = function(config) {
	config = config || {};
	this.ident = config.ident || 'meuitem'+Ext.id();
	Ext.applyIf(config,{
		title: _('ms2_menu_update')
		,id: this.ident
		,width: 600
		,autoHeight: true
		,labelAlign: 'left'
		,labelWidth: 180
		,url: miniShop2.config.connector_url
		,action: 'mgr/settings/paymentstatus/update'
		,fields: config.fields
		,keys: [{key: Ext.EventObject.ENTER,shift: true,fn: function() {this.submit() },scope: this}]
	});
	miniShop2.window.UpdatePaymentStatus.superclass.constructor.call(this,config);
};
Ext.extend(miniShop2.window.UpdatePaymentStatus,MODx.Window);
Ext.reg('minishop2-window-paymentstatus-update',miniShop2.window.UpdatePaymentStatus);