miniShop2.grid.Notifications = function (config) {
    config = config || {};

    this.exp = new Ext.grid.RowExpander({
        expandOnDblClick: false
        , tpl: new Ext.Template('<p class="desc">{description}</p>')
        , renderer: function (v, p, record) {
            return record.data.description != '' && record.data.description != null ? '<div class="x-grid3-row-expander">&#160;</div>' : '&#160;';
        }
    });
    this.dd = function (grid) {
        this.dropTarget = new Ext.dd.DropTarget(grid.container, {
            ddGroup: 'dd',
            copy: false,
            notifyDrop: function (dd, e, data) {
                var store = grid.store.data.items;
                var target = store[dd.getDragData(e).rowIndex].id;
                var source = store[data.rowIndex].id;
                if (target != source) {
                    dd.el.mask(_('loading'), 'x-mask-loading');
                    MODx.Ajax.request({
                        url: miniShop2.config.connector_url
                        , params: {
                            action: config.action || 'mgr/settings/notifications/sort'
                            , source: source
                            , target: target
                        }
                        , listeners: {
                            success: {
                                fn: function (r) {
                                    dd.el.unmask();
                                    grid.refresh();
                                }, scope: grid
                            }
                            , failure: {
                                fn: function (r) {
                                    dd.el.unmask();
                                }, scope: grid
                            }
                        }
                    });
                }
            }
        });
    };

    Ext.applyIf(config, {
        id: 'minishop2-grid-notifications'
        ,
        url: miniShop2.config.connector_url
        ,
        baseParams: {
            action: 'mgr/settings/notifications/getlist'
        }
        ,
        fields: ['id', 'name', 'type', 'viber', 'email', 'sms', 'chank_email',
            'text_viber', 'text_sms', 'caption', 'action_button',
            'email_subject',
            'is_transactional',
            'description','limit','counter',
            'image_include', 'image_url']
        ,
        autoHeight: true
        ,
        paging: true
        ,
        remoteSort: true
        ,
        save_action: 'mgr/settings/notifications/updatefromgrid'
        ,
        autosave: true
        ,
        plugins: this.exp
        ,
        columns: [this.exp
            , {header: _('ms2_id'), dataIndex: 'id', width: 50}
            , {header: _('ms2_name'), dataIndex: 'name', width: 100, editor: {xtype: 'textfield', allowBlank: false}}
            // , {header: 'Тип', dataIndex: 'type', width: 100, editor: {xtype: 'textfield', allowBlank: false}}
            , {header: 'Viber', dataIndex: 'viber', width: 100, editor: {xtype: 'textfield', allowBlank: false}}
            , {header: 'Sms', dataIndex: 'sms', width: 100, editor: {xtype: 'textfield', allowBlank: false}}
            , {header: 'Email', dataIndex: 'email', width: 100, editor: {xtype: 'textfield', allowBlank: false}}
            // ,{header: _('ms2_add_cost'),dataIndex: 'price',width: 50, editor: {xtype: 'textfield'}}
            // ,{header: _('ms2_logo'),dataIndex: 'logo',width: 75, renderer: this.renderLogo}
            // ,{header: _('ms2_active'),dataIndex: 'active',width: 50, editor: {xtype: 'combo-boolean', renderer: 'boolean'}}
            // ,{header: _('ms2_class'),dataIndex: 'class',width: 75, editor: {xtype: 'textfield'}}
        ]
        ,
        tbar: [{
            text: _('ms2_btn_create')
            , handler: this.createNotifications
            , scope: this
        }]
        ,
        ddGroup: 'dd'
        ,
        enableDragDrop: true
        ,
        listeners: {render: {fn: this.dd, scope: this}}
    });
    miniShop2.grid.Notifications.superclass.constructor.call(this, config);

    this.store.on('load', function (store) {
        miniShop2.NotificationsArray = [];
        var items = store.data.items;
        for (i in items) {
            if (items.hasOwnProperty(i)) {
                miniShop2.NotificationsArray.push({id: items[i].id, name: items[i].data.name})
            }
        }
    })
};
Ext.extend(miniShop2.grid.Notifications, MODx.grid.Grid, {
    windows: {}

    , getMenu: function () {
        var m = [];
        m.push({
            text: _('ms2_menu_update')
            , handler: this.updateNotifications
        });
        m.push('-');
        m.push({
            text: _('ms2_menu_remove')
            , handler: this.removeNotifications
        });
        this.addContextMenuItem(m);
    }

    , renderLogo: function (value) {
        if (/(jpg|png|gif|jpeg)$/i.test(value)) {
            if (!/^\//.test(value)) {
                value = '/' + value;
            }
            return '<img src="' + value + '" height="35" />';
        }
        else {
            return '';
        }
    }

    , createNotifications: function (btn, e) {
        if (!this.windows.createNotifications) {
            this.windows.createNotifications = MODx.load({
                xtype: 'minishop2-window-notifications-create'
                , fields: this.getNotificationsFields('create')
                , listeners: {
                    success: {
                        fn: function () {
                            this.refresh();
                        }, scope: this
                    }
                }
            });
        }
        this.windows.createNotifications.fp.getForm().reset();
        this.windows.createNotifications.show(e.target);
    }
    , updateNotifications: function (btn, e) {
        if (!this.menu.record || !this.menu.record.id) return false;
        var r = this.menu.record;

        if (!this.windows.updateNotifications) {
            this.windows.updateNotifications = MODx.load({
                xtype: 'minishop2-window-notifications-update'
                , record: r
                , fields: this.getNotificationsFields('update')
                , listeners: {
                    success: {
                        fn: function () {
                            this.refresh();
                        }, scope: this
                    }
                }
            });
        }
        this.windows.updateNotifications.fp.getForm().reset();
        this.windows.updateNotifications.fp.getForm().setValues(r);
        this.windows.updateNotifications.show(e.target);
    }

    , removeNotifications: function (btn, e) {
        if (!this.menu.record) return false;

        MODx.msg.confirm({
            title: _('ms2_menu_remove') + '"' + this.menu.record.name + '"'
            , text: _('ms2_menu_remove_confirm')
            , url: this.config.url
            , params: {
                action: 'mgr/settings/notifications/remove'
                , id: this.menu.record.id
            }
            , listeners: {
                success: {
                    fn: function (r) {
                        this.refresh();
                    }, scope: this
                }
            }
        });
    }

    , getNotificationsFields: function (type) {
        return [
            {
                layout: 'column'
                , defaults: {msgTarget: 'under', border: false}
                , style: 'padding:5px 0px;'
                , items: [
                    {
                        columnWidth: .48
                        , layout: 'form'
                        , border: false
                        , anchor: '100%'
                        , style: 'padding:15px 0px 15px 15px;'
                        , items: [
                            {xtype: 'hidden', name: 'id', id: 'minishop2-notifications-id-' + type}
                            , {
                                xtype: 'minishop2-combo-notifications_type',
                                fieldLabel: 'Тип',
                                name: 'type',
                                allowBlank: false,
                                anchor: '99%',
                                id: 'minishop2-notifications-type-' + type
                            }
                            , {
                                xtype: 'textfield',
                                fieldLabel: _('ms2_name'),
                                name: 'name',
                                allowBlank: false,
                                anchor: '99%',
                                id: 'minishop2-notifications-name-' + type
                            }
                            , {
                                xtype: 'displayfield',
                                fieldLabel: 'Описание',
                                name: 'description',
                                allowBlank: false,
                                anchor: '99%',
                                id: 'minishop2-notifications-description-' + type
                            }
                            , {
                                xtype: 'textfield',
                                fieldLabel: 'Лимит на день (Вайбер)',
                                name: 'limit',
                                allowBlank: false,
                                anchor: '99%',
                                id: 'minishop2-notifications-limit-' + type
                            }
                            , {
                                xtype: 'displayfield',
                                fieldLabel: 'Отправлено за день (Вайбер)',
                                name: 'counter',
                                allowBlank: false,
                                anchor: '99%',
                                id: 'minishop2-notifications-counter-' + type
                            },
                            {
                                xtype: 'combo-boolean',
                                fieldLabel: 'SMS',
                                name: 'sms',
                                anchor: '99%',
                                id: 'minishop2-notifications-sms-' + type,
                                renderer: 'boolean'
                            }
                            , {
                                xtype: 'textarea',
                                fieldLabel: 'Текст sms',
                                name: 'text_sms',
                                anchor: '99%',
                                id: 'minishop2-notifications-text_sms-' + type
                            }

                        ]
                    },
                    {
                        columnWidth: .48
                        , layout: 'form'
                        , border: false
                        , anchor: '100%'
                        , style: 'padding:15px 0px;'
                        , items: [
                            {
                                xtype: 'combo-boolean',
                                fieldLabel: 'Email',
                                anchor: '99%',
                                name: 'email',
                                id: 'minishop2-notifications-email-' + type,
                                renderer: 'boolean'
                            }
                            , {
                                xtype: 'minishop2-combo-chank_email',
                                fieldLabel: 'Чанк для email',
                                name: 'chank_email',
                                allowBlank: true,
                                anchor: '99%',
                                id: 'minishop2-notifications-chank_email-' + type
                            }
                            , {
                                xtype: 'textfield',
                                fieldLabel: 'Тема письма',
                                name: 'email_subject',
                                allowBlank: true,
                                anchor: '99%',
                                id: 'minishop2-notifications-email_subject-' + type
                            }

                        ]
                    },
                    {
                        columnWidth: .48
                        , layout: 'form'
                        , border: false
                        , style: 'padding:15px 0px;'
                        , anchor: '100%'
                        , items: [
                            {
                                xtype: 'combo-boolean',
                                fieldLabel: 'Viber',
                                name: 'viber',
                                anchor: '99%',
                                id: 'minishop2-notifications-viber-' + type,
                                renderer: 'boolean'
                            }
                            , {
                                xtype: 'textarea',
                                fieldLabel: 'Текст вайбер',
                                name: 'text_viber',
                                anchor: '99%',
                                id: 'minishop2-notifications-text_viber-' + type
                            }
                            , {
                                xtype: 'textfield',
                                fieldLabel: 'Заголовок кнопки',
                                name: 'caption',
                                allowBlank: true,
                                anchor: '99%',
                                id: 'minishop2-notifications-caption-' + type
                            }
                            , {
                                xtype: 'textfield',
                                fieldLabel: 'Ссылка кнопки',
                                name: 'action_button',
                                allowBlank: true,
                                anchor: '99%',
                                id: 'minishop2-notifications-action_button-' + type
                            }
                            ,{
                                xtype: 'combo-boolean',
                                fieldLabel: 'Добавлять картинку',
                                name: 'image_include',
                                anchor: '99%',
                                id: 'minishop2-notifications-image_include-' + type,
                                renderer: 'boolean'
                            }
                            ,{
                                xtype: 'combo-boolean',
                                fieldLabel: 'Is transactional',
                                name: 'is_transactional',
                                anchor: '99%',
                                id: 'minishop2-notifications-is_transactional-' + type,
                                renderer: 'boolean'
                            }
                            ,{
                                xtype: 'textfield',
                                fieldLabel: 'Ссылка на картинку',
                                name: 'image_url',
                                allowBlank: true,
                                anchor: '99%',
                                id: 'minishop2-notifications-image_url-' + type
                            }
                        ]
                    },
                    // {
                    //     columnWidth: .48
                    //     , layout: 'form'
                    //     , border: false
                    //     , style: 'padding:15px 0px;'
                    //     , anchor: '100%'
                    //     , items: [
                    //
                    //     ]
                    // }

                ]
            }];
    }

    , beforeDestroy: function () {
        if (this.rendered) {
            this.dropTarget.destroy();
        }
        miniShop2.grid.Notifications.superclass.beforeDestroy.call(this);
    }
});
Ext.reg('minishop2-grid-notifications', miniShop2.grid.Notifications);


miniShop2.window.CreateNotifications = function (config) {
    config = config || {};
    this.ident = config.ident || 'mecitem' + Ext.id();
    Ext.applyIf(config, {
        title: _('ms2_menu_create')
        , width: '80%'
        , autoHeight: true
        , labelAlign: 'left'
        , labelWidth: 180
        , url: miniShop2.config.connector_url
        , action: 'mgr/settings/notifications/create'
        , fields: config.fields
        , keys: [{
            key: Ext.EventObject.ENTER, shift: true, fn: function () {
                this.submit()
            }, scope: this
        }]
    });
    miniShop2.window.CreateNotifications.superclass.constructor.call(this, config);
};
Ext.extend(miniShop2.window.CreateNotifications, MODx.Window);
Ext.reg('minishop2-window-notifications-create', miniShop2.window.CreateNotifications);

miniShop2.window.UpdateNotifications = function (config) {
    config = config || {};
    this.ident = config.ident || 'meuitem' + Ext.id();
    console.log(this.ident);
    Ext.applyIf(config, {
        title: _('ms2_menu_update')
        , id: this.ident
        , width: '80%'
        , autoHeight: true
        , labelAlign: 'left'
        , labelWidth: 180
        , url: miniShop2.config.connector_url
        , action: 'mgr/settings/notifications/update'
        , fields: config.fields
        , keys: [{
            key: Ext.EventObject.ENTER, shift: true, fn: function () {
                this.submit()
            }, scope: this
        }]
    });
    miniShop2.window.UpdateNotifications.superclass.constructor.call(this, config);
};
Ext.extend(miniShop2.window.UpdateNotifications, MODx.Window);
Ext.reg('minishop2-window-notifications-update', miniShop2.window.UpdateNotifications);