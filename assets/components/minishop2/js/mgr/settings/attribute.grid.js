miniShop2.grid.Attribute = function(config) {
	config = config || {};

	this.exp = new Ext.grid.RowExpander({
		expandOnDblClick: false
		,tpl : new Ext.Template('<p class="desc">{description}</p>')
		,renderer : function(v, p, record){return record.data.description != '' && record.data.description != null ? '<div class="x-grid3-row-expander">&#160;</div>' : '&#160;';}
	});

	Ext.applyIf(config,{
		id: 'minishop2-grid-attribute'
		,url: miniShop2.config.connector_url
		,baseParams: {
			action: 'mgr/settings/attribute/getlist'
		}
		,fields: ['id','name','name_ua','attribute_group','key','sort','sort_related','sef','meta_title','meta_description','h1','meta_title_ua','meta_description_ua','h1_ua']
		,autoHeight: true
		,paging: true
		,remoteSort: true
		,save_action: 'mgr/settings/attribute/updatefromgrid'
		,autosave: true
		,plugins: this.exp
		,columns: [this.exp
			,{header: _('ms2_id'),dataIndex: 'id',width: 50, sortable: true}
			,{header: _('ms2_name'),dataIndex: 'name',width: 100, editor: {xtype: 'textfield', allowBlank: false}, sortable: true}
            ,{header: 'Имя на укр',dataIndex: 'name_ua',width: 100, editor: {xtype: 'textfield', allowBlank: false}, sortable: true}
			,{header: _('ms2_attribute_groups'),dataIndex: 'attribute_group',width: 100, editor: {xtype: 'minishop2-combo-attribute_group'}, sortable: true}
			,{header: 'Ключ',dataIndex: 'key',width: 100, editor: {xtype: 'textfield'}, sortable: true}
            ,{header: 'ЧПУ',dataIndex: 'sef',width: 100, editor: {xtype: 'combo-boolean', renderer: 'boolean'},  sortable: true}
            ,{header: 'Title',dataIndex: 'meta_title',width: 100, editor: {xtype: 'textfield'}, sortable: true}
            ,{header: 'Meta Description',dataIndex: 'meta_description',width: 100, editor: {xtype: 'textfield'}, sortable: true}
            ,{header: 'H1',dataIndex: 'h1',width: 100, editor: {xtype: 'textfield'}, sortable: true}

            ,{header: 'Title укр',dataIndex: 'meta_title_ua',width: 100, editor: {xtype: 'textfield'}, sortable: true}
            ,{header: 'Meta Description укр',dataIndex: 'meta_description_ua',width: 100, editor: {xtype: 'textfield'}, sortable: true}
            ,{header: 'H1 укр',dataIndex: 'h1_ua',width: 100, editor: {xtype: 'textfield'}, sortable: true}
			,{header: 'Порядок',dataIndex: 'sort',width: 100, editor: {xtype: 'textfield'}, sortable: true}
            ,{header: 'Порядок для похожих',dataIndex: 'sort_related',width: 100, editor: {xtype: 'combo-boolean', renderer: 'boolean'}, sortable: true}
		]
		,ddGroup: 'dd'
		,enableDragDrop: true
		,listeners: {
			render: {fn: this._initDD, scope: this}
		}
		,tbar: [{
			text: _('ms2_btn_create')
			,handler: this.createAttribute
			,scope: this
		}]
	});
	
	miniShop2.grid.Attribute.superclass.constructor.call(this,config);
};
Ext.extend(miniShop2.grid.Attribute,MODx.grid.Grid,{
	windows: {}
,_initDD: function(grid) {

		new Ext.dd.DropTarget(grid.el, {
			ddGroup : 'dd',
			copy:false,
			notifyDrop : function(dd, e, data) {
				var store = grid.store.data.items;

				var target = store[dd.getDragData(e).rowIndex].data;

				var source = store[data.rowIndex].data;

				if ((target.parent == source.parent) && (target.id != source.id)) {

					dd.el.mask(_('loading'),'x-mask-loading');

					MODx.Ajax.request({

						url: miniShop2.config.connector_url

						,params: {

							//action: 'mgr/product/sort'
							action: 'mgr/settings/attribute/sortatr'

							,source: source.id

							,target: target.id

						}

						,listeners: {

							success: {fn:function(r) {dd.el.unmask();grid.refresh();},scope:grid}

							,failure: {fn:function(r) {dd.el.unmask();},scope:grid}

						}

					});

				}

			}

		});

	}
	,getMenu: function() {
		var m = [];
		m.push({
			text: _('ms2_menu_update')
			,handler: this.updateAttribute
		});
		m.push('-');
		m.push({
			text: _('ms2_menu_remove')
			,handler: this.removeAttribute
		});
		this.addContextMenuItem(m);
	}

	,createAttribute: function(btn,e) {
		if (!this.windows.createAttribute) {
			this.windows.createAttribute = MODx.load({
				xtype: 'minishop2-window-attribute-create'
				,fields: this.getAttributeFields('create')
				,listeners: {
					success: {fn:function() { this.refresh(); },scope:this}
				}
			});
		}
		this.windows.createAttribute.fp.getForm().reset();
		this.windows.createAttribute.show(e.target);
	}

	,updateAttribute: function(btn,e) {
		if (!this.menu.record || !this.menu.record.id) return false;
		var r = this.menu.record;

		if (!this.windows.updateAttribute) {
			this.windows.updateAttribute = MODx.load({
				xtype: 'minishop2-window-attribute-update'
				,record: r
				,fields: this.getAttributeFields('update')
				,listeners: {
					success: {fn:function() { this.refresh(); },scope:this}
				}
			});
		}
		this.windows.updateAttribute.fp.getForm().reset();
		this.windows.updateAttribute.fp.getForm().setValues(r);
		this.windows.updateAttribute.show(e.target);
	}

	,removeAttribute: function(btn,e) {
		if (!this.menu.record) return false;

		MODx.msg.confirm({
			title: _('ms2_menu_remove') + '"' + this.menu.record.name + '"'
			,text: _('ms2_menu_remove_confirm')
			,url: this.config.url
			,params: {
				action: 'mgr/settings/attribute/remove'
				,id: this.menu.record.id
			}
			,listeners: {
				success: {fn:function(r) {this.refresh();}, scope:this}
			}
		});
	}

	,getAttributeFields: function(type) {
		return [
			{xtype: 'hidden',name: 'id', id: 'minishop2-attribute-id-'+type}
			,{xtype: 'textfield',fieldLabel: _('ms2_name'), name: 'name', allowBlank: false, anchor: '99%', id: 'minishop2-attribute-name-'+type}
            ,{xtype: 'textfield',fieldLabel: 'Имя ukr', name: 'name_ua', allowBlank: false, anchor: '99%', id: 'minishop2-attribute-name_ua-'+type}
			,{xtype: 'minishop2-combo-attribute_group',fieldLabel: _('ms2_attribute_groups'), name: 'attribute_group', anchor: '99%', id: 'minishop2-attribute-attribute_group-'+type}
			,{xtype: 'textfield',fieldLabel: 'Порядок', name: 'sort', allowBlank: false, anchor: '99%', id: 'minishop2-attribute-sort-'+type}
            ,{xtype: 'combo-boolean',fieldLabel: 'Порядок для Похожих', name: 'sort_related', allowBlank: false, anchor: '99%', id: 'minishop2-attribute-sortrelated-'+type, renderer: 'boolean'}

            ,{xtype: 'textfield',fieldLabel: 'Ключ', name: 'key', allowBlank: false, anchor: '99%', id: 'minishop2-attribute-key-'+type}
            ,{xtype: 'combo-boolean',fieldLabel: 'ЧПУ', name: 'sef', allowBlank: false, anchor: '99%', id: 'minishop2-attribute-sef-'+type, renderer: 'boolean'}
            ,{xtype: 'textfield',fieldLabel: 'Title', name: 'meta_title', allowBlank: true, anchor: '99%', id: 'minishop2-attribute-title-'+type}
            ,{xtype: 'textfield',fieldLabel: 'Meta Description', name: 'meta_description', allowBlank: true, anchor: '99%', id: 'minishop2-attribute-description-'+type}
            ,{xtype: 'textfield',fieldLabel: 'H1', name: 'h1', allowBlank: true, anchor: '99%', id: 'minishop2-attribute-h1-'+type}

            ,{xtype: 'textfield',fieldLabel: 'Title ukr', name: 'meta_title_ua', allowBlank: true, anchor: '99%', id: 'minishop2-attribute-meta_title_ua-'+type}
            ,{xtype: 'textfield',fieldLabel: 'Meta Description ukr', name: 'meta_description_ua', allowBlank: true, anchor: '99%', id: 'minishop2-attribute-meta_description_ua-'+type}
            ,{xtype: 'textfield',fieldLabel: 'H1 ukr', name: 'h1_ua', allowBlank: true, anchor: '99%', id: 'minishop2-attribute-h1_ua-'+type}

            ,{html: 'Шаблон поддерживает переменные:<br/>'+
            '{$min_price} - цена самого дешевого товара (опции) в данной категории (странице тега). Пример, 39. {$min_price} не должен равняться 0, должно выбираться следующее число после 0.<br/>'+
            '{$counter} - кол-во товаров в категории + "товар".<br/>'+
            '{$top_brands} - первые 3 самых популярных бренда в категории. Пример: Royal Canine, Purina, Hill\'s. Если брендов в категории меньше 3-х - выводится 2 или 1.<br/>'+
			'{$animal} - вывод типа животных. Пример: для кошек'+
            '{$category} - название категории. <br/>',
				id: 'minishop2-template-desc',
                style: 'font-style: italic; padding: 10px; color: #555555;'
            }
		];
	}

});
Ext.reg('minishop2-grid-attribute',miniShop2.grid.Attribute);




miniShop2.window.CreateAttribute = function(config) {
	config = config || {};
	this.ident = config.ident || 'mecitem'+Ext.id();
	Ext.applyIf(config,{
		title: _('ms2_menu_create')
		,id: this.ident
		,width: 600
		,autoHeight: true
		,labelAlign: 'left'
		,labelWidth: 180
		,url: miniShop2.config.connector_url
		,action: 'mgr/settings/attribute/create'
		,fields: config.fields
		,keys: [{key: Ext.EventObject.ENTER,shift: true,fn: function() {this.submit() },scope: this}]
	});
	miniShop2.window.CreateAttribute.superclass.constructor.call(this,config);
};
Ext.extend(miniShop2.window.CreateAttribute,MODx.Window);
Ext.reg('minishop2-window-attribute-create',miniShop2.window.CreateAttribute);


miniShop2.window.UpdateAttribute = function(config) {
	config = config || {};
	this.ident = config.ident || 'meuitem'+Ext.id();
	Ext.applyIf(config,{
		title: _('ms2_menu_update')
		,id: this.ident
		,width: 600
		,autoHeight: true
		,labelAlign: 'left'
		,labelWidth: 180
		,url: miniShop2.config.connector_url
		,action: 'mgr/settings/attribute/update'
		,fields: config.fields
		,keys: [{key: Ext.EventObject.ENTER,shift: true,fn: function() {this.submit() },scope: this}]
	});
	miniShop2.window.UpdateAttribute.superclass.constructor.call(this,config);
};
Ext.extend(miniShop2.window.UpdateAttribute,MODx.Window);
Ext.reg('minishop2-window-attribute-update',miniShop2.window.UpdateAttribute);
