miniShop2.grid.Vendor = function (config) {
    config = config || {};

    this.exp = new Ext.grid.RowExpander({
        expandOnDblClick: false
        , tpl: new Ext.Template('<p class="desc">{description}</p>')
        , renderer: function (v, p, record) {
            return record.data.description != '' && record.data.description != null ? '<div class="x-grid3-row-expander">&#160;</div>' : '&#160;';
        }
    });

    Ext.applyIf(config, {
        id: 'minishop2-grid-vendor'
        ,
        url: miniShop2.config.connector_url
        ,
        baseParams: {
            action: 'mgr/settings/vendor/getlist'
        }
        ,
        fields: ['id', 'name','name_ua', 'resource', 'sort', 'name_ru', 'country', 'country_ua','email', 'logo',
            'address', 'showinmain', 'url', 'phone', 'fax', 'po_nilishiy',
            'description','description_ua',
            'post',
            'top', 'new', 'po_nilishiy','discount_prom','show_icon_delivery','show_icon_cachback','from_amount_cachback','dop_text','dop_text_ua']
        ,
        autoHeight: true
        ,
        paging: true
        ,
        remoteSort: true
        ,
        save_action: 'mgr/settings/vendor/updatefromgrid'
        ,
        autosave: true
        ,
        plugins: this.exp
        ,
        columns: [this.exp
            , {header: _('ms2_id'), dataIndex: 'id', width: 50, sortable: true}
            , {
                header: _('ms2_name'),
                dataIndex: 'name',
                width: 100,
                editor: {xtype: 'textfield', allowBlank: false},
                sortable: true
            }
            //,{header: _('ms2_resource'),dataIndex: 'resource',width: 100, editor: {xtype: 'minishop2-combo-resource'}, sortable: true, hidden: true}
            , {header: _('ms2_country'), dataIndex: 'country', width: 75, editor: {xtype: 'textfield'}, sortable: true}
            //,{header: _('ms2_email'),dataIndex: 'email',width: 100, editor: {xtype: 'textfield'}, sortable: true}
            , {header: _('ms2_url'), dataIndex: 'url', width: 100, editor: {xtype: 'textfield'}, sortable: true}
            , {header: _('ms2_name_ru'), dataIndex: 'name_ru', width: 75, editor: {xtype: 'textfield'}, sortable: true}
            //,{header: _('ms2_address'),dataIndex: 'address',width: 100, editor: {xtype: 'textarea'}}
            //,{header: _('ms2_phone'),dataIndex: 'phone',width: 75, editor: {xtype: 'textfield'}}
            //,{header: _('ms2_fax'),dataIndex: 'fax',width: 75, editor: {xtype: 'textfield'}}

            , {header: _('ms2_discount_prom'), dataIndex: 'discount_prom', width: 100, editor: {xtype: 'textfield'}, sortable: true}
            , {header: _('ms2_sort'), dataIndex: 'sort', width: 100, editor: {xtype: 'textfield'}, sortable: true}

            , {
                header: _('ms2_nalishie_brand'),
                dataIndex: 'po_nilishiy',
                width: 50,
                editor: {xtype: 'combo-boolean', renderer: 'boolean'},
                sortable: true
            }
            , {
                header: 'Бесплатная доставка',
                dataIndex: 'show_icon_delivery',
                width: 50,
                editor: {xtype: 'combo-boolean', renderer: 'boolean'},
                sortable: true
            }
            , {
                header: 'Кешбек',
                dataIndex: 'show_icon_cachback',
                width: 50,
                editor: {xtype: 'combo-boolean', renderer: 'boolean'},
                sortable: true
            }
            , {
                header: _('ms2_showinmain'),
                dataIndex: 'showinmain',
                width: 50,
                editor: {xtype: 'combo-boolean', renderer: 'boolean'},
                sortable: true
            }
            , {
                header: 'ТОП',
                dataIndex: 'top',
                width: 50,
                editor: {xtype: 'combo-boolean', renderer: 'boolean'},
                sortable: true
            }
            , {
                header: 'НОВИНКА',
                dataIndex: 'new',
                width: 50,
                editor: {xtype: 'combo-boolean', renderer: 'boolean'},
                sortable: true
            }
            , {
                header: 'Поставщик',
                dataIndex: 'post',
                width: 50,
                editor: {xtype: 'minishop2-combo-vendor_post'},
                sortable: true
            }			//xcheckbox


        ]
        ,
        tbar: [{
            text: _('ms2_btn_create')
            , handler: this.createVendor
            , scope: this
        }]
    });
    miniShop2.grid.Vendor.superclass.constructor.call(this, config);
};
Ext.extend(miniShop2.grid.Vendor, MODx.grid.Grid, {
    windows: {}

    , getMenu: function () {
        var m = [];
        m.push({
            text: _('ms2_menu_update')
            , handler: this.updateVendor
        });
        m.push('-');
        m.push({
            text: _('ms2_menu_remove')
            , handler: this.removeVendor
        });
        this.addContextMenuItem(m);
    }

    , renderLogo: function (value) {
        if (/(jpg|png|gif|jpeg)$/i.test(value)) {
            if (!/^\//.test(value)) {
                value = '/' + value;
            }
            return '<img src="' + value + '" height="35" />';
        }
        else {
            return '';
        }
    }

    , createVendor: function (btn, e) {
        if (!this.windows.createVendor) {
            this.windows.createVendor = MODx.load({
                xtype: 'minishop2-window-vendor-create'
                , fields: this.getVendorFields('create')
                , listeners: {
                    success: {
                        fn: function () {
                            this.refresh();
                        }, scope: this
                    }
                }
            });
        }
        this.windows.createVendor.fp.getForm().reset();
        this.windows.createVendor.show(e.target);
    }

    , updateVendor: function (btn, e) {
        if (!this.menu.record || !this.menu.record.id) return false;
        var r = this.menu.record;

        if (!this.windows.updateVendor) {
            this.windows.updateVendor = MODx.load({
                xtype: 'minishop2-window-vendor-update'
                , record: r
                , width: '50%'
                , fields: this.getVendorFields('update')
                , listeners: {
                    success: {
                        fn: function () {
                            this.refresh();
                        }, scope: this
                    }
                }
            });
        }
        this.windows.updateVendor.fp.getForm().reset();
        this.windows.updateVendor.fp.getForm().setValues(r);
        this.windows.updateVendor.show(e.target);
    }

    , removeVendor: function (btn, e) {
        if (!this.menu.record) return false;

        MODx.msg.confirm({
            title: _('ms2_menu_remove') + '"' + this.menu.record.name + '"'
            , text: _('ms2_menu_remove_confirm')
            , url: this.config.url
            , params: {
                action: 'mgr/settings/vendor/remove'
                , id: this.menu.record.id
            }
            , listeners: {
                success: {
                    fn: function (r) {
                        this.refresh();
                    }, scope: this
                }
            }
        });
    }

    , getVendorFields: function (type) {
        return [
            {
                layout: 'column'
                , defaults: {msgTarget: 'under', border: false}
                , style: 'padding:5px 0px;'
                , items: [
                    {
                        columnWidth: .99
                        , layout: 'form'
                        , border: false
                        , anchor: '100%'
                        , style: 'padding:5px 0px;'
                        , items: [{xtype: 'hidden', name: 'id', id: 'minishop2-vendor-id-' + type}
                            , {
                                xtype: 'textfield',
                                fieldLabel: _('ms2_name'),
                                name: 'name',
                                allowBlank: false,
                                anchor: '99%',
                                id: 'minishop2-vendor-name-' + type
                            }
                            , {
                                xtype: 'textfield',
                                fieldLabel: 'Имя на укр',
                                name: 'name_ua',
                                allowBlank: false,
                                anchor: '99%',
                                id: 'minishop2-vendor-name_ua-' + type
                            }
                            , {
                                xtype: 'textfield',
                                fieldLabel: _('ms2_country'),
                                name: 'country',
                                anchor: '99%',
                                id: 'minishop2-vendor-country-' + type
                            }
                            , {
                                xtype: 'textfield',
                                fieldLabel: 'Страна на укр',
                                name: 'country_ua',
                                anchor: '99%',
                                id: 'minishop2-vendor-country_ua-' + type
                            }
                            , {
                                xtype: 'textfield',
                                fieldLabel: _('ms2_url'),
                                name: 'url',
                                anchor: '99%',
                                id: 'minishop2-vendor-url-' + type
                            }
                            , {
                                xtype: 'textfield',
                                fieldLabel: _('ms2_name_ru'),
                                name: 'name_ru',
                                anchor: '99%',
                                id: 'minishop2-vendor-name_ru-' + type
                            }
                            , {
                                xtype: 'minishop2-combo-browser',
                                fieldLabel: _('ms2_logo'),
                                name: 'logo',
                                anchor: '99%',
                                id: 'minishop2-vendor-logo-' + type
                            }
                            , {
                                xtype: 'modx-htmleditor',
                                fieldLabel: _('ms2_description'),
                                name: 'description',
                                anchor: '99%',
                                id: 'minishop2-vendor-description-' + type,
                                cls: 'modx-richtext x-form-textarea'
                            }
                            , {
                                xtype: 'modx-htmleditor',
                                fieldLabel: 'Описание на укр',
                                name: 'description_ua',
                                anchor: '99%',
                                id: 'minishop2-vendor-description_ua-' + type,
                                cls: 'modx-richtext x-form-textarea'
                            }
                            , {
                                xtype: 'minishop2-combo-vendor_post',
                                fieldLabel: 'Поставщик',
                                name: 'post',
                                anchor: '99%',
                                id: 'minishop2-vendor-post-' + type
                            }
                        ]
                    },
                    {
                        columnWidth: .48
                        , layout: 'form'
                        , border: false
                        , style: 'padding:5px 0px;'
                        , anchor: '100%'
                        , items: [
                            {
                                xtype: 'textfield',
                                fieldLabel: _('ms2_sort'),
                                name: 'sort',
                                anchor: '99%',
                                id: 'minishop2-vendor-sort-' + type
                            }]
                    },
                    {
                        columnWidth: .48
                        , layout: 'form'
                        , style: 'padding:5px 0px;'
                        , border: false
                        , anchor: '100%'
                        , items: [
                            {
                                xtype: 'combo-boolean',
                                fieldLabel: _('ms2_showinmain'),
                                name: 'showinmain',
                                anchor: '99%',
                                id: 'minishop2-vendor-showinmain-' + type,
                                renderer: 'boolean'
                            }

                        ]
                    },
                    {
                        columnWidth: .48
                        , layout: 'form'
                        , style: 'padding:5px 0px;'
                        , border: false
                        , anchor: '100%'
                        , items: [
                            {
                                xtype: 'combo-boolean',
                                fieldLabel: _('ms2_nalishie_brand'),
                                name: 'po_nilishiy',
                                anchor: '99%',
                                id: 'minishop2-vendor-nalishie-' + type,
                                renderer: 'boolean'
                            }
                        ]
                    },
                    {
                        columnWidth: .48
                        ,
                        layout: 'form'
                        ,
                        border: false
                        ,
                        style: 'padding:5px 0px;'
                        ,
                        anchor: '100%'
                        ,
                        items: [{
                            xtype: 'combo-boolean',
                            fieldLabel: 'Топ',
                            name: 'top',
                            anchor: '99%',
                            id: 'minishop2-vendor-top-' + type,
                            renderer: 'boolean'
                        }]
                    }, {
                        columnWidth: .48
                        , layout: 'form'
                        , style: 'padding:5px 0px;'
                        , border: false
                        , anchor: '100%'
                        , items: [
                            {
                                xtype: 'combo-boolean',
                                fieldLabel: 'Новинка',
                                name: 'new',
                                anchor: '99%',
                                id: 'minishop2-vendor-new-' + type,
                                renderer: 'boolean'
                            }
                        ]
                    }
                    , {
                        columnWidth: .48
                        , layout: 'form'
                        , style: 'padding:5px 0px;'
                        , border: false
                        , anchor: '100%'
                        , items: [
                            {
                                xtype: 'textfield',
                                fieldLabel: 'Дисконт prom',//_('ms2_discount_prom'),
                                name: 'discount_prom',
                                anchor: '99%',
                                id: 'minishop2-vendor-discount_prom-' + type
                            }
                        ]
                    }
                    , {
                        columnWidth: .48
                        , layout: 'form'
                        , style: 'padding:5px 0px;'
                        , border: false
                        , anchor: '100%'
                        , items: [
                            {
                                xtype: 'combo-boolean',
                                fieldLabel: 'Выводить доставка',
                                name: 'show_icon_delivery',
                                anchor: '99%',
                                id: 'minishop2-vendor-delivery-' + type,
                                renderer: 'boolean'
                            }
                            // {
                            //     xtype: 'combo-boolean',
                            //     fieldLabel: 'Выводить иконка доставка',//_('ms2_discount_prom'),
                            //     name: 'show_icon_delivery',
                            //     anchor: '99%',
                            //     id: 'minishop2-vendor-show_icon_delivery-' + type,
                            //     renderer: 'boolean'
                            // }
                        ]
                    }
                    , {
                        columnWidth: .48
                        , layout: 'form'
                        , style: 'padding:5px 0px;'
                        , border: false
                        , anchor: '100%'
                        , items: [
                            {
                                xtype: 'combo-boolean',
                                fieldLabel: 'Выводить Кешбек',
                                name: 'show_icon_cachback',
                                anchor: '99%',
                                id: 'minishop2-vendor-cachback-' + type,
                                renderer: 'boolean'
                            }
                            // {
                            //     xtype: 'combo-boolean',
                            //     fieldLabel: 'Выводить Кешбек',//_('ms2_discount_prom'),
                            //     name: 'show_icon_cachback',
                            //     anchor: '99%',
                            //     id: 'minishop2-vendor-icon_cachback-' + type,
                            //     renderer: 'boolean'
                            // }
                        ]
                    }
                    , {
                        columnWidth: .48
                        , layout: 'form'
                        , style: 'padding:5px 0px;'
                        , border: false
                        , anchor: '100%'
                        , items: [
                            {
                                xtype: 'textfield',
                                fieldLabel: 'От какой суммы кешбек',
                                name: 'from_amount_cachback',
                                anchor: '99%',
                                id: 'minishop2-vendor-from_amount_cachbackm-' + type
                            }
                        ]
                    }
                    , {
                        columnWidth: .48
                        , layout: 'form'
                        , style: 'padding:5px 0px;'
                        , border: false
                        , anchor: '100%'
                        , items: [
                            {
                                xtype: 'textarea',//field',
                                fieldLabel: 'Дополнительный текст перед описанием',
                                name: 'dop_text',
                                anchor: '100%',
                                width:'300px',
                                id: 'minishop2-vendor-dop_text-' + type,
                                cls: 'modx-richtext x-form-textarea'
                            }
                        ]
                    }
                    , {
                        columnWidth: .99
                        , layout: 'form'
                        , style: 'padding:5px 0px;'
                        , border: false
                        , anchor: '100%'
                        , items: [
                            {
                                xtype: 'textarea',//field',
                                fieldLabel: 'Дополнительный текст перед описанием (укр)',
                                name: 'dop_text_ua',
                                anchor: '100%',
                                width:'300px',
                                id: 'minishop2-vendor-dop_text_ua-' + type,
                                cls: 'modx-richtext x-form-textarea'
                            }
                        ]
                    }
                ]
            }
        ];//textarea
    }

});
Ext.reg('minishop2-grid-vendor', miniShop2.grid.Vendor);
/*
    Ext.onReady(function() {
        
            var config = {
                selector: '.x-form-textarea',// + field.cls,
                setup: function(ed) {
                    ed.on("click", function(e) {
                        MODx.fireResourceFormChange();
                    });
                }
            };
            Ext.applyIf(config, MODx.ux.CKEditor.editorConfig);
           
//MODx.ux.CKEditor.init(config);
       // }
    });
*/

miniShop2.window.CreateVendor = function (config) {
    config = config || {};
    this.ident = config.ident || 'mecitem' + Ext.id();
    Ext.applyIf(config, {
        title: _('ms2_menu_create')
        , id: this.ident
        , width: 600
        , autoHeight: true
        , labelAlign: 'left'
        , labelWidth: 180
        , url: miniShop2.config.connector_url
        , action: 'mgr/settings/vendor/create'
        , fields: config.fields
        , keys: [{
            key: Ext.EventObject.ENTER, shift: true, fn: function () {
                this.submit()
            }, scope: this
        }]
    });
    miniShop2.window.CreateVendor.superclass.constructor.call(this, config);
};
Ext.extend(miniShop2.window.CreateVendor, MODx.Window);
Ext.reg('minishop2-window-vendor-create', miniShop2.window.CreateVendor);


miniShop2.window.UpdateVendor = function (config) {
    config = config || {};
    this.ident = config.ident || 'meuitem' + Ext.id();
    Ext.applyIf(config, {
        title: _('ms2_menu_update')
        , id: this.ident
        , width: '80%'
        , autoHeight: true
        , labelAlign: 'left'
        , labelWidth: 180
        , url: miniShop2.config.connector_url
        , action: 'mgr/settings/vendor/update'
        , fields: config.fields
        , keys: [{
            key: Ext.EventObject.ENTER, shift: true, fn: function () {
                this.submit()
            }, scope: this
        }]
    });
    miniShop2.window.UpdateVendor.superclass.constructor.call(this, config);
};
Ext.extend(miniShop2.window.UpdateVendor, MODx.Window);
Ext.reg('minishop2-window-vendor-update', miniShop2.window.UpdateVendor);
