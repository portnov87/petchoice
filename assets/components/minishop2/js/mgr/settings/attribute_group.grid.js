miniShop2.grid.AttributeGroup = function(config) {
	config = config || {};

	this.exp = new Ext.grid.RowExpander({
		expandOnDblClick: false
		,tpl : new Ext.Template('<p class="desc">{description}</p>')
		,renderer : function(v, p, record){return record.data.description != '' && record.data.description != null ? '<div class="x-grid3-row-expander">&#160;</div>' : '&#160;';}
	});

	Ext.applyIf(config,{
		id: 'minishop2-grid-attribute_group'
		,url: miniShop2.config.connector_url
		,baseParams: {
			action: 'mgr/settings/attribute_group/getlist'
		}
		,fields: ['id','name','name_ua','key','sort','sort_related']
		,autoHeight: true
		,paging: true
		//,enableDragDrop : true  
		,remoteSort: true
		,save_action: 'mgr/settings/attribute_group/updatefromgrid'
		,autosave: true
		,plugins: this.exp
		,columns: [this.exp
			,{header: _('ms2_id'),dataIndex: 'id',width: 50, sortable: true}
			,{header: _('ms2_name'),dataIndex: 'name',width: 100, editor: {xtype: 'textfield', allowBlank: false}, sortable: true}
            ,{header: 'Имя укр',dataIndex: 'name_ua',width: 100, editor: {xtype: 'textfield', allowBlank: false}, sortable: true}
			,{header: 'Ключ',dataIndex: 'key',width: 100, editor: {xtype: 'textfield', allowBlank: false}, sortable: true}
			,{header: 'Сортировка',dataIndex: 'sort',width: 100, editor: {xtype: 'textfield', allowBlank: false}, sortable: true}
            ,{header: 'Сортировка для Похожие товары',dataIndex: 'sort_related',width: 100, editor: {xtype: 'combo-boolean', renderer: 'boolean'}, sortable: true}
		]
		,ddGroup: 'dd'
		,enableDragDrop: true
		,listeners: {
			render: {fn: this._initDD, scope: this}
		}

		,tbar: [{
			text: _('ms2_btn_create')
			,handler: this.createAttributeGroup
			,scope: this
		}]
	});
	miniShop2.grid.AttributeGroup.superclass.constructor.call(this,config);
};
Ext.extend(miniShop2.grid.AttributeGroup,MODx.grid.Grid,{
	windows: {}
	,_initDD: function(grid) {

		new Ext.dd.DropTarget(grid.el, {
			ddGroup : 'dd',
			copy:false,
			notifyDrop : function(dd, e, data) {
				var store = grid.store.data.items;

				var target = store[dd.getDragData(e).rowIndex].data;

				var source = store[data.rowIndex].data;

				if ((target.parent == source.parent) && (target.id != source.id)) {

					dd.el.mask(_('loading'),'x-mask-loading');

					MODx.Ajax.request({

						url: miniShop2.config.connector_url

						,params: {
							//action: 'mgr/product/sort'
							action: 'mgr/settings/attribute_group/sort'
							,source: source.id
							,target: target.id
						}

						,listeners: {

							success: {fn:function(r) {dd.el.unmask();grid.refresh();},scope:grid}

							,failure: {fn:function(r) {dd.el.unmask();},scope:grid}

						}

					});

				}

			}

		});

	}

	,getMenu: function() {
		var m = [];
		m.push({
			text: _('ms2_menu_update')
			,handler: this.updateAttributeGroup
		});
		m.push('-');
		m.push({
			text: _('ms2_menu_remove')
			,handler: this.removeAttributeGroup
		});
		this.addContextMenuItem(m);
	}

	,createAttributeGroup: function(btn,e) {
		if (!this.windows.createAttributeGroup) {
			this.windows.createAttributeGroup = MODx.load({
				xtype: 'minishop2-window-attribute_group-create'
				,fields: this.getAttributeGroupFields('create')
				,listeners: {
					success: {fn:function() { this.refresh(); },scope:this}
				}
			});
		}
		this.windows.createAttributeGroup.fp.getForm().reset();
		this.windows.createAttributeGroup.show(e.target);
	}

	,updateAttributeGroup: function(btn,e) {
		if (!this.menu.record || !this.menu.record.id) return false;
		var r = this.menu.record;

		if (!this.windows.updateAttributeGroup) {
			this.windows.updateAttributeGroup = MODx.load({
				xtype: 'minishop2-window-attribute_group-update'
				,record: r
				,fields: this.getAttributeGroupFields('update')
				,listeners: {
					success: {fn:function() { this.refresh(); },scope:this}
				}
			});
		}
		this.windows.updateAttributeGroup.fp.getForm().reset();
		this.windows.updateAttributeGroup.fp.getForm().setValues(r);
		this.windows.updateAttributeGroup.show(e.target);
	}

	,removeAttributeGroup: function(btn,e) {
		if (!this.menu.record) return false;

		MODx.msg.confirm({
			title: _('ms2_menu_remove') + '"' + this.menu.record.name + '"'
			,text: _('ms2_menu_remove_confirm')
			,url: this.config.url
			,params: {
				action: 'mgr/settings/attribute_group/remove'
				,id: this.menu.record.id
			}
			,listeners: {
				success: {fn:function(r) {this.refresh();}, scope:this}
			}
		});
	}

	,getAttributeGroupFields: function(type) {
		return [
			{xtype: 'hidden',name: 'id', id: 'minishop2-attribute_group-id-'+type}
			,{xtype: 'textfield',fieldLabel: _('ms2_name'), name: 'name', allowBlank: true, anchor: '99%', id: 'minishop2-attribute_group-name-'+type}
            ,{xtype: 'textfield',fieldLabel: 'Имя укр', name: 'name_ua', allowBlank: true, anchor: '99%', id: 'minishop2-attribute_group-name_ua-'+type}
			,{xtype: 'textfield',fieldLabel: 'Ключ', name: 'key', allowBlank: true, anchor: '99%', id: 'minishop2-attribute_group-key-'+type}
			,{xtype: 'textfield',fieldLabel: 'Сортировка', name: 'sort', allowBlank: true, anchor: '99%', id: 'minishop2-attribute_group-sort-'+type}
            ,{xtype: 'combo-boolean',fieldLabel: 'приоритет для похожих товаров', name: 'sort_related', allowBlank: true, anchor: '99%', id: 'minishop2-attribute_group-sort_related-'+type, renderer: 'boolean'}
		];
	}

});
Ext.reg('minishop2-grid-attribute_group',miniShop2.grid.AttributeGroup);




miniShop2.window.CreateAttributeGroup = function(config) {
	config = config || {};
	this.ident = config.ident || 'mecitem'+Ext.id();
	Ext.applyIf(config,{
		title: _('ms2_menu_create')
		,id: this.ident
		,width: 600
		,autoHeight: true
		,labelAlign: 'left'
		,labelWidth: 180
		,url: miniShop2.config.connector_url
		,action: 'mgr/settings/attribute_group/create'
		,fields: config.fields
		,keys: [{key: Ext.EventObject.ENTER,shift: true,fn: function() {this.submit() },scope: this}]
	});
	miniShop2.window.CreateAttributeGroup.superclass.constructor.call(this,config);
};
Ext.extend(miniShop2.window.CreateAttributeGroup,MODx.Window);
Ext.reg('minishop2-window-attribute_group-create',miniShop2.window.CreateAttributeGroup);


miniShop2.window.UpdateAttributeGroup = function(config) {
	config = config || {};
	this.ident = config.ident || 'meuitem'+Ext.id();
	Ext.applyIf(config,{
		title: _('ms2_menu_update')
		,id: this.ident
		,width: 600
		,autoHeight: true
		,labelAlign: 'left'
		,labelWidth: 180
		,url: miniShop2.config.connector_url
		,action: 'mgr/settings/attribute_group/update'
		,fields: config.fields
		,keys: [{key: Ext.EventObject.ENTER,shift: true,fn: function() {this.submit() },scope: this}]
	});
	miniShop2.window.UpdateAttributeGroup.superclass.constructor.call(this,config);
};
Ext.extend(miniShop2.window.UpdateAttributeGroup,MODx.Window);
Ext.reg('minishop2-window-attribute_group-update',miniShop2.window.UpdateAttributeGroup);
