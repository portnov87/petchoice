miniShop2 = {};
miniShop2Config = {
	cssUrl: "/assets/components/minishop2/css/web/"
	,jsUrl: "/assets/components/minishop2/js/web/"
	,imagesUrl: "/assets/components/minishop2/images/web/"
	,actionUrl: "/assets/components/minishop2/action.php"
	,ctx: "web"
	,close_all_message: "закрыть все"
	,price_format: [2, ".", " "]
	,price_format_no_zeros: 1
	,weight_format: [3, ".", " "]
	,weight_format_no_zeros: 1
	,callbacksObjectTemplate: function() {
		return {
			before: function() {/*return false to prevent send data*/}
			,response: {success: function(response) {},error: function(response) {}}
			,ajax: {done: function(xhr) {},fail: function(xhr) {},always: function(xhr) {}}
		};
	}
};
miniShop2.Callbacks = miniShop2Config.Callbacks = {
	Cart: {
		add: miniShop2Config.callbacksObjectTemplate()
		,remove: miniShop2Config.callbacksObjectTemplate()
		,change: miniShop2Config.callbacksObjectTemplate()
		,clean: miniShop2Config.callbacksObjectTemplate()
	}
	,Order: {
		add: miniShop2Config.callbacksObjectTemplate()
		,getcost: miniShop2Config.callbacksObjectTemplate()
		,clean: miniShop2Config.callbacksObjectTemplate()
		,submit: miniShop2Config.callbacksObjectTemplate()
		,promocode: miniShop2Config.callbacksObjectTemplate()
		,getRequired: miniShop2Config.callbacksObjectTemplate()
	}
};