<?php
/**
 * Created by PhpStorm.
 * User: portnovvit
 * Date: 20.08.2018
 * Time: 11:00
 */


define('MODX_API_MODE', true);
set_time_limit(0);
//$root='/var/www/admin/www/petchoice.com.ua/';
$root=dirname(dirname(dirname(d irname(__FILE__))));
error_reporting(E_ALL | E_STRICT);
ini_set('display_errors', 1);
//define('MODX_API_MODE', true);
require_once $root.'/index.php';

$q = $modx->newQuery('msProduct');
$q->select(array(
    'msProduct.id',
    //'Data.*',
    'TV.contentid',
    'TV.value',
    'TV.tmplvarid',
    'Vendor.po_nilishiy'
));

$q->innerJoin('msProductData', 'Data', 'msProduct.id = Data.id');
$q->leftJoin('modTemplateVarResource', 'TV', 'msProduct.id = TV.contentid AND TV.tmplvarid= 1');
$q->leftJoin('msVendor', 'Vendor', 'Data.vendor = Vendor.id');

$q->where(['Vendor.po_nilishiy:=' => 1]);
$q->groupby('msProduct.id');
$q->prepare();
$q->stmt->execute();
$result = $q->stmt->fetchAll(PDO::FETCH_ASSOC);

$delete_option=[];
foreach ($result as $product) {
    $options = $product['value'];
    $id_option = $modx->fromJSON($options);
    $new_option=$id_option;
    foreach ($id_option as $key=>$opt){
        if (!isset($opt['price_supplier']))
        {
            //$opt['price_supplier']==''){
            unset($new_option[$key]);
            $delete_option[$key]=$key;
        }
    }

    $new_option = $modx->toJSON($new_option);

    $nodes = $modx->getIterator('modTemplateVarResource',
        array('contentid' => $product['contentid'], 'tmplvarid' => 1));

    foreach ($nodes as $node) {
        $node->set('value', $new_option);
        $node->save();
    }
}


echo count($result).' '.count($delete_option);
