<?php
/**
 * Created by PhpStorm.
 * User: portn
 * Date: 28.09.2017
 * Time: 14:14
 */
?>
<link rel="stylesheet"
        type="text/css"
        href="https://code.jquery.com/ui/1.12.0-beta.1/themes/smoothness/jquery-ui.css" />


<script src="https://code.jquery.com/jquery-1.12.4.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script>
    setTimeout(function(){
    !function(a){function f(a,b){if(!(a.originalEvent.touches.length>1)){a.preventDefault();var c=a.originalEvent.changedTouches[0],d=document.createEvent("MouseEvents");d.initMouseEvent(b,!0,!0,window,1,c.screenX,c.screenY,c.clientX,c.clientY,!1,!1,!1,!1,0,null),a.target.dispatchEvent(d)}}if(a.support.touch="ontouchend"in document,a.support.touch){var e,b=a.ui.mouse.prototype,c=b._mouseInit,d=b._mouseDestroy;b._touchStart=function(a){var b=this;!e&&b._mouseCapture(a.originalEvent.changedTouches[0])&&(e=!0,b._touchMoved=!1,f(a,"mouseover"),f(a,"mousemove"),f(a,"mousedown"))},b._touchMove=function(a){e&&(this._touchMoved=!0,f(a,"mousemove"))},b._touchEnd=function(a){e&&(f(a,"mouseup"),f(a,"mouseout"),this._touchMoved||f(a,"click"),e=!1)},b._mouseInit=function(){var b=this;b.element.bind({touchstart:a.proxy(b,"_touchStart"),touchmove:a.proxy(b,"_touchMove"),touchend:a.proxy(b,"_touchEnd")}),c.call(b)},b._mouseDestroy=function(){var b=this;b.element.unbind({touchstart:a.proxy(b,"_touchStart"),touchmove:a.proxy(b,"_touchMove"),touchend:a.proxy(b,"_touchEnd")}),d.call(b)}}}(jQuery);


    jQuery(document).ready(function(){

            jQuery('.delivery_map_container').show();
            jQuery('.loading').hide();
            //jQuery(".delivery_map_container").on('mouseenter',function(){

                    $( function() {
                        $(".delivery_map>div").draggable({
                            drag: function (event, ui) {
                                var diffX = $(this).width() - $(this).parent().width();
                                var diffY = $(this).height() -$(this).parent().height();

                                if ((diffX + ui.position.left) > 0) {
                                    ui.position.left = Math.min(0, ui.position.left);
                                } else {
                                    ui.position.left = Math.max(-diffX, ui.position.left);
                                }

                                if ((diffY + ui.position.top) > 0) {
                                    ui.position.top = Math.min(0, ui.position.top);
                                } else {
                                    ui.position.top = Math.max(-diffY, ui.position.top);
                                }
                            }
                        });
                    });

            //});

    });
    },1500);
</script>
<div class="loading" style="height:200px;width:100%;"></div>
<div class="delivery_map_container" style="display:none;">
    <div class="drag_help"></div>
    <div class="zone_help">
        <div class="map_color_1">- Бесплатно от 499 грн</div>
        <div class="map_color_2">- Бесплатно от 999 грн</div>
        <div class="map_color_3">- Доставка 70 грн, минимальный заказ 999 грн</div>
    </div>
    <div class="delivery_map" >
        <div></div>
    </div>
</div>

<style>
    .zone_help{
        display: inline-block;
        width: auto;
        background: rgba(28, 56, 71, 0.8);
        padding: 4px 20px;
        position: absolute;
        z-index: 2;
        left: 0;
        top:0;
    }
    .zone_help>div{
        display: inline-block;
        width: auto;
        height: 20px;
        margin-right: 10px;
        padding-left: 35px;
        float: left;
        color: white;
        position: relative;
        font:300 12px/20px "Open Sans", Arial, sans-serif;
    }
    .zone_help>div:before{
        content: "";
        display: block;
        width: 30px;
        height: 14px;
        border:1px solid rgba(255,255,255,.4);
        position: absolute;
        left: 0;
        top:50%;
        margin-top:-8px;
        -webkit-border-radius:3px;
        -moz-border-radius:3px;
        border-radius:3px;
    }
    .zone_help>div.map_color_1:before{
        background-color: #8fae3b;
    }
    .zone_help>div.map_color_2:before{
        background-color: #ff9000;
    }
    .zone_help>div.map_color_3:before{
        background-color: #c62f48;
    }
    .delivery_map,
    .delivery_map_container{
        height: 680px;
    }
    .delivery_map_container{
        display: block;
        width: 100%;
        position: relative;
    }
    .delivery_map_container .drag_help{
        display: block;
        width:60px;
        height:60px;
        position: absolute;
        top:15px;
        right: 15px;
        background: url("https://petchoice.ua/assets/images/map/dragged.png") 50% 50% no-repeat;
        background-size: 60px auto !important;
        z-index: 3;
        -webkit-animation:      zoomInOne 1.4s infinite ease-in-out;
        -moz-animation:         zoomInOne 1.4s infinite ease-in-out;
        -ms-animation:          zoomInOne 1.4s infinite ease-in-out;
        -o-animation:           zoomInOne 1.4s infinite ease-in-out;
        animation:              zoomInOne 1.4s infinite ease-in-out;
        opacity: 0.9;
    }
    @media only screen and (max-width :1200px),
    only screen and (max-device-width : 1200px){
        .delivery_map_container .drag_help {
            width: 40px;
            height: 40px;
            top: 10px;
            right: 10px;
            background: url("https://petchoice.ua/assets/images/map/dragged_mobile.png") 50% 50% no-repeat;
            background-size: 40px auto !important;
        }
    }
    .delivery_map{
        width: 100%;
        margin: 0 auto;
        overflow: hidden;
    }
<?php if (isset($_REQUEST['mobile'])):?>
    .delivery_map>div{
        display: block;
        width: 3000px;
        height: 3333px;
        position: relative;
        background: url("https://petchoice.ua/assets/images/map/delivery_map.jpg") 50% 50% no-repeat;
        background-size: 3600px auto !important;
        left: -1060px;
        left: -1300px;
        top:-1400px;
        top:-1450px;
        left: -1209px;
        top: -1608px;
    }
    <?php else: ?>
    .delivery_map>div{
        display: block;
        width: 3000px;
        height: 3333px;
        position: relative;
        background: url("https://petchoice.ua/assets/images/map/delivery_map.jpg") 50% 50% no-repeat;
        background-size: 3600px auto !important;
        left: -1060px;
        top:-1400px;
        top:-1300px;
        left: -980px;
        top: -1592px;
    }
    <?php endif;?>


    .delivery_map:hover{
        cursor: url("https://petchoice.ua/assets/images/map/icn_cursor_hand.png"), auto;
    }
    .delivery_map:active{
        cursor: url("https://petchoice.ua/assets/images/map/icn_cursor_drag.png"), auto;
    }
    @-webkit-keyframes zoomInOne {
        0%, 100% {
            -webkit-transform:      scale(0.9);
            transform:              scale(0.9);
        }
        50% {
            -webkit-transform:      scale(1.0);
            transform:              scale(1.0);
        }
    }
    @keyframes zoomInOne {
        0%, 100% {
            -webkit-transform:      scale(0.9);
            transform:              scale(0.9);
        }
        50% {
            -webkit-transform:      scale(1.0);
            transform:              scale(1.0);
        }
    }
    @media only screen and (max-height :760px),
    only screen and (max-device-height : 760px){
        .delivery_map,
        .delivery_map_container{
            height: 480px;
        }
    }
    @media only screen and (max-height :568px),
    only screen and (max-device-height : 568px){
        .delivery_map,
        .delivery_map_container{
            height: 400px;
        }
    }


</style>

