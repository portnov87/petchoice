<?php

$root=dirname(dirname(dirname(dirname(dirname(__FILE__)))));
error_reporting(E_ALL | E_STRICT);
ini_set('display_errors', 1);
define('MODX_API_MODE', true);

require_once $root.'/index.php';


$esputnikPath = MODX_CORE_PATH . 'components/minishop2/model/minishop2/esputnik.class.php';

require_once $esputnikPath;


$data = array();
$subject = $modx->getOption('subject_mailmore45');
$emails = array();

$miniShop2 = $modx->getService('minishop2');
$miniShop2->initialize($modx->context->key);


function validatecode($modx)
{
    global $modx;
    //if ($code=='')
    $code = rand(10000, 99999);
    $item = $modx->getObject('PromocodeItem', array('code' => $code));
    if (!$item) return $code;
    else return validatecode($modx);
}

function generatepromocode($discount_promocode)
{
    global $modx;
    $code = validatecode($modx);
    $created = date('Y-m-d');

    $after7day = mktime(0, 0, 0, date("m"), date("d") + 10, date("Y"));
    $term = date('Y-m-d H:m:s',$after7day);


    $status = 1;
    $arraypost = array(
        'code' => $code,
        'name' => 'код с почты',
        'term' => $term,
        'category' => 10,
        'type' => 'dinamic',
        'status' => $status,
        'discount' => $discount_promocode.'%',//'7%',
        'removeloylnost' => 0,
        'created' => $created,
        'email' => 1,
    );


    $item = $modx->newObject('PromocodeItem', $arraypost);

    if ($item->save()) {
        $item_category = $modx->newObject('PromocodeCategory');

        $item_category->set('category', 10);
        $item_category->set('promocode', (int)$item->get('id'));
        $item_category->save();

        return $code;
    } else return false;

}


$discountPromocodes = [7, 10, 12];
$modelEsputnik = new Esputnik($modx);
$dataEsputnik = [];


foreach ($discountPromocodes as $discount) {
    $codes = [];
    $dataEsputnik=[];
    $expirationDate = date('Y-m-d', mktime(23, 59, 59, date("m"), date("d") + 10, date("Y")));
    //.'T'.date('H:m', mktime(23, 59, 59, date("m"), date("d") + 7, date("Y")));
        //date('c', mktime(23, 59, 59, date("m"), date("d") + 7, date("Y")));
    for ($i = 1; $i <= 200; $i++) {
        $codes[] = generatepromocode($discount);
    }
    $dataEsputnik = [
        'promocodes' => $codes,
        'expirationDate' => $expirationDate,
        'discount' => $discount,
        'type' => $discount,
        'inUse' => false
    ];

    $returnGenPromo = $modelEsputnik->sentPromocode($dataEsputnik);

}


//$promocodes=$modelEsputnik->getPromocodes();


die();
