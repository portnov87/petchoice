<?php
/**
 * Created by PhpStorm.
 * User: portn
 * Date: 13.07.2017
 * Time: 12:37
 */
define('MODX_API_MODE', true);
set_time_limit(0);
//$root='/var/www/admin/www/petchoice.com.ua/';
$root=dirname(dirname(dirname(dirname(dirname(__FILE__)))));
error_reporting(E_ALL | E_STRICT);
ini_set('display_errors', 1);
//define('MODX_API_MODE', true);
//require_once $root.'/index.php';


/* define this as true in another entry file, then include this file to simply access the API
 * without executing the MODX request handler */
if (!defined('MODX_API_MODE')) {
    define('MODX_API_MODE', false);
}

/* this can be used to disable caching in MODX absolutely */
$modx_cache_disabled= false;

/* include custom core config and define core path */
@include($root. '/config.core.php');
if (!defined('MODX_CORE_PATH')) define('MODX_CORE_PATH', dirname(__FILE__) . '/core/');

//
/* include the modX class */
if (!@include_once (MODX_CORE_PATH . "model/modx/modx.class.php")) {
    //echo MODX_CORE_PATH . "model/modx/modx.class.php";
    //die();
    $errorMessage = 'Site temporarily unavailable';
    @include(MODX_CORE_PATH . 'error/unavailable.include.php');
    header($_SERVER['SERVER_PROTOCOL'] . ' 503 Service Unavailable');
    echo "<html><title>Error 503: Site temporarily unavailable</title><body><h1>Error 503</h1><p>{$errorMessage}</p></body></html>";
    exit();
}

/* start output buffering */
ob_start();

/* Create an instance of the modX class */
$modx= new modX();
if (!is_object($modx) || !($modx instanceof modX)) {
    //echo MODX_CORE_PATH . "model/modx/modx.class.php";
    //die();
    ob_get_level() && @ob_end_flush();
    $errorMessage = '<a href="setup/">MODX not installed. Install now?</a>';
    @include(MODX_CORE_PATH . 'error/unavailable.include.php');
    header($_SERVER['SERVER_PROTOCOL'] . ' 503 Service Unavailable');
    echo "<html><title>Error 503: Site temporarily unavailable</title><body><h1>Error 503</h1><p>{$errorMessage}</p></body></html>";
    exit();
}
$modx->initialize('web');




//die();

$miniShop2 = $modx->getService('minishop2');
$miniShop2->initialize($modx->context->key);

$miniShop2->getAllCategories($root);
$miniShop2->getProducts($root);
//$miniShop2->getPhotos($root);

$miniShop2->getUsers($root);



$miniShop2->UploadSales($root);
$miniShop2->UploadSalesBonuses($root);


$miniShop2->UploadAvailable($root);
?>