<?php

$root=dirname(dirname(dirname(dirname(dirname(__FILE__)))));
error_reporting(E_ALL | E_STRICT);
ini_set('display_errors', 1);
define('MODX_API_MODE', true);

require_once $root.'/index.php';


$esputnikPath = MODX_CORE_PATH . 'components/minishop2/model/minishop2/esputnik.class.php';

require_once $esputnikPath;

$miniShop2 = $modx->getService('minishop2');
$miniShop2->initialize($modx->context->key);


$modelEsputnik = new Esputnik($modx);

$term=date('Y-m-d H:m:s',strtotime('- 10 day')); // -7


$sql = "SELECT promo.* FROM modx_promo_promocode promo WHERE promo.term <='$term'";

$q = $modx->prepare($sql);
$q->execute(array(0));
$codes=[];
$promocodeItems = $q->fetchAll(PDO::FETCH_ASSOC);
foreach ($promocodeItems as $promo) {

    $promocodeId=$promo['id'];
    $promocode=$promo['code'];
    $discount=$promo['discount'];
    $_term=$promo['term'];
        //$term=
    $item = $modx->getObject('PromocodeItem', array('id:=' => $promocodeId));
    if ($item)
    {
        $item->remove();
        $itemPromoCategory = $modx->getObject('PromocodeCategory', array('promocode:=' => $promocodeId));
        if ($itemPromoCategory)
            $itemPromoCategory->remove();

        if (!isset($codes[$discount]))
        {
            $codes[$discount]=[];
            $codes[$discount]['promocodes']=[];
        }

        $codes[$discount]['promocodes'][]=$promocode;
        $codes[$discount]['inUse']=true;
        $codes[$discount]['discount']=$discount;
        $codes[$discount]['type']=$discount;
        $codes[$discount]['expirationDate']=date('Y-m-d', strtotime($_term));

    }

}


if (count($codes)>0) {
    foreach ($codes as $dataEsputnik) {
        $returnGenPromo = $modelEsputnik->sentPromocode($dataEsputnik);
    }
}

die();
