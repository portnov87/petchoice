<?php
/**
 * Created by PhpStorm.
 * User: portnovvit
 * Date: 04.02.2019
 * Time: 12:59
 */
$root = '/var/www/admin/www/petchoice.com.ua/';
error_reporting(E_ALL | E_STRICT);
ini_set('display_errors', 1);
define('MODX_API_MODE', true);
require_once $root . 'index.php';


$miniShop2 = $modx->getService('minishop2');
$miniShop2->initialize($modx->context->key);



$orderId=179916;
$order=$modx->getObject('msOrder',$orderId);
$user_id = $order->get('user_id');
$user = $modx->getObject('modUser', $user_id);
$user_id = $user->get('id');
$profile = $user->getOne('Profile');
$extended = $profile->get('extended');
$user_discount = $extended['discount'];

$private_key = $modx->getOption('privat_keyliqpay');//'vQP1hC3r1nWHN2nhAwfLUDFhorbm1epVtZgwo30h';
$public_key = $modx->getOption('public_keyliqpay');//'i63764050824';

$amount = $order->get('cost');
$order_id = $order->get('num');

$currency = 'UAH';

$description = 'Order #' . $order_id;
$result_url = 'https://petchoice.ua/';
$server_url = 'https://petchoice.ua/assets/components/minishop2/getorder.php?action=payliqpay';

$type = 'buy';

$version = '3';
$language = 'ru';

$data = base64_encode(
    json_encode(
        array('version' => $version,
            'public_key' => $public_key,
            'action' => 'pay',
            'amount' => $amount,
            'currency' => $currency,
            'description' => $description,
            'order_id' => $order_id,
            'type' => $type,
            'sandbox' => 0,
            'result_url' => $result_url,
            'server_url' => $server_url,
            'language' => $language)
    )
);

$signature = base64_encode(sha1($private_key . $data . $private_key, 1));
$link = 'https://www.liqpay.ua/api/3/checkout?data=' . $data . '&signature=' . $signature;


$link=$miniShop2->createShortUrl($link);

$dataprofile = $user->getOne('Profile')->toArray();
$data=array_merge(
    $dataprofile,//$user->getOne('Profile')->toArray(),
    $user->toArray(),
    $order->toArray(),
    array(
        'nameuser' => $dataprofile['fullname'] . ' ' . $dataprofile['extended']['lastname'],
        'fullname' => $dataprofile['fullname'],

        'url_liqpay' => $link,
        'num_order'=>$order->get('num'),
        'num' => $order->get('num'),
        'order_id' => $order->get('id')
    )
);

$data['phone'] = $user->get('username');//$user['username'];
$type = 26;
//$userObject = $this->modx->getObject('modUser', $user['id']);
echo '$link = '.$link."\r\n\r\n";
$miniShop2->eventNotification($type, $user, $data,['action'=>'liqpay','liqpay_link'=>$link]);
