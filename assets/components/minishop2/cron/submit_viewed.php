<?php
/**
 * Created by PhpStorm.
 * User: portnovvit
 * Date: 04.02.2019
 * Time: 12:59
 */
$root = '/var/www/admin/www/petchoice.com.ua/';
error_reporting(E_ALL | E_STRICT);
ini_set('display_errors', 1);
define('MODX_API_MODE', true);
require_once $root . 'index.php';


$miniShop2 = $modx->getService('minishop2');
$miniShop2->initialize($modx->context->key);


$miniShop2->getUsersForViewedSubmit();

$q = $modx->newQuery('modUser');
//echo $daydate . ' ' . $daydate;
$now=date('Y-m-d H:i:s');
$date_15_days=date('Y-m-d H:i:s', strtotime('-15 days'));
$where ="modUser.mailsubmitviews!=1 AND modUser.last_mailviews BETWEEN '$date_15_days' AND '$now' ";
   /* array(
    'modUser.createdon:>=' => $daydatebegin,
    'modUser.mailsubmitviews:=' => 1
);*/
$q->select(array(
    'modUser.id'
));
//$q->sortby('msOrder.createdon', 'DESC');
$q->where($where);
$q->prepare();
$q->stmt->execute();
$result1 = $q->stmt->fetch(PDO::FETCH_ASSOC);
foreach ($result1 as $us){
    $user_id=$us['id'];
    if ($user_id!='') {
        $c = $modx->newQuery('modUser');
        $c->command('update');
        $c->set(array(
            'mailsubmitviews' => 0,
            'last_mailviews' => NULL,
        ));
        $c->where(array(
            'id' => $user_id,
        ));
        $c->prepare();
        $c->stmt->execute();
    }
}