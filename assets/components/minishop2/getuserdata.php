<?php
define('MODX_API_MODE', true);
require_once dirname(dirname(dirname(dirname(__FILE__)))) . '/index.php';


//error_reporting(E_ALL | E_STRICT);
//ini_set('display_errors', 1);
$userid=$_REQUEST['user_id'];
$data=array();
$output=array();
if ($userid)
{

	$user=$modx->getObject('modUser',$userid);
	if ($user){
	
		$userarray=$user->toArray();
		$profile = $user->getOne('Profile');
		$profilearray=$profile->toArray();
		
		$_SESSION['user_for_order']=$userarray+$profilearray;
		$extended = $profile->get('extended');
		$data['receiver']=$profilearray['fullname'];
		$data['lastname']=$extended['lastname'];
		$data['phone']=$profilearray['phone'];
		$data['email']=$profilearray['email'];
		$data['city']=$profilearray['city'];
		$data['city_id']=$extended['city_id'];
		$data['suggest_locality']=$profilearray['city'];//extended['lastname'];
		$data['delivery']=$extended['delivery'];
		$data['warehouse']=$extended['warehouse'];
		$data['street']=$extended['street'];
		$data['house']=$extended['house'];
        $data['parade']=$extended['parade'];
        $data['floor']=$extended['floor'];
		$data['subscribe']=$userarray['subscribe'];


        $data['street_np']=$extended['street_np'];
        $data['street_np_ref']=$extended['street_np_ref'];
        $data['street_id_np']=$extended['street_id_np'];
        $data['city_id_np']=$extended['city_id_np'];

		
		$data['room']=$extended['room'];
		
		$data['payment']=$extended['payment'];		
		
		
		$data['payments']=$extended['payments'];
		
		
		$deliveries_model = $modx->getCollection('msDelivery', array(
			'active' => 1,
			'class:!=' => '',
		));
		$output_delivery=$output_payment='';
		$city=$extended['city_id'];
		//echo $city.' '.$extended['delivery'];
		if (!empty($deliveries_model)) {			
			//$output_delivery.="<option value='' style='display:none;' disabled >выберите из списка</option>";//
			foreach ($deliveries_model as $delivery) {						
					$isAvailableForCity = $delivery->isAvailableForCity($city);				
					
					if ($isAvailableForCity === true) {
					$delar=array();
						if ($extended['delivery']&&($extended['delivery']!='')){ 
							$del=array('delivery'=>$extended['delivery']);
							$delar=$delivery->toArray();
							$delar=array_merge($delar,$del);
							
						}
						
						$output_delivery .= $modx->getChunk('deliveryTpl', $delar);//$delivery->toArray());
					}
			}
		}
		$data['deliveries']=$output_delivery;
		if (isset($extended['delivery'])){
			
			$q = $modx->newQuery('msPayment');//msDeliveryMember', array('delivery_id'=>$_REQUEST['id']));//msPayment');
			$q->innerJoin('msDeliveryMember', 'msDeliveryMember', array('`msDeliveryMember`.`payment_id` = `msPayment`.`id`',
			'`msDeliveryMember`.`delivery_id`'=>$extended['delivery']
			));						
			$payments=array();
			$q->groupby('`msPayment`.`id`');
			$q->select('*');
			//$output_payment.="<option value='' disabled style='display:none;'>выберите из списка</option>";//
			if ($q->prepare() && $q->stmt->execute()) {				
				while ($row = $q->stmt->fetch(PDO::FETCH_ASSOC)) {
					/*if ($extended['payment']&&($extended['payment']!='')){ 
							//$del=array('payment'=>$extended['payment']);
							
							//$delar=array_merge($delar,$del);
							
						}*/
					$output_payment.=$modx->getChunk('deliveryTpl', array('id'=>$row['msPayment_id'], 'payment'=>$extended['payment'],'name'=>$row['msPayment_name']));
				}          	   
				
			}
			$data['payments']=$output_payment;			
		}
		
		
	}
	
}
$myclassModelPath = MODX_CORE_PATH . 'components/minishop2/model/minishop2/';
$modx->getService('msDelivery', 'msDelivery', $myclassModelPath);
if (isset($_REQUEST['total_cost']))
{
	$total_order=$_REQUEST['total_cost'];
	
	$costdel=$modx->msDelivery->getcostdelivery($extended['delivery'],$extended['city_id'],$total_order);
	$cost_delivery=$costdel['cost_delivery'];	
	$infodelivery=$costdel['infodelivery'];
	$descdelivery=$costdel['descdelivery'];
	
	$data['infodelivery']=$infodelivery;
	$data['total_order_without_delivery']=$total_order;
	$total_order=$total_order+$cost_delivery;
	$data['total_order']=$total_order;
	
	$price1=$modx->runSnippet('msPriceDelimiter',array(
		'delimiter'=>'.',
		'price'=>(float)$total_order,
		'action'=>'left'
		));	
	$price2=$modx->runSnippet('msPriceDelimiter',array(
		'delimiter'=>'.',
		'price'=>(float)$total_order,
		'action'=>'right'
		));	
	$total_order_format=$price1.' <sup>'.$price2.'</sup>';
	
	$data['total_order_format']=$total_order_format;
	
	$data['descdelivery']=((($descdelivery!='')&&($descdelivery!=null))?$descdelivery:'');//,$descdelivery;
	if ($cost_delivery=='')$data['cost_delivery']='';
	elseif ($cost_delivery==0) $data['cost_delivery']='Бесплатно';
	else $data['cost_delivery']=$cost_delivery;
	
}
$discount=0;
if (isset($extended['discount']))$discount=$extended['discount'];

//echo $discount;
	if (!$user) $userid=null;
// необходимо обновить корзину с учётом дисконта клиента
$data['cart']=$modx->runSnippet('msCart',array(
		'tplRow'=>'cartRowTpl',
		'delivery'=>(isset($extended['delivery'])?$extended['delivery']:''),
		'payment'=>(isset($extended['payment'])?$extended['payment']:''),
		'infodeliver'=>$infodelivery,
		'costdeliver'=>$cost_delivery,
		'discount'=>$discount,
		'descdelivery'=>((($data['descdelivery']!='')&&($data['descdelivery']!=null))?$data['descdelivery']:''),		
		'order_cost_withdelivery'=>$total_order,
		'tplOuter'=>'cartOuterTpl',
		'userid'=>$userid
		));	
 

if (count($data)>0) {
$output['result']=true;
$output['data']=$data;
}
else {
$output['result']=false;
$output['data']=array();
}

//$output
echo json_encode($output);

@session_write_close();
?>