/*- Код
- Название
- Дата создания
- Активен
- Дата активации
- Заказ*/
promocode.grid.Items = function(config) {
    config = config || {};
	this.sm = new Ext.grid.CheckboxSelectionModel();
    Ext.applyIf(config,{
        id: 'modextra-grid-items'
        ,url: promocode.config.connector_url
		,fields: this.getFields(config)
		,columns: this.getColumns(config)
		//,fields: this.getFields(config)
		//,columns: this.getColumns(config)
        ,baseParams: {
            action: 'mgr/item/getlist'
        }
		,sm: this.sm
        //,fields: ['id','name','code','created','status','date_active', 'summa','discount','order_id']
        ,autoHeight: true
        ,paging: true
        ,remoteSort: true
        //,columns:  this.getColumns()
       /* ,tbar: [{
            text: _('modextra.item_create')
            ,handler: this.createItem
            ,scope: this
        }]
		*/
		,tbar: this.getTopBar(config)
    });
    promocode.grid.Items.superclass.constructor.call(this,config);
};
Ext.extend(promocode.grid.Items,MODx.grid.Grid,{
    windows: {}

    ,getMenu: function() {
        var m = [];
        m.push({
            text: _('modextra.item_update')
            ,handler: this.updateItem
        });
        m.push('-');
        m.push({
            text: _('modextra.item_remove')
            ,handler: this.removeItem
        });
        this.addContextMenuItem(m);
    }
    
    ,createItem: function(btn,e) {
        if (!this.windows.createItem) {
            this.windows.createItem = MODx.load({
                xtype: 'modextra-window-item-create'
                ,listeners: {
                    'success': {fn:function() { this.refresh(); },scope:this}
                }
            });
        }
        this.windows.createItem.fp.getForm().reset();
        this.windows.createItem.show(e.target);
    }
    ,updateItem: function(btn,e) {
        if (!this.menu.record || !this.menu.record.id) return false;
        var r = this.menu.record;

        if (!this.windows.updateItem) {
            this.windows.updateItem = MODx.load({
                xtype: 'modextra-window-item-update'
                ,record: r
                ,listeners: {
                    'success': {fn:function() { this.refresh(); },scope:this}
                }
            });
        }
        this.windows.updateItem.fp.getForm().reset();
        this.windows.updateItem.fp.getForm().setValues(r);
        this.windows.updateItem.show(e.target);
    }
    
    ,removeItem: function(btn,e) {
        if (!this.menu.record) return false;
        
        MODx.msg.confirm({
            title: _('modextra.item_remove')
            ,text: _('modextra.item_remove_confirm')
            ,url: this.config.url
            ,params: {
                action: 'mgr/item/remove'
                ,id: this.menu.record.id
            }
            ,listeners: {
                'success': {fn:function(r) { this.refresh(); },scope:this}
            }
        });
    },
	/*
	disableItem: function (act, btn, e) {
		var ids = this._getSelectedIds();
		if (!ids.length) {
			return false;
		}
		MODx.Ajax.request({
			url: this.config.url,
			params: {
				action: 'mgr/item/disable',
				ids: Ext.util.JSON.encode(ids),
			},
			listeners: {
				success: {
					fn: function () {
						this.refresh();
					}, scope: this
				}
			}
		})
	},

	enableItem: function (act, btn, e) {
		var ids = this._getSelectedIds();
		if (!ids.length) {
			return false;
		}
		MODx.Ajax.request({
			url: this.config.url,
			params: {
				action: 'mgr/item/enable',
				ids: Ext.util.JSON.encode(ids),
			},
			listeners: {
				success: {
					fn: function () {
						this.refresh();
					}, scope: this
				}
			}
		})
	},

	getFields: function (config) {
		return ['id', 'name', 'description', 'active', 'actions'];
	},*/

	getFields: function (config) {
		return ['id','name','code','created','status','date_active','removeloylnost','category', 'term','summa','type','discount','order_num'];
		//['id', 'name', 'description', 'active', 'actions'];
	},

	getColumns: function (config) {
		
		var all = {
			id: {width: 35}
			,name: {width: 100, sortable: true}//, renderer: miniShop2.utils.userLink}
			,code: {width: 100, sortable: true}//, renderer: {fn:this._renderCustomer,scope:this}, id: 'main'}
			,created: {width: 100, sortable: true}
			,status: {width: 75, sortable: true}//, renderer: miniShop2.utils.formatDate}
			,removeloylnost: {header:'Суммировать с лояльность',width: 75, sortable: true}
			,term: {width: 75, sortable: true, renderer: function(string) {
				
				if (string && string != '1970-01-01 00:00:00'&& string != '-1-11-30 00:00:00' && string != '0000-00-00 00:00:00' && string != 0) {
					var date = /^[0-9]+$/.test(string)
						? new Date(string * 1000)
						: new Date(string.replace(/(\d+)-(\d+)-(\d+)/, '$2/$3/$1'));

					return date.strftime(MODx.config.ms2_date_format);
				}
				else {
					return '&nbsp;';
				}
			}}
			,date_active: {width: 75, sortable: true, renderer: function(string) {
				
				if (string && string != '-1-11-30 00:00:00' && string != '0000-00-00 00:00:00' && string != 0) {
					var date = /^[0-9]+$/.test(string)
						? new Date(string * 1000)
						: new Date(string.replace(/(\d+)-(\d+)-(\d+)/, '$2/$3/$1'));

					return date.strftime(MODx.config.ms2_date_format);
				}
				else {
					return '&nbsp;';
				}
			}}
			,discount: {width: 75, sortable: true}//, renderer: this._renderCost}
			,type: {width: 75, sortable: true}
			,summa: {width: 75, sortable: true}
			,order_num: {width: 75, sortable: true}
			
		};

		var columns = [this.sm];//, this.exp];
		fields=this.getFields();
		for(var i=0; i < fields.length; i++) {
			var field = fields[i];//miniShop2.config.order_grid_fields[i];
			//alert(field);
			if (all[field]) {
				Ext.applyIf(all[field], {
					header: _(field)
					,dataIndex: field
				});
				columns.push(all[field]);
			}
		}

		return columns;
		
		return [{
            header: _('id')
            ,dataIndex: 'id'
			,sortable: true
            ,width: 70
        },{
            header: _('name')
            ,dataIndex: 'name'
			,sortable: true
            ,width: 150
        },{
            header: _('code')
            ,dataIndex: 'code'
			,sortable: true
            ,width: 200
        },{
            header: _('created')
            ,dataIndex: 'created'
			,sortable: true
            ,width: 150
        },{
            header: _('status')
            ,dataIndex: 'status'
			,sortable: true
            ,width: 100
        },{
            header: _('date_active')
            ,dataIndex: 'date_active'
			,sortable: true
            ,width: 150
        }
		,{            
            header: _('discount')
            ,dataIndex: 'discount'
			,sortable: true
            ,width: 150
        }		
		,{            
            header: _('summa')
            ,dataIndex: 'summa' 
,sortable: true			
			,width: 200			
        }		
		,{
            header: _('order_num')
            ,dataIndex: 'order_num'
			,sortable: true
            ,width: 150
        }
		,{
            header: 'Суммировать лояльность'
            ,dataIndex: 'removeloylnost'
			,sortable: true
            ,width: 150
        }
		];
		
		
		
		
		/*[{
			header: _('modextra_item_id'),
			dataIndex: 'id',
			sortable: true,
			width: 70
		}, {
			header: _('modextra_item_name'),
			dataIndex: 'name',
			sortable: true,
			width: 200,
		}, {
			header: _('modextra_item_description'),
			dataIndex: 'description',
			sortable: false,
			width: 250,
		}, {
			header: _('modextra_item_active'),
			dataIndex: 'active',
			renderer: modExtra.utils.renderBoolean,
			sortable: true,
			width: 100,
		}, {
			header: _('modextra_grid_actions'),
			dataIndex: 'actions',
			renderer: modExtra.utils.renderActions,
			sortable: false,
			width: 100,
			id: 'actions'
		}];*/
	},

	getTopBar: function (config) {
		return [{
				text: '<i class="'+ (MODx.modx23 ? 'icon icon-list' : 'bicon-list') + '"></i> ' + 'Действия'//_('ms2_bulk_actions')
				,menu: [
					{
					text: 'Удалить выбранные'//_('ms2_menu_remove_multiple')
					,handler: this.removeSelected
					,scope: this
				}]
			},{
            text: _('modextra.item_create')
            ,handler: this.createItem
            ,scope: this
        },/*{
			text: '<i class="icon icon-plus"></i>&nbsp;' + _('modextra_item_create'),
			handler: this.createItem,
			scope: this
		},*/ '->', {
			xtype: 'textfield',
			name: 'query',
			width: 200,
			id: config.id + '-search-field',
			emptyText: _('modextra_grid_search'),
			listeners: {
				render: {
					fn: function (tf) {
						tf.getEl().addKeyListener(Ext.EventObject.ENTER, function () {
							this._doSearch(tf);
						}, this);
					}, scope: this
				}
			}
		}, {
			xtype: 'button',
			id: config.id + '-search-clear',
			text: '<i class="icon icon-times">Очистить</i>',
			listeners: {
				click: {fn: this._clearSearch, scope: this}
			}
		}];
	},
	_doSearch: function (tf, nv, ov) {
		this.getStore().baseParams.query = tf.getValue();
		this.getBottomToolbar().changePage(1);
		this.refresh();
	},

	_clearSearch: function (btn, e) {
		this.getStore().baseParams.query = '';
		Ext.getCmp(this.config.id + '-search-field').setValue('');
		this.getBottomToolbar().changePage(1);
		this.refresh();
	}
	,removeSelected: function(btn,e) {
		var cs = this.getSelectedAsList();
		if (cs === false) return false;

		MODx.msg.confirm({
			title: 'Удалить выбраные'//_('ms2_menu_remove_multiple')
			,text: 'Точно удалить'//_('ms2_menu_remove_multiple_confirm')
			,url: this.config.url
			,params: {
				action: 'mgr/item/remove_multiple'
				,ids: cs
			}
			,listeners: {
				success: {fn:function(r) {
					this.getSelectionModel().clearSelections(true);
					this.refresh();
				},scope:this}
			}
		});
		return true;
	}
});
Ext.reg('modextra-grid-items',promocode.grid.Items);


// Комбобоксы статусов, складов и категорий товаров
/*var storeitems = new Ext.data.ArrayStore({
    id: 'type'
    ,fields: ['value',{name: 'name', type: 'string']}
    ,data: [[1,'Статический'],[0,'Динамический']]
});*/
promocode.combo.type = function(config) {
	config = config || {};
	/*Ext.applyIf(config,{
		store: new Ext.data.SimpleStore({
			fields: ['id', 'name']
			,data: [[1,'Статический'],[0,'Динамический']]
		})
		,emptyText: ''
		,displayField: 'name'
		,valueField: 'id'
		,hiddenName: 'id'
		//,mode: 'local'
		//,triggerAction: 'all'
		//,editable: false
		//,selectOnFocus: false
		//,preventRender: true
		//,forceSelection: true
		//,enableKeyEvents: true
	});*/
	
	 Ext.applyIf(config,{
		name: 'type'		
       ,store: new Ext.data.SimpleStore({
			fields: ['id', 'name'],
			data: [['1','Group1'],['2','Group2'],['3','Group3'],['4','Group4']]
		})
		,displayField: 'name'
		,valueField: 'id'
		,fields: ['id','name']
		,pageSize: 20		
		,itemSelector: 'div.promocode-type-item'
	});
	
	

	
	promocode.combo.type.superclass.constructor.call(this,config);
};
Ext.extend(promocode.combo.type,MODx.combo.ComboBox);

Ext.reg('promocode-combo-type',promocode.combo.type);



promocode.combo.CategoryCombo = function(config) {
    config = config || {};
    Ext.applyIf(config,{
        name: 'category'
        ,hiddenName: 'category'
        ,displayField: 'name'
        ,valueField: 'id'
        ,url: promocode.config.connectorUrl
        ,baseParams: { action: 'mgr/category/getlist' }
        ,fields: ['id','name']
        ,pageSize: 20        
        ,typeAhead: true
        ,editable: true
    });
    promocode.combo.CategoryCombo.superclass.constructor.call(this,config);
};
Ext.extend(promocode.combo.CategoryCombo,MODx.combo.ComboBox);
Ext.reg('promocode-combo-categorycombo',promocode.combo.CategoryCombo);


promocode.window.CreateItem = function(config) {
    config = config || {};
    this.ident = config.ident || 'modextra-mecitem'+Ext.id();
    Ext.applyIf(config,{
        title: _('modextra.item_create')
        ,id: this.ident
        ,height: 150
        ,width: 475
        ,url: promocode.config.connector_url
        ,action: 'mgr/item/create'
        ,fields: [
		{
            xtype: 'textfield'
            ,fieldLabel: _('name')
            ,name: 'name'
            ,id: this.ident+'-name'
            ,anchor: '100%'
        },		
		/*{
            xtype: 'datefield'
            ,fieldLabel: _('term')
            ,name: 'term'
			,decimalPrecision: 2
            ,startDay: 1
			,minValue: new Date()
            //,disabledDates: disableDates
            ,id: this.ident+'-term'
            ,anchor: '100%'
        },*/
		{
            
			xtype: 'xdatetime'
			,fieldLabel: _('term')
			//,description: '<b>[[*publishedon]]</b><br />'+_('resource_publishedon_help')
			,name: 'term'
			,id: this.ident+'-term'
			,allowBlank: true
			,dateFormat: MODx.config.manager_date_format
			,timeFormat: MODx.config.manager_time_format
			,startDay: parseInt(MODx.config.manager_week_start)
			,dateWidth: 120
			,timeWidth: 120
			
        },
		{
            xtype: 'textfield'
            ,fieldLabel: _('code')
            ,name: 'code'
            ,id: this.ident+'-code'
            ,anchor: '100%'
        }		
		, {
			xtype: 'minishop2-combo-category',//minishop2-combo-user',//minishop2-combo-options',//minishop2-combo-category',//promocode-combo-options',//promocode-tree-categories',//promocode-combo-categorycombo',
			fieldLabel: _('category'),
			name: 'category',
			hiddenName: 'category',
			id: this.ident + '-category',
			anchor: '99%',
			allowBlank: false
			
		}		
		,{			
			xtype : 'combo'
			,editable : false
			,width : 40
			,fieldLabel : _('type')
			,name : 'type'
			,valueField : 'id'
			,hiddenName: 'type'
			,displayField : 'type'
			,triggerAction:'all'
			,allowBlank : false
			,mode:'local'
			,anchor: '50%'
			,store: new Ext.data.ArrayStore({
				id: this.ident+'-type'
				,fields: ['id','type']
				,data: [['static','Статический'],['dinamic','Динамический']]				
			})
		}
		,{xtype: 'combo-boolean',fieldLabel: 'Суммировать лояльность',name: 'removeloylnost',hiddenName: 'removeloylnost',id: 'tbar-minishop2-combo-removeloylnost',anchor: '50%'}		
		,{xtype: 'combo-boolean',fieldLabel: _('status'),name: 'status',hiddenName: 'status',id: 'tbar-minishop2-combo-status',anchor: '50%'}		
		,{
            xtype: 'textfield'
            ,fieldLabel: _('discount')
            ,name: 'discount'
            ,id: this.ident+'-discount'
            ,anchor: '100%'
        }
		,{
            xtype: 'numberfield'
            ,fieldLabel: _('count')
            ,name: 'count'
            ,id: this.ident+'-count'
            ,anchor: '100%'
        }
		,{
            xtype: 'numberfield'
            ,fieldLabel: _('summa')
            ,name: 'summa'
            ,id: this.ident+'-summa'
            ,anchor: '100%'
        }
		]
    });
    promocode.window.CreateItem.superclass.constructor.call(this,config);
};
Ext.extend(promocode.window.CreateItem,MODx.Window);
Ext.reg('modextra-window-item-create',promocode.window.CreateItem);


promocode.window.UpdateItem = function(config) {
    config = config || {};
    this.ident = config.ident || 'modextra-meuitem'+Ext.id();
    Ext.applyIf(config,{
        title: _('modextra.item_update')
        ,id: this.ident
        ,height: 150
        ,width: 475
        ,url: promocode.config.connector_url
        ,action: 'mgr/item/update'
        ,fields: [
		{
				xtype: 'hidden'
				,name: 'id'
			},
		{
            xtype: 'textfield'
            ,fieldLabel: _('name')
            ,name: 'name'
            ,id: this.ident+'-name'
            ,anchor: '100%'
        },		
		{
            
			xtype: 'xdatetime'
			,fieldLabel: _('term')
			//,description: '<b>[[*publishedon]]</b><br />'+_('resource_publishedon_help')
			,name: 'term'
			,id: 'modx-resource-term'
			,allowBlank: true
			,dateFormat: MODx.config.manager_date_format
			,timeFormat: MODx.config.manager_time_format
			,startDay: parseInt(MODx.config.manager_week_start)
			,dateWidth: 120
			,timeWidth: 120
			,value: config.record.term
        },
		
		//,publishedon: {width:50, sortable:true, editor:{xtype:'minishop2-xdatetime', timePosition:'below'}, renderer: miniShop2.utils.formatDate}
		{
            xtype: 'textfield'
            ,fieldLabel: _('code')
            ,name: 'code'
            ,id: this.ident+'-code'
            ,anchor: '100%'
        }		
		, {
			xtype: 'minishop2-combo-category',//minishop2-combo-user',//minishop2-combo-options',//minishop2-combo-category',//promocode-combo-options',//promocode-tree-categories',//promocode-combo-categorycombo',
			fieldLabel: _('category'),
			name: 'category',
			hiddenName: 'category',
			id: this.ident + '-category',
			value: config.record.category,
			anchor: '99%',
			allowBlank: false
			//,value: config.record.category
		}
/*
var storeitems = new Ext.data.ArrayStore({
    id: 'advert-items'
    ,fields: ['value',{name: 'name', type: 'string'}]
    ,data: [[1,'Да'],[0,'Нет']]
});
advert.combo.Items = function(config) {
    config = config || {};
    Ext.applyIf(config,{
        store: storeitems
        ,displayField: 'name'
        ,valueField: 'value'
        ,hiddenName: 'поле в базе=из mysql map' //не забудьте поменять
        ,mode: 'local'
        ,triggerAction: 'all'
        ,editable: false
        ,selectOnFocus: false
        ,preventRender: true
        ,forceSelection: true
        ,enableKeyEvents: true
    });
    advert.combo.Items.superclass.constructor.call(this,config);
};		*/
		/*,{			
			xtype : 'combo'
			//,editable : false
			,width : 40
			,fieldLabel : _('type')
			,name : 'type'
			,valueField : 'value'
			,hiddenName: 'type'
			,displayField : 'name'
			//,triggerAction:'all'
			//,allowBlank : false
			  ,triggerAction: 'all'
			,editable: false
			,selectOnFocus: false
			,preventRender: true
			,forceSelection: true
			,enableKeyEvents: true
			,mode:'local'
			,anchor: '50%'			
			,store: new Ext.data.ArrayStore({
				id : this.ident + '-type'
				//,fields: ['id','type']
				,fields: ['value',{name: 'name', type: 'string'}]
				//,data : [[1,"Y"],[2,"N"]]
				,data: [['static','Статический'],['dinamic','Динамический']]				
			})
		}
		,{
            xtype: 'displayfield'
            ,fieldLabel: _('type')
            ,name: 'type'
            ,id: this.ident+'-type'
            ,anchor: '100%'
			
        }*/		
		,{xtype: 'combo-boolean',fieldLabel: 'Суммировать лояльность',name: 'removeloylnost',hiddenName: 'removeloylnost',id: this.ident + '-removeloylnost',anchor: '50%'}		
		,{xtype: 'combo-boolean',fieldLabel: _('status'),name: 'status',hiddenName: 'status',id: this.ident + '-status',anchor: '50%'}		
		,{
            xtype: 'textfield'
            ,fieldLabel: _('discount')
            ,name: 'discount'
            ,id: this.ident+'-discount'
            ,anchor: '100%'
        },{
            xtype: 'numberfield'
            ,fieldLabel: _('summa')
            ,name: 'summa'
            ,id: this.ident+'-summa'
            ,anchor: '100%'
        }
		,{
            xtype: 'displayfield'
            ,fieldLabel: _('order_num')
            ,name: 'order_num'
            ,id: this.ident+'-order_num'
            ,anchor: '100%'
			
        }
		,{
            xtype: 'displayfield'
            ,fieldLabel: _('type')
            ,name: 'type'
            ,id: this.ident+'-type'
            ,anchor: '100%'
			
        }
		,{
            xtype: 'displayfield'
            ,fieldLabel: _('date_active')
            ,name: 'date_active'
			,format: 'yyyy-MM-dd HH:mm'			
            ,id: this.ident+'-date_active'
            ,anchor: '100%'
        },{
            xtype: 'displayfield'
            ,fieldLabel: _('created')
            ,name: 'created'
            ,id: this.ident+'-created'
            ,anchor: '100%'
        }
		/*,{
            xtype: 'numberfield'
            ,fieldLabel: _('count')
            ,name: 'count'
            ,id: this.ident+'-count'
            ,anchor: '100%'
        }*/
		
		]
    });
    promocode.window.UpdateItem.superclass.constructor.call(this,config);
};
Ext.extend(promocode.window.UpdateItem,MODx.Window);
Ext.reg('modextra-window-item-update',promocode.window.UpdateItem);