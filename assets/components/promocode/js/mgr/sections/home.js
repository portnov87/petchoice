Ext.onReady(function() {
    MODx.load({ xtype: 'modextra-page-home'});
});

promocode.page.Home = function(config) {
    config = config || {};
    Ext.applyIf(config,{
        components: [{
            xtype: 'modextra-panel-home'
            ,renderTo: 'modextra-panel-home-div'
        }]
    }); 
    promocode.page.Home.superclass.constructor.call(this,config);
};
Ext.extend(promocode.page.Home,MODx.Component);
Ext.reg('modextra-page-home',promocode.page.Home);