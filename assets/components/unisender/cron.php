<?php
/**
 * Created by PhpStorm.
 * User: portn
 * Date: 08.11.2017
 * Time: 18:23
 */
require_once dirname(dirname(dirname(dirname(__FILE__)))).'/config.core.php';
require_once MODX_CORE_PATH.'config/'.MODX_CONFIG_KEY.'.inc.php';
require_once MODX_CONNECTORS_PATH.'index.php';

$namespace = $modx->getObject('modNamespace', 'unisender');
$corePath = MODX_CORE_PATH.'components/unisender/';
require_once $corePath.'model/unisender/unisender.class.php';

$modelunisender = new Unisender($modx);



$q = $modx->newQuery('modUser');
$q->select([
    'modUser.username',
    'modUser.id',
    'modUser.registr',
    'modUser.created',
    'modUser.registr_date',
    'modUser.subscribe',
    'modUser.last_order',
    'modUser.total_orders',
]);
//$q->innerJoin('modUserProfile', 'modUserProfile', 'modUser.id=modUserProfile.internalKey');
//$q->limit(10);
$q->sortby('modUser.created', 'DESC');
//$q->groupby('modUser.id');//modUser.id');//msOrder.id');
//$q->where($where);
//$q->where(array('modUser.email' => $id));
//$q->limit(10);
$q->prepare();
$q->stmt->execute();
//echo $q->toSQL();

$result = $q->stmt->fetchAll(PDO::FETCH_ASSOC);
/*echo "<pre>result";
print_r($result);
echo "</pre>";*/

foreach ($result as $res) {
    $user=$res;
    $profile_object=$modx->getObject('modUserProfile', array('internalKey'=> $res['id']));
    $profile=[];
    if ($profile_object)
        $profile=$profile_object->toArray();

  /*  echo "<pre>".$res['id'];
    print_r( $profile);
    echo "</pre>";
    break;*/

  if ($profile['email']!='') {

      $result = $modelunisender->UpdateSubscribeUser($user, $profile);
  }
    //if ($result) break;
    sleep(0.5);
}