<?php
define('MODX_API_MODE', true);
require_once dirname(dirname(dirname(dirname(__FILE__)))) . '/index.php';

$modx->getService('error', 'error.modError');
$modx->setLogLevel(modX::LOG_LEVEL_ERROR);
$modx->setLogTarget('FILE');

$output = array();

if (!empty($_REQUEST['name'])) {
	$name = $_REQUEST['name'];

	//$cityRef=$_REQUEST['city_id'];
    $cityRef=false;
    $cityName=$_REQUEST['city_name'];
    $_cityName=explode('-',$cityName);
    if (isset($_cityName[0]))
        $cityName=trim($_cityName[0]);

    if (!isset($_REQUEST['city_id'])) {
//$cityRef=$_REQUEST['city_id'];
        $city_exist = $modx->getObject('msNpCities', ['DescriptionRu' => $cityName]);//array('id' => $ref));


        if ($city_exist) {
            $cityRef = $city_exist->get('id');
        }
    }else
        $cityRef=$_REQUEST['city_id'];



    if ($cityRef) {

        $query = $modx->newQuery('msNpStreets');

        $query->select('msNpStreets.id,msNpStreets.StreetsType,msNpStreets.DescriptionRu,msNpStreets.Description');
        $query->limit(10);
        //echo $name.$name;
        $query->where(array(
            'Description:LIKE' => $name . "%",
            "CityRef:=" => $cityRef
        ));
        $query->sortby('Description', 'ASC');

        $data = array();
        if ($query->prepare() && $query->stmt->execute()) {
            $result = $query->stmt->fetchAll(PDO::FETCH_ASSOC);

            foreach ($result as $res) {
                $data[][$res['id']] = $res['StreetsType'] . ' ' . $res['Description'];
            }
        }

        $count = count($data);
        $output['code'] = 1;
        $output['content']['records'] = $data;
        $output['content']['text'] = $name;
        $output['content']['count'] = $count;//count($data);
    }else{
        $output['code'] = 0;
        $output['content']['records'] = [];
        $output['content']['text'] = $name;
        $output['content']['count'] = $count;
    }
}

echo json_encode($output);

@session_write_close();

?>