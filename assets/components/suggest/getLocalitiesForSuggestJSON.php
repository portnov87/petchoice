<?php
define('MODX_API_MODE', true);
require_once dirname(dirname(dirname(dirname(__FILE__)))) . '/index.php';

$modx->getService('error', 'error.modError');
$modx->setLogLevel(modX::LOG_LEVEL_ERROR);
$modx->setLogTarget('FILE');


$polylang = $modx->getService('polylang', 'Polylang');
$polyLangTools = $polylang->getTools();
$language = false;
$language = $polyLangTools->detectLanguage(true);



$output = array();

if (!empty($_REQUEST['name'])) {
	$name = $_REQUEST['name'];
	switch ($name)
	{
		case 'Днепропетровск':
		$name='Днепр';
		break;
		case 'Днепродзержинск':
		$name='Каменское';
		break;
		case 'Ильичевск':
		$name='Черноморск';
		break;
	}
	
	//$output['success'] = true;
	//$output['message'] = '';

    $query = $modx->newQuery('msNpCities');

    $query->select('*');

    $query->limit(10);
    //echo $name.$name;
    $query->where(array('DescriptionRu:LIKE' => $name . "%","OR:Description:LIKE"=>$name."%"));
    $query->sortby('DescriptionRu','ASC');
    $query->sortby('Description','ASC');
    $query->sortby('AreaDescriptionRu','ASC');



    if ($query->prepare() && $query->stmt->execute()) {
        $result = $query->stmt->fetchAll(PDO::FETCH_ASSOC);

        foreach ($result as $res) {

            if (!empty($res['msNpCities_Description'])) {

                if ($language->get('culture_key')=='ua')
                {
                    $nameCity=$res['msNpCities_Description'];
                }
                else $nameCity=$res['msNpCities_DescriptionRu'];


                $area = $modx->getObject('msNpArea', ['Ref' => $res['msNpCities_Area']]);
                $region = '';
                if ($area) {
                    if (!empty($area->get('DescriptionRu'))) {
                        if ($language->get('culture_key')=='ua')
                        {
                            $region = ' - ' . $area->get('Description') . ' обл. ';
                        }else {
                            $region = ' - ' . $area->get('DescriptionRu') . ' обл. ';
                        }
                    }
                }
                $data[][$res['msNpCities_Ref']] = $nameCity . $region;
            }

        }

    }



	if (count($data)==0)
	{
        $data[]=array('0'=>'Город не найден. Проверьте написание или обратитесь к менеджеру');//Город не найден. Проверьте написание или обратитесь к менеджеру');

        $count=1;
	}else $count=count($data);

	$output['code'] = 1;
	$output['content']['records'] = $data;
	$output['content']['text'] = $name;
	$output['content']['count'] = $count;//count($data);
} else {
    $data = array();
	/*$output['success'] = false;
	$output['message'] = 'Произошла ошибка при выполнении запроса.';*/

    $query = $modx->newQuery('msNpCities');

    $query->select('*');
    $query->limit(10);
    $query->where(array('DescriptionRu:=' => "Одесса","OR:DescriptionRu:="=>'Киев',
        "OR:DescriptionRu:="=>'Днепр',"OR:DescriptionRu:="=>'Запорожье',"OR:DescriptionRu:="=>'Харьков'));
    $query->sortby('DescriptionRu','ASC');
    $query->sortby('Description','ASC');
    $query->sortby('AreaDescriptionRu','ASC');
    if ($query->prepare() && $query->stmt->execute()) {
        $result = $query->stmt->fetchAll(PDO::FETCH_ASSOC);
        foreach ($result as $res)
        {

            if ($language->get('culture_key')=='ua') {
                $nameCity = $res['msNpCities_Description'];
            }else{
                $nameCity = $res['msNpCities_DescriptionRu'];
            }

            $area = $modx->getObject('msNpArea', ['Ref' => $res['msNpCities_Area']]);
            $region = '';
            if ($area) {
                if (!empty($area->get('DescriptionRu'))) {
                    if ($language->get('culture_key')=='ua')
                    {
                        $region = ' - ' . $area->get('Description') . ' обл. ';
                    }else {
                        $region = ' - ' . $area->get('DescriptionRu') . ' обл. ';
                    }
                }
            }


            $data[][$res['msNpCities_Ref']] = $nameCity . $region;

            //$data[] = ['id' => $res['msNpCities_Ref'], 'label' => $nameCity];
            //$data[$res['msNpCities_Ref']] = $nameCity;
        }

    }

    //$output=$data;
        //$data[]=array(30=>'Одесса',381=>'Киев',267=>'Днепр',313=>'Запорожье',930=>'Харьков');
	$output['code'] = 1;
	$output['content']['records'] = $data;
	$output['content']['text'] = '';
	$output['content']['count'] = 5;//$count;
}

echo json_encode($output);

@session_write_close();

?>