<?php
define('MODX_API_MODE', true);
require_once dirname(dirname(dirname(dirname(__FILE__)))) . '/index.php';

$modx->getService('error', 'error.modError');
$modx->setLogLevel(modX::LOG_LEVEL_ERROR);
$modx->setLogTarget('FILE');

$output = array();

if (!empty($_REQUEST['pet_type'])) {
	$output['success'] = true;
	$output['message'] = '';
	$output['data'] = $modx->runSnippet('getPetBreeds', array(
		'type' => $_REQUEST['pet_type'],
		'tpl' => 'tpl.petTypeRow',
	));
} else {
	$output['success'] = false;
	$output['message'] = 'Произошла ошибка при выполнении запроса.';
}

echo json_encode($output);

@session_write_close();

?>