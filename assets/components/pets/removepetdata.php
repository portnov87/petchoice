<?php
define('MODX_API_MODE', true);
require_once dirname(dirname(dirname(dirname(__FILE__)))) . '/index.php';

$modx->getService('error', 'error.modError');
$modx->setLogLevel(modX::LOG_LEVEL_ERROR);
$modx->setLogTarget('FILE');

$output = array();

if (!empty($_REQUEST['id'])) {
	$output['success'] = true;
	$output['message'] = 'Информация успешно удалена';
	$pet = $modx->getObject('msPet', $_REQUEST['id']);
	if ($pet->remove() == false) {
		$output['message'] = 'An error occurred while trying to remove the pet!';
	} else {
		$output['message'] = 'Информация успешно удалена';
	}
} else {
	$output['success'] = false;
	$output['message'] = 'Произошла ошибка при выполнении запроса.';
}

echo json_encode($output);

@session_write_close();

?>