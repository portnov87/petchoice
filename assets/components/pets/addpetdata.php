<?php
define('MODX_API_MODE', true);
require_once dirname(dirname(dirname(dirname(__FILE__)))) . '/index.php';

$modx->getService('error', 'error.modError');
$modx->setLogLevel(modX::LOG_LEVEL_ERROR);
$modx->setLogTarget('FILE');

if (empty($_POST['type'])) {
	//return $AjaxForm->error('Ошибки в форме', array('type' => 'Вы не выбрали тип животного'));
}
if (empty($_POST['breed'])) {
	//return $AjaxForm->error('Ошибки в форме', array('breed' => 'Вы не выбрали породу животного'));
}
if (empty($_POST['genre'])) {
	//return $AjaxForm->error('Ошибки в форме', array('genre' => 'Вы не выбрали пол животного'));
} else {
	$type = $_POST['type'];
	$breed = $_POST['breed'];
	$genre = $_POST['genre'];
	$bday = $_POST['day'];
	$bmonth = $_POST['month'];
	$byear = $_POST['year'];
	$name = $_POST['name'];
	$comment = $_POST['comment'];
    $add_myself = 1;//$_POST['comment'];

	$created=date("Y-m-d H:m:s");
	$user = &$modx->user;
	$userId = $user->get('id');

	$pet = $modx->newObject('msPet');

	$_current_photo = $pet->get('photo');
	$default_params = array('w' => 250, 'h' => 250, 'bg' => 'ffffff', 'q' => 95, 'zc' => 1, 'f' => 'jpg');
	$params = $modx->fromJSON($avatarParams);
	if (!is_array($params)) {
		$params = array();
	}
	$params = array_merge($default_params, $params);

	$path = 'images/pets/';
	$file = strtolower(md5($pet->get('id') . time()) . '.' . $params['f']);

	$url = MODX_ASSETS_URL . $path . $file;
	$dst = MODX_ASSETS_PATH . $path . $file;

	// Check image dir
	$tmp = explode('/', str_replace(MODX_BASE_PATH, '', MODX_ASSETS_PATH . $path));
	$dir = rtrim(MODX_BASE_PATH, '/');
	foreach ($tmp as $v) {
		if (empty($v)) {continue;}
		$dir .= '/' . $v;
		if (!file_exists($dir) || !is_dir($dir)) {
			@mkdir($dir);
		}
	}
	if (!file_exists(MODX_ASSETS_PATH . $path) || !is_dir(MODX_ASSETS_PATH . $path)) {
		$modx->log(modX::LOG_LEVEL_ERROR, '[Office] Could not create images dir "' . MODX_ASSETS_PATH . $path . '"');
		return false;
	}

	// Remove image
	if (!empty($_current_photo) && isset($_POST['photo']) && empty($_POST['photo'])) {
		$tmp = explode('/', $_current_photo);
		if (!empty($tmp[1])) {
			$cur = MODX_ASSETS_PATH . $path . end($tmp);
			if (!empty($cur) && file_exists($cur)) {
				@unlink($cur);
			}
		}
		$pet->set('photo', '');
	}
	// Upload a new one
	elseif (!empty($_FILES['newphoto']) && preg_match('/image/', $_FILES['newphoto']['type']) && $_FILES['newphoto']['error'] == 0) {
		move_uploaded_file($_FILES['newphoto']['tmp_name'], $dst);

		$phpThumb = $modx->getService('modphpthumb', 'modPhpThumb', MODX_CORE_PATH . 'model/phpthumb/', array());
		$phpThumb->setSourceFilename($dst);
		foreach ($params as $k => $v) {
			$phpThumb->setParameter($k, $v);
		}
		if ($phpThumb->GenerateThumbnail()) {
			if ($phpThumb->renderToFile($dst)) {
				if (!empty($cur) && file_exists($cur)) {@unlink($cur);}
				$pet->set('photo', $url);
				$_POST['newphoto'] = $url;
			} else {
				$modx->log(modX::LOG_LEVEL_ERROR, '[Office] Could not save rendered image to "' . $dst . '"');
			}
		} else {
			$modx->log(modX::LOG_LEVEL_ERROR, '[Office] ' . print_r($phpThumb->debugmessages, true));
		}
	}

	$pet->fromArray(array(
		'type' => $type,
		'breed' => $breed,
		'genre' => $genre,
		'bday' => $bday,
		'bmonth' => $bmonth,
		'byear' => $byear,
		'name' => $name,
		'comment' => $comment,
		'user_id' => $userId,
        'created'=>$created,
        'updated'=>$created,
        'add_myself'=>$add_myself
	));
	$pet->save();

    $sql = "SELECT p.* FROM modx_users u 
                    	LEFT JOIN modx_ms2_pets p 
                    				ON u.id=p.user_id 
WHERE p.user_id ='".$userId."' AND p.add_myself=0";

    $q = $modx->prepare($sql);
    $q->execute(array(0));
    $user_pets_not_myself = $q->fetchAll(PDO::FETCH_ASSOC);

    foreach ($user_pets_not_myself as $_pet)
    {
        $pet_id=$_pet['id'];

        $pet_object = $modx->getObject('msPet', $pet_id);
        if ($pet_object){

            $pet_object->remove();
        }
    }



	$_POST['pet_id'] = $pet->get('id');

	$output = $modx->runSnippet('getPet', array(
		'id' => $pet->get('id'),
		'tpl' => 'tpl.UpdatePetForm',
	));

	//return $AjaxForm->success('',$output);
	return $output;
}

echo json_encode($_POST);

@session_write_close();

?>