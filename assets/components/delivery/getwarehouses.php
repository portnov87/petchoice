<?php
define('MODX_API_MODE', true);

require_once dirname(dirname(dirname(dirname(__FILE__)))) . '/index.php';


$modx->getService('error', 'error.modError');
$modx->setLogLevel(modX::LOG_LEVEL_ERROR);
$modx->setLogTarget('FILE');

$city_id = $_REQUEST['city_id'];
$poshtomat = (isset($_REQUEST['poshtomat']) ? $_REQUEST['poshtomat'] : 0);//(int) $_REQUEST['poshtomat'];
$delivery_id = (int)$_REQUEST['delivery_id'];
//if ($delivery_id==6)
//    $poshtomat=1;



$polylang = $modx->getService('polylang', 'Polylang');
$polyLangTools = $polylang->getTools();
$language = false;
$language = $polyLangTools->detectLanguage(true);




$output = '';
$results = [];

if (!empty($city_id) && $delivery_id > 0) {
    $delivery = $modx->getObject('msDelivery', $delivery_id);
    $warehouses = $delivery->getWarehouses($city_id, $poshtomat);

    foreach ($warehouses as $warehouse) {
        $data = array();
        /*echo '<pre>';
        print_R($warehouse);
        echo '</pre>';*/
        //if ((strpos($warehouse['DescriptionRu'],'Почтомат')===false)&&(strpos($warehouse['DescriptionRu'],'Поштомат')===false)){

        if (($_REQUEST['idwar'] != '') && (isset($_REQUEST['idwar']))) {
            $data['warehouse'] = $_REQUEST['idwar'];

        }



        if ($language->get('culture_key')=='ua')
        {
            $nameWarehouse=$warehouse['name_ua'];
        }
        else $nameWarehouse=$warehouse['name'];


        $weight = $warehouse['TotalMaxWeightAllowed'];
        $data['id'] = $warehouse['Ref'];//Number'];
        $data['description'] = $nameWarehouse;//$warehouse['name'];
        $data['max_weight'] = ($weight > 0 ? 'Вес от ' . $weight : '');


        //$results[] = $data;
        //max_weight

        $output .= $modx->getChunk('warehouseTpl', $data);
        //}
    }
}

echo json_encode($output);

@session_write_close();
?>