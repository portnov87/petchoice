<?php
define('MODX_API_MODE', true);
require_once dirname(dirname(dirname(dirname(__FILE__)))) . '/index.php';

$modx->getService('error', 'error.modError');
$modx->setLogLevel(modX::LOG_LEVEL_ERROR);
$modx->setLogTarget('FILE');

if (isset($_POST['user_id'])){//&&(isset($_POST['photo']))
    $user_id=$_POST['user_id'];
    $profile = $modx->getObject('modUserProfile', ['internalKey'=>$user_id]);
    $_current_photo = $profile->get('photo');
    $default_params = array('w' => 250, 'h' => 250, 'bg' => 'ffffff', 'q' => 95, 'zc' => 1, 'f' => 'jpg');
    $params =[];// $modx->fromJSON($avatarParams);
    if (!is_array($params)) {
        $params = array();
    }
    $params = array_merge($default_params, $params);

    $path = 'images/users/';
    $file = strtolower(md5($profile->get('internalKey') . time()) . '.' . $params['f']);

    $url = MODX_ASSETS_URL . $path . $file;
    $dst = MODX_ASSETS_PATH . $path . $file;

    // Check image dir
    $tmp = explode('/', str_replace(MODX_BASE_PATH, '', MODX_ASSETS_PATH . $path));
    $dir = rtrim(MODX_BASE_PATH, '/');
    foreach ($tmp as $v) {
        if (empty($v)) {continue;}
        $dir .= '/' . $v;
        if (!file_exists($dir) || !is_dir($dir)) {
            @mkdir($dir);
        }
    }
    if (!file_exists(MODX_ASSETS_PATH . $path) || !is_dir(MODX_ASSETS_PATH . $path)) {
        $modx->log(modX::LOG_LEVEL_ERROR, '[Office] Could not create images dir "' . MODX_ASSETS_PATH . $path . '"');
        return false;
    }

    // Remove image
    if (!empty($_current_photo) && isset($_POST['photo']) && empty($_POST['photo'])) {
        $tmp = explode('/', $_current_photo);
        if (!empty($tmp[1])) {
            $cur = MODX_ASSETS_PATH . $path . end($tmp);
            if (!empty($cur) && file_exists($cur)) {
                @unlink($cur);
            }
        }
        $profile->set('photo', '');
    }
    // Upload a new one
    elseif (!empty($_FILES['newphoto']) && preg_match('/image/', $_FILES['newphoto']['type']) && $_FILES['newphoto']['error'] == 0) {
        move_uploaded_file($_FILES['newphoto']['tmp_name'], $dst);

        $phpThumb = $modx->getService('modphpthumb', 'modPhpThumb', MODX_CORE_PATH . 'model/phpthumb/', array());
        $phpThumb->setSourceFilename($dst);
        foreach ($params as $k => $v) {
            $phpThumb->setParameter($k, $v);
        }
        if ($phpThumb->GenerateThumbnail()) {
            if ($phpThumb->renderToFile($dst)) {
                if (!empty($cur) && file_exists($cur)) {@unlink($cur);}
                $profile->set('photo', $url);
                $_POST['newphoto'] = $url;
            } else {
                $modx->log(modX::LOG_LEVEL_ERROR, '[Office] Could not save rendered image to "' . $dst . '"');
            }
        } else {
            $modx->log(modX::LOG_LEVEL_ERROR, '[Office] ' . print_r($phpThumb->debugmessages, true));
        }
    }
/*
    $profile->fromArray(array(
        'photo' => $type,
        'breed' => $breed,
        'genre' => $genre,
        'bday' => $bday,
        'bmonth' => $bmonth,
        'byear' => $byear,
        'name' => $name,
        'comment' => $comment,
        'user_id' => $userId,
    ));*/
    $profile->save();

    //return $AjaxForm->success('Информация успешно обновлена', $_POST);
}

echo json_encode($_POST);

@session_write_close();

?>