<?php

error_reporting(E_ALL & ~E_NOTICE);

if (!isset($modx)) {
    define('MODX_API_MODE', true);
    require_once dirname(dirname(dirname(dirname(__FILE__)))) . '/index.php';
    $modx->getService('error', 'error.modError');
    $modx->getRequest();
    $modx->setLogLevel(modX::LOG_LEVEL_ERROR);
    $modx->setLogTarget('FILE');
    $modx->error->message = null;
}

if (empty($_REQUEST['action'])) {
    exit($modx->toJSON(array('success' => false, 'message' => 'Access denied')));
} else {
    $action = $_REQUEST['action'];
}
/** @var pdoFetch $pdoFetch */
$fqn = $modx->getOption('pdoFetch.class', null, 'pdotools.pdofetch', true);
//ho $fqn;
require_once MODX_CORE_PATH . 'components/pdotools/model/pdotools/pdofetch.class.php';
$pdoClass = $modx->loadClass($fqn, '', false, true);

if (!$pdoClass = $modx->loadClass($fqn, '', false, true)) {
    return false;
}


//if (!file_exists(MODX_CORE_PATH . 'components/pdotools')) {return false;}

$pdoFetch = new $pdoClass($modx, array());

$pdoFetch->addTime('pdoTools loaded.');

// Switch context if need
if (!empty($_REQUEST['pageId']) && !empty($_REQUEST['key'])) {
    $modx->resource = $modx->getObject('modResource', $_REQUEST['pageId']);
    if ($modx->resource->get('context_key') != 'web') {
        $modx->switchContext($modx->resource->context_key);
    }
    $config = @$_SESSION['mSearch2'][$_REQUEST['key']];
}


// Load config
if (empty($config) || !is_array($config)) {
    $action = 'no_config';
    $config = $scriptProperties = array();
} else {
    $scriptProperties = isset($config['scriptProperties'])
        ? $config['scriptProperties']
        : $config;
    /** @var mSearch2 $mSearch2 */
    $mSearch2 = $modx->getService('msearch2', 'mSearch2', MODX_CORE_PATH . 'components/msearch2/model/msearch2/', $scriptProperties);
}

// Base url for pdoPage
if ($modx->getOption('friendly_urls')) {
    $q_var = $modx->getOption('request_param_alias', null, 'q');
    $_REQUEST[$q_var] = $modx->makeUrl($_REQUEST['pageId']);
} else {
    $id_var = $modx->getOption('request_param_id', null, 'id');
    $_GET[$id_var] = $_REQUEST['pageId'];
}

//$polylang = $modx->getService('polylang', 'Polylang');
//$polyLangTools = $polylang->getTools();
//$language = false;
$language = $_REQUEST['lang_key'];//$polyLangTools->detectLanguage();

//lang_key
//echo 'culture_key'.$language->get('culture_key');


$page_id = $_REQUEST['pageId'];
// Unset service variables
unset($_REQUEST['pageId'], $_REQUEST['action'], $_REQUEST['key']);

switch ($action) {
    case 'filter':


        if ($_REQUEST['query']) {
            $miniShop2 = $modx->getService('minishop2');
            $miniShop2->initialize($modx->context->key);


            $pageyandex = $_REQUEST['page'] - 1;
            $keyvalue = urlencode($_REQUEST['query']);
            $urlrest = 'https://catalogapi.site.yandex.net/v1.0?apikey=1b1800e1-6710-4b18-883f-16ef11a4ee1e&text=' . $keyvalue . '&searchid=2278252&page=' . $pageyandex;

            $lexicon = $modx->getService('lexicon', 'modLexicon');


            $lexicon->load('poylang:site');

            if ($ch = curl_init($urlrest)) {
                curl_setopt($ch, CURLOPT_HEADER, 0);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 30);
                curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
                curl_setopt($ch, CURLOPT_COOKIEJAR, dirname(__FILE__) . '/cookie.txt');
                curl_setopt($ch, CURLOPT_COOKIEFILE, dirname(__FILE__) . '/cookie.txt');

                curl_setopt($ch, CURLOPT_POST, 0);

                $res = curl_exec($ch);

                curl_close($ch);
                $res1 = json_decode($res);


                $tplRow = 'productRowTpl.tpl';
                $tplproducts = 'products.tpl';


                if (isset($res1->documents)) {
                    $total = $res1->docsTotal;
                    $currentpage = $res1->page + 1;
                    $onpage = $res1->perPage;
                    $allpage = round($total / $onpage);


                    $products = $arrayproducts = array();
                    foreach ($res1->documents as $document) {
                        $idproduct = false;
                        if (isset($document->parameters)) {
                            foreach ($document->parameters as $param) {
                                if ($param->name == 'idproduct') $idproduct = $param->value;
                            }
                        }

                        if ($idproduct) $arrayproducts[] = $idproduct;


                    }


                    if (count($arrayproducts) > 0) {


                        $productmodel = $modx->newObject('msProduct');
                        $q = $modx->newQuery('msProduct');
                        //$q->select('msProduct.id');
                        $q->innerJoin('msProductData', 'Data', 'msProduct.id = Data.id');
                        $q->innerJoin('msVendor', 'Vendor', 'Data.vendor = Vendor.id');
                        $q->select(array('msProduct.id as id',
                            'msProduct.pagetitle',
                            'Data.article',
                            'msProduct.introtext',
                            'Data.popular',
                            'Data.new',
                            'msProduct.parent',
                            'Data.image'
                        ));
                        $q->where(array('msProduct.id:in' => $arrayproducts));
                        $q->prepare();
                        $q->stmt->execute();
                        $result2 = $q->stmt->fetchAll(PDO::FETCH_ASSOC);
                        foreach ($result2 as $product) {


                            $resproducts[$product['id']] = array(
                                'id' => $product['id'],
                                'pagetitle' => $product['pagetitle'],
                                'article' => $product['article'],
                                'introtext' => $product['introtext'],
                                'popular' => $product['popular'],
                                'new' => $product['new'],
                                'image' => $product['image'],
                            );


                            $idcat = $docid = $product['id'];
                            $idcat = $product['parent'];

                            $valaction = $productmodel->getvalueaction($product['id'], $idcat);

                            $q3 = $modx->newQuery('modTemplateVar');
                            $tv_id = 1;
                            $q3->innerJoin('modTemplateVarResource', 'v', 'v.tmplvarid = modTemplateVar.id');
                            $q3->where(array(
                                'v.contentid' => $product['id'],
                                'modTemplateVar.id' => $tv_id,
                            ));
                            $q3->limit(1);
                            $q3->select(array('modTemplateVar.*', 'v.value'));
                            $action = '';

                            if ($q3->prepare() && $q3->stmt->execute()) {
                                while ($tv = $q3->stmt->fetch(PDO::FETCH_ASSOC)) {
                                    $valueoptions = $tv['value'];
                                    $arrayoptions = $modx->fromJSON($valueoptions);
                                    usort($arrayoptions, 'cmp2');

                                    if ($arrayoptions[0]['action']) $action = $arrayoptions[0]['action'];
                                }
                            }

                            if ($action == '') {
                                if ($valaction != '') $action = $valaction;
                            }


                            $outputattr = $outputtabs = array();

                            foreach ($arrayoptions as $keyopt => $ropt) {

                                if ($ropt['action'] != '') $action = 1;
                                else {
                                    if ($valaction != '') $action = 1;
                                    else $action = 0;
                                }

                                $ropt['valaction'] = $valaction;
                                $ropt['idcategory'] = $idcat;
                                $priceall = $productmodel->getAllPrice($ropt);
                                $price = $priceall['price'];
                                $oldprice = $priceall['oldprice'];
                                $arrayoptions[$keyopt]['price'] = $price;
                                $arrayoptions[$keyopt]['action'] = $action;
                                $arrayoptions[$keyopt]['oldprice'] = $oldprice;
                            }
                            usort($arrayoptions, 'cmp1');

                            $dataopt = $arrayoptions;
                            $i = 1;
                            $j = 0;
                            $data1opt = $dataopt;
                            $k1 = '';

                            $outstock = 0;
                            foreach ($data1opt as $keyopt => $ropt) {
                                if ($ropt['instock'] == 0) {
                                    $k1 = $data1opt[$keyopt];
                                    unset($data1opt[$keyopt]);
                                    if ($k1 != '') $data1opt[] = $k1;
                                    $outstock++;
                                }

                            }


                            $dataopt = $data1opt;

                            if ($outstock == count($data1opt)) {
                                $data2 = array();
                                $data2[] = array_shift($data1opt);//[0];

                                $data1opt = $data2;
                            }
                            $ii = 1;
                            foreach ($dataopt as $keyopt => $ropt) {

                                $price = $ropt['price'];
                                if ($j == 0) {
                                    $price_prev = $price;//$r['price'];
                                    $weight_prev = $ropt['weight'];
                                    $weight_prefix_prev = $ropt['weight_prefix'];
                                }


                                $ropt['price'] = str_replace(',', '.', $ropt['price']);
                                $ropt['price'] = str_replace(' ', '', $ropt['price']);
                                $price = str_replace(',', '.', $price);
                                $price = str_replace(' ', '', $price);
                                $ropt['price'] = $miniShop2->formatPrice($ropt['price']);
                                $price = $miniShop2->formatPrice($price);
                                if ($ii <= 3) {
                                    $outputattr[] = array(
                                        'id' => $ropt['MIGX_id'],
                                        'idproduct' => $product['id'],//$idproduct,
                                        'idx' => $i,
                                        'price_prev' => $price_prev,
                                        'productname' => $product['pagetitle'],//$productname,
                                        'articleproduct' => $product['article'],
                                        'weight_prev' => $weight_prev,
                                        'weight_prefix_prev' => $weight_prefix_prev,
                                        'instock' => $ropt['instock'],
                                        'weight' => $ropt['weight'],
                                        'weight_prefix' => $ropt['weight_prefix'],
                                        'price_supplier' => $ropt['price_supplier'],
                                        'currency_supplier' => $ropt['currency_supplier'],
                                        'markup' => $ropt['markup'],
                                        'action' => $ropt['action'],
                                        'price' => $price,
                                        'oldprice' => $ropt['oldprice'],
                                        'polylang_site_product_button_basket' => $modx->lexicon('polylang_site_product_button_basket'),
                                        'polylang_site_product_label_code' => $modx->lexicon('polylang_site_product_label_code')
                                    );
                                    $outputtabs[] = array(
                                        'idproduct' => $product['id'],//idproduct,  //'idproduct'=>$idproduct,
                                        'idx' => $i,
                                        'productname' => $product['pagetitle'],//$productname,
                                        'price_prev' => $price_prev,
                                        'weight_prev' => $weight_prev,
                                        'weight_prefix_prev' => $weight_prefix_prev,
                                        'instock' => $ropt['instock'],
                                        'weight' => $ropt['weight'],
                                        'weight_prefix' => $ropt['weight_prefix'],
                                        'price_supplier' => $ropt['price_supplier'],
                                        'currency_supplier' => $ropt['currency_supplier'],
                                        'markup' => $ropt['markup'],
                                        'action' => $ropt['action'],
                                        'price' => $price,
                                        'oldprice' => $ropt['oldprice'],
                                        'polylang_site_product_button_basket' => $modx->lexicon('polylang_site_product_button_basket'),
                                        'polylang_site_product_label_code' => $modx->lexicon('polylang_site_product_label_code')
                                    );
                                }
                                $ii++;


                                $j++;
                                $i++;
                                if ($j == $limitoption) break;
                            }

//                            $modx->smarty->assign('polylang_site_product_button_basket', $modx->lexicon('polylang_site_product_button_basket'));
//                            $modx->smarty->assign('polylang_site_product_label_code', $modx->lexicon('polylang_site_product_label_code'));

                            $resproducts[$product['id']]['polylang_site_product_button_basket'] = $modx->lexicon('polylang_site_product_button_basket');
                            $resproducts[$product['id']]['polylang_site_product_label_code'] = $modx->lexicon('polylang_site_product_label_code');
                            $resproducts[$product['id']]['options']['outputtabs'] = $outputtabs;
                            $resproducts[$product['id']]['options']['outputattr'] = $outputattr;
                        }

                    }
                    $products = $resproducts;
                    $smarty = $modx->getService('smarty');


                    if (!isset($modx->smarty)) {
                        //echo 'eeee';die();
                        $core_path = $modx->getOption('core_path', null) . 'components/modxsmarty/';// $modx->getOption('modxSmarty.core_path');//, $modx->getOption('core_path', null).'components/modxsmarty/');
                        $template_dir = $modx->getOption('modxSmarty.template_dir');//', $core_path.'templates/');
                        $template = $modx->getOption('modxSmarty.template');//, 'default');
                        $cache_dir = $modx->getOption('modxSmarty.cache_dir');//, $modx->getOption('core_path', null).'cache/modxsmarty/');

                        if (!$compile_dir = $modx->getOption('modxSmarty.compile_dir')) {//, null)){
                            $compile_dir = "{$core_path}compiled/";
                        }
                        $config = array(
                            'template_dir' => $template_dir . "{$template}/",
                            'compile_dir' => $compile_dir,
                            'cache_dir' => $cache_dir,
                            'caching' => $modx->getOption('modxSmarty.caching'),//, $scriptProperties, false),
                            'cache_lifetime' => $modx->getOption('modxSmarty.cache_lifetime'),//, $scriptProperties, -1),
                        );

                        if (
                            $modx->context->key == 'mgr'
                            or (isset($modx->smarty) && is_object($modx->smarty))
                        ) {//echo 'eeeee';
                            return;
                        }

                        $smarty = $modx->getService('smarty', 'modxSmarty', MODX_CORE_PATH . 'components/modxsmarty/model/modxSmarty/', $config);

                        $templates = array();

                        $_compile_dir = "{$template}/";
                        if ($pre_template = $modx->getOption('modxSmarty.pre_template', null, false)) {
                            $templates['prepend'] = $template_dir . "{$pre_template}/";
                            $modx->smarty->assign('pre_template', $pre_template);
                            $modx->smarty->assign('pre_template_url', $modx->getOption('modxSite.template_url') . $pre_template . '/');
                            $_compile_dir .= "{$pre_template}/";
                        }

                        $_compile_dir .= $modx->context->key . "/";

                        $templates['main'] = $template_dir . "{$template}/";
                        $smarty->setTemplateDir($templates);
                        $smarty->setCompileDir($config['compile_dir'] . $_compile_dir);

                        $smarty->inheritance_merge_compiled_includes = false;

                        $plugins_dir = array(
                            '/var/www/admin/www/petchoice.com.ua/core/components/modxsmarty/smarty_plugins',
                        );

                        //echo $plugins_dir;

                        $modx->smarty->addPluginsDir($plugins_dir);
                        $modx->smarty->assign('modx', $modx);
                        $modx->smarty->assign('template_url', '/core/components/modxsite/templates/default/');// $modx->getOption('modxSite.template_url'). $template .'/');


                        $modx->smarty->assign('polylang_site_product_button_basket', $modx->lexicon('polylang_site_product_button_basket'));
                        $modx->smarty->assign('polylang_site_product_label_code', $modx->lexicon('polylang_site_product_label_code'));

                        $modx->smarty->assign('products', $products);


                        if (($_SERVER['HTTP_HOST'] == 'm.petchoice.pp.ua') || ($_SERVER['HTTP_HOST'] == 'm.petchoice.ua')) {
                            //echo 'tpl/m/products/' . $tplproducts;
                            $cont = $modx->smarty->fetch('tpl/m/products/' . $tplproducts);
                            //echo $cont.'<br style="clear:both;"/>';
                            //echo $pagination;
                        } else {
                            $cont = $modx->smarty->fetch('tpl/products/' . $tplproducts);
                            //echo $cont.'<br style="clear:both;"/>';
                            //echo $pagination;


                        }
                        $cont = str_replace(
                            [
                                '[[%polylang_site_product_button_basket]]',
                                '[[%polylang_site_product_label_code]]'
                            ],
                            [
                                $modx->lexicon('polylang_site_product_button_basket'),
                                $modx->lexicon('polylang_site_product_label_code')

                            ], $cont);

                    }

                    $total = $res1->docsTotal;
                    $currentpage = $res1->page + 1;
                    $onpage = $res1->perPage;
                    $allpage = round($total / $onpage);/**/
                    $ostatok = $total - ($onpage * $currentpage);
                    $results = $cont;
                    $response = array(
                        'success' => true,
                        'message' => '',
                        'data' => array(
                            'results' => !empty($results) ? $results : $modx->lexicon('mse2_err_no_results'),
                            'result_category' => $result_category,
                            'pagination' => $pagination,
                            'total' => empty($total) ? 0 : $total,
                            'ostatok' => (int)$ostatok,//(int)
                            //'suggestions' => $suggestions,
                            //'log' => ($modx->user->hasSessionContext('mgr') && !empty($scriptProperties['showLog'])) ? print_r($pdoFetch->getTime(), 1) : '',
                        )
                    );
                    $response = $modx->toJSON($response);
                    echo $response;
                    die();
                    exit($response);//$response);
                }
            }
        }

        //echo 'ddfdfdf';die();
        $paginatorProperties = $config['paginatorProperties'];
        //$paginatorProperties['fastMode'] = true;
        $paginatorProperties['toPlaceholder'] = '';
        $paginatorProperties['toPlaceholders'] = '';
        $paginatorProperties['toSeparatePlaceholders'] = '';
        $paginatorProperties['ajax'] = 0;
        $paginatorProperties['ajaxMode'] = '';
//sortby

        if (isset($_REQUEST['vendor'])) {
            $_GET['ms|vendor'] = $_REQUEST['ms|vendor'] = $_GET['vendor'] = $_REQUEST['vendor'] = $_REQUEST['vendor'];//$id . ',' . $_REQUEST['vendor'];
        } else {
            //$_GET['ms|vendor'] = $_REQUEST['ms|vendor'] =$_GET['vendor'] = $_REQUEST['vendor'] = $id;
        }


        if (isset($_REQUEST['vendor'])) $_REQUEST['ms|vendor'] = $_REQUEST['vendor'];
        elseif (isset($_REQUEST['ms|vendor'])) $_REQUEST['vendor'] = $_REQUEST['ms|vendor'];

        $sortdir = 'desc';
        if (!empty($_REQUEST['sort'])) {
            $sortexplode = explode(':', $_REQUEST['sort']);
            $sort = (isset($sortexplode[0]) ? $sortexplode[0] : $_REQUEST['sort']);
            $sortdir = (isset($sortexplode[1]) ? $sortexplode[1] : $sortdir);
        } else {//if (!empty($paginatorProperties['defaultSort'])) {
            $sort = 'hits';//$paginatorProperties['defaultSort'];
        }

        if (isset($_REQUEST['sortdir'])) {
            $sortdir = $_REQUEST['sortdir'];
        } //else $sortdir = 'desc';//desc';


        /*if ($_SERVER['REMOTE_ADDR']=='188.130.177.18'){
        //echo '$sortdir'.$sort.'<br/>';
        echo '<pre>$paginatorProperties';
        print_R($paginatorProperties);
        echo '</pre>';

        }*/


        if (empty($_REQUEST['limit'])) {
            $paginatorProperties['limit'] = $_REQUEST['limit'] = $paginatorProperties['start_limit'];
        }

        switch ($page_id) {

            case '12430':
            case '12431':
            case '12432':

                $paginatorProperties['sortby'] = !empty($sort)
                    ? $mSearch2->getSortFields($sort, $sortdir)
                    : '';

                $paginatorProperties['sortdir'] = 'DESC';//$sortdir;
                $paginatorProperties['sortby'] = 'count_points';
                $paginatorProperties['raiting'] = 1;
                $paginatorProperties['raiting_page'] = 1;

                break;
            default:
                $paginatorProperties['sortby'] = !empty($sort)
                    ? $mSearch2->getSortFields($sort, $sortdir)
                    : '';

                $paginatorProperties['sortdir'] = $sortdir;
                if ($paginatorProperties['sortby'] == '') $paginatorProperties['sortby'] = 'hits';
                break;
        }


        // Switching chunk for rows, if specified
        if (!empty($paginatorProperties['tpls']) && is_array($paginatorProperties['tpls'])) {
            $tmp = isset($_REQUEST['tpl']) ? (integer)$_REQUEST['tpl'] : 0;
            if (isset($paginatorProperties['tpls'][$tmp])) {
                $paginatorProperties['tpl'] = $paginatorProperties['tpls'][$tmp];
            }
        }

        if (strpos($paginatorProperties['resources'], '{') === 0) {
            $found = $modx->fromJSON($paginatorProperties['resources']);
            $ids = array_keys($found);
        } else {
            $ids = explode(',', $paginatorProperties['resources']);
        }

        $resources = implode(',', $ids);

        //$pdoFetch->addTime('Getting filters for saved ids: ('.$resources.')');

        $matched = $mSearch2->Filter($ids, $_REQUEST);

        $ids = array_intersect($ids, $matched);


        if (($_SERVER['HTTP_HOST'] == 'm.petchoice.pp.ua') || ($_SERVER['HTTP_HOST'] == 'm.petchoice.ua') || ($_SERVER['HTTP_HOST'] == 'm.petchoice.test')) {
            $suggestions = array();
        } else {
            $filters_n = $mSearch2->filters_n;
            $suggestions = $mSearch2->getSuggestions($resources, $_REQUEST, $ids, $filters_n);
        }

        $counts = count($matched);


        // Retrieving results
        if (!empty($ids)) {
            $_GET = $_REQUEST;

            $paginatorProperties['resources'] = is_array($ids) ? implode(',', $ids) : $ids;
            // Trying to save weight of found ids if using mSearch2
            if (!empty($found) && strtolower($paginatorProperties['element']) == 'msearch2') {
                $tmp = array();
                foreach ($ids as $v) {
                    $tmp[$v] = @$found[$v];
                }
                $paginatorProperties['resources'] = $modx->toJSON($tmp);
            }

            $results = $modx->runSnippet($mSearch2->config['paginator'], $paginatorProperties);

            $pagination = $modx->getPlaceholder($paginatorProperties['pageNavVar']);
            $total = $modx->getPlaceholder($paginatorProperties['totalVar']);

            if (!empty($paginatorProperties['fastMode'])) {
                $results = $pdoFetch->fastProcess($results);
                $pagination = $pdoFetch->fastProcess($pagination);
            } else {
                $maxIterations = (integer)$modx->getOption('parser_max_iterations', null, 10);
                $modx->getParser()->processElementTags('', $results, false, false, '[[', ']]', array(), $maxIterations);
                $modx->getParser()->processElementTags('', $results, true, true, '[[', ']]', array(), $maxIterations);
                $modx->getParser()->processElementTags('', $pagination, false, false, '[[', ']]', array(), $maxIterations);
                $modx->getParser()->processElementTags('', $pagination, true, true, '[[', ']]', array(), $maxIterations);
            }
        } else {
            $results = $pagination = '';
        }
        if (isset($_REQUEST['page']))
            $all = (int)$_REQUEST['page'] * (int)$paginatorProperties['limit'];
        else $all = (int)$paginatorProperties['limit'];

        $ostatok = (int)$total - (int)$all;

        $response = array(
            'success' => true,
            'message' => '',
            'data' => array(
                'results' => !empty($results) ? $results : $modx->lexicon('mse2_err_no_results'),
                'pagination' => $pagination,
                'total' => empty($total) ? 0 : $total,
                'ostatok' => (int)$ostatok,//(int)
                'suggestions' => $suggestions,
                'log' => ($modx->user->hasSessionContext('mgr') && !empty($scriptProperties['showLog'])) ? print_r($pdoFetch->getTime(), 1) : '',
            )
        );
        $response = $modx->toJSON($response);


        break;

    case 'search':

        $prefLang = '';
        if ($language == 'ru') {
            $prefLang = 'ru/';
        }

        $snippet = !empty($scriptProperties['element'])
            ? $scriptProperties['element']
            : 'mSearch2';

        $results = array();
        $query = trim(@$_REQUEST[$scriptProperties['queryVar']]);
        if (empty($scriptProperties['limit'])) {
            $scriptProperties['limit'] = 5;
        }
        if (empty($scriptProperties['introCutAfter'])) {
            $scriptProperties['introCutAfter'] = 100;
        }

        if (!empty($scriptProperties['autocomplete'])) {

            switch (strtolower($scriptProperties['autocomplete'])) {
                case 'queries':
                    $query = $string = preg_replace('/[^_-а-яёa-z0-9\s\.\/]+/iu', ' ', $modx->stripTags($query));
                    $query = $mSearch2->addAliases($query);
                    $condition = "`found` > 0 AND (`query` LIKE '%$query%'";
                    $words = $mSearch2->getAllForms($query);

                    foreach ($words as $tmp) {
                        foreach ($tmp as $word) {
                            $condition .= " OR `query` LIKE '%$word%'";
                        }
                    }
                    $condition .= ')';


                    $scriptProperties['sortby'] = 'quantity';
                    $scriptProperties['sortdir'] = 'desc';
                    $rows = $pdoFetch->getCollection('mseQuery', '["' . $condition . '"]', $scriptProperties);
                    $i = 1;
                    foreach ($rows as $row) {
                        $intro = $mSearch2->Highlight($row['query'], $query);
                        if (empty($intro)) {
                            $intro = $row['query'];
                        }
                        $row['pagetitle'] = $row['title'] = $intro;
                        $row['idx'] = $i;
                        $results[] = array(
                            'value' => html_entity_decode($row['query'], ENT_QUOTES, 'UTF-8'),
                            'label' => $pdoFetch->getChunk($scriptProperties['tpl'], $row),
                        );
                        $i++;
                    }
                    break;

                default:


                    $modx->lexicon->load('poylang:site');
                    //$found = $mSearch2->Search($query);
                    $found = $arrayproducts = array();
                    $keyvalue = urlencode($query);
                    $urlrest = 'https://catalogapi.site.yandex.net/v1.0?apikey=1b1800e1-6710-4b18-883f-16ef11a4ee1e&text=' . $keyvalue . '&searchid=2278252';
                    if ($ch = curl_init($urlrest)) {
                        curl_setopt($ch, CURLOPT_HEADER, 0);
                        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 30);
                        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
                        curl_setopt($ch, CURLOPT_COOKIEJAR, dirname(__FILE__) . '/cookie.txt');
                        curl_setopt($ch, CURLOPT_COOKIEFILE, dirname(__FILE__) . '/cookie.txt');
                        //curl_setopt($ch, CURLOPT_USERPWD, $auth_name.':'.$auth_pass);

                        curl_setopt($ch, CURLOPT_POST, 0);
                        //curl_setopt($ch, CURLOPT_POSTFIELDS,'ХХХХ:ХХХХХ');
                        $res = curl_exec($ch);

                        curl_close($ch);
                        //
                        //echo dirname(__FILE__).'/cookie.txt';
                        $res1 = json_decode($res);
//                        echo '<pre>$res';
//                        print_r($res);
//                        echo '</pre>';
                        if (isset($res1->documents)) {
                            $products = $arrayproducts = array();
                            foreach ($res1->documents as $document) {
                                $idproduct = false;
                                if (isset($document->parameters)) {
                                    foreach ($document->parameters as $param) {
                                        if ($param->name == 'idproduct') $idproduct = $param->value;
                                    }
                                }

                                if ($idproduct) $arrayproducts[] = $idproduct;

                            }
                        }
                    }
                    //$arrayproducts=[8374, 423, 220, 470, 8316, 223];
                    $found = $arrayproducts;
                    //if (!empty($found)) {
                    if (count($found) > 0) {
                        /*$resources = strtolower($snippet) == 'msearch2'
                            ? $modx->toJSON($found)
                            : implode(',', array_keys($found));*/
                        $resources = implode(',', $found);//array_keys($found));
                        /*echo '<pre>$resources';
                        print_R($resources);
                        echo '</pre>';*/
                        if (!isset($scriptProperties['parents'])) {
                            $scriptProperties['parents'] = 0;
                        }
                        if (empty($scriptProperties['sortby'])) {
                            $scriptProperties['sortby'] = '';
                        }
                        if (!isset($scriptProperties['sortdir'])) {
                            $scriptProperties['sortdir'] = '';
                        }


                        $scriptProperties['sortby'] = 'hits';
                        $scriptProperties['sortdir'] = 'DESC';
                        $scriptProperties['returnIds'] = 0;
                        $scriptProperties['resources'] = $resources;
                        $scriptProperties['outputSeparator'] = '<!-- msearch2 -->';

                        $html = $modx->runSnippet($snippet, $scriptProperties);
                        if ($modx->user->hasSessionContext('mgr') && !empty($scriptProperties['showLog'])) {
                            preg_match('#<pre class=".*?Log">(.*?)</pre>#s', $html, $matches);
                            $log = $matches[1];
                            $html = str_replace($matches[0], '', $html);
                        }
                        $processed = explode('<!-- msearch2 -->', $html);

                        $scriptProperties['select'] = 'id,pagetitle';
                        $scriptProperties['returnIds'] = 1;
                        unset($scriptProperties['limit']);
                        //$scriptProperties['resources'] = $modx->runSnippet($snippet, $scriptProperties);
                        $where = array(
                            'id:IN' => $scriptProperties['resources']//explode(',',$scriptProperties['resources'])
                        );

                        $q2 = $modx->newQuery('msProduct');//modResource');
                        $q2->where(array(
                            'id:IN' => $arrayproducts,//explode(',',$scriptProperties['resources']),//$scriptProperties['resources']//explode(',',$scriptProperties['resources'])//$scriptProperties['resources'],//
                            'class_key' => 'msProduct'
                        ));

                        //$where);
                        $q2->select(array(
                            'msProduct.*'
                        ));

                        $q2->limit(5);
                        $q2->prepare();
                        $q2->stmt->execute();

                        $rows = $q2->stmt->fetchAll(PDO::FETCH_ASSOC);

                        $i = 0;


                        $q = $modx->newQuery('msProductFile', array('type' => 'image', 'parent' => 0));
                        $q->innerJoin('msProduct', 'msProduct', '`msProductFile`.`product_id` = `msProduct`.`id`');
                        $q->select('`msProductFile`.`id`, `msProductFile`.`url`, `msProductFile`.`product_id`, `msProduct`.`pagetitle`');


                        $q->where(array('`msProduct`.`id`:IN' => explode(',', $scriptProperties['resources'])));

                        if ($q->prepare() && $q->stmt->execute()) {
                            while ($image = $q->stmt->fetch(PDO::FETCH_ASSOC)) {
                                $images[$image['product_id']] = $image['url'];//'<a href="'.$image['url'].'" rel="prettyPhoto['.$image['product_id'].']" title="'.$image['pagetitle'].'"></a>'."\n";

                            }
                        }

                        $result_category = $result_products = [];
                        //$result_products=[];

                        foreach ($rows as $k => $row) {


                            $row['image'] = $images[$row['id']];
                            //price
                            $modelproduct = $modx->getObject('msProduct', $row['id']);
                            $price = $modelproduct->getPrice();
                            $article = $modelproduct->get('article');

                            if (empty($delimeter)) {
                                $delimeter = ' , ';
                            }
                            $scheme = $modx->getOption('link_tag_scheme', null, 'full', true);

                            $q = $modx->newQuery('msCategory');
                            $q->leftJoin('msCategoryMember', 'msCategoryMember', array(
                                '`msCategory`.`id` = `msCategoryMember`.`category_id`'
                            ));
                            $q->sortby('pagetitle', 'ASC');
                            $q->groupby('id');
                            $q->select(array('parent', 'id', 'pagetitle'));
                            $q->where('`msCategory`.`id` = ' . $row['parent']);//`msCategoryMember`.`product_id` = '.$row['id'].' OR

                            $result = array();
                            $category = array();
                            if ($q->prepare() && $q->stmt->execute()) {
                                $row1 = array();
                                while ($row1 = $q->stmt->fetch(PDO::FETCH_ASSOC)) {
                                    //$url = $modx->makeUrl($row1['id'], '', '', $scheme);
                                    $category = array();
                                    $modelcategory = $modx->getObject('msCategory', $row1['parent']);
                                    $pageTitleCat = $modelcategory->get('pagetitle');
                                    $pageTitle = $row1['pagetitle'];


                                    if ($language != 'ru') {
                                        $sql = "SELECT C.pagetitle FROM modx_polylang_content as C WHERE content_id= '" . $row1['id'] . "' and culture_key='ua' LIMIT 1";

                                        $q1 = $modx->prepare($sql);
                                        $q1->execute(array(0));
                                        $arr1 = $q1->fetchAll(PDO::FETCH_ASSOC);

                                        foreach ($arr1 as $ar) {
                                            $pageTitle = $ar['pagetitle'];
                                        }

                                        $sql = "SELECT C.pagetitle FROM modx_polylang_content as C WHERE content_id= '" . $row1['parent'] . "' and culture_key='ua' LIMIT 1";

                                        $q2 = $modx->prepare($sql);
                                        $q2->execute(array(0));
                                        $arr2 = $q2->fetchAll(PDO::FETCH_ASSOC);

                                        foreach ($arr2 as $ar) {
                                            $pageTitleCat = $ar['pagetitle'];
                                        }
                                    }

                                    $url = '/searchresult?query=' . $query . '&category_id=' . $row1['id'];

                                    $result_category[] = ['url' => $url, 'value' => $pageTitleCat . ' > ' . $pageTitle];
                                    //$url]=$pageTitleCat.' > '.$pageTitle;

                                    $category[] = '<span>' . $pageTitleCat . '</span>';
                                    $category[] = '<span>' . $pageTitle . '</span>';//'<a href="'.$url.'">'..'</a>';
                                }
                            }
                            $category = implode(' > ', $category);


                            $miniShop2 = $modx->getService('minishop2');
                            $row['category'] = $category;
                            $row['article'] = $article;
                            $row['price'] = $miniShop2->formatPrice($price);


                            $results[] = array(
                                'id' => $row['id'],
                                'url' => '/' . $prefLang . $row['uri'],
                                'value' => html_entity_decode($row['pagetitle'], ENT_QUOTES, 'UTF-8'),
                                'label' => $pdoFetch->getChunk($scriptProperties['tpl'], $row),
                            );
                            $i++;
                        }
                        $polylang_site_search_all_res = $modx->lexicon('polylang_site_search_all_res');

                        /*if (count($results) > 0) {
                            $results[] = array(
                                'id' => '',
                                'url' => '/'.$prefLang.'searchresult?query=' . $query,
                                'value' => $polylang_site_search_all_res,
                                'label' => '<div class="bordered grey small" style="text-align:right;border:none;width:100%;margin-bottom:5px;"><span style="text-align:right; margin-right:10px;color: #476dc7;text-decoration: none;border-bottom: 1px dashed #476dc7">'.$polylang_site_search_all_res.'</span></div>',
                            );
                        }*/
                    }
            }
        }

        if (!empty($scriptProperties['fastMode'])) {
            foreach ($results as &$v) {
                if (!empty($v['label'])) {
                    $v['label'] = $pdoFetch->fastProcess($v['label']);
                }
            }
        } else {
            $maxIterations = (integer)$modx->getOption('parser_max_iterations', null, 10);
            foreach ($results as &$v) {
                if (!empty($v['label'])) {
                    $modx->getParser()->processElementTags('', $v['label'], false, false, '[[', ']]', array(), $maxIterations);
                    $modx->getParser()->processElementTags('', $v['label'], true, true, '[[', ']]', array(), $maxIterations);
                }
            }
        }
        $polylang_site_category_no_found = $modx->lexicon('polylang_site_category_no_found');
        if (count($results) == 0) {
            $results[] = array(
                'id' => '',
                'url' => '',
                'value' => '[[%polylang_site_category_no_found]]',
                'label' => '<div class="product-item-title" style="text-align:center;border:none;width:100%;"><span style="text-align:center;padding:5px;">' . $polylang_site_category_no_found . '</span></div>',//По Вашему запросу ничего не найдено
            );

        }


        $response = array(
            'success' => true,
            'lang' => $language,
            'message' => '',
            'data' => [
                'results' => $results,
                'show_all' => '/' . $prefLang . 'searchresult?query=' . $query,
                'result_category' => $result_category,
                'total' => count($results),
            ]
        );
        if (!empty($log)) {
            $response['data']['log'] = $log;
        }
        $response = $modx->toJSON($response);
        break;

    case 'no_config':
        $response = $modx->toJSON(array('success' => false, 'message' => 'Could not load config'));
        break;
    default:
        $response = $modx->toJSON(array('success' => false, 'message' => 'Access denied'));
}

@session_write_close();
exit($response);