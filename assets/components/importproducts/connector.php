<?php
/**
 * modExtra
 *
 * Copyright 2010 by Shaun McCormick <shaun+importproducts@modx.com>
 *
 * modExtra is free software; you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 2 of the License, or (at your option) any later
 * version.
 *
 * modExtra is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * modExtra; if not, write to the Free Software Foundation, Inc., 59 Temple
 * Place, Suite 330, Boston, MA 02111-1307 USA
 *
 * @package importproducts
 */
/**
 * modExtra Connector
 *
 * @package importproducts
 */

set_time_limit(0);
ini_set('MAX_EXECUTION_TIME', 0);
require_once dirname(dirname(dirname(dirname(__FILE__)))) . '/config.core.php';

require_once MODX_CORE_PATH . 'config/' . MODX_CONFIG_KEY . '.inc.php';

require_once MODX_CONNECTORS_PATH . 'index.php';

$importproducts = $modx->getService('importproducts', 'importproducts', $modx->getOption('importproducts_core_path', null, $modx->getOption('core_path') . 'components/importproducts/') . 'model/importproducts/');
$modx->lexicon->load('importproducts:default');

// handle request
$corePath = $modx->getOption('importproducts_core_path', null, $modx->getOption('core_path') . 'components/importproducts/');
$path = $modx->getOption('processorsPath', $importproducts->config, $corePath . 'processors/');

$modx->request->handleRequest(array(
	'processors_path' => $path,
	'location' => '',
));