Ext.onReady(function() {
    MODx.load({ xtype: 'modextra-page-home'});
});

importproducts.page.Home = function(config) {
    config = config || {};
    Ext.applyIf(config,{
        components: [{
            xtype: 'modextra-panel-home'
            ,renderTo: 'modextra-panel-home-div'
        }]
    }); 
    importproducts.page.Home.superclass.constructor.call(this,config);
};
Ext.extend(importproducts.page.Home,MODx.Component);
Ext.reg('modextra-page-home',importproducts.page.Home);