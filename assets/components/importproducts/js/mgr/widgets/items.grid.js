/*- Код
- Название
- Дата создания
- Активен
- Дата активации
- Заказ*/
importproducts.grid.Items = function(config) {
    config = config || {};
	this.sm = new Ext.grid.CheckboxSelectionModel();
    Ext.applyIf(config,{
        id: 'modextra-grid-items'
        ,url: importproducts.config.connector_url
		,fields: this.getFields(config)
		,columns: this.getColumns(config)
		//,fields: this.getFields(config)
		//,columns: this.getColumns(config)
        ,baseParams: {
            action: 'mgr/item/getlist'
        }
		,sm: this.sm
        //,fields: ['id','name','code','created','status','date_active', 'summa','discount','order_id']
        ,autoHeight: true
        ,paging: true
        ,remoteSort: true
        
		,tbar: this.getTopBar(config)
    });
    importproducts.grid.Items.superclass.constructor.call(this,config);
};
Ext.extend(importproducts.grid.Items,MODx.grid.Grid,{
    windows: {}

    ,getMenu: function() {
        var m = [];
        m.push({
            text: _('modextra.item_update')
            ,handler: this.updateItem
        });
        m.push('-');
        m.push({
            text: _('modextra.item_remove')
            ,handler: this.removeItem
        });
        this.addContextMenuItem(m);
    }
    
    ,createItem: function(btn,e) {
        if (!this.windows.createItem) {
            this.windows.createItem = MODx.load({
                xtype: 'modextra-window-item-create'
                ,listeners: {
                    'success': {fn:function() { this.refresh(); },scope:this}
                }
            });
        }
        this.windows.createItem.fp.getForm().reset();
        this.windows.createItem.show(e.target);
    }
    ,updateItem: function(btn,e) {
        if (!this.menu.record || !this.menu.record.id) return false;
        var r = this.menu.record;

        if (!this.windows.updateItem) {
            this.windows.updateItem = MODx.load({
                xtype: 'modextra-window-item-update'
                ,record: r
                ,listeners: {
                    'success': {fn:function() { this.refresh(); },scope:this}
                }
            });
        }
        this.windows.updateItem.fp.getForm().reset();
        this.windows.updateItem.fp.getForm().setValues(r);
        this.windows.updateItem.show(e.target);
    }
    
    ,removeItem: function(btn,e) {
        if (!this.menu.record) return false;
        
        MODx.msg.confirm({
            title: _('modextra.item_remove')
            ,text: _('modextra.item_remove_confirm')
            ,url: this.config.url
            ,params: {
                action: 'mgr/item/remove'
                ,id: this.menu.record.id
            }
            ,listeners: {
                'success': {fn:function(r) { this.refresh(); },scope:this}
            }
        });
    },
	

	getFields: function (config) {
		return ['file'];
		//['id', 'name', 'description', 'active', 'actions'];
	},

	getColumns: function (config) {
		
		var all = {			
			file: {width: 100, sortable: false}
		};

		var columns = [this.sm];//, this.exp];
		fields=this.getFields();
		for(var i=0; i < fields.length; i++) {
			var field = fields[i];//miniShop2.config.order_grid_fields[i];
			//alert(field);
			if (all[field]) {
				Ext.applyIf(all[field], {
					header: _(field)
					,dataIndex: field
				});
				columns.push(all[field]);
			}
		}

		return columns;
		
		
		
	},

	getTopBar: function (config) {
		return [/*{
				text: '<i class="'+ (MODx.modx23 ? 'icon icon-list' : 'bicon-list') + '"></i> ' + 'Действия'//_('ms2_bulk_actions')
				,menu: [
					{
					text: 'Удалить выбранные'//_('ms2_menu_remove_multiple')
					,handler: this.removeSelected
					,scope: this
				}]
			},*/
		/*{
            text: 'Сгенерировать транслит артиклов'//_('modextra.item_create')
            ,handler: this._createItem			
            ,scope: this
        },*/
            {
                text: 'Сгенерировать Похожие товары'//_('modextra.item_create')
                ,handler: this._createRelated
                ,scope: this
            },
		{
            text: 'Сгенерировать транслит артиклов'//_('modextra.item_create')
            ,handler: this._createItem			
            ,scope: this
        },
		{
            text: 'Скачать csv для гугл'//_('modextra.item_create')
            ,handler: this._createCsv			
            ,scope: this
        },

		{
			xtype: 'textfield'
			,name: 'query'
			,width: 200
			,id: 'minishop2-product-search'
			,emptyText: _('ms2_search')
			,listeners: {render: {fn: function(tf) {
				tf.getEl().addKeyListener(Ext.EventObject.ENTER, function() {this._download(tf);}, this);
				//tf.setValue(baseParams.query);
			},scope: this}}
		},		
		
		'->',
		/*{
            xtype: 'displayfield'
            ,fieldLabel: 'Обновление  поставщика'
            ,name: 'updatepost'
            ,id: this.ident+'-updatepost'
            ,anchor: '100%'
			text: 'Обновление Марина Бобровник'
        },
		{
			//xtype: 'textfield',
			   xtype: 'modx-combo-browser'
            ,fieldLabel: 'Filepost'
            ,name: 'filepost'
            ,id: 'pm-filepost'
			
			,placeholder:'Обновление Марина Бобровник'
            ,allowBlank: false
            ,hideFiles: true
			,listeners: {
				
				render: {
					fn: function (tf) {
						tf.getEl().addKeyListener(Ext.EventObject.ENTER, function () {
							this._uploadbob(tf);
						}, this);
					}, scope: this
				}
			}
		},	*/		
		{
			//xtype: 'textfield',
			   xtype: 'modx-combo-browser'
            ,fieldLabel: 'File'
            ,name: 'file'
            ,id: 'pm-file'
            ,allowBlank: false
            ,hideFiles: true
			,listeners: {
				
				render: {
					fn: function (tf) {
						tf.getEl().addKeyListener(Ext.EventObject.ENTER, function () {
							this._upload(tf);
						}, this);
					}, scope: this
				}
			}
			
		}];
	},
	_createItem:function (tf, nv, ov) {
		this.getStore().baseParams.generate = 1;
		this.refresh();	
	},
    _createRelated:function (tf, nv, ov) {
        this.getStore().baseParams.updateRelated = 1;
        this.refresh();
    },
    _createCsv:function (tf, nv, ov) {
		this.getStore().baseParams.createCsv = 1;
		this.refresh();	
	},	
	
	_uploadbob:function (tf, nv, ov) {
		this.getStore().baseParams.csvbob = tf.getValue();
		
		this.refresh();
		
	},
	_upload:function (tf, nv, ov) {
		this.getStore().baseParams.csv = tf.getValue();
		//this.getBottomToolbar().changePage(1);
		this.refresh();
		//alert(tf.getValue());
	},
	_download:function (tf, nv, ov) {
		this.getStore().baseParams.brand = tf.getValue();
		
		this.refresh();
		//alert(tf.getValue());
	},
	_doSearch: function (tf, nv, ov) {
		this.getStore().baseParams.query = tf.getValue();
		this.getBottomToolbar().changePage(1);
		this.refresh();
	},

	_clearSearch: function (btn, e) {
		this.getStore().baseParams.query = '';
		Ext.getCmp(this.config.id + '-search-field').setValue('');
		this.getBottomToolbar().changePage(1);
		this.refresh();
	}
	,removeSelected: function(btn,e) {
		var cs = this.getSelectedAsList();
		if (cs === false) return false;

		MODx.msg.confirm({
			title: 'Удалить выбраные'//_('ms2_menu_remove_multiple')
			,text: 'Точно удалить'//_('ms2_menu_remove_multiple_confirm')
			,url: this.config.url
			,params: {
				action: 'mgr/item/remove_multiple'
				,ids: cs
			}
			,listeners: {
				success: {fn:function(r) {
					this.getSelectionModel().clearSelections(true);
					this.refresh();
				},scope:this}
			}
		});
		return true;
	}
});
Ext.reg('modextra-grid-items',importproducts.grid.Items);


// Комбобоксы статусов, складов и категорий товаров
/*var storeitems = new Ext.data.ArrayStore({
    id: 'type'
    ,fields: ['value',{name: 'name', type: 'string']}
    ,data: [[1,'Статический'],[0,'Динамический']]
});*/
importproducts.combo.type = function(config) {
	config = config || {};
	/*Ext.applyIf(config,{
		store: new Ext.data.SimpleStore({
			fields: ['id', 'name']
			,data: [[1,'Статический'],[0,'Динамический']]
		})
		,emptyText: ''
		,displayField: 'name'
		,valueField: 'id'
		,hiddenName: 'id'
		//,mode: 'local'
		//,triggerAction: 'all'
		//,editable: false
		//,selectOnFocus: false
		//,preventRender: true
		//,forceSelection: true
		//,enableKeyEvents: true
	});*/
	
	 Ext.applyIf(config,{
		name: 'type'		
       ,store: new Ext.data.SimpleStore({
			fields: ['id', 'name'],
			data: [['1','Group1'],['2','Group2'],['3','Group3'],['4','Group4']]
		})
		,displayField: 'name'
		,valueField: 'id'
		,fields: ['id','name']
		,pageSize: 20		
		,itemSelector: 'div.importproducts-type-item'
	});
	
	

	
	importproducts.combo.type.superclass.constructor.call(this,config);
};
Ext.extend(importproducts.combo.type,MODx.combo.ComboBox);

Ext.reg('importproducts-combo-type',importproducts.combo.type);



importproducts.combo.CategoryCombo = function(config) {
    config = config || {};
    Ext.applyIf(config,{
        name: 'category'
        ,hiddenName: 'category'
        ,displayField: 'name'
        ,valueField: 'id'
        ,url: importproducts.config.connectorUrl
        ,baseParams: { action: 'mgr/category/getlist' }
        ,fields: ['id','name']
        ,pageSize: 20        
        ,typeAhead: true
        ,editable: true
    });
    importproducts.combo.CategoryCombo.superclass.constructor.call(this,config);
};
Ext.extend(importproducts.combo.CategoryCombo,MODx.combo.ComboBox);
Ext.reg('importproducts-combo-categorycombo',importproducts.combo.CategoryCombo);


importproducts.window.CreateItem = function(config) {
   this.getStore().baseParams.generate = 1;
					this.refresh();
};
Ext.extend(importproducts.window.CreateItem,MODx.Window);
Ext.reg('modextra-window-item-create',importproducts.window.CreateItem);


importproducts.window.UpdateItem = function(config) {
    config = config || {};
    this.ident = config.ident || 'modextra-meuitem'+Ext.id();
    Ext.applyIf(config,{
        title: _('modextra.item_update')
        ,id: this.ident
        ,height: 150
        ,width: 475
        ,url: importproducts.config.connector_url
        ,action: 'mgr/item/update'
        ,fields: [
		{
				xtype: 'hidden'
				,name: 'id'
			},
		{
            xtype: 'textfield'
            ,fieldLabel: _('name')
            ,name: 'name'
            ,id: this.ident+'-name'
            ,anchor: '100%'
        },		
		{
            
			xtype: 'xdatetime'
			,fieldLabel: _('term')
			//,description: '<b>[[*publishedon]]</b><br />'+_('resource_publishedon_help')
			,name: 'term'
			,id: 'modx-resource-term'
			,allowBlank: true
			,dateFormat: MODx.config.manager_date_format
			,timeFormat: MODx.config.manager_time_format
			,startDay: parseInt(MODx.config.manager_week_start)
			,dateWidth: 120
			,timeWidth: 120
			/*,renderer: function(value) {
				alert(value);
					return value == true ? 'Yes' : 'No'
				}*/
			,value: config.record.term
			//,listeners: oc
        },
		
		//,publishedon: {width:50, sortable:true, editor:{xtype:'minishop2-xdatetime', timePosition:'below'}, renderer: miniShop2.utils.formatDate}
		{
            xtype: 'textfield'
            ,fieldLabel: _('code')
            ,name: 'code'
            ,id: this.ident+'-code'
            ,anchor: '100%'
        }		
		, {
			xtype: 'minishop2-combo-category',//minishop2-combo-user',//minishop2-combo-options',//minishop2-combo-category',//importproducts-combo-options',//importproducts-tree-categories',//importproducts-combo-categorycombo',
			fieldLabel: _('category'),
			name: 'category',
			hiddenName: 'category',
			id: this.ident + '-category',
			value: config.record.category,
			anchor: '99%',
			allowBlank: false
			//,value: config.record.category
		}
/*
var storeitems = new Ext.data.ArrayStore({
    id: 'advert-items'
    ,fields: ['value',{name: 'name', type: 'string'}]
    ,data: [[1,'Да'],[0,'Нет']]
});
advert.combo.Items = function(config) {
    config = config || {};
    Ext.applyIf(config,{
        store: storeitems
        ,displayField: 'name'
        ,valueField: 'value'
        ,hiddenName: 'поле в базе=из mysql map' //не забудьте поменять
        ,mode: 'local'
        ,triggerAction: 'all'
        ,editable: false
        ,selectOnFocus: false
        ,preventRender: true
        ,forceSelection: true
        ,enableKeyEvents: true
    });
    advert.combo.Items.superclass.constructor.call(this,config);
};		*/
		/*,{			
			xtype : 'combo'
			//,editable : false
			,width : 40
			,fieldLabel : _('type')
			,name : 'type'
			,valueField : 'value'
			,hiddenName: 'type'
			,displayField : 'name'
			//,triggerAction:'all'
			//,allowBlank : false
			  ,triggerAction: 'all'
			,editable: false
			,selectOnFocus: false
			,preventRender: true
			,forceSelection: true
			,enableKeyEvents: true
			,mode:'local'
			,anchor: '50%'			
			,store: new Ext.data.ArrayStore({
				id : this.ident + '-type'
				//,fields: ['id','type']
				,fields: ['value',{name: 'name', type: 'string'}]
				//,data : [[1,"Y"],[2,"N"]]
				,data: [['static','Статический'],['dinamic','Динамический']]				
			})
		}
		,{
            xtype: 'displayfield'
            ,fieldLabel: _('type')
            ,name: 'type'
            ,id: this.ident+'-type'
            ,anchor: '100%'
			
        }*/
		,{xtype: 'combo-boolean',fieldLabel: _('status'),name: 'status',hiddenName: 'status',id: this.ident + '-status',anchor: '50%'}		
		,{
            xtype: 'textfield'
            ,fieldLabel: _('discount')
            ,name: 'discount'
            ,id: this.ident+'-discount'
            ,anchor: '100%'
        },{
            xtype: 'numberfield'
            ,fieldLabel: _('summa')
            ,name: 'summa'
            ,id: this.ident+'-summa'
            ,anchor: '100%'
        }
		,{
            xtype: 'displayfield'
            ,fieldLabel: _('order_num')
            ,name: 'order_num'
            ,id: this.ident+'-order_num'
            ,anchor: '100%'
			
        }
		,{
            xtype: 'displayfield'
            ,fieldLabel: _('type')
            ,name: 'type'
            ,id: this.ident+'-type'
            ,anchor: '100%'
			
        }
		,{
            xtype: 'displayfield'
            ,fieldLabel: _('date_active')
            ,name: 'date_active'
			,format: 'yyyy-MM-dd HH:mm'			
            ,id: this.ident+'-date_active'
            ,anchor: '100%'
        },{
            xtype: 'displayfield'
            ,fieldLabel: _('created')
            ,name: 'created'
            ,id: this.ident+'-created'
            ,anchor: '100%'
        }
		/*,{
            xtype: 'numberfield'
            ,fieldLabel: _('count')
            ,name: 'count'
            ,id: this.ident+'-count'
            ,anchor: '100%'
        }*/
		
		]
    });
    importproducts.window.UpdateItem.superclass.constructor.call(this,config);
};
Ext.extend(importproducts.window.UpdateItem,MODx.Window);
Ext.reg('modextra-window-item-update',importproducts.window.UpdateItem);