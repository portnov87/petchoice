var AjaxForm = {

	initialize: function() {
		if(!jQuery().ajaxForm) {
			document.write('<script src="'+afConfig.assetsUrl+'js/lib/jquery.form.min.js"><\/script>');
		}
		if(!jQuery().jGrowl) {
			document.write('<script src="'+afConfig.assetsUrl+'js/lib/jquery.jgrowl.min.js"><\/script>');
		}

		$(document).ready(function() {
			$.jGrowl.defaults.closerTemplate = '<div>[ '+afConfig.closeMessage+' ]</div>';
		});

		$(document).on('submit', afConfig.formSelector, function(e) {
			$(this).ajaxSubmit({
				dataType: 'json'
				,data: {pageId: afConfig.pageId}
				,url: afConfig.actionUrl
				,beforeSerialize: function(form, options) {
				
					form.find(':submit').each(function() {
						if (!form.find('input[type="hidden"][name="' + $(this).attr('name') + '"]').length) {
							$(form).append(
								$('<input type="hidden">').attr({
									name: $(this).attr('name'),
									value: $(this).attr('value')
								})
							);
						}
					})
				}
				,beforeSubmit: function(fields, form) {
				//alert('111');
					if (typeof(afValidated) != 'undefined' && afValidated == false) {
						return false;
					}
					form.find('.error').html('');
					form.find('.error').removeClass('error');
					form.find('input,textarea,select,button').attr('disabled', true);
					return true;
				}
				,success: function(response, status, xhr, form) {
					form.find('input,textarea,select,button').attr('disabled', false);
					response.form=form;
					
					$(document).trigger('af_complete', response);
					
					if (!response.success) {
						AjaxForm.Message.error(response.message);
						//response.message
						
						
						if (response.data) {
							var key, value;
							for (key in response.data) {
								if (response.data.hasOwnProperty(key)) {
									value = response.data[key];
									form.find('.error_' + key).html(value).addClass('error');
									form.find('[name="' + key + '"]').addClass('error');
								}
							}
						}
					}
					else {
						AjaxForm.Message.success(response.message);
						//alert(form.html());
						if(form.hasClass('ajaxaskform')) {
						form.find('.modal-body').html('<div class="thanks-text" style="text-align:center;">Заявка успешно отправлена. <br/>Менеджер свяжется с Вами когда товар появится в наличии</div>');
						form.find('.modal-footer').html('');
						
						}
						//alert(response.message);
						form.find('.error').removeClass('error');
						if(!form.hasClass('form-info-pets')) {
							form[0].reset();
						}
						if(form.attr('id') == 'add_pet_form') {
							$('#petModal').modal('hide');
							appendPet(response.data);
						}
						if(response.data.newphoto) {
							if(form.hasClass('form-info-pets')) {
								form.find('.img-circle').attr('src',response.data.newphoto);
							}
						}
					}
				}
			});
			e.preventDefault();
			return false;
		});

		$(document).on('reset', afConfig.formSelector, function(e) {
			$(this).find('.error').html('');
			AjaxForm.Message.close();
		});
	}

};


AjaxForm.Message = {
	success: function(message, sticky) {
		if (message) {
			if (!sticky) {sticky = false;}
			$.jGrowl(message, {theme: 'af-message-success', sticky: sticky});
		}
	}
	,error: function(message, sticky) {
		if (message) {
			if (!sticky) {sticky = false;}
			$.jGrowl(message, {theme: 'af-message-error', sticky: sticky});
		}
	}
	,info: function(message, sticky) {
		if (message) {
			if (!sticky) {sticky = false;}
			$.jGrowl(message, {theme: 'af-message-info', sticky: sticky});
		}
	}
	,close: function() {
		$.jGrowl('close');
	}
};


AjaxForm.initialize();
