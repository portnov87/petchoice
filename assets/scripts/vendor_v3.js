function setCookie(name, value, options) {
    options = options || {};
    var expires = options.expires;

    if (typeof expires == "number" && expires) {
        var d = new Date();
        d.setTime(d.getTime() + expires * 1000);
        expires = options.expires = d;
    }
    if (expires && expires.toUTCString) {
        options.expires = expires.toUTCString();
    }

    value = encodeURIComponent(value);

    var updatedCookie = name + "=" + value;

    for (var propName in options) {
        updatedCookie += "; " + propName;
        var propValue = options[propName];
        if (propValue !== true) {
            updatedCookie += "=" + propValue;
        }
    }

    document.cookie = updatedCookie;
}

// возвращает cookie с именем name, если есть, если нет, то undefined
function getCookie(name) {
    var matches = document.cookie.match(new RegExp(
        "(?:^|; )" + name.replace(/([\.$?*|{}\(\)\[\]\\\/\+^])/g, '\\$1') + "=([^;]*)"
    ));
    return matches ? decodeURIComponent(matches[1]) : undefined;
}

/*!
 * Validator v0.7.0 for Bootstrap 3, by @1000hz
 * Copyright 2015 Cina Saffary
 * Licensed under http://opensource.org/licenses/MIT
 *
 * https://github.com/1000hz/bootstrap-validator
 */

+function (a) {
    "use strict";

    function b(b) {
        return this.each(function () {
            var d = a(this), e = a.extend({}, c.DEFAULTS, d.data(), "object" == typeof b && b),
                f = d.data("bs.validator");
            (f || "destroy" != b) && (f || d.data("bs.validator", f = new c(this, e)), "string" == typeof b && f[b]())
        })
    }

    var c = function (b, c) {
        this.$element = a(b), this.options = c, this.$element.attr("novalidate", !0), this.toggleSubmit(), this.$element.on("input.bs.validator change.bs.validator focusout.bs.validator", a.proxy(this.validateInput, this)), this.$element.on("submit.bs.validator", a.proxy(this.onSubmit, this)), this.$element.find("[data-match]").each(function () {
            var b = a(this), c = b.data("match");
            a(c).on("input.bs.validator", function () {
                b.val() && b.trigger("input")
            })
        })
    };
    c.DEFAULTS = {
        delay: 500,
        html: !1,
        disable: !0,
        errors: {match: "Does not match", minlength: "Not long enough"}
    }, c.VALIDATORS = {
        "native": function (a) {
            var b = a[0];
            return b.checkValidity ? b.checkValidity() : !0
        }, match: function (b) {
            var c = b.data("match");
            return !b.val() || b.val() === a(c).val()
        }, minlength: function (a) {
            var b = a.data("minlength");
            return !a.val() || a.val().length >= b
        }
    }, c.prototype.validateInput = function (b) {
        var c = a(b.target), d = c.data("bs.validator.errors");
        if (c.is('[type="radio"]') && (c = this.$element.find('input[name="' + c.attr("name") + '"]')), this.$element.trigger(b = a.Event("validate.bs.validator", {relatedTarget: c[0]})), !b.isDefaultPrevented()) {
            var e = this;
            this.runValidators(c).done(function (f) {
                c.data("bs.validator.errors", f), f.length ? e.showErrors(c) : e.clearErrors(c), d && f.toString() === d.toString() || (b = f.length ? a.Event("invalid.bs.validator", {
                    relatedTarget: c[0],
                    detail: f
                }) : a.Event("valid.bs.validator", {
                    relatedTarget: c[0],
                    detail: d
                }), e.$element.trigger(b)), e.toggleSubmit(), e.$element.trigger(a.Event("validated.bs.validator", {relatedTarget: c[0]}))
            })
        }
    }, c.prototype.runValidators = function (b) {
        function d(a) {
            return b.data(a + "-error") || b.data("error") || "native" == a && b[0].validationMessage || g.errors[a]
        }

        var e = [], f = ([c.VALIDATORS.native], a.Deferred()), g = this.options;
        return b.data("bs.validator.deferred") && b.data("bs.validator.deferred").reject(), b.data("bs.validator.deferred", f), a.each(c.VALIDATORS, a.proxy(function (a, c) {
            if ((b.data(a) || "native" == a) && !c.call(this, b)) {
                var f = d(a);
                !~e.indexOf(f) && e.push(f)
            }
        }, this)), !e.length && b.val() && b.data("remote") ? this.defer(b, function () {
            var c = {};
            c[b.attr("name")] = b.val(), a.get(b.data("remote"), c).fail(function (a, b, c) {
                e.push(d("remote") || c)
            }).always(function () {
                f.resolve(e)
            })
        }) : f.resolve(e), f.promise()
    }, c.prototype.validate = function () {
        var a = this.options.delay;
        return this.options.delay = 0, this.$element.find(":input").trigger("input"), this.options.delay = a, this
    }, c.prototype.showErrors = function (b) {
        var c = this.options.html ? "html" : "text";
        this.defer(b, function () {
            var d = b.closest(".form-group"), e = d.find(".help-block.with-errors"), f = b.data("bs.validator.errors");
            f.length && (f = a("<ul/>").addClass("list-unstyled").append(a.map(f, function (b) {
                return a("<li/>")[c](b)
            })), void 0 === e.data("bs.validator.originalContent") && e.data("bs.validator.originalContent", e.html()), e.empty().append(f), d.addClass("has-error"))
        })
    }, c.prototype.clearErrors = function (a) {
        var b = a.closest(".form-group"), c = b.find(".help-block.with-errors");
        c.html(c.data("bs.validator.originalContent")), b.removeClass("has-error")
    }, c.prototype.hasErrors = function () {
        function b() {
            return !!(a(this).data("bs.validator.errors") || []).length
        }

        return !!this.$element.find(":input:enabled").filter(b).length
    }, c.prototype.isIncomplete = function () {
        function b() {
            return "checkbox" === this.type ? !this.checked : "radio" === this.type ? !a('[name="' + this.name + '"]:checked').length : "" === a.trim(this.value)
        }

        return !!this.$element.find(":input[required]:enabled").filter(b).length
    }, c.prototype.onSubmit = function (a) {
        this.validate(), (this.isIncomplete() || this.hasErrors()) && a.preventDefault()
    }, c.prototype.toggleSubmit = function () {
        if (this.options.disable) {
            var a = this.$element.find('input[type="submit"], button[type="submit"]');
            a.toggleClass("disabled", this.isIncomplete() || this.hasErrors()).css({
                "pointer-events": "all",
                cursor: "pointer"
            })
        }
    }, c.prototype.defer = function (a, b) {
        return this.options.delay ? (window.clearTimeout(a.data("bs.validator.timeout")), void a.data("bs.validator.timeout", window.setTimeout(b, this.options.delay))) : b()
    }, c.prototype.destroy = function () {
        return this.$element.removeAttr("novalidate").removeData("bs.validator").off(".bs.validator"), this.$element.find(":input").off(".bs.validator").removeData(["bs.validator.errors", "bs.validator.deferred"]).each(function () {
            var b = a(this), c = b.data("bs.validator.timeout");
            window.clearTimeout(c) && b.removeData("bs.validator.timeout")
        }), this.$element.find(".help-block.with-errors").each(function () {
            var b = a(this), c = b.data("bs.validator.originalContent");
            b.removeData("bs.validator.originalContent").html(c)
        }), this.$element.find('input[type="submit"], button[type="submit"]').removeClass("disabled"), this.$element.find(".has-error").removeClass("has-error"), this
    };
    var d = a.fn.validator;
    a.fn.validator = b, a.fn.validator.Constructor = c, a.fn.validator.noConflict = function () {
        return a.fn.validator = d, this
    }, a(window).on("load", function () {
        a('form[data-toggle="validator"]').each(function () {
            var c = a(this);
            b.call(c, c.data())
        })
    })
}(jQuery);


var matched, browser;

jQuery.uaMatch = function (ua) {
    ua = ua.toLowerCase();

    var match = /(chrome)[ \/]([\w.]+)/.exec(ua) ||
        /(webkit)[ \/]([\w.]+)/.exec(ua) ||
        /(opera)(?:.*version|)[ \/]([\w.]+)/.exec(ua) ||
        /(msie) ([\w.]+)/.exec(ua) ||
        ua.indexOf("compatible") < 0 && /(mozilla)(?:.*? rv:([\w.]+)|)/.exec(ua) ||
        [];

    return {
        browser: match[1] || "",
        version: match[2] || "0"
    };
};

matched = jQuery.uaMatch(navigator.userAgent);
browser = {};

if (matched.browser) {
    browser[matched.browser] = true;
    browser.version = matched.version;
}

// Chrome is Webkit, but Webkit is also Safari.
if (browser.chrome) {
    browser.webkit = true;
} else if (browser.webkit) {
    browser.safari = true;
}

jQuery.browser = browser;

function strpos(haystack, needle, offset) {
    var i = (haystack + '').indexOf(needle, (offset || 0));
    return i === -1 ? false : i;
}

$(document).ready(function () {

    $('#mse2_attributes_type_pet').on('change', function () {
        select_pet = $(this).val();
        //$(document).on('change', '.pet_type', function (e) {

        if (select_pet != '') {

            document.location.href = select_pet;
            //window.location.href = select_pet;
        }
    });
    if ($('.mail-order-validate').length) {
        validateemailyandex('.mail-order-validate');
    }

    if ($('.mail-personal-account').length) {
        validateemailyandex('.mail-personal-account');
    }

    $('.mail-order-validate input').on('keyup', function () {
        validateemailyandex('.mail-order-validate');
    });

    $('.mail-personal-account input').on('keyup', function () {
        validateemailyandex('.mail-personal-account');
    });


    function validateemailyandex(field) {
        val_email = $(field).find('input').val();
        indexOfYandexru = val_email.indexOf('@yandex.ru');
        indexOfYandexua = val_email.indexOf('@yandex.ua');

        indexOfMailua = val_email.indexOf('@mail.ua');
        indexOfMailru = val_email.indexOf('@mail.ru');
        if ((indexOfYandexru > 0) || (indexOfYandexua > 0) || (indexOfMailua > 0) || (indexOfMailru > 0)) {
            $(field).find('.small.grey').html('Указан неиспользуемый email? Замените на актуальный');//parent().parent().
        } else {
            $(field).find('.small.grey').html('');//parent().parent().
        }
    }


    $(document).on('click', "a.open-comment[data-toggle='modal']", function (e) {
        $_clObj = $(this); //clicked object


        $('#AnswerCommentModal').on('shown.bs.modal', {_clObj: $_clObj}, function (event) {
            //link = event.data._clObj; //$_clObj is the clicked element !!!
            // alert('_clObj');
            var link = event.data._clObj;//$(event.data._clObj);
            //alert(link.data('parent'));//event.relatedTarget);
            //do your stuff here...
            var parent = link.data('parent');
            $('#answer-comment-form').find("input[name='parent']").val(parent);
        });

    });


    $(document).on('mouseover', '.product-item.big-item', function () {
        $(this).find('.rowaction_info').slideDown();//400);//show('fast',"swing");//slideUp();//show('slow');//'slow');

    });
    $(document).on('mouseleave', '.product-item.big-item', function () {
        $(this).find('.rowaction_info').slideUp();//400);
        //$(this).find('.rowaction_info').hide('slow');

    });
});


(function (r, G, f, v) {
    var J = f("html"), n = f(r), p = f(G), b = f.fancybox = function () {
        b.open.apply(this, arguments)
    }, I = navigator.userAgent.match(/msie/i), B = null, s = G.createTouch !== v, t = function (a) {
        return a && a.hasOwnProperty && a instanceof f
    }, q = function (a) {
        return a && "string" === f.type(a)
    }, E = function (a) {
        return q(a) && 0 < a.indexOf("%")
    }, l = function (a, d) {
        var e = parseInt(a, 10) || 0;
        d && E(a) && (e *= b.getViewport()[d] / 100);
        return Math.ceil(e)
    }, w = function (a, b) {
        return l(a, b) + "px"
    };
    f.extend(b, {
        version: "2.1.5",
        defaults: {
            padding: 15,
            margin: 20,
            width: 800,
            height: 600,
            minWidth: 100,
            minHeight: 100,
            maxWidth: 9999,
            maxHeight: 9999,
            pixelRatio: 1,
            autoSize: !0,
            autoHeight: !1,
            autoWidth: !1,
            autoResize: !0,
            autoCenter: !s,
            fitToView: !0,
            aspectRatio: !1,
            topRatio: 0.5,
            leftRatio: 0.5,
            scrolling: "auto",
            wrapCSS: "",
            arrows: !0,
            closeBtn: !0,
            closeClick: !1,
            nextClick: !1,
            mouseWheel: !0,
            autoPlay: !1,
            playSpeed: 3E3,
            preload: 3,
            modal: !1,
            loop: !0,
            ajax: {dataType: "html", headers: {"X-fancyBox": !0}},
            iframe: {scrolling: "auto", preload: !0},
            swf: {wmode: "transparent", allowfullscreen: "true", allowscriptaccess: "always"},
            keys: {
                next: {13: "left", 34: "up", 39: "left", 40: "up"},
                prev: {8: "right", 33: "down", 37: "right", 38: "down"},
                close: [27],
                play: [32],
                toggle: [70]
            },
            direction: {next: "left", prev: "right"},
            scrollOutside: !0,
            index: 0,
            type: null,
            href: null,
            content: null,
            title: null,
            tpl: {
                wrap: '<div class="fancybox-wrap" tabIndex="-1"><div class="fancybox-skin"><div class="fancybox-outer"><div class="fancybox-inner"></div></div></div></div>',
                image: '<img class="fancybox-image" src="{href}" alt="" />',
                iframe: '<iframe id="fancybox-frame{rnd}" name="fancybox-frame{rnd}" class="fancybox-iframe" frameborder="0" vspace="0" hspace="0" webkitAllowFullScreen mozallowfullscreen allowFullScreen' + (I ? ' allowtransparency="true"' : "") + "></iframe>",
                error: '<p class="fancybox-error">The requested content cannot be loaded.<br/>Please try again later.</p>',
                closeBtn: '<a title="Закрыть" class="fancybox-item fancybox-close" href="javascript:;"></a>',
                next: '<a title="Следующая" class="fancybox-nav fancybox-next" href="javascript:;"><span></span></a>',
                prev: '<a title="Предыдущая" class="fancybox-nav fancybox-prev" href="javascript:;"><span></span></a>'
            },
            openEffect: "fade",
            openSpeed: 250,
            openEasing: "swing",
            openOpacity: !0,
            openMethod: "zoomIn",
            closeEffect: "fade",
            closeSpeed: 250,
            closeEasing: "swing",
            closeOpacity: !0,
            closeMethod: "zoomOut",
            nextEffect: "elastic",
            nextSpeed: 250,
            nextEasing: "swing",
            nextMethod: "changeIn",
            prevEffect: "elastic",
            prevSpeed: 250,
            prevEasing: "swing",
            prevMethod: "changeOut",
            helpers: {overlay: !0, title: !0},
            onCancel: f.noop,
            beforeLoad: f.noop,
            afterLoad: f.noop,
            beforeShow: f.noop,
            afterShow: f.noop,
            beforeChange: f.noop,
            beforeClose: f.noop,
            afterClose: f.noop
        },
        group: {},
        opts: {},
        previous: null,
        coming: null,
        current: null,
        isActive: !1,
        isOpen: !1,
        isOpened: !1,
        wrap: null,
        skin: null,
        outer: null,
        inner: null,
        player: {timer: null, isActive: !1},
        ajaxLoad: null,
        imgPreload: null,
        transitions: {},
        helpers: {},
        open: function (a, d) {
            if (a && (f.isPlainObject(d) || (d = {}), !1 !== b.close(!0))) return f.isArray(a) || (a = t(a) ? f(a).get() : [a]), f.each(a, function (e, c) {
                var k = {}, g, h, j, m, l;
                "object" === f.type(c) && (c.nodeType && (c = f(c)), t(c) ? (k = {
                    href: c.data("fancybox-href") || c.attr("href"),
                    title: c.data("fancybox-title") || c.attr("title"),
                    isDom: !0,
                    element: c
                }, f.metadata && f.extend(!0, k, c.metadata())) : k = c);
                g = d.href || k.href || (q(c) ? c : null);
                h = d.title !== v ? d.title : k.title || "";
                m = (j = d.content || k.content) ? "html" : d.type || k.type;
                !m && k.isDom && (m = c.data("fancybox-type"), m || (m = (m = c.prop("class").match(/fancybox\.(\w+)/)) ? m[1] : null));
                q(g) && (m || (b.isImage(g) ? m = "image" : b.isSWF(g) ? m = "swf" : "#" === g.charAt(0) ? m = "inline" : q(c) && (m = "html", j = c)), "ajax" === m && (l = g.split(/\s+/, 2), g = l.shift(), l = l.shift()));
                j || ("inline" === m ? g ? j = f(q(g) ? g.replace(/.*(?=#[^\s]+$)/, "") : g) : k.isDom && (j = c) : "html" === m ? j = g : !m && (!g && k.isDom) && (m = "inline", j = c));
                f.extend(k, {href: g, type: m, content: j, title: h, selector: l});
                a[e] = k
            }), b.opts = f.extend(!0, {}, b.defaults, d), d.keys !== v && (b.opts.keys = d.keys ? f.extend({}, b.defaults.keys, d.keys) : !1), b.group = a, b._start(b.opts.index)
        },
        cancel: function () {
            var a = b.coming;
            a && !1 !== b.trigger("onCancel") && (b.hideLoading(), b.ajaxLoad && b.ajaxLoad.abort(), b.ajaxLoad = null, b.imgPreload && (b.imgPreload.onload = b.imgPreload.onerror = null), a.wrap && a.wrap.stop(!0, !0).trigger("onReset").remove(), b.coming = null, b.current || b._afterZoomOut(a))
        },
        close: function (a) {
            b.cancel();
            !1 !== b.trigger("beforeClose") && (b.unbindEvents(), b.isActive && (!b.isOpen || !0 === a ? (f(".fancybox-wrap").stop(!0).trigger("onReset").remove(), b._afterZoomOut()) : (b.isOpen = b.isOpened = !1, b.isClosing = !0, f(".fancybox-item, .fancybox-nav").remove(), b.wrap.stop(!0, !0).removeClass("fancybox-opened"), b.transitions[b.current.closeMethod]())))
        },
        play: function (a) {
            var d = function () {
                clearTimeout(b.player.timer)
            }, e = function () {
                d();
                b.current && b.player.isActive && (b.player.timer = setTimeout(b.next, b.current.playSpeed))
            }, c = function () {
                d();
                p.unbind(".player");
                b.player.isActive = !1;
                b.trigger("onPlayEnd")
            };
            if (!0 === a || !b.player.isActive && !1 !== a) {
                if (b.current && (b.current.loop || b.current.index < b.group.length - 1)) b.player.isActive = !0, p.bind({
                    "onCancel.player beforeClose.player": c,
                    "onUpdate.player": e,
                    "beforeLoad.player": d
                }), e(), b.trigger("onPlayStart")
            } else c()
        },
        next: function (a) {
            var d = b.current;
            d && (q(a) || (a = d.direction.next), b.jumpto(d.index + 1, a, "next"))
        },
        prev: function (a) {
            var d = b.current;
            d && (q(a) || (a = d.direction.prev), b.jumpto(d.index - 1, a, "prev"))
        },
        jumpto: function (a, d, e) {
            var c = b.current;
            c && (a = l(a), b.direction = d || c.direction[a >= c.index ? "next" : "prev"], b.router = e || "jumpto", c.loop && (0 > a && (a = c.group.length + a % c.group.length), a %= c.group.length), c.group[a] !== v && (b.cancel(), b._start(a)))
        },
        reposition: function (a, d) {
            var e = b.current, c = e ? e.wrap : null, k;
            c && (k = b._getPosition(d), a && "scroll" === a.type ? (delete k.position, c.stop(!0, !0).animate(k, 200)) : (c.css(k), e.pos = f.extend({}, e.dim, k)))
        },
        update: function (a) {
            var d = a && a.type, e = !d || "orientationchange" === d;
            e && (clearTimeout(B), B = null);
            b.isOpen && !B && (B = setTimeout(function () {
                var c = b.current;
                c && !b.isClosing && (b.wrap.removeClass("fancybox-tmp"), (e || "load" === d || "resize" === d && c.autoResize) && b._setDimension(), "scroll" === d && c.canShrink || b.reposition(a), b.trigger("onUpdate"), B = null)
            }, e && !s ? 0 : 300))
        },
        toggle: function (a) {
            b.isOpen && (b.current.fitToView = "boolean" === f.type(a) ? a : !b.current.fitToView, s && (b.wrap.removeAttr("style").addClass("fancybox-tmp"), b.trigger("onUpdate")), b.update())
        },
        hideLoading: function () {
            p.unbind(".loading");
            f("#fancybox-loading").remove()
        },
        showLoading: function () {
            var a, d;
            b.hideLoading();
            a = f('<div id="fancybox-loading"><div></div></div>').click(b.cancel).appendTo("body");
            p.bind("keydown.loading", function (a) {
                if (27 === (a.which || a.keyCode)) a.preventDefault(), b.cancel()
            });
            b.defaults.fixed || (d = b.getViewport(), a.css({
                position: "absolute",
                top: 0.5 * d.h + d.y,
                left: 0.5 * d.w + d.x
            }))
        },
        getViewport: function () {
            var a = b.current && b.current.locked || !1, d = {x: n.scrollLeft(), y: n.scrollTop()};
            a ? (d.w = a[0].clientWidth, d.h = a[0].clientHeight) : (d.w = s && r.innerWidth ? r.innerWidth : n.width(), d.h = s && r.innerHeight ? r.innerHeight : n.height());
            return d
        },
        unbindEvents: function () {
            b.wrap && t(b.wrap) && b.wrap.unbind(".fb");
            p.unbind(".fb");
            n.unbind(".fb")
        },
        bindEvents: function () {
            var a = b.current, d;
            a && (n.bind("orientationchange.fb" + (s ? "" : " resize.fb") + (a.autoCenter && !a.locked ? " scroll.fb" : ""), b.update), (d = a.keys) && p.bind("keydown.fb", function (e) {
                var c = e.which || e.keyCode, k = e.target || e.srcElement;
                if (27 === c && b.coming) return !1;
                !e.ctrlKey && (!e.altKey && !e.shiftKey && !e.metaKey && (!k || !k.type && !f(k).is("[contenteditable]"))) && f.each(d, function (d, k) {
                    if (1 < a.group.length && k[c] !== v) return b[d](k[c]), e.preventDefault(), !1;
                    if (-1 < f.inArray(c, k)) return b[d](), e.preventDefault(), !1
                })
            }), f.fn.mousewheel && a.mouseWheel && b.wrap.bind("mousewheel.fb", function (d, c, k, g) {
                for (var h = f(d.target || null), j = !1; h.length && !j && !h.is(".fancybox-skin") && !h.is(".fancybox-wrap");) j = h[0] && !(h[0].style.overflow && "hidden" === h[0].style.overflow) && (h[0].clientWidth && h[0].scrollWidth > h[0].clientWidth || h[0].clientHeight && h[0].scrollHeight > h[0].clientHeight), h = f(h).parent();
                if (0 !== c && !j && 1 < b.group.length && !a.canShrink) {
                    if (0 < g || 0 < k) b.prev(0 < g ? "down" : "left"); else if (0 > g || 0 > k) b.next(0 > g ? "up" : "right");
                    d.preventDefault()
                }
            }))
        },
        trigger: function (a, d) {
            var e, c = d || b.coming || b.current;
            if (c) {
                f.isFunction(c[a]) && (e = c[a].apply(c, Array.prototype.slice.call(arguments, 1)));
                if (!1 === e) return !1;
                c.helpers && f.each(c.helpers, function (d, e) {
                    if (e && b.helpers[d] && f.isFunction(b.helpers[d][a])) b.helpers[d][a](f.extend(!0, {}, b.helpers[d].defaults, e), c)
                });
                p.trigger(a)
            }
        },
        isImage: function (a) {
            return q(a) && a.match(/(^data:image\/.*,)|(\.(jp(e|g|eg)|gif|png|bmp|webp|svg)((\?|#).*)?$)/i)
        },
        isSWF: function (a) {
            return q(a) && a.match(/\.(swf)((\?|#).*)?$/i)
        },
        _start: function (a) {
            var d = {}, e, c;
            a = l(a);
            e = b.group[a] || null;
            if (!e) return !1;
            d = f.extend(!0, {}, b.opts, e);
            e = d.margin;
            c = d.padding;
            "number" === f.type(e) && (d.margin = [e, e, e, e]);
            "number" === f.type(c) && (d.padding = [c, c, c, c]);
            d.modal && f.extend(!0, d, {
                closeBtn: !1,
                closeClick: !1,
                nextClick: !1,
                arrows: !1,
                mouseWheel: !1,
                keys: null,
                helpers: {overlay: {closeClick: !1}}
            });
            d.autoSize && (d.autoWidth = d.autoHeight = !0);
            "auto" === d.width && (d.autoWidth = !0);
            "auto" === d.height && (d.autoHeight = !0);
            d.group = b.group;
            d.index = a;
            b.coming = d;
            if (!1 === b.trigger("beforeLoad")) b.coming = null; else {
                c = d.type;
                e = d.href;
                if (!c) return b.coming = null, b.current && b.router && "jumpto" !== b.router ? (b.current.index = a, b[b.router](b.direction)) : !1;
                b.isActive = !0;
                if ("image" === c || "swf" === c) d.autoHeight = d.autoWidth = !1, d.scrolling = "visible";
                "image" === c && (d.aspectRatio = !0);
                "iframe" === c && s && (d.scrolling = "scroll");
                d.wrap = f(d.tpl.wrap).addClass("fancybox-" + (s ? "mobile" : "desktop") + " fancybox-type-" + c + " fancybox-tmp " + d.wrapCSS).appendTo(d.parent || "body");
                f.extend(d, {
                    skin: f(".fancybox-skin", d.wrap),
                    outer: f(".fancybox-outer", d.wrap),
                    inner: f(".fancybox-inner", d.wrap)
                });
                f.each(["Top", "Right", "Bottom", "Left"], function (a, b) {
                    d.skin.css("padding" + b, w(d.padding[a]))
                });
                b.trigger("onReady");
                if ("inline" === c || "html" === c) {
                    if (!d.content || !d.content.length) return b._error("content")
                } else if (!e) return b._error("href");
                "image" === c ? b._loadImage() : "ajax" === c ? b._loadAjax() : "iframe" === c ? b._loadIframe() : b._afterLoad()
            }
        },
        _error: function (a) {
            f.extend(b.coming, {
                type: "html",
                autoWidth: !0,
                autoHeight: !0,
                minWidth: 0,
                minHeight: 0,
                scrolling: "no",
                hasError: a,
                content: b.coming.tpl.error
            });
            b._afterLoad()
        },
        _loadImage: function () {
            var a = b.imgPreload = new Image;
            a.onload = function () {
                this.onload = this.onerror = null;
                b.coming.width = this.width / b.opts.pixelRatio;
                b.coming.height = this.height / b.opts.pixelRatio;
                b._afterLoad()
            };
            a.onerror = function () {
                this.onload = this.onerror = null;
                b._error("image")
            };
            a.src = b.coming.href;
            !0 !== a.complete && b.showLoading()
        },
        _loadAjax: function () {
            var a = b.coming;
            b.showLoading();
            b.ajaxLoad = f.ajax(f.extend({}, a.ajax, {
                url: a.href, error: function (a, e) {
                    b.coming && "abort" !== e ? b._error("ajax", a) : b.hideLoading()
                }, success: function (d, e) {
                    "success" === e && (a.content = d, b._afterLoad())
                }
            }))
        },
        _loadIframe: function () {
            var a = b.coming,
                d = f(a.tpl.iframe.replace(/\{rnd\}/g, (new Date).getTime())).attr("scrolling", s ? "auto" : a.iframe.scrolling).attr("src", a.href);
            f(a.wrap).bind("onReset", function () {
                try {
                    f(this).find("iframe").hide().attr("src", "//about:blank").end().empty()
                } catch (a) {
                }
            });
            a.iframe.preload && (b.showLoading(), d.one("load", function () {
                f(this).data("ready", 1);
                s || f(this).bind("load.fb", b.update);
                f(this).parents(".fancybox-wrap").width("100%").removeClass("fancybox-tmp").show();
                b._afterLoad()
            }));
            a.content = d.appendTo(a.inner);
            a.iframe.preload || b._afterLoad()
        },
        _preloadImages: function () {
            var a = b.group, d = b.current, e = a.length, c = d.preload ? Math.min(d.preload, e - 1) : 0, f, g;
            for (g = 1; g <= c; g += 1) f = a[(d.index + g) % e], "image" === f.type && f.href && ((new Image).src = f.href)
        },
        _afterLoad: function () {
            var a = b.coming, d = b.current, e, c, k, g, h;
            b.hideLoading();
            if (a && !1 !== b.isActive) if (!1 === b.trigger("afterLoad", a, d)) a.wrap.stop(!0).trigger("onReset").remove(), b.coming = null; else {
                d && (b.trigger("beforeChange", d), d.wrap.stop(!0).removeClass("fancybox-opened").find(".fancybox-item, .fancybox-nav").remove());
                b.unbindEvents();
                e = a.content;
                c = a.type;
                k = a.scrolling;
                f.extend(b, {wrap: a.wrap, skin: a.skin, outer: a.outer, inner: a.inner, current: a, previous: d});
                g = a.href;
                switch (c) {
                    case"inline":
                    case"ajax":
                    case"html":
                        a.selector ? e = f("<div>").html(e).find(a.selector) : t(e) && (e.data("fancybox-placeholder") || e.data("fancybox-placeholder", f('<div class="fancybox-placeholder"></div>').insertAfter(e).hide()), e = e.show().detach(), a.wrap.bind("onReset", function () {
                            f(this).find(e).length && e.hide().replaceAll(e.data("fancybox-placeholder")).data("fancybox-placeholder", !1)
                        }));
                        break;
                    case"image":
                        e = a.tpl.image.replace("{href}", g);
                        break;
                    case"swf":
                        e = '<object id="fancybox-swf" classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000" width="100%" height="100%"><param name="movie" value="' + g + '"></param>', h = "", f.each(a.swf, function (a, b) {
                            e += '<param name="' + a + '" value="' + b + '"></param>';
                            h += " " + a + '="' + b + '"'
                        }), e += '<embed src="' + g + '" type="application/x-shockwave-flash" width="100%" height="100%"' + h + "></embed></object>"
                }
                (!t(e) || !e.parent().is(a.inner)) && a.inner.append(e);
                b.trigger("beforeShow");
                a.inner.css("overflow", "yes" === k ? "scroll" : "no" === k ? "hidden" : k);
                b._setDimension();
                b.reposition();
                b.isOpen = !1;
                b.coming = null;
                b.bindEvents();
                if (b.isOpened) {
                    if (d.prevMethod) b.transitions[d.prevMethod]()
                } else f(".fancybox-wrap").not(a.wrap).stop(!0).trigger("onReset").remove();
                b.transitions[b.isOpened ? a.nextMethod : a.openMethod]();
                b._preloadImages()
            }
        },
        _setDimension: function () {
            var a = b.getViewport(), d = 0, e = !1, c = !1, e = b.wrap, k = b.skin, g = b.inner, h = b.current,
                c = h.width, j = h.height, m = h.minWidth, u = h.minHeight, n = h.maxWidth, p = h.maxHeight,
                s = h.scrolling, q = h.scrollOutside ? h.scrollbarWidth : 0, x = h.margin, y = l(x[1] + x[3]),
                r = l(x[0] + x[2]), v, z, t, C, A, F, B, D, H;
            e.add(k).add(g).width("auto").height("auto").removeClass("fancybox-tmp");
            x = l(k.outerWidth(!0) - k.width());
            v = l(k.outerHeight(!0) - k.height());
            z = y + x;
            t = r + v;
            C = E(c) ? (a.w - z) * l(c) / 100 : c;
            A = E(j) ? (a.h - t) * l(j) / 100 : j;
            if ("iframe" === h.type) {
                if (H = h.content, h.autoHeight && 1 === H.data("ready")) try {
                    H[0].contentWindow.document.location && (g.width(C).height(9999), F = H.contents().find("body"), q && F.css("overflow-x", "hidden"), A = F.outerHeight(!0))
                } catch (G) {
                }
            } else if (h.autoWidth || h.autoHeight) g.addClass("fancybox-tmp"), h.autoWidth || g.width(C), h.autoHeight || g.height(A), h.autoWidth && (C = g.width()), h.autoHeight && (A = g.height()), g.removeClass("fancybox-tmp");
            c = l(C);
            j = l(A);
            D = C / A;
            m = l(E(m) ? l(m, "w") - z : m);
            n = l(E(n) ? l(n, "w") - z : n);
            u = l(E(u) ? l(u, "h") - t : u);
            p = l(E(p) ? l(p, "h") - t : p);
            F = n;
            B = p;
            h.fitToView && (n = Math.min(a.w - z, n), p = Math.min(a.h - t, p));
            z = a.w - y;
            r = a.h - r;
            h.aspectRatio ? (c > n && (c = n, j = l(c / D)), j > p && (j = p, c = l(j * D)), c < m && (c = m, j = l(c / D)), j < u && (j = u, c = l(j * D))) : (c = Math.max(m, Math.min(c, n)), h.autoHeight && "iframe" !== h.type && (g.width(c), j = g.height()), j = Math.max(u, Math.min(j, p)));
            if (h.fitToView) if (g.width(c).height(j), e.width(c + x), a = e.width(), y = e.height(), h.aspectRatio) for (; (a > z || y > r) && (c > m && j > u) && !(19 < d++);) j = Math.max(u, Math.min(p, j - 10)), c = l(j * D), c < m && (c = m, j = l(c / D)), c > n && (c = n, j = l(c / D)), g.width(c).height(j), e.width(c + x), a = e.width(), y = e.height(); else c = Math.max(m, Math.min(c, c - (a - z))), j = Math.max(u, Math.min(j, j - (y - r)));
            q && ("auto" === s && j < A && c + x + q < z) && (c += q);
            g.width(c).height(j);
            e.width(c + x);
            a = e.width();
            y = e.height();
            e = (a > z || y > r) && c > m && j > u;
            c = h.aspectRatio ? c < F && j < B && c < C && j < A : (c < F || j < B) && (c < C || j < A);
            f.extend(h, {
                dim: {width: w(a), height: w(y)},
                origWidth: C,
                origHeight: A,
                canShrink: e,
                canExpand: c,
                wPadding: x,
                hPadding: v,
                wrapSpace: y - k.outerHeight(!0),
                skinSpace: k.height() - j
            });
            !H && (h.autoHeight && j > u && j < p && !c) && g.height("auto")
        },
        _getPosition: function (a) {
            var d = b.current, e = b.getViewport(), c = d.margin, f = b.wrap.width() + c[1] + c[3],
                g = b.wrap.height() + c[0] + c[2], c = {position: "absolute", top: c[0], left: c[3]};
            d.autoCenter && d.fixed && !a && g <= e.h && f <= e.w ? c.position = "fixed" : d.locked || (c.top += e.y, c.left += e.x);
            c.top = w(Math.max(c.top, c.top + (e.h - g) * d.topRatio));
            c.left = w(Math.max(c.left, c.left + (e.w - f) * d.leftRatio));
            return c
        },
        _afterZoomIn: function () {
            var a = b.current;
            a && (b.isOpen = b.isOpened = !0, b.wrap.css("overflow", "visible").addClass("fancybox-opened"), b.update(), (a.closeClick || a.nextClick && 1 < b.group.length) && b.inner.css("cursor", "pointer").bind("click.fb", function (d) {
                !f(d.target).is("a") && !f(d.target).parent().is("a") && (d.preventDefault(), b[a.closeClick ? "close" : "next"]())
            }), a.closeBtn && f(a.tpl.closeBtn).appendTo(b.skin).bind("click.fb", function (a) {
                a.preventDefault();
                b.close()
            }), a.arrows && 1 < b.group.length && ((a.loop || 0 < a.index) && f(a.tpl.prev).appendTo(b.outer).bind("click.fb", b.prev), (a.loop || a.index < b.group.length - 1) && f(a.tpl.next).appendTo(b.outer).bind("click.fb", b.next)), b.trigger("afterShow"), !a.loop && a.index === a.group.length - 1 ? b.play(!1) : b.opts.autoPlay && !b.player.isActive && (b.opts.autoPlay = !1, b.play()))
        },
        _afterZoomOut: function (a) {
            a = a || b.current;
            f(".fancybox-wrap").trigger("onReset").remove();
            f.extend(b, {
                group: {},
                opts: {},
                router: !1,
                current: null,
                isActive: !1,
                isOpened: !1,
                isOpen: !1,
                isClosing: !1,
                wrap: null,
                skin: null,
                outer: null,
                inner: null
            });
            b.trigger("afterClose", a)
        }
    });
    b.transitions = {
        getOrigPosition: function () {
            var a = b.current, d = a.element, e = a.orig, c = {}, f = 50, g = 50, h = a.hPadding, j = a.wPadding,
                m = b.getViewport();
            !e && (a.isDom && d.is(":visible")) && (e = d.find("img:first"), e.length || (e = d));
            t(e) ? (c = e.offset(), e.is("img") && (f = e.outerWidth(), g = e.outerHeight())) : (c.top = m.y + (m.h - g) * a.topRatio, c.left = m.x + (m.w - f) * a.leftRatio);
            if ("fixed" === b.wrap.css("position") || a.locked) c.top -= m.y, c.left -= m.x;
            return c = {
                top: w(c.top - h * a.topRatio),
                left: w(c.left - j * a.leftRatio),
                width: w(f + j),
                height: w(g + h)
            }
        }, step: function (a, d) {
            var e, c, f = d.prop;
            c = b.current;
            var g = c.wrapSpace, h = c.skinSpace;
            if ("width" === f || "height" === f) e = d.end === d.start ? 1 : (a - d.start) / (d.end - d.start), b.isClosing && (e = 1 - e), c = "width" === f ? c.wPadding : c.hPadding, c = a - c, b.skin[f](l("width" === f ? c : c - g * e)), b.inner[f](l("width" === f ? c : c - g * e - h * e))
        }, zoomIn: function () {
            var a = b.current, d = a.pos, e = a.openEffect, c = "elastic" === e, k = f.extend({opacity: 1}, d);
            delete k.position;
            c ? (d = this.getOrigPosition(), a.openOpacity && (d.opacity = 0.1)) : "fade" === e && (d.opacity = 0.1);
            b.wrap.css(d).animate(k, {
                duration: "none" === e ? 0 : a.openSpeed,
                easing: a.openEasing,
                step: c ? this.step : null,
                complete: b._afterZoomIn
            })
        }, zoomOut: function () {
            var a = b.current, d = a.closeEffect, e = "elastic" === d, c = {opacity: 0.1};
            e && (c = this.getOrigPosition(), a.closeOpacity && (c.opacity = 0.1));
            b.wrap.animate(c, {
                duration: "none" === d ? 0 : a.closeSpeed,
                easing: a.closeEasing,
                step: e ? this.step : null,
                complete: b._afterZoomOut
            })
        }, changeIn: function () {
            var a = b.current, d = a.nextEffect, e = a.pos, c = {opacity: 1}, f = b.direction, g;
            e.opacity = 0.1;
            "elastic" === d && (g = "down" === f || "up" === f ? "top" : "left", "down" === f || "right" === f ? (e[g] = w(l(e[g]) - 200), c[g] = "+=200px") : (e[g] = w(l(e[g]) + 200), c[g] = "-=200px"));
            "none" === d ? b._afterZoomIn() : b.wrap.css(e).animate(c, {
                duration: a.nextSpeed,
                easing: a.nextEasing,
                complete: b._afterZoomIn
            })
        }, changeOut: function () {
            var a = b.previous, d = a.prevEffect, e = {opacity: 0.1}, c = b.direction;
            "elastic" === d && (e["down" === c || "up" === c ? "top" : "left"] = ("up" === c || "left" === c ? "-" : "+") + "=200px");
            a.wrap.animate(e, {
                duration: "none" === d ? 0 : a.prevSpeed, easing: a.prevEasing, complete: function () {
                    f(this).trigger("onReset").remove()
                }
            })
        }
    };
    b.helpers.overlay = {
        defaults: {closeClick: !0, speedOut: 200, showEarly: !0, css: {}, locked: !s, fixed: !0},
        overlay: null,
        fixed: !1,
        el: f("html"),
        create: function (a) {
            a = f.extend({}, this.defaults, a);
            this.overlay && this.close();
            this.overlay = f('<div class="fancybox-overlay"></div>').appendTo(b.coming ? b.coming.parent : a.parent);
            this.fixed = !1;
            a.fixed && b.defaults.fixed && (this.overlay.addClass("fancybox-overlay-fixed"), this.fixed = !0)
        },
        open: function (a) {
            var d = this;
            a = f.extend({}, this.defaults, a);
            this.overlay ? this.overlay.unbind(".overlay").width("auto").height("auto") : this.create(a);
            this.fixed || (n.bind("resize.overlay", f.proxy(this.update, this)), this.update());
            a.closeClick && this.overlay.bind("click.overlay", function (a) {
                if (f(a.target).hasClass("fancybox-overlay")) return b.isActive ? b.close() : d.close(), !1
            });
            this.overlay.css(a.css).show()
        },
        close: function () {
            var a, b;
            n.unbind("resize.overlay");
            this.el.hasClass("fancybox-lock") && (f(".fancybox-margin").removeClass("fancybox-margin"), a = n.scrollTop(), b = n.scrollLeft(), this.el.removeClass("fancybox-lock"), n.scrollTop(a).scrollLeft(b));
            f(".fancybox-overlay").remove().hide();
            f.extend(this, {overlay: null, fixed: !1})
        },
        update: function () {
            var a = "100%", b;
            this.overlay.width(a).height("100%");
            I ? (b = Math.max(G.documentElement.offsetWidth, G.body.offsetWidth), p.width() > b && (a = p.width())) : p.width() > n.width() && (a = p.width());
            this.overlay.width(a).height(p.height())
        },
        onReady: function (a, b) {
            var e = this.overlay;
            f(".fancybox-overlay").stop(!0, !0);
            e || this.create(a);
            a.locked && (this.fixed && b.fixed) && (e || (this.margin = p.height() > n.height() ? f("html").css("margin-right").replace("px", "") : !1), b.locked = this.overlay.append(b.wrap), b.fixed = !1);
            !0 === a.showEarly && this.beforeShow.apply(this, arguments)
        },
        beforeShow: function (a, b) {
            var e, c;
            b.locked && (!1 !== this.margin && (f("*").filter(function () {
                return "fixed" === f(this).css("position") && !f(this).hasClass("fancybox-overlay") && !f(this).hasClass("fancybox-wrap")
            }).addClass("fancybox-margin"), this.el.addClass("fancybox-margin")), e = n.scrollTop(), c = n.scrollLeft(), this.el.addClass("fancybox-lock"), n.scrollTop(e).scrollLeft(c));
            this.open(a)
        },
        onUpdate: function () {
            this.fixed || this.update()
        },
        afterClose: function (a) {
            this.overlay && !b.coming && this.overlay.fadeOut(a.speedOut, f.proxy(this.close, this))
        }
    };
    b.helpers.title = {
        defaults: {type: "float", position: "bottom"}, beforeShow: function (a) {
            var d = b.current, e = d.title, c = a.type;
            f.isFunction(e) && (e = e.call(d.element, d));
            if (q(e) && "" !== f.trim(e)) {
                d = f('<div class="fancybox-title fancybox-title-' + c + '-wrap">' + e + "</div>");
                switch (c) {
                    case"inside":
                        c = b.skin;
                        break;
                    case"outside":
                        c = b.wrap;
                        break;
                    case"over":
                        c = b.inner;
                        break;
                    default:
                        c = b.skin, d.appendTo("body"), I && d.width(d.width()), d.wrapInner('<span class="child"></span>'), b.current.margin[2] += Math.abs(l(d.css("margin-bottom")))
                }
                d["top" === a.position ? "prependTo" : "appendTo"](c)
            }
        }
    };
    f.fn.fancybox = function (a) {
        var d, e = f(this), c = this.selector || "", k = function (g) {
            var h = f(this).blur(), j = d, k, l;
            !g.ctrlKey && (!g.altKey && !g.shiftKey && !g.metaKey) && !h.is(".fancybox-wrap") && (k = a.groupAttr || "data-fancybox-group", l = h.attr(k), l || (k = "rel", l = h.get(0)[k]), l && ("" !== l && "nofollow" !== l) && (h = c.length ? f(c) : e, h = h.filter("[" + k + '="' + l + '"]'), j = h.index(this)), a.index = j, !1 !== b.open(h, a) && g.preventDefault())
        };
        a = a || {};
        d = a.index || 0;
        !c || !1 === a.live ? e.unbind("click.fb-start").bind("click.fb-start", k) : p.undelegate(c, "click.fb-start").delegate(c + ":not('.fancybox-item, .fancybox-nav')", "click.fb-start", k);
        this.filter("[data-fancybox-start=1]").trigger("click");
        return this
    };
    p.ready(function () {
        var a, d;
        f.scrollbarWidth === v && (f.scrollbarWidth = function () {
            var a = f('<div style="width:50px;height:50px;overflow:auto"><div/></div>').appendTo("body"),
                b = a.children(), b = b.innerWidth() - b.height(99).innerWidth();
            a.remove();
            return b
        });
        if (f.support.fixedPosition === v) {
            a = f.support;
            d = f('<div style="position:fixed;top:20px;"></div>').appendTo("body");
            var e = 20 === d[0].offsetTop || 15 === d[0].offsetTop;
            d.remove();
            a.fixedPosition = e
        }
        f.extend(b.defaults, {scrollbarWidth: f.scrollbarWidth(), fixed: f.support.fixedPosition, parent: f("body")});
        a = f(r).width();
        J.addClass("fancybox-lock-test");
        d = f(r).width();
        J.removeClass("fancybox-lock-test");
        f("<style type='text/css'>.fancybox-margin{margin-right:" + (d - a) + "px;}</style>").appendTo("head")
    })
})(window, document, jQuery);


!function (a, b) {
    "use strict";
    "undefined" != typeof module && module.exports ? module.exports = b(require("jquery")) : "function" == typeof define && define.amd ? define(["jquery"], function (a) {
        return b(a)
    }) : b(a.jQuery)
}(this, function (a) {
    "use strict";
    var b = function (b, c) {
        this.$element = a(b), this.options = a.extend({}, a.fn.typeahead.defaults, c), this.matcher = this.options.matcher || this.matcher, this.sorter = this.options.sorter || this.sorter, this.select = this.options.select || this.select, this.autoSelect = "boolean" == typeof this.options.autoSelect ? this.options.autoSelect : !0, this.highlighter = this.options.highlighter || this.highlighter, this.render = this.options.render || this.render, this.updater = this.options.updater || this.updater, this.displayText = this.options.displayText || this.displayText, this.source = this.options.source, this.delay = this.options.delay, this.$menu = a(this.options.menu), this.$appendTo = this.options.appendTo ? a(this.options.appendTo) : null, this.shown = !1, this.listen(), this.showHintOnFocus = "boolean" == typeof this.options.showHintOnFocus ? this.options.showHintOnFocus : !1, this.afterSelect = this.options.afterSelect, this.addItem = !1
    };
    b.prototype = {
        constructor: b, select: function () {
            var a = this.$menu.find(".active").data("value");
            if (this.$element.data("active", a), this.autoSelect || a) {
                var b = this.updater(a);
                this.$element.val(this.displayText(b) || b).change(), this.afterSelect(b)
            }
            return this.hide()
        }, updater: function (a) {
            return a
        }, setSource: function (a) {
            this.source = a
        }, show: function () {
            var b, c = a.extend({}, this.$element.position(), {height: this.$element[0].offsetHeight});
            return b = "function" == typeof this.options.scrollHeight ? this.options.scrollHeight.call() : this.options.scrollHeight, (this.$appendTo ? this.$menu.appendTo(this.$appendTo) : this.$menu.insertAfter(this.$element)).css({
                top: c.top + c.height + b,
                left: c.left
            }).show(), this.shown = !0, this
        }, hide: function () {
            return this.$menu.hide(), this.shown = !1, this
        }, lookup: function (b) {
            if (this.query = "undefined" != typeof b && null !== b ? b : this.$element.val() || "", this.query.length < this.options.minLength) return this.shown ? this.hide() : this;
            var c = a.proxy(function () {
                a.isFunction(this.source) ? this.source(this.query, a.proxy(this.process, this)) : this.source && this.process(this.source)
            }, this);
            clearTimeout(this.lookupWorker), this.lookupWorker = setTimeout(c, this.delay)
        }, process: function (b) {
            var c = this;
            return b = a.grep(b, function (a) {
                return c.matcher(a)
            }), b = this.sorter(b), b.length || this.options.addItem ? (b.length > 0 ? this.$element.data("active", b[0]) : this.$element.data("active", null), this.options.addItem && b.push(this.options.addItem), "all" == this.options.items ? this.render(b).show() : this.render(b.slice(0, this.options.items)).show()) : this.shown ? this.hide() : this
        }, matcher: function (a) {
            var b = this.displayText(a);
            return ~b.toLowerCase().indexOf(this.query.toLowerCase())
        }, sorter: function (a) {
            for (var b, c = [], d = [], e = []; b = a.shift();) {
                var f = this.displayText(b);
                f.toLowerCase().indexOf(this.query.toLowerCase()) ? ~f.indexOf(this.query) ? d.push(b) : e.push(b) : c.push(b)
            }
            return c.concat(d, e)
        }, highlighter: function (b) {
            var c, d, e, f, g, h = a("<div></div>"), i = this.query, j = b.toLowerCase().indexOf(i.toLowerCase());
            if (c = i.length, 0 === c) return h.text(b).html();
            for (; j > -1;) d = b.substr(0, j), e = b.substr(j, c), f = b.substr(j + c), g = a("<strong></strong>").text(e), h.append(document.createTextNode(d)).append(g), b = f, j = b.toLowerCase().indexOf(i.toLowerCase());
            return h.append(document.createTextNode(b)).html()
        }, render: function (b) {
            var c = this, d = this, e = !1;
            return b = a(b).map(function (b, f) {
                var g = d.displayText(f);
                return b = a(c.options.item).data("value", f), b.find("a").html(c.highlighter(g)), g == d.$element.val() && (b.addClass("active"), d.$element.data("active", f), e = !0), b[0]
            }), this.autoSelect && !e && (b.first().addClass("active"), this.$element.data("active", b.first().data("value"))), this.$menu.html(b), this
        }, displayText: function (a) {
            return a.name || a
        }, next: function (b) {
            var c = this.$menu.find(".active").removeClass("active"), d = c.next();
            d.length || (d = a(this.$menu.find("li")[0])), d.addClass("active")
        }, prev: function (a) {
            var b = this.$menu.find(".active").removeClass("active"), c = b.prev();
            c.length || (c = this.$menu.find("li").last()), c.addClass("active")
        }, listen: function () {
            this.$element.on("focus", a.proxy(this.focus, this)).on("blur", a.proxy(this.blur, this)).on("keypress", a.proxy(this.keypress, this)).on("keyup", a.proxy(this.keyup, this)), this.eventSupported("keydown") && this.$element.on("keydown", a.proxy(this.keydown, this)), this.$menu.on("click", a.proxy(this.click, this)).on("mouseenter", "li", a.proxy(this.mouseenter, this)).on("mouseleave", "li", a.proxy(this.mouseleave, this))
        }, destroy: function () {
            this.$element.data("typeahead", null), this.$element.data("active", null), this.$element.off("focus").off("blur").off("keypress").off("keyup"), this.eventSupported("keydown") && this.$element.off("keydown"), this.$menu.remove()
        }, eventSupported: function (a) {
            var b = a in this.$element;
            return b || (this.$element.setAttribute(a, "return;"), b = "function" == typeof this.$element[a]), b
        }, move: function (a) {
            if (this.shown) {
                switch (a.keyCode) {
                    case 9:
                    case 13:
                    case 27:
                        a.preventDefault();
                        break;
                    case 38:
                        if (a.shiftKey) return;
                        a.preventDefault(), this.prev();
                        break;
                    case 40:
                        if (a.shiftKey) return;
                        a.preventDefault(), this.next()
                }
                a.stopPropagation()
            }
        }, keydown: function (b) {
            this.suppressKeyPressRepeat = ~a.inArray(b.keyCode, [40, 38, 9, 13, 27]), this.shown || 40 != b.keyCode ? this.move(b) : this.lookup()
        }, keypress: function (a) {
            this.suppressKeyPressRepeat || this.move(a)
        }, keyup: function (a) {
            switch (a.keyCode) {
                case 40:
                case 38:
                case 16:
                case 17:
                case 18:
                    break;
                case 9:
                case 13:
                    if (!this.shown) return;
                    this.select();
                    break;
                case 27:
                    if (!this.shown) return;
                    this.hide();
                    break;
                default:
                    this.lookup()
            }
            a.stopPropagation(), a.preventDefault()
        }, focus: function (a) {
        }, blur: function (a) {
            this.focused = !1, !this.mousedover && this.shown && this.hide()
        }, click: function (a) {
            a.stopPropagation(), a.preventDefault(), this.select(), this.$element.focus()
        }, mouseenter: function (b) {
            this.mousedover = !0, this.$menu.find(".active").removeClass("active"), a(b.currentTarget).addClass("active")
        }, mouseleave: function (a) {
            this.mousedover = !1, !this.focused && this.shown
        }
    };
    var c = a.fn.typeahead;
    a.fn.typeahead = function (c) {
        var d = arguments;
        return "string" == typeof c && "getActive" == c ? this.data("active") : this.each(function () {
            var e = a(this), f = e.data("typeahead"), g = "object" == typeof c && c;
            f || e.data("typeahead", f = new b(this, g)), "string" == typeof c && (d.length > 1 ? f[c].apply(f, Array.prototype.slice.call(d, 1)) : f[c]())
        })
    }, a.fn.typeahead.defaults = {
        source: [],
        items: 8,
        menu: '<ul class="typeahead dropdown-menu" role="listbox"></ul>',
        item: '<li><a href="#" role="option"></a></li>',
        minLength: 1,
        scrollHeight: 0,
        autoSelect: !0,
        afterSelect: a.noop,
        addItem: !1,
        delay: 0
    }, a.fn.typeahead.Constructor = b, a.fn.typeahead.noConflict = function () {
        return a.fn.typeahead = c, this
    }, a(document).on("focus.typeahead.data-api", '[data-provide="typeahead"]', function (b) {
        var c = a(this);
        c.data("typeahead") || c.typeahead(c.data())
    })
});
!function (e) {
    "use strict";
    "function" == typeof define && define.amd ? define(["jquery"], e) : e("undefined" != typeof jQuery ? jQuery : window.Zepto)
}(function (e) {
    "use strict";

    function t(t) {
        var r = t.data;
        t.isDefaultPrevented() || (t.preventDefault(), e(t.target).ajaxSubmit(r))
    }

    function r(t) {
        var r = t.target, a = e(r);
        if (!a.is("[type=submit],[type=image]")) {
            var n = a.closest("[type=submit]");
            if (0 === n.length) return;
            r = n[0]
        }
        var i = this;
        if (i.clk = r, "image" == r.type) if (void 0 !== t.offsetX) i.clk_x = t.offsetX, i.clk_y = t.offsetY; else if ("function" == typeof e.fn.offset) {
            var o = a.offset();
            i.clk_x = t.pageX - o.left, i.clk_y = t.pageY - o.top
        } else i.clk_x = t.pageX - r.offsetLeft, i.clk_y = t.pageY - r.offsetTop;
        setTimeout(function () {
            i.clk = i.clk_x = i.clk_y = null
        }, 100)
    }

    function a() {
        if (e.fn.ajaxSubmit.debug) {
            var t = "[jquery.form] " + Array.prototype.join.call(arguments, "");
            window.console && window.console.log ? window.console.log(t) : window.opera && window.opera.postError && window.opera.postError(t)
        }
    }

    var n = {};
    n.fileapi = void 0 !== e("<input type='file'/>").get(0).files, n.formdata = void 0 !== window.FormData;
    var i = !!e.fn.prop;
    e.fn.attr2 = function () {
        if (!i) return this.attr.apply(this, arguments);
        var e = this.prop.apply(this, arguments);
        return e && e.jquery || "string" == typeof e ? e : this.attr.apply(this, arguments)
    }, e.fn.ajaxSubmit = function (t) {
        function r(r) {
            var a, n, i = e.param(r, t.traditional).split("&"), o = i.length, s = [];
            for (a = 0; o > a; a++) i[a] = i[a].replace(/\+/g, " "), n = i[a].split("="), s.push([decodeURIComponent(n[0]), decodeURIComponent(n[1])]);
            return s
        }

        function o(a) {
            for (var n = new FormData, i = 0; i < a.length; i++) n.append(a[i].name, a[i].value);
            if (t.extraData) {
                var o = r(t.extraData);
                for (i = 0; i < o.length; i++) o[i] && n.append(o[i][0], o[i][1])
            }
            t.data = null;
            var s = e.extend(!0, {}, e.ajaxSettings, t, {
                contentType: !1,
                processData: !1,
                cache: !1,
                type: u || "POST"
            });
            t.uploadProgress && (s.xhr = function () {
                var r = e.ajaxSettings.xhr();
                return r.upload && r.upload.addEventListener("progress", function (e) {
                    var r = 0, a = e.loaded || e.position, n = e.total;
                    e.lengthComputable && (r = Math.ceil(a / n * 100)), t.uploadProgress(e, a, n, r)
                }, !1), r
            }), s.data = null;
            var c = s.beforeSend;
            return s.beforeSend = function (e, r) {
                r.data = t.formData ? t.formData : n, c && c.call(this, e, r)
            }, e.ajax(s)
        }

        function s(r) {
            function n(e) {
                var t = null;
                try {
                    e.contentWindow && (t = e.contentWindow.document)
                } catch (r) {
                    a("cannot get iframe.contentWindow document: " + r)
                }
                if (t) return t;
                try {
                    t = e.contentDocument ? e.contentDocument : e.document
                } catch (r) {
                    a("cannot get iframe.contentDocument: " + r), t = e.document
                }
                return t
            }

            function o() {
                function t() {
                    try {
                        var e = n(g).readyState;
                        a("state = " + e), e && "uninitialized" == e.toLowerCase() && setTimeout(t, 50)
                    } catch (r) {
                        a("Server abort: ", r, " (", r.name, ")"), s(k), j && clearTimeout(j), j = void 0
                    }
                }

                var r = f.attr2("target"), i = f.attr2("action"), o = "multipart/form-data",
                    c = f.attr("enctype") || f.attr("encoding") || o;
                w.setAttribute("target", p), (!u || /post/i.test(u)) && w.setAttribute("method", "POST"), i != m.url && w.setAttribute("action", m.url), m.skipEncodingOverride || u && !/post/i.test(u) || f.attr({
                    encoding: "multipart/form-data",
                    enctype: "multipart/form-data"
                }), m.timeout && (j = setTimeout(function () {
                    T = !0, s(D)
                }, m.timeout));
                var l = [];
                try {
                    if (m.extraData) for (var d in m.extraData) m.extraData.hasOwnProperty(d) && l.push(e.isPlainObject(m.extraData[d]) && m.extraData[d].hasOwnProperty("name") && m.extraData[d].hasOwnProperty("value") ? e('<input type="hidden" name="' + m.extraData[d].name + '">').val(m.extraData[d].value).appendTo(w)[0] : e('<input type="hidden" name="' + d + '">').val(m.extraData[d]).appendTo(w)[0]);
                    m.iframeTarget || v.appendTo("body"), g.attachEvent ? g.attachEvent("onload", s) : g.addEventListener("load", s, !1), setTimeout(t, 15);
                    try {
                        w.submit()
                    } catch (h) {
                        var x = document.createElement("form").submit;
                        x.apply(w)
                    }
                } finally {
                    w.setAttribute("action", i), w.setAttribute("enctype", c), r ? w.setAttribute("target", r) : f.removeAttr("target"), e(l).remove()
                }
            }

            function s(t) {
                if (!x.aborted && !F) {
                    if (M = n(g), M || (a("cannot access response document"), t = k), t === D && x) return x.abort("timeout"), void S.reject(x, "timeout");
                    if (t == k && x) return x.abort("server abort"), void S.reject(x, "error", "server abort");
                    if (M && M.location.href != m.iframeSrc || T) {
                        g.detachEvent ? g.detachEvent("onload", s) : g.removeEventListener("load", s, !1);
                        var r, i = "success";
                        try {
                            if (T) throw"timeout";
                            var o = "xml" == m.dataType || M.XMLDocument || e.isXMLDoc(M);
                            if (a("isXml=" + o), !o && window.opera && (null === M.body || !M.body.innerHTML) && --O) return a("requeing onLoad callback, DOM not available"), void setTimeout(s, 250);
                            var u = M.body ? M.body : M.documentElement;
                            x.responseText = u ? u.innerHTML : null, x.responseXML = M.XMLDocument ? M.XMLDocument : M, o && (m.dataType = "xml"), x.getResponseHeader = function (e) {
                                var t = {"content-type": m.dataType};
                                return t[e.toLowerCase()]
                            }, u && (x.status = Number(u.getAttribute("status")) || x.status, x.statusText = u.getAttribute("statusText") || x.statusText);
                            var c = (m.dataType || "").toLowerCase(), l = /(json|script|text)/.test(c);
                            if (l || m.textarea) {
                                var f = M.getElementsByTagName("textarea")[0];
                                if (f) x.responseText = f.value, x.status = Number(f.getAttribute("status")) || x.status, x.statusText = f.getAttribute("statusText") || x.statusText; else if (l) {
                                    var p = M.getElementsByTagName("pre")[0], h = M.getElementsByTagName("body")[0];
                                    p ? x.responseText = p.textContent ? p.textContent : p.innerText : h && (x.responseText = h.textContent ? h.textContent : h.innerText)
                                }
                            } else "xml" == c && !x.responseXML && x.responseText && (x.responseXML = X(x.responseText));
                            try {
                                E = _(x, c, m)
                            } catch (y) {
                                i = "parsererror", x.error = r = y || i
                            }
                        } catch (y) {
                            a("error caught: ", y), i = "error", x.error = r = y || i
                        }
                        x.aborted && (a("upload aborted"), i = null), x.status && (i = x.status >= 200 && x.status < 300 || 304 === x.status ? "success" : "error"), "success" === i ? (m.success && m.success.call(m.context, E, "success", x), S.resolve(x.responseText, "success", x), d && e.event.trigger("ajaxSuccess", [x, m])) : i && (void 0 === r && (r = x.statusText), m.error && m.error.call(m.context, x, i, r), S.reject(x, "error", r), d && e.event.trigger("ajaxError", [x, m, r])), d && e.event.trigger("ajaxComplete", [x, m]), d && !--e.active && e.event.trigger("ajaxStop"), m.complete && m.complete.call(m.context, x, i), F = !0, m.timeout && clearTimeout(j), setTimeout(function () {
                            m.iframeTarget ? v.attr("src", m.iframeSrc) : v.remove(), x.responseXML = null
                        }, 100)
                    }
                }
            }

            var c, l, m, d, p, v, g, x, y, b, T, j, w = f[0], S = e.Deferred();
            if (S.abort = function (e) {
                x.abort(e)
            }, r) for (l = 0; l < h.length; l++) c = e(h[l]), i ? c.prop("disabled", !1) : c.removeAttr("disabled");
            if (m = e.extend(!0, {}, e.ajaxSettings, t), m.context = m.context || m, p = "jqFormIO" + (new Date).getTime(), m.iframeTarget ? (v = e(m.iframeTarget), b = v.attr2("name"), b ? p = b : v.attr2("name", p)) : (v = e('<iframe name="' + p + '" src="' + m.iframeSrc + '" />'), v.css({
                position: "absolute",
                top: "-1000px",
                left: "-1000px"
            })), g = v[0], x = {
                aborted: 0,
                responseText: null,
                responseXML: null,
                status: 0,
                statusText: "n/a",
                getAllResponseHeaders: function () {
                },
                getResponseHeader: function () {
                },
                setRequestHeader: function () {
                },
                abort: function (t) {
                    var r = "timeout" === t ? "timeout" : "aborted";
                    a("aborting upload... " + r), this.aborted = 1;
                    try {
                        g.contentWindow.document.execCommand && g.contentWindow.document.execCommand("Stop")
                    } catch (n) {
                    }
                    v.attr("src", m.iframeSrc), x.error = r, m.error && m.error.call(m.context, x, r, t), d && e.event.trigger("ajaxError", [x, m, r]), m.complete && m.complete.call(m.context, x, r)
                }
            }, d = m.global, d && 0 === e.active++ && e.event.trigger("ajaxStart"), d && e.event.trigger("ajaxSend", [x, m]), m.beforeSend && m.beforeSend.call(m.context, x, m) === !1) return m.global && e.active--, S.reject(), S;
            if (x.aborted) return S.reject(), S;
            y = w.clk, y && (b = y.name, b && !y.disabled && (m.extraData = m.extraData || {}, m.extraData[b] = y.value, "image" == y.type && (m.extraData[b + ".x"] = w.clk_x, m.extraData[b + ".y"] = w.clk_y)));
            var D = 1, k = 2, A = e("meta[name=csrf-token]").attr("content"),
                L = e("meta[name=csrf-param]").attr("content");
            L && A && (m.extraData = m.extraData || {}, m.extraData[L] = A), m.forceSync ? o() : setTimeout(o, 10);
            var E, M, F, O = 50, X = e.parseXML || function (e, t) {
                return window.ActiveXObject ? (t = new ActiveXObject("Microsoft.XMLDOM"), t.async = "false", t.loadXML(e)) : t = (new DOMParser).parseFromString(e, "text/xml"), t && t.documentElement && "parsererror" != t.documentElement.nodeName ? t : null
            }, C = e.parseJSON || function (e) {
                return window.eval("(" + e + ")")
            }, _ = function (t, r, a) {
                var n = t.getResponseHeader("content-type") || "", i = "xml" === r || !r && n.indexOf("xml") >= 0,
                    o = i ? t.responseXML : t.responseText;
                return i && "parsererror" === o.documentElement.nodeName && e.error && e.error("parsererror"), a && a.dataFilter && (o = a.dataFilter(o, r)), "string" == typeof o && ("json" === r || !r && n.indexOf("json") >= 0 ? o = C(o) : ("script" === r || !r && n.indexOf("javascript") >= 0) && e.globalEval(o)), o
            };
            return S
        }

        if (!this.length) return a("ajaxSubmit: skipping submit process - no element selected"), this;
        var u, c, l, f = this;
        "function" == typeof t ? t = {success: t} : void 0 === t && (t = {}), u = t.type || this.attr2("method"), c = t.url || this.attr2("action"), l = "string" == typeof c ? e.trim(c) : "", l = l || window.location.href || "", l && (l = (l.match(/^([^#]+)/) || [])[1]), t = e.extend(!0, {
            url: l,
            success: e.ajaxSettings.success,
            type: u || e.ajaxSettings.type,
            iframeSrc: /^https/i.test(window.location.href || "") ? "javascript:false" : "about:blank"
        }, t);
        var m = {};
        if (this.trigger("form-pre-serialize", [this, t, m]), m.veto) return a("ajaxSubmit: submit vetoed via form-pre-serialize trigger"), this;
        if (t.beforeSerialize && t.beforeSerialize(this, t) === !1) return a("ajaxSubmit: submit aborted via beforeSerialize callback"), this;
        var d = t.traditional;
        void 0 === d && (d = e.ajaxSettings.traditional);
        var p, h = [], v = this.formToArray(t.semantic, h);
        if (t.data && (t.extraData = t.data, p = e.param(t.data, d)), t.beforeSubmit && t.beforeSubmit(v, this, t) === !1) return a("ajaxSubmit: submit aborted via beforeSubmit callback"), this;
        if (this.trigger("form-submit-validate", [v, this, t, m]), m.veto) return a("ajaxSubmit: submit vetoed via form-submit-validate trigger"), this;
        var g = e.param(v, d);
        p && (g = g ? g + "&" + p : p), "GET" == t.type.toUpperCase() ? (t.url += (t.url.indexOf("?") >= 0 ? "&" : "?") + g, t.data = null) : t.data = g;
        var x = [];
        if (t.resetForm && x.push(function () {
            f.resetForm()
        }), t.clearForm && x.push(function () {
            f.clearForm(t.includeHidden)
        }), !t.dataType && t.target) {
            var y = t.success || function () {
            };
            x.push(function (r) {
                var a = t.replaceTarget ? "replaceWith" : "html";
                e(t.target)[a](r).each(y, arguments)
            })
        } else t.success && x.push(t.success);
        if (t.success = function (e, r, a) {
            for (var n = t.context || this, i = 0, o = x.length; o > i; i++) x[i].apply(n, [e, r, a || f, f])
        }, t.error) {
            var b = t.error;
            t.error = function (e, r, a) {
                var n = t.context || this;
                b.apply(n, [e, r, a, f])
            }
        }
        if (t.complete) {
            var T = t.complete;
            t.complete = function (e, r) {
                var a = t.context || this;
                T.apply(a, [e, r, f])
            }
        }
        var j = e("input[type=file]:enabled", this).filter(function () {
                return "" !== e(this).val()
            }), w = j.length > 0, S = "multipart/form-data", D = f.attr("enctype") == S || f.attr("encoding") == S,
            k = n.fileapi && n.formdata;
        a("fileAPI :" + k);
        var A, L = (w || D) && !k;
        t.iframe !== !1 && (t.iframe || L) ? t.closeKeepAlive ? e.get(t.closeKeepAlive, function () {
            A = s(v)
        }) : A = s(v) : A = (w || D) && k ? o(v) : e.ajax(t), f.removeData("jqxhr").data("jqxhr", A);
        for (var E = 0; E < h.length; E++) h[E] = null;
        return this.trigger("form-submit-notify", [this, t]), this
    }, e.fn.ajaxForm = function (n) {
        if (n = n || {}, n.delegation = n.delegation && e.isFunction(e.fn.on), !n.delegation && 0 === this.length) {
            var i = {s: this.selector, c: this.context};
            return !e.isReady && i.s ? (a("DOM not ready, queuing ajaxForm"), e(function () {
                e(i.s, i.c).ajaxForm(n)
            }), this) : (a("terminating; zero elements found by selector" + (e.isReady ? "" : " (DOM not ready)")), this)
        }
        return n.delegation ? (e(document).off("submit.form-plugin", this.selector, t).off("click.form-plugin", this.selector, r).on("submit.form-plugin", this.selector, n, t).on("click.form-plugin", this.selector, n, r), this) : this.ajaxFormUnbind().bind("submit.form-plugin", n, t).bind("click.form-plugin", n, r)
    }, e.fn.ajaxFormUnbind = function () {
        return this.unbind("submit.form-plugin click.form-plugin")
    }, e.fn.formToArray = function (t, r) {
        var a = [];
        if (0 === this.length) return a;
        var i, o = this[0], s = this.attr("id"), u = t ? o.getElementsByTagName("*") : o.elements;
        if (u && !/MSIE [678]/.test(navigator.userAgent) && (u = e(u).get()), s && (i = e(':input[form="' + s + '"]').get(), i.length && (u = (u || []).concat(i))), !u || !u.length) return a;
        var c, l, f, m, d, p, h;
        for (c = 0, p = u.length; p > c; c++) if (d = u[c], f = d.name, f && !d.disabled) if (t && o.clk && "image" == d.type) o.clk == d && (a.push({
            name: f,
            value: e(d).val(),
            type: d.type
        }), a.push({name: f + ".x", value: o.clk_x}, {
            name: f + ".y",
            value: o.clk_y
        })); else if (m = e.fieldValue(d, !0), m && m.constructor == Array) for (r && r.push(d), l = 0, h = m.length; h > l; l++) a.push({
            name: f,
            value: m[l]
        }); else if (n.fileapi && "file" == d.type) {
            r && r.push(d);
            var v = d.files;
            if (v.length) for (l = 0; l < v.length; l++) a.push({
                name: f,
                value: v[l],
                type: d.type
            }); else a.push({name: f, value: "", type: d.type})
        } else null !== m && "undefined" != typeof m && (r && r.push(d), a.push({
            name: f,
            value: m,
            type: d.type,
            required: d.required
        }));
        if (!t && o.clk) {
            var g = e(o.clk), x = g[0];
            f = x.name, f && !x.disabled && "image" == x.type && (a.push({
                name: f,
                value: g.val()
            }), a.push({name: f + ".x", value: o.clk_x}, {name: f + ".y", value: o.clk_y}))
        }
        return a
    }, e.fn.formSerialize = function (t) {
        return e.param(this.formToArray(t))
    }, e.fn.fieldSerialize = function (t) {
        var r = [];
        return this.each(function () {
            var a = this.name;
            if (a) {
                var n = e.fieldValue(this, t);
                if (n && n.constructor == Array) for (var i = 0, o = n.length; o > i; i++) r.push({
                    name: a,
                    value: n[i]
                }); else null !== n && "undefined" != typeof n && r.push({name: this.name, value: n})
            }
        }), e.param(r)
    }, e.fn.fieldValue = function (t) {
        for (var r = [], a = 0, n = this.length; n > a; a++) {
            var i = this[a], o = e.fieldValue(i, t);
            null === o || "undefined" == typeof o || o.constructor == Array && !o.length || (o.constructor == Array ? e.merge(r, o) : r.push(o))
        }
        return r
    }, e.fieldValue = function (t, r) {
        var a = t.name, n = t.type, i = t.tagName.toLowerCase();
        if (void 0 === r && (r = !0), r && (!a || t.disabled || "reset" == n || "button" == n || ("checkbox" == n || "radio" == n) && !t.checked || ("submit" == n || "image" == n) && t.form && t.form.clk != t || "select" == i && -1 == t.selectedIndex)) return null;
        if ("select" == i) {
            var o = t.selectedIndex;
            if (0 > o) return null;
            for (var s = [], u = t.options, c = "select-one" == n, l = c ? o + 1 : u.length, f = c ? o : 0; l > f; f++) {
                var m = u[f];
                if (m.selected) {
                    var d = m.value;
                    if (d || (d = m.attributes && m.attributes.value && !m.attributes.value.specified ? m.text : m.value), c) return d;
                    s.push(d)
                }
            }
            return s
        }
        return e(t).val()
    }, e.fn.clearForm = function (t) {
        return this.each(function () {
            e("input,select,textarea", this).clearFields(t)
        })
    }, e.fn.clearFields = e.fn.clearInputs = function (t) {
        var r = /^(?:color|date|datetime|email|month|number|password|range|search|tel|text|time|url|week)$/i;
        return this.each(function () {
            var a = this.type, n = this.tagName.toLowerCase();
            r.test(a) || "textarea" == n ? this.value = "" : "checkbox" == a || "radio" == a ? this.checked = !1 : "select" == n ? this.selectedIndex = -1 : "file" == a ? /MSIE/.test(navigator.userAgent) ? e(this).replaceWith(e(this).clone(!0)) : e(this).val("") : t && (t === !0 && /hidden/.test(a) || "string" == typeof t && e(this).is(t)) && (this.value = "")
        })
    }, e.fn.resetForm = function () {
        return this.each(function () {
            ("function" == typeof this.reset || "object" == typeof this.reset && !this.reset.nodeType) && this.reset()
        })
    }, e.fn.enable = function (e) {
        return void 0 === e && (e = !0), this.each(function () {
            this.disabled = !e
        })
    }, e.fn.selected = function (t) {
        return void 0 === t && (t = !0), this.each(function () {
            var r = this.type;
            if ("checkbox" == r || "radio" == r) this.checked = t; else if ("option" == this.tagName.toLowerCase()) {
                var a = e(this).parent("select");
                t && a[0] && "select-one" == a[0].type && a.find("option").selected(!1), this.selected = t
            }
        })
    }, e.fn.ajaxSubmit.debug = !1
});
!function (a) {
    "function" == typeof define && define.amd ? define(["jquery"], a) : a(jQuery)
}(function (a) {
    a.extend(a.fn, {
        validate: function (b) {
            if (!this.length) return void(b && b.debug && window.console && console.warn("Nothing selected, can't validate, returning nothing."));
            var c = a.data(this[0], "validator");
            return c ? c : (this.attr("novalidate", "novalidate"), c = new a.validator(b, this[0]), a.data(this[0], "validator", c), c.settings.onsubmit && (this.validateDelegate(":submit", "click", function (b) {
                c.settings.submitHandler && (c.submitButton = b.target), a(b.target).hasClass("cancel") && (c.cancelSubmit = !0), void 0 !== a(b.target).attr("formnovalidate") && (c.cancelSubmit = !0)
            }), this.submit(function (b) {
                function d() {
                    var d, e;
                    return c.settings.submitHandler ? (c.submitButton && (d = a("<input type='hidden'/>").attr("name", c.submitButton.name).val(a(c.submitButton).val()).appendTo(c.currentForm)), e = c.settings.submitHandler.call(c, c.currentForm, b), c.submitButton && d.remove(), void 0 !== e ? e : !1) : !0
                }

                return c.settings.debug && b.preventDefault(), c.cancelSubmit ? (c.cancelSubmit = !1, d()) : c.form() ? c.pendingRequest ? (c.formSubmitted = !0, !1) : d() : (c.focusInvalid(), !1)
            })), c)
        }, valid: function () {
            var b, c;
            return a(this[0]).is("form") ? b = this.validate().form() : (b = !0, c = a(this[0].form).validate(), this.each(function () {
                b = c.element(this) && b
            })), b
        }, removeAttrs: function (b) {
            var c = {}, d = this;
            return a.each(b.split(/\s/), function (a, b) {
                c[b] = d.attr(b), d.removeAttr(b)
            }), c
        }, rules: function (b, c) {
            var d, e, f, g, h, i, j = this[0];
            if (b) switch (d = a.data(j.form, "validator").settings, e = d.rules, f = a.validator.staticRules(j), b) {
                case"add":
                    a.extend(f, a.validator.normalizeRule(c)), delete f.messages, e[j.name] = f, c.messages && (d.messages[j.name] = a.extend(d.messages[j.name], c.messages));
                    break;
                case"remove":
                    return c ? (i = {}, a.each(c.split(/\s/), function (b, c) {
                        i[c] = f[c], delete f[c], "required" === c && a(j).removeAttr("aria-required")
                    }), i) : (delete e[j.name], f)
            }
            return g = a.validator.normalizeRules(a.extend({}, a.validator.classRules(j), a.validator.attributeRules(j), a.validator.dataRules(j), a.validator.staticRules(j)), j), g.required && (h = g.required, delete g.required, g = a.extend({required: h}, g), a(j).attr("aria-required", "true")), g.remote && (h = g.remote, delete g.remote, g = a.extend(g, {remote: h})), g
        }
    }), a.extend(a.expr[":"], {
        blank: function (b) {
            return !a.trim("" + a(b).val())
        }, filled: function (b) {
            return !!a.trim("" + a(b).val())
        }, unchecked: function (b) {
            return !a(b).prop("checked")
        }
    }), a.validator = function (b, c) {
        this.settings = a.extend(!0, {}, a.validator.defaults, b), this.currentForm = c, this.init()
    }, a.validator.format = function (b, c) {
        return 1 === arguments.length ? function () {
            var c = a.makeArray(arguments);
            return c.unshift(b), a.validator.format.apply(this, c)
        } : (arguments.length > 2 && c.constructor !== Array && (c = a.makeArray(arguments).slice(1)), c.constructor !== Array && (c = [c]), a.each(c, function (a, c) {
            b = b.replace(new RegExp("\\{" + a + "\\}", "g"), function () {
                return c
            })
        }), b)
    }, a.extend(a.validator, {
        defaults: {
            messages: {},
            groups: {},
            rules: {},
            errorClass: "error",
            validClass: "valid",
            errorElement: "label",
            focusCleanup: !1,
            focusInvalid: !0,
            errorContainer: a([]),
            errorLabelContainer: a([]),
            onsubmit: !0,
            ignore: ":hidden",
            ignoreTitle: !1,
            onfocusin: function (a) {
                this.lastActive = a, this.settings.focusCleanup && (this.settings.unhighlight && this.settings.unhighlight.call(this, a, this.settings.errorClass, this.settings.validClass), this.hideThese(this.errorsFor(a)))
            },
            onfocusout: function (a) {
                this.checkable(a) || !(a.name in this.submitted) && this.optional(a) || this.element(a)
            },
            onkeyup: function (a, b) {
                (9 !== b.which || "" !== this.elementValue(a)) && (a.name in this.submitted || a === this.lastElement) && this.element(a)
            },
            onclick: function (a) {
                a.name in this.submitted ? this.element(a) : a.parentNode.name in this.submitted && this.element(a.parentNode)
            },
            highlight: function (b, c, d) {
                "radio" === b.type ? this.findByName(b.name).addClass(c).removeClass(d) : a(b).addClass(c).removeClass(d)
            },
            unhighlight: function (b, c, d) {
                "radio" === b.type ? this.findByName(b.name).removeClass(c).addClass(d) : a(b).removeClass(c).addClass(d)
            }
        },
        setDefaults: function (b) {
            a.extend(a.validator.defaults, b)
        },
        messages: {
            required: "",
            remote: "Please fix this field.",
            email: "Введите корректный email",
            url: "Please enter a valid URL.",
            date: "Please enter a valid date.",
            dateISO: "Please enter a valid date ( ISO ).",
            number: "Please enter a valid number.",
            digits: "Please enter only digits.",
            creditcard: "Please enter a valid credit card number.",
            equalTo: "Please enter the same value again.",
            maxlength: a.validator.format("Please enter no more than {0} characters."),
            minlength: a.validator.format("Please enter at least {0} characters."),
            rangelength: a.validator.format("Please enter a value between {0} and {1} characters long."),
            range: a.validator.format("Please enter a value between {0} and {1}."),
            max: a.validator.format("Please enter a value less than or equal to {0}."),
            min: a.validator.format("Please enter a value greater than or equal to {0}.")
        },
        autoCreateRanges: !1,
        prototype: {
            init: function () {
                function b(b) {
                    var c = a.data(this[0].form, "validator"), d = "on" + b.type.replace(/^validate/, ""),
                        e = c.settings;
                    e[d] && !this.is(e.ignore) && e[d].call(c, this[0], b)
                }

                this.labelContainer = a(this.settings.errorLabelContainer), this.errorContext = this.labelContainer.length && this.labelContainer || a(this.currentForm), this.containers = a(this.settings.errorContainer).add(this.settings.errorLabelContainer), this.submitted = {}, this.valueCache = {}, this.pendingRequest = 0, this.pending = {}, this.invalid = {}, this.reset();
                var c, d = this.groups = {};
                a.each(this.settings.groups, function (b, c) {
                    "string" == typeof c && (c = c.split(/\s/)), a.each(c, function (a, c) {
                        d[c] = b
                    })
                }), c = this.settings.rules, a.each(c, function (b, d) {
                    c[b] = a.validator.normalizeRule(d)
                }), a(this.currentForm).validateDelegate(":text, [type='password'], [type='file'], select, textarea, [type='number'], [type='search'] ,[type='tel'], [type='url'], [type='email'], [type='datetime'], [type='date'], [type='month'], [type='week'], [type='time'], [type='datetime-local'], [type='range'], [type='color'], [type='radio'], [type='checkbox']", "focusin focusout keyup", b).validateDelegate("select, option, [type='radio'], [type='checkbox']", "click", b), this.settings.invalidHandler && a(this.currentForm).bind("invalid-form.validate", this.settings.invalidHandler), a(this.currentForm).find("[required], [data-rule-required], .required").attr("aria-required", "true")
            }, form: function () {
                return this.checkForm(), a.extend(this.submitted, this.errorMap), this.invalid = a.extend({}, this.errorMap), this.valid() || a(this.currentForm).triggerHandler("invalid-form", [this]), this.showErrors(), this.valid()
            }, checkForm: function () {
                this.prepareForm();
                for (var a = 0, b = this.currentElements = this.elements(); b[a]; a++) this.check(b[a]);
                return this.valid()
            }, element: function (b) {
                var c = this.clean(b), d = this.validationTargetFor(c), e = !0;
                return this.lastElement = d, void 0 === d ? delete this.invalid[c.name] : (this.prepareElement(d), this.currentElements = a(d), e = this.check(d) !== !1, e ? delete this.invalid[d.name] : this.invalid[d.name] = !0), a(b).attr("aria-invalid", !e), this.numberOfInvalids() || (this.toHide = this.toHide.add(this.containers)), this.showErrors(), e
            }, showErrors: function (b) {
                if (b) {
                    a.extend(this.errorMap, b), this.errorList = [];
                    for (var c in b) this.errorList.push({message: b[c], element: this.findByName(c)[0]});
                    this.successList = a.grep(this.successList, function (a) {
                        return !(a.name in b)
                    })
                }
                this.settings.showErrors ? this.settings.showErrors.call(this, this.errorMap, this.errorList) : this.defaultShowErrors()
            }, resetForm: function () {
                a.fn.resetForm && a(this.currentForm).resetForm(), this.submitted = {}, this.lastElement = null, this.prepareForm(), this.hideErrors(), this.elements().removeClass(this.settings.errorClass).removeData("previousValue").removeAttr("aria-invalid")
            }, numberOfInvalids: function () {
                return this.objectLength(this.invalid)
            }, objectLength: function (a) {
                var b, c = 0;
                for (b in a) c++;
                return c
            }, hideErrors: function () {
                this.hideThese(this.toHide)
            }, hideThese: function (a) {
                a.not(this.containers).text(""), this.addWrapper(a).hide()
            }, valid: function () {
                return 0 === this.size()
            }, size: function () {
                return this.errorList.length
            }, focusInvalid: function () {
                if (this.settings.focusInvalid) try {
                    a(this.findLastActive() || this.errorList.length && this.errorList[0].element || []).filter(":visible").focus().trigger("focusin")
                } catch (b) {
                }
            }, findLastActive: function () {
                var b = this.lastActive;
                return b && 1 === a.grep(this.errorList, function (a) {
                    return a.element.name === b.name
                }).length && b
            }, elements: function () {
                var b = this, c = {};
                return a(this.currentForm).find("input, select, textarea").not(":submit, :reset, :image, [disabled], [readonly]").not(this.settings.ignore).filter(function () {
                    return !this.name && b.settings.debug && window.console && console.error("%o has no name assigned", this), this.name in c || !b.objectLength(a(this).rules()) ? !1 : (c[this.name] = !0, !0)
                })
            }, clean: function (b) {
                return a(b)[0]
            }, errors: function () {
                var b = this.settings.errorClass.split(" ").join(".");
                return a(this.settings.errorElement + "." + b, this.errorContext)
            }, reset: function () {
                this.successList = [], this.errorList = [], this.errorMap = {}, this.toShow = a([]), this.toHide = a([]), this.currentElements = a([])
            }, prepareForm: function () {
                this.reset(), this.toHide = this.errors().add(this.containers)
            }, prepareElement: function (a) {
                this.reset(), this.toHide = this.errorsFor(a)
            }, elementValue: function (b) {
                var c, d = a(b), e = b.type;
                return "radio" === e || "checkbox" === e ? a("input[name='" + b.name + "']:checked").val() : "number" === e && "undefined" != typeof b.validity ? b.validity.badInput ? !1 : d.val() : (c = d.val(), "string" == typeof c ? c.replace(/\r/g, "") : c)
            }, check: function (b) {
                b = this.validationTargetFor(this.clean(b));
                var c, d, e, f = a(b).rules(), g = a.map(f, function (a, b) {
                    return b
                }).length, h = !1, i = this.elementValue(b);
                for (d in f) {
                    e = {method: d, parameters: f[d]};
                    try {
                        if (c = a.validator.methods[d].call(this, i, b, e.parameters), "dependency-mismatch" === c && 1 === g) {
                            h = !0;
                            continue
                        }
                        if (h = !1, "pending" === c) return void(this.toHide = this.toHide.not(this.errorsFor(b)));
                        if (!c) return this.formatAndAdd(b, e), !1
                    } catch (j) {
                        throw this.settings.debug && window.console && console.log("Exception occurred when checking element " + b.id + ", check the '" + e.method + "' method.", j), j
                    }
                }
                if (!h) return this.objectLength(f) && this.successList.push(b), !0
            }, customDataMessage: function (b, c) {
                return a(b).data("msg" + c.charAt(0).toUpperCase() + c.substring(1).toLowerCase()) || a(b).data("msg")
            }, customMessage: function (a, b) {
                var c = this.settings.messages[a];
                return c && (c.constructor === String ? c : c[b])
            }, findDefined: function () {
                for (var a = 0; a < arguments.length; a++) if (void 0 !== arguments[a]) return arguments[a];
                return void 0
            }, defaultMessage: function (b, c) {
                return this.findDefined(this.customMessage(b.name, c), this.customDataMessage(b, c), !this.settings.ignoreTitle && b.title || void 0, a.validator.messages[c], "<strong>Warning: No message defined for " + b.name + "</strong>")
            }, formatAndAdd: function (b, c) {
                var d = this.defaultMessage(b, c.method), e = /\$?\{(\d+)\}/g;
                "function" == typeof d ? d = d.call(this, c.parameters, b) : e.test(d) && (d = a.validator.format(d.replace(e, "{$1}"), c.parameters)), this.errorList.push({
                    message: d,
                    element: b,
                    method: c.method
                }), this.errorMap[b.name] = d, this.submitted[b.name] = d
            }, addWrapper: function (a) {
                return this.settings.wrapper && (a = a.add(a.parent(this.settings.wrapper))), a
            }, defaultShowErrors: function () {
                var a, b, c;
                for (a = 0; this.errorList[a]; a++) c = this.errorList[a], this.settings.highlight && this.settings.highlight.call(this, c.element, this.settings.errorClass, this.settings.validClass), this.showLabel(c.element, c.message);
                if (this.errorList.length && (this.toShow = this.toShow.add(this.containers)), this.settings.success) for (a = 0; this.successList[a]; a++) this.showLabel(this.successList[a]);
                if (this.settings.unhighlight) for (a = 0, b = this.validElements(); b[a]; a++) this.settings.unhighlight.call(this, b[a], this.settings.errorClass, this.settings.validClass);
                this.toHide = this.toHide.not(this.toShow), this.hideErrors(), this.addWrapper(this.toShow).show()
            }, validElements: function () {
                return this.currentElements.not(this.invalidElements())
            }, invalidElements: function () {
                return a(this.errorList).map(function () {
                    return this.element
                })
            }, showLabel: function (b, c) {
                var d, e, f, g = this.errorsFor(b), h = this.idOrName(b), i = a(b).attr("aria-describedby");
                g.length ? (g.removeClass(this.settings.validClass).addClass(this.settings.errorClass), g.html(c)) : (g = a("<" + this.settings.errorElement + ">").attr("id", h + "-error").addClass(this.settings.errorClass).html(c || ""), d = g, this.settings.wrapper && (d = g.hide().show().wrap("<" + this.settings.wrapper + "/>").parent()), this.labelContainer.length ? this.labelContainer.append(d) : this.settings.errorPlacement ? this.settings.errorPlacement(d, a(b)) : d.insertAfter(b), g.is("label") ? g.attr("for", h) : 0 === g.parents("label[for='" + h + "']").length && (f = g.attr("id").replace(/(:|\.|\[|\])/g, "\\$1"), i ? i.match(new RegExp("\\b" + f + "\\b")) || (i += " " + f) : i = f, a(b).attr("aria-describedby", i), e = this.groups[b.name], e && a.each(this.groups, function (b, c) {
                    c === e && a("[name='" + b + "']", this.currentForm).attr("aria-describedby", g.attr("id"))
                }))), !c && this.settings.success && (g.text(""), "string" == typeof this.settings.success ? g.addClass(this.settings.success) : this.settings.success(g, b)), this.toShow = this.toShow.add(g)
            }, errorsFor: function (b) {
                var c = this.idOrName(b), d = a(b).attr("aria-describedby"),
                    e = "label[for='" + c + "'], label[for='" + c + "'] *";
                return d && (e = e + ", #" + d.replace(/\s+/g, ", #")), this.errors().filter(e)
            }, idOrName: function (a) {
                return this.groups[a.name] || (this.checkable(a) ? a.name : a.id || a.name)
            }, validationTargetFor: function (b) {
                return this.checkable(b) && (b = this.findByName(b.name)), a(b).not(this.settings.ignore)[0]
            }, checkable: function (a) {
                return /radio|checkbox/i.test(a.type)
            }, findByName: function (b) {
                return a(this.currentForm).find("[name='" + b + "']")
            }, getLength: function (b, c) {
                switch (c.nodeName.toLowerCase()) {
                    case"select":
                        return a("option:selected", c).length;
                    case"input":
                        if (this.checkable(c)) return this.findByName(c.name).filter(":checked").length
                }
                return b.length
            }, depend: function (a, b) {
                return this.dependTypes[typeof a] ? this.dependTypes[typeof a](a, b) : !0
            }, dependTypes: {
                "boolean": function (a) {
                    return a
                }, string: function (b, c) {
                    return !!a(b, c.form).length
                }, "function": function (a, b) {
                    return a(b)
                }
            }, optional: function (b) {
                var c = this.elementValue(b);
                return !a.validator.methods.required.call(this, c, b) && "dependency-mismatch"
            }, startRequest: function (a) {
                this.pending[a.name] || (this.pendingRequest++, this.pending[a.name] = !0)
            }, stopRequest: function (b, c) {
                this.pendingRequest--, this.pendingRequest < 0 && (this.pendingRequest = 0), delete this.pending[b.name], c && 0 === this.pendingRequest && this.formSubmitted && this.form() ? (a(this.currentForm).submit(), this.formSubmitted = !1) : !c && 0 === this.pendingRequest && this.formSubmitted && (a(this.currentForm).triggerHandler("invalid-form", [this]), this.formSubmitted = !1)
            }, previousValue: function (b) {
                return a.data(b, "previousValue") || a.data(b, "previousValue", {
                    old: null,
                    valid: !0,
                    message: this.defaultMessage(b, "remote")
                })
            }
        },
        classRuleSettings: {
            required: {required: !0},
            email: {email: !0},
            url: {url: !0},
            date: {date: !0},
            dateISO: {dateISO: !0},
            number: {number: !0},
            digits: {digits: !0},
            creditcard: {creditcard: !0}
        },
        addClassRules: function (b, c) {
            b.constructor === String ? this.classRuleSettings[b] = c : a.extend(this.classRuleSettings, b)
        },
        classRules: function (b) {
            var c = {}, d = a(b).attr("class");
            return d && a.each(d.split(" "), function () {
                this in a.validator.classRuleSettings && a.extend(c, a.validator.classRuleSettings[this])
            }), c
        },
        attributeRules: function (b) {
            var c, d, e = {}, f = a(b), g = b.getAttribute("type");
            for (c in a.validator.methods) "required" === c ? (d = b.getAttribute(c), "" === d && (d = !0), d = !!d) : d = f.attr(c), /min|max/.test(c) && (null === g || /number|range|text/.test(g)) && (d = Number(d)), d || 0 === d ? e[c] = d : g === c && "range" !== g && (e[c] = !0);
            return e.maxlength && /-1|2147483647|524288/.test(e.maxlength) && delete e.maxlength, e
        },
        dataRules: function (b) {
            var c, d, e = {}, f = a(b);
            for (c in a.validator.methods) d = f.data("rule" + c.charAt(0).toUpperCase() + c.substring(1).toLowerCase()), void 0 !== d && (e[c] = d);
            return e
        },
        staticRules: function (b) {
            var c = {}, d = a.data(b.form, "validator");
            return d.settings.rules && (c = a.validator.normalizeRule(d.settings.rules[b.name]) || {}), c
        },
        normalizeRules: function (b, c) {
            return a.each(b, function (d, e) {
                if (e === !1) return void delete b[d];
                if (e.param || e.depends) {
                    var f = !0;
                    switch (typeof e.depends) {
                        case"string":
                            f = !!a(e.depends, c.form).length;
                            break;
                        case"function":
                            f = e.depends.call(c, c)
                    }
                    f ? b[d] = void 0 !== e.param ? e.param : !0 : delete b[d]
                }
            }), a.each(b, function (d, e) {
                b[d] = a.isFunction(e) ? e(c) : e
            }), a.each(["minlength", "maxlength"], function () {
                b[this] && (b[this] = Number(b[this]))
            }), a.each(["rangelength", "range"], function () {
                var c;
                b[this] && (a.isArray(b[this]) ? b[this] = [Number(b[this][0]), Number(b[this][1])] : "string" == typeof b[this] && (c = b[this].replace(/[\[\]]/g, "").split(/[\s,]+/), b[this] = [Number(c[0]), Number(c[1])]))
            }), a.validator.autoCreateRanges && (null != b.min && null != b.max && (b.range = [b.min, b.max], delete b.min, delete b.max), null != b.minlength && null != b.maxlength && (b.rangelength = [b.minlength, b.maxlength], delete b.minlength, delete b.maxlength)), b
        },
        normalizeRule: function (b) {
            if ("string" == typeof b) {
                var c = {};
                a.each(b.split(/\s/), function () {
                    c[this] = !0
                }), b = c
            }
            return b
        },
        addMethod: function (b, c, d) {
            a.validator.methods[b] = c, a.validator.messages[b] = void 0 !== d ? d : a.validator.messages[b], c.length < 3 && a.validator.addClassRules(b, a.validator.normalizeRule(b))
        },
        methods: {
            required: function (b, c, d) {
                if (!this.depend(d, c)) return "dependency-mismatch";
                if ("select" === c.nodeName.toLowerCase()) {
                    var e = a(c).val();
                    return e && e.length > 0
                }
                return this.checkable(c) ? this.getLength(b, c) > 0 : a.trim(b).length > 0
            }, email: function (a, b) {
                return this.optional(b) || /^[a-zA-Z0-9.!#$%&'*+\/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$/.test(a)
            }, url: function (a, b) {
                return this.optional(b) || /^(https?|s?ftp):\/\/(((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:)*@)?(((\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5])\.(\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5])\.(\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5])\.(\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5]))|((([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.)+(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.?)(:\d*)?)(\/((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)+(\/(([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)*)*)?)?(\?((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)|[\uE000-\uF8FF]|\/|\?)*)?(#((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)|\/|\?)*)?$/i.test(a)
            }, date: function (a, b) {
                return this.optional(b) || !/Invalid|NaN/.test(new Date(a).toString())
            }, dateISO: function (a, b) {
                return this.optional(b) || /^\d{4}[\/\-](0?[1-9]|1[012])[\/\-](0?[1-9]|[12][0-9]|3[01])$/.test(a)
            }, number: function (a, b) {
                return this.optional(b) || /^-?(?:\d+|\d{1,3}(?:,\d{3})+)?(?:\.\d+)?$/.test(a)
            }, digits: function (a, b) {
                return this.optional(b) || /^\d+$/.test(a)
            }, creditcard: function (a, b) {
                if (this.optional(b)) return "dependency-mismatch";
                if (/[^0-9 \-]+/.test(a)) return !1;
                var c, d, e = 0, f = 0, g = !1;
                if (a = a.replace(/\D/g, ""), a.length < 13 || a.length > 19) return !1;
                for (c = a.length - 1; c >= 0; c--) d = a.charAt(c), f = parseInt(d, 10), g && (f *= 2) > 9 && (f -= 9), e += f, g = !g;
                return e % 10 === 0
            }, minlength: function (b, c, d) {
                var e = a.isArray(b) ? b.length : this.getLength(b, c);
                return this.optional(c) || e >= d
            }, maxlength: function (b, c, d) {
                var e = a.isArray(b) ? b.length : this.getLength(b, c);
                return this.optional(c) || d >= e
            }, rangelength: function (b, c, d) {
                var e = a.isArray(b) ? b.length : this.getLength(b, c);
                return this.optional(c) || e >= d[0] && e <= d[1]
            }, min: function (a, b, c) {
                return this.optional(b) || a >= c
            }, max: function (a, b, c) {
                return this.optional(b) || c >= a
            }, range: function (a, b, c) {
                return this.optional(b) || a >= c[0] && a <= c[1]
            }, equalTo: function (b, c, d) {
                var e = a(d);
                return this.settings.onfocusout && e.unbind(".validate-equalTo").bind("blur.validate-equalTo", function () {
                    a(c).valid()
                }), b === e.val()
            }, remote: function (b, c, d) {
                if (this.optional(c)) return "dependency-mismatch";
                var e, f, g = this.previousValue(c);
                return this.settings.messages[c.name] || (this.settings.messages[c.name] = {}), g.originalMessage = this.settings.messages[c.name].remote, this.settings.messages[c.name].remote = g.message, d = "string" == typeof d && {url: d} || d, g.old === b ? g.valid : (g.old = b, e = this, this.startRequest(c), f = {}, f[c.name] = b, a.ajax(a.extend(!0, {
                    url: d,
                    mode: "abort",
                    port: "validate" + c.name,
                    dataType: "json",
                    data: f,
                    context: e.currentForm,
                    success: function (d) {
                        var f, h, i, j = d === !0 || "true" === d;
                        e.settings.messages[c.name].remote = g.originalMessage, j ? (i = e.formSubmitted, e.prepareElement(c), e.formSubmitted = i, e.successList.push(c), delete e.invalid[c.name], e.showErrors()) : (f = {}, h = d || e.defaultMessage(c, "remote"), f[c.name] = g.message = a.isFunction(h) ? h(b) : h, e.invalid[c.name] = !0, e.showErrors(f)), g.valid = j, e.stopRequest(c, j)
                    }
                }, d)), "pending")
            }
        }
    }), a.format = function () {
        throw"$.format has been deprecated. Please use $.validator.format instead."
    };
    var b, c = {};
    a.ajaxPrefilter ? a.ajaxPrefilter(function (a, b, d) {
        var e = a.port;
        "abort" === a.mode && (c[e] && c[e].abort(), c[e] = d)
    }) : (b = a.ajax, a.ajax = function (d) {
        var e = ("mode" in d ? d : a.ajaxSettings).mode, f = ("port" in d ? d : a.ajaxSettings).port;
        return "abort" === e ? (c[f] && c[f].abort(), c[f] = b.apply(this, arguments), c[f]) : b.apply(this, arguments)
    }), a.extend(a.fn, {
        validateDelegate: function (b, c, d) {
            return this.bind(c, function (c) {
                var e = a(c.target);
                return e.is(b) ? d.apply(e, arguments) : void 0
            })
        }
    })
});
!function (a) {
    "function" == typeof define && define.amd ? define(["jquery"], a) : a("object" == typeof exports ? require("jquery") : jQuery)
}(function (a) {
    var b, c = navigator.userAgent, d = /iphone/i.test(c), e = /chrome/i.test(c), f = /android/i.test(c);
    a.mask = {
        definitions: {9: "[0-9]", a: "[A-Za-z]", "*": "[A-Za-z0-9]"},
        autoclear: !0,
        dataName: "rawMaskFn",
        placeholder: "_"
    }, a.fn.extend({
        caret: function (a, b) {
            var c;
            if (0 !== this.length && !this.is(":hidden")) return "number" == typeof a ? (b = "number" == typeof b ? b : a, this.each(function () {
                this.setSelectionRange ? this.setSelectionRange(a, b) : this.createTextRange && (c = this.createTextRange(), c.collapse(!0), c.moveEnd("character", b), c.moveStart("character", a), c.select())
            })) : (this[0].setSelectionRange ? (a = this[0].selectionStart, b = this[0].selectionEnd) : document.selection && document.selection.createRange && (c = document.selection.createRange(), a = 0 - c.duplicate().moveStart("character", -1e5), b = a + c.text.length), {
                begin: a,
                end: b
            })
        }, unmask: function () {
            return this.trigger("unmask")
        },
        mask: function (c, g) {
            var h, i, j, k, l, m, n, o;
            if (!c && this.length > 0) {
                h = a(this[0]);
                var p = h.data(a.mask.dataName);
                return p ? p() : void 0
            }
            return g = a.extend({
                autoclear: a.mask.autoclear,
                placeholder: a.mask.placeholder,
                completed: null
            }, g), i = a.mask.definitions, j = [], k = n = c.length, l = null, a.each(c.split(""), function (a, b) {
                "?" == b ? (n--, k = a) : i[b] ? (j.push(new RegExp(i[b])), null === l && (l = j.length - 1), k > a && (m = j.length - 1)) : j.push(null)
            }), this.trigger("unmask").each(function () {
                function h() {
                    if (g.completed) {
                        for (var a = l; m >= a; a++) if (j[a] && C[a] === p(a)) return;
                        g.completed.call(B)
                    }
                }

                function p(a) {
                    return g.placeholder.charAt(a < g.placeholder.length ? a : 0)
                }

                function q(a) {
                    for (; ++a < n && !j[a];) ;
                    return a
                }

                function r(a) {
                    for (; --a >= 0 && !j[a];) ;
                    return a
                }

                function s(a, b) {
                    var c, d;
                    if (!(0 > a)) {
                        for (c = a, d = q(b); n > c; c++) if (j[c]) {
                            if (!(n > d && j[c].test(C[d]))) break;
                            C[c] = C[d], C[d] = p(d), d = q(d)
                        }
                        z(), B.caret(Math.max(l, a))
                    }
                }

                function t(a) {
                    var b, c, d, e;
                    for (b = a, c = p(a); n > b; b++) if (j[b]) {
                        if (d = q(b), e = C[b], C[b] = c, !(n > d && j[d].test(e))) break;
                        c = e
                    }
                }

                function u() {
                    var a = B.val(), b = B.caret();
                    if (a.length < o.length) {
                        for (A(!0); b.begin > 0 && !j[b.begin - 1];) b.begin--;
                        if (0 === b.begin) for (; b.begin < l && !j[b.begin];) b.begin++;
                        B.caret(b.begin, b.begin)
                    } else {
                        for (A(!0); b.begin < n && !j[b.begin];) b.begin++;
                        B.caret(b.begin, b.begin)
                    }
                    h()
                }

                function v() {
                    A(), B.val() != E && B.change()
                }

                function w(a) {
                    if (!B.prop("readonly")) {
                        var b, c, e, f = a.which || a.keyCode;
                        o = B.val(), 8 === f || 46 === f || d && 127 === f ? (b = B.caret(), c = b.begin, e = b.end, e - c === 0 && (c = 46 !== f ? r(c) : e = q(c - 1), e = 46 === f ? q(e) : e), y(c, e), s(c, e - 1), a.preventDefault()) : 13 === f ? v.call(this, a) : 27 === f && (B.val(E), B.caret(0, A()), a.preventDefault())
                    }
                }

                function x(b) {
                    if (!B.prop("readonly")) {
                        var c, d, e, g = b.which || b.keyCode, i = B.caret();
                        if (!(b.ctrlKey || b.altKey || b.metaKey || 32 > g) && g && 13 !== g) {
                            if (i.end - i.begin !== 0 && (y(i.begin, i.end), s(i.begin, i.end - 1)), c = q(i.begin - 1), n > c && (d = String.fromCharCode(g), j[c].test(d))) {
                                if (t(c), C[c] = d, z(), e = q(c), f) {
                                    var k = function () {
                                        a.proxy(a.fn.caret, B, e)()
                                    };
                                    setTimeout(k, 0)
                                } else B.caret(e);
                                i.begin <= m && h()
                            }
                            b.preventDefault()
                        }
                    }
                }

                function y(a, b) {
                    var c;
                    for (c = a; b > c && n > c; c++) j[c] && (C[c] = p(c))
                }

                function z() {
                    B.val(C.join(""))
                }

                function A(a) {
                    var b, c, d, e = B.val(), f = -1;
                    for (b = 0, d = 0; n > b; b++) if (j[b]) {
                        for (C[b] = p(b); d++ < e.length;) if (c = e.charAt(d - 1), j[b].test(c)) {
                            C[b] = c, f = b;
                            break
                        }
                        if (d > e.length) {
                            y(b + 1, n);
                            break
                        }
                    } else C[b] === e.charAt(d) && d++, k > b && (f = b);
                    return a ? z() : k > f + 1 ? g.autoclear || C.join("") === D ? (B.val() && B.val(""), y(0, n)) : z() : (z(), B.val(B.val().substring(0, f + 1))), k ? b : l
                }

                var B = a(this), C = a.map(c.split(""), function (a, b) {
                    return "?" != a ? i[a] ? p(b) : a : void 0
                }), D = C.join(""), E = B.val();
                B.data(a.mask.dataName, function () {
                    return a.map(C, function (a, b) {
                        return j[b] && a != p(b) ? a : null
                    }).join("")
                }), B.one("unmask", function () {
                    B.off(".mask").removeData(a.mask.dataName)
                }).on("focus.mask", function () {
                    if (!B.prop("readonly")) {
                        clearTimeout(b);
                        var a;
                        E = B.val(), a = A(), b = setTimeout(function () {
                            z(), a == c.replace("?", "").length ? B.caret(0, a) : B.caret(a)
                        }, 10)
                    }
                }).on("blur.mask", v).on("keydown.mask", w).on("keypress.mask", x).on("input.mask paste.mask", function () {
                    B.prop("readonly") || setTimeout(function () {
                        var a = A(!0);
                        B.caret(a), h()
                    }, 0)
                }), e && f && B.off("input.mask").on("input.mask", u), A()
            })
        }
    })
});
(function ($) {
    $.prettyLoader = {version: '1.0.1'};
    $.prettyLoader = function (settings) {
        settings = jQuery.extend({
            animation_speed: 'fast',
            bind_to_ajax: true,
            delay: false,
            loader: '/assets/images/prettyLoader/ajax-loader.gif',
            offset_top: 13,
            offset_left: 10
        }, settings);
        scrollPos = _getScroll();
        imgLoader = new Image();
        imgLoader.onerror = function () {
            //alert('Preloader image cannot be loaded. Make sure the path is correct in the settings and that the image is reachable.');
        };
        imgLoader.src = settings.loader;
        if (settings.bind_to_ajax) jQuery(document).ajaxStart(function () {
            $.prettyLoader.show()
        }).ajaxStop(function () {
            $.prettyLoader.hide()
        });
        $.prettyLoader.positionLoader = function (e) {
            e = e ? e : window.event;
            cur_x = (e.clientX) ? e.clientX : cur_x;
            cur_y = (e.clientY) ? e.clientY : cur_y;
            left_pos = cur_x + settings.offset_left + scrollPos['scrollLeft'];
            top_pos = cur_y + settings.offset_top + scrollPos['scrollTop'];
            $('.prettyLoader').css({'top': top_pos, 'left': left_pos});
        }
        $.prettyLoader.show = function (delay) {
            if ($('.prettyLoader').size() > 0) return;
            scrollPos = _getScroll();
            $('<div></div>').addClass('prettyLoader').addClass('prettyLoader_' + settings.theme).appendTo('body').hide();
            if ($.browser.msie && $.browser.version == 6) $('.prettyLoader').addClass('pl_ie6');
            $('<img />').attr('src', settings.loader).appendTo('.prettyLoader');
            $('.prettyLoader').fadeIn(settings.animation_speed);
            $(document).bind('click', $.prettyLoader.positionLoader);
            $(document).bind('mousemove', $.prettyLoader.positionLoader);
            $(window).scroll(function () {
                scrollPos = _getScroll();
                $(document).triggerHandler('mousemove');
            });
            delay = (delay) ? delay : settings.delay;
            if (delay) {
                setTimeout(function () {
                    $.prettyLoader.hide()
                }, delay);
            }
        };
        $.prettyLoader.hide = function () {
            $(document).unbind('click', $.prettyLoader.positionLoader);
            $(document).unbind('mousemove', $.prettyLoader.positionLoader);
            $(window).unbind('scroll');
            $('.prettyLoader').fadeOut(settings.animation_speed, function () {
                $(this).remove();
            });
        };

        function _getScroll() {
            if (self.pageYOffset) {
                return {scrollTop: self.pageYOffset, scrollLeft: self.pageXOffset};
            } else if (document.documentElement && document.documentElement.scrollTop) {
                return {scrollTop: document.documentElement.scrollTop, scrollLeft: document.documentElement.scrollLeft};
            } else if (document.body) {
                return {scrollTop: document.body.scrollTop, scrollLeft: document.body.scrollLeft};
            }
            ;
        };
        return this;
    };
})(jQuery);

/*scripts.js*/
var min = false;
$(document).scroll(function () {
    if (scroll == true) {
        if ($(document).scrollTop() > 30 && !min) {
            $(".basket-header").css('position', 'fixed');
            $(".basket-header").css('top', '0');
            $(".basket-header").css('width', '100%');
            $(".basket-header").css('border-bottom', '1px solid #c5d4e4');
            $(".basket-header").css('z-index', '99999');
            min = true;
        }
        if ($(document).scrollTop() <= 30 && min) {
            $(".basket-header").css('position', 'relative');
            $(".basket-header").css('border-bottom', 'none');
            min = false;
        }
    }
});
$('form').each(function () {
    $(this).validate({
        errorElement: "div",
        highlight: function (element, errorClass, validClass) {
            $(element).parent('.form-group').addClass('has-error');
        },
        unhighlight: function (element, errorClass, validClass) {
            $(element).parent('.form-group').removeClass('has-error');
        },
        rules: {
            password: {required: true, minlength: 6},
            password_confirm: {required: true, equalTo: "#password", minlength: 6},
            specifiedpassword: {minlength: 6},
            confirmpassword: {equalTo: "#inputNewPassword", minlength: 6}
        },
        messages: {
            password: {minlength: "Пароль должен содержать не меньше 6 символов"},
            password_confirm: {
                minlength: "Пароль должен содержать не меньше 6 символов",
                equalTo: "Пароли не совпадают"
            },
            specifiedpassword: {minlength: "Пароль должен содержать не меньше 6 символов"},
            confirmpassword: {minlength: "Пароль должен содержать не меньше 6 символов", equalTo: "Пароли не совпадают"}
        }
    });
});


$("input[name='username']").mask("+389999999999");
$("input[name='phone']").mask("+389999999999");

//$("input[name='username']").inputmask("+389999999999");
//$("input[name='phone']").inputmask("+389999999999");

$(function () {
    'use strict';
    if ($('#mse2_selected span').size() > 1) $('#mse2_selected').removeClass('hidden');
    else
        $('#mse2_selected').addClass('hidden');
    if ($('#petlist form').size() == 0) $('#petlist .info-msg').show(); else
        $('#petlist .info-msg').hide();
});
vendor = document.getElementById('mse2_ms|vendor');
$(vendor).children('.checkbox:gt(10)').addClass('hidden');
$(function () {
    'use strict';
    $('[data-toggle="popover"]').popover({
        html: true, content: function () {
            var content = $(this).attr('data-popover-content');
            return $(content).children('.popover-body').html();
        }, title: function () {
            var title = $(this).attr('data-popover-content');
            return $(title).children('.popover-heading').html();
        }
    });
});
$(document).on('click', 'a.upload', function (e) {
    $(this).next('input[type=file]').click();
    return false;
});
$(document).on('click', 'div.upload div.file-upload-btn', function (e) {
    $(this).next('input[type=file]').click();
    return false;
});
$(document).on('change', '.pet_type', function (e) {
    var type_id = this.value;
    var petData = {pet_type: type_id};
    var $this = $(this);
    $.ajax({
        type: "GET", url: '/assets/components/pets/getpetdata.php', data: petData, success: function (data) {
            data = $.parseJSON(data);
            if (data.success === true) {
                response = data.data;
                another = $(response).filter(function () {
                    return $(this).html() == "Другая";
                }).val();
                $pet_breed = $this.closest('form').find('.pet_breed');
                $pet_breed.html(data.data);
                $pet_breed.children('option:first').attr('selected', 'selected');
                var selected_text = $pet_breed.children('option:first').text();
                $pet_breed.next('.custom-select').find('span span').text(selected_text);
                $pet_breed.find('option[value="' + another + '"]').remove();
                $pet_breed.append($(response).filter(function () {
                    return $(this).html() == "Другая";
                }));
            } else if (data.success === false) {
            }
        }
    });
});
$('#shownextcomment').on('click', function (e) {
    var Data = {id: $(this).data('id'), thread: '463', action: 'comment/getnext'};
    $.ajax({
        type: "POST", url: '/assets/components/tickets/action.php', data: Data, success: function (data) {
            data = $.parseJSON(data);
            if (data.success === true) {

                dataLayer.push({
                    'event': 'event-GA',
                    'eventCategory': 'product',
                    'eventAction': 'buy-preview'
                });
                $('.callback-single .user-photo img').attr('src', data.data.avatar);
                $('.callback-popup .greener b').html(data.data.fullname);
                $('.callback-popup .small.grey').html(data.data.date_ago);
                $('.callback-popup p.textcom').html(data.data.text);
                $('#shownextcomment').data('id', data.data.id);
            } else if (data.success === false) {
                alert(data.message);
            }
        }
    });
});
$('body').on('click', function (e) {
    'use strict';
    $('[data-toggle="popover"]').each(function () {
        if (!$(this).is(e.target) && $(this).has(e.target).length === 0 && $('.popover').has(e.target).length === 0) {
            $(this).popover('hide');
        }
    });
});
$('body').on('click', '#showmore', function (e) {
    'use strict';
    var next_page = $('#mse2_pagination li.active').next().children('a');
    var tmp = $(next_page).prop('href').match(/page[=|\/](\d+)/);
    var page = tmp && tmp[1] ? Number(tmp[1]) : 1;
    var results = $('#mse2_results');
    var pagination = $('#mse2_pagination');
    //var params=mSearch2.Hash.get();
    var params = mSearch2.getFilters();
    params['action'] = 'filter';
    params['pageId'] = mse2Config.pageId;
    params['key'] = mse2Config.key;
    if (mse2Config[mse2Config.queryVar] != '') {
        params[mse2Config.queryVar] = mse2Config[mse2Config.queryVar];
    }
    if (mse2Config[mse2Config.parentsVar] != '') {
        params[mse2Config.parentsVar] = mse2Config[mse2Config.parentsVar];
    }
    if (mse2Config.sort != '') {
        params.sort = mse2Config.sort;
    }
    //if (mse2Config.sort != '') {params.sort = mse2Config.sort;}
    //alert(mse2Config.sort);
    if (mse2Config.sort == '') params.sort = 'hits';
    else params.sort = mse2Config.sort;
    if (mse2Config.sortdir == undefined) {
        params.sortdir = 'desc';
    } else {
        params.sortdir = mse2Config.sortdir;
    }

    params['page'] = page;
    vendor = document.getElementById('mse2_ms|vendor');
    if ($(vendor).find('.checkbox input:checked').val() != 'undefined') {
        params['ms|vendor'] = $(vendor).find('.checkbox input:checked').val();
    }
    mSearch2.beforeLoad();
    $.ajax({
        type: "GET", url: '/assets/components/msearch2/action.php', data: params, success: function (data) {
            mSearch2.afterLoad();
            data = $.parseJSON(data);
            if (data.success === true) {
                results.append(data.data.results);
                pagination.html(data.data.pagination);
            } else if (data.success === false) {
                results.html(data.message);
            }
            if (parseInt(data.data.ostatok) < 0) {
                $('#showmore').hide();
            } else if (data.data.ostatok < 24) {
                var ost = parseInt(data.data.ostatok);
                $('#showmore .count').html(ost);
            } else {
                $('#showmore .count').html(24);
                $('#showmore').show();
            }
            mSearch2.Hash.set(params);
        }
    });
});
$(function () {
    'use strict';
    $.prettyLoader();
});
$(document).ajaxStart(function () {
    $.prettyLoader.show();
}).ajaxStop(function () {
    $.prettyLoader.hide();
});
$(function () {
    'use strict';
    $('.open-comment').click(function (e) {
        e.preventDefault();
        $(this).parents('.row').siblings('.comment-text-inpt').toggleClass('hide');
    });
});
$(function () {
    'use strict';
    $('.pay-btn').click(function () {
        $(this).addClass('hidden').siblings('.count-box').removeClass('hidden');
    });
});
$(function () {
    'use strict';
    $(document).on("click", ".pay-product-btn", function (e) {
        /*    var imgEl = $(this).parents('.product-list').siblings('.product-item-img').children().children('img');

            if (imgEl.length>0){//typeof imgEl != undefined ) {
                var imgClone = $(this).parents('.product-list').siblings('.product-item-img').children().children('img').clone();
                var paddLeft = document.documentElement.clientWidth;
                var paddEl = imgEl.offset().left + 260;
                paddEl = paddLeft - paddEl;
                imgClone.appendTo($(this).parents('.product-list').siblings('.product-item-img')).css({
                    'position': 'absolute',
                    'top': 0,
                    'left': '17%',
                    'z-index': 50,
                    'width': '62%'
                });
            }


            var imgEl = $(this).parents('.product-item-tabs').siblings('.product-item-img').children().children('img');
            if (imgEl.length>0) {
                var imgClone = $(this).parents('.product-item-tabs').siblings('.product-item-img').children().children('img').clone();
                var paddLeft = document.documentElement.clientWidth;
                var paddEl = imgEl.offset().left + 260;
                paddEl = paddLeft - paddEl;
                imgClone.appendTo($(this).parents('.product-item-tabs').siblings('.product-item-img')).css({
                    'position': 'absolute',
                    'top': 0,
                    'left': '17%',
                    'z-index': 50,
                    'width': '62%'
                });
            }



            imgClone.animate({width: 0, height: 0, opacity: 0, left: '+=' + paddEl, top: '-=350'}, 1000);*/
        $(this).addClass('hidden').siblings('.count-box').removeClass('hidden');
    });
});
$(function () {
    'use strict';
    var slideActions = $('.slider-actions').bxSlider({
        minSlides: 1,
        maxSlides: 5,
        infiniteLoop: false,
        slideWidth: 176,
        slideMargin: 22,
        moveSlides: 1,
        nextText: '',
        prevText: ''
    });
    if (slideActions.length > 0) {
        var slideQt = slideActions.getSlideCount();
        if (slideQt < 6) {
            slideActions.parents().parents('.bx-wrapper').addClass('non-align');
        }
    }
    ;$('.brands-bx').bxSlider({
        minSlides: 1,
        maxSlides: 5,
        infiniteLoop: false,
        slideWidth: 176,
        slideMargin: 22,
        moveSlides: 1,
        nextText: '',
        prevText: ''
    });
    $('.product-complex-bx').bxSlider({
        minSlides: 1,
        maxSlides: 1,
        slideWidth: 1015,
        moveSlides: 1,
        nextText: '',
        prevText: ''
    });
    var product_slider = $('.product-gallery-bx').bxSlider({

        pagerCustom: '#bx-pager', nextText: 'Следующий', prevText: 'Предыдущий', auto: false, autoControls: false
    });
    //pager:false,
    $('a[href="#new-products-pane"]').one('shown.bs.tab', function () {
        $('.slider-new-products').bxSlider({
            minSlides: 1,
            infiniteLoop: false,
            maxSlides: 5,
            slideWidth: 176,
            slideMargin: 22,
            moveSlides: 1,
            nextText: '',
            prevText: ''
        });
    });
    $('a[href="#royal-canin-pane"]').one('shown.bs.tab', function () {

        $('.slider-royal-canin').bxSlider({
            minSlides: 1,
            infiniteLoop: false,
            maxSlides: 5,
            slideWidth: 176,
            slideMargin: 24,
            moveSlides: 1,
            nextText: '',
            prevText: ''
        });
    });

});
$(function () {
    'use strict';
    $('.stars-rating').barrating({showSelectedRating: false});
});


$(document).on("click", "#showCartModal .minus-count", function (e) {
    e.preventDefault();
    var qtty = $(this).next('.count-products');
    var parent = $(this).closest('.ms2_product');

    var product_id = $(parent).find('input[name="product_id"]').val();
    var size = $(qtty).data('product-size');


    product_vendor = $(qtty).data('product-vendor');
    product_category = $(qtty).data('product-category');
    product_price = $(qtty).data('product-price');
    product_name = $(qtty).data('product-name');


    var size = $(this).closest('.card-costs-item').find('.weight').text();
    var btn_cart = $(qtty).parent('.count-box').prev('.add-cart');
    var key = $(this).parent().parent().parent().parent().find('.product-option').data('product-key');
    //alert(product_name);
    if ($(qtty).get(0).value - 1 > 0) {
        $(qtty).get(0).value--;
        cartChange(key, $(qtty).val());
        if ($(qtty).val() == 0) {
            dataLayer.push({
                'event': 'removeFromCart',
                'ecommerce': {
                    'remove': {                               // 'remove' actionFieldObject measures.
                        'products': [{                          //  removing a product to a shopping cart.
                            'name': product_name,
                            'id': product_id,
                            'price': product_price,
                            'brand': product_vendor,
                            'category': product_category,
                            'variant': size,
                            'quantity': 1
                        }]
                    }
                }
            });
        }

    } else {
        $(qtty).parent('.card-button .count-box').addClass('hidden');
        $(qtty).parent('.count-box').find('.minus-count').addClass('minus-countoff');


        dataLayer.push({
            'event': 'removeFromCart',
            'ecommerce': {
                'remove': {                               // 'remove' actionFieldObject measures.
                    'products': [{                          //  removing a product to a shopping cart.
                        'name': product_name,
                        'id': product_id,
                        'price': product_price,
                        'brand': product_vendor,
                        'category': product_category,
                        'variant': size,
                        'quantity': 1
                    }]
                }
            }
        });


        $(qtty).parent('.count-box').find('.minus-count').removeClass('minus-count');
        $(qtty).get(0).value = 0;
        $(btn_cart).removeClass('hidden');
        cartChange(key, $(qtty).val());
    }
});


$(document).on("click", ".plus-count", function (e) {
    e.preventDefault();
    var qtty = $(this).prev('.count-products');
    var parent = $(this).closest('.ms2_product');
    var product_id = $(parent).find('input[name="product_id"]').val();
    var size = $(this).closest('.card-costs-item').find('.weight').text();
    var key = $(this).closest('.product-option').data('product-key');
    $(qtty).get(0).value++;


    $(qtty).parent('.count-box').find('.minus-countoff').addClass('minus-count');
    $(qtty).parent('.count-box').find('.minus-countoff').removeClass('minus-countoff');
    cartChange(key, $(qtty).val());

    product_vendor = $(qtty).data('product-vendor');
    product_category = $(qtty).data('product-category');

    product_price = $(qtty).data('product-price');
    product_name = $(qtty).data('product-name');
    if ($(qtty).val() == 1) {
        dataLayer.push({
            'event': 'addToCart',
            'ecommerce': {
                'currencyCode': 'EUR',
                'add': {                                // 'add' actionFieldObject measures.
                    'products': [{                        //  adding a product to a shopping cart.
                        'name': product_name,
                        'id': product_id,
                        'price': product_price,
                        'brand': product_vendor,
                        'category': product_category,
                        'variant': size,
                        'quantity': 1
                    }]
                }
            }
        });
    }


});
$(document).on("click", ".minus-countoff", function (e) {
    return false;
});

$(document).on("click", ".basket-item .minus-count", function (e) {
    e.preventDefault();
    var qtty = $(this).next('.count-products');
    var parent = $(this).closest('.ms2_product');
    var product_id = $(parent).find('input[name="product_id"]').val();
    var size = $(this).closest('.card-costs-item').find('.weight').text();
    var btn_cart = $(qtty).parent('.count-box').prev('.add-cart');
    var key = $(this).closest('.product-option').data('product-key');
    if ($(qtty).get(0).value - 1 > 0) {
        $(qtty).get(0).value--;
        cartChange(key, $(qtty).val());
        var product_id = $(parent).find('input[name="product_id"]').val();
        var size = $(qtty).data('product-size');


        product_vendor = $(qtty).data('product-vendor');
        product_category = $(qtty).data('product-category');
        product_price = $(qtty).data('product-price');
        product_name = $(qtty).data('product-name');

        if ($(qtty).val() == 0) {
            dataLayer.push({
                'event': 'removeFromCart',
                'ecommerce': {
                    'remove': {                               // 'remove' actionFieldObject measures.
                        'products': [{                          //  removing a product to a shopping cart.
                            'name': product_name,
                            'id': product_id,
                            'price': product_price,
                            'brand': product_vendor,
                            'category': product_category,
                            'variant': size,
                            'quantity': 1
                        }]
                    }
                }
            });
        }

    } else {
        $(qtty).parent('.count-box').find('.minus-count').addClass('minus-countoff');
        $(qtty).parent('.count-box').find('.minus-count').removeClass('minus-count');
        $(qtty).get(0).value = 1;
        $(btn_cart).removeClass('hidden');
        cartChange(key, $(qtty).val());


        product_vendor = $(qtty).data('product-vendor');
        product_category = $(qtty).data('product-category');
        product_price = $(qtty).data('product-price');
        product_name = $(qtty).data('product-name');


        if ($(qtty).val() == 0) {
            dataLayer.push({
                'event': 'removeFromCart',
                'ecommerce': {
                    'remove': {                               // 'remove' actionFieldObject measures.
                        'products': [{                          //  removing a product to a shopping cart.
                            'name': product_name,
                            'id': product_id,
                            'price': product_price,
                            'brand': product_vendor,
                            'category': product_category,
                            'variant': size,
                            'quantity': 1
                        }]
                    }
                }
            });
        }
    }
});

$(document).on("click", ".card-button .minus-count", function (e) {
    e.preventDefault();
    var qtty = $(this).next('.count-products');
    var parent = $(this).closest('.ms2_product');

    var product_id = $(parent).find('input[name="product_id"]').val();
    var size = $(qtty).data('product-size');


    product_vendor = $(qtty).data('product-vendor');
    product_category = $(qtty).data('product-category');
    product_price = $(qtty).data('product-price');
    product_name = $(qtty).data('product-name');


    var size = $(this).closest('.card-costs-item').find('.weight').text();
    var btn_cart = $(qtty).parent('.count-box').prev('.add-cart');
    var key = $(this).closest('.product-option').data('product-key');
    //alert(product_name);
    if ($(qtty).get(0).value - 1 > 0) {
        $(qtty).get(0).value--;
        cartChange(key, $(qtty).val());
        if ($(qtty).val() == 0) {
            dataLayer.push({
                'event': 'removeFromCart',
                'ecommerce': {
                    'remove': {                               // 'remove' actionFieldObject measures.
                        'products': [{                          //  removing a product to a shopping cart.
                            'name': product_name,
                            'id': product_id,
                            'price': product_price,
                            'brand': product_vendor,
                            'category': product_category,
                            'variant': size,
                            'quantity': 1
                        }]
                    }
                }
            });
        }

    } else {
        $(qtty).parent('.card-button .count-box').addClass('hidden');
        $(qtty).parent('.count-box').find('.minus-count').addClass('minus-countoff');


        dataLayer.push({
            'event': 'removeFromCart',
            'ecommerce': {
                'remove': {                               // 'remove' actionFieldObject measures.
                    'products': [{                          //  removing a product to a shopping cart.
                        'name': product_name,
                        'id': product_id,
                        'price': product_price,
                        'brand': product_vendor,
                        'category': product_category,
                        'variant': size,
                        'quantity': 1
                    }]
                }
            }
        });


        $(qtty).parent('.count-box').find('.minus-count').removeClass('minus-count');
        $(qtty).get(0).value = 0;
        $(btn_cart).removeClass('hidden');
        cartChange(key, $(qtty).val());
    }
});
$(document).on("click", ".product-item.small-item .minus-count", function (e) {
    e.preventDefault();
    var qtty = $(this).next('.count-products');
    var parent = $(this).closest('.ms2_product');
    var product_id = $(parent).find('input[name="product_id"]').val();
    var size = $(this).closest('.card-costs-item').find('.weight').text();
    var btn_cart = $(qtty).parent('.count-box').prev('.add-cart');
    var key = $(this).closest('.product-option').data('product-key');
    if ($(qtty).get(0).value - 1 > 0) {
        $(qtty).get(0).value--;
        cartChange(key, $(qtty).val());
    } else {
        $(qtty).parent('.product-item.small-item .count-box').addClass('hidden');
        $(qtty).parent('.count-box').find('.minus-count').addClass('minus-countoff');
        $(qtty).parent('.count-box').find('.minus-count').removeClass('minus-count');
        $(qtty).get(0).value = 0;
        $(btn_cart).removeClass('hidden');
        cartChange(key, $(qtty).val());
    }
});
$('#msOrder').keydown(function (event) {
    if (event.keyCode == 13) {
        event.preventDefault();
        return false;
    }
});
$("input[name='suggest_locality']").blur(function () {
    setTimeout(function () {
        if ($("input[name='suggest_locality']").val() == '') {
            $("input[name='city']").val('');
            $("input[name='extended[city_id]'").val('');
        }
    }, 2000);
});

//$("input[name='email_search'], input[name='suggest_locality']").on('click',function(){$(this).select();});
$(document).on("blur", ".count-products", function () {
    var qtty = $(this).val();
    var key = $(this).closest('.product-option').data('product-key');
    cartChange(key, qtty);
});
$('.sort').on("click", ".mSort", function (e) {
    var data = $(this).data('sort');
    switch (data) {
        case'ms_popular,desc':
            $(this).addClass('up-sort');
            $(this).data('sort', 'ms_popular,asc');
            break;
        case'ms_popular,asc':
            $(this).removeClass('up-sort');
            $(this).data('sort', 'ms_popular,desc');
            break;
        case'ms_price,desc':
            $(this).addClass('up-sort');
            $(this).data('sort', 'ms_price,asc');
            break;
        case'ms_price,asc':
            $(this).removeClass('up-sort');
            $(this).data('sort', 'ms_price,desc');
            break;
        case'name,desc':
            $(this).addClass('up-sort');
            $(this).data('sort', 'name,asc');
            break;
        case'name,asc':
            $(this).removeClass('up-sort');
            $(this).data('sort', 'name,desc');
            break;
        default:
            console.log('Received unknown sort "' + data + '"');
    }
});
$(function () {
    'use strict';
    $(document).ready(function () {
        $('#showmore_mobile').click(function () {
            'use strict';
            var page = $('#paginatorpage').val();
            var results = $('.big-prevs-list');
            var params = mSearch2.Hash.get();
            params['action'] = 'filter';
            params['pageId'] = mse2Config.pageId;
            params['key'] = mse2Config.key;
            params['page'] = page;
            vendor = document.getElementById('mse2_ms|vendor');
            if ($(vendor).find('.checkbox input:checked').val() != 'undefined') {
                params['ms|vendor'] = $(vendor).find('.checkbox input:checked').val();
            }
            $.ajax({
                type: "GET", url: '/assets/components/msearch2/action.php', data: params, success: function (data) {
                    data = $.parseJSON(data);
                    if (data.success === true) {
                        results.append(data.data.results);
                        var pa = parseInt($('#paginatorpage').val());
                        pa = pa + 1;
                        $('#paginatorpage').val(pa);
                    } else if (data.success === false) {
                        results.html(data.message);
                    }
                    if (parseInt(data.data.ostatok) < 0) {
                        $('#showmore_mobile').hide();
                    } else if (data.data.ostatok == 0) {
                        $('#showmore_mobile').hide();
                    } else if (data.data.ostatok < 10) {
                        var ost = parseInt(data.data.ostatok);
                        $('#showmore_mobile .count').html(ost);
                    } else {
                        $('#showmore_mobile .count').html(10);
                        $('#showmore_mobile').show();
                    }
                    var showproduct = data.data.total - data.data.ostatok;
                    $('.number-products-vis').html(showproduct);
                    mSearch2.Hash.set(params);
                }
            });
        });
        $('.fancybox').fancybox({"frameWidth": 700, "imageScale": true, "frameHeight": 600,});
    });
});
(function ($) {
    $.fn.customSelect = function (settings) {
        var config = {
            replacedClass: 'replaced',
            customSelectClass: 'custom-select',
            activeClass: 'active',
            wrapperElement: '<div class="custom-select-container" />'
        };
        if (settings) {
            $.extend(config, settings);
        }
        this.each(function () {
            var select = $(this);
            select.addClass(config.replacedClass);
            select.wrap(config.wrapperElement);
            var update = function () {
                val = $('option:selected', this).text();
                span.find('span span').text(val);
            };
            select.change(update);
            select.keyup(update);
            var span = $('<span class="' + config.customSelectClass + '" aria-hidden="true"><span><span>' + $('option:selected', this).text() + '</span></span></span>');
            select.after(span);
            select.bind({
                mouseenter: function () {
                    span.addClass(config.activeClass);
                }, mouseleave: function () {
                    span.removeClass(config.activeClass);
                }, focus: function () {
                    span.addClass(config.activeClass);
                }, blur: function () {
                    span.removeClass(config.activeClass);
                }
            });
        });
    };
})(jQuery);
$(document).ready(function () {
    $('#office-profile-form .js-form-expand').hide();
    $('body').on('click', '.js-form-trigger', function () {
        $(this).toggleClass('is-active');
        $(this).parents('.js-form').find('.js-form-expand').slideToggle('fast');
    });
    $('select').not('.stars-rating,#sort-by,#mse2_sort, .prevcatalogselect').customSelect();
    if ($('#discountsumma').html() == '') $('.js-form-expand').hide();
    $('.brand-expandall').click(function () {
        vendor1 = $('.allbrands');
        vendor2 = $('.commonbrands');
        vendor2.css('display', 'block');
        vendor1.css('display', 'none');
    });
    $(".checkbox.hidden input:checkbox:checked").parent().removeClass('hidden');
    $('.brand-expand').click(function () {
        vendor = $(this).parent('.js-form');
        vendor.children('.checkbox:gt(10)').toggleClass('hidden', 300);
        $(this).toggleClass('is-active');
        if ($(this).text() == 'Показать все бренды') {
            $(this).text('Показать основные бренды');
        } else {
            $(this).text('Показать все бренды');
        }
    });
    $('.js-form-link').click(function () {
        var input = $(this).parents('.js-form-wrapper').find('.js-form-input');
        if (input.is(':disabled')) {
            input.prop("disabled", false);
        } else {
            input.prop("disabled", true);
        }
    });
    $('.checkbox').click(function () {
        var filter = $(this).children('input');
        var filter_name = $(filter).val();
        filter_name = filter_name.replace('топ', '<sub>топ</sub>');

        var filter_id = $(filter).attr('id');

        $('<div class="choose-el" data-filter="' + filter_id + '"><span class="icon del-icon"></span>' + filter_name + '</div>').appendTo('.filter-selected-items');
        if ($(this).hasClass('disabled')) {
            $(this).find('input[type="checkbox"]').prop('disabled', true);
        }
    });
    $(document).on('change', $('#mse2_selected').size(), function (e) {
        if ($('#mse2_selected span').size() > 1) {
            $('#mse2_selected').removeClass('hidden');
        } else {
            $('#mse2_selected').addClass('hidden');
        }
    });
    $('.delete-filters').click(function (e) {
        url = $(this).data('url');
        if (url != '') location.href = url;
        e.preventDefault();
        $('#mse2_filters').trigger('reset').submit();
        $('.filter-selected-items').empty();
        $('#mse2_selected span').empty();
        $('#mse2_selected').addClass('hidden');
    });
    $(document).on("click", "a,button", function (e) {
        var action = $(this).data('action');
        if (action) {
            switch (action) {
                case'cart/remove':
                    var cart_row = $(this).parent('.product-option');
                    var key = cart_row.data('product-key');
                    cart_row.fadeOut(300, function () {
                        $(this).remove();
                    });

                    product_name = $(this).data('product-name');
                    product_id = $(this).data('product-id');
                    product_price = $(this).data('product-price');
                    product_vendor = $(this).data('product-vendor');
                    product_category = $(this).data('product-category');
                    size = $(this).data('product-size');
                    countcart = $(this).data('product-count');

                    // if (countcart==0) {
                    dataLayer.push({
                        'event': 'removeFromCart',
                        'ecommerce': {
                            'remove': {                               // 'remove' actionFieldObject measures.
                                'products': [{                          //  removing a product to a shopping cart.
                                    'name': product_name,
                                    'id': product_id,
                                    'price': product_price,
                                    'brand': product_vendor,
                                    'category': product_category,
                                    'variant': size,
                                    'quantity': 1
                                }]
                            }
                        }
                    });
                    // }
                    removeFromCart(key);
                    break;

                case'cart/add':
                    var product_id = $(this).data('product-id');
                    var size = $(this).data('size');
                    var count = $(this).next().children('.count-products').val();

                    //alert(count);
                    var product_name = $(this).data('product-name');
                    var product_price = $(this).data('product-price');
                    var product_category = $(this).data('product-category');
                    var product_vendor = $(this).data('product-vendor');
                    var type = $(this).data('product-type');

                    var optionId = $(this).data('option-id');

                    if (type == 'page') {
                        dataLayer.push({
                            'event': 'event-GA',
                            'eventCategory': 'product',
                            'eventAction': 'buy-product-page'
                        });
                    }
                    else {
                        dataLayer.push({
                            'event': 'event-GA',
                            'eventCategory': 'product',
                            'eventAction': 'buy-preview'
                        });
                    }

                    //if (count==1) {
                    // alert(product_name);
                    dataLayer.push({
                        'ecommerce': {
                            'event': 'addToCart',
                            'currencyCode': 'UAH',
                            'add': {                                // 'add' actionFieldObject measures.
                                'products': [{                        //  adding a product to a shopping cart.
                                    'name': product_name,
                                    'id': product_id,
                                    'price': product_price,
                                    'brand': product_vendor,
                                    'category': product_category,
                                    'variant': size,
                                    'quantity': 1
                                }]
                            }
                        },
                        'event': 'gtm-ee-event',
                        'gtm-ee-event-category': 'Enhanced Ecommerce',
                        'gtm-ee-event-action': 'Adding to Shopping Cart',
                        'gtm-ee-event-non-interaction': 'False',

                    });
                    //}

                    addToCart(product_id, size, 1,optionId);

                    if (count < 1) {
                        $(this).next().children('.count-products').val(1);
                    }
                    break;
                case'cart/update':
                    break;
                case'cart/get':
                    break;
                case'pet/update':
                    e.preventDefault();
                    var form = $(this).closest('form');
                    var postData = new FormData(form[0]);

                    var photonew = form.find('.newphoto');
                    if (photonew) {
                        //alert(photonew.val());
                        if (photonew.val() == '') {
                            $.ajax({
                                type: "POST",
                                url: '/assets/components/pets/updatepetdata.php',
                                //enctype: 'multipart/form-data',
                                data: postData,
                                processData: false, // add this here
                                processData: false,
                                contentType: false
                            }).done(function (data) {

                                data = $.parseJSON(data);
                                var photo = form.find('.img-circle');
                                if (data.newphoto) photo.attr('src', data.newphoto);
                            });

                        } else {
                            $.ajax({
                                type: "POST",
                                url: '/assets/components/pets/updatepetdata.php',
                                enctype: 'multipart/form-data',
                                data: postData,
                                processData: false,
                                contentType: false
                            }).done(function (data) {

                                data = $.parseJSON(data);
                                var photo = form.find('.img-circle');
                                if (data.newphoto) photo.attr('src', data.newphoto);
                            });
                        }
                    }


                    break;
                case'pet/remove':
                    e.preventDefault();
                    var pet_id = $(this).closest('form').find('input[name="pet_id"]').val();
                    $(this).closest('form').remove();
                    petRemove(pet_id);
                    if ($('#petlist form').size() == 0) $('#petlist .info-msg').show(); else
                        $('#petlist .info-msg').hide();
                    break;
                default:
                    console.log('Received unknown action "' + action + '"');
            }
        }
    });
    $('#warehouses').on('change', function (e) {
        var warehouse_obj = $(this).find(':selected');
        var val = warehouse_obj.val();
        var textval = warehouse_obj.text();
        $("#textwarehouse").val(textval);
    });
    $('#msOrder').validate({
        ignore: "",
        rules: {
            // name: {
            //     required: true
            // },
            // fathersname: {
            //     required: true
            // }
        }
    });


    $('#deliveries').on('change', function (e) {
        var delivery_obj = $(this).find(':selected');
        var delivery_payments = $(this).find(':selected').data('payments');
        var delivery_id = delivery_obj.val();
        var city_id = $('input[name="extended[city_id]"]').val();
        $('#warehouse-block .with-errors').hide();
        $('#warehouse-block .custom-select-container').show();
        $('#warehouses').prop('disabled', false);
        $('#payments').prop('disabled', false);
        if ($('#delivery_id').val() == 2)
            $('#infodostavkaorder').html('');


        if (delivery_id == 5){// курьер НП
            $('.fathername').removeClass('hide');
            $("#msOrder").validate(); //sets up the validator

            console.log(delivery_id+' fathername required ');
            $('#fathername').rules('add', { required: true });
            $('#fathername').prop('required', true);//addAttribute();
            $('.form-group.fathername').addClass('has-error');

        }else{
            $('.fathername').addClass('hide');
            $('#fathername').prop('required', false);
            $('.form-group.fathername').removeClass('has-error');
            //$('#fathername').rules('remove', 'required');
        }

            getPayments(delivery_id);
        $('#payments option').each(function () {
            var payment = $(this).val();
            if ($.inArray(payment, delivery_payments) == -1) {
                $(this).prop('disabled', 'disabled').addClass('hidden');
                $('.payment-descr').addClass('hidden');
            } else {
                $(this).prop('disabled', false).removeClass('hidden');
                $(this).prop('disabled', false).removeClass('hide');
            }
        });

        if (($(this).val() == 5) || ($(this).val() == 4)) {
            $('#warehouse-block').addClass('hide');
            $('#courier-block').removeClass('hide');
            $('#samovivoz-block').addClass('hide');
            if ($(this).val() == 5) $('.showdopkurier').hide();

            if ($(this).val() == 4) {
                $('#street_ext').show();
                $('#street_np').hide();
                $('#courier-block .show_np').hide();
                $('#courier-block .hidenp').show();
            }else{
                $('#courier-block .show_np').show();

                $('#street_ext').hide();
                $('#street_ext').val('');

                $('#street_np').show();

                var cityId=$('#cityidorder').val();
                var cityName=$('input[name=city]').val();
                $('input#street_np').typeahead(
                    {
                        source: function (query, process) {

                            return $.post('/assets/components/suggest/getStreetsForSuggestJSON.php', {
                                'name': query,
                                'city_id':cityId,
                                'city_name':cityName,
                            }, function (response) {
                                $('input#street_np').focus();
                                var data = new Array();
                                $('#street_np_ref').val('');
                                if (response.content) {
                                    if (response.content.records) {
                                        //var records = response.content.records;

                                        $.each(response.content.records, function (i) {
                                            $.each(response.content.records[i], function (key, val) {
                                                data.push(key + '_' + val);
                                            });
                                            return process(data);
                                        });
                                    }
                                }
                            }, 'json');
                        },
                        matcher: function (item) {
                            return true;
                            if (item.toLowerCase().indexOf(this.query.trim().toLowerCase()) != -1) {
                                return true;
                            }
                        },
                        sorter: function (items) {
                            return items.sort();
                        },
                        highlighter: function (item) {
                            var parts = item.split('_');
                            parts.shift();
                            return parts.join('_');
                        },
                        updater: function (item) {
                            var parts = item.split('_');

                            console.log(parts);

                            $('#street_np_ref').val(parts[0]);//parts[0]);
                            $('#street_np').val(parts);//[1]);

                            return parts[1];//parts.join('_');

                            //var cityId = parts.shift();
                            /*if (cityId != 0) {
                                setTimeout(function () {
                                    $('.deliverieswrap .col-xs-8 .custom-select-container').addClass('loading');
                                }, 200);
                                $.post('/assets/components/delivery/getdeliverydata.php', {'city_id': cityId}, function (deliveries) {
                                    $('#deliveries').prop('disabled', false);
                                    $('#deliveries').html('');
                                    $('#deliveries').customSelect('update');
                                    $('#deliveries').html(deliveries);
                                    var getFirstDelivery = $("#deliveries option:first").val();
                                    $('#deliveries').customSelect('update');
                                    $('#deliveries').prop('selectedIndex', getFirstDelivery);
                                    $('#deliveries option:first').prop('selected', true);
                                    var getFirstWarehouse = $("#deliveries option:first").val();
                                    if (getFirstWarehouse == 4) {
                                        $('#warehouse-block').addClass('hide');
                                        $('#courier-block').removeClass('hide');
                                    }
                                    else if (getFirstWarehouse == 1) {
                                        $('#warehouse-block').addClass('hide');
                                        $('#samovivoz-block').removeClass('hide');
                                        $('#courier-block').addClass('hide');
                                    }
                                    else {
                                        $('#courier-block').addClass('hide');
                                        $('#samovivoz-block').addClass('hide');
                                        $('#warehouse-block').addClass('hide');
                                    }
                                    setTimeout(function () {
                                        $('.deliverieswrap .col-xs-8 .custom-select-container').removeClass('loading');
                                    }, 500);
                                    $('.deliverieswrap .col-xs-8 .custom-select-container').customSelect('update');
                                }, 'json');
                                $("input[name=city]").val(parts);
                                $("input[name='extended[city_id]']").val(cityId);
                                return parts.join('_');
                            } else return '';
*/
                        },
                        autoSelect: false, highlight: false, minLength: 0, delay: 400, items: 10
                    });
                $('#courier-block .hidenp').hide();
            }
        }
        else if ($(this).val() == 1) {
            $('#warehouse-block').addClass('hide');
            $('#samovivoz-block').removeClass('hide');
            $('#courier-block').addClass('hide');

            $('#street_ext').show();
            $('#street_np').hide();
            $('#courier-block .show_np').hide();
            $('#courier-block .hidenp').show();
        }
        else {
            $('#courier-block').addClass('hide');
            $('#samovivoz-block').addClass('hide');
            $('#warehouse-block').removeClass('hide');
            $('#warehouse-block .col-xs-8 .custom-select-container').addClass('loading');
            getWarehouses(city_id, delivery_id);


            $('#street_ext').show();
            $('#street_np').hide();
            $('#courier-block .show_np').hide();
            $('#courier-block .hidenp').show();
        }
    });
    if (($('#deliveries').val() == 5) || ($('#deliveries').val() == 4)) {
        $('#warehouse-block').addClass('hide');
        $('#courier-block').removeClass('hide');

        if ($('#deliveries').val() == 4)  {
            $('#courier-block .show_np').hide();

            $('#courier-block .hidenp').show();
            $('#courier-block .hidenp').show();
        }else{
            $('#courier-block .show_np').show();
            $('#courier-block .hidenp').hide();
            $('#courier-block .hidenp').hide();
        }
    }
    else if ($('#deliveries').val() == 1) {
        $('#samovivoz-block').removeClass('hide');
        $('#courier-block').addClass('hide');
        $('#warehouse-block').addClass('hide');
    } else {
        $('#courier-block').addClass('hide');
        $('#samovivoz-block').addClass('hide');
        $('#warehouse-block').removeClass('hide');
    }
    $(".payment-descr[data-payment-id='" + $('#payments').find(':selected').val() + "']").removeClass('hidden');
    $(".payment-descr[data-payment-id='" + $('#payments').find(':selected').val() + "']").removeClass('hide');
    $('#payments').on('change', function (e) {
        if ($(this).val() == 4) $('#payment_liqpay_submit').removeClass('hide');
        else $('#payment_liqpay_submit').addClass('hide');
        $('.payment-descr').addClass('hidden');
        $(".payment-descr[data-payment-id='" + $(this).val() + "']").removeClass('hidden');
        $(".payment-descr[data-payment-id='" + $(this).val() + "']").removeClass('hide');

        total_cost_order = parseFloat($('.total_cost_order').val());

        if ($(this).val() == 6) {
            if ($('#summadostavkaorder').html() == 'Бесплатно') $('#descdeliveryorder').html('Внимание! Перевод денег осуществляется за Ваш счет');
            else if (total_cost_order < 199) {
                payment_desc = 'Заказы до 199 грн наложенным платежом не отправляются';
                //$(".payment-descr[data-payment-id='" + $(this).val() + "']").html()+'<br/>Заказы до 199 грн наложенным платежом не отправляются';
                $(".payment-descr[data-payment-id='" + $(this).val() + "']").html(payment_desc);
            }
            else {
                $('#descdeliveryorder').html('');
                $(".payment-descr[data-payment-id='" + $(this).val() + "']").html('Перевод денег осуществляется за Ваш счет');

            }
        } else $('#descdeliveryorder').html('');
    });
});
$('.payorder').on('click', function () {
    if ($(this).data('value') != '') location.href = $(this).data('value');
    return false;
});
$('#getorder input[name="msorder"]').on('keyup', function () {
    var msorder = $(this).val();
    var name = $('#getorder').find('.name');
    $.ajax({
        type: "GET",
        url: '/assets/components/minishop2/getorder.php',
        data: {msorder: msorder},
        success: function (data) {
            data = $.parseJSON(data);
            if (data.success === true) {
                $('.errorliqpay').addClass('hide');
                $('.errorliqpay').removeClass('show');
                $(name).text(data.data.name);
                if ((data.data.paymentid != 3) && (data.data.paymentid != 4)) {
                    $('#getorder').find('.orange-btn').addClass('hidden');
                    $('#getorder').find('.pull-left').css('width', '100%');
                } else {
                    $('#getorder').find('.orange-btn').removeClass('hidden');
                    if (data.data.link != '') {
                        $('.payorder').data('value', data.data.link);
                    }
                    if (data.data.form != '') $('#getorder').append(data.data.form);
                }
                $('#getorder').find('.payment').removeClass('hidden');
                $('#getorder').find('.deliveryorderfooter').html(data.data.delivery);
                $('#getorder').find('.payment').html(data.data.payment + '<br/>' + data.data.text);
                $('#getorder').find('.green-check-icon').removeClass('hidden');
                $('#getorder').find('.red-check-icon').addClass('hidden');
                $('#getorder').find('.product-price').html(data.data.totalcost);
                $('#getorder').find('.modal-footer').removeClass('hidden');
            } else {
                $('.errorliqpay').removeClass('hide');
                $('.errorliqpay').addClass('show');
                $('.errorliqpay').html(data.message);
                $('#getorder').find('.payment').addClass('hidden');
                $('#getorder').find('.green-check-icon').addClass('hidden');
                $('#getorder').find('.red-check-icon').removeClass('hidden');
                $('#getorder').find('.modal-footer').addClass('hidden');
            }
        }
    });
});
$(document).on("click", "#order_submit", function (e) {
    $('#msOrder').submit();
});
$(".count-products").each(function (index) {
    if ($(this).val() > 0) {
        $(this).closest('.count-box').removeClass('hidden');
        $(this).closest('.card-button').children('.add-cart').addClass('hidden');
    }
});
action_url = '/assets/components/minishop2/action.php';
//$('#showCartModal').modal();

$(document).on("click", ".close_modal_cart", function (e) {

    $('#showCartModal').modal('hide');
    return false;
});

function addToCart(product_id, size, count, optionId) {
    if (!count) count = 1;
    var productData = {
        count: count,
        ctx: "web",
        option_id: optionId,
        id: product_id,
        options: {size: size},
        ms2_action: 'cart/add'
    };
    $.ajax({
        type: "POST", url: action_url, data: productData, success: function (data) {
            data = $.parseJSON(data);
            if (data.success === true) {


                $('#showCartModal .modal-body').html(data.data.cart);
                $('#showCartModal').modal();

                setTimeout(function () {
                    $('#showCartModal .bxslider').bxSlider({
                        minSlides: 2,
                        maxSlides: 3,
                        infiniteLoop: false,
                        slideWidth: 176,
                        slideMargin: 22,
                        moveSlides: 1,
                        nextText: '',
                        prevText: ''
                    });
                }, 500);

                if (data.data.total_count > 0) {
                    $('.basket-header .total').html(data.data.total_count + ' товаров - ' + data.data.total_cost + ' грн.');
                    if ($('#msMiniCart .basket-btn').hasClass('hidden')) $('#msMiniCart .basket-btn').removeClass('hidden');
                } else {
                    $('.basket-header .total').html('Ваша корзина пуста');
                    $('#msMiniCart .basket-btn').addClass('hidden');
                }
            } else if (data.success === false) {
            }
        }
    });
}

function addToCartMobile(product_id, size, count,optionId) {
    if (!count) count = 1;
    var productData = {count: count, ctx: "web", id: product_id, option_id: optionId, options: {size: size}, ms2_action: 'cart/add'};
    $.ajax({
        type: "POST", url: action_url, data: productData, success: function (data) {
            data = $.parseJSON(data);
            if (data.success === true) {
                if (data.data.total_count > 0) {
                    $('#msMiniCart span.products-on-basket').html('<a href="/order?edit_cart=1">' + data.data.total_count + '</a>');
                } else {
                    $('#msMiniCart span.products-on-basket').html('<a href="/order?edit_cart=1">' + data.data.total_count + '</a>');
                }
                $('.windows-popup-container').fadeOut(300);
            } else if (data.success === false) {
            }
        }
    });
}

function cartChangeMobile(key, count) {
    var productData = {
        count: count,
        ctx: "web",
        key: key,
        cityid: $('#cityidorder').val(),
        delivery: $('#deliveries option:selected').val(),
        payment: $('#payments option:selected').val(),
        ms2_action: 'cart/change'
    };
    $.ajax({
        async: "false", type: "POST", url: action_url, data: productData, success: function (data) {
            data = $.parseJSON(data);
            if (data.success === true) {
                var total = data.data.total_cost;
                if (data.data.cost_delivery) {
                    if (typeof data.data.cost_delivery == "number") {
                        cost = data.data.cost_delivery + ' грн.';
                    } else {
                        cost = data.data.cost_delivery;
                    }
                    $('#summadostavkaorder').html(cost);
                }
                if (data.data.infodelivery) {
                    $('#infodeliveryorder').html(data.data.infodelivery);
                }
                if (data.data.descdelivery) {
                    $('#descdeliveryorder').html(data.data.descdelivery);
                }
                if ((data.data.economy !== false) && (data.data.economy !== undefined)) {
                    economy = data.data.economy.toString().split('.', 2);
                    if (economy[1] == undefined) economylit = '00'; else economylit = economy[1].toString();
                    eco = economy[0] + '<sup>' + economylit.toString() + '</sup>';
                    $('#discountsumma').html(eco);
                } else {
                    eco = '<b>0</b><sup>00</sup>';
                    $('#discountsumma').html(eco);
                }
                if ((data.data.message_promocode !== false) && (data.data.message_promocode !== undefined)) {
                }
                $.each(data.data.items, function (i, val) {
                    var product = $('.basket-list').find('*[data-product-key=' + val.key + ']');
                    cost = val.cost;
                    if (val.count == 1) {
                        minuscount = $(product).find('.minus-count');
                        minuscount.addClass('minus-countoff');
                        minuscount.removeClass('minus-count');
                    }
                    cost = cost.toString().split('.', 2);
                    $(product).find('.price-large b').text(cost[0]);
                    $(product).find('.count-products').val(val.count);
                    if (cost[1] == undefined) $(product).find('.price-large sup.small').text('00'); else $(product).find('.price-large sup.small').text(cost[1]);
                    if (key == val.key) {
                        if (val.keynew != '') {
                            keypodarok = val.keynew;
                            if (val.htmlimage != '') {
                                var newproduct1 = $('.basket-list').find('*[data-product-key=' + keypodarok + ']');
                                if (newproduct1.html() == undefined) $('.basket-list').append(val.htmlimage);
                            } else {
                                if (parseInt(val.count) < parseInt(val.plusonecondition)) {
                                    var newproduct1 = $('.basket-list').find('*[data-product-key=' + keypodarok + ']');
                                    newproduct1.remove();
                                }
                            }
                        }
                    }
                });
                cost = data.data.total_cost_withdelivery.toString().split('.', 2);
                if (cost[1] == undefined) costlit = '00'; else costlit = cost[1].toString();
                co = cost[0] + '<sup>' + costlit.toString() + '</sup>';
                $('.price-summary .price-xlarge').html(co);
                $('.total_cost_order').val(total);
                if (data.data.total_count > 0) {
                    $('.basket-header .total').html(data.data.total_count + ' товаров - ' + data.data.total_cost + ' грн.');
                    if ($('#msMiniCart .basket-btn').hasClass('hidden')) $('#msMiniCart .basket-btn').removeClass('hidden');
                } else {
                    $('.basket-header .total').html('Ваша корзина пуста');
                    $('#msMiniCart .basket-btn').addClass('hidden');
                }
            } else if (data.success === false) {
            }
        }
    });
}

function cartChange(key, count) {
    var productData = {
        count: count,
        ctx: "web",
        key: key,
        cityid: $('#cityidorder').val(),
        delivery: $('#deliveries option:selected').val(),
        payment: $('#payments option:selected').val(),
        ms2_action: 'cart/change'
    };
    $.ajax({
        async: "false", type: "POST", url: action_url, data: productData, success: function (data) {
            data = $.parseJSON(data);

            if (data.success === true) {
                var total = data.data.total_cost;
                var count_text_products = data.data.count_text_products;
                $('#showCartModal .count_product a').html(count_text_products);
                $('#showCartModal .total_amount_cart span').html(total + ' грн.');
                if (data.data.cost_delivery) {
                    if (typeof data.data.cost_delivery == "number") {
                        cost = data.data.cost_delivery + ' грн.';
                    } else {
                        cost = data.data.cost_delivery;
                    }
                    $('#summadostavkaorder').html(cost);
                }
                if (data.data.infodelivery) {
                    $('#infodeliveryorder').html(data.data.infodelivery);
                }
                if (data.data.descdelivery) {
                    $('#descdeliveryorder').html(data.data.descdelivery);
                }
                if ((data.data.economy !== false) && (data.data.economy !== undefined)) {
                    economy = data.data.economy.toString().split('.', 2);
                    if (economy[1] == undefined) economylit = '00'; else economylit = economy[1].toString();
                    eco = economy[0] + '<sup>' + economylit.toString() + '</sup>';
                    $('#discountsumma').html(eco);
                } else {
                    eco = '<b>0</b><sup>00</sup>';
                    $('#discountsumma').html(eco);
                }
                if ((data.data.message_promocode !== false) && (data.data.message_promocode !== undefined)) {
                }
                $.each(data.data.items, function (i, val) {
                    var product = $('.basket-list').find('*[data-product-key=' + val.key + ']');
                    cost = val.cost;
                    if (val.count == 1) {
                        minuscount = $(product).find('.minus-count');
                        minuscount.addClass('minus-countoff');
                        minuscount.removeClass('minus-count');
                    }
                    cost = cost.toString().split('.', 2);
                    $(product).find('.price-large b').text(cost[0]);
                    $(product).find('.count-products').val(val.count);
                    if (cost[1] == undefined) $(product).find('.price-large sup.small').text('00'); else $(product).find('.price-large sup.small').text(cost[1]);
                    if (key == val.key) {
                        if (val.keynew != '') {
                            keypodarok = val.keynew;
                            if (val.htmlimage != '') {
                                var newproduct1 = $('.basket-list').find('*[data-product-key=' + keypodarok + ']');
                                if (newproduct1.html() == undefined) $('.basket-list').append(val.htmlimage);
                            } else {
                                if (parseInt(val.count) < parseInt(val.plusonecondition)) {
                                    var newproduct1 = $('.basket-list').find('*[data-product-key=' + keypodarok + ']');
                                    newproduct1.remove();
                                }
                            }
                        }
                    }
                });
                cost = data.data.total_cost_withdelivery.toString().split('.', 2);
                if (cost[1] == undefined) costlit = '00'; else costlit = cost[1].toString();
                co = cost[0] + '<sup>' + costlit.toString() + '</sup>';
                $('.price-summary .price-xlarge').html(co);
                $('.total_cost_order').val(total);
                if (data.data.total_count > 0) {
                    $('.basket-header .total').html(data.data.total_count + ' товаров - ' + data.data.total_cost + ' грн.');
                    if ($('#msMiniCart .basket-btn').hasClass('hidden')) $('#msMiniCart .basket-btn').removeClass('hidden');
                } else {
                    $('.basket-header .total').html('Ваша корзина пуста');
                    $('#msMiniCart .basket-btn').addClass('hidden');
                }
            } else if (data.success === false) {
            }
        }
    });
}

function cartChangeMobile(key, count) {
    var productData = {count: count, ctx: "web", key: key, ms2_action: 'cart/change'};
    $.ajax({
        async: "false", type: "POST", url: action_url, data: productData, success: function (data) {
            data = $.parseJSON(data);
            if (data.success === true) {
                var total = data.data.total_cost;
                $.each(data.data.items, function (i, val) {
                    var product = $('.basket-table-list').find('*[data-product-key=' + val.key + ']');
                    cost = val.cost;
                    if (val.count == 1) {
                        minuscount = $(product).find('.minus-count');
                        minuscount.addClass('minus-countoff');
                        minuscount.removeClass('minus-count');
                    }
                    cost = cost.toString();
                    $(product).find('.product-total-price').html(cost);
                    if (key == val.key) {
                        if (val.keynew != '') {
                            keypodarok = val.keynew;
                            if (val.htmlimage != '') {
                                var newproduct1 = $('.basket-table-list').find('*[data-product-key=' + keypodarok + ']');
                                if (newproduct1.html() == undefined) $('.basket-table-list').append(val.htmlimage);
                            } else {
                                if (parseInt(val.count) < parseInt(val.plusonecondition)) {
                                    var newproduct1 = $('.basket-table-list').find('*[data-product-key=' + keypodarok + ']');
                                    newproduct1.remove();
                                }
                            }
                        }
                    }
                });
                cost = data.data.total_cost_withdelivery.toString();
                $('.orders-total-price div').html(cost);
            } else if (data.success === false) {
            }
        }
    });
}


function removeFromCart(key, button) {
    var productData = {
        key: key,
        cityid: $('#cityidorder').val(),
        delivery: $('#deliveries option:selected').val(),
        payment: $('#payments option:selected').val(),
        ctx: "web",
        ms2_action: 'cart/remove'
    };


    product_id = $(button).data('product-id');

    product_vendor = $(button).data('product-vendor');
    product_category = $(button).data('product-category');
    product_price = $(button).data('product-price');
    product_size = $(button).data('size');
    product_name = $(button).data('product-name');
    dataLayer.push({
        'ecommerce': {
            'remove': {                               // 'remove' actionFieldObject measures.
                'products': [{                          //  removing a product to a shopping cart.
                    'name': product_name,
                    'id': product_id,
                    'price': product_price,
                    'brand': product_vendor,
                    'category': product_category,
                    'variant': product_size,
                    'quantity': 1
                }]
            }
        },
        'event': 'gtm-ee-event',
        'gtm-ee-event-category': 'Enhanced Ecommerce',
        'gtm-ee-event-action': 'Removing from Shopping Cart',
        'gtm-ee-event-non-interaction': 'False',

    });

    $.ajax({
        type: "POST", url: action_url, data: productData, success: function (data) {
            data = $.parseJSON(data);
            if (data.success === true) {
                if (data.data.total_count > 0) {
                    $('.basket-header .total').html(data.data.total_count + ' товаров - ' + data.data.total_cost + ' грн.');
                    cost = data.data.total_cost_withdelivery.toString().split('.', 2);
                    if (cost[1] == undefined) costlit = '00'; else costlit = cost[1];
                    co = cost[0] + '<sup>' + costlit + '</sup>';
                    $('.price-summary .price-xlarge').html(co);
                    $('.total_cost_order').val(data.data.total_cost);
                    if (data.data.keydelete) {
                        $('.basket-list').find('*[data-product-key=' + data.data.keydelete + ']').remove();
                    }
                    var total = data.data.total_cost;
                    if (data.data.cost_delivery) {
                        if (typeof data.data.cost_delivery == "number") {
                            cost = data.data.cost_delivery + ' грн.';
                        } else {
                            cost = data.data.cost_delivery;
                        }
                        $('#summadostavkaorder').html(cost);
                    }
                    if (data.data.infodelivery) {
                        $('#infodeliveryorder').html(data.data.infodelivery);
                    }
                    if (data.data.descdelivery) {
                        $('#descdeliveryorder').html(data.data.descdelivery);
                    }
                } else {
                    $('.basket-header .total').html('Ваша корзина пуста');
                    $('.product-option.basket-item').remove();
                    $('.price-summary .price-xlarge').text(0);
                    $('.total_cost_order').val(0);
                }
            } else if (data.success === false) {
            }
        }
    });
}


function removeFromCartMobile(key) {
    var productData = {key: key, ctx: "web", ms2_action: 'cart/remove'};
    $.ajax({
        type: "POST", url: action_url, data: productData, success: function (data) {
            data = $.parseJSON(data);
            if (data.success === true) {
                if (data.data.total_count > 0) {
                    $('#msMiniCart span').html('<a href="/order?edit_cart=1">' + data.data.total_count + '</a>');
                    $('.basket-table-list').find('*[data-product-key=' + key + ']').remove();
                    var total = data.data.total_cost;
                    $('.orders-total-price div').html(total + ' грн.');
                } else {
                    $('.basket-header .total').html('Ваша корзина пуста');
                    $('.product-option.basket-item').remove();
                    $('.price-summary .price-xlarge').text(0);
                    $('.total_cost_order').val(0);
                }
            } else if (data.success === false) {
            }
        }
    });
}

function getCart() {
    var productData = {ctx: "web", ms2_action: 'cart/get'};
    $.ajax({
        async: "false", type: "GET", url: action_url, data: productData, success: function (data) {
            data = $.parseJSON(data);
            if (data.success === true) {
                if (data.data.total_count > 0) {
                    $('.basket-header .total').html(data.data.total_count + ' товаров - ' + data.data.total_cost + ' грн.');
                    if ($('#msMiniCart .basket-btn').hasClass('hidden')) $('#msMiniCart .basket-btn').removeClass('hidden');
                } else {
                    $('.basket-header .total').html('Ваша корзина пуста');
                    $('#msMiniCart .basket-btn').addClass('hidden');
                }
            } else if (data.success === false) {
            }
        }
    });
}

function cleanCart() {
    var productData = {ctx: "web", ms2_action: 'cart/clean'};
    $.ajax({type: "GET", url: action_url, data: productData});
}

$('.product.add-cart').on('click', function (e) {
    var parent = $(this).closest('.ms2_product');
    var product_id = $(parent).find('input[name="product_id"]').val();
    var size = $(this).closest('.card-costs-item').find('.weight').text();
    var count = $(this).next().children('.count-products').val();


    if (count < 1) {
        $(this).next().children('.count-products').val(1);
    }
});

function appendPet(data) {
    var clone = $('#add_pet_form').clone();
    clone.removeAttr('id');
    $("#petlist").append(data);
    $('#add_pet_form')[0].reset();
    $('select').customSelect();
}

function petRemove(id) {
    var petData = {id: id};
    $.ajax({type: "POST", url: '/assets/components/pets/removepetdata.php', data: petData});
}

function petUpdate(data) {
    $.ajax({
        type: "POST",
        url: '/assets/components/pets/updatepetdata.php',
        enctype: 'multipart/form-data',
        data: data,
        processData: false,
        contentType: false
    }).done(function (data) {
        console.log("PHP Output:");
        console.log(data);
    });
}

function getPayments(delivery_id) {
    var paymentData = {
        id: delivery_id,
        total_order: $('.total_cost_order').val(),
        cityidorder: $('#cityidorder').val(),
    };
    $.ajax({
        async: "false",
        type: "GET",
        url: '/assets/components/delivery/getpaymentdata.php',
        data: paymentData,
        success: function (data) {
            data = $.parseJSON(data);
            if (data.success === true) {
                $('#payments').html(data.data);
                $('#infodeliveryorder').html(data.infodelivery);
                if (typeof data.cost_delivery == "number") {
                    cost = data.cost_delivery + ' грн.';
                } else {
                    cost = data.cost_delivery;
                }
                if (data.cost_delivery != '') $('#summadostavkaorder').html(cost); else $('#summadostavkaorder').html(data.descdelivery);
                $('#totalcart').html(data.total_order_format);
                $('.total_cost_order').val(data.total_order_without_delivery);
            } else if (data.success === false) {
            }
        }
    });
}

/*var frm=$('#comment-form');frm.submit(function(ev){$.ajax({type:frm.attr('method'),url:frm.attr('action'),data:frm.serialize(),
success:function(data){frm[0].reset();
$('#feedbackModal .modal-body').html('<div class="thanks-text" style="text-align:center;">Спасибо! <br/>Ваш отзыв успешно отправлен</div>');
$('#feedbackModal .modal-footer').html('');setTimeout(function(){$('#feedbackModal').modal('hide');},3000);}});ev.preventDefault();});
*/

$('.boxavatar_account .small.upload').on('click', function () {
    $('.avatar #profile-photo').click();
    return;
});
$('#petlist').bind('DOMSubtreeModified', function () {
    if ($('#petlist form').size() == 0) $('#petlist .info-msg').show(); else
        $('#petlist .info-msg').hide();
});
var formHasChanged = false;
var submitted = false;


$(document).on("change", '.avatar #profile-photo', function () {
    $('.boxavatar_account .small.upload').html('Изображение загружено');

    action = '/assets/components/office/updatephoto.php';
    form = $('#office-profile-form');
    var postData = new FormData(form[0]);
    $.ajax({
        type: "POST",
        enctype: 'multipart/form-data',
        processData: false,
        contentType: false,
        url: action,
        dataType: 'json',
        data: postData,
        success: function (response) {
            $('#office-profile-form').find('#profile-user-photo').attr('src', response.newphoto);
            $('#office-profile-form').find('.avatar input[name="photo"]').val(response.newphoto);
        }
    });

});
$(document).on("change", '#add_pet_form input[name="newphoto"]', function () {
    $('#add_pet_form .small.upload').html('Изображение загружено<br />Сохраните изменения');
});

$(document).on("change", '#petlist input[name="newphoto"]', function () {
    //$('#petlist .small.upload').html('Изображение загружено<br />Сохраните изменения');
    $(this).parent().parent().parent().find('button[type="submit"]').trigger('click');
});

$(document).on("change", '.modal-body input[name="newphoto"]', function () {
    $('.boxavatar_account .small.upload').html('Изображение загружено<br/> Сохраните изменения');
});
$(document).on("change", '.upload input[name="newphoto"]', function () {
    $(this).parent().find('.upload .file-upload-btn').html('Изображение загружено<br/> Сохраните изменения');
});

function getWarehouses(city_id, delivery_id, idwar) {
    $.get('/assets/components/delivery/getwarehouses.php', {
        'city_id': city_id,
        'lang_key': Polylang_key,
        'delivery_id': delivery_id,
        'idwar': idwar
    }, function (warehouses) {
        $('#warehouses').html(warehouses);
        setTimeout(function () {
            $('#deliveries').next().remove();
            $('#deliveries').customSelect('update');
            $('#warehouses').next().remove();
            $('#warehouses').customSelect('update');
            if (warehouses.length > 0) {
                $('#warehouse-block .custom-select-container').show();
                $('#warehouses').prop('disabled', false);
                $('#warehouse-block .with-errors').html('');
            } else {
                $('#warehouse-block .custom-select-container').hide();
                $('#warehouses').prop('disabled', true);
                $('#warehouse-block .with-errors').show();
                $('#warehouse-block .with-errors').html('В Вашем населенном пункте нет отделений. Выберите ближайший город');
            }
            $('.deliverieswrap .col-xs-8 .custom-select-container').removeClass('loading');
            $('#warehouse-block .col-xs-8 .custom-select-container').removeClass('loading');
        }, 1300);
    }, 'json');
}

$(document).ready(function () {
    $('#msOrder').on('click', '.hidedopkurier', function () {
        $('#courier-block2').addClass('hide');
        $('.showdopkurier').show();
    });
    $('#msOrder').on('click', '.showdopkurier', function () {
        $('#courier-block2').removeClass('hide');
        $('.showdopkurier').hide();
    });

    $('input[name=suggest_locality]').on('typeahead:open', function ($e) {
    });
    $('.seeallcom').on('click', function () {
        $('#productCardTab li').removeClass('active');
        $('.allcomments').addClass('active');
    });
    $('.card-costs-item a.askpr').on('click', function () {
        product = $(this).data('product');
        article = $(this).data('article');
        $('#boxStatus').find('#productask').val(product);
        $('#boxStatus').find('#articleask').val(article);
    });
    $('.product-item-tab-content a').on('click', function () {
        product = $(this).data('product');
        article = $(this).data('article');
        $('#boxStatus').find('#productask').val(product);
        $('#boxStatus').find('#articleask').val(article);
    });
    $('#boxlogin').on('submit', 'form.ajaxloginbox', function () {
        $.ajax({
            type: "POST",
            cache: false,
            url: "/account/login",
            data: $(this).serializeArray(),
            success: function (data) {
                var errMessage = $(data).find(".has-error .with-errors").text();
                if (errMessage == "") {
                    $(".form-group").removeClass('has-error');
                    $(".ajaxloginbox .with-errors").html('');
                    $(".ajaxloginbox .with-errors").removeClass('show');
                    location.reload();
                } else {
                    $(".ajaxloginbox .form-group").addClass('has-error');
                    $(".ajaxloginbox .with-errors").html('<p class="error">' + errMessage + '</p>');
                    $(".ajaxloginbox .with-errors").addClass('show');
                }
            }
        });
        return false;
    });
    $('.buttonsubscribe, #buttonsubscribe').on('click', function () {
        action = $(this).parent().attr('action');
        type = $(this).data('type');
        $.ajax({
            type: "POST", cache: false, url: action, data: $(this).parent().serialize(), success: function (response) {
                data = $.parseJSON(response);
                $(this).parent().parent().find('.messagesubscribe').html('');
                if (data.success) {

                    if (type == 'footer') {
                        dataLayer.push({
                            'event': 'event-GA',
                            'eventCategory': 'subscribe',
                            'eventAction': 'footer'
                        });
                    } else if (type == 'home') {
                        dataLayer.push({
                            'event': 'event-GA',
                            'eventCategory': 'subscribe',
                            'eventAction': 'main-page'
                        });
                    }
                    else if (type == 'sidebar') {
                        dataLayer.push({
                            'event': 'event-GA',
                            'eventCategory': 'subscribe',
                            'eventAction': 'sidebar'
                        });
                    }
                    $('#subscribeSuccessModal .modal-body').html(data.message);
                    $('#subscribeSuccessModal').modal('show');
                } else {
                    $('.messagesubscribe').html(data.message);
                }
            }
        });
        return false;
    });
    $('#boxregistr').on('submit', 'form.ajaxregistrbox', function () {
        $.ajax({
            type: "POST",
            cache: false,
            url: "/account/register",
            data: $(this).serializeArray(),
            success: function (data) {
                var errMessage = $(data).find(".has-error .with-errors").text();
                if (errMessage == "") {
                    $(".form-group").removeClass('has-error');
                    $(".ajaxregistrbox .with-errors").html('');
                    $(".ajaxregistrbox .with-errors").removeClass('show');
                    location.reload();
                } else {
                    var textform = $(data).find(".formaregistration .row .col-xs-8").html();
                    $(".ajaxregistrbox .modal-body").html(textform);
                    $("input[name='username']").mask("+389999999999");
                    $("input[name='phone']").mask("+389999999999");
                }
            }
        });
        return false;
    });
    $('.formreglabel').on('click', function () {
        if ($('.loginform').css('display') == 'none') {
            $('.loginform').show();
            $('.regformorder').hide();
            $('.formreglabel').html('Оформить без авторизации');
        } else {
            $('.regformorder').show();
            $('.loginform').hide();
            $('.formreglabel').html('Я зарегистрирован');
        }
    });
});

/*jquery-ui-1.10.4.custom.min.js*/
/*! jQuery UI - v1.10.4 - 2014-01-21
* http://jqueryui.com
* Includes: jquery.ui.core.js, jquery.ui.widget.js, jquery.ui.mouse.js, jquery.ui.position.js, jquery.ui.autocomplete.js, jquery.ui.menu.js, jquery.ui.slider.js
* Copyright 2014 jQuery Foundation and other contributors; Licensed MIT */

(function (e, t) {
    function i(t, i) {
        var s, a, o, r = t.nodeName.toLowerCase();
        return "area" === r ? (s = t.parentNode, a = s.name, t.href && a && "map" === s.nodeName.toLowerCase() ? (o = e("img[usemap=#" + a + "]")[0], !!o && n(o)) : !1) : (/input|select|textarea|button|object/.test(r) ? !t.disabled : "a" === r ? t.href || i : i) && n(t)
    }

    function n(t) {
        return e.expr.filters.visible(t) && !e(t).parents().addBack().filter(function () {
            return "hidden" === e.css(this, "visibility")
        }).length
    }

    var s = 0, a = /^ui-id-\d+$/;
    e.ui = e.ui || {}, e.extend(e.ui, {
        version: "1.10.4",
        keyCode: {
            BACKSPACE: 8,
            COMMA: 188,
            DELETE: 46,
            DOWN: 40,
            END: 35,
            ENTER: 13,
            ESCAPE: 27,
            HOME: 36,
            LEFT: 37,
            NUMPAD_ADD: 107,
            NUMPAD_DECIMAL: 110,
            NUMPAD_DIVIDE: 111,
            NUMPAD_ENTER: 108,
            NUMPAD_MULTIPLY: 106,
            NUMPAD_SUBTRACT: 109,
            PAGE_DOWN: 34,
            PAGE_UP: 33,
            PERIOD: 190,
            RIGHT: 39,
            SPACE: 32,
            TAB: 9,
            UP: 38
        }
    }), e.fn.extend({
        focus: function (t) {
            return function (i, n) {
                return "number" == typeof i ? this.each(function () {
                    var t = this;
                    setTimeout(function () {
                        e(t).focus(), n && n.call(t)
                    }, i)
                }) : t.apply(this, arguments)
            }
        }(e.fn.focus), scrollParent: function () {
            var t;
            return t = e.ui.ie && /(static|relative)/.test(this.css("position")) || /absolute/.test(this.css("position")) ? this.parents().filter(function () {
                return /(relative|absolute|fixed)/.test(e.css(this, "position")) && /(auto|scroll)/.test(e.css(this, "overflow") + e.css(this, "overflow-y") + e.css(this, "overflow-x"))
            }).eq(0) : this.parents().filter(function () {
                return /(auto|scroll)/.test(e.css(this, "overflow") + e.css(this, "overflow-y") + e.css(this, "overflow-x"))
            }).eq(0), /fixed/.test(this.css("position")) || !t.length ? e(document) : t
        }, zIndex: function (i) {
            if (i !== t) return this.css("zIndex", i);
            if (this.length) for (var n, s, a = e(this[0]); a.length && a[0] !== document;) {
                if (n = a.css("position"), ("absolute" === n || "relative" === n || "fixed" === n) && (s = parseInt(a.css("zIndex"), 10), !isNaN(s) && 0 !== s)) return s;
                a = a.parent()
            }
            return 0
        }, uniqueId: function () {
            return this.each(function () {
                this.id || (this.id = "ui-id-" + ++s)
            })
        }, removeUniqueId: function () {
            return this.each(function () {
                a.test(this.id) && e(this).removeAttr("id")
            })
        }
    }), e.extend(e.expr[":"], {
        data: e.expr.createPseudo ? e.expr.createPseudo(function (t) {
            return function (i) {
                return !!e.data(i, t)
            }
        }) : function (t, i, n) {
            return !!e.data(t, n[3])
        }, focusable: function (t) {
            return i(t, !isNaN(e.attr(t, "tabindex")))
        }, tabbable: function (t) {
            var n = e.attr(t, "tabindex"), s = isNaN(n);
            return (s || n >= 0) && i(t, !s)
        }
    }), e("<a>").outerWidth(1).jquery || e.each(["Width", "Height"], function (i, n) {
        function s(t, i, n, s) {
            return e.each(a, function () {
                i -= parseFloat(e.css(t, "padding" + this)) || 0, n && (i -= parseFloat(e.css(t, "border" + this + "Width")) || 0), s && (i -= parseFloat(e.css(t, "margin" + this)) || 0)
            }), i
        }

        var a = "Width" === n ? ["Left", "Right"] : ["Top", "Bottom"], o = n.toLowerCase(), r = {
            innerWidth: e.fn.innerWidth,
            innerHeight: e.fn.innerHeight,
            outerWidth: e.fn.outerWidth,
            outerHeight: e.fn.outerHeight
        };
        e.fn["inner" + n] = function (i) {
            return i === t ? r["inner" + n].call(this) : this.each(function () {
                e(this).css(o, s(this, i) + "px")
            })
        }, e.fn["outer" + n] = function (t, i) {
            return "number" != typeof t ? r["outer" + n].call(this, t) : this.each(function () {
                e(this).css(o, s(this, t, !0, i) + "px")
            })
        }
    }), e.fn.addBack || (e.fn.addBack = function (e) {
        return this.add(null == e ? this.prevObject : this.prevObject.filter(e))
    }), e("<a>").data("a-b", "a").removeData("a-b").data("a-b") && (e.fn.removeData = function (t) {
        return function (i) {
            return arguments.length ? t.call(this, e.camelCase(i)) : t.call(this)
        }
    }(e.fn.removeData)), e.ui.ie = !!/msie [\w.]+/.exec(navigator.userAgent.toLowerCase()), e.support.selectstart = "onselectstart" in document.createElement("div"), e.fn.extend({
        disableSelection: function () {
            return this.bind((e.support.selectstart ? "selectstart" : "mousedown") + ".ui-disableSelection", function (e) {
                e.preventDefault()
            })
        }, enableSelection: function () {
            return this.unbind(".ui-disableSelection")
        }
    }), e.extend(e.ui, {
        plugin: {
            add: function (t, i, n) {
                var s, a = e.ui[t].prototype;
                for (s in n) a.plugins[s] = a.plugins[s] || [], a.plugins[s].push([i, n[s]])
            }, call: function (e, t, i) {
                var n, s = e.plugins[t];
                if (s && e.element[0].parentNode && 11 !== e.element[0].parentNode.nodeType) for (n = 0; s.length > n; n++) e.options[s[n][0]] && s[n][1].apply(e.element, i)
            }
        }, hasScroll: function (t, i) {
            if ("hidden" === e(t).css("overflow")) return !1;
            var n = i && "left" === i ? "scrollLeft" : "scrollTop", s = !1;
            return t[n] > 0 ? !0 : (t[n] = 1, s = t[n] > 0, t[n] = 0, s)
        }
    })
})(jQuery);
(function (t, e) {
    var i = 0, s = Array.prototype.slice, n = t.cleanData;
    t.cleanData = function (e) {
        for (var i, s = 0; null != (i = e[s]); s++) try {
            t(i).triggerHandler("remove")
        } catch (o) {
        }
        n(e)
    }, t.widget = function (i, s, n) {
        var o, a, r, h, l = {}, c = i.split(".")[0];
        i = i.split(".")[1], o = c + "-" + i, n || (n = s, s = t.Widget), t.expr[":"][o.toLowerCase()] = function (e) {
            return !!t.data(e, o)
        }, t[c] = t[c] || {}, a = t[c][i], r = t[c][i] = function (t, i) {
            return this._createWidget ? (arguments.length && this._createWidget(t, i), e) : new r(t, i)
        }, t.extend(r, a, {
            version: n.version,
            _proto: t.extend({}, n),
            _childConstructors: []
        }), h = new s, h.options = t.widget.extend({}, h.options), t.each(n, function (i, n) {
            return t.isFunction(n) ? (l[i] = function () {
                var t = function () {
                    return s.prototype[i].apply(this, arguments)
                }, e = function (t) {
                    return s.prototype[i].apply(this, t)
                };
                return function () {
                    var i, s = this._super, o = this._superApply;
                    return this._super = t, this._superApply = e, i = n.apply(this, arguments), this._super = s, this._superApply = o, i
                }
            }(), e) : (l[i] = n, e)
        }), r.prototype = t.widget.extend(h, {widgetEventPrefix: a ? h.widgetEventPrefix || i : i}, l, {
            constructor: r,
            namespace: c,
            widgetName: i,
            widgetFullName: o
        }), a ? (t.each(a._childConstructors, function (e, i) {
            var s = i.prototype;
            t.widget(s.namespace + "." + s.widgetName, r, i._proto)
        }), delete a._childConstructors) : s._childConstructors.push(r), t.widget.bridge(i, r)
    }, t.widget.extend = function (i) {
        for (var n, o, a = s.call(arguments, 1), r = 0, h = a.length; h > r; r++) for (n in a[r]) o = a[r][n], a[r].hasOwnProperty(n) && o !== e && (i[n] = t.isPlainObject(o) ? t.isPlainObject(i[n]) ? t.widget.extend({}, i[n], o) : t.widget.extend({}, o) : o);
        return i
    }, t.widget.bridge = function (i, n) {
        var o = n.prototype.widgetFullName || i;
        t.fn[i] = function (a) {
            var r = "string" == typeof a, h = s.call(arguments, 1), l = this;
            return a = !r && h.length ? t.widget.extend.apply(null, [a].concat(h)) : a, r ? this.each(function () {
                var s, n = t.data(this, o);
                return n ? t.isFunction(n[a]) && "_" !== a.charAt(0) ? (s = n[a].apply(n, h), s !== n && s !== e ? (l = s && s.jquery ? l.pushStack(s.get()) : s, !1) : e) : t.error("no such method '" + a + "' for " + i + " widget instance") : t.error("cannot call methods on " + i + " prior to initialization; " + "attempted to call method '" + a + "'")
            }) : this.each(function () {
                var e = t.data(this, o);
                e ? e.option(a || {})._init() : t.data(this, o, new n(a, this))
            }), l
        }
    }, t.Widget = function () {
    }, t.Widget._childConstructors = [], t.Widget.prototype = {
        widgetName: "widget",
        widgetEventPrefix: "",
        defaultElement: "<div>",
        options: {disabled: !1, create: null},
        _createWidget: function (e, s) {
            s = t(s || this.defaultElement || this)[0], this.element = t(s), this.uuid = i++, this.eventNamespace = "." + this.widgetName + this.uuid, this.options = t.widget.extend({}, this.options, this._getCreateOptions(), e), this.bindings = t(), this.hoverable = t(), this.focusable = t(), s !== this && (t.data(s, this.widgetFullName, this), this._on(!0, this.element, {
                remove: function (t) {
                    t.target === s && this.destroy()
                }
            }), this.document = t(s.style ? s.ownerDocument : s.document || s), this.window = t(this.document[0].defaultView || this.document[0].parentWindow)), this._create(), this._trigger("create", null, this._getCreateEventData()), this._init()
        },
        _getCreateOptions: t.noop,
        _getCreateEventData: t.noop,
        _create: t.noop,
        _init: t.noop,
        destroy: function () {
            this._destroy(), this.element.unbind(this.eventNamespace).removeData(this.widgetName).removeData(this.widgetFullName).removeData(t.camelCase(this.widgetFullName)), this.widget().unbind(this.eventNamespace).removeAttr("aria-disabled").removeClass(this.widgetFullName + "-disabled " + "ui-state-disabled"), this.bindings.unbind(this.eventNamespace), this.hoverable.removeClass("ui-state-hover"), this.focusable.removeClass("ui-state-focus")
        },
        _destroy: t.noop,
        widget: function () {
            return this.element
        },
        option: function (i, s) {
            var n, o, a, r = i;
            if (0 === arguments.length) return t.widget.extend({}, this.options);
            if ("string" == typeof i) if (r = {}, n = i.split("."), i = n.shift(), n.length) {
                for (o = r[i] = t.widget.extend({}, this.options[i]), a = 0; n.length - 1 > a; a++) o[n[a]] = o[n[a]] || {}, o = o[n[a]];
                if (i = n.pop(), 1 === arguments.length) return o[i] === e ? null : o[i];
                o[i] = s
            } else {
                if (1 === arguments.length) return this.options[i] === e ? null : this.options[i];
                r[i] = s
            }
            return this._setOptions(r), this
        },
        _setOptions: function (t) {
            var e;
            for (e in t) this._setOption(e, t[e]);
            return this
        },
        _setOption: function (t, e) {
            return this.options[t] = e, "disabled" === t && (this.widget().toggleClass(this.widgetFullName + "-disabled ui-state-disabled", !!e).attr("aria-disabled", e), this.hoverable.removeClass("ui-state-hover"), this.focusable.removeClass("ui-state-focus")), this
        },
        enable: function () {
            return this._setOption("disabled", !1)
        },
        disable: function () {
            return this._setOption("disabled", !0)
        },
        _on: function (i, s, n) {
            var o, a = this;
            "boolean" != typeof i && (n = s, s = i, i = !1), n ? (s = o = t(s), this.bindings = this.bindings.add(s)) : (n = s, s = this.element, o = this.widget()), t.each(n, function (n, r) {
                function h() {
                    return i || a.options.disabled !== !0 && !t(this).hasClass("ui-state-disabled") ? ("string" == typeof r ? a[r] : r).apply(a, arguments) : e
                }

                "string" != typeof r && (h.guid = r.guid = r.guid || h.guid || t.guid++);
                var l = n.match(/^(\w+)\s*(.*)$/), c = l[1] + a.eventNamespace, u = l[2];
                u ? o.delegate(u, c, h) : s.bind(c, h)
            })
        },
        _off: function (t, e) {
            e = (e || "").split(" ").join(this.eventNamespace + " ") + this.eventNamespace, t.unbind(e).undelegate(e)
        },
        _delay: function (t, e) {
            function i() {
                return ("string" == typeof t ? s[t] : t).apply(s, arguments)
            }

            var s = this;
            return setTimeout(i, e || 0)
        },
        _hoverable: function (e) {
            this.hoverable = this.hoverable.add(e), this._on(e, {
                mouseenter: function (e) {
                    t(e.currentTarget).addClass("ui-state-hover")
                }, mouseleave: function (e) {
                    t(e.currentTarget).removeClass("ui-state-hover")
                }
            })
        },
        _focusable: function (e) {
            this.focusable = this.focusable.add(e), this._on(e, {
                focusin: function (e) {
                    t(e.currentTarget).addClass("ui-state-focus")
                }, focusout: function (e) {
                    t(e.currentTarget).removeClass("ui-state-focus")
                }
            })
        },
        _trigger: function (e, i, s) {
            var n, o, a = this.options[e];
            if (s = s || {}, i = t.Event(i), i.type = (e === this.widgetEventPrefix ? e : this.widgetEventPrefix + e).toLowerCase(), i.target = this.element[0], o = i.originalEvent) for (n in o) n in i || (i[n] = o[n]);
            return this.element.trigger(i, s), !(t.isFunction(a) && a.apply(this.element[0], [i].concat(s)) === !1 || i.isDefaultPrevented())
        }
    }, t.each({show: "fadeIn", hide: "fadeOut"}, function (e, i) {
        t.Widget.prototype["_" + e] = function (s, n, o) {
            "string" == typeof n && (n = {effect: n});
            var a, r = n ? n === !0 || "number" == typeof n ? i : n.effect || i : e;
            n = n || {}, "number" == typeof n && (n = {duration: n}), a = !t.isEmptyObject(n), n.complete = o, n.delay && s.delay(n.delay), a && t.effects && t.effects.effect[r] ? s[e](n) : r !== e && s[r] ? s[r](n.duration, n.easing, o) : s.queue(function (i) {
                t(this)[e](), o && o.call(s[0]), i()
            })
        }
    })
})(jQuery);
(function (t) {
    var e = !1;
    t(document).mouseup(function () {
        e = !1
    }), t.widget("ui.mouse", {
        version: "1.10.4",
        options: {cancel: "input,textarea,button,select,option", distance: 1, delay: 0},
        _mouseInit: function () {
            var e = this;
            this.element.bind("mousedown." + this.widgetName, function (t) {
                return e._mouseDown(t)
            }).bind("click." + this.widgetName, function (i) {
                return !0 === t.data(i.target, e.widgetName + ".preventClickEvent") ? (t.removeData(i.target, e.widgetName + ".preventClickEvent"), i.stopImmediatePropagation(), !1) : undefined
            }), this.started = !1
        },
        _mouseDestroy: function () {
            this.element.unbind("." + this.widgetName), this._mouseMoveDelegate && t(document).unbind("mousemove." + this.widgetName, this._mouseMoveDelegate).unbind("mouseup." + this.widgetName, this._mouseUpDelegate)
        },
        _mouseDown: function (i) {
            if (!e) {
                this._mouseStarted && this._mouseUp(i), this._mouseDownEvent = i;
                var s = this, n = 1 === i.which,
                    a = "string" == typeof this.options.cancel && i.target.nodeName ? t(i.target).closest(this.options.cancel).length : !1;
                return n && !a && this._mouseCapture(i) ? (this.mouseDelayMet = !this.options.delay, this.mouseDelayMet || (this._mouseDelayTimer = setTimeout(function () {
                    s.mouseDelayMet = !0
                }, this.options.delay)), this._mouseDistanceMet(i) && this._mouseDelayMet(i) && (this._mouseStarted = this._mouseStart(i) !== !1, !this._mouseStarted) ? (i.preventDefault(), !0) : (!0 === t.data(i.target, this.widgetName + ".preventClickEvent") && t.removeData(i.target, this.widgetName + ".preventClickEvent"), this._mouseMoveDelegate = function (t) {
                    return s._mouseMove(t)
                }, this._mouseUpDelegate = function (t) {
                    return s._mouseUp(t)
                }, t(document).bind("mousemove." + this.widgetName, this._mouseMoveDelegate).bind("mouseup." + this.widgetName, this._mouseUpDelegate), i.preventDefault(), e = !0, !0)) : !0
            }
        },
        _mouseMove: function (e) {
            return t.ui.ie && (!document.documentMode || 9 > document.documentMode) && !e.button ? this._mouseUp(e) : this._mouseStarted ? (this._mouseDrag(e), e.preventDefault()) : (this._mouseDistanceMet(e) && this._mouseDelayMet(e) && (this._mouseStarted = this._mouseStart(this._mouseDownEvent, e) !== !1, this._mouseStarted ? this._mouseDrag(e) : this._mouseUp(e)), !this._mouseStarted)
        },
        _mouseUp: function (e) {
            return t(document).unbind("mousemove." + this.widgetName, this._mouseMoveDelegate).unbind("mouseup." + this.widgetName, this._mouseUpDelegate), this._mouseStarted && (this._mouseStarted = !1, e.target === this._mouseDownEvent.target && t.data(e.target, this.widgetName + ".preventClickEvent", !0), this._mouseStop(e)), !1
        },
        _mouseDistanceMet: function (t) {
            return Math.max(Math.abs(this._mouseDownEvent.pageX - t.pageX), Math.abs(this._mouseDownEvent.pageY - t.pageY)) >= this.options.distance
        },
        _mouseDelayMet: function () {
            return this.mouseDelayMet
        },
        _mouseStart: function () {
        },
        _mouseDrag: function () {
        },
        _mouseStop: function () {
        },
        _mouseCapture: function () {
            return !0
        }
    })
})(jQuery);
(function (t, e) {
    function i(t, e, i) {
        return [parseFloat(t[0]) * (p.test(t[0]) ? e / 100 : 1), parseFloat(t[1]) * (p.test(t[1]) ? i / 100 : 1)]
    }

    function s(e, i) {
        return parseInt(t.css(e, i), 10) || 0
    }

    function n(e) {
        var i = e[0];
        return 9 === i.nodeType ? {
            width: e.width(),
            height: e.height(),
            offset: {top: 0, left: 0}
        } : t.isWindow(i) ? {
            width: e.width(),
            height: e.height(),
            offset: {top: e.scrollTop(), left: e.scrollLeft()}
        } : i.preventDefault ? {width: 0, height: 0, offset: {top: i.pageY, left: i.pageX}} : {
            width: e.outerWidth(),
            height: e.outerHeight(),
            offset: e.offset()
        }
    }

    t.ui = t.ui || {};
    var a, o = Math.max, r = Math.abs, l = Math.round, h = /left|center|right/, c = /top|center|bottom/,
        u = /[\+\-]\d+(\.[\d]+)?%?/, d = /^\w+/, p = /%$/, f = t.fn.position;
    t.position = {
        scrollbarWidth: function () {
            if (a !== e) return a;
            var i, s,
                n = t("<div style='display:block;position:absolute;width:50px;height:50px;overflow:hidden;'><div style='height:100px;width:auto;'></div></div>"),
                o = n.children()[0];
            return t("body").append(n), i = o.offsetWidth, n.css("overflow", "scroll"), s = o.offsetWidth, i === s && (s = n[0].clientWidth), n.remove(), a = i - s
        }, getScrollInfo: function (e) {
            var i = e.isWindow || e.isDocument ? "" : e.element.css("overflow-x"),
                s = e.isWindow || e.isDocument ? "" : e.element.css("overflow-y"),
                n = "scroll" === i || "auto" === i && e.width < e.element[0].scrollWidth,
                a = "scroll" === s || "auto" === s && e.height < e.element[0].scrollHeight;
            return {width: a ? t.position.scrollbarWidth() : 0, height: n ? t.position.scrollbarWidth() : 0}
        }, getWithinInfo: function (e) {
            var i = t(e || window), s = t.isWindow(i[0]), n = !!i[0] && 9 === i[0].nodeType;
            return {
                element: i,
                isWindow: s,
                isDocument: n,
                offset: i.offset() || {left: 0, top: 0},
                scrollLeft: i.scrollLeft(),
                scrollTop: i.scrollTop(),
                width: s ? i.width() : i.outerWidth(),
                height: s ? i.height() : i.outerHeight()
            }
        }
    }, t.fn.position = function (e) {
        if (!e || !e.of) return f.apply(this, arguments);
        e = t.extend({}, e);
        var a, p, g, m, v, _, b = t(e.of), y = t.position.getWithinInfo(e.within), k = t.position.getScrollInfo(y),
            w = (e.collision || "flip").split(" "), D = {};
        return _ = n(b), b[0].preventDefault && (e.at = "left top"), p = _.width, g = _.height, m = _.offset, v = t.extend({}, m), t.each(["my", "at"], function () {
            var t, i, s = (e[this] || "").split(" ");
            1 === s.length && (s = h.test(s[0]) ? s.concat(["center"]) : c.test(s[0]) ? ["center"].concat(s) : ["center", "center"]), s[0] = h.test(s[0]) ? s[0] : "center", s[1] = c.test(s[1]) ? s[1] : "center", t = u.exec(s[0]), i = u.exec(s[1]), D[this] = [t ? t[0] : 0, i ? i[0] : 0], e[this] = [d.exec(s[0])[0], d.exec(s[1])[0]]
        }), 1 === w.length && (w[1] = w[0]), "right" === e.at[0] ? v.left += p : "center" === e.at[0] && (v.left += p / 2), "bottom" === e.at[1] ? v.top += g : "center" === e.at[1] && (v.top += g / 2), a = i(D.at, p, g), v.left += a[0], v.top += a[1], this.each(function () {
            var n, h, c = t(this), u = c.outerWidth(), d = c.outerHeight(), f = s(this, "marginLeft"),
                _ = s(this, "marginTop"), x = u + f + s(this, "marginRight") + k.width,
                C = d + _ + s(this, "marginBottom") + k.height, M = t.extend({}, v),
                T = i(D.my, c.outerWidth(), c.outerHeight());
            "right" === e.my[0] ? M.left -= u : "center" === e.my[0] && (M.left -= u / 2), "bottom" === e.my[1] ? M.top -= d : "center" === e.my[1] && (M.top -= d / 2), M.left += T[0], M.top += T[1], t.support.offsetFractions || (M.left = l(M.left), M.top = l(M.top)), n = {
                marginLeft: f,
                marginTop: _
            }, t.each(["left", "top"], function (i, s) {
                t.ui.position[w[i]] && t.ui.position[w[i]][s](M, {
                    targetWidth: p,
                    targetHeight: g,
                    elemWidth: u,
                    elemHeight: d,
                    collisionPosition: n,
                    collisionWidth: x,
                    collisionHeight: C,
                    offset: [a[0] + T[0], a[1] + T[1]],
                    my: e.my,
                    at: e.at,
                    within: y,
                    elem: c
                })
            }), e.using && (h = function (t) {
                var i = m.left - M.left, s = i + p - u, n = m.top - M.top, a = n + g - d, l = {
                    target: {element: b, left: m.left, top: m.top, width: p, height: g},
                    element: {element: c, left: M.left, top: M.top, width: u, height: d},
                    horizontal: 0 > s ? "left" : i > 0 ? "right" : "center",
                    vertical: 0 > a ? "top" : n > 0 ? "bottom" : "middle"
                };
                u > p && p > r(i + s) && (l.horizontal = "center"), d > g && g > r(n + a) && (l.vertical = "middle"), l.important = o(r(i), r(s)) > o(r(n), r(a)) ? "horizontal" : "vertical", e.using.call(this, t, l)
            }), c.offset(t.extend(M, {using: h}))
        })
    }, t.ui.position = {
        fit: {
            left: function (t, e) {
                var i, s = e.within, n = s.isWindow ? s.scrollLeft : s.offset.left, a = s.width,
                    r = t.left - e.collisionPosition.marginLeft, l = n - r, h = r + e.collisionWidth - a - n;
                e.collisionWidth > a ? l > 0 && 0 >= h ? (i = t.left + l + e.collisionWidth - a - n, t.left += l - i) : t.left = h > 0 && 0 >= l ? n : l > h ? n + a - e.collisionWidth : n : l > 0 ? t.left += l : h > 0 ? t.left -= h : t.left = o(t.left - r, t.left)
            }, top: function (t, e) {
                var i, s = e.within, n = s.isWindow ? s.scrollTop : s.offset.top, a = e.within.height,
                    r = t.top - e.collisionPosition.marginTop, l = n - r, h = r + e.collisionHeight - a - n;
                e.collisionHeight > a ? l > 0 && 0 >= h ? (i = t.top + l + e.collisionHeight - a - n, t.top += l - i) : t.top = h > 0 && 0 >= l ? n : l > h ? n + a - e.collisionHeight : n : l > 0 ? t.top += l : h > 0 ? t.top -= h : t.top = o(t.top - r, t.top)
            }
        }, flip: {
            left: function (t, e) {
                var i, s, n = e.within, a = n.offset.left + n.scrollLeft, o = n.width,
                    l = n.isWindow ? n.scrollLeft : n.offset.left, h = t.left - e.collisionPosition.marginLeft,
                    c = h - l, u = h + e.collisionWidth - o - l,
                    d = "left" === e.my[0] ? -e.elemWidth : "right" === e.my[0] ? e.elemWidth : 0,
                    p = "left" === e.at[0] ? e.targetWidth : "right" === e.at[0] ? -e.targetWidth : 0,
                    f = -2 * e.offset[0];
                0 > c ? (i = t.left + d + p + f + e.collisionWidth - o - a, (0 > i || r(c) > i) && (t.left += d + p + f)) : u > 0 && (s = t.left - e.collisionPosition.marginLeft + d + p + f - l, (s > 0 || u > r(s)) && (t.left += d + p + f))
            }, top: function (t, e) {
                var i, s, n = e.within, a = n.offset.top + n.scrollTop, o = n.height,
                    l = n.isWindow ? n.scrollTop : n.offset.top, h = t.top - e.collisionPosition.marginTop, c = h - l,
                    u = h + e.collisionHeight - o - l, d = "top" === e.my[1],
                    p = d ? -e.elemHeight : "bottom" === e.my[1] ? e.elemHeight : 0,
                    f = "top" === e.at[1] ? e.targetHeight : "bottom" === e.at[1] ? -e.targetHeight : 0,
                    g = -2 * e.offset[1];
                0 > c ? (s = t.top + p + f + g + e.collisionHeight - o - a, t.top + p + f + g > c && (0 > s || r(c) > s) && (t.top += p + f + g)) : u > 0 && (i = t.top - e.collisionPosition.marginTop + p + f + g - l, t.top + p + f + g > u && (i > 0 || u > r(i)) && (t.top += p + f + g))
            }
        }, flipfit: {
            left: function () {
                t.ui.position.flip.left.apply(this, arguments), t.ui.position.fit.left.apply(this, arguments)
            }, top: function () {
                t.ui.position.flip.top.apply(this, arguments), t.ui.position.fit.top.apply(this, arguments)
            }
        }
    }, function () {
        var e, i, s, n, a, o = document.getElementsByTagName("body")[0], r = document.createElement("div");
        e = document.createElement(o ? "div" : "body"), s = {
            visibility: "hidden",
            width: 0,
            height: 0,
            border: 0,
            margin: 0,
            background: "none"
        }, o && t.extend(s, {position: "absolute", left: "-1000px", top: "-1000px"});
        for (a in s) e.style[a] = s[a];
        e.appendChild(r), i = o || document.documentElement, i.insertBefore(e, i.firstChild), r.style.cssText = "position: absolute; left: 10.7432222px;", n = t(r).offset().left, t.support.offsetFractions = n > 10 && 11 > n, e.innerHTML = "", i.removeChild(e)
    }()
})(jQuery);
(function (e) {
    e.widget("ui.autocomplete", {
        version: "1.10.4",
        defaultElement: "<input>",
        options: {
            appendTo: null,
            autoFocus: !1,
            delay: 300,
            minLength: 1,
            position: {my: "left top", at: "left bottom", collision: "none"},
            source: null,
            change: null,
            close: null,
            focus: null,
            open: null,
            response: null,
            search: null,
            select: null
        },
        requestIndex: 0,
        pending: 0,
        _create: function () {
            var t, i, s, n = this.element[0].nodeName.toLowerCase(), a = "textarea" === n, o = "input" === n;
            this.isMultiLine = a ? !0 : o ? !1 : this.element.prop("isContentEditable"), this.valueMethod = this.element[a || o ? "val" : "text"], this.isNewMenu = !0, this.element.addClass("ui-autocomplete-input").attr("autocomplete", "off"), this._on(this.element, {
                keydown: function (n) {
                    if (this.element.prop("readOnly")) return t = !0, s = !0, i = !0, undefined;
                    t = !1, s = !1, i = !1;
                    var a = e.ui.keyCode;
                    switch (n.keyCode) {
                        case a.PAGE_UP:
                            t = !0, this._move("previousPage", n);
                            break;
                        case a.PAGE_DOWN:
                            t = !0, this._move("nextPage", n);
                            break;
                        case a.UP:
                            t = !0, this._keyEvent("previous", n);
                            break;
                        case a.DOWN:
                            t = !0, this._keyEvent("next", n);
                            break;
                        case a.ENTER:
                        case a.NUMPAD_ENTER:
                            this.menu.active && (t = !0, n.preventDefault(), this.menu.select(n));
                            break;
                        case a.TAB:
                            this.menu.active && this.menu.select(n);
                            break;
                        case a.ESCAPE:
                            this.menu.element.is(":visible") && (this._value(this.term), this.close(n), n.preventDefault());
                            break;
                        default:
                            i = !0, this._searchTimeout(n)
                    }
                }, keypress: function (s) {
                    if (t) return t = !1, (!this.isMultiLine || this.menu.element.is(":visible")) && s.preventDefault(), undefined;
                    if (!i) {
                        var n = e.ui.keyCode;
                        switch (s.keyCode) {
                            case n.PAGE_UP:
                                this._move("previousPage", s);
                                break;
                            case n.PAGE_DOWN:
                                this._move("nextPage", s);
                                break;
                            case n.UP:
                                this._keyEvent("previous", s);
                                break;
                            case n.DOWN:
                                this._keyEvent("next", s)
                        }
                    }
                }, input: function (e) {
                    return s ? (s = !1, e.preventDefault(), undefined) : (this._searchTimeout(e), undefined)
                }, focus: function () {
                    this.selectedItem = null, this.previous = this._value()
                }, blur: function (e) {
                    return this.cancelBlur ? (delete this.cancelBlur, undefined) : (clearTimeout(this.searching), this.close(e), this._change(e), undefined)
                }
            }), this._initSource(), this.menu = e("<ul>").addClass("ui-autocomplete ui-front").appendTo(this._appendTo()).menu({role: null}).hide().data("ui-menu"), this._on(this.menu.element, {
                mousedown: function (t) {
                    t.preventDefault(), this.cancelBlur = !0, this._delay(function () {
                        delete this.cancelBlur
                    });
                    var i = this.menu.element[0];
                    e(t.target).closest(".ui-menu-item").length || this._delay(function () {
                        var t = this;
                        this.document.one("mousedown", function (s) {
                            s.target === t.element[0] || s.target === i || e.contains(i, s.target) || t.close()
                        })
                    })
                }, menufocus: function (t, i) {
                    if (this.isNewMenu && (this.isNewMenu = !1, t.originalEvent && /^mouse/.test(t.originalEvent.type))) return this.menu.blur(), this.document.one("mousemove", function () {
                        e(t.target).trigger(t.originalEvent)
                    }), undefined;
                    var s = i.item.data("ui-autocomplete-item");
                    !1 !== this._trigger("focus", t, {item: s}) ? t.originalEvent && /^key/.test(t.originalEvent.type) && this._value(s.value) : this.liveRegion.text(s.value)
                }, menuselect: function (e, t) {
                    var i = t.item.data("ui-autocomplete-item"), s = this.previous;
                    this.element[0] !== this.document[0].activeElement && (this.element.focus(), this.previous = s, this._delay(function () {
                        this.previous = s, this.selectedItem = i
                    })), !1 !== this._trigger("select", e, {item: i}) && this._value(i.value), this.term = this._value(), this.close(e), this.selectedItem = i
                }
            }), this.liveRegion = e("<span>", {
                role: "status",
                "aria-live": "polite"
            }).addClass("ui-helper-hidden-accessible").insertBefore(this.element), this._on(this.window, {
                beforeunload: function () {
                    this.element.removeAttr("autocomplete")
                }
            })
        },
        _destroy: function () {
            clearTimeout(this.searching), this.element.removeClass("ui-autocomplete-input").removeAttr("autocomplete"), this.menu.element.remove(), this.liveRegion.remove()
        },
        _setOption: function (e, t) {
            this._super(e, t), "source" === e && this._initSource(), "appendTo" === e && this.menu.element.appendTo(this._appendTo()), "disabled" === e && t && this.xhr && this.xhr.abort()
        },
        _appendTo: function () {
            var t = this.options.appendTo;
            return t && (t = t.jquery || t.nodeType ? e(t) : this.document.find(t).eq(0)), t || (t = this.element.closest(".ui-front")), t.length || (t = this.document[0].body), t
        },
        _initSource: function () {
            var t, i, s = this;
            e.isArray(this.options.source) ? (t = this.options.source, this.source = function (i, s) {
                s(e.ui.autocomplete.filter(t, i.term))
            }) : "string" == typeof this.options.source ? (i = this.options.source, this.source = function (t, n) {
                s.xhr && s.xhr.abort(), s.xhr = e.ajax({
                    url: i, data: t, dataType: "json", success: function (e) {
                        n(e)
                    }, error: function () {
                        n([])
                    }
                })
            }) : this.source = this.options.source
        },
        _searchTimeout: function (e) {
            clearTimeout(this.searching), this.searching = this._delay(function () {
                this.term !== this._value() && (this.selectedItem = null, this.search(null, e))
            }, this.options.delay)
        },
        search: function (e, t) {
            return e = null != e ? e : this._value(), this.term = this._value(), e.length < this.options.minLength ? this.close(t) : this._trigger("search", t) !== !1 ? this._search(e) : undefined
        },
        _search: function (e) {
            this.pending++, this.element.addClass("ui-autocomplete-loading"), this.cancelSearch = !1, this.source({term: e}, this._response())
        },
        _response: function () {
            var t = ++this.requestIndex;
            return e.proxy(function (e) {
                t === this.requestIndex && this.__response(e), this.pending--, this.pending || this.element.removeClass("ui-autocomplete-loading")
            }, this)
        },
        __response: function (e) {
            e && (e = this._normalize(e)), this._trigger("response", null, {content: e}), !this.options.disabled && e && e.length && !this.cancelSearch ? (this._suggest(e), this._trigger("open")) : this._close()
        },
        close: function (e) {
            this.cancelSearch = !0, this._close(e)
        },
        _close: function (e) {
            this.menu.element.is(":visible") && (this.menu.element.hide(), this.menu.blur(), this.isNewMenu = !0, this._trigger("close", e))
        },
        _change: function (e) {
            this.previous !== this._value() && this._trigger("change", e, {item: this.selectedItem})
        },
        _normalize: function (t) {
            return t.length && t[0].label && t[0].value ? t : e.map(t, function (t) {
                return "string" == typeof t ? {label: t, value: t} : e.extend({
                    label: t.label || t.value,
                    value: t.value || t.label
                }, t)
            })
        },
        _suggest: function (t) {
            var i = this.menu.element.empty();
            this._renderMenu(i, t), this.isNewMenu = !0, this.menu.refresh(), i.show(), this._resizeMenu(), i.position(e.extend({of: this.element}, this.options.position)), this.options.autoFocus && this.menu.next()
        },
        _resizeMenu: function () {
            var e = this.menu.element;
            e.outerWidth(Math.max(e.width("").outerWidth() + 1, this.element.outerWidth()))
        },
        _renderMenu: function (t, i) {
            var s = this;
            e.each(i, function (e, i) {
                s._renderItemData(t, i)
            })
        },
        _renderItemData: function (e, t) {
            return this._renderItem(e, t).data("ui-autocomplete-item", t)
        },
        _renderItem: function (t, i) {
            return e("<li>").append(e("<a>").text(i.label)).appendTo(t)
        },
        _move: function (e, t) {
            return this.menu.element.is(":visible") ? this.menu.isFirstItem() && /^previous/.test(e) || this.menu.isLastItem() && /^next/.test(e) ? (this._value(this.term), this.menu.blur(), undefined) : (this.menu[e](t), undefined) : (this.search(null, t), undefined)
        },
        widget: function () {
            return this.menu.element
        },
        _value: function () {
            return this.valueMethod.apply(this.element, arguments)
        },
        _keyEvent: function (e, t) {
            (!this.isMultiLine || this.menu.element.is(":visible")) && (this._move(e, t), t.preventDefault())
        }
    }), e.extend(e.ui.autocomplete, {
        escapeRegex: function (e) {
            return e.replace(/[\-\[\]{}()*+?.,\\\^$|#\s]/g, "\\$&")
        }, filter: function (t, i) {
            var s = RegExp(e.ui.autocomplete.escapeRegex(i), "i");
            return e.grep(t, function (e) {
                return s.test(e.label || e.value || e)
            })
        }
    }), e.widget("ui.autocomplete", e.ui.autocomplete, {
        options: {
            messages: {
                noResults: "No search results.",
                results: function (e) {
                    return e + (e > 1 ? " results are" : " result is") + " available, use up and down arrow keys to navigate."
                }
            }
        }, __response: function (e) {
            var t;
            this._superApply(arguments), this.options.disabled || this.cancelSearch || (t = e && e.length ? this.options.messages.results(e.length) : this.options.messages.noResults, this.liveRegion.text(t))
        }
    })
})(jQuery);
(function (t) {
    t.widget("ui.menu", {
        version: "1.10.4",
        defaultElement: "<ul>",
        delay: 300,
        options: {
            icons: {submenu: "ui-icon-carat-1-e"},
            menus: "ul",
            position: {my: "left top", at: "right top"},
            role: "menu",
            blur: null,
            focus: null,
            select: null
        },
        _create: function () {
            this.activeMenu = this.element, this.mouseHandled = !1, this.element.uniqueId().addClass("ui-menu ui-widget ui-widget-content ui-corner-all").toggleClass("ui-menu-icons", !!this.element.find(".ui-icon").length).attr({
                role: this.options.role,
                tabIndex: 0
            }).bind("click" + this.eventNamespace, t.proxy(function (t) {
                this.options.disabled && t.preventDefault()
            }, this)), this.options.disabled && this.element.addClass("ui-state-disabled").attr("aria-disabled", "true"), this._on({
                "mousedown .ui-menu-item > a": function (t) {
                    t.preventDefault()
                }, "click .ui-state-disabled > a": function (t) {
                    t.preventDefault()
                }, "click .ui-menu-item:has(a)": function (e) {
                    var i = t(e.target).closest(".ui-menu-item");
                    !this.mouseHandled && i.not(".ui-state-disabled").length && (this.select(e), e.isPropagationStopped() || (this.mouseHandled = !0), i.has(".ui-menu").length ? this.expand(e) : !this.element.is(":focus") && t(this.document[0].activeElement).closest(".ui-menu").length && (this.element.trigger("focus", [!0]), this.active && 1 === this.active.parents(".ui-menu").length && clearTimeout(this.timer)))
                }, "mouseenter .ui-menu-item": function (e) {
                    var i = t(e.currentTarget);
                    i.siblings().children(".ui-state-active").removeClass("ui-state-active"), this.focus(e, i)
                }, mouseleave: "collapseAll", "mouseleave .ui-menu": "collapseAll", focus: function (t, e) {
                    var i = this.active || this.element.children(".ui-menu-item").eq(0);
                    e || this.focus(t, i)
                }, blur: function (e) {
                    this._delay(function () {
                        t.contains(this.element[0], this.document[0].activeElement) || this.collapseAll(e)
                    })
                }, keydown: "_keydown"
            }), this.refresh(), this._on(this.document, {
                click: function (e) {
                    t(e.target).closest(".ui-menu").length || this.collapseAll(e), this.mouseHandled = !1
                }
            })
        },
        _destroy: function () {
            this.element.removeAttr("aria-activedescendant").find(".ui-menu").addBack().removeClass("ui-menu ui-widget ui-widget-content ui-corner-all ui-menu-icons").removeAttr("role").removeAttr("tabIndex").removeAttr("aria-labelledby").removeAttr("aria-expanded").removeAttr("aria-hidden").removeAttr("aria-disabled").removeUniqueId().show(), this.element.find(".ui-menu-item").removeClass("ui-menu-item").removeAttr("role").removeAttr("aria-disabled").children("a").removeUniqueId().removeClass("ui-corner-all ui-state-hover").removeAttr("tabIndex").removeAttr("role").removeAttr("aria-haspopup").children().each(function () {
                var e = t(this);
                e.data("ui-menu-submenu-carat") && e.remove()
            }), this.element.find(".ui-menu-divider").removeClass("ui-menu-divider ui-widget-content")
        },
        _keydown: function (e) {
            function i(t) {
                return t.replace(/[\-\[\]{}()*+?.,\\\^$|#\s]/g, "\\$&")
            }

            var s, n, a, o, r, l = !0;
            switch (e.keyCode) {
                case t.ui.keyCode.PAGE_UP:
                    this.previousPage(e);
                    break;
                case t.ui.keyCode.PAGE_DOWN:
                    this.nextPage(e);
                    break;
                case t.ui.keyCode.HOME:
                    this._move("first", "first", e);
                    break;
                case t.ui.keyCode.END:
                    this._move("last", "last", e);
                    break;
                case t.ui.keyCode.UP:
                    this.previous(e);
                    break;
                case t.ui.keyCode.DOWN:
                    this.next(e);
                    break;
                case t.ui.keyCode.LEFT:
                    this.collapse(e);
                    break;
                case t.ui.keyCode.RIGHT:
                    this.active && !this.active.is(".ui-state-disabled") && this.expand(e);
                    break;
                case t.ui.keyCode.ENTER:
                case t.ui.keyCode.SPACE:
                    this._activate(e);
                    break;
                case t.ui.keyCode.ESCAPE:
                    this.collapse(e);
                    break;
                default:
                    l = !1, n = this.previousFilter || "", a = String.fromCharCode(e.keyCode), o = !1, clearTimeout(this.filterTimer), a === n ? o = !0 : a = n + a, r = RegExp("^" + i(a), "i"), s = this.activeMenu.children(".ui-menu-item").filter(function () {
                        return r.test(t(this).children("a").text())
                    }), s = o && -1 !== s.index(this.active.next()) ? this.active.nextAll(".ui-menu-item") : s, s.length || (a = String.fromCharCode(e.keyCode), r = RegExp("^" + i(a), "i"), s = this.activeMenu.children(".ui-menu-item").filter(function () {
                        return r.test(t(this).children("a").text())
                    })), s.length ? (this.focus(e, s), s.length > 1 ? (this.previousFilter = a, this.filterTimer = this._delay(function () {
                        delete this.previousFilter
                    }, 1e3)) : delete this.previousFilter) : delete this.previousFilter
            }
            l && e.preventDefault()
        },
        _activate: function (t) {
            this.active.is(".ui-state-disabled") || (this.active.children("a[aria-haspopup='true']").length ? this.expand(t) : this.select(t))
        },
        refresh: function () {
            var e, i = this.options.icons.submenu, s = this.element.find(this.options.menus);
            this.element.toggleClass("ui-menu-icons", !!this.element.find(".ui-icon").length), s.filter(":not(.ui-menu)").addClass("ui-menu ui-widget ui-widget-content ui-corner-all").hide().attr({
                role: this.options.role,
                "aria-hidden": "true",
                "aria-expanded": "false"
            }).each(function () {
                var e = t(this), s = e.prev("a"),
                    n = t("<span>").addClass("ui-menu-icon ui-icon " + i).data("ui-menu-submenu-carat", !0);
                s.attr("aria-haspopup", "true").prepend(n), e.attr("aria-labelledby", s.attr("id"))
            }), e = s.add(this.element), e.children(":not(.ui-menu-item):has(a)").addClass("ui-menu-item").attr("role", "presentation").children("a").uniqueId().addClass("ui-corner-all").attr({
                tabIndex: -1,
                role: this._itemRole()
            }), e.children(":not(.ui-menu-item)").each(function () {
                var e = t(this);
                /[^\-\u2014\u2013\s]/.test(e.text()) || e.addClass("ui-widget-content ui-menu-divider")
            }), e.children(".ui-state-disabled").attr("aria-disabled", "true"), this.active && !t.contains(this.element[0], this.active[0]) && this.blur()
        },
        _itemRole: function () {
            return {menu: "menuitem", listbox: "option"}[this.options.role]
        },
        _setOption: function (t, e) {
            "icons" === t && this.element.find(".ui-menu-icon").removeClass(this.options.icons.submenu).addClass(e.submenu), this._super(t, e)
        },
        focus: function (t, e) {
            var i, s;
            this.blur(t, t && "focus" === t.type), this._scrollIntoView(e), this.active = e.first(), s = this.active.children("a").addClass("ui-state-focus"), this.options.role && this.element.attr("aria-activedescendant", s.attr("id")), this.active.parent().closest(".ui-menu-item").children("a:first").addClass("ui-state-active"), t && "keydown" === t.type ? this._close() : this.timer = this._delay(function () {
                this._close()
            }, this.delay), i = e.children(".ui-menu"), i.length && t && /^mouse/.test(t.type) && this._startOpening(i), this.activeMenu = e.parent(), this._trigger("focus", t, {item: e})
        },
        _scrollIntoView: function (e) {
            var i, s, n, a, o, r;
            this._hasScroll() && (i = parseFloat(t.css(this.activeMenu[0], "borderTopWidth")) || 0, s = parseFloat(t.css(this.activeMenu[0], "paddingTop")) || 0, n = e.offset().top - this.activeMenu.offset().top - i - s, a = this.activeMenu.scrollTop(), o = this.activeMenu.height(), r = e.height(), 0 > n ? this.activeMenu.scrollTop(a + n) : n + r > o && this.activeMenu.scrollTop(a + n - o + r))
        },
        blur: function (t, e) {
            e || clearTimeout(this.timer), this.active && (this.active.children("a").removeClass("ui-state-focus"), this.active = null, this._trigger("blur", t, {item: this.active}))
        },
        _startOpening: function (t) {
            clearTimeout(this.timer), "true" === t.attr("aria-hidden") && (this.timer = this._delay(function () {
                this._close(), this._open(t)
            }, this.delay))
        },
        _open: function (e) {
            var i = t.extend({of: this.active}, this.options.position);
            clearTimeout(this.timer), this.element.find(".ui-menu").not(e.parents(".ui-menu")).hide().attr("aria-hidden", "true"), e.show().removeAttr("aria-hidden").attr("aria-expanded", "true").position(i)
        },
        collapseAll: function (e, i) {
            clearTimeout(this.timer), this.timer = this._delay(function () {
                var s = i ? this.element : t(e && e.target).closest(this.element.find(".ui-menu"));
                s.length || (s = this.element), this._close(s), this.blur(e), this.activeMenu = s
            }, this.delay)
        },
        _close: function (t) {
            t || (t = this.active ? this.active.parent() : this.element), t.find(".ui-menu").hide().attr("aria-hidden", "true").attr("aria-expanded", "false").end().find("a.ui-state-active").removeClass("ui-state-active")
        },
        collapse: function (t) {
            var e = this.active && this.active.parent().closest(".ui-menu-item", this.element);
            e && e.length && (this._close(), this.focus(t, e))
        },
        expand: function (t) {
            var e = this.active && this.active.children(".ui-menu ").children(".ui-menu-item").first();
            e && e.length && (this._open(e.parent()), this._delay(function () {
                this.focus(t, e)
            }))
        },
        next: function (t) {
            this._move("next", "first", t)
        },
        previous: function (t) {
            this._move("prev", "last", t)
        },
        isFirstItem: function () {
            return this.active && !this.active.prevAll(".ui-menu-item").length
        },
        isLastItem: function () {
            return this.active && !this.active.nextAll(".ui-menu-item").length
        },
        _move: function (t, e, i) {
            var s;
            this.active && (s = "first" === t || "last" === t ? this.active["first" === t ? "prevAll" : "nextAll"](".ui-menu-item").eq(-1) : this.active[t + "All"](".ui-menu-item").eq(0)), s && s.length && this.active || (s = this.activeMenu.children(".ui-menu-item")[e]()), this.focus(i, s)
        },
        nextPage: function (e) {
            var i, s, n;
            return this.active ? (this.isLastItem() || (this._hasScroll() ? (s = this.active.offset().top, n = this.element.height(), this.active.nextAll(".ui-menu-item").each(function () {
                return i = t(this), 0 > i.offset().top - s - n
            }), this.focus(e, i)) : this.focus(e, this.activeMenu.children(".ui-menu-item")[this.active ? "last" : "first"]())), undefined) : (this.next(e), undefined)
        },
        previousPage: function (e) {
            var i, s, n;
            return this.active ? (this.isFirstItem() || (this._hasScroll() ? (s = this.active.offset().top, n = this.element.height(), this.active.prevAll(".ui-menu-item").each(function () {
                return i = t(this), i.offset().top - s + n > 0
            }), this.focus(e, i)) : this.focus(e, this.activeMenu.children(".ui-menu-item").first())), undefined) : (this.next(e), undefined)
        },
        _hasScroll: function () {
            return this.element.outerHeight() < this.element.prop("scrollHeight")
        },
        select: function (e) {
            this.active = this.active || t(e.target).closest(".ui-menu-item");
            var i = {item: this.active};
            this.active.has(".ui-menu").length || this.collapseAll(e, !0), this._trigger("select", e, i)
        }
    })
})(jQuery);
(function (t) {
    var e = 5;
    t.widget("ui.slider", t.ui.mouse, {
        version: "1.10.4",
        widgetEventPrefix: "slide",
        options: {
            animate: !1,
            distance: 0,
            max: 100,
            min: 0,
            orientation: "horizontal",
            range: !1,
            step: 1,
            value: 0,
            values: null,
            change: null,
            slide: null,
            start: null,
            stop: null
        },
        _create: function () {
            this._keySliding = !1, this._mouseSliding = !1, this._animateOff = !0, this._handleIndex = null, this._detectOrientation(), this._mouseInit(), this.element.addClass("ui-slider ui-slider-" + this.orientation + " ui-widget" + " ui-widget-content" + " ui-corner-all"), this._refresh(), this._setOption("disabled", this.options.disabled), this._animateOff = !1
        },
        _refresh: function () {
            this._createRange(), this._createHandles(), this._setupEvents(), this._refreshValue()
        },
        _createHandles: function () {
            var e, i, s = this.options,
                n = this.element.find(".ui-slider-handle").addClass("ui-state-default ui-corner-all"),
                a = "<a class='ui-slider-handle ui-state-default ui-corner-all' href='#'></a>", o = [];
            for (i = s.values && s.values.length || 1, n.length > i && (n.slice(i).remove(), n = n.slice(0, i)), e = n.length; i > e; e++) o.push(a);
            this.handles = n.add(t(o.join("")).appendTo(this.element)), this.handle = this.handles.eq(0), this.handles.each(function (e) {
                t(this).data("ui-slider-handle-index", e)
            })
        },
        _createRange: function () {
            var e = this.options, i = "";
            e.range ? (e.range === !0 && (e.values ? e.values.length && 2 !== e.values.length ? e.values = [e.values[0], e.values[0]] : t.isArray(e.values) && (e.values = e.values.slice(0)) : e.values = [this._valueMin(), this._valueMin()]), this.range && this.range.length ? this.range.removeClass("ui-slider-range-min ui-slider-range-max").css({
                left: "",
                bottom: ""
            }) : (this.range = t("<div></div>").appendTo(this.element), i = "ui-slider-range ui-widget-header ui-corner-all"), this.range.addClass(i + ("min" === e.range || "max" === e.range ? " ui-slider-range-" + e.range : ""))) : (this.range && this.range.remove(), this.range = null)
        },
        _setupEvents: function () {
            var t = this.handles.add(this.range).filter("a");
            this._off(t), this._on(t, this._handleEvents), this._hoverable(t), this._focusable(t)
        },
        _destroy: function () {
            this.handles.remove(), this.range && this.range.remove(), this.element.removeClass("ui-slider ui-slider-horizontal ui-slider-vertical ui-widget ui-widget-content ui-corner-all"), this._mouseDestroy()
        },
        _mouseCapture: function (e) {
            var i, s, n, a, o, r, l, h, u = this, c = this.options;
            return c.disabled ? !1 : (this.elementSize = {
                width: this.element.outerWidth(),
                height: this.element.outerHeight()
            }, this.elementOffset = this.element.offset(), i = {
                x: e.pageX,
                y: e.pageY
            }, s = this._normValueFromMouse(i), n = this._valueMax() - this._valueMin() + 1, this.handles.each(function (e) {
                var i = Math.abs(s - u.values(e));
                (n > i || n === i && (e === u._lastChangedValue || u.values(e) === c.min)) && (n = i, a = t(this), o = e)
            }), r = this._start(e, o), r === !1 ? !1 : (this._mouseSliding = !0, this._handleIndex = o, a.addClass("ui-state-active").focus(), l = a.offset(), h = !t(e.target).parents().addBack().is(".ui-slider-handle"), this._clickOffset = h ? {
                left: 0,
                top: 0
            } : {
                left: e.pageX - l.left - a.width() / 2,
                top: e.pageY - l.top - a.height() / 2 - (parseInt(a.css("borderTopWidth"), 10) || 0) - (parseInt(a.css("borderBottomWidth"), 10) || 0) + (parseInt(a.css("marginTop"), 10) || 0)
            }, this.handles.hasClass("ui-state-hover") || this._slide(e, o, s), this._animateOff = !0, !0))
        },
        _mouseStart: function () {
            return !0
        },
        _mouseDrag: function (t) {
            var e = {x: t.pageX, y: t.pageY}, i = this._normValueFromMouse(e);
            return this._slide(t, this._handleIndex, i), !1
        },
        _mouseStop: function (t) {
            return this.handles.removeClass("ui-state-active"), this._mouseSliding = !1, this._stop(t, this._handleIndex), this._change(t, this._handleIndex), this._handleIndex = null, this._clickOffset = null, this._animateOff = !1, !1
        },
        _detectOrientation: function () {
            this.orientation = "vertical" === this.options.orientation ? "vertical" : "horizontal"
        },
        _normValueFromMouse: function (t) {
            var e, i, s, n, a;
            return "horizontal" === this.orientation ? (e = this.elementSize.width, i = t.x - this.elementOffset.left - (this._clickOffset ? this._clickOffset.left : 0)) : (e = this.elementSize.height, i = t.y - this.elementOffset.top - (this._clickOffset ? this._clickOffset.top : 0)), s = i / e, s > 1 && (s = 1), 0 > s && (s = 0), "vertical" === this.orientation && (s = 1 - s), n = this._valueMax() - this._valueMin(), a = this._valueMin() + s * n, this._trimAlignValue(a)
        },
        _start: function (t, e) {
            var i = {handle: this.handles[e], value: this.value()};
            return this.options.values && this.options.values.length && (i.value = this.values(e), i.values = this.values()), this._trigger("start", t, i)
        },
        _slide: function (t, e, i) {
            var s, n, a;
            this.options.values && this.options.values.length ? (s = this.values(e ? 0 : 1), 2 === this.options.values.length && this.options.range === !0 && (0 === e && i > s || 1 === e && s > i) && (i = s), i !== this.values(e) && (n = this.values(), n[e] = i, a = this._trigger("slide", t, {
                handle: this.handles[e],
                value: i,
                values: n
            }), s = this.values(e ? 0 : 1), a !== !1 && this.values(e, i))) : i !== this.value() && (a = this._trigger("slide", t, {
                handle: this.handles[e],
                value: i
            }), a !== !1 && this.value(i))
        },
        _stop: function (t, e) {
            var i = {handle: this.handles[e], value: this.value()};
            this.options.values && this.options.values.length && (i.value = this.values(e), i.values = this.values()), this._trigger("stop", t, i)
        },
        _change: function (t, e) {
            if (!this._keySliding && !this._mouseSliding) {
                var i = {handle: this.handles[e], value: this.value()};
                this.options.values && this.options.values.length && (i.value = this.values(e), i.values = this.values()), this._lastChangedValue = e, this._trigger("change", t, i)
            }
        },
        value: function (t) {
            return arguments.length ? (this.options.value = this._trimAlignValue(t), this._refreshValue(), this._change(null, 0), undefined) : this._value()
        },
        values: function (e, i) {
            var s, n, a;
            if (arguments.length > 1) return this.options.values[e] = this._trimAlignValue(i), this._refreshValue(), this._change(null, e), undefined;
            if (!arguments.length) return this._values();
            if (!t.isArray(arguments[0])) return this.options.values && this.options.values.length ? this._values(e) : this.value();
            for (s = this.options.values, n = arguments[0], a = 0; s.length > a; a += 1) s[a] = this._trimAlignValue(n[a]), this._change(null, a);
            this._refreshValue()
        },
        _setOption: function (e, i) {
            var s, n = 0;
            switch ("range" === e && this.options.range === !0 && ("min" === i ? (this.options.value = this._values(0), this.options.values = null) : "max" === i && (this.options.value = this._values(this.options.values.length - 1), this.options.values = null)), t.isArray(this.options.values) && (n = this.options.values.length), t.Widget.prototype._setOption.apply(this, arguments), e) {
                case"orientation":
                    this._detectOrientation(), this.element.removeClass("ui-slider-horizontal ui-slider-vertical").addClass("ui-slider-" + this.orientation), this._refreshValue();
                    break;
                case"value":
                    this._animateOff = !0, this._refreshValue(), this._change(null, 0), this._animateOff = !1;
                    break;
                case"values":
                    for (this._animateOff = !0, this._refreshValue(), s = 0; n > s; s += 1) this._change(null, s);
                    this._animateOff = !1;
                    break;
                case"min":
                case"max":
                    this._animateOff = !0, this._refreshValue(), this._animateOff = !1;
                    break;
                case"range":
                    this._animateOff = !0, this._refresh(), this._animateOff = !1
            }
        },
        _value: function () {
            var t = this.options.value;
            return t = this._trimAlignValue(t)
        },
        _values: function (t) {
            var e, i, s;
            if (arguments.length) return e = this.options.values[t], e = this._trimAlignValue(e);
            if (this.options.values && this.options.values.length) {
                for (i = this.options.values.slice(), s = 0; i.length > s; s += 1) i[s] = this._trimAlignValue(i[s]);
                return i
            }
            return []
        },
        _trimAlignValue: function (t) {
            if (this._valueMin() >= t) return this._valueMin();
            if (t >= this._valueMax()) return this._valueMax();
            var e = this.options.step > 0 ? this.options.step : 1, i = (t - this._valueMin()) % e, s = t - i;
            return 2 * Math.abs(i) >= e && (s += i > 0 ? e : -e), parseFloat(s.toFixed(5))
        },
        _valueMin: function () {
            return this.options.min
        },
        _valueMax: function () {
            return this.options.max
        },
        _refreshValue: function () {
            var e, i, s, n, a, o = this.options.range, r = this.options, l = this,
                h = this._animateOff ? !1 : r.animate, u = {};
            this.options.values && this.options.values.length ? this.handles.each(function (s) {
                i = 100 * ((l.values(s) - l._valueMin()) / (l._valueMax() - l._valueMin())), u["horizontal" === l.orientation ? "left" : "bottom"] = i + "%", t(this).stop(1, 1)[h ? "animate" : "css"](u, r.animate), l.options.range === !0 && ("horizontal" === l.orientation ? (0 === s && l.range.stop(1, 1)[h ? "animate" : "css"]({left: i + "%"}, r.animate), 1 === s && l.range[h ? "animate" : "css"]({width: i - e + "%"}, {
                    queue: !1,
                    duration: r.animate
                })) : (0 === s && l.range.stop(1, 1)[h ? "animate" : "css"]({bottom: i + "%"}, r.animate), 1 === s && l.range[h ? "animate" : "css"]({height: i - e + "%"}, {
                    queue: !1,
                    duration: r.animate
                }))), e = i
            }) : (s = this.value(), n = this._valueMin(), a = this._valueMax(), i = a !== n ? 100 * ((s - n) / (a - n)) : 0, u["horizontal" === this.orientation ? "left" : "bottom"] = i + "%", this.handle.stop(1, 1)[h ? "animate" : "css"](u, r.animate), "min" === o && "horizontal" === this.orientation && this.range.stop(1, 1)[h ? "animate" : "css"]({width: i + "%"}, r.animate), "max" === o && "horizontal" === this.orientation && this.range[h ? "animate" : "css"]({width: 100 - i + "%"}, {
                queue: !1,
                duration: r.animate
            }), "min" === o && "vertical" === this.orientation && this.range.stop(1, 1)[h ? "animate" : "css"]({height: i + "%"}, r.animate), "max" === o && "vertical" === this.orientation && this.range[h ? "animate" : "css"]({height: 100 - i + "%"}, {
                queue: !1,
                duration: r.animate
            }))
        },
        _handleEvents: {
            keydown: function (i) {
                var s, n, a, o, r = t(i.target).data("ui-slider-handle-index");
                switch (i.keyCode) {
                    case t.ui.keyCode.HOME:
                    case t.ui.keyCode.END:
                    case t.ui.keyCode.PAGE_UP:
                    case t.ui.keyCode.PAGE_DOWN:
                    case t.ui.keyCode.UP:
                    case t.ui.keyCode.RIGHT:
                    case t.ui.keyCode.DOWN:
                    case t.ui.keyCode.LEFT:
                        if (i.preventDefault(), !this._keySliding && (this._keySliding = !0, t(i.target).addClass("ui-state-active"), s = this._start(i, r), s === !1)) return
                }
                switch (o = this.options.step, n = a = this.options.values && this.options.values.length ? this.values(r) : this.value(), i.keyCode) {
                    case t.ui.keyCode.HOME:
                        a = this._valueMin();
                        break;
                    case t.ui.keyCode.END:
                        a = this._valueMax();
                        break;
                    case t.ui.keyCode.PAGE_UP:
                        a = this._trimAlignValue(n + (this._valueMax() - this._valueMin()) / e);
                        break;
                    case t.ui.keyCode.PAGE_DOWN:
                        a = this._trimAlignValue(n - (this._valueMax() - this._valueMin()) / e);
                        break;
                    case t.ui.keyCode.UP:
                    case t.ui.keyCode.RIGHT:
                        if (n === this._valueMax()) return;
                        a = this._trimAlignValue(n + o);
                        break;
                    case t.ui.keyCode.DOWN:
                    case t.ui.keyCode.LEFT:
                        if (n === this._valueMin()) return;
                        a = this._trimAlignValue(n - o)
                }
                this._slide(i, r, a)
            }, click: function (t) {
                t.preventDefault()
            }, keyup: function (e) {
                var i = t(e.target).data("ui-slider-handle-index");
                this._keySliding && (this._keySliding = !1, this._stop(e, i), this._change(e, i), t(e.target).removeClass("ui-state-active"))
            }
        }
    })
})(jQuery);


/*"/assets/components/minishop2/js/web/default.js"*/
typeof $.fn.jGrowl == 'function' || document.write('<script src="' + miniShop2Config.jsUrl + 'lib/jquery.jgrowl.min.js"><\/script>');
(function (window, document, $, undefined) {
    miniShop2.ajaxProgress = false;
    miniShop2.setup = function () {
        this.actionName = 'ms2_action';
        this.actionNamePayment = 'ms2_action_payment';
        this.action = ':submit[name=' + this.actionName + ']';
        this.action_payment = ':submit[name=' + this.actionNamePayment + ']';
        this.form = '.ms2_form';
        this.$doc = $(document);
        this.sendData = {$form: null, action: null, formData: null};
    };
    miniShop2.initialize = function () {
        miniShop2.setup();
        miniShop2.$doc.ajaxStart(function () {
            miniShop2.ajaxProgress = true;
        }).ajaxStop(function () {
            miniShop2.ajaxProgress = false;
        }).on('submit', miniShop2.form, function (e) {
            $('.error-phone').html('');
            $('.error-phone').html('');
            e.preventDefault();
            var $form = $(this);
            var action = $form.find(miniShop2.action).val();
            var action_payment = $form.find(miniShop2.action_payment).val();
            console.log(e);
            //alert(action_payment);

            if (action) {
                var formData = $form.serializeArray();
                formData.push({name: miniShop2.actionName, value: action});

                //action_payment
                miniShop2.sendData = {$form: $form, action: action, formData: formData};
                miniShop2.controller();
            }
        }).on('click', '#payment_liqpay_submit', function (e) {
            $('.error-phone').html('');
            $('.error-phone').html('');
            e.preventDefault();
            var $form = $('#msOrder');//'#ms2_form');
            //ms2_action_payment
            var action = 'order/submitpayment';//$form.find(miniShop2.actionNamePayment).val();
            //alert(action);
            //var action_payment = $form.find(miniShop2.action_payment).val();
            //console.log(e);
            //alert(action_payment);

            //if (action) {
            var formData = $form.serializeArray();
            formData.push({name: miniShop2.actionNamePayment, value: action});
            formData.push({name: miniShop2.actionName, value: action});

            //action_payment
            miniShop2.sendData = {$form: $form, action: action, formData: formData};
            // console.log(miniShop2.sendData);
            miniShop2.controller();
            // }
        });
//
        //jQuery(document)


        miniShop2.Cart.initialize();
        miniShop2.Message.initialize();
        miniShop2.Order.initialize();
        miniShop2.Gallery.initialize();
    }
    miniShop2.controller = function () {
        var self = this;
        //alert('action'+self.sendData.action);
        switch (self.sendData.action) {
            case 'cart/add':
                miniShop2.Cart.add();
                break;
            case 'cart/remove':
                miniShop2.Cart.remove();
                break;
            case 'cart/change':
                miniShop2.Cart.change();
                break;
            case 'cart/clean':
                miniShop2.Cart.clean();
                break;
            case 'order/submitpayment':
                miniShop2.Order.submitpayment();
                break;
            case 'order/submit':
                miniShop2.Order.submit();
                break;
            case 'order/submitquick':
                miniShop2.Order.submitquick();
                break;

            case 'order/promocode':
                miniShop2.Order.promocode();
                break;
            case 'order/clean':
                miniShop2.Order.clean();
                break;
            case 'order/get':
                miniShop2.Order.get();
                break;
            default:
                return;
        }
    };

    $('#msOrder').on('click', '.promocodevalidate', function () {
        miniShop2.Order.promocode();
        return false;
        //alert('sss');

    });

    $('#msOrder').on('click', '.bonusesvalidate', function () {
        value = $('#bonuses').val();

        $.post('/assets/components/minishop2/action.php', {
            'ctx': 'web',
            'key': 'bonuses',
            'ms2_action': 'order/bonuses',
            'value': value
        }, function (response) {


            if (!response.success) {
                alert(response.message);
            } else {

                $('#messagebonuses').html('Бонусы успешно применены');
                $('#bonusessumma').html(response.data.bonuses + ' <sup>00</sup>');
                $('#totalcart').html(response.data.total + ' <sup>00</sup>');

            }
        }, 'json');
        return false;

    });


    miniShop2.send = function (data, callbacks, userCallbacks) {
        var runCallback = function (callback, bind) {
            if (typeof callback == 'function') {
                return callback.apply(bind, Array.prototype.slice.call(arguments, 2));
            } else {
                return true;
            }
        }
        if ($.isArray(data)) {
            data.push({name: 'ctx', value: miniShop2Config.ctx});
        } else if ($.isPlainObject(data)) {
            data.ctx = miniShop2Config.ctx;
        } else if (typeof data == 'string') {
            data += '&ctx=' + miniShop2Config.ctx;
        }
        var formActionUrl = (miniShop2.sendData.$form) ? miniShop2.sendData.$form.attr('action') : false;
        var url = (formActionUrl) ? formActionUrl : (miniShop2Config.actionUrl) ? miniShop2Config.actionUrl : document.location.href;
        var formMethod = (miniShop2.sendData.$form) ? miniShop2.sendData.$form.attr('method') : false;
        var method = (formMethod) ? formMethod : 'post';
        if (runCallback(callbacks.before) === false || runCallback(userCallbacks.before) === false) {
            return;
        }
        var xhr = function (callbacks, userCallbacks) {
            return $[method](url, data, function (response) {
                if (data.ms2_action == 'order/promocode') {
                    $('#messagepromocode').html(response.message);
                }
                if (response.success) {
                    if (response.message) {
                        if (data.ms2_action != 'order/promocode') miniShop2.Message.success(response.message);
                    }
                    runCallback(callbacks.response.success, miniShop2, response);
                    runCallback(userCallbacks.response.success, miniShop2, response);
                } else {
                    eco = '<b>0</b><sup>00</sup>';
                    $('#discountsumma').html(eco);
                    if (typeof(response.data.email) != 'undefined') {
                        $('.error-email').parent().parent().addClass('has-error');
                        setTimeout(function () {
                            $('.error-email').html(response.data.email)
                        }, 500);
                    }
                    if (typeof(response.data.phone) != 'undefined') {
                        $('.error-phone').parent().parent().addClass('has-error');
                        setTimeout(function () {
                            $('.error-phone').html(response.data.phone)
                        }, 500);
                    }
                    if (data.ms2_action != 'order/promocode') miniShop2.Message.error(response.message);
                    runCallback(callbacks.response.error, miniShop2, response);
                    runCallback(userCallbacks.response.error, miniShop2, response);
                }
            }, 'json').done(function () {
                runCallback(callbacks.ajax.done, miniShop2, xhr);
                runCallback(userCallbacks.ajax.done, miniShop2, xhr);
            }).fail(function () {
                runCallback(callbacks.ajax.fail, miniShop2, xhr);
                runCallback(userCallbacks.ajax.fail, miniShop2, xhr);
            }).always(function () {
                runCallback(callbacks.ajax.always, miniShop2, xhr);
                runCallback(userCallbacks.ajax.always, miniShop2, xhr);
            });
        }(callbacks, userCallbacks);
    };
    miniShop2.Cart = {
        callbacks: {
            add: miniShop2Config.callbacksObjectTemplate(),
            remove: miniShop2Config.callbacksObjectTemplate(),
            change: miniShop2Config.callbacksObjectTemplate(),
            clean: miniShop2Config.callbacksObjectTemplate()
        }, setup: function () {
            miniShop2.Cart.cart = '#msCart';
            miniShop2.Cart.miniCart = '#msMiniCart';
            miniShop2.Cart.miniCartNotEmptyClass = 'full';
            miniShop2.Cart.countInput = 'input[name=count]';
            miniShop2.Cart.totalWeight = '.ms2_total_weight';
            miniShop2.Cart.totalCount = '.ms2_total_count';
            miniShop2.Cart.totalCost = '.ms2_total_cost';
        }, initialize: function () {
            miniShop2.Cart.setup();
            if (!$(miniShop2.Cart.cart).length) return;
            miniShop2.$doc.on('change', miniShop2.Cart.cart + ' ' + miniShop2.Cart.countInput, function () {
                $(this).closest(miniShop2.form).submit();
            });
        }, add: function () {
            var callbacks = miniShop2.Cart.callbacks;
            callbacks.add.response.success = function (response) {
                this.Cart.status(response.data);
            }
            miniShop2.send(miniShop2.sendData.formData, miniShop2.Cart.callbacks.add, miniShop2.Callbacks.Cart.add);
        }, remove: function () {
            var callbacks = miniShop2.Cart.callbacks;
            callbacks.remove.response.success = function (response) {
                this.Cart.remove_position(miniShop2.Utils.getValueFromSerializedArray('key'));
                this.Cart.status(response.data);
            }
            miniShop2.send(miniShop2.sendData.formData, miniShop2.Cart.callbacks.remove, miniShop2.Callbacks.Cart.remove);
        }, change: function () {
            var callbacks = miniShop2.Cart.callbacks;
            callbacks.change.response.success = function (response) {
                if (typeof(response.data.key) == 'undefined') {
                    this.Cart.remove_position(miniShop2.Utils.getValueFromSerializedArray('key'));
                } else {
                    $('#' + miniShop2.Utils.getValueFromSerializedArray('key')).find('');
                }
                this.Cart.status(response.data);
            }
            miniShop2.send(miniShop2.sendData.formData, miniShop2.Cart.callbacks.change, miniShop2.Callbacks.Cart.change);
        }, status: function (status) {
            if (status['total_count'] < 1) {
                location.reload();
            } else {
                var $cart = $(miniShop2.Cart.cart);
                var $miniCart = $(miniShop2.Cart.miniCart);
                if (status['total_count'] > 0 && !$miniCart.hasClass(miniShop2.Cart.miniCartNotEmptyClass)) {
                    $miniCart.addClass(miniShop2.Cart.miniCartNotEmptyClass);
                }
                $(miniShop2.Cart.totalWeight).text(miniShop2.Utils.formatWeight(status['total_weight']));
                $(miniShop2.Cart.totalCount).text(status['total_count']);
                $(miniShop2.Cart.totalCost).text(miniShop2.Utils.formatPrice(status['total_cost']));
                if ($(miniShop2.Order.orderCost, miniShop2.Order.order).length) {
                    miniShop2.Order.getcost();
                }
            }
        }, clean: function () {
            var callbacks = miniShop2.Cart.callbacks;
            callbacks.clean.response.success = function (response) {
                this.Cart.status(response.data);
            }
            miniShop2.send(miniShop2.sendData.formData, miniShop2.Cart.callbacks.clean, miniShop2.Callbacks.Cart.clean);
        }, remove_position: function (key) {
            $('#' + key).remove();
        }
    };
    miniShop2.Gallery = {
        setup: function () {
            miniShop2.Gallery.gallery = '#msGallery';
            miniShop2.Gallery.mainImage = '#mainImage';
            miniShop2.Gallery.thumbnail = '.thumbnail';
        }, initialize: function () {
            miniShop2.Gallery.setup();
            if ($(miniShop2.Gallery.gallery).length) {
                miniShop2.$doc.on('click', miniShop2.Gallery.gallery + ' ' + miniShop2.Gallery.thumbnail, function (e) {
                    var src = $(this).attr('href');
                    var href = $(this).data('image');
                    $(miniShop2.Gallery.mainImage, miniShop2.Gallery.gallery).attr('src', src).parent().attr('href', href);
                    e.preventDefault();
                });
                $(miniShop2.Gallery.thumbnail + ':first', miniShop2.Gallery.gallery).trigger('click');
            }
        }
    };
    miniShop2.Order = {
        callbacks: {
            add: miniShop2Config.callbacksObjectTemplate(),
            getcost: miniShop2Config.callbacksObjectTemplate(),
            promocode: miniShop2Config.callbacksObjectTemplate(),
            clean: miniShop2Config.callbacksObjectTemplate(),
            submit: miniShop2Config.callbacksObjectTemplate(),
            getRequired: miniShop2Config.callbacksObjectTemplate(),
            get: miniShop2Config.callbacksObjectTemplate()
        },
        setup: function () {
            miniShop2.Order.order = '#msOrder';
            miniShop2.Order.deliveries = '#deliveries';
            miniShop2.Order.payments = '#payments';
            miniShop2.Order.deliveryInput = 'input[name="delivery"]';
            miniShop2.Order.inputParent = '.input-parent';
            miniShop2.Order.paymentInput = 'input[name="payment"]';
            miniShop2.Order.paymentInputUniquePrefix = 'input#payment_';
            miniShop2.Order.deliveryInputUniquePrefix = 'input#delivery_';
            miniShop2.Order.orderCost = '#ms2_order_cost';

            $('input[name=phone_search]').typeahead({
                source: function (query, process) {
                    return $.post('/assets/components/suggest/getPhone.php', {'name': query}, function (response) {
                        $('input[name=phone_search]').focus();
                        var data = new Array();
                        var records = response.content.records;
                        $.each(response.content.records, function (i) {
                            $.each(response.content.records[i], function (key, val) {
                                data.push(key + '_' + val);
                            });
                            return process(data);
                        });
                    }, 'json');
                }, matcher: function (item) {
                    return true;
                    if (item.toLowerCase().indexOf(this.query.trim().toLowerCase()) != -1) {
                        return true;
                    }
                }, sorter: function (items) {
                    return items.sort();
                }, highlighter: function (item) {
                    var parts = item.split('_');
                    parts.shift();
                    return parts.join('_');
                }, updater: function (item) {
                    var parts = item.split('_');
                    var userId = parts.shift();
                    if (userId != 0) {
                        $('#msOrder').addClass('loading');
                        $.post('/assets/components/minishop2/getuserdata.php', {
                                'user_id': userId,
                                'total_cost': $('.total_cost_order').val()
                            },
                            function (userdata) {
                                user = userdata.data;
                                $('#deliveries').prop('disabled', false);
                                $('#msOrder').removeClass('loading');
                                $('input[name=receiver]').val(user.receiver);
                                $('input[name=lastname]').val(user.lastname);
                                $('input[name=phone]').val(user.phone);
                                $('input[name=email]').val(user.email);
                                $('input[name=suggest_locality]').val(user.suggest_locality);
                                $('input[name=city]').val(user.city);
                                $('input#cityidorder').val(user.city_id);
                                $('input#streetorder').val(user.street);

                                $('input#houseorder').val(user.house);
                                $('input#roomorder').val(user.room);
                                if ((user.subscribe == 0) || (user.subscribe == '')) {
                                    $('#subscribe').attr('checked', false);
                                } else {
                                    $('#subscribe').attr('checked', true);
                                }
                                getWarehouses(user.city_id, user.delivery, user.warehouse);
                                $('#deliveries').html('');
                                $('#deliveries').html(user.deliveries);
                                $('.deliverieswrap .custom-select span span').text($("#deliveries option:selected").text());
                                if ((user.delivery != 2) && (user.delivery != 3) && (user.delivery != 4)) {
                                    $('#warehouse-block').addClass('hide');
                                }
                                /*if ((user.delivery == 4) || (user.delivery == 5)) {
                                    $('#warehouse-block').addClass('hide');// курьерская
                                    $('#courier-block').removeClass('hide');
                                    $('#samovivoz-block').addClass('hide');

                                    $('#street_user').val(user.street);
                                    $('#house_user').val(user.house);
                                    $('#room_user').val(user.room);
                                    $('#parade_user').val(user.parade);
                                    $('#floor_user').val(user.floor);
                                }hidenp */

                                //hidenp
                                if ((user.delivery == 4) || (user.delivery == 5)) {
                                    $('#warehouse-block').addClass('hide');// курьерская
                                    $('#courier-block').removeClass('hide');
                                    $('#samovivoz-block').addClass('hide');

                                    $('#street_ext').val(user.street);
                                    $('#street_np_ref').val(user.street_np_ref);
                                    $('#street_np').val(user.street_np);
                                    //street_np

                                    //$('#street_user').val(user.street);
                                    $('#house_user').val(user.house);
                                    $('#room_user').val(user.room);
                                    $('#parade_user').val(user.parade);
                                    $('#floor_user').val(user.floor);

                                    //$('#courier-block .hidenp').show();
                                    //$('#floor_user').hidenp
                                    if (user.delivery == 5)
                                        $('#courier-block .hidenp').hide();
                                    else
                                        $('#courier-block .hidenp').show();
                                }
                                else if (user.delivery == 1) {
                                    $('#warehouse-block').addClass('hide');// курьерская
                                    $('#courier-block').addClass('hide');
                                    $('#samovivoz-block').removeClass('hide');
                                }
                                else {
                                    $('#warehouse-block').removeClass('hide');// курьерская
                                    $('#courier-block').addClass('hide');
                                    $('#samovivoz-block').addClass('hide');
                                }

                                $('#descdeliveryorder').html(user.descdelivery);
                                if (user.payment) {
                                    $('#payments').html(user.payments);
                                    $('#payments').prop('disabled', false);
                                    $('.paymentswrap .custom-select span span').text($("#payments option:selected").text());
                                }
                                $('#deliveries').customSelect('update');
                                $('#deliveries').customSelect('change');
                                $('#payments').customSelect('update');
                                if (user.cart) {
                                    $('#ajaxcartcontent').html(user.cart);
                                }
                            }, 'json');

                        return parts.join('_');
                    } else return '';
                }, autoSelect: true, minLength: 3, delay: 500, items: 10,
            });
            $('input[name=email_search]').typeahead({
                source: function (query, process) {
                    return $.post('/assets/components/suggest/getPhone.php', {'name_email': query}, function (response) {
                        $('input[name=email_search]').focus();
                        var data = new Array();
                        var records = response.content.records;
                        $.each(response.content.records, function (i) {
                            $.each(response.content.records[i], function (key, val) {
                                data.push(key + '_' + val);
                            });
                            return process(data);
                        });
                    }, 'json');
                }, matcher: function (item) {
                    return true;
                    if (item.toLowerCase().indexOf(this.query.trim().toLowerCase()) != -1) {
                        return true;
                    }
                }, sorter: function (items) {
                    return items.sort();
                }, highlighter: function (item) {
                    var parts = item.split('_');
                    parts.shift();
                    return parts.join('_');
                }, updater: function (item) {
                    var parts = item.split('_');
                    var userId = parts.shift();

                    if (userId != 0) {
                        $('#msOrder').addClass('loading');
                        $.post('/assets/components/minishop2/getuserdata.php', {
                                'user_id': userId,
                                'total_cost': $('.total_cost_order').val()
                            },
                            function (userdata) {
                                user = userdata.data;
                                $('#deliveries').prop('disabled', false);
                                $('#msOrder').removeClass('loading');
                                $('input[name=receiver]').val(user.receiver);
                                $('input[name=lastname]').val(user.lastname);
                                $('input[name=phone]').val(user.phone);
                                $('input[name=email]').val(user.email);
                                $('input[name=suggest_locality]').val(user.suggest_locality);
                                $('input[name=city]').val(user.city);
                                $('input#cityidorder').val(user.city_id);
                                $('input#streetorder').val(user.street);
                                $('input#houseorder').val(user.house);
                                $('input#roomorder').val(user.room);
                                $('#deliveries').html('');
                                $('#deliveries').html(user.deliveries);
                                $('.deliverieswrap .custom-select span span').text($("#deliveries option:selected").text());
                                if ((user.delivery != 3) && (user.delivery != 4)) {
                                    $('#warehouse-block').addClass('hide');
                                }
//alert(user.delivery);
                                if (user.payment) {
                                    $('#payments').html(user.payments);
                                    $('#payments').prop('disabled', false);
                                    $('.paymentswrap .custom-select span span').text($("#payments option:selected").text());
                                }
                                $('#deliveries').customSelect('update');
                                $('#deliveries').customSelect('change');
                                $('#payments').customSelect('update');
                                if (user.cart) {
                                    $('#ajaxcartcontent').html(user.cart);
                                }
                            }, 'json');
                        return parts.join('_');
                    } else return '';
                }, autoSelect: true, minLength: 3, delay: 500, items: 10,
            });

            function updatecity(item) {
                var parts = item.split('_');
                var cityId = parts.shift();
                if (cityId != 0) {
                    setTimeout(function () {
                        $('.deliverieswrap .col-xs-8 .custom-select-container').addClass('loading');
                    }, 200);
                    $.post('/assets/components/delivery/getdeliverydata.php', {'city_id': cityId}, function (deliveries) {
                        $('#deliveries').prop('disabled', false);
                        $('#deliveries').html('');
                        $('#deliveries').customSelect('update');
                        $('#deliveries').html(deliveries);
                        var getFirstDelivery = $("#deliveries option:first").val();
                        $('#deliveries').customSelect('update');
                        $('#deliveries').prop('selectedIndex', getFirstDelivery);
                        $('#deliveries option:first').prop('selected', true);
                        var getFirstWarehouse = $("#deliveries option:first").val();
                        if (getFirstWarehouse == 4) {
                            $('#warehouse-block').addClass('hide');
                            $('#courier-block').removeClass('hide');
                        } else if (getFirstWarehouse == 1) {
                            $('#warehouse-block').addClass('hide');
                            $('#samovivoz-block').removeClass('hide');
                            $('#courier-block').addClass('hide');
                        } else {
                            $('#courier-block').addClass('hide');
                            $('#samovivoz-block').addClass('hide');
                            $('#warehouse-block').addClass('hide');
                        }
                        setTimeout(function () {
                            $('.deliverieswrap .col-xs-8 .custom-select-container').removeClass('loading');
                        }, 500);
                        $('.deliverieswrap .col-xs-8 .custom-select-container').customSelect('update');
                    }, 'json');
                    $("input[name=city]").val(parts);
                    $("input[name='extended[city_id]']").val(cityId);
                    return parts.join('_');
                } else return '';
            }


            $('input[name=suggest_locality]').on('click', function () {
                //$('input[name=suggest_locality]').typeahead('open');
                $('.typeahead.dropdown-menu.additionmenu').show();
                $('input[name=suggest_locality]').focus();
                $(this).select();
            });
            /*$('input[name=suggest_locality]').on('keyup',fucntion(){
                $('.typeahead.dropdown-menu').hide();
            });*/
            $('.typeahead.dropdown-menu.additionmenu li a.basiccity').on('click', function () {
                //show=true;
                value = $(this).data('id');
                parts = $(this).text();//data('value');
                //if (show){
                if (value != 0) {
                    $('input[name=suggest_locality]').val($(this).text());
                    $('input[name=suggest_locality]').focus();
                    $("input[name='extended[city_id]']").val(value);
                    $('.typeahead.dropdown-menu.additionmenu').hide();
                    //alert(value);
                    cityId = value;
                    //alert(cityId);
                    setTimeout(function () {
                        $('.deliverieswrap .col-xs-8 .custom-select-container').addClass('loading');
                    }, 200);
                    $.post('/assets/components/delivery/getdeliverydata.php', {'city_id': cityId}, function (deliveries) {
                        $('#deliveries').prop('disabled', false);
                        $('#deliveries').html('');
                        $('#deliveries').customSelect('update');
                        $('#deliveries').html(deliveries);
                        var getFirstDelivery = $("#deliveries option:first").val();
                        $('#deliveries').customSelect('update');
                        $('#deliveries').prop('selectedIndex', getFirstDelivery);
                        $('#deliveries option:first').prop('selected', true);
                        var getFirstWarehouse = $("#deliveries option:first").val();
                        if (getFirstWarehouse == 4) {
                            $('#warehouse-block').addClass('hide');
                            $('#courier-block').removeClass('hide');
                        }
                        else if (getFirstWarehouse == 1) {
                            $('#warehouse-block').addClass('hide');
                            $('#samovivoz-block').removeClass('hide');
                            $('#courier-block').addClass('hide');
                        }
                        else {
                            $('#courier-block').addClass('hide');
                            $('#samovivoz-block').addClass('hide');
                            $('#warehouse-block').addClass('hide');
                        }
                        setTimeout(function () {
                            $('.deliverieswrap .col-xs-8 .custom-select-container').removeClass('loading');
                        }, 500);
                        $('.deliverieswrap .col-xs-8 .custom-select-container').customSelect('update');
                    }, 'json');
                    $("input[name=city]").val(parts);
                    $("input[name='extended[city_id]']").val(cityId);
                    //eturn parts.join('_');


                }
                else {
                    //show=false;
                    $('input[name=suggest_locality]').val('');

                    $('.typeahead.dropdown-menu.additionmenu').hide();
                    setTimeout(function () {
                        $('input[name=suggest_locality]').focus();
                        //$('input[name=suggest_locality]').blur();
                    }, 200);
                }
                //}
            });


            $(document).ready(function () {

                $('#registrpopup').on('click', function () {

                });

                $('form.ajaxregistrbox input, form.formaregistration input').on('change', function () {
                    //alert('dddd');
                    $('input[name="registerbtn"]').removeClass('disabled');
                    key = $(this).attr('name');
                    object = this;
                    value = $(this).val();//attr('name');
                    $.post('/assets/components/minishop2/action.php', {
                        'ctx': 'web',
                        'key': key,
                        'ms2_action': 'order/add',
                        'value': value
                    }, function (response) {
                        if (!response.success) {
                            $(object).parent().addClass('has-error');//show();
                            $(object).parent().find('.with-errors').html(response.message);

                            $('input[name="registerbtn"]').addClass('disabled');
                            //.formaregistration

                        } else {
                            $(object).parent().find('.with-errors').removeClass('has-error');
                            //$('.formaregistration input[name="registerbtn"]').removeClass('disabled');
                            $('input[name="registerbtn"]').removeClass('disabled');
                        }


                        $.each(response.data, function (i) {
                            //alert(response.data[i]);
                            $(object).val(response.data[i]);
                        });

                    }, 'json');
                });


            });
            $('input[name=suggest_locality]').typeahead(
                {

                    source: function (query, process) {
                        if (query != '')
                            $('.typeahead.dropdown-menu.additionmenu').hide();
                        else {
                            $('.typeahead.dropdown-menu').hide();
                            $('.typeahead.dropdown-menu.additionmenu').show();
                        }
                        return $.post('/assets/components/suggest/getLocalitiesForSuggestJSON.php', {'name': query,'lang_key': Polylang_key}, function (response) {
                            $('input[name=suggest_locality]').focus();
                            var data = new Array();
                            var records = response.content.records;
                            $.each(response.content.records, function (i) {
                                $.each(response.content.records[i], function (key, val) {
                                    data.push(key + '_' + val);
                                });
                                return process(data);
                            });
                        }, 'json');
                    },
                    matcher: function (item) {
                        return true;
                        if (item.toLowerCase().indexOf(this.query.trim().toLowerCase()) != -1) {
                            return true;
                        }
                    },
                    sorter: function (items) {
                        return items.sort();
                    },
                    highlighter: function (item) {
                        var parts = item.split('_');
                        parts.shift();
                        return parts.join('_');
                    },
                    updater: function (item) {
                        var parts = item.split('_');
                        var cityId = parts.shift();
                        if (cityId != 0) {
                            setTimeout(function () {
                                $('.deliverieswrap .col-xs-8 .custom-select-container').addClass('loading');
                            }, 200);
                            $.post('/assets/components/delivery/getdeliverydata.php', {'city_id': cityId}, function (deliveries) {
                                $('#deliveries').prop('disabled', false);
                                $('#deliveries').html('');
                                $('#deliveries').customSelect('update');
                                $('#deliveries').html(deliveries);
                                var getFirstDelivery = $("#deliveries option:first").val();
                                $('#deliveries').customSelect('update');
                                $('#deliveries').prop('selectedIndex', getFirstDelivery);
                                $('#deliveries option:first').prop('selected', true);
                                var getFirstWarehouse = $("#deliveries option:first").val();
                                if (getFirstWarehouse == 4) {
                                    $('#warehouse-block').addClass('hide');
                                    $('#courier-block').removeClass('hide');
                                }
                                else if (getFirstWarehouse == 1) {
                                    $('#warehouse-block').addClass('hide');
                                    $('#samovivoz-block').removeClass('hide');
                                    $('#courier-block').addClass('hide');
                                }
                                else {
                                    $('#courier-block').addClass('hide');
                                    $('#samovivoz-block').addClass('hide');
                                    $('#warehouse-block').addClass('hide');
                                }
                                setTimeout(function () {
                                    $('.deliverieswrap .col-xs-8 .custom-select-container').removeClass('loading');
                                }, 500);
                                $('.deliverieswrap .col-xs-8 .custom-select-container').customSelect('update');
                            }, 'json');
                            $("input[name=city]").val(parts);
                            $("input[name='extended[city_id]']").val(cityId);
                            return parts.join('_');
                        } else return '';

                    },
                    autoSelect: false, highlight: false, minLength: 0, delay: 400, items: 10
                });


            var $input = $('input[name=suggest_locality]');
            $input.blur(function () {
                if ($("input[name='extended[city_id]']").val() == '') {
                    $input.val('');
                }
            });
            $input.change(function () {
                $("input[name='extended[city_id]']").val('');
                var current = $input.typeahead("getActive");
                if (current != undefined) {
                    var item = current;
                    if (item != '') current = item.split('_');
                }
                if ((current) && (current != 'undefined')) {
                    if (current[0] != 0) {
                        if (current[1] == $input.val()) $input.val(current[1]);
                        $("input[name='extended[city_id]']").val(current[0]);
                    } else {
                        $input.val('');
                        $("input[name='extended[city_id]']").val('');
                    }
                } else {
                    $input.val('');
                    $("input[name='extended[city_id]']").val('');
                }
                /*}else{$input.val('');
                }*/
            });
        },
        initialize: function () {
            miniShop2.Order.setup();
            if ($(miniShop2.Order.order).length) {
                miniShop2.$doc.on('click', miniShop2.Order.order + ' [name="' + miniShop2.actionName + '"][value="order/clean"]',
                    function (e) {
                        miniShop2.Order.clean();
                        e.preventDefault();
                    }).on('change', miniShop2.Order.order + ' input[name!="suggest_locality"], textarea', function (e) {

                    //alert('ddddd');
                    var $this = $(this);
                    var key = $this.attr('name');
                    var value = $this.val();
                    miniShop2.Order.add(key, value);
                });
                var $deliveryInputChecked = $(miniShop2.Order.deliveryInput + ':checked', miniShop2.Order.order);
                $deliveryInputChecked.trigger('change');
                miniShop2.Order.updatePayments($deliveryInputChecked.data('payments'));
            }
        }, updatePayments: function (payments) {
            var $paymentInputs = $(miniShop2.Order.paymentInput, miniShop2.Order.order);
            $paymentInputs.attr('disabled', true).prop('disabled', true).closest(miniShop2.Order.inputParent).hide();
            if (payments != undefined) {
                if (payments.length > 0) {
                    for (var i in payments) {
                        if (payments.hasOwnProperty(i)) {
                            $paymentInputs.filter(miniShop2.Order.paymentInputUniquePrefix + payments[i]).attr('disabled', false).prop('disabled', false).closest(miniShop2.Order.inputParent).show();
                        }
                    }
                }
            }
            if ($paymentInputs.filter(':visible:checked').length == 0) {
                $paymentInputs.filter(':visible:first').trigger('click');
            }
        }, add: function (key, value) {
            var callbacks = miniShop2.Order.callbacks;
            var old_value = value;
            callbacks.add.response.success = function (response) {
                (function (key, value, old_value) {
                    var $field = $('[name="' + key + '"]', miniShop2.Order.order);
                    switch (key) {
                        case'delivery':
                            $field = $(miniShop2.Order.deliveryInputUniquePrefix + response.data[key]);
                            if (response.data[key] != old_value) {
                                $field.trigger('click');
                            } else {
                                miniShop2.Order.getRequired(value);
                                miniShop2.Order.updatePayments($field.data('payments'));
                                miniShop2.Order.getcost();
                            }
                            break;
                        case'payment':
                            $field = $(miniShop2.Order.paymentInputUniquePrefix + response.data[key]);
                            if (response.data[key] != old_value) {
                                $field.trigger('click');
                            } else {
                                miniShop2.Order.getcost();
                            }
                            break;
                    }
                    $field.val(response.data[key]).removeClass('error').closest(miniShop2.Order.inputParent).removeClass('error');
                })(key, value, old_value);
            }
            callbacks.add.response.error = function (response) {
                (function (key) {
                    var $field = $('[name="' + key + '"]', miniShop2.Order.order);
                    if ($field.attr('type') == 'checkbox' || $field.attr('type') == 'radio') {
                        $field.closest(miniShop2.Order.inputParent).addClass('error');
                    } else {
                        $field.addClass('error');
                    }
                })(key);
            }
            var data = {key: key, value: value};
            data[miniShop2.actionName] = 'order/add';
            miniShop2.send(data, miniShop2.Order.callbacks.add, miniShop2.Callbacks.Order.add);
        }, getcost: function () {
            var callbacks = miniShop2.Order.callbacks;
            callbacks.getcost.response.success = function (response) {
                $(miniShop2.Order.orderCost, miniShop2.Order.order).text(miniShop2.Utils.formatPrice(response.data['cost']));
            }
            var data = {};
            data[miniShop2.actionName] = 'order/getcost';
            miniShop2.send(data, miniShop2.Order.callbacks.getcost, miniShop2.Callbacks.Order.getcost);
        }, clean: function () {
            var callbacks = miniShop2.Order.callbacks;
            callbacks.clean.response.success = function (response) {
                location.reload();
            }
            var data = {};
            data[miniShop2.actionName] = 'order/clean';
            miniShop2.send(data, miniShop2.Order.callbacks.clean, miniShop2.Callbacks.Order.clean);
        },
        promocode: function () {

            var callbacks = miniShop2.Order.callbacks;
            callbacks.promocode.response.success = function (response) {
                $.each(response.data.cart, function (i, val) {
                    var product = $('.basket-list').find('*[data-product-key=' + val.key + ']');
                    if (val.newaction >= 0)
                        $(product).find('.col-xs-2 .product-price b').html(val.newaction);
                    if (val.newprice) {
                        cost = val.newprice.toString().split('.', 2);
                        $(product).find('.price-large b').text(cost[0]);
                        if (cost[1] == undefined) $(product).find('.price-large sup.small').text('00');
                        else $(product).find('.price-large sup.small').text(cost[1]);
                    } else {
                    }
                });
                if (response.data.economy) {
                    economy = response.data.economy.toString().split('.', 2);
                    if (economy[1] == undefined) economylit = '00'; else economylit = economy[1].toString();
                    eco = economy[0] + '<sup>' + economylit.toString() + '</sup>';
                    $('#discountsumma').html(eco);
                } else {
                    eco = '<b>0</b><sup>00</sup>';
                    $('#discountsumma').html(eco);
                }
                $('.total_cost_order').val(response.data.total_cost);
                $('#summadostavkaorder').html(response.data.cost_delivery);
                cost = response.data.total_cost_withdelivery.toString().split('.', 2);
                if (cost[1] == undefined) costlit = '00'; else costlit = cost[1].toString();
                co = cost[0] + '<sup>' + costlit.toString() + '</sup>';
                $('#totalcart').html(co);
            }
            var data = {};
            data[miniShop2.actionName] = 'order/promocode';
            data['promocode'] = $('#promocode').val();
            data['cityid'] = $('#cityidorder').val();
            data['delivery'] = $("#deliveries").val();
            data['payment'] = $('#payments').val();
//alert('test');
            miniShop2.send(data, miniShop2.Order.callbacks.promocode, miniShop2.Callbacks.Order.promocode);

        },
        submitpayment: function () {
            //alert('submitpayment');return false;
            miniShop2.Message.close();
            $('#msOrder').addClass('loading');
            if (miniShop2.ajaxProgress) {
                miniShop2.$doc.ajaxComplete(function () {
                    miniShop2.ajaxProgress = false;
                    miniShop2.$doc.unbind('ajaxComplete');
                    miniShop2.Order.submit();
                });
                //console.log('submitpayment false');
                return false;
            }
            //console.log('submitpayment false2');
            //return false;
            var callbacks = miniShop2.Order.callbacks;
            callbacks.submit.before = function () {
                $(':button, a', miniShop2.Order.order).attr('disabled', true).prop('disabled', true);
            }
            callbacks.submit.ajax.always = function (xhr) {
                $(':button, a', miniShop2.Order.order).attr('disabled', false).prop('disabled', false);
            }
            callbacks.submit.response.success = function (response) {
                $('#msOrder').removeClass('loading');
                //alet(response.data['ecommerc']);
                if (response.data['ecommerc']) {

                    //response.data['ecommerc']
                    var scr = document.createElement("script");
                    scr.type = "text/javascript";

// We have to use .text for IE, .textContent for standards compliance.

                    scr.text = response.data['ecommerc'];

// Finally, insert the script element into the div
                    document.getElementById("msOrder").appendChild(scr);


                }
                setTimeout(function () {
                    if (response.data['redirect']) {
                        document.location.href = response.data['redirect'];
                    } else if (response.data['msorder']) {
                        document.location.href = /\?/.test(document.location.href) ? document.location.href + '&msorder=' + response.data['msorder'] : document.location.href + '?msorder=' + response.data['msorder'];
                    } else {
                        location.reload();
                    }
                }, 1000);
            }
            callbacks.submit.response.error = function (response) {
                $('#msOrder').removeClass('loading');
                $('[name]', miniShop2.Order.order).removeClass('error').closest(miniShop2.Order.inputParent).removeClass('error');
                for (var i in response.data) {
                    if (response.data.hasOwnProperty(i)) {
                        var key = response.data[i];
                        var $field = $('[name="' + key + '"]', miniShop2.Order.order);
                        if ($field.attr('type') == 'checkbox' || $field.attr('type') == 'radio') {
                            $field.closest(miniShop2.Order.inputParent).addClass('error');
                        } else {
                            $field.addClass('error');
                        }
                    }
                }
            }
            return miniShop2.send(miniShop2.sendData.formData, miniShop2.Order.callbacks.submit, miniShop2.Callbacks.Order.submit);
        },
        submit: function () {
            //alert('order');return false;
            miniShop2.Message.close();
            $('#msOrder').addClass('loading');
            if (miniShop2.ajaxProgress) {
                miniShop2.$doc.ajaxComplete(function () {
                    miniShop2.ajaxProgress = false;
                    miniShop2.$doc.unbind('ajaxComplete');
                    miniShop2.Order.submit();
                });
                //console.log('submitpayment false');
                return false;
            }
            //console.log('submitpayment false2');
            //return false;
            var callbacks = miniShop2.Order.callbacks;
            callbacks.submit.before = function () {
                $(':button, a', miniShop2.Order.order).attr('disabled', true).prop('disabled', true);
            }
            callbacks.submit.ajax.always = function (xhr) {
                $(':button, a', miniShop2.Order.order).attr('disabled', false).prop('disabled', false);
            }
            callbacks.submit.response.success = function (response) {
                $('#msOrder').removeClass('loading');
                /*if (response.data['redirect']) {
                    console.log(response.data['redirect']);
                    //document.location.href = response.data['redirect'];
                }
                else*/
                //if (response.data['msorder']) {
                document.location.href = "/order?msorder=" + response.data['msorder'];
                ///\?/.test(document.location.href) ? document.location.href + '&msorder=' + response.data['msorder'] : document.location.href + '?msorder=' + response.data['msorder'];
                //} else {
                //    location.reload();
                //}


            }
            callbacks.submit.response.error = function (response) {
                $('#msOrder').removeClass('loading');
                $('[name]', miniShop2.Order.order).removeClass('error').closest(miniShop2.Order.inputParent).removeClass('error');
                for (var i in response.data) {
                    if (response.data.hasOwnProperty(i)) {
                        var key = response.data[i];
                        var $field = $('[name="' + key + '"]', miniShop2.Order.order);
                        if ($field.attr('type') == 'checkbox' || $field.attr('type') == 'radio') {
                            $field.closest(miniShop2.Order.inputParent).addClass('error');
                        } else {
                            $field.addClass('error');
                        }
                    }
                }
            }
            return miniShop2.send(miniShop2.sendData.formData, miniShop2.Order.callbacks.submit, miniShop2.Callbacks.Order.submit);
        },

        submitquick: function () {
            miniShop2.Message.close();
            $('#msOrderQuick').addClass('loading');
            if (miniShop2.ajaxProgress) {
                miniShop2.$doc.ajaxComplete(function () {
                    miniShop2.ajaxProgress = false;
                    miniShop2.$doc.unbind('ajaxComplete');
                    miniShop2.Order.msOrderQuick();
                });
                return false;
            }
            var callbacks = miniShop2.Order.callbacks;
            callbacks.submit.before = function () {
                $(':button, a', miniShop2.Order.order).attr('disabled', true).prop('disabled', true);
            }
            callbacks.submit.ajax.always = function (xhr) {
                $(':button, a', miniShop2.Order.order).attr('disabled', false).prop('disabled', false);
            }
            callbacks.submit.response.success = function (response) {
                $('#msOrderQuick').removeClass('loading');
                if (response.data['redirect']) {
                    document.location.href = response.data['redirect'];
                }
                else if (response.data['msorder']) {
                    document.location.href = /\?/.test(document.location.href) ? document.location.href + '&msorder=' + response.data['msorder'] : document.location.href + '?msorder=' + response.data['msorder'];
                }
                else {
                    location.reload();
                }
            }
            callbacks.submit.response.error = function (response) {
                $('#msOrder').removeClass('loading');
                $('[name]', miniShop2.Order.order).removeClass('error').closest(miniShop2.Order.inputParent).removeClass('error');
                for (var i in response.data) {
                    if (response.data.hasOwnProperty(i)) {
                        var key = response.data[i];
                        var $field = $('[name="' + key + '"]', miniShop2.Order.order);
                        if ($field.attr('type') == 'checkbox' || $field.attr('type') == 'radio') {
                            $field.closest(miniShop2.Order.inputParent).addClass('error');
                        }
                        else {
                            $field.addClass('error');
                        }
                    }
                }
            }
            return miniShop2.send(miniShop2.sendData.formData, miniShop2.Order.callbacks.submit, miniShop2.Callbacks.Order.submit);
        },


        getRequired: function (value) {
            var callbacks = miniShop2.Order.callbacks;
            callbacks.getRequired.response.success = function (response) {
                $('[name]', miniShop2.Order.order).removeClass('required').closest(miniShop2.Order.inputParent).removeClass('required');
                var requires = response.data['requires'];
                for (var i = 0, length = requires.length; i < length; i++) {
                    $('[name=' + requires[i] + ']', miniShop2.Order.order).addClass('required').closest(miniShop2.Order.inputParent).addClass('required');
                }
            };
            callbacks.getRequired.response.error = function (response) {
                $('[name]', miniShop2.Order.order).removeClass('required').closest(miniShop2.Order.inputParent).removeClass('required');
            }
            var data = {id: value};
            data[miniShop2.actionName] = 'order/getrequired';
            miniShop2.send(data, miniShop2.Order.callbacks.getRequired, miniShop2.Callbacks.Order.getRequired);
        }, get: function (order_id) {
            var callbacks = miniShop2.Order.callbacks;
            callbacks.get.response.success = function (response) {
                $(miniShop2.Order.orderCost, miniShop2.Order.order).text(miniShop2.Utils.formatPrice(response.data['cost']));
                console.log('success');
            };
            callbacks.submit.response.error = function (response) {
                console.log('error');
            }
            var data = {};
            data[miniShop2.actionName] = 'order/get';
            miniShop2.send(data, miniShop2.Order.callbacks.get, miniShop2.Callbacks.Order.get);
        }
    };
    miniShop2.Message = {
        initialize: function () {
            if (typeof $.fn.jGrowl != 'undefined') {
                $.jGrowl.defaults.closerTemplate = '<div>[ ' + miniShop2Config.close_all_message + ' ]</div>';
                miniShop2.Message.close = function () {
                    $.jGrowl('close');
                }
                miniShop2.Message.show = function (message, options) {
                    if (!message) return;
                    $.jGrowl(message, options);
                }
            } else {
                miniShop2.Message.close = function () {
                };
                miniShop2.Message.show = function (message) {
                    if (message) {
                        alert(message);
                    }
                };
            }
        }, success: function (message) {
            miniShop2.Message.show(message, {theme: 'ms2-message-success', sticky: false});
        }, error: function (message) {
            miniShop2.Message.show(message, {theme: 'ms2-message-error', sticky: false});
        }, info: function (message) {
            miniShop2.Message.show(message, {theme: 'ms2-message-info', sticky: false});
        }
    };
    miniShop2.Utils = {
        empty: function (val) {
            return (typeof(val) == 'undefined' || val == 0 || val === null || val === false || (typeof(val) == 'string' && val.replace(/\s+/g, '') == '') || (typeof(val) == 'array' && val.length == 0));
        }, formatPrice: function (price) {
            var pf = miniShop2Config.price_format;
            price = this.number_format(price, pf[0], pf[1], pf[2]);
            if (miniShop2Config.price_format_no_zeros) {
                price = price.replace(/(0+)$/, '');
                price = price.replace(/[^0-9]$/, '');
            }
            return price;
        }, formatWeight: function (weight) {
            var wf = miniShop2Config.weight_format;
            weight = this.number_format(weight, wf[0], wf[1], wf[2]);
            if (miniShop2Config.weight_format_no_zeros) {
                weight = weight.replace(/(0+)$/, '');
                weight = weight.replace(/[^0-9]$/, '');
            }
            return weight;
        }, number_format: function (number, decimals, dec_point, thousands_sep) {
            var i, j, kw, kd, km;
            if (isNaN(decimals = Math.abs(decimals))) {
                decimals = 2;
            }
            if (dec_point == undefined) {
                dec_point = ",";
            }
            if (thousands_sep == undefined) {
                thousands_sep = ".";
            }
            i = parseInt(number = (+number || 0).toFixed(decimals)) + "";
            if ((j = i.length) > 3) {
                j = j % 3;
            } else {
                j = 0;
            }
            km = (j ? i.substr(0, j) + thousands_sep : "");
            kw = i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + thousands_sep);
            kd = (decimals ? dec_point + Math.abs(number - i).toFixed(decimals).replace(/-/, '0').slice(2) : '');
            return km + kw + kd;
        }, getValueFromSerializedArray: function (name, arr) {
            if (!$.isArray(arr)) {
                arr = miniShop2.sendData.formData;
            }
            for (var i = 0, length = arr.length; i < length; i++) {
                if (arr[i].name = name) {
                    return arr[i].value;
                }
            }
            return null;
        }
    };
    $(document).ready(function ($) {

        miniShop2.initialize();
        var html = $('html');
        html.removeClass('no-js');
        if (!html.hasClass('js')) {
            html.addClass('js');
        }
    });
})(this, document, jQuery);


/*/assets/components/msearch2/js/web/default.js"*/

var mSearch2 = {
    options: {
        wrapper: '#mse2_mfilter'
        ,
        filters: '#mse2_filters'
        ,
        results: '#mse2_results'
        ,
        total: '#mse2_total'
        ,
        pagination: '#mse2_pagination'
        ,
        sort: '#mse2_sort'
        ,
        limit: '#mse2_limit'
        ,
        slider: '.mse2_number_slider'
        ,
        selected: '#mse2_selected'

        ,
        pagination_link: '#mse2_pagination a'
        //,sort_link: '#mse2_sort a'
        ,
        sort_link: '#mse2_sort a'
        ,
        tpl_link: '#mse2_tpl a'
        //,selected_tpl: '<a href="#" data-id="[[+id]]" class="mse2_selected_link"><em>[[+title]]</em><sup>x</sup></a>'
        ,
        selected_tpl: '<a href="#" data-id="[[+id]]" data-brand="[[+urlbrand]]" class="choose-el mse2_selected_link"><span class="icon del-icon"></span>[[+title]]</a>'

        ,
        active_class: 'active'
        ,
        disabled_class: 'disabled'
        ,
        disabled_class_fieldsets: 'disabled_fieldsets'
        ,
        prefix: 'mse2_'
        ,
        m: 0
        ,
        max: 4000
        ,
        suggestion: 'i' // inside filter item, e.g. #mse2_filters
    }
    , sliders: {}
    , initialize: function (selector) {
        this.options['m'] = $('#pricekg_min').text();
        this.options['ma'] = $('#pricekg_max').text();

        var elements = ['filters', 'results', 'pagination', 'total', 'sort', 'selected', 'limit'];
        for (var i in elements) {
            if (elements.hasOwnProperty(i)) {
                var elem = elements[i];
                //alert(selector);
                this[elem] = $(selector).find(this.options[elem]);
                //alert(elem+' '+this[elem].html());
                if (!this[elem].length) {
                    //console.log('Error: could not initialize element "' + elem + '" with selector "' + this.options[elem] + '".');
                }
            }
        }

        this.handlePagination();
        this.handleSort();
        this.handleTpl();
        this.handleSlider();
        this.handleLimit();

        $(document).on('submit', this.options.filters, function (e) {
            mse2Config.page = '';
            mSearch2.load();
            return false;
        });

        $(document).on('change', this.options.filters, function (e) {
            return $(this).submit();
        });

        if (this.selected) {
            var selectors = [
                this.options.filters + ' input[type="checkbox"]',
                this.options.filters + ' input[type="radio"]',
                this.options.filters + ' select'
            ];
            $(document).on('change', selectors.join(', '), function (e) {
                mSearch2.handleSelected($(this));
            });

            selectors = [
                'input[type="checkbox"]:checked',
                'input[type="radio"]:checked',
                'select'
            ];
            this.filters.find(selectors.join(', ')).each(function (e) {
                //alert($(this).val());
                mSearch2.handleSelected($(this));
            });
            params = this.getFilters();

            $(document).on('click', this.options.selected + ' a', function (e) {
                var id = $(this).data('id').replace(mse2Config.filter_delimeter, "\\" + mse2Config.filter_delimeter);
                //alert( $(this).parent().html());
                var elem = $('#' + id);
                //alert(id);

                var ur = '';
                //alert($(this).data('brand'));
                if ($(this).data('brand') && ($(this).data('brand') != '')) {
                    br = $(this).data('brand');
                    var posbr = location.href.indexOf(br);
                    var pos = window.location.href.indexOf('?');
                    //ms|vendor
                    //alert(pos);

                    if (pos != -1) {
                        if (posbr != '-1') {
                            //var
                            ur = location.href.replace('/' + $(this).data('brand'), '');
                            ur = ur.replace(',' + elem.val(), '');
                            ur = ur.replace(elem.val() + ',', '');
                            ur = ur.replace(elem.val(), '');

                            //ur = ur.split('?');
                            //alert(ur[1]);
                            //alert(ur);

                            //location.href.replace('/'+$(this).data('brand'),'');
                        }
                    }
                    //return;
                }

                if (elem[0]) {
                    switch (elem[0].tagName) {
                        case 'INPUT':

                            elem.trigger('click');
                            break;
                        case 'SELECT':
                            elem.find('option:first').prop('selected', true).trigger('change');
                            break;
                    }
                }
                if (ur != '') location.href = ur;//ur[0];
                return false;
            });
        }
        mSearch2.setEmptyFieldsets();
        mSearch2.setTotal(this.total.text());
        return true;
    }


    , handlePagination: function () {
        $(document).on('click', this.options.pagination_link, function (e) {
            if (!$(this).hasClass(mSearch2.options.active_class)) {
                $(mSearch2.options.pagination).removeClass(mSearch2.options.active_class);
                $(this).addClass(mSearch2.options.active_class);

                var tmp = $(this).prop('href').match(/page[=|\/](\d+)/);
                var page = tmp && tmp[1] ? Number(tmp[1]) : 1;
                //alert(tmp);
                mse2Config.page = (page != mse2Config.start_page) ? page : '';
                //alert(mse2Config.page);


                /*for (var arr in inputsdop) {
                    inp=inputsdop[arr];
                    if (jQuery(inp).is(':checked')) {
                        name=jQuery(inp).attr('name');

                        value=jQuery(inp).val();

                        if (name) {
                            data[name] += mse2Config.values_delimeter + value;
                        }
                        else {
                            data[name] = value;
                        }
                    }
                }*/
                /*

                                https://petchoice.ua/koshkam/suxoj-korm?vkus=kurica&sort=hits&sortdir=desc&_ga=GA1.2.162876483.1506076398&jv_visits_count_F5vWBfK7QG=8&biatv-cookie=%7B%22firstVisitAt%22%3A1506076400%2C%22visitsCount%22%3A14%2C%22campaignCount%22%3A1%2C%22currentVisitStartedAt%22%3A1508843413%2C%22currentVisitOpenPages%22%3A19%2C%22location%22%3A%22https%3A%2F%2Fpetchoice.ua%2Fkoshkam%2Fsuxoj-korm%22%2C%22userAgent%22%3A%22Mozilla%2F5.0+%28Windows+NT+10.0%3B+WOW64%3B+rv%3A55.0%29+Gecko%2F20100101+Firefox%2F55.0%22%2C%22language%22%3A%22ru-ru%22%2C%22encoding%22%3A%22utf-8%22%2C%22screenResolution%22%3A%221760x1100%22%2C%22currentVisitUpdatedAt%22%3A1508846149%2C%22utmDataCurrent%22%3A%7B%22utm_source%22%3A%22%28direct%29%22%2C%22utm_medium%22%3A%22%28none%29%22%2C%22utm_campaign%22%3A%22%28direct%29%22%2C%22utm_content%22%3A%22%28not+set%29%22%2C%22utm_term%22%3A%22%28not+set%29%22%2C%22beginning_at%22%3A1506076400%7D%2C%22campaignTime%22%3A1506076400%2C%22utmDataFirst%22%3A%7B%22utm_source%22%3A%22%28direct%29%22%2C%22utm_medium%22%3A%22%28none%29%22%2C%22utm_campaign%22%3A%22%28direct%29%22%2C%22utm_content%22%3A%22%28not+set%29%22%2C%22utm_term%22%3A%22%28not+set%29%22%2C%22beginning_at%22%3A1506076400%7D%2C%22geoipData%22%3A%7B%22country%22%3A%22Ukraine%22%2C%22region%22%3A%22Odes%27ka+Oblast%27%22%2C%22city%22%3A%22Odessa%22%2C%22org%22%3A%22NetArt+Group+s.r.o.%22%7D%7D&Tickets_User=1e7f0269a4b6c11f5a25a5f0ce107885&_ym_uid=1507300537171241086&PHPSESSID=4f26e37fdcea1f126260b8b8e9f9f02c&minishop2-category-grid-10=%7B%22start%22%3A0%2C%22limit%22%3A100%2C%22action%22%3A%22mgr%2Fproduct%2Fgetlist%22%2C%22parent%22%3A%2210%22%2C%22query%22%3A%22Trixie+%25u041F%25u0435%25u043B%25u0435%25u043D%25u043A%25u0438+%25u0434%25u043B%25u044F+%25u0441%25u043E%25u0431%25u0430%25u043A+40%25u044560%22%7D&_gid=GA1.2.1884908137.1508843411&_ym_visorc_32576380=w&countpage=4&timestart=Tue+Oct+24+2017+14%3A10%3A12+GMT%2B0300&_ym_isad=2&jv_enter_ts_F5vWBfK7QG=1508843413639&jv_utm_F5vWBfK7QG=&bingc-activity-data=%7B%22numberOfImpressions%22%3A2%2C%22activeFormSinceLastDisplayed%22%3A1074%2C%22pageviews%22%3A20%2C%22callWasMade%22%3A0%2C%22updatedAt%22%3A1508846151%7D&_dc_gtm_UA-67805669-1=1&limit=24
                */
                mSearch2.load('', function () {
                    $('html, body').animate({
                        scrollTop: $(mSearch2.options.wrapper).position().top || 0
                    }, 0);
                });

                //$('#showmore .count').html(24);
                //$('#showmore').show();
            }

            return false;
        });
    }

    , handleSort: function () {
        var params = this.Hash.get();

        if (params.sort) {
            var sorts = params.sort.split(mse2Config.values_delimeter);
            for (var i = 0; i < sorts.length; i++) {
                var tmp = sorts[i].split(mse2Config.method_delimeter);
                if (tmp[0] && tmp[1]) {
                    $(this.options.sort_link + '[data-sort="' + tmp[0] + '"]').data('dir', tmp[1]).attr('data-dir', tmp[1]).addClass(this.options.active_class);
                    if (tmp[1] == 'asc')
                        $(this.options.sort_link + '[data-sort="' + tmp[0] + '"]').addClass('up-sort');
                    else $(this.options.sort_link + '[data-sort="' + tmp[0] + '"]').addClass('down-sort');

                }
            }
        }

        $(document).on('click', this.options.sort_link, function (e) {

            $(mSearch2.options.sort_link).removeClass(mSearch2.options.active_class);
            $('.filter-link').removeClass('active');
            $('.filter-link').removeClass('unactive');
            $('.filter-link').removeClass('up-sort');
            $('.filter-link').removeClass('down-sort');
            $(this).addClass(mSearch2.options.active_class);
            var dir;
            if ($(this).data('dir').length == 0) {
                dir = $(this).data('default');
            }
            else {
                dir = $(this).data('dir') == 'desc'
                    ? 'asc'
                    : 'desc';
            }

            $(mSearch2.options.sort_link).data('dir', '').attr('data-dir', '');
            $(this).data('dir', dir).attr('data-dir', dir);

            var sort = $(this).data('sort');
            //alert(sort);

            //$(this).removeClass('up-sort');$(this).removeClass('down-sort');
            if (dir == 'desc') $(this).addClass('down-sort');
            else $(this).addClass('up-sort');

            if (dir) {
                //sort += '&sortdir='+dir;//mse2Config.method_delimeter + dir;
            }
            mse2Config.sort = (sort != mse2Config.start_sort) ? sort : '';
            mse2Config.sortdir = dir;

            mSearch2.load();

            return false;
        });
    }

    , handleTpl: function () {
        $(document).on('click', this.options.tpl_link, function (e) {

            if (!$(this).hasClass(mSearch2.options.active_class)) {
                $(mSearch2.options.tpl_link).removeClass(mSearch2.options.active_class);
                $(this).addClass(mSearch2.options.active_class);

                var tpl = $(this).data('tpl');
                mse2Config.tpl = (tpl != mse2Config.start_tpl && tpl != 0) ? tpl : '';

                mSearch2.load();
            }

            return false;
        });
    }

    , handleSlider: function () {
        if (!$(mSearch2.options.slider).length) {
            return false;
        }
        else if (!$.ui || !$.ui.slider) {
            return mSearch2.loadJQUI(mSearch2.handleSlider);
        }
        $(mSearch2.options.slider).each(function () {
            var fieldset = $(this).parents('fieldset');
            var imin = fieldset.find('input.minCost');
            var imax = fieldset.find('input.maxCost');
            var vmin = Number(imin.val());
            var vmax = Number(imax.val());
            var $this = $(this);
            console.log(vmax);
            console.log(vmin);
            m = Number($('#pricekg_min').text());
            ma = Number($('#pricekg_max').text());
            //alert(mSearch2.options['m']);
            //alert(mSearch2.options['ma']);
            $this.slider({
                min: m//vmin//mSearch2.options['m']
                , max: ma//vmax//mSearch2.options['ma']
                , values: [vmin, vmax]
                , range: true
                //,step: 0.1
                , stop: function (event, ui) {
                    imin.val($this.slider('values', 0));
                    imax.val($this.slider('values', 1));
                    imin.trigger('change');
                },
                slide: function (event, ui) {
                    imin.val($this.slider('values', 0));
                    imax.val($this.slider('values', 1));
                }
            });

            var name = imin.prop('name');
            var values = mSearch2.Hash.get();
            if (values[name]) {
                var tmp = values[name].split(mse2Config.values_delimeter);
                if (tmp[0].match(/(?!^-)[^0-9\.]/g)) {
                    tmp[0] = tmp[0].replace(/(?!^-)[^0-9\.]/g, '');
                }
                if (tmp.length > 1) {
                    if (tmp[1].match(/(?!^-)[^0-9\.]/g)) {
                        tmp[1] = tmp[1].replace(/(?!^-)[^0-9\.]/g, '');
                    }
                }
                imin.val(tmp[0]);
                imax.val(tmp.length > 1 ? tmp[1] : tmp[0]);
            }


            if (values[name]) {
                imin.add(imax).trigger('click');
            }

            mSearch2.sliders[name] = [vmin, vmax];
        });
        return true;
    }

    , handleLimit: function () {
        $(document).on('change', this.options.limit, function (e) {
            var limit = $(this).val();
            mse2Config.page = '';
            if (limit == mse2Config.start_limit) {
                mse2Config.limit = '';
            }
            else {
                mse2Config.limit = limit;
            }
            mSearch2.load();
        });
    }

    , handleSelected: function (input) {
        if (!input[0]) {
            return;
        }
        var id = input.prop('id');
        var idurl = input.data('url');

        var title = '';
        var elem = '';
        //input[0].
        //alert(input[0].tagName);
        switch (input[0].tagName) {
            case 'INPUT':
                var label = input.next('label');
                var match = label.html().match(/>(.*?)</);
                if (match && match[1]) {
                    title = match[1].replace(/(\s$)/, '');
                }

                title = label.children('span').eq(0).text();
                //alert('title '+title);
                brandurl = label.children('span').eq(0).data('url');

                if (input.is(':checked')) {

                    elem = this.options.selected_tpl.replace('[[+id]]', id).replace('[[+title]]', title).replace('[[+urlbrand]]', brandurl);
                    elem = elem.replace('топ', '<sup>топ</sup>');
                    if ($('.mse2_selected_link[data-id="' + id + '"]').html() == undefined)//$('#mse2_selected_link').f
                    {
                        this.selected.find('span.spanselected').append(elem);
                    }
                }
                else {
                    $('[data-id="' + id + '"]', this.selected).remove();
                }
                break;

            case 'SELECT':
                var option = input.find('option:selected');
                $('[data-id="' + id + '"]', this.selected).remove();
                if (input.val()) {
                    title = ' ' + option.text().replace(/(\(.*\)$)/, '');
                    elem = this.options.selected_tpl.replace('[[+id]]', id).replace('[[+title]]', title);
                    elem = elem.replace('топ', '<sup>топ</sup>');
                    //this.selected.find('span').append(elem);
                    if ($('.mse2_selected_link[data-id="' + id + '"]').html() == undefined)//$('#mse2_selected_link').f
                    {
                        this.selected.find('span').append(elem);
                    }
                }
                break;
        }

        if (this.selected.find('a').length) {
            this.selected.show();
        }
        else {
            this.selected.hide();
        }
    }

    , load: function (params, callback) {
        if (!params) {
            params = this.getFilters();
        }
        if (mse2Config[mse2Config.queryVar] != '') {
            params[mse2Config.queryVar] = mse2Config[mse2Config.queryVar];
        }
        if (mse2Config[mse2Config.parentsVar] != '') {
            params[mse2Config.parentsVar] = mse2Config[mse2Config.parentsVar];
        }
        if (mse2Config.sort != '') {
            params.sort = mse2Config.sort;
        }
        //if (mse2Config.sort != '') {params.sort = mse2Config.sort;}
        //alert(mse2Config.sort);
        if (mse2Config.sort == '') params.sort = 'hits';
        else params.sort = mse2Config.sort;
        if (mse2Config.sortdir == undefined) {
            params.sortdir = 'desc';
        } else {
            params.sortdir = mse2Config.sortdir;
        }
        //alert(params.sortdir);

        //alert(params.sortdir);
        if (mse2Config.tpl != '') {
            params.tpl = mse2Config.tpl;
        }
        if (mse2Config.page > 0) {
            params.page = mse2Config.page;
        }
        if (mse2Config.limit > 0) {
            params.limit = mse2Config.limit;
        }
//alert(params[mse2Config.parentsVar]);
        for (var i in this.sliders) {
            if (this.sliders.hasOwnProperty(i) && params[i]) {
                if (this.sliders[i].join(mse2Config.values_delimeter) == params[i]) {
                    //alert(params[i]+' '+i);
                    delete params[i];
                }
            }
        }
        //alert('ddd');
        this.Hash.set(params);
        params.action = 'filter';
        params.pageId = mse2Config.pageId;

        this.beforeLoad();
        params.key = mse2Config.key;
        $.post(mse2Config.actionUrl, params, function (response) {
            mSearch2.afterLoad();
            if (response.success) {
                mSearch2.Message.success(response.message);
                mSearch2.results.html(response.data.results);
                mSearch2.pagination.html(response.data.pagination);
                if (response.data.pagination == null) {
                    mSearch2.pagination.closest('.page-buttons').addClass('hidden');
                } else {
                    mSearch2.pagination.closest('.page-buttons').removeClass('hidden');
                }
                mSearch2.setTotal(response.data.total);
                mSearch2.setSuggestions(response.data.suggestions);
                mSearch2.setEmptyFieldsets();
                if (response.data.log) {
                    $('.mFilterLog').html(response.data.log);
                }
                if (callback && $.isFunction(callback)) {
                    callback.call(this, response, params);
                }
                if (parseInt(response.data.ostatok) < 0) {
                    $('#showmore').hide();
                }
                else if (parseInt(response.data.ostatok) < 24) {
                    var ost = parseInt(response.data.ostatok);//24 -
                    $('#showmore .count').html(ost);
                    $('#showmore').show();
                }
                else {
                    $('#showmore .count').html(24);
                    $('#showmore').show();
                }
                //alert(response.data.ostatok);

                $(document).trigger('mse2_load', response);
            }
            else {
                mSearch2.Message.error(response.message);
            }
        }, 'json');
    }

    , getFilters: function () {
        var data = {};
        inputs = $('input[name="vendor"]:checked');//.find('[value="' + value + '"]');
        for (var arr in inputs) {
            inp = inputs[arr];
            if (jQuery(inp).is(':checked')) {
                //alert(jQuery(inp).val());
                value = jQuery(inp).val();
                if (data['vendor']) {
                    data['vendor'] += mse2Config.values_delimeter + value;
                }
                else {
                    data['vendor'] = value;
                }
            }
        }

        /*inputsdop=$('form#mse2_filters input:checked');

        for (var arr in inputsdop) {
            inp=inputsdop[arr];
            if (jQuery(inp).is(':checked')) {
                name=jQuery(inp).attr('name');

                value=jQuery(inp).val();
                if ((value=='undefined')||(value === '')) {continue;}
                if (name) {
                    data[name] += mse2Config.values_delimeter + value;
                }
                else {
                    data[name] = value;
                }
            }
        }
*/

        $.map(this.filters.serializeArray(), function (n, i) {
//alert(n['value']);
            if ((n['value'] === undefined) || (n['value'] === '')) {
                return;
            }
            if (data[n['name']]) {
                data[n['name']] += mse2Config.values_delimeter + n['value'];
            }
            else {
                data[n['name']] = n['value'];
            }
        });

        return data;
    }

    , setSuggestions: function (suggestions) {
        for (var filter in suggestions) {
            if (suggestions.hasOwnProperty(filter)) {
                var arr = suggestions[filter];
                for (var value in arr) {
                    if (arr.hasOwnProperty(value)) {
                        var count = arr[value];
                        var selector = filter.replace(mse2Config.filter_delimeter, "\\" + mse2Config.filter_delimeter);
                        var input = $('.' + mSearch2.options.prefix + selector, mSearch2.filters).find('[value="' + value + '"]');
                        //var input = $('.' + mSearch2.options.prefix + selector).find('[value="' + value + '"]');
                        //alert(mSearch2.options.prefix + selector);
                        //, mSearch2.filters
                        //alert(value);
                        //mse2_vkus
                        //alert(selector);
                        if (!input[0]) {
                            input = $('.commonbrands').find('[value="' + value + '"]');
                            //.mse2_'+selector
                            //alert('#' + mSearch2.options.prefix + selector);
                            //alert('.mse2_'+selector+' '+value);
                            //mse2_attributes|vkus
                            //mse2_attributes|vkus_2
                            if (!input[0]) {
                                continue;
                            }
                        }
//alert('#' + mSearch2.options.prefix + selector);
                        switch (input[0].tagName) {
                            case 'INPUT':
                                var proptype = input.prop('type');
                                if (proptype != 'checkbox' && proptype != 'radio') {
                                    continue;
                                }
                                //var label = $('#' + mSearch2.options.prefix + selector, mSearch2.filters).find('label[for="' + input.prop('id') + '"]');
                                var label = $('.' + mSearch2.options.prefix + selector, mSearch2.filters).find('label[for="' + input.prop('id') + '"]');
                                if (!label[0]) {
                                    //var label = $('#' + mSearch2.options.prefix + selector, mSearch2.filters).find('label[for="' + input.prop('id') + '"]');
                                    //var input = $('#mse2_attributes|'+selector)).find('[value="' + value + '"]');
                                    //var label = ('#mse2_attributes|'+selector).find('label[for="' + input.prop('id') + '"]');
                                }
                                var elem = input.parent().find(mSearch2.options.suggestion);
                                //alert(mSearch2.options.suggestion);
                                //alert(count);
                                elem.text('(' + count + ')');
//alert(count);
                                if (count == 0) {
                                    input.prop('disabled', true);
                                    label.addClass(mSearch2.options.disabled_class);
                                    label.parent().addClass(mSearch2.options.disabled_class);
                                    if (input.is(':checked')) {
                                        input.prop('checked', false);

                                        mSearch2.handleSelected(input);
                                    }
                                }
                                else {
                                    input.prop('disabled', false);
                                    label.removeClass(mSearch2.options.disabled_class);
                                }

                                if (input.is(':checked')) {
                                    elem.hide();
                                }
                                else {
                                    elem.show();
                                }
                                break;

                            case 'OPTION':
                                var text = input.text();
                                var matches = text.match(/\s\(.*\)$/);
                                var src = matches
                                    ? matches[0]
                                    : '';
                                var dst = '';
//alert('option'+count);
                                if (!count) {
                                    input.prop('disabled', true).addClass(mSearch2.options.disabled_class);
                                    if (input.is(':selected')) {
                                        input.prop('selected', false);
                                        //alert('sel');
                                        //mSearch2.handleSelected(input);
                                    }
                                }
                                else {
                                    dst = ' (' + count + ')';
                                    input.prop('disabled', false).removeClass(mSearch2.options.disabled_class);
                                }

                                if (input.is(':selected')) {
                                    dst = '';
                                }

                                if (src) {
                                    text = text.replace(src, dst);
                                }
                                else {
                                    text += dst;
                                }
                                //console.log(count,text)
                                input.text(text);

                                break;
                        }
                    }
                }
            }
        }
    }

    , setEmptyFieldsets: function () {
        this.filters.find('fieldset').each(function (e) {
            var all_children_disabled = $(this).find('label:not(.' + mSearch2.options.disabled_class + ')').length == 0;
            if (all_children_disabled) {
                $(this).addClass(mSearch2.options.disabled_class_fieldsets);
            }
            if (!all_children_disabled && $(this).hasClass(mSearch2.options.disabled_class_fieldsets)) {
                $(this).removeClass(mSearch2.options.disabled_class_fieldsets);
            }
        });
    }

    , setTotal: function (total) {
        if (this.total.length != 0) {
            if (!total || total == 0) {
                this.total.parent().hide();
                this.limit.parent().hide();
                this.sort.hide();
                this.total.text(0);
            }
            else {
                this.total.parent().show();
                this.limit.parent().show();
                this.sort.show();
                this.total.text(total);
            }
        }
    }

    , beforeLoad: function () {
        this.results.css('opacity', .5);
        $(this.options.pagination_link).addClass(this.options.active_class);
        this.filters.find('input, select').prop('disabled', true).addClass(this.options.disabled_class);
    }

    , afterLoad: function () {
        this.results.css('opacity', 1);
        this.filters.find('.' + this.options.disabled_class).prop('disabled', false).removeClass(this.options.disabled_class);
    }

    , loadJQUI: function (callback, parameters) {
        $('<link/>', {
            rel: 'stylesheet',
            type: 'text/css',
            href: mse2Config.cssUrl + 'redmond/jquery-ui-1.10.4.custom.min.css'
        }).appendTo('head');


//alert(mse2Config.jsUrl + 'lib/jquery-ui-1.10.4.custom.min.js');
        //return $.getScript(mse2Config.jsUrl + 'lib/jquery-ui-1.10.4.custom.min.js', function() {

        if (typeof callback == 'function') {

            callback(parameters);
        }
        //});
    }

};

mSearch2.Form = {
    initialize: function (selector) {

        $(selector).each(function () {
            var form = $(this);
            var config = mse2FormConfig[form.data('key')];
            var cache = {};

            if (config.autocomplete == '0' || config.autocomplete == 'false') {
                return false;
            }
            else if (!$.ui || !$.ui.autocomplete) {
                //alert(selector);
                //return mSearch2.loadJQUI(mSearch2.Form.initialize, selector);
            }

            form.find('input[name="' + config.queryVar + '"]').autocomplete({
                source: function (request, callback) {
                    if (request.term in cache) {
                        callback(cache[request.term]);
                        return;
                    }
                    var data = {
                        action: 'search'
                        , key: form.data('key')
                        , pageId: config.pageId
                    };
                    data[config.queryVar] = request.term;
                    $.post(mse2Config.actionUrl, data, function (response) {
                        if (response.data.log) {
                            $('.mSearchFormLog').html(response.data.log);
                        }
                        else {
                            $('.mSearchFormLog').html('');
                        }
                        cache[request.term] = response.data.results;
                        callback(response.data.results)
                    }, 'json');
                }
                , minLength: config.minQuery || 3
                , select: function (event, ui) {
                    if (ui.item.url) {
                        document.location.href = ui.item.url;
                    }
                    else {
                        setTimeout(function () {
                            form.submit();
                        }, 100);
                    }
                }
            })
                .data("ui-autocomplete")._renderItem = function (ul, item) {
                return $("<li></li>")
                    .data("item.autocomplete", item)
                    .addClass("mse2-ac-wrapper")
                    .append("<a href=\"" + item.url + "\"class=\"mse2-ac-link\">" + item.label + "</a>")
                    .appendTo(ul);
            };
            return true;
        });
    }
};

mSearch2.Message = {
    success: function (message) {

    }
    , error: function (message) {
        alert(message);
    }
};

mSearch2.Hash = {
    get: function () {
        var vars = {}, hash, splitter, hashes;
        if (!this.oldbrowser()) {
            var pos = window.location.href.indexOf('?');
            hashes = (pos != -1) ? decodeURIComponent(window.location.href.substr(pos + 1)) : '';
            splitter = '&';
        }
        else {
            hashes = decodeURIComponent(window.location.hash.substr(1));
            splitter = '/';
        }

        if (hashes.length == 0) {
            return vars;
        }
        else {
            hashes = hashes.split(splitter);
        }

        for (var i in hashes) {
            if (hashes.hasOwnProperty(i)) {
                hash = hashes[i].split('=');
                if (typeof hash[1] == 'undefined') {
                    vars['anchor'] = hash[0];
                }
                else {
                    vars[hash[0]] = hash[1];
                }
            }
        }
        return vars;
    }
    , set: function (vars) {
        var hash = '';
        for (var i in vars) {
            if (vars.hasOwnProperty(i)) {
                hash += '&' + i + '=';
                //+ vars[i];
                //if (i=='')
                //if (i=='ms|vendor'){
                //alert(i);

                if ((i == 'ms|vendor') || (i == 'vendor')) {
                    //alert(vars[i]);
                    if (vars[i] != undefined) {
                        val1 = vars[i].split(',');

                        var result = [];

                        nextInput:
                            for (var i1 = 0; i1 < val1.length; i1++) {
                                var str = val1[i1]; // для каждого элемента
                                for (var j = 0; j < result.length; j++) { // ищем, был ли он уже?
                                    if (result[j] == str) continue nextInput; // если да, то следующий
                                }
                                //alert('str'+str);
                                result.push(str);
                            }
                        //alert(branddata[3]);
                        /*if (branddata[2]!=undefined)
                        {
                            result.push(branddata[2]);
                    //hash += result;
                        }*/
                        hash += result;
                    }

                } else hash += vars[i];
            }
        }
        //alert('hash'+hash);

        if (!this.oldbrowser()) {
            if (hash.length != 0) {
                hash = '?' + hash.substr(1);
            }

            window.history.pushState(hash, '', document.location.pathname + hash);
        }
        else {
            window.location.hash = hash.substr(1);
        }
    }
    , add: function (key, val) {
        var hash = this.get();
        hash[key] = val;
        this.set(hash);
    }
    , remove: function (key) {
        var hash = this.get();

        //alert('key '+key);
        delete hash[key];
        this.set(hash);
    }
    , clear: function () {
        this.set({});
    }
    , oldbrowser: function () {
        return !(window.history && history.pushState);
    }
};

function unique(arr) {
    var result = [];

    nextInput:
        for (var i = 0; i < arr.length; i++) {
            var str = arr[i]; // для каждого элемента
            for (var j = 0; j < result.length; j++) { // ищем, был ли он уже?
                if (result[j] == str) continue nextInput; // если да, то следующий
            }
            result.push(str);
        }

    return result;
}


// Initialize Filters
if ($('#mse2_mfilter').length) {
    if (window.location.hash != '' && mSearch2.Hash.oldbrowser()) {
        var uri = window.location.hash.replace('#', '?');
        window.location.href = document.location.pathname + uri;
    }
    else {
        //mSearch2.initialize('#mse2_mfilter');
        mSearch2.initialize('body');
    }
}
// Initialize Form
if ($('form.msearch2').length) {
    mSearch2.Form.initialize('form.msearch2');
}


jQuery(document).ready(function () {
    jQuery(".btn-close").click(function () {


        setCookie('modal', '1', {expires: 86400});
        jQuery(this).parents(".attention-top").fadeOut(200);

    });
    /*
    $('a[href^=""#""]').click(function(){
                                    var el = $(this).attr('href');
                                    $('body').animate({
                                        scrollTop: $(el).offset().top}, 2000);
                                    return false;
                            });*/
});

/*<script type="text/javascript" src="/assets/components/ajaxform/js/default.js"></script>*/
var AjaxForm = {

    initialize: function () {
        if (!jQuery().ajaxForm) {
            document.write('<script src="' + afConfig.assetsUrl + 'js/lib/jquery.form.min.js"><\/script>');
        }
        if (!jQuery().jGrowl) {
            document.write('<script src="' + afConfig.assetsUrl + 'js/lib/jquery.jgrowl.min.js"><\/script>');
        }

        $(document).ready(function () {
            $.jGrowl.defaults.closerTemplate = '<div>[ ' + afConfig.closeMessage + ' ]</div>';
        });

        $(document).on('submit', afConfig.formSelector, function (e) {
            $(this).ajaxSubmit({
                dataType: 'json'
                , data: {pageId: afConfig.pageId}
                , url: afConfig.actionUrl
                , beforeSerialize: function (form, options) {

                    form.find(':submit').each(function () {
                        if (!form.find('input[type="hidden"][name="' + $(this).attr('name') + '"]').length) {
                            $(form).append(
                                $('<input type="hidden">').attr({
                                    name: $(this).attr('name'),
                                    value: $(this).attr('value')
                                })
                            );
                        }
                    })
                }
                , beforeSubmit: function (fields, form) {
                    //alert('111');
                    if (typeof(afValidated) != 'undefined' && afValidated == false) {
                        return false;
                    }
                    form.find('.error').html('');
                    form.find('.error').removeClass('error');
                    form.find('input,textarea,select,button').attr('disabled', true);
                    return true;
                }
                , success: function (response, status, xhr, form) {
                    form.find('input,textarea,select,button').attr('disabled', false);
                    response.form = form;

                    $(document).trigger('af_complete', response);

                    if (!response.success) {
                        AjaxForm.Message.error(response.message);
                        //response.message


                        if (response.data) {
                            var key, value;
                            for (key in response.data) {
                                if (response.data.hasOwnProperty(key)) {
                                    value = response.data[key];
                                    form.find('.error_' + key).html(value).addClass('error');
                                    form.find('[name="' + key + '"]').addClass('error');
                                }
                            }
                        }
                    }
                    else {
                        AjaxForm.Message.success(response.message);
                        //alert(form.html());
                        if (form.hasClass('ajaxaskform')) {
                            form.find('.modal-body').html('<div class="thanks-text" style="text-align:center;">Заявка успешно отправлена. <br/>Менеджер свяжется с Вами когда товар появится в наличии</div>');
                            form.find('.modal-footer').html('');

                        }
                        //alert(response.message);
                        form.find('.error').removeClass('error');
                        if (!form.hasClass('form-info-pets')) {
                            form[0].reset();
                        }
                        if (form.attr('id') == 'add_pet_form') {
                            $('form#add_pet_form select').val('');
                            $('form#add_pet_form .custom-select-container').customSelect('update');
                            //$('form#add_pet_form input[type="text"],form#add_pet_form input[type="hidden"]').val('');
                            $('form#add_pet_form .small.upload').html('Изменить фото');
                            $('#petModal').modal('hide');
                            appendPet(response.data);
                        }
                        if (response.data.newphoto) {
                            if (form.hasClass('form-info-pets')) {
                                form.find('.img-circle').attr('src', response.data.newphoto);
                            }
                        }
                    }
                }
            });
            e.preventDefault();
            return false;
        });

        $(document).on('reset', afConfig.formSelector, function (e) {
            $(this).find('.error').html('');
            AjaxForm.Message.close();
        });
    }

};


AjaxForm.Message = {
    success: function (message, sticky) {
        if (message) {
            if (!sticky) {
                sticky = false;
            }
            $.jGrowl(message, {theme: 'af-message-success', sticky: sticky});
        }
    }
    , error: function (message, sticky) {
        if (message) {
            if (!sticky) {
                sticky = false;
            }
            $.jGrowl(message, {theme: 'af-message-error', sticky: sticky});
        }
    }
    , info: function (message, sticky) {
        if (message) {
            if (!sticky) {
                sticky = false;
            }
            $.jGrowl(message, {theme: 'af-message-info', sticky: sticky});
        }
    }
    , close: function () {
        $.jGrowl('close');
    }
};


AjaxForm.initialize();


/*
<script type="text/javascript" src="/assets/components/office/js/auth/default.js"></script>*/

//if(typeof Office !== "undefined")
//{
//	alert(Office);


/*
<script type="text/javascript" src="/assets/components/office/js/main/default.js"></script>*/

if (typeof OfficeConfig !== "undefined") {
    Office = {
        initialize: function () {

            if (!jQuery().ajaxForm) {
                document.write('<script src="' + OfficeConfig.jsUrl + 'main/lib/jquery.form.min.js"><\/script>');
            }
            if (!jQuery().jGrowl) {
                document.write('<script src="' + OfficeConfig.jsUrl + 'main/lib/jquery.jgrowl.min.js"><\/script>');
            }

            $(document).ready(function () {
                $.jGrowl.defaults.closerTemplate = '<div>[ ' + OfficeConfig.close_all_message + ' ]</div>';
            });
        }

    };
}
if (typeof Office !== "undefined") {
    Office.Message = {
        success: function (message, sticky) {
            if (sticky == null) {
                sticky = false;
            }
            if (message) {
                $.jGrowl(message, {theme: 'office-message-success', sticky: sticky});
            }
        }
        , error: function (message, sticky) {
            if (sticky == null) {
                sticky = true;
            }
            if (message) {
                $.jGrowl(message, {theme: 'office-message-error', sticky: sticky});
            }
        }
        , info: function (message, sticky) {
            if (sticky == null) {
                sticky = false;
            }
            if (message) {
                $.jGrowl(message, {theme: 'office-message-info', sticky: sticky});
            }
        }
        , close: function () {
            $.jGrowl('close');
        }
    };

    Office.initialize();


    Office.Auth = {

        initialize: function (selector) {
            var elem = $(selector);
            if (!elem.length) {
                return false;
            }

            $(document).on('submit', selector, function (e) {
                e.preventDefault();
                $('#forgotPasswordModal').modal('hide');
                $(this).ajaxSubmit({
                    url: OfficeConfig.actionUrl,
                    dataType: 'json',
                    type: 'post',
                    data: {
                        pageId: OfficeConfig.pageId
                    },
                    beforeSubmit: function (formData, $form) {
                        // Additional check for old version of form
                        var found = false;
                        for (var i in formData) {
                            if (formData.hasOwnProperty(i) && formData[i]['name'] == 'action') {
                                found = true;
                            }
                        }
                        if (!found) {
                            formData.push({name: 'action', value: 'auth/sendLink'});
                        }
                        // --
                        $form.find('input, button, a').attr('disabled', true);
                        return true;
                    },
                    success: function (response, status, xhr, $form) {
                        $form.find('input, button, a').attr('disabled', false);
                        if (response.success) {
                            //Office.Message.success(response.message);

                            // $('.modal-backdrop.fade').removeClass('in');
                            //$('.modal-backdrop.fade').addClass('out');
                            setTimeout(function () {
                                $('#recoverySuccessModal').modal()
                            }, 100);

                            $form.resetForm();

                            /* setTimeout(function(){
                                //$('#forgotPasswordModal').hide();
                                $('#recoverySuccessModal').hide();
                            },2000);*/
                        }
                        else {
                            //Office.Message.error(response.message, false);
                            $('#office-auth-form input[name="email"]').after('<div class="error">' + response.message + '</div>');
                            setTimeout(function () {
                                $('#office-auth-form .error').remove();
                            }, 5000);
                        }
                        if (response.data.refresh) {
                            document.location.href = response.data.refresh;
                        }
                    }
                });
                return false;
            });

            return true;
        }

    };

    Office.Auth.initialize('#office-auth-form form');

}
/*

<script type="text/javascript" src="/assets/components/tickets/js/web/default.js"></script>*/

var Tickets = {
    initialize: function () {

        if (typeof window['prettyPrint'] != 'function') {
            document.write('<script src="/assets/components/tickets/js/web/lib/prettify/prettify.js"><\/script>');
            document.write('<link href="/assets/components/tickets/js/web/lib/prettify/prettify.css" rel="stylesheet">');
        }
        if (!jQuery().ajaxForm) {
            document.write('<script src="/assets/components/tickets/js/web/lib/jquery.form.min.js"><\/script>');
        }
        if (!jQuery().jGrowl) {
            document.write('<script src="/assets/components/tickets/js/web/lib/jquery.jgrowl.min.js"><\/script>');
        }
        if (!jQuery().sisyphus) {
            document.write('<script src="/assets/components/tickets/js/web/lib/jquery.sisyphus.min.js"><\/script>');
        }

        // Forms listeners
        $(document).on('click', '#comment-preview-placeholder a', function () {
            return false;
        });
        $(document).on('change', '#comments-subscribe', function () {
            Tickets.comment.subscribe($('[name="thread"]', $('#comment-form')));
        });
        $(document).on('change', '#tickets-subscribe', function () {
            Tickets.ticket.subscribe($(this).data('id'));
        });
        $(document).on('submit', '#ticketForm', function (e) {
            Tickets.ticket.save(this, $(this).find('[type="submit"]')[0]);
            e.preventDefault();
            return false;
        });


        $(document).on('submit', '#answer-comment-form', function (e) {
            Tickets.comment.save(this, $(this).find('[type="submit"]')[0]);
            e.preventDefault();
            $('#AnswerCommentModal').modal('hide');

            return false;
        });
        /*
        $(document).on('submit', '#comment-form-veterinar', function(e) {

            alert('submit ticket veterinar');

            $('#feedbackModal .modal-footer').html('');setTimeout(function(){$('#feedbackModal').modal('hide');},3000);


            Tickets.comment.save(this, $(this).find('[type="submit"]')[0]);
            e.preventDefault();

            return false;
        });
*/
        $(document).on('submit', '#comment-form', function (e) {


            $('#feedbackModal .modal-footer').html('');
            setTimeout(function () {
                $('#feedbackModal').modal('hide');
            }, 3000);


            Tickets.comment.save(this, $(this).find('[type="submit"]')[0]);
            e.preventDefault();

            return false;
        });
        // Preview and submit
        $(document).on('click touchend', '#ticketForm .preview, #ticketForm .save, #ticketForm .draft, #ticketForm .publish', function (e) {
            if ($(this).hasClass('preview')) {
                Tickets.ticket.preview(this.form, this);
            }
            else {
                Tickets.ticket.save(this.form, this);
            }
            e.preventDefault();
            return false;
        });
        /*$(document).on('click touchend', '#comment-form .preview, #comment-form .submit', function(e) {
            alert('dddd');
            if ($(this).hasClass('preview')) {
                Tickets.comment.preview(this.form, this);
            }
            else {
                Tickets.comment.save(this.form, this);
            }
            e.preventDefault();
            return false;
        });*/
        // Hotkeys
        $(document).on('keydown', '#ticketForm, #comment-form', function (e) {
            if (e.keyCode == 13) {
                if (e.shiftKey && (e.ctrlKey || e.metaKey)) {
                    $(this).submit();
                }
                else if ((e.ctrlKey || e.metaKey)) {
                    $(this).find('input[type="button"].preview').click();
                }
            }
        });
        // Show and hide forms
        $(document).on('click touchend', '#comment-new-link a', function (e) {
            Tickets.forms.comment();
            e.preventDefault();
            return false;
        });
        $(document).on('click touchend', '.comment-reply a', function (e) {
            var id = $(this).parents('.ticket-comment').data('id');
            if ($(this).hasClass('reply')) {
                Tickets.forms.reply(id);
            }
            else if ($(this).hasClass('edit')) {
                Tickets.forms.edit(id);
            }
            e.preventDefault();
            return false;
        });
        // Votes and rating
        $(document).on('click touchend', '.ticket-comment-rating.active > .vote', function (e) {
            var id = $(this).parents('.ticket-comment').data('id');
            if ($(this).hasClass('plus')) {
                Tickets.Vote.comment.vote(this, id, 1);
            }
            else if ($(this).hasClass('minus')) {
                Tickets.Vote.comment.vote(this, id, -1);
            }
            e.preventDefault();
            return false;
        });
        $(document).on('click touchend', '.ticket-rating.active > .vote', function (e) {
            var id = $(this).parents('.ticket-meta').data('id');
            if ($(this).hasClass('plus')) {
                Tickets.Vote.ticket.vote(this, id, 1);
            }
            else if ($(this).hasClass('minus')) {
                Tickets.Vote.ticket.vote(this, id, -1);
            }
            else {
                Tickets.Vote.ticket.vote(this, id, 0);
            }
            e.preventDefault();
            return false;
        });
        // --
        // Stars
        $(document).on('click touchend', '.ticket-comment-star.active > .star', function (e) {
            var id = $(this).parents('.ticket-comment').data('id');
            Tickets.Star.comment.star(this, id, 0);
            e.preventDefault();
            return false;
        });
        $(document).on('click touchend', '.ticket-star.active > .star', function (e) {
            var id = $(this).parents('.ticket-meta').data('id');
            Tickets.Star.ticket.star(this, id, 0);
            e.preventDefault();
            return false;
        });

        $(document).ready(function () {
            /*if (TicketsConfig.enable_editor == true) {
                $('#ticket-editor').markItUp(TicketsConfig.editor.ticket);
            }
            if (TicketsConfig.enable_editor == true) {
                $('#comment-editor').markItUp(TicketsConfig.editor.comment);
            }*/

            $.jGrowl.defaults.closerTemplate = '<div>[ Закрыть все сообщения ]</div>';

            var count = $('.ticket-comment').size();
            $('#comment-total, .ticket-comments-count').text(count);

            $("#ticketForm.create").sisyphus({
                excludeFields: $('#ticketForm .disable-sisyphus')
            });

            // Auto hide new comment button
            if ($('#comment-form').is(':visible')) {
                $('#comment-new-link').hide();
            }
        });

        // Link to parent comment
        $('.comments').on('click touchend', '.ticket-comment-up a', function () {
            var id = $(this).data('id');
            var parent = $(this).data('parent');
            if (parent && id) {
                Tickets.utils.goto('comment-' + parent);
                $('#comment-' + parent + ' .ticket-comment-down:lt(1)').show().find('a').attr('data-child', id);
            }
            return false;
        });

        // Link to child comment
        $('.comments').on('click touchend', '.ticket-comment-down a', function () {
            var child = $(this).data('child');
            if (child) {
                Tickets.utils.goto('comment-' + child);
            }
            $(this).attr('data-child', '').parent().hide();
            return false;
        });
    }

    , ticket: {
        preview: function (form, button) {

            $(form).ajaxSubmit({
                data: {action: 'ticket/preview'}
                , url: '/assets/components/tickets/action.php'//TicketsConfig.actionUrl
                , form: form
                , button: button
                , dataType: 'json'
                , beforeSubmit: function () {
                    $(button).attr('disabled', 'disabled');
                    return true;
                }
                , success: function (response) {

                    var element = $('#ticket-preview-placeholder');
                    if (response.success) {
                        element.html(response.data.preview).show();
                        prettyPrint();
                    }
                    else {
                        element.html('').hide();
                        Tickets.Message.error(response.message);
                    }
                    $(button).removeAttr('disabled');
                }
            });
        }

        , save: function (form, button) {
            var action = 'ticket/';
            switch ($(button).prop('name')) {
                case 'draft':
                    action += 'draft';
                    break;
                case 'save':
                    action += 'save';
                    break;
                default:
                    action += 'publish';
                    break;
            }

            $(form).ajaxSubmit({
                data: {action: action}
                , url: '/assets/components/tickets/action.php'//TicketsConfig.actionUrl
                , form: form
                , button: button
                , dataType: 'json'
                , beforeSubmit: function () {
                    $(form).find('input[type="submit"], input[type="button"]').attr('disabled', 'disabled');
                    $('.error', form).text('');
                    return true;
                }
                , success: function (response) {
                    $('#ticketForm.create').sisyphus().manuallyReleaseData();
//alert(response.message);Tickets.Message.success(response.message);

                    if (response.success) {

                        if (response.message) {

                            Tickets.Message.success(response.message);
                        }
                        if (action == 'ticket/save') {
                            $(form).find('input[type="submit"], input[type="button"]').removeAttr('disabled');
                        }
                        else if (response.data.redirect) {
                            document.location.href = response.data.redirect;
                        }
                    }
                    else {
                        $(form).find('input[type="submit"], input[type="button"]').removeAttr('disabled');
                        Tickets.Message.error(response.message);
                        if (response.data) {
                            var i, field;
                            for (i in response.data) {
                                field = response.data[i];
                                $(form).find('[name="' + field.field + '"]').parent().find('.error').text(field.message)
                            }
                        }
                    }
                }
            });
        }

        , subscribe: function (section) {
            if (section) {
                $.post(TicketsConfig.actionUrl, {action: "section/subscribe", section: section}, function (response) {
                    if (response.success) {
                        Tickets.Message.success(response.message);
                    }
                    else {
                        Tickets.Message.error(response.message);
                    }
                }, 'json');
            }
        }
    }

    , comment: {
        preview: function (form, button) {
            $(form).ajaxSubmit({
                data: {action: 'comment/preview'}
                , url: '/assets/components/tickets/action.php'//TicketsConfig.actionUrl
                , form: form
                , button: button
                , dataType: 'json'
                , beforeSubmit: function () {
                    $(button).attr('disabled', 'disabled');
                    return true;
                }
                , success: function (response) {
                    $(button).removeAttr('disabled');
                    if (response.success) {
                        $('#comment-preview-placeholder').html(response.data.preview).show();
                        prettyPrint();
                    }
                    else {
                        Tickets.Message.error(response.message);
                    }
                }
            });
            return false;
        }

        , save: function (form, button) {
            $(form).ajaxSubmit({
                data: {action: 'comment/save'}
                , url: '/assets/components/tickets/action.php'//TicketsConfig.actionUrl
                , form: form
                , button: button
                , dataType: 'json'
                , beforeSubmit: function () {
                    clearInterval(window.timer);
                    $('.error', form).text('');
                    $(button).attr('disabled', 'disabled');
                    return true;
                }
                , success: function (response) {
                    $(button).removeAttr('disabled');
                    if (response.success) {
                        $('#feedbackModal .modal-footer').html('');
                        setTimeout(function () {
                            $('#feedbackModal').modal('hide');
                        }, 1000);

                        dataLayer.push({
                            'event': 'event-GA',
                            'eventCategory': 'product',
                            'eventAction': 'review'
                        });

                        Tickets.forms.comment(false);
                        $('#comment-preview-placeholder').html('').hide();
                        $('#comment-editor', form).val('');
                        $('.ticket-comment .comment-reply a').show();

                        // autoPublish = 0
                        if (!response.data.length && response.message) {
                            Tickets.Message.info(response.message);
                        }
                        else {
                            Tickets.comment.insert(response.data.comment);
                            Tickets.utils.goto($(response.data.comment).attr('id'));
                            //if (response.message) {
                            Tickets.Message.success('Спасибо! Ваш отзыв размещен на сайте');
                            //}
                        }
                        //response.data.count
                        $('.tabcountsreview').text('(' + response.data.count + ')');
                        $('span.count.text-bottom').text('(' + response.data.count + ')');

                        Tickets.comment.getlist();
                        prettyPrint();//<span class="tabcountsreview">

                    }
                    else {
                        Tickets.Message.error(response.message);
                        if (response.data) {
                            var i, field;
                            for (i in response.data) {
                                field = response.data[i];
                                $(form).find('[name="' + field.field + '"]').parent().find('.error').text(field.message)
                            }
                        }
                    }
                    if (response.data.captcha) {
                        $('input[name="captcha"]', form).val('').focus();
                        $('#comment-captcha', form).text(response.data.captcha);
                    }
                }
            });
            return false;
        }

        , getlist: function () {
            var form = $('#comment-form');
            var thread = $('[name="thread"]', form);
            if (!thread) {
                return false;
            }
            //Tickets.tpanel.start();//TicketsConfig.actionUrl
            $.post('/assets/components/tickets/action.php', {
                action: 'comment/getlist',
                thread: thread.val()
            }, function (response) {
                for (var k in response.data.comments) {
                    if (response.data.comments.hasOwnProperty(k)) {
                        Tickets.comment.insert(response.data.comments[k], true);
                    }
                }
                var count = $('.ticket-comment').size();
                $('#comment-total, .ticket-comments-count').text(count);

                //Tickets.tpanel.stop();
            }, 'json');
            return true;
        }

        , insert: function (data, remove) {
            var comment = $(data);
            var parent = $(comment).attr('data-parent');
            var id = $(comment).attr('id');
            var exists = $('#' + id);
            var children = '';

            if (exists.length > 0) {
                var np = exists.data('newparent');
                comment.attr('data-newparent', np);
                data = comment[0].outerHTML;
                if (remove) {
                    children = exists.find('.comments-list').html();
                    exists.remove();
                }
                else {
                    exists.replaceWith(data);
                    return;
                }
            }

            /*if (parent == 0 && TicketsConfig.formBefore) {
                $('.comments').prepend(data)
            }
            else if (parent == 0) {*/
            $('.comments').prepend(data)//append(data)
            /*}
            else {
                var pcomm = $('#comment-'+parent);
                if (pcomm.data('parent') != pcomm.data('newparent')) {
                    parent = pcomm.data('newparent');
                    comment.attr('data-newparent', parent);
                    data = comment[0].outerHTML;
                }
                else if (TicketsConfig.thread_depth) {
                    var level = pcomm.parents('.ticket-comment').length;
                    if (level > 0 && level >= (TicketsConfig.thread_depth - 1)) {
                        parent = pcomm.data('parent');
                        comment.attr('data-newparent', parent);
                        data = comment[0].outerHTML;
                    }
                }
                $('#comment-'+parent+' > .comments-list').append(data);
            }*/

            if (children.length > 0) {
                $('#' + id).find('.comments-list').html(children);
            }
        }

        , subscribe: function (thread) {
            if (thread.length) {
                $.post(TicketsConfig.actionUrl, {
                    action: "comment/subscribe",
                    thread: thread.val()
                }, function (response) {
                    if (response.success) {
                        Tickets.Message.success(response.message);
                    }
                    else {
                        Tickets.Message.error(response.message);
                    }
                }, 'json');
            }
        }
    }

    , forms: {
        reply: function (comment_id) {
            $('#comment-new-link').show();

            clearInterval(window.timer);
            var form = $('#comment-form');
            $('.time', form).text('');
            $('.ticket-comment .comment-reply a').show();

            $('#comment-preview-placeholder').hide();
            $('input[name="parent"]', form).val(comment_id);
            $('input[name="id"]', form).val(0);

            var reply = $('#comment-' + comment_id + ' > .comment-reply');
            form.insertAfter(reply).show();
            $('a', reply).hide();
            reply.parents('.ticket-comment').removeClass('ticket-comment-new');

            $('#comment-editor', form).val('').focus();
            return false;
        }

        , comment: function (focus) {
            if (focus !== false) {
                focus = true;
            }
            clearInterval(window.timer);

            $('#comment-new-link').hide();

            var form = $('#comment-form');
            $('.time', form).text('');
            $('.ticket-comment .comment-reply a:hidden').show();

            $('#comment-preview-placeholder').hide();
            $('input[name="parent"]', form).val(0);
            $('input[name="id"]', form).val(0);
            $(form).insertAfter('#comment-form-placeholder').show();

            $('#comment-editor', form).val('');
            if (focus) {
                $('#comment-editor', form).focus();
            }
            return false;
        }

        , edit: function (comment_id) {
            $('#comment-new-link').show();

            var thread = $('#comment-form [name="thread"]').val();
            $.post(TicketsConfig.actionUrl, {
                action: "comment/get",
                id: comment_id,
                thread: thread
            }, function (response) {
                if (!response.success) {
                    Tickets.Message.error(response.message);
                }
                else {
                    clearInterval(window.timer);
                    $('.ticket-comment .comment-reply a:hidden').show();
                    var form = $('#comment-form');
                    $('#comment-preview-placeholder').hide();
                    $('input[name="parent"]', form).val(0);
                    $('input[name="id"]', form).val(comment_id);

                    var reply = $('#comment-' + comment_id + ' > .comment-reply');
                    var time_left = $('.time', form);

                    time_left.text('');
                    form.insertAfter(reply).show();
                    $('a', reply).hide();

                    $('#comment-editor', form).val(response.data.raw).focus();
                    if (response.data.name) {
                        $('[name="name"]', form).val(response.data.name);
                    }
                    if (response.data.email) {
                        $('[name="email"]', form).val(response.data.email);
                    }

                    var time = response.data.time;
                    window.timer = setInterval(function () {
                        if (time > 0) {
                            time -= 1;
                            time_left.text(Tickets.utils.timer(time));
                        }
                        else {
                            clearInterval(window.timer);
                            time_left.text('');
                            //Tickets.forms.comment();
                        }
                    }, 1000);
                }
            }, 'json');

            return false;
        }
    }

    , utils: {
        timer: function (diff) {
            days = Math.floor(diff / (60 * 60 * 24));
            hours = Math.floor(diff / (60 * 60));
            mins = Math.floor(diff / (60));
            secs = Math.floor(diff);

            dd = days;
            hh = hours - days * 24;
            mm = mins - hours * 60;
            ss = secs - mins * 60;

            var result = [];

            if (hh > 0) result.push(hh ? this.addzero(hh) : '00');
            result.push(mm ? this.addzero(mm) : '00');
            result.push(ss ? this.addzero(ss) : '00');

            return result.join(':');
        }

        , addzero: function (n) {
            return (n < 10) ? '0' + n : n;
        }

        , goto: function (id) {
            //alert($('#' + id));
            if ($('#' + id).offset() != undefined) {
                $('html, body').animate({
                    scrollTop: $('#' + id).offset().top
                }, 1000);
            }
        }
    }
};


Tickets.Message = {
    success: function (message) {
        if (message) {
            $.jGrowl(message, {theme: 'tickets-message-success'});
        }
    }
    , error: function (message) {
        if (message) {
            $.jGrowl(message, {theme: 'tickets-message-error'/*, sticky: true*/});
        }
    }
    , info: function (message) {
        if (message) {
            $.jGrowl(message, {theme: 'tickets-message-info'});
        }
    }
    , close: function () {
        $.jGrowl('close');
    }
};


Tickets.Vote = {

    comment: {
        options: {
            active: 'active'
            , inactive: 'inactive'
            , voted: 'voted'
            , vote: 'vote'
            , rating: 'rating'
            , positive: 'positive'
            , negative: 'negative'
        }
        , vote: function (link, id, value) {
            link = $(link);
            var parent = link.parent();
            var options = this.options;
            var rating = parent.find('.' + options.rating);
            if (parent.hasClass(options.inactive)) {
                return false;
            }

            $.post(TicketsConfig.actionUrl, {action: 'comment/vote', id: id, value: value}, function (response) {
                if (response.success) {
                    link.addClass(options.voted);
                    parent.removeClass(options.active).addClass(options.inactive);
                    parent.find('.' + options.vote);
                    rating.text(response.data.rating).attr('title', response.data.title);

                    rating.removeClass(options.positive + ' ' + options.negative);
                    if (response.data.status == 1) {
                        rating.addClass(options.positive);
                    }
                    else if (response.data.status == -1) {
                        rating.addClass(options.negative);
                    }
                }
                else {
                    Tickets.Message.error(response.message);
                }
            }, 'json');

            return true;
        }
    }
    , ticket: {
        options: {
            active: 'active'
            , inactive: 'inactive'
            , voted: 'voted'
            , vote: 'vote'
            , rating: 'rating'
            , positive: 'positive'
            , negative: 'negative'
        }
        , vote: function (link, id, value) {
            link = $(link);
            var parent = link.parent();
            var options = this.options;
            var rating = parent.find('.' + options.rating);
            if (parent.hasClass(options.inactive)) {
                return false;
            }

            $.post(TicketsConfig.actionUrl, {action: 'ticket/vote', id: id, value: value}, function (response) {
                if (response.success) {
                    link.addClass(options.voted);
                    parent.removeClass(options.active).addClass(options.inactive);
                    parent.find('.' + options.vote);
                    rating.text(response.data.rating).attr('title', response.data.title).removeClass(options.vote);

                    rating.removeClass(options.positive + ' ' + options.negative);
                    if (response.data.status == 1) {
                        rating.addClass(options.positive);
                    }
                    else if (response.data.status == -1) {
                        rating.addClass(options.negative);
                    }
                }
                else {
                    Tickets.Message.error(response.message);
                }
            }, 'json');

            return true;
        }
    }
};


Tickets.Star = {
    comment: {
        options: {
            stared: 'stared'
            , unstared: 'unstared'
            //,count: 'ticket-comment-star-count'
        }
        , star: function (link, id, value) {
            link = $(link);
            var options = this.options;
            var parent = link.parent();

            $.post(TicketsConfig.actionUrl, {action: 'comment/star', id: id}, function (response) {
                if (response.success) {
                    link.toggleClass(options.stared).toggleClass(options.unstared);
                }
                else {
                    Tickets.Message.error(response.message);
                }
            }, 'json');

            return true;
        }
    }
    , ticket: {
        options: {
            stared: 'stared'
            , unstared: 'unstared'
            , count: 'ticket-star-count'
        }
        , star: function (link, id, value) {
            link = $(link);
            var options = this.options;
            var count = link.parent().find('.' + this.options.count);

            $.post(TicketsConfig.actionUrl, {action: 'ticket/star', id: id}, function (response) {
                if (response.success) {
                    link.toggleClass(options.stared).toggleClass(options.unstared);
                    count.text(response.data.stars);
                }
                else {
                    Tickets.Message.error(response.message);
                }
            }, 'json');

            return true;
        }
    }
};
if (typeof TicketsConfig !== "undefined") {

    Tickets.tpanel = {
        wrapper: $('#comments-tpanel')
        , refresh: $('#tpanel-refresh')
        , new_comments: $('#tpanel-new')
        , class_new: 'ticket-comment-new'

        , initialize: function () {
            if (TicketsConfig.tpanel) {
                this.wrapper.show();
                this.stop();
            }

            this.refresh.on('click', function () {
                $('.' + Tickets.tpanel.class_new).removeClass(Tickets.tpanel.class_new);
                Tickets.comment.getlist();
            });

            this.new_comments.on('click', function () {
                var elem = $('.' + Tickets.tpanel.class_new + ':first');
                $('html, body').animate({
                    scrollTop: elem.offset().top
                }, 1000, 'linear', function () {
                    elem.removeClass(Tickets.tpanel.class_new);
                });

                var count = parseInt(Tickets.tpanel.new_comments.text());
                if (count > 1) {
                    Tickets.tpanel.new_comments.text(count - 1);
                }
                else {
                    Tickets.tpanel.new_comments.text('').hide();
                }
            });
        }

        , start: function () {
            this.refresh.addClass('loading');
        }

        , stop: function () {
            var count = $('.' + this.class_new).size();
            if (count > 0) {
                this.new_comments.text(count).show();
            }
            else {
                this.new_comments.hide();
            }
            this.refresh.removeClass('loading');
        }

    };


}
Tickets.initialize();
if (typeof TicketsConfig !== "undefined") {
    Tickets.tpanel.initialize();
}


var count_to_show = 4;//количество раз показывать попап
var interval_show = 1800000;// как часто показывать сообщение - не чаще чем через 30 минут, в мс
var show_after = 60000; //после начала сессии через сколько времени показывать попап 60 сек
var now = new Date(); // текущее время
jQuery(document).ready(function () {
    popupregistr();

});

function popupregistr() {
    pageshow = getCookie('countpagepopup');
    if (pageshow === undefined) {
        setCookie('countpagepopup', '1', {expires: 86400});
        setCookie('timestart', new Date(), {expires: 86400});
    } else {
        countpage = getCookie('countpagepopup');
        countpage_new = parseInt(countpage) + 1;
        timestart = getCookie('timestart');
        if (timestart === undefined) setCookie('timestart', new Date(), {expires: 86400});
        timestart1 = new Date(timestart);
        now1 = new Date();
        var deltastartses = now1 - timestart1;
        setCookie('countpagepopup', countpage_new, {expires: 86400});
        if ((countpage_new >= 3) && (deltastartses > show_after)) showpopup();
    }

}

function showpopup() {
    show_popup = false;
    count_show = getCookie('count_show');
    if (count_show === undefined) {
        show_popup = true;
        count_show = 0;
    }
    else if (parseInt(count_show) <= count_to_show) {
        time_show = getCookie('time_show');
        date2 = new Date(time_show);
        date1 = new Date();
        var delta = date1 - date2;
        if (delta >= interval_show) show_popup = true;
    }
    if (show_popup) {
        count_show = parseInt(count_show) + 1;
        setCookie('count_show', count_show);
        var now = new Date();
        setCookie('time_show', now, {expires: 86400});
        jQuery("input[name='phone']").mask("+389999999999");
        jQuery('#addAskregistrModal').modal('show');
    }
}


jQuery(document).ready(function () {


    /*
    jQuery('#payments').on('change', function () {
        jQuery('#payment_liqpay_submit').removeClass('hide');
    });*/

    jQuery('.showmapdelivery').on('click', function () {
        jQuery('#showMapModal .modal-body').html('');
        $.ajax({
            type: "POST", url: '/assets/components/minishop2/map.php', success: function (data) {
                jQuery('#showMapModal .modal-body').html(data);
            }
        });

    });
    jQuery('#registrpopup').on('click', function () {
        form = jQuery('#form_reg_popup');
        object = form.find('input[name="phone"]');
        phone = object.val();


        $.post('/assets/components/minishop2/validateuser.php', {'phone': phone}, function (response) {
            if (!response.result) {
                message = response.message;
                object.parent().addClass('has-error');
                object.parent().find('.with-errors').html(message);
                object.parent().find('.with-errors').removeClass('hide');

                jQuery('#registrpopup').addClass('disabled');

            } else {
                object.parent().find('.with-errors').removeClass('has-error');
                //$('.formaregistration input[name="registerbtn"]').removeClass('disabled');
                jQuery('#registrpopup').removeClass('disabled');
                object.parent().find('.with-errors').addClass('hide');
                form.submit();
            }
        }, 'json');
        return false;
    });
});