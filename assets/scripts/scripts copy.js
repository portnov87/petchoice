/*global $:false */

$('form').each(function () {
    $(this).validate({
      errorElement: "div",
      highlight: function(element, errorClass, validClass) {
        $(element).parent('.form-group').addClass('has-error');
      },
      unhighlight: function(element, errorClass, validClass) {
        $(element).parent('.form-group').removeClass('has-error');
      },
      rules: {
        password: {
          required: true,
          minlength: 6
        },
        password_confirm: {
          required: true,
          equalTo: "#password",
          minlength: 6
        },
        specifiedpassword: {
          minlength: 6
        },
        confirmpassword: {
          equalTo: "#inputNewPassword",
          minlength: 6
        }
      },
      messages: {
        password: {
            //required: "Please provide a password",
            minlength: "Пароль должен содержать не меньше 6 символов"
        },
        password_confirm: {
            //required: "Please provide a password",
            minlength: "Пароль должен содержать не меньше 6 символов",
            equalTo: "Пароли не совпадают"
        },
        specifiedpassword: {
            //required: "Please provide a password",
            minlength: "Пароль должен содержать не меньше 6 символов"
        },
        confirmpassword: {
            //required: "Please provide a password",
            minlength: "Пароль должен содержать не меньше 6 символов",
            equalTo: "Пароли не совпадают"
        }
      }
      //,errorClass: "help-block with-errors"
    });
});

$("input[name='username']").mask("+380999999999");
$("input[name='phone']").mask("+380999999999");

$(function() {
  'use strict';
  if($('#mse2_selected span').size() > 1)
    $('#mse2_selected').removeClass('hiiden');
  else
    $('#mse2_selected').addClass('hidden');

  if($('#petlist form').size() == 0)
    $('#petlist .info-msg').show();
  else
    $('#petlist .info-msg').hide();
});

vendor = document.getElementById('mse2_ms|vendor');
$(vendor).children('.checkbox:gt(9)').addClass('hidden');

$(function() {
  'use strict';
  $('[data-toggle="popover"]').popover({
    html: true,
    content: function() {
      var content = $(this).attr('data-popover-content');
      return $(content).children('.popover-body').html();
    },
    title: function() {
      var title = $(this).attr('data-popover-content');
      return $(title).children('.popover-heading').html();
    }
  });
});

$(document).on('click', '.upload', function(e) {
    $(this).next('input[type=file]').click();
    return false;
});

$(document).on('change', '.pet_type', function(e) {
  var type_id = this.value;
  var petData = {
    pet_type: type_id
  };
  var $this = $(this);
  $.ajax({
    type: "GET",
    url: '/assets/components/pets/getpetdata.php',
    data: petData,
    success: function(data) {
      data = $.parseJSON(data);
      if (data.success === true) {
        response = data.data;
        another = $(response).filter(function () { return $(this).html() == "Другая"; }).val();
        $pet_breed = $this.closest('form').find('.pet_breed');
        $pet_breed.html(data.data);
        $pet_breed.children('option:first').attr('selected','selected');
        var selected_text = $pet_breed.children('option:first').text();
        $pet_breed.next('.custom-select').find('span span').text(selected_text);
        $pet_breed.find('option[value="'+another+'"]').remove();
        $pet_breed.append($(response).filter(function () { return $(this).html() == "Другая"; }));
      } else if (data.success === false) {

      }
    }
  });
});

$('body').on('click', function(e) {
  'use strict';
  $('[data-toggle="popover"]').each(function() {
    if (!$(this).is(e.target) && $(this).has(e.target).length === 0 && $('.popover').has(e.target).length === 0) {
      $(this).popover('hide');
    }
  });
});

$('#showmore').on('click', function(e) {
  'use strict';
  var next_page = $('#mse2_pagination li.active').next().children('a');
  var tmp = $(next_page).prop('href').match(/page[=|\/](\d+)/);
  var page = tmp && tmp[1] ? Number(tmp[1]) : 1;
  var results = $('#mse2_results');
  var pagination = $('#mse2_pagination');
  var params = mSearch2.Hash.get();
  params['action'] = 'filter';
  params['pageId'] = mse2Config.pageId;
  params['key'] = mse2Config.key;
  params['page'] = page;
  $.ajax({
    type: "GET",
    url: '/assets/components/msearch2/action.php',
    data: params,
    success: function(data) {
      data = $.parseJSON(data);
      if (data.success === true) {
          results.append(data.data.results);
          pagination.html(data.data.pagination);
      } else if (data.success === false) {
          results.html(data.message);
      }
      mSearch2.Hash.set(params);
    }
  });
});

$(function() {
  'use strict';
  $.prettyLoader();
});

// AJAX activity indicator bound to ajax start/stop document events
$(document).ajaxStart(function () {
  $.prettyLoader.show();
}).ajaxStop(function () {
  $.prettyLoader.hide();
});

$(function() {
  'use strict';
  $('.open-comment').click(function(e) {
    e.preventDefault();
    $(this).parents('.row').siblings('.comment-text-inpt').toggleClass('hide');
  });
});

$(function() {
  'use strict';
  $('.pay-btn').click(function() {
    $(this).addClass('hidden').siblings('.count-box').removeClass('hidden');
  });
});

$(function() {
  'use strict';
  $(document).on("click", ".pay-product-btn", function(e) {
    var imgEl = $(this).parents('.product-item-tabs').siblings('.product-item-img').children().children('img');
    var imgClone = $(this).parents('.product-item-tabs').siblings('.product-item-img').children().children('img').clone();
    var paddLeft = document.documentElement.clientWidth;
    var paddEl = imgEl.offset().left + 260;
    paddEl = paddLeft - paddEl;
    imgClone.appendTo($(this).parents('.product-item-tabs').siblings('.product-item-img')).css({
      'position': 'absolute',
      'top': 0,
      'left': '17%',
      'z-index': 50,
      'width': '62%'
    });
    imgClone.animate({
      width: 0,
      height: 0,
      opacity: 0,
      left: '+=' + paddEl,
      top: '-=350'
    }, 1000);
    $(this).addClass('hidden').siblings('.count-box').removeClass('hidden');
  });
});


/* bxslider for brands */
$(function() {
  'use strict';

  $('.slider-actions').bxSlider({
    minSlides: 5,
    maxSlides: 5,
    slideWidth: 176,
    slideMargin: 24,
    moveSlides: 1,
    nextText: '',
    prevText: ''
  });

  $('.brands-bx').bxSlider({
    minSlides: 5,
    maxSlides: 5,
    slideWidth: 176,
    slideMargin: 24,
    moveSlides: 1,
    nextText: '',
    prevText: ''
  });

  /* product card slider */
  $('.product-complex-bx').bxSlider({
    minSlides: 1,
    maxSlides: 1,
    slideWidth: 1015,
    moveSlides: 1,
    nextText: '',
    prevText: ''
  });


  /* Prooduct card gallery */
  /*$('.product-gallery-bx').bxSlider({
    pager : true,
    captions: false,
    pagerCustom: '#bx-pager'
  });*/

  var product_slider = $('.product-gallery-bx').bxSlider({
    pagerCustom: '#bx-pager'
  });

  $('a[href="#new-products-pane"]').one('shown.bs.tab', function() {
    $('.slider-new-products').bxSlider({
      minSlides: 5,
      maxSlides: 5,
      slideWidth: 176,
      slideMargin: 24,
      moveSlides: 1,
      nextText: '',
      prevText: ''
    });
  });

});

/* Stars rating */
$(function() {
  'use strict';

  $('.stars-rating').barrating({
    showSelectedRating: false
  });
});


/* Slider range */
/*$(function() {
  'use strict';

  $('#slider-range').slider({
    range: true,
    min: 0,
    max: 10000,
    values: [325, 4620],
    slide: function(event, ui) {
      $('#amount1').val(ui.values[0]);
      $('#amount2').val(ui.values[1]);
    }
  });
  $('#amount1').val($('#slider-range').slider('values', 0));
  $('#amount2').val($('#slider-range').slider('values', 1));
});*/

$(document).on("click", ".plus-count", function(e) {
  e.preventDefault();
  var qtty = $(this).prev('.count-products');
  var parent = $(this).closest('.ms2_product');
  var product_id = $(parent).find('input[name="product_id"]').val();
  var size = $(this).closest('.card-costs-item').find('.weight').text();
  var key = $(this).closest('.product-option').data('product-key');
  $(qtty).get(0).value++;
  cartChange(key, $(qtty).val());

});
$(document).on("click", ".minus-count", function(e) {
  e.preventDefault();
  var qtty = $(this).next('.count-products');
  var parent = $(this).closest('.ms2_product');
  var product_id = $(parent).find('input[name="product_id"]').val();
  var size = $(this).closest('.card-costs-item').find('.weight').text();
  var btn_cart = $(qtty).parent('.count-box').prev('.add-cart');
  var key = $(this).closest('.product-option').data('product-key');
  if ($(qtty).get(0).value -1 > 0) {
      $(qtty).get(0).value--;
    } else {
      $(qtty).get(0).value = 0;
      $(btn_cart).removeClass('hidden');
      $(qtty).parent('.count-box').addClass('hidden');
      $(qtty).get(0).value = 1;
    }
  cartChange(key, $(qtty).val());
});

$(document).on("blur", ".count-products", function() {
    var qtty = $(this).val();
    var key = $(this).closest('.product-option').data('product-key');
    cartChange(key, qtty);
});
// сортировка в фильтре
$('.sort').on("click", ".mSort", function(e) {
    var data = $(this).data('sort');
    switch (data) {
        case 'ms_popular,desc':
              $(this).addClass('up-sort');
              $(this).data( 'sort', 'ms_popular,asc' );
            break;
        case 'ms_popular,asc':
              $(this).removeClass('up-sort');
              $(this).data( 'sort', 'ms_popular,desc' );
            break;
        case 'ms_price,desc':
              $(this).addClass('up-sort');
              $(this).data( 'sort', 'ms_price,asc' );
            break;
        case 'ms_price,asc':
              $(this).removeClass('up-sort');
              $(this).data( 'sort', 'ms_price,desc' );
            break;
        case 'name,desc':
              $(this).addClass('up-sort');
              $(this).data( 'sort', 'name,asc' );
            break;
        case 'name,asc':
              $(this).removeClass('up-sort');
              $(this).data( 'sort', 'name,desc' );
            break;
        default:
            console.log('Received unknown sort "' + data + '"');
    }
});


/* Fancybox */
$(function() {
  'use strict';

  $(document).ready(function() {
    $('.fancybox').fancybox();

    $('input[name=city]').typeahead({
      //источник данных
      source: function (query, process) {
         return $.post('/assets/components/suggest/getLocalitiesForSuggestJSON.php', {'name':query},
               function (response) {
                    var data = new Array();
                    //преобразовываем данные из json в массив
                    /*$.each(response.content.records, function(i, name)
                    {
                      data.push(i+'_'+name);
                    });
                    console.log(data);
                    return process(data);*/
                    $.each(response.content.records, function(i) {
                      $.each(response.content.records[i], function(key,val) {
                        data.push(key+'_'+val);
                      });
                      return process(data);
                    });
                  },
               'json'
               );
        }
        //источник данных
        //вывод данных в выпадающем списке
        , highlighter: function(item) {
            var parts = item.split('_');
            console.log(parts);
            console.log('_____');
            
            parts.shift();
            return parts.join('_');
        }
        //вывод данных в выпадающем списке
        //действие, выполняемое при выборе елемента из списка
        , updater: function(item) {
                   var parts = item.split('_');
                   var userId = parts.shift();
                   $.post('getuserdata', {'user_id':userId},
                        function (user) {
                          $('input[name=email]').val(user.email);
                          $('input[name=phone]').val(user.phone);
                        },
                     'json'
                    );
                    return parts.join('_');
                 }
        //действие, выполняемое при выборе елемента из списка
        }
    );
  });
});
/*
 * This is based on ideas from a technique described by Alen Grakalic in
 * http://cssglobe.com/post/8802/custom-styling-of-the-select-elements
 */
(function($) {
  $.fn.customSelect = function(settings) {
    var config = {
      replacedClass: 'replaced', // Class name added to replaced selects
      customSelectClass: 'custom-select', // Class name of the (outer) inserted span element
      activeClass: 'active', // Class name assigned to the fake select when the real select is in hover/focus state
      wrapperElement: '<div class="custom-select-container" />' // Element that wraps the select to enable positioning
    };
    if (settings) {
      $.extend(config, settings);
    }
    this.each(function() {
      var select = $(this);
      select.addClass(config.replacedClass);
      select.wrap(config.wrapperElement);
      var update = function() {
        val = $('option:selected', this).text();
        span.find('span span').text(val);
      };
      // Update the fake select when the real selectâ€™s value changes
      select.change(update);
      /* Gecko browsers don't trigger onchange until the select closes, so
       * changes made by using the arrow keys aren't reflected in the fake select.
       * See https://bugzilla.mozilla.org/show_bug.cgi?id=126379.
       * IE normally triggers onchange when you use the arrow keys to change the selected
       * option of a closed select menu. Unfortunately jQuery doesnâ€™t seem able to bind to this.
       * As a workaround the text is also updated when any key is pressed and then released
       * in all browsers, not just in Firefox.
       */
      select.keyup(update);
      /* Create and insert the spans that will be styled as the fake select
       * To prevent (modern) screen readers from announcing the fake select in addition to the real one,
       * aria-hidden is used to hide it.
       */
      // Three nested spans? The only way I could get text-overflow:ellipsis to work in IE7.
      var span = $('<span class="' + config.customSelectClass + '" aria-hidden="true"><span><span>' + $('option:selected', this).text() + '</span></span></span>');
      select.after(span);
      // Change class names to enable styling of hover/focus states
      select.bind({
        mouseenter: function() {
          span.addClass(config.activeClass);
        },
        mouseleave: function() {
          span.removeClass(config.activeClass);
        },
        focus: function() {
          span.addClass(config.activeClass);
        },
        blur: function() {
          span.removeClass(config.activeClass);
        }
      });
    });
  };
})(jQuery);



$(document).ready(function() {
  $('select').not('.stars-rating').customSelect();
  $('.js-form-expand').hide();
  $('.js-form-trigger').click(function() {
    $(this).toggleClass('is-active');
    $(this).parents('.js-form').find('.js-form-expand').slideToggle('fast');
  });

  $('.brand-expand').click(function() {
      vendor = $(this).parent('.js-form');
      vendor.children('.checkbox:gt(10)').toggleClass('hidden',300);
      $(this).toggleClass('is-active');
      if ($(this).text()=='Показать все бренды') {
        $(this).text('Показать основные бренды');
      } else {
        $(this).text('Показать все бренды');
      }
  });

  $('.js-form-link').click(function() {
    var input = $(this).parents('.js-form-wrapper').find('.js-form-input');
    if (input.is(':disabled')) {
      input.prop("disabled", false);
    } else {
      input.prop("disabled", true);
    }
  });

  $('.checkbox').click(function() {
    var filter = $(this).children('input');
    var filter_name = $(filter).val();
    var filter_id = $(filter).attr('id');
    $('<div class="choose-el" data-filter="' + filter_id + '"><span class="icon del-icon"></span>' + filter_name + '</div>').appendTo('.filter-selected-items');
    if ($(this).hasClass('disabled')) {
      $(this).find('input[type="checkbox"]').prop('disabled', true);
    }
  });

  $(document).on('change', $('#mse2_selected').size(), function(e) {
      if($('#mse2_selected span').size() > 1) {
        $('#mse2_selected').removeClass('hidden');
      } else {
        $('#mse2_selected').addClass('hidden');
      }
    });

  /*$(document).on("click", '.del-icon', function(e) {
    var filter_id = $(this).parent().data('filter');
    $(this).parent().remove();
    console.log(filter_id);
  });*/

  $('.delete-filters').click(function(e) {
    e.preventDefault();
    $('#mse2_filters').trigger('reset').submit();
    $('.filter-selected-items').empty();
    $('#mse2_selected span').empty();
    $('#mse2_selected').addClass('hidden');
  });

  $(document).on("click", "a,button", function(e) {
    var action = $(this).data('action');
    if (action) {
      switch (action) {
        case 'cart/remove':
          var cart_row = $(this).parent('.product-option');
          var key = cart_row.data('product-key');
          cart_row.fadeOut(300, function() {
            $(this).remove();
          });
          removeFromCart(key);
          break;
        case 'cart/add':
          var product_id = $(this).data('product-id');
          var size = $(this).data('size');
          var count = $(this).next().children('.count-products').val();
          addToCart(product_id, size, 1);
          if(count < 1) {
            $(this).next().children('.count-products').val(1);
          }
          break;
        case 'cart/update':
          break;
        case 'cart/get':
          break;
        case 'pet/update':
        e.preventDefault();
          var form = $(this).closest('form');
          var postData = new FormData(form[0]);
          //petUpdate(postData);
              $.ajax({
                type: "POST",
                url: '/assets/components/pets/updatepetdata.php',
                enctype: 'multipart/form-data',
                data: postData,
                processData: false,  // tell jQuery not to process the data
                contentType: false   // tell jQuery not to set contentType
              }).done(function( data ) {
                data = $.parseJSON(data);
                var photo = form.find('.img-circle');
                if(data.newphoto)
                  photo.attr('src',data.newphoto);
              });
          break;
        case 'pet/remove':
          e.preventDefault();
          var pet_id = $(this).closest('form').find('input[name="pet_id"]').val();
          $(this).closest('form').remove();
          petRemove(pet_id);
          if($('#petlist form').size() == 0)
            $('#petlist .info-msg').show();
          else
            $('#petlist .info-msg').hide();
          break;
        default:
          console.log('Received unknown action "' + action + '"');
      }
    }
  });

  $('#deliveries').on('change', function(e) {
      var delivery_obj = $(this).find(':selected');
      var delivery_payments = $(this).find(':selected').data('payments');
      $('#payments option').each(function() {
          var payment = $(this).val();
          if($.inArray(payment, delivery_payments) == -1) {
              $(this).prop('disabled', 'disabled').addClass('hidden');
              $('.payment-descr').addClass('hidden');
          } else {
              $(this).prop('disabled', false).removeClass('hidden');
              
          }
      });
      $('#payments').prop('selectedIndex',0);
  });

  $(".payment-descr[data-payment-id='" + $('#payments').find(':selected').val() +"']").removeClass('hidden');

  $('#payments').on('change', function(e) {
    $('.payment-descr').addClass('hidden');
      $(".payment-descr[data-payment-id='" + $(this).val() +"']").removeClass('hidden');
  });

});

$('#getorder input[name="msorder"]').on('keyup', function() {
    var msorder = $(this).val();
    var name = $('#getorder').find('.name');
    $.ajax({
      type: "GET",
      url: '/getorder',
      data: {
        msorder: msorder
      },
      success: function(data) {
        data = $.parseJSON(data);
        if (data.success === true) {
          $(name).text(data.data.name);
          $('#getorder').find('.green-check-icon').removeClass('hidden');
          $('#getorder').find('.red-check-icon').addClass('hidden');
          $('#getorder').find('.product-price').text(data.data.total);
          $('#getorder').find('.modal-footer').removeClass('hidden');
        } else {
          $(name).text(data.message);
          $('#getorder').find('.green-check-icon').addClass('hidden');
          $('#getorder').find('.red-check-icon').removeClass('hidden');
          $('#getorder').find('.modal-footer').addClass('hidden');
        }
      }
    });
});

$(document).on("click", "#order_submit", function(e) {
  $('#msOrder').submit();
  //$("#msOrder").ajaxSubmit({url: action_url, type: 'post'})
});

$( ".count-products" ).each(function( index ) {
  if($(this).val() > 0) {
    $(this).closest('.count-box').removeClass('hidden');
    $(this).closest('.card-button').children('.add-cart').addClass('hidden');
  }
});

action_url = '/assets/components/minishop2/action.php';

function addToCart(product_id, size, count) {
  if (!count) count = 1;
  var productData = {
    count: count,
    ctx: "web",
    id: product_id,
    options: {
      size: size
    },
    ms2_action: 'cart/add'
  };
  $.ajax({
    type: "POST",
    url: action_url,
    data: productData,
    success: function(data) {
      data = $.parseJSON(data);
      if (data.success === true) {
        if (data.data.total_count > 0) {
          $('.basket-header .total').html(data.data.total_count + ' товаров - ' + data.data.total_cost + ' грн.');
          if ($('#msMiniCart .basket-btn').hasClass('hidden'))
          $('#msMiniCart .basket-btn').removeClass('hidden');
        } else {
          $('.basket-header .total').html('Ваша корзина пуста');
          $('#msMiniCart .basket-btn').addClass('hidden');
        }
      } else if (data.success === false) {

      }
    }
  });
}

function cartChange(key, count) {
  var productData = {
    count: count,
    ctx: "web",
    key: key,
    ms2_action: 'cart/change'
  };
  $.ajax({
    async: "false",
    type: "POST",
    url: action_url,
    data: productData,
    success: function(data) {
      data = $.parseJSON(data);
      if (data.success === true) {
        var total = data.data.total_cost;
        $.each( data.data.items, function( i, val ) {
          if(key == val.key) {
            //var product = $('.basket-item').data('product-key', key);
            var product = $('*[data-product-key='+key+']');
            $(product).find('.price-large b').text(val.cost);
          }
        });
        $('.price-summary .price-xlarge').text(total);
        if (data.data.total_count > 0) {
          $('.basket-header .total').html(data.data.total_count + ' товаров - ' + data.data.total_cost + ' грн.');
          if ($('#msMiniCart .basket-btn').hasClass('hidden'))
          $('#msMiniCart .basket-btn').removeClass('hidden');
        } else {
          $('.basket-header .total').html('Ваша корзина пуста');
          $('#msMiniCart .basket-btn').addClass('hidden');
        }
      } else if (data.success === false) {

      }
    }
  });
}

function removeFromCart(key) {
  var productData = {
    key: key,
    ctx: "web",
    ms2_action: 'cart/remove'
  };
  $.ajax({
    type: "POST",
    url: action_url,
    data: productData,
    success: function(data) {
      data = $.parseJSON(data);
      if (data.success === true) {
        if (data.data.total_count > 0)
          $('.basket-header .total').html(data.data.total_count + ' товаров - ' + data.data.total_cost + ' грн.');
        else
          $('.basket-header .total').html('Ваша корзина пуста');
      } else if (data.success === false) {

      }
    }
  });
}

function getCart() {
  var productData = {
    ctx: "web",
    ms2_action: 'cart/get'
  };
  $.ajax({
    async: "false",
    type: "GET",
    url: action_url,
    data: productData,
    success: function(data) {
      data = $.parseJSON(data);
      if (data.success === true) {
        if (data.data.total_count > 0) {
          $('.basket-header .total').html(data.data.total_count + ' товаров - ' + data.data.total_cost + ' грн.');
          if ($('#msMiniCart .basket-btn').hasClass('hidden'))
          $('#msMiniCart .basket-btn').removeClass('hidden');
        } else {
          $('.basket-header .total').html('Ваша корзина пуста');
          $('#msMiniCart .basket-btn').addClass('hidden');
        }
      } else if (data.success === false) {

      }
    }
  });
}

function cleanCart() {
  var productData = {
    ctx: "web",
    ms2_action: 'cart/clean'
  };
  $.ajax({
    type: "GET",
    url: action_url,
    data: productData
  });
}

$('.add-cart').on('click', function(e) {
  var parent = $(this).closest('.ms2_product');
  var product_id = $(parent).find('input[name="product_id"]').val();
  var size = $(this).closest('.card-costs-item').find('.weight').text();
  var count = $(this).next().children('.count-products').val();
  if(count < 1) {
    $(this).next().children('.count-products').val(1);
  }
  addToCart(product_id, size, 1);
});

function appendPet(data) {
  //$('#add_pet_form').clone().appendTo('#petlist');
  var clone = $('#add_pet_form').clone();
  clone.removeAttr('id');
  //data = $.parseJSON(data);
  /*if(data.newphoto) {
    clone.find('.img-circle').attr('src',data.newphoto);
  }
  clone.find('select[name="type"]').val(data.type);
  clone.find('select[name="breed"]').val(data.breed);
  clone.find('select[name="genre"]').val(data.genre);
  clone.find('input[name="name"]').val(data.name);
  clone.find('input[name="pet_id"]').val(data.pet_id);
  clone.find('input[name="af_action"]').val(data.af_action);
  clone.find('textarea').text(data.comment);
  clone.find('.col-xs-2').removeClass('hide');*/
  //data.appendTo('#petlist');
  $( "#petlist" ).append(data);
  $('#add_pet_form')[0].reset();
  $('select').customSelect();
}
function petRemove(id) {
  var petData = {
    id: id
  };
  $.ajax({
    type: "POST",
    url: '/assets/components/pets/removepetdata.php',
    data: petData
  });
}
function petUpdate(data) {
  $.ajax({
    type: "POST",
    url: '/assets/components/pets/updatepetdata.php',
    enctype: 'multipart/form-data',
    data: data,
    processData: false,  // tell jQuery not to process the data
    contentType: false   // tell jQuery not to set contentType
  }).done(function( data ) {
      console.log("PHP Output:");
      console.log( data );
  });
}

function getPayments(delivery_id) {
  var paymentData = {
    id: delivery_id
  };
  $.ajax({
    async: "false",
    type: "GET",
    url: '/assets/components/delivery/getpaymentdata.php',
    data: paymentData,
    success: function(data) {
      data = $.parseJSON(data);
      if (data.success === true) {

      } else if (data.success === false) {

      }
    }
  });
}

var frm = $('#comment-form');
frm.submit(function (ev) {
    $.ajax({
        type: frm.attr('method'),
        url: frm.attr('action'),
        data: frm.serialize(),
        success: function (data) {
            frm[0].reset();
            $('#feedbackModal').modal('hide');
        }
    });

    ev.preventDefault();
});



$('#petlist').bind('DOMSubtreeModified', function() {
  if($('#petlist form').size() == 0)
    $('#petlist .info-msg').show();
  else
    $('#petlist .info-msg').hide();
});

var formHasChanged = false;
var submitted = false;

/*$(document).on('change', '#office-profile-form input, #office-profile-form select, #office-profile-form textarea', function (e) {
    formHasChanged = true;
});

$(document).ready(function () {
    window.onbeforeunload = function (e) {
        if (formHasChanged && !submitted) {
            var message = "Вы не сохранили изменения.", e = e || window.event;
            if (e) {
                e.returnValue = message;
            }
            return message;
        }
    }
});*/

$(document).on("change", 'input[name="newphoto"]', function(){
    $(this).closest('form').find('button[type="submit"]').trigger('click');
});