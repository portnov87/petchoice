<?php
/**
 * @package modx
 * @subpackage manager.controllers
 */

/**
 * Loads update user page
 *
 * @package modx
 * @subpackage manager.controllers
 */
class SecurityUserUpdateManagerController extends modManagerController
{
    /** @var string $onUserFormRender */
    public $onUserFormRender = '';
    /** @var array $extendedFields */
    public $extendedFields = array();
    /** @var array $remoteFields */
    public $remoteFields = array();
    /** @var modUser $user */
    public $user;
    public $pets;

    /**
     * Check for any permissions or requirements to load page
     * @return bool
     */
    public function checkPermissions()
    {
        return $this->modx->hasPermission('edit_user');
    }

    /**
     * Register custom CSS/JS for the page
     * @return void
     */
    public function loadCustomCssJs()
    {
        $mgrUrl = $this->modx->getOption('manager_url', null, MODX_MANAGER_URL);
        $this->addHtml('<script type="text/javascript">
// <![CDATA[
MODx.onUserFormRender = "' . $this->onUserFormRender . '";
// ]]>
</script>');

        /* register JS scripts */


        $this->addJavaScript('/assets/components/minishop2/js/mgr/minishop2.js');
//<script type="text/javascript" src="/manager/min/index.php?f=/assets/components/minishop2/js/mgr/minishop2.js,
///assets/components/minishop2/js/mgr/misc/ms2.utils.js,/assets/components/minishop2/js/mgr/misc/ms2.combo.js,/assets/components/minishop2/js/mgr/orders/orders.grid.js,/assets/components/minishop2/js/mgr/orders/orders.panel.js"></script>

        $this->addHtml('	
		
<script type="text/javascript">
	miniShop2.config = {"assetsUrl":"\/assets\/components\/minishop2\/","cssUrl":"\/assets\/components\/minishop2\/css\/","jsUrl":"\/assets\/components\/minishop2\/js\/","jsPath":"\/home\/petchoic\/petchoice.com.ua\/www\/assets\/components\/minishop2\/js\/","imagesUrl":"\/assets\/components\/minishop2\/images\/","customPath":"\/home\/petchoic\/petchoice.com.ua\/www\/core\/components\/minishop2\/custom\/","connectorUrl":"\/assets\/components\/minishop2\/connector.php","actionUrl":"\/assets\/components\/minishop2\/action.php","corePath":"\/home\/petchoic\/petchoice.com.ua\/www\/core\/components\/minishop2\/","assetsPath":"\/home\/petchoic\/petchoice.com.ua\/www\/assets\/components\/minishop2\/","modelPath":"\/home\/petchoic\/petchoice.com.ua\/www\/core\/components\/minishop2\/model\/","ctx":"web","json_response":false,"templatesPath":"\/home\/petchoic\/petchoice.com.ua\/www\/core\/components\/minishop2\/elements\/templates\/"};
	miniShop2.config.connector_url = "/assets/components/minishop2/connector.php";
</script>
<script type="text/javascript">
	miniShop2.config.order_grid_fields = ["id","num","customer","status","cost","weight","delivery","payment","createdon","updatedon","comment","user_id","type","actions"];
	miniShop2.config.order_address_fields = ["receiver","phone","city","comment"];
	miniShop2.config.order_product_fields = ["createdon","order_num","status","name","product_article","option_size","price_old","count","cost","id","product_id"];

	Ext.onReady(function() {
		MODx.load({ xtype: "minishop2-page-orders"});
	});
</script>');

        $this->addJavascript($mgrUrl . 'assets/modext/util/datetime.js');
        $this->addJavascript($mgrUrl . 'assets/modext/widgets/core/modx.orm.js');
        $this->addJavascript($mgrUrl . 'assets/modext/widgets/core/modx.grid.settings.js');
        $this->addJavascript($mgrUrl . 'assets/modext/widgets/security/modx.grid.user.settings.js');
        $this->addJavascript($mgrUrl . 'assets/modext/widgets/security/modx.grid.user.group.js');

        $this->addJavascript('/backend/assets/modext/util/utilities.js');
        $this->addJavascript('/assets/components/minishop2/js/mgr/misc/ms2.utils.js');


        $this->addJavascript($mgrUrl . 'assets/modext/widgets/security/modx.panel.user.js');


        $this->addJavascript($mgrUrl . 'assets/modext/sections/security/user/update.js');


        $this->addHtml('<script type="text/javascript">
// <![CDATA[
Ext.onReady(function() {
    MODx.load({
        xtype: "modx-page-user-update"
        ,user: "' . $this->user->get('id') . '"
        ' . (!empty($this->remoteFields) ? ',remoteFields: ' . $this->modx->toJSON($this->remoteFields) : '') . '
		' . (!empty($this->pets) ? ',pets: ' . $this->modx->toJSON($this->pets) : '') . '
        ' . (!empty($this->extendedFields) ? ',extendedFields: ' . $this->modx->toJSON($this->extendedFields) : '') . '
    });
});
// ]]>
</script>');

    }

    /**
     * Custom logic code here for setting placeholders, etc
     * @param array $scriptProperties
     * @return mixed
     */
    public function process(array $scriptProperties = array())
    {
        $placeholders = array();
        //echo 'eedededed';die();
        /* get user */
        if (empty($scriptProperties['id']) || strlen($scriptProperties['id']) !== strlen((integer)$scriptProperties['id'])) {
            return $this->failure($this->modx->lexicon('user_err_ns'));
        }
        $this->user = $this->modx->getObject('modUser', array('id' => $scriptProperties['id']));
        if ($this->user == null) return $this->failure($this->modx->lexicon('user_err_nf'));

        /* process remote data, if existent */
        $this->remoteFields = array();
        $remoteData = $this->user->get('remote_data');
        if (!empty($remoteData)) {
            $this->remoteFields = $this->_parseCustomData($remoteData);
        }

        /* parse extended data, if existent */
        $this->user->getOne('Profile');
        if ($this->user->Profile) {
            $this->extendedFields = array();
            $extendedData = $this->user->Profile->get('extended');
            if (!empty($extendedData)) {
                $this->extendedFields = $this->_parseCustomData($extendedData);
            }
        }

        /* invoke OnUserFormPrerender event */
        $onUserFormPrerender = $this->modx->invokeEvent('OnUserFormPrerender', array(
            'id' => $this->user->get('id'),
            'user' => &$this->user,
            'mode' => modSystemEvent::MODE_UPD,
        ));
        if (is_array($onUserFormPrerender)) {
            $onUserFormPrerender = implode('', $onUserFormPrerender);
        }
        $placeholders['OnUserFormPrerender'] = $onUserFormPrerender;

        /* invoke OnUserFormRender event */
        $onUserFormRender = $this->modx->invokeEvent('OnUserFormRender', array(
            'id' => $this->user->get('id'),
            'user' => &$this->user,
            'mode' => modSystemEvent::MODE_UPD,
        ));
        if (is_array($onUserFormRender)) $onUserFormRender = implode('', $onUserFormRender);
        $this->onUserFormRender = str_replace(array('"', "\n", "\r"), array('\"', '', ''), $onUserFormRender);
        $placeholders['OnUserFormRender'] = $this->onUserFormRender;

        $this->pets = '';

        $q = $this->modx->newQuery('msPet');

        $q->where(array('user_id' => $scriptProperties['id']));
        $q->leftJoin('msPetType', 'type', '`msPet`.`type`=`type`.`id`');
        $q->leftJoin('msBreed', 'breed', '`msPet`.`breed`=`breed`.`id`');
        $q->select(array(
            'msPet.*',
            'type.name as nametype',
            'breed.name as namebreed',
        ));

        $pets = $this->modx->getCollection('modResource', $q);

        if (!empty($pets)) {

            foreach ($pets as $pet) {
                $petArray = $pet->toArray();
                $petar[] = 'Тип животного: ' . $petArray['nametype'] . '<br/> Порода: ' . $petArray['namebreed'] . "<br/>Имя: " . $petArray['name'] . "<br/>" .
                    (!empty($petArray['comment']) ? 'Комментарий: ' . $petArray['comment'] : '') .
                    (!empty($petArray['photo']) ? "</br> <img width=\"50\" src=\"" . $petArray['photo'] . "\"/>" : '');
                //">";

            }

            $petsstr = implode('<br/><br/>', $petar);
        } else {

            $petsstr = 'Нет питомцев';
        }
        $fieldspets = array();//[]
        $fieldspets[] = array('value' => $petsstr);
        $this->pets = array('value' => $petsstr);//$fieldspets;//array($petsstr);//array($petsstr);
        //$petsstr;

        return $placeholders;
    }

    private function _parseCustomData(array $remoteData = array(), $path = '')
    {
        $fields = array();
        foreach ($remoteData as $key => $value) {
            $field = array(
                'name' => $key,
                'id' => (!empty($path) ? $path . '.' : '') . $key,
            );
            if (is_array($value)) {
                $field['text'] = $key;
                $field['leaf'] = false;
                $field['children'] = $this->_parseCustomData($value, $key);
            } else {
                $v = $value;
                /*if (strlen($v) > 30) {
                    $v = substr($v, 0, 30) . '...';
                }*/
                $field['text'] = $key . ' - <i>' . $v . '</i>';
                $field['leaf'] = true;
                $field['value'] = $value;
            }
            $fields[] = $field;
        }
        /*echo '<pre>$scriptProperties';
        print_r( $fields);
        echo '</pre>';*/
        return $fields;
    }

    /**
     * Return the pagetitle
     *
     * @return string
     */
    public function getPageTitle()
    {
        return $this->modx->lexicon('user') . ': ' . $this->user->get('username');
    }

    /**
     * Return the location of the template file
     * @return string
     */
    public function getTemplateFile()
    {
        return 'security/user/update.tpl';
    }

    /**
     * Specify the language topics to load
     * @return array
     */
    public function getLanguageTopics()
    {
        return array('user', 'setting', 'access');
    }
}
