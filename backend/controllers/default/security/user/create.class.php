<?php
/**
 * Loads the create user page
 *
 * @package modx
 * @subpackage manager.controllers
 */
class SecurityUserCreateManagerController extends modManagerController {
    public $onUserFormRender;
    /**
     * Check for any permissions or requirements to load page
     * @return bool
     */
    public function checkPermissions() {
        return $this->modx->hasPermission('new_user');
    }

    /**
     * Register custom CSS/JS for the page
     * @return void
     */
    public function loadCustomCssJs() {
        $mgrUrl = $this->modx->getOption('manager_url',null,MODX_MANAGER_URL);
        $this->addJavascript($mgrUrl.'assets/modext/widgets/core/modx.orm.js');
        $this->addJavascript($mgrUrl.'assets/modext/widgets/security/modx.grid.user.group.js');
        $this->addJavascript($mgrUrl.'assets/modext/widgets/security/modx.panel.user.js');
        $this->addHtml('<script type="text/javascript">
        // <![CDATA[
        Ext.onReady(function() {
            MODx.load({ xtype: "modx-page-user-create" });
        });
        MODx.onUserFormRender = "'.$this->onUserFormRender.'";
        // ]]>
        </script>');
		
		
		
		
		
		
		$this->addJavaScript('/assets/components/minishop2/js/mgr/minishop2.js');
//<script type="text/javascript" src="/manager/min/index.php?f=/assets/components/minishop2/js/mgr/minishop2.js,
///assets/components/minishop2/js/mgr/misc/ms2.utils.js,/assets/components/minishop2/js/mgr/misc/ms2.combo.js,/assets/components/minishop2/js/mgr/orders/orders.grid.js,/assets/components/minishop2/js/mgr/orders/orders.panel.js"></script>

		$this->addHtml('	
		
<script type="text/javascript">
	miniShop2.config = {"assetsUrl":"\/assets\/components\/minishop2\/","cssUrl":"\/assets\/components\/minishop2\/css\/","jsUrl":"\/assets\/components\/minishop2\/js\/","jsPath":"\/home\/petchoic\/petchoice.com.ua\/www\/assets\/components\/minishop2\/js\/","imagesUrl":"\/assets\/components\/minishop2\/images\/","customPath":"\/home\/petchoic\/petchoice.com.ua\/www\/core\/components\/minishop2\/custom\/","connectorUrl":"\/assets\/components\/minishop2\/connector.php","actionUrl":"\/assets\/components\/minishop2\/action.php","corePath":"\/home\/petchoic\/petchoice.com.ua\/www\/core\/components\/minishop2\/","assetsPath":"\/home\/petchoic\/petchoice.com.ua\/www\/assets\/components\/minishop2\/","modelPath":"\/home\/petchoic\/petchoice.com.ua\/www\/core\/components\/minishop2\/model\/","ctx":"web","json_response":false,"templatesPath":"\/home\/petchoic\/petchoice.com.ua\/www\/core\/components\/minishop2\/elements\/templates\/"};
	miniShop2.config.connector_url = "/assets/components/minishop2/connector.php";
</script>
<script type="text/javascript">
	miniShop2.config.order_grid_fields = ["id","num","customer","status","cost","weight","delivery","payment","createdon","updatedon","comment","user_id","type","actions"];
	miniShop2.config.order_address_fields = ["receiver","phone","city","comment"];
	miniShop2.config.order_product_fields = ["createdon","order_num","status","name","product_article","option_size","price_old","count","cost","id","product_id"];

	Ext.onReady(function() {
		MODx.load({ xtype: "minishop2-page-orders"});
	});
</script>');

$this->addJavascript($mgrUrl.'assets/modext/util/datetime.js');
        
        $this->addJavascript($mgrUrl.'assets/modext/widgets/core/modx.grid.settings.js');
        $this->addJavascript($mgrUrl.'assets/modext/widgets/security/modx.grid.user.settings.js');
        
		
		$this->addJavascript('/backend/assets/modext/util/utilities.js');
		$this->addJavascript('/assets/components/minishop2/js/mgr/misc/ms2.utils.js');
		
		
		
        
		
        $this->addJavascript($mgrUrl.'assets/modext/sections/security/user/create.js');

    }

    /**
     * Custom logic code here for setting placeholders, etc
     * @param array $scriptProperties
     * @return mixed
     */
    public function process(array $scriptProperties = array()) {
        $placeholders = array();

        /* invoke OnUserFormPrerender event */
        $onUserFormPrerender = $this->modx->invokeEvent('OnUserFormPrerender', array(
            'id' => 0,
            'mode' => modSystemEvent::MODE_NEW,
        ));
        if (is_array($onUserFormPrerender)) $onUserFormPrerender = implode('',$onUserFormPrerender);
        $placeholders['OnUserFormPrerender'] = $onUserFormPrerender;

        /* invoke OnUserFormRender event */
        $this->onUserFormRender = $this->modx->invokeEvent('OnUserFormRender', array(
            'id' => 0,
            'mode' => modSystemEvent::MODE_NEW,
        ));
        if (is_array($this->onUserFormRender)) $this->onUserFormRender = implode('',$this->onUserFormRender);
        $this->onUserFormRender = str_replace(array('"',"\n","\r"),array('\"','',''),$this->onUserFormRender);

        $placeholders['OnUserFormRender'] = $this->onUserFormRender;

        return $placeholders;
    }

    /**
     * Return the pagetitle
     *
     * @return string
     */
    public function getPageTitle() {
        return $this->modx->lexicon('user_new');
    }

    /**
     * Return the location of the template file
     * @return string
     */
    public function getTemplateFile() {
        return 'security/user/create.tpl';
    }

    /**
     * Specify the language topics to load
     * @return array
     */
    public function getLanguageTopics() {
        return array('user','setting','access');
    }

    /**
     * Get the Help URL
     * @return string
     */
    public function getHelpUrl() {
        return 'Users';
    }
}