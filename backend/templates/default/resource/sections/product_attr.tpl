{$OnResourceattrFormPrerender}

<!--<input type="hidden" name="attrs" value="1" />-->

{foreach from=$allattribute item=attribute}
    <div  class="x-form-item x-tab-item modx-attr attr-first">


        <label class="x-form-item-label modx-attr-label" for="attr{$attribute.id}">
            <div class="modx-attr-label-title">
                <span class="modx-attr-caption">{if $attribute.name}{$attribute.name}{/if}</span>
            </div>

        </label>

        <div class="x-form-element modx-attr-form-element">
            {foreach from=$attribute.child item=attr}
                <!--<input type="checkbox" name="attribute[{$attribute.id}][{$attr.id}]"/>
			<label>{$attr.name}</label>-->

                <div id="x-form-el-attr{$attribute.id}-0" class="x-form-element" style="padding-left:105px">
                    <div id="ext-gen{$attribute.id}" class="x-form-check-wrap" style="width: 644px;">
                        <input id="attr{$attribute.id}-{$attr.id}" {if ($attr.select==true)}checked="checked"{/if} class=" x-form-checkbox x-form-field" type="checkbox" name="attribute[{$attribute.id}][{$attr.id}]" autocomplete="off" value="{$attr.name}">
                        <label class="x-form-cb-label" for="attr{$attribute.id}-{$attr.id}">{$attr.name}</label>
                    </div>
                </div>
            {/foreach}
        </div>
    </div>
{/foreach}


{literal}
<script type="text/javascript">
    // <![CDATA[
    Ext.onReady(function() {
        MODx.resetAttr = function(id) {
            var i = Ext.get('attr'+id);
            var d = Ext.get('attrdef'+id);

            if (i) {
                i.dom.value = d.dom.value;
                i.dom.checked = d.dom.value ? true : false;
            }
            var c = Ext.getCmp('attr'+id);
            if (c) {
                if (c.xtype == 'checkboxgroup' || c.xtype == 'radiogroup') {
                    var cbs = d.dom.value.split(',');
                    for (var i=0;i<c.items.length;i++) {
                        if (c.items.items[i]) {
                            c.items.items[i].seattralue(cbs.indexOf(c.items.items[i].id) != -1);
                        }
                    }
                } else {
                    c.seattralue(d.dom.value);
                }
            }
            var p = Ext.getCmp('modx-panel-resource');
            if (p) {
                p.markDirty();
                p.fireEvent('attr-reset',{id:id});
            }
        };

        Ext.select('.modx-attr-reset').on('click',function(e,t,o) {
            var id = t.id.split('-');
            id = id[3];
            MODx.resetattr(id);
        });
        MODx.refreshattrs = function() {
            if (MODx.unloadattrRTE) { MODx.unloadattrRTE(); }
            Ext.getCmp('modx-panel-resource-attr').refreshattrs();
        };
        {/literal}{if $attrcount GT 0}{literal}
        MODx.load({
            xtype: 'modx-vtabs'
            ,applyTo: 'modx-attr-tabs'
            ,autoTabs: true
            ,border: false
            ,plain: true
            ,deferredRender: false
            ,id: 'modx-resource-vtabs'
            ,headerCfg: {
                tag: 'div'
                ,cls: 'x-tab-panel-header vertical-tabs-header'
                ,id: 'modx-resource-vtabs-header'
                ,html: MODx.config.show_attr_categories_header == true ? '<h4 id="modx-resource-vtabs-header-title">'+_('categories')+'</h4>' : ''
            }
        });
        {/literal}{/if}

        MODx.attrCounts = '{$attrCounts}';
        MODx.attrMap = '{$attrMap}';
        {literal}
    });
    // ]]>
</script>
{/literal}

{$OnResourceattrFormRender}

<div class="clear"></div>
