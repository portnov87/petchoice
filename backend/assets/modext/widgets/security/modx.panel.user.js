/**
 * @class MODx.panel.User
 * @extends MODx.FormPanel
 * @param {Object} config An object of configuration properties
 * @xtype modx-panel-user
 */
MODx.panel.User = function (config) {
    config = config || {};
    Ext.applyIf(config, {
        url: MODx.config.connector_url
        , baseParams: {}
        , id: 'modx-panel-user'
        , cls: 'container'
        , defaults: {collapsible: false, autoHeight: true}
        , bodyStyle: ''
        , items: [{
            html: '<h2>' + _('user_new') + '</h2>'
            , border: false
            , cls: 'modx-page-header'
            , id: 'modx-user-header'
        }, {
            xtype: 'modx-tabs'
            , id: 'modx-user-tabs'
            , deferredRender: false
            , defaults: {
                autoHeight: true
                , layout: 'form'
                , labelWidth: 150
                , bodyCssClass: 'tab-panel-wrapper'
                , layoutOnTabChange: true
            }
            , items: this.getFields(config)
        }]
        , useLoadingMask: true
        , listeners: {
            'setup': {fn: this.setup, scope: this}
            , 'success': {fn: this.success, scope: this}
            , 'beforeSubmit': {fn: this.beforeSubmit, scope: this}
        }
    });
    MODx.panel.User.superclass.constructor.call(this, config);
    Ext.getCmp('modx-user-panel-newpassword').getEl().dom.style.display = 'none';
    Ext.getCmp('modx-user-password-genmethod-s').on('check', this.showNewPassword, this);
};
Ext.extend(MODx.panel.User, MODx.FormPanel, {
    setup: function () {
        if (this.config.user === '' || this.config.user === 0) {
            this.fireEvent('ready');
            return false;
        }
        MODx.Ajax.request({
            url: this.config.url
            , params: {
                action: 'security/user/get'
                , id: this.config.user
                , getGroups: true
            }
            , listeners: {
                'success': {
                    fn: function (r) {
                        this.getForm().setValues(r.object);

                        var d = Ext.decode(r.object.groups);
                        var g = Ext.getCmp('modx-grid-user-groups');
                        if (g) {
                            var s = g.getStore();
                            if (s) {
                                s.loadData(d);
                            }
                        }
                        Ext.get('modx-user-header').update('<h2>' + _('user') + ': ' + r.object.username + '</h2>');
                        this.fireEvent('ready', r.object);
                        MODx.fireEvent('ready');
                    }, scope: this
                }
            }
        });
    }
    , beforeSubmit: function (o) {
        var d = {};
        var g = Ext.getCmp('modx-grid-user-settings');
        if (g) {
            d.settings = g.encodeModified();
        }

        var h = Ext.getCmp('modx-grid-user-groups');
        if (h) {
            d.groups = h.encode();
        }

        var t = Ext.getCmp('modx-remote-tree');
        if (t) {
            d.remote_data = t.encode();
        }

        var et = Ext.getCmp('modx-extended-tree');
        if (et) {
            d.extended = et.encode();
        }

        Ext.apply(o.form.baseParams, d);
    }

    , success: function (o) {
        var userId = this.config.user;
        if (Ext.getCmp('modx-user-passwordnotifymethod-s').getValue() === true && o.result.message != '') {
            Ext.Msg.hide();
            Ext.Msg.show({
                title: _('password_notification')
                , msg: o.result.message
                , buttons: Ext.Msg.OK
                , fn: function (btn) {
                    if (userId == 0) {
                        MODx.loadPage('security/user/update', 'id=' + o.result.object.id);
                    }
                    return false;
                }
            });
            this.clearDirty();
        } else if (userId == 0) {
            MODx.loadPage('security/user/update', 'id=' + o.result.object.id);
        }
    }

    , showNewPassword: function (cb, v) {
        var el = Ext.getCmp('modx-user-panel-newpassword').getEl();
        if (v) {
            el.slideIn('t', {useDisplay: true});
        } else {
            el.slideOut('t', {useDisplay: true});
        }
    }

    , getFields: function (config) {
        var f = [{
            title: _('general_information')
            , defaults: {msgTarget: 'side', autoHeight: true}
            , cls: 'main-wrapper form-with-labels'
            , labelAlign: 'top' // prevent default class of x-form-label-left
            , items: this.getGeneralFields(config)
        }];

        if (config.remoteFields && config.remoteFields.length) {
            f.push({
                title: _('remote_data')
                , layout: 'form'
                , defaults: {border: false, autoHeight: true}
                , hideMode: 'offsets'
                , items: [{
                    html: '<p>' + _('user_remote_data_msg') + '</p>'
                    , bodyCssClass: 'panel-desc'
                }, {
                    layout: 'column'
                    , cls: 'main-wrapper'
                    , items: [{
                        columnWidth: 0.4
                        , title: _('attributes')
                        , layout: 'fit'
                        , border: false
                        , items: {
                            xtype: 'modx-orm-tree'
                            , id: 'modx-remote-tree'
                            , data: config.remoteFields
                            , formPanel: 'modx-panel-user'
                            , prefix: 'remote'
                        }
                    }, {
                        xtype: 'modx-orm-form'
                        , columnWidth: 0.6
                        , title: _('editing_form')
                        , id: 'modx-remote-form'
                        , prefix: 'remote'
                        , treePanel: 'modx-remote-tree'
                        , formPanel: 'modx-panel-user'
                    }]
                }]
            });
        }
        config.extendedFields = config.extendedFields || [];
        f.push({
            title: _('extended_fields')
            , layout: 'form'
            , defaults: {border: false, autoHeight: true}
            , hideMode: 'offsets'
            , items: [{
                html: '<p>' + _('extended_fields_msg') + '</p>'
                , bodyCssClass: 'panel-desc'
            }, {
                layout: 'column'
                , cls: 'main-wrapper'
                , items: [{
                    columnWidth: 0.4
                    , title: _('attributes')
                    , layout: 'fit'
                    , border: false
                    , items: {
                        xtype: 'modx-orm-tree'
                        , id: 'modx-extended-tree'
                        , data: config.extendedFields
                        , formPanel: 'modx-panel-user'
                        , prefix: 'extended'
                        , enableDD: true
                        , listeners: {
                            'dragdrop': {
                                fn: function () {
                                    this.markDirty();
                                }, scope: this
                            }
                        }
                    }
                }, {
                    xtype: 'modx-orm-form'
                    , columnWidth: 0.6
                    , title: _('editing_form')
                    , id: 'modx-extended-form'
                    , prefix: 'extended'
                    , treePanel: 'modx-extended-tree'
                    , formPanel: 'modx-panel-user'
                }]
            }]
        });


        f.push({
            title: 'История покупок'
            , layout: 'form'
            , defaults: {border: false, autoHeight: true}
            , hideMode: 'offsets'
            , items: [{
                html: '<p>история</p>'
                , bodyCssClass: 'panel-desc'
            }, {
                xtype: 'modx-grid-orders-products'//modx-grid-user-groups'
                , cls: 'main-wrapper'
                , title: ''
                , preventRender: true
                , user: config.user
                , width: '97%'

            }]
        });
        f.push({
            title: 'История бонусов'
            , layout: 'form'
            , defaults: {border: false, autoHeight: true}
            , hideMode: 'offsets'
            , items: [{
                html: '<p>история бонусов</p>'
                , bodyCssClass: 'panel-desc'
            }, {
                xtype: 'modx-grid-bonuses'//modx-grid-user-groups'
                , cls: 'main-wrapper'
                , title: ''
                , preventRender: true
                , user: config.user
                , width: '97%'

            }]
        });

        f.push({
            title: 'Питомцы'
            , layout: 'form'
            , defaults: {border: false, autoHeight: true}
            , hideMode: 'offsets'
            , items: [{
                html: '<p>список питомцев пользвоателя</p>'
                , bodyCssClass: 'panel-desc'
            }, {
                xtype: 'modx-grid-pets'//modx-grid-user-groups'
                , cls: 'main-wrapper'
                , title: ''
                , preventRender: true
                , user: config.user
                , width: '97%'

            }]
        });

        return f;
    }

    , getGeneralFields: function (config) {
        return [{
            layout: 'column'
            , border: false
            , defaults: {
                layout: 'form'
                , labelAlign: 'top'
                , labelSeparator: ''
                , anchor: '100%'
                , border: false
            }
            , items: [{
                columnWidth: .5
                , defaults: {
                    msgTarget: 'under'
                }
                , items: [{
                    id: 'modx-user-id'
                    , name: 'id'
                    , xtype: 'hidden'
                    , value: config.user
                }, {
                    id: 'modx-user-username'
                    , name: 'username'
                    , fieldLabel: _('username')
                    , description: _('user_username_desc')
                    , xtype: 'textfield'
                    , anchor: '100%'
                }, {
                    id: 'modx-user-fullname'
                    , name: 'fullname'
                    , fieldLabel: _('user_full_name')
                    , xtype: 'textfield'
                    , anchor: '100%'
                    , maxLength: 255
                }, {
                    id: 'modx-user-lastname'
                    , name: 'lastname'
                    , fieldLabel: 'Фамилия'//_('user_full_name')
                    , xtype: 'textfield'
                    , anchor: '100%'
                    , maxLength: 255

                }, {
                    id: 'modx-user-fathername'
                            , name: 'fathername'
                            , fieldLabel: 'Отчество'//_('user_full_name')
                            , xtype: 'textfield'
                            , anchor: '100%'
                            , maxLength: 255
               }, {
                    id: 'modx-user-email'
                    , name: 'email'
                    , fieldLabel: _('user_email')
                    , xtype: 'textfield'
                    , anchor: '100%'
                    , maxLength: 255
                    // ,allowBlank: true
                    //,blankText: 'ddddd'
                }, {
                    id: 'modx-user-phone'
                    , name: 'phone'
                    , fieldLabel: _('user_phone')
                    , xtype: 'textfield'
                    , width: 200
                    , maxLength: 255
                }, {
                    id: 'modx-user-mobilephone'
                    , name: 'mobilephone'
                    , fieldLabel: _('user_mobile')
                    , xtype: 'textfield'
                    , width: 200
                    , maxLength: 255
                }, /*{
                 id: 'modx-user-fax'
                 ,name: 'fax'
                 ,fieldLabel: _('user_fax')
                 ,xtype: 'textfield'
                 ,width: 200
                 ,maxLength: 255
                 },{
                 id: 'modx-user-address'
                 ,name: 'address'
                 ,fieldLabel: _('address')
                 ,xtype: 'textarea'
                 ,anchor: '100%'
                 ,grow: true
                 }*//*,{
                 id: 'modx-user-state'
                 ,name: 'state'
                 ,fieldLabel: _('user_state')
                 ,xtype: 'textfield'
                 ,width: 100
                 ,maxLength: 100
                 },{
                 id: 'modx-user-zip'
                 ,name: 'zip'
                 ,fieldLabel: _('user_zip')
                 ,xtype: 'textfield'
                 ,width: 100
                 ,maxLength: 25
                 },{
                 id: 'modx-user-country'
                 ,fieldLabel: _('user_country')
                 ,xtype: 'modx-combo-country'
                 ,value: ''
                 },{
                 id: 'modx-user-website'
                 ,name: 'website'
                 ,fieldLabel: _('user_website')
                 ,xtype: 'textfield'
                 ,anchor: '100%'
                 ,maxLength: 255
                 }

                 ,{
                 fieldLabel: _('user_photo')
                 ,name: 'photo'
                 ,xtype: 'modx-combo-browser'
                 ,hideFiles: true
                 ,source: MODx.config['photo_profile_source'] || MODx.config.default_media_source
                 ,hideSourceCombo: true
                 ,anchor: '100%'
                 },*/
                    {
                        id: 'modx-user-discount'
                        , name: 'discount'
                        , fieldLabel: 'Дисконт (%)'//_('city')
                        , xtype: 'textfield'
                        , anchor: '100%'
                        , maxLength: 255
                    },
                    {
                        id: 'modx-user-bonuses'
                        , name: 'bonuses'
                        , fieldLabel: 'Бонусы'
                        , xtype: 'textfield'
                        , anchor: '100%'
                        , maxLength: 255
                    },
                    {
                        id: 'modx-user-date_clear_bonuses'
                        , name: 'date_clear_bonuses'
                        , fieldLabel: 'Дата обнуления'
                        , xtype: 'displayfield'
                        , anchor: '100%'
                        , maxLength: 255
                    },


                    {
                        id: 'modx-user-registr',
                        xtype: 'modx-combo-registr',
                        name: 'registr',
                        fieldLabel: 'Регистрация',
                        anchor: '100%'
                    }
                    , {
                        id: 'modx-user-subscribe',
                        xtype: 'modx-combo-subscribe',
                        name: 'subscribe',
                        fieldLabel: 'Подписка',
                        anchor: '100%'
                    },

                    {
                        id: 'modx-user-subscribe_viber'
                        , name: 'subscribe_viber'
                        , fieldLabel: 'Подписка вайбер'
                        , xtype: 'modx-combo-yesno'
                        , anchor: '100%'
                        , maxLength: 255
                    },
                    {
                        id: 'modx-user-viber_status'
                        , name: 'viber_status'
                        , fieldLabel: 'Вайбер статус'
                        , xtype: 'displayfield'
                        , anchor: '100%'
                        , maxLength: 255
                    },
                    {
                        id: 'modx-user-subscribe_sms'
                        , name: 'subscribe_sms'
                        , fieldLabel: 'Подписка sms'
                        , xtype: 'modx-combo-yesno'
                        , anchor: '100%'
                        , maxLength: 255
                    },
                    {
                        id: 'modx-user-subscribe_email_trans'
                        , name: 'subscribe_email_trans'
                        , fieldLabel: 'Подписка email транзакционные'
                        , xtype: 'modx-combo-yesno'
                        , anchor: '100%'
                        , maxLength: 255
                    },
                    {
                        id: 'modx-user-subscribe_email_info'
                        , name: 'subscribe_email_info'
                        , fieldLabel: 'Подписка инфо письма'
                        , xtype: 'modx-combo-yesno'
                        , anchor: '100%'
                        , maxLength: 255
                    },

                    {
                        id: 'modx-user-city'
                        , name: 'city'
                        , fieldLabel: _('city')
                        , xtype: 'textfield'
                        , anchor: '100%'
                        , maxLength: 255
                    }
                    , {
                        id: 'modx-user-street'
                        , name: 'street'
                        , fieldLabel: 'Улица'
                        , xtype: 'textfield'
                        , anchor: '100%'
                        , maxLength: 255
                    }
                    , {
                        id: 'modx-user-house'
                        , name: 'house'
                        , fieldLabel: 'Дом'
                        , xtype: 'textfield'
                        , anchor: '100%'
                        , maxLength: 255
                    }
                    , {
                        id: 'modx-user-room'
                        , name: 'room'
                        , fieldLabel: 'Квартира'
                        , xtype: 'textfield'
                        , anchor: '100%'
                        , maxLength: 255
                    }
                    , {
                        id: 'modx-user-housing'
                        , name: 'housing'
                        , fieldLabel: 'Корпус'
                        , xtype: 'textfield'
                        , anchor: '100%'
                        , maxLength: 255
                    }
                    , {
                        id: 'modx-user-parade'
                        , name: 'parade'
                        , fieldLabel: 'Парадная'
                        , xtype: 'textfield'
                        , anchor: '100%'
                        , maxLength: 255
                    }
                    , {
                        id: 'modx-user-doorphone'
                        , name: 'doorphone'
                        , fieldLabel: 'Домофон'
                        , xtype: 'textfield'
                        , anchor: '100%'
                        , maxLength: 255
                    }
                    , {
                        id: 'modx-user-floor'
                        , name: 'floor'
                        , fieldLabel: 'Этаж'
                        , xtype: 'textfield'
                        , anchor: '100%'
                        , maxLength: 255
                    }


                    , {
                        id: 'modx-user-delivery',
                        xtype: 'modx-combo-delivery',
                        name: 'delivery',
                        fieldLabel: 'Доставка',
                        anchor: '100%'
                    }
                    , {
                        id: 'modx-user-payment',
                        xtype: 'modx-combo-payment',
                        name: 'payment',
                        fieldLabel: 'Способ оплаты',
                        anchor: '100%'
                    }
                    , {
                        id: 'modx-user-warehouse'
                        , name: 'warehouse'
                        , fieldLabel: 'Отделение'//_('city')
                        , xtype: 'textfield'
                        , anchor: '100%'
                        , maxLength: 255
                    }, {
                        id: 'modx-user-sales'
                        , name: 'sales'
                        , fieldLabel: 'Сумма покупок'//_('city')
                        //,xtype: 'displayfield'
                        , xtype: 'textfield'
                        , anchor: '100%'
                        , maxLength: 255
                    }]
            }, {
                columnWidth: .5
                , defaults: {
                    msgTarget: 'under'
                }
                , items: [{
                    id: 'modx-user-newpassword'
                    , name: 'newpassword'
                    , xtype: 'hidden'
                    , value: false
                }, {
                    id: 'modx-user-primary-group'
                    , name: 'primary_group'
                    , xtype: 'hidden'
                },
                    {
                        id: 'modx-user-comment'
                        , name: 'comment'
                        , fieldLabel: _('comment')
                        , xtype: 'textarea'
                        , height: '200px'
                        , anchor: '100%'
                        //,autoHeight: true
                        //,style: {height: '250px'}
                        , style: "width:100px;height:260px"
                        , grow: true
                    },
                    {
                        id: 'modx-user-active'
                        , name: 'active'
                        , hideLabel: true
                        , boxLabel: _('active')
                        , description: _('user_active_desc')
                        , xtype: 'xcheckbox'
                        , inputValue: 1
                    }, {
                        id: 'modx-user-sudo'
                        , name: 'sudo'
                        , hideLabel: true
                        , boxLabel: _('user_sudo')
                        , description: _('user_sudo_desc')
                        , xtype: 'xcheckbox'
                        , inputValue: 1
                        , value: 0
                    },
                    {
                        id: 'modx-user-blocked'
                        , name: 'blocked'
                        , hideLabel: true
                        , boxLabel: _('user_block')
                        , description: _('user_block_desc')
                        , xtype: 'xcheckbox'
                        , inputValue: 1
                    }, {
                        id: 'modx-user-blockeduntil'
                        , name: 'blockeduntil'
                        , fieldLabel: _('user_blockeduntil')
                        , description: _('user_blockeduntil_desc')
                        , xtype: 'xdatetime'
                        , width: 300
                        , timeWidth: 150
                        , dateWidth: 150
                        , allowBlank: true
                        , dateFormat: MODx.config.manager_date_format
                        , timeFormat: MODx.config.manager_time_format
                        , hiddenFormat: 'Y-m-d H:i:s'
                    }, {
                        id: 'modx-user-blockedafter'
                        , name: 'blockedafter'
                        , fieldLabel: _('user_blockedafter')
                        , description: _('user_blockedafter_desc')
                        , xtype: 'xdatetime'
                        , width: 300
                        , timeWidth: 150
                        , dateWidth: 150
                        , allowBlank: true
                        , dateFormat: MODx.config.manager_date_format
                        , timeFormat: MODx.config.manager_time_format
                        , hiddenFormat: 'Y-m-d H:i:s'
                    }, {
                        id: 'modx-user-logincount'
                        , name: 'logincount'
                        , fieldLabel: _('user_logincount')
                        , description: _('user_logincount_desc')
                        , xtype: 'statictextfield'
                    }, {
                        id: 'modx-user-lastlogin'
                        , name: 'lastlogin'
                        , fieldLabel: _('user_prevlogin')
                        , description: _('user_prevlogin_desc')
                        , xtype: 'statictextfield'
                    }, {
                        id: 'modx-user-failedlogincount'
                        , name: 'failedlogincount'
                        , fieldLabel: _('user_failedlogincount')
                        , description: _('user_failedlogincount_desc')
                        , xtype: 'textfield'
                    }, {
                        id: 'modx-user-class-key'
                        , name: 'class_key'
                        , fieldLabel: _('class_key')
                        , description: _('user_class_key_desc')
                        , xtype: 'textfield'
                        , anchor: '100%'
                        , value: 'modUser'
                    }, {
                        id: 'modx-user-fs-newpassword'
                        , title: _('password_new')
                        , xtype: 'fieldset'
                        , cls: 'x-fieldset-checkbox-toggle' // add a custom class for checkbox replacement
                        , checkboxToggle: true
                        , collapsed: (config.user ? true : false)
                        , forceLayout: true
                        , listeners: {
                            'expand': {
                                fn: function (p) {
                                    Ext.getCmp('modx-user-newpassword').setValue(true);
                                    this.markDirty();
                                }, scope: this
                            }
                            , 'collapse': {
                                fn: function (p) {
                                    Ext.getCmp('modx-user-newpassword').setValue(false);
                                    this.markDirty();
                                }, scope: this
                            }
                        }
                        , items: [{
                            xtype: 'radiogroup'
                            , fieldLabel: _('password_method')
                            , columns: 1
                            , items: [{
                                id: 'modx-user-passwordnotifymethod-e'
                                , name: 'passwordnotifymethod'
                                , boxLabel: _('password_method_email')
                                , xtype: 'radio'
                                , value: 'e'
                                , inputValue: 'e'
                            }, {
                                id: 'modx-user-passwordnotifymethod-s'
                                , name: 'passwordnotifymethod'
                                , boxLabel: _('password_method_screen')
                                , xtype: 'radio'
                                , value: 's'
                                , inputValue: 's'
                                , checked: true
                            }]
                        }, {
                            xtype: 'radiogroup'
                            , fieldLabel: _('password_gen_method')
                            , columns: 1
                            , items: [{
                                id: 'modx-user-password-genmethod-g'
                                , name: 'passwordgenmethod'
                                , boxLabel: _('password_gen_gen')
                                , xtype: 'radio'
                                , inputValue: 'g'
                                , value: 'g'
                                , checked: true
                            }, {
                                id: 'modx-user-password-genmethod-s'
                                , name: 'passwordgenmethod'
                                , boxLabel: _('password_gen_specify')
                                , xtype: 'radio'
                                , inputValue: 'spec'
                                , value: 'spec'
                            }]
                        }, {
                            id: 'modx-user-panel-newpassword'
                            , xtype: 'panel'
                            , layout: 'form'
                            , border: false
                            , autoHeight: true
                            , style: 'padding-top: 15px' // nested form, add padding-top as the label will not have it
                            , items: [{
                                id: 'modx-user-specifiedpassword'
                                , name: 'specifiedpassword'
                                , fieldLabel: _('change_password_new')
                                , xtype: 'textfield'
                                , inputType: 'password'
                                , anchor: '100%'
                            }, {
                                id: 'modx-user-confirmpassword'
                                , name: 'confirmpassword'
                                , fieldLabel: _('change_password_confirm')
                                , xtype: 'textfield'
                                , inputType: 'password'
                                , anchor: '100%'
                            }]
                        }]
                    }]
            }]
        }, {
            html: MODx.onUserFormRender
            , border: false
        }];
    }
});
Ext.reg('modx-panel-user', MODx.panel.User);

/**
 * Displays a gender combo
 *
 * @class MODx.combo.Gender
 * @extends Ext.form.ComboBox
 * @param {Object} config An object of configuration properties
 * @xtype modx-combo-gender
 */
MODx.combo.Gender = function (config) {
    config = config || {};
    Ext.applyIf(config, {
        store: new Ext.data.SimpleStore({
            fields: ['d', 'v']
            , data: [['', 0], [_('user_male'), 1], [_('user_female'), 2], [_('user_other'), 3]]
        })
        , displayField: 'd'
        , valueField: 'v'
        , mode: 'local'
        , triggerAction: 'all'
        , editable: false
        , selectOnFocus: false
    });
    MODx.combo.Gender.superclass.constructor.call(this, config);
};
Ext.extend(MODx.combo.Gender, Ext.form.ComboBox);
Ext.reg('modx-combo-gender', MODx.combo.Gender);


MODx.combo.Registr = function (config) {
    config = config || {};
    Ext.applyIf(config, {
        store: new Ext.data.SimpleStore({
            fields: ['v', 'd']
            , data: [[0, 'Не зарегистрирован'], [1, 'Зарегистрирован']]
            //,['не зарегистрирован',2],[_('user_other'),3]
        })
        , displayField: 'd'
        , valueField: 'v'
        , mode: 'local'
        , triggerAction: 'all'
        , editable: false
        , selectOnFocus: false
    });
    MODx.combo.Registr.superclass.constructor.call(this, config);
};
Ext.extend(MODx.combo.Registr, Ext.form.ComboBox);
Ext.reg('modx-combo-registr', MODx.combo.Registr);

MODx.combo.Yesno = function (config) {
    config = config || {};
    Ext.applyIf(config, {
        store: new Ext.data.SimpleStore({
            fields: ['v', 'd']
            , data: [[0, 'Нет'], [1, 'Да']]
        })
        , displayField: 'd'
        , valueField: 'v'
        , mode: 'local'
        , triggerAction: 'all'
        , editable: false
        , selectOnFocus: false
    });
    MODx.combo.Yesno.superclass.constructor.call(this, config);
};
Ext.extend(MODx.combo.Yesno, Ext.form.ComboBox);
Ext.reg('modx-combo-yesno', MODx.combo.Yesno);


MODx.combo.Subscribe = function (config) {
    config = config || {};
    Ext.applyIf(config, {
        store: new Ext.data.SimpleStore({
            fields: ['d', 'v']
            , data: [['Не подписан', 0], ['Подписан', 1]]
        })
        , displayField: 'd'
        , valueField: 'v'
        , mode: 'local'
        , triggerAction: 'all'
        , editable: false
        , selectOnFocus: false
    });
    MODx.combo.Subscribe.superclass.constructor.call(this, config);
};
Ext.extend(MODx.combo.Subscribe, Ext.form.ComboBox);
Ext.reg('modx-combo-subscribe', MODx.combo.Subscribe);


MODx.combo.Delivery = function (config) {
    config = config || {};
    Ext.applyIf(config, {
        store: new Ext.data.SimpleStore({
            fields: ['d', 'v']
            , data: [['', 0], ['Самовывоз', 1], ['Новая Почта', 2], ['Интайм', 3], ['Курьерская доставка', 4]]
        })
        /*url: '/assets/components/minishop2/action.php'//MODx.config.connector_url
         ,baseParams: {
         action: 'mgr/settings/delivery/getlist'
         ,combo: true
         //,addall: config.addall || 0
         //,order_id: config.order_id || 0
         }*/

        , displayField: 'd'
        , valueField: 'v'
        , mode: 'local'
        , triggerAction: 'all'
        , editable: false
        , selectOnFocus: false
    });
    MODx.combo.Delivery.superclass.constructor.call(this, config);
};
Ext.extend(MODx.combo.Delivery, Ext.form.ComboBox);
Ext.reg('modx-combo-delivery', MODx.combo.Delivery);


MODx.combo.Payment = function (config) {
    config = config || {};
    Ext.applyIf(config, {
        store: new Ext.data.SimpleStore({
            fields: ['d', 'v']
            ,
            data: [['', 0], ['Оплата курьеру', 1], ['Visa или Master Cart', 4], ['Наложенный платеж', 6], ['Наличными при получении', 7]]
        })

        , displayField: 'd'
        , valueField: 'v'
        , mode: 'local'
        , triggerAction: 'all'
        , editable: false
        , selectOnFocus: false
    });
    MODx.combo.Payment.superclass.constructor.call(this, config);
};
Ext.extend(MODx.combo.Payment, Ext.form.ComboBox);
Ext.reg('modx-combo-payment', MODx.combo.Payment);


MODx.grid.Products = function (config) {
    config = config || {};

    Ext.applyIf(config, {
        //id: this.ident
        //,
        url: '/assets/components/minishop2/connector.php'///assets/components/minishop2/action.php'//miniShop2.config.connector_url
        ,
        baseParams: {
            action: 'mgr/orders/product/getlistuser'
            , user_id: config.user
        }
        ,
        fields: ["createdon", "order_num", "status", "product_article", "name", "option_size", "price_old", "count", "cost", "id", "product_id"]
        ,
        pageSize: Math.round(MODx.config.default_per_page / 2)
        ,
        autoHeight: true
        ,
        paging: true
        ,
        remoteSort: true
        ,
        columns: this.getColumns()

    });
    MODx.grid.Products.superclass.constructor.call(this, config);

};
Ext.extend(MODx.grid.Products, MODx.grid.Grid, {

    getColumns: function () {
        var fields = {
            id: {hidden: true, sortable: true, width: 40}
            , product_id: {hidden: true, sortable: true, width: 40}
            , createdon: {header: 'Дата покупки', width: 50}
            , order_num: {header: 'Номер заказа', width: 50}
            , status: {header: 'Статус заказа', width: 50}
            , product_article: {header: 'Код товара', width: 50}
            , name: {header: 'Наименование', width: 100, renderer: miniShop2.utils.productLink}
            , option_size: {header: 'Опция', width: 100}
            , price_old: {header: 'Цена', width: 50}
            , count: {header: 'Количество', sortable: true, width: 50}
            , cost: {header: 'Стоимость', width: 50}

        };

        //var columns = [];
        var columns = [];
        /*columns.push(
         {header: 'Дата заказа', dataIndex: 'createdon', width: 75}
         );

         columns.push(
         {header: 'Номер заказа', dataIndex: 'order_num', width: 75}
         );*/
        //status,name,stock,option_size,available,product_article,option_artman,option_artpost,price_old,count,cost,action,skidka,discount
        for (var i = 0; i < miniShop2.config.order_product_fields.length; i++) {
            var field = miniShop2.config.order_product_fields[i];

            if (fields[field]) {
                Ext.applyIf(fields[field], {
                    header: _('ms2_' + field)
                    , dataIndex: field
                });
                columns.push(fields[field]);
            }
            else if (/^option_/.test(field)) {
                columns.push(
                    {header: _(field.replace(/^option_/, 'ms2_')), dataIndex: field, width: 50}
                );
            }
            else if (/^product_/.test(field)) {
                columns.push(
                    {header: _(field.replace(/^product_/, 'ms2_')), dataIndex: field, width: 75}
                );
            }
        }
        /**/
        return columns;

        //return fields;//columns;
    }

});
Ext.reg('modx-grid-orders-products', MODx.grid.Products);





MODx.grid.Bonuses = function (config) {
    config = config || {};

    Ext.applyIf(config, {
        url: '/assets/components/minishop2/connector.php'
        ,
        baseParams: {
            action: 'mgr/orders/getlistbonusesuser'//byuser'
            , user_id: config.user
        }
        ,
        fields: ["created", "manager","product_name","order_num", "amount","reason" ]
        ,
        pageSize: Math.round(MODx.config.default_per_page / 2)
        ,
        autoHeight: true
        ,
        paging: true
        ,
        remoteSort: true
        ,
        columns: this.getColumns()

    });
    MODx.grid.Bonuses.superclass.constructor.call(this, config);

};
Ext.extend(MODx.grid.Bonuses, MODx.grid.Grid, {

    getColumns: function () {
        var fields = {
            id: {hidden: true, sortable: true, width: 40}
            , created: {header: 'Дата', width: 50}
            , manager: {header: 'Менеджер',sortable: true, width: 40}
            , order_num: {header: 'Номер заказа', width: 50}
            , product_name: {header: 'Продукт (за отзыв)', width: 50}
            , amount: {header: 'Снятие/зачисление', width: 50}
            , reason: {header: 'Причина', width: 50}

        };

        //var columns = [];
        var columns = [];
        /*columns.push(
         {header: 'Дата заказа', dataIndex: 'createdon', width: 75}
         );

         columns.push(
         {header: 'Номер заказа', dataIndex: 'order_num', width: 75}
         );*/
        fileds_bonuses=['id','manager','created','product_name','order_num','amount','reason'];
        for (var i = 0; i < fileds_bonuses.length; i++) {
            var field = fileds_bonuses[i];

            if (fields[field]) {
                Ext.applyIf(fields[field], {
                    header: _('ms2_' + field)
                    , dataIndex: field
                });
                columns.push(fields[field]);
            }
            else if (/^option_/.test(field)) {
                columns.push(
                    {header: _(field.replace(/^option_/, 'ms2_')), dataIndex: field, width: 50}
                );
            }
            else if (/^product_/.test(field)) {
                columns.push(
                    {header: _(field.replace(/^product_/, 'ms2_')), dataIndex: field, width: 75}
                );
            }
        }
        /**/
        return columns;

        //return fields;//columns;
    }

});
Ext.reg('modx-grid-bonuses', MODx.grid.Bonuses);





MODx.grid.Pets = function (config) {
    config = config || {};

    Ext.applyIf(config, {
        url: '/assets/components/minishop2/connector.php'
        ,
        baseParams: {
            action: 'mgr/users/getpetsbyuser'
            , user_id: config.user
        }
        ,//
        fields: ["photo","name","birsday","type","breed", "genre","comment","created","updated"]//"bday", "bmonth","byear"
        ,
        pageSize: Math.round(MODx.config.default_per_page / 2)
        ,
        autoHeight: true
        ,
        paging: true
        ,
        remoteSort: true
        ,
        columns: this.getColumns()

    });
    MODx.grid.Pets.superclass.constructor.call(this, config);

};
Ext.extend(MODx.grid.Pets, MODx.grid.Grid, {

    getColumns: function () {
        var fields = {
            id: {hidden: true, sortable: true, width: 40}
            , photo: {header: 'Фото', width: 50, renderer: miniShop2.utils.photo}
            , name: {header: 'Имя', width: 50}
            , birsday: {header: 'День рождения', width: 50}
            , genre: {header: 'Пол', width: 50}
            , type: {header: 'Тип',sortable: true, width: 40}
            , breed: {header: 'Порода', width: 50}
            , comment: {header: 'Описание', width: 50}
            , created: {header: 'Дата создания', width: 50}
            , updated: {header: 'Дата обновления', width: 50}
        };


        var columns = [];

        fields_pets=["photo","name","birsday","type","breed", "genre","comment","created","updated"];
        //fields_pets=["name","birsday","type","breed", "genre","comment","created","updated"];

        for (var i = 0; i < fields_pets.length; i++) {
            var field = fields_pets[i];

            if (fields[field]) {
                Ext.applyIf(fields[field], {
                    header: _('ms2_' + field)
                    , dataIndex: field
                });
                columns.push(fields[field]);
            }
            else if (/^option_/.test(field)) {
                columns.push(
                    {header: _(field.replace(/^option_/, 'ms2_')), dataIndex: field, width: 50}
                );
            }
            else if (/^product_/.test(field)) {
                columns.push(
                    {header: _(field.replace(/^product_/, 'ms2_')), dataIndex: field, width: 75}
                );
            }
        }
        return columns;
    }

});
Ext.reg('modx-grid-pets', MODx.grid.Pets);

