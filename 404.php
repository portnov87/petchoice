<?php
$properties = $modx->resource->getOne('Template')->getProperties();



if(!empty($properties['tpl'])){
    $tpl = $properties['tpl'];
}
else{
    $tpl = '404.tpl';
}


if (($_SERVER['HTTP_HOST']=='m.petchoice.pp.ua')||($_SERVER['HTTP_HOST']=='m.petchoice.ua')||($_SERVER['HTTP_HOST']=='m.petchoice.test'))  $tpl = 'm/404.tpl';
if ($modx->resource->cacheable != '1') {
    $modx->smarty->caching = false;
}

if(!empty($properties['phptemplates.non-cached'])){
    $modx->smarty->compile_check = false;
    $modx->smarty->force_compile = true;
}





return preg_replace("/[ \n\t\r]+$/sm", "\r", $modx->smarty->fetch("tpl/{$tpl}"));