[
{"caption":"Info", "fields": [
{"field":"disable_option","caption":"","inputTVtype":"checkbox","inputOptionValues":"Отключить опцию==1", "default":"0"},
{"field":"artpost","caption":"Артикул поставщика","default":""},{"field":"artman","caption":"Артикул производителя","default":""},
{"field":"barcode","caption":"Штрихкод"},
{"field":"instock","caption":"В наличии","inputTVtype":"listbox","inputOptionValues":"да==1||нет==0"},
{"field":"weight","caption":"Вес"},{"field":"weight_prefix","caption":"Значение опции","inputTVtype":"listbox","inputOptionValues":"-==||мл==мл||гр==гр||кг==кг||см==см||м==м||л==л||шт==шт","default":""},
{"field":"price_supplier","caption":"Цена поставщика"},
{"field":"currency_supplier","caption":"Валюта поставщика","inputTVtype":"listbox","inputOptionValues":"грн.==1||usd==0"},
{"field":"markup","caption":"Наценка","default":"25","description":"(поле для ввода целого числа. По умолчанию 25%)"},
{"field":"price","caption":"Цена на сайте"},
{"field":"id_1c","caption":"Ид 1с","readonly":true,"default":""}
]},
{"caption":"Наличие", "fields": [
{"field":"availability_1","disabled":false,"caption":"Офис"},
{"field":"availability_2","disabled":false,"caption":"Магазин"},
{"field":"availability_3","disabled":false,"caption":"Магазин 2"},
{"field":"min_ostatok","disabled":false,"caption":"Минимальный остаток"},
{"field":"bystock","caption":"По наличию","inputTVtype":"listbox","inputOptionValues":"нет==0||да==1", "default":"0"},
{"field":"in_box","caption":"В коробке"}
]
},
{"caption":"Бонусы", "fields": [
{"field":"bonuses","caption":"Бонусы","default":"0"}]
},
{"caption":"Скидка N%", "fields": [
{"field":"action","caption":"Скидка"},
{"field":"action_hide1c","style":"width:30%;display:inline-block;", "caption":"Не выгружать в 1с","inputTVtype":"checkbox","inputOptionValues":"Не выгружать в 1с==1", "default":"0"},
{"field":"text_action","caption":"Текст в накладной"},
{"field":"action_hotline","caption":"Акция на хотлайн","inputTVtype":"checkbox","inputOptionValues":"Акция на хотлайн==1", "default":"0"},
{"field":"action_hide","caption":"Скрытая скидка","inputTVtype":"checkbox","inputOptionValues":"Скрытая скидка==1", "default":"0"},
{"field":"top_action","caption":"ТОП акция","inputTVtype":"checkbox","inputOptionValues":"топ Акция==1","style":"width:30%;display:inline-block;","default":"0"},
{"field":"no_discount_prom","caption":"Не выгружать на Prom","inputTVtype":"checkbox","inputOptionValues":"Не выгружать на Prom==1","style":"width:30%;display:inline-block;","default":"0"},
{"field":"by_weight","caption":"Развес","inputTVtype":"checkbox","inputOptionValues":"Развес==1", "default":"0"},
{"field":"spec","caption":"Акция","inputTVtype":"checkbox","inputOptionValues":"Акция==1","style":"width:30%;display:inline-block;","default":"0"},
{"field":"markdown","caption":"Уценка","inputTVtype":"checkbox","inputOptionValues":"Уценка==1", "default":"0"},
{"field":"gift","style":"width:30%;display:inline-block;","class":"gift box","caption":"Подарок","inputTVtype":"checkbox","inputOptionValues":"Подарок==1", "default":"0"},
{"field":"markdown_text","caption":"Текст под опцией","default":""},

{"field":"plusone","caption":"Включить акцию плюс 1","inputTVtype":"listbox","inputOptionValues":"нет==0||да==1", "default":"0"},
{"field":"podarok","caption":"Включить акцию 'Подарок'","inputTVtype":"listbox","inputOptionValues":"нет==0||да==1", "default":"0"},
{"field":"condition","caption":"1+1. Сколько шт надо купить чтобы получить в подарок"}
]
}]






[
{"header": "Отключена опция", "sortable":"true",  "width":"400", "dataIndex": "disable_option"},
{"header": "Уценка", "sortable":"true",  "width":"400", "dataIndex": "markdown"},
{"header": "Акция", "sortable":"true",  "width":"400", "dataIndex": "spec"},
{"header": "Артикул поставщика", "sortable":"true",  "width":"400", "dataIndex": "artpost"},
{"header": "Артикул производителя", "sortable": "true", "dataIndex": "artman"},
{"header": "Вес", "sortable": "true", "dataIndex": "weight"},
{"header": "Штрихкод", "sortable": "true", "dataIndex": "barcode"},
{"header": "В наличии", "sortable": "true", "dataIndex": "instock"},
{"header": "Значение опции", "sortable": "true", "dataIndex": "weight_prefix"},
{"header": "Цена поставщика", "sortable": "true", "dataIndex": "price_supplier"},
{"header": "Наценка", "sortable": "true", "dataIndex": "markup"},
{"header": "Цена на сайте", "sortable": "true", "dataIndex": "price"},
{"header": "Офис", "sortable": "true", "dataIndex": "availability_1"},
{"header": "Магазин", "sortable": "true", "dataIndex": "availability_2"},
{"header": "Магазин 2", "sortable": "true", "dataIndex": "availability_3"},
{"header": "Бонусы", "sortable": "true", "dataIndex": "bonuses"}
{"header": "Скидка", "sortable": "true", "dataIndex": "action"},
]