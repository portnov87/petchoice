 <?
 // функция для вывода времяни (можно не использовать)
  function microtime_float() {
      list($usec, $sec) = explode(" ", microtime());
      return ((float)$usec + (float)$sec);
  }
  // функция извлечения массива с файла
  function geFile($key) {
    $value= null;
    if (file_exists($key)) {
      if ($files = @fopen($key, 'rb')) {      
        if (flock($files, LOCK_SH)) {
          $value= @include $key;
          flock($files, LOCK_UN);
            if ($value === null) {
                fclose($files);
                @ unlink($key);
            }
        }
        @fclose($files);
      }
    }
    return $value;
  }
  // стартуем
  $time_start = microtime_float();
  // Получаем название ключа для кэша
  $file = md5($_SERVER['REQUEST_URI']);
  // формируем путь для нашего кэша
  $arrayUrl = array_filter(explode('/',$_SERVER['REQUEST_URI']),function($el){ return !empty($el);});
  $pathUri = implode('/',$arrayUrl);
  if(strlen($pathUri) > 0) $pathUri .= '/'; 
  // достаём с кэша конфиг файл 
  $system_cache_pach = MODX_CORE_PATH.'cache/system_settings/config.cache.php';
  $valueFile = geFile($system_cache_pach);  
  // берём с настроек модекса префикс кэша и время жизни кэша
  $cache_prefix = $valueFile['cache_prefix'];
  $cache_expires = intval($valueFile['cache_expires']);
  // проверяем. если есть файл в кэше
  switch ($valueFile['cache_handler']) {
    case 'cache.xPDOAPCCache':      
      $cache_key = "static/$pathUri$cache_prefix$file";
      // проверяем, есть ли файл в кэше
      if(apc_exists($cache_key)) {
        // получаем файл с кэша
        $value = apc_fetch($cache_key);
        // Если его время жизни ещё не прошло или cache_expires у нас нуль, то выводим
        if ((time() - $cache_expires) < $value['time'] || $cache_expires == 0) {
            // подсчитываем время
            $time_end = microtime_float();
            $time = $time_end - $time_start;
            // парсим наш тег [^t^] и заменяем на время
            echo str_replace('[^t^]', 'APCCache: '.round($time,6).' s', $value['output']); // Выводим содержимое файла  
            exit;
        } else {
          // удаляем файл кэша
          apc_delete($cache_key);
        }
      }               
    break;
    case 'xPDOFileCache':
      $cache_file = MODX_CORE_PATH."cache/static/$pathUri$file.cache.php"; 
      // проверяем, есть ли файл в кэше
      if (file_exists($cache_file)) {
        // получаем наш файл
        $value = geFile($cache_file);
        // Если его время жизни ещё не прошло или cache_expires у нас нуль, то выводим
        if ((time() - $cache_expires) < $value['time'] || $cache_expires == 0) {
            // подсчитываем время
            $time_end = microtime_float();
            $time = $time_end - $time_start;
            // парсим наш тег [^t^] и заменяем на время
            echo str_replace('[^t^]', 'FileCache: '.round($time,6).' s', $value['output']); // Выводим содержимое файла  
            exit;
        } else {
          // удаляем файл с кэша
          unlink($cache_file);
        }
      }
    break;
  }