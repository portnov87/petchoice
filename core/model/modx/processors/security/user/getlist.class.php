<?php

//ini_set('display_errors', 1);
//error_reporting(E_ALL | E_STRICT);

/**
 * Gets a list of users
 *
 * @param string $username (optional) Will filter the grid by searching for this
 * username
 * @param integer $start (optional) The record to start at. Defaults to 0.
 * @param integer $limit (optional) The number of records to limit to. Defaults
 * to 10.
 * @param string $sort (optional) The column to sort by. Defaults to name.
 * @param string $dir (optional) The direction of the sort. Defaults to ASC.
 *
 * @package modx
 * @subpackage processors.security.user
 */
class modUserGetListProcessor extends modObjectGetListProcessor
{
    public $classKey = 'modUser';
    public $languageTopics = array('user');
    public $permission = 'view_user';
    public $defaultSortField = 'username';

    public function initialize()
    {
        $initialized = parent::initialize();
        $this->setDefaultProperties(array(
            'usergroup' => false,
            'query' => '',
        ));
        if ($this->getProperty('sort') == 'username_link') $this->setProperty('sort', 'username');
        if ($this->getProperty('sort') == 'id') $this->setProperty('sort', 'modUser.id');
        return $initialized;
    }

    public function sql_valid($data)
    {
        $data = str_replace("\\", "\\\\", $data);
        $data = str_replace("'", "\'", $data);
        $data = str_replace('"', '\"', $data);
        //$data = str_replace("\x00", "\\x00", $data);
        //$data = str_replace("\x1a", "\\x1a", $data);
        $data = str_replace("\r", "\\r", $data);
        $data = str_replace("\n", "\\n", $data);
        return ($data);
    }

    public function prepareQueryBeforeCount(xPDOQuery $c)
    {
        $c->leftJoin('modUserProfile', 'Profile');

        if (!function_exists('mb_ucfirst') && extension_loaded('mbstring')) {
            /**
             * mb_ucfirst - преобразует первый символ в верхний регистр
             * @param string $str - строка
             * @param string $encoding - кодировка, по-умолчанию UTF-8
             * @return string
             */
            function mb_ucfirst($str, $encoding = 'UTF-8')
            {
                $str = mb_ereg_replace('^[\ ]+', '', $str);
                $str = mb_strtoupper(mb_substr($str, 0, 1, $encoding), $encoding) .
                    mb_substr($str, 1, mb_strlen($str), $encoding);
                return $str;
            }
        }

        $query = $this->getProperty('query', '');
        if (!empty($query)) {
            $ar = array('lastname' => $query);

            // $fio1 = json_encode($query);//
            $fio1 = json_encode($query);
            /*
                            //htmlspecialchars(addslashes(json_encode($query)));//mysqli_real_escape_string($c,json_encode($query));

                       // $fio1 = json_encode($fio1);
                        $fio1=str_replace('\"', '', $fio1);
                        $fio1=str_replace('"', '', $fio1);



                        //$fio1=$this->modx->quote($fio1);

                        $fio2 = json_encode(mb_ucfirst($query));//json_encode(mb_ucfirst($query));//
                       // echo $fio1.' '.$fio2."\r\n";
                        //$fio2 = mysqli_real_escape_string(json_encode(mb_ucfirst($query)));
                       // $fio2 = json_encode($fio2);
                        $fio2=str_replace('\"', '', $fio2);
                        $fio2=str_replace('"', '', $fio2);
                        //$fio2=$this->modx->quote($fio2);
                        $query = trim($query);
                        //'OR:mobilephone:LIKE' => '%'.$phoneclient.'%') );
                        //echo $query
\u041f\u043e\u0440\u0442\u043d\u043e\u0432
            */
            $match = '^' . preg_quote($fio1) . '$';
            $match2 = '^' . preg_quote($query) . '$';


            //echo '$fio1 '.$query.' '.$fio1.'<br/>';
            $c->where(array(
                // 'modUser.mobilephone:LIKE' => '%'.$query.'%',
                'modUser.username:LIKE' => '%' . $query . '%',
                'OR:Profile.lastname:LIKE' => '%' . $query . '%',
                'OR:Profile.fullname:LIKE' => '%' . $query . '%',
                'OR:Profile.email:LIKE' => '%' . $query . '%',
                //'OR:Profile.extended:REGEXP' => '%"lastname":"' . $fio1 . '","%',//
                //"%whatiamlookingfor%"%’
                //'OR:Profile.extended:LIKE' => '%' . $fio1 . '%',

                //"lastname":"'.$fio1.'"%',//"lastname":"([^"])'.$fio1.'([^"])"',//
                //'OR:Profile.extended:REGEXP' => '"lastname":"([^"])'.$fio1.'([^"])"',//
                //'OR:Profile.extended:LIKE' => '%'.$query.'%',//"lastname":"' . $fio2 . '%',
                'OR:Profile.extended:REGEXP' =>  $match,//'" . mysql_real_escape_string($match) . "'
                //'OR:Profile.extended:REGEXP' =>  $match2,


                'OR:productorder.name:LIKE' => '%' . $query . '%',
                //'OR:productorder.name:LIKE' => $query . '%',
                //'OR:productorder.name:LIKE' => '%' . $query,
                'OR:Profile.phone:LIKE' => '%' . $query . '%',
                'OR:Profile.mobilephone:LIKE' => '%' . $query . '%',
                'OR:order.num:=' => $query
            ));

            //REGEXP ‘“key_name”:"([^"])key_word([^"])"’;

            $c->leftJoin('msOrder', 'order', 'modUser.id =order.user_id');
            $c->leftJoin('msOrderProduct', 'productorder', 'order.id = productorder.order_id');
            //$c->groupBy('modUser.username');
        }


        $userGroup = $this->getProperty('usergroup', 0);
        if (!empty($userGroup)) {
            if ($userGroup === 'anonymous') {
                $c->join('modUserGroupMember', 'UserGroupMembers', 'LEFT OUTER JOIN');
                $c->where(array(
                    'UserGroupMembers.user_group' => NULL,
                ));
            } else {
                $c->distinct();
                $c->innerJoin('modUserGroupMember', 'UserGroupMembers');
                $c->where(array(
                    'UserGroupMembers.user_group' => $userGroup,
                ));
            }
        }
        //echo 'sql'.$c;//->toSql();

        return $c;
    }

    public function prepareQueryAfterCount(xPDOQuery $c)
    {
        $c->select($this->modx->getSelectColumns('modUser', 'modUser'));
        $c->select($this->modx->getSelectColumns('modUserProfile', 'Profile', '', array('fullname', 'email', 'blocked', 'extended', 'comment')));

        $query = $this->getProperty('query', '');
        if (!empty($query)) {
            $c->select($this->modx->getSelectColumns('msOrder', 'order', '', array('num', 'status')));
        }

        return $c;
    }

    /**
     * Prepare the row for iteration
     * @param xPDOObject $object
     * @return array
     */
    public function prepareRow(xPDOObject $object)
    {
        $objectArray = $object->toArray();
        $objectArray['blocked'] = $object->get('blocked') ? true : false;
        $objectArray['registr'] = $object->get('registr') ? true : false;

        $extended = $object->get('extended');
        $extended_array = $this->modx->fromJSON($extended);
        if (isset($extended_array['lastname']))
            $objectArray['lastname'] = $extended_array['lastname'];
        else $objectArray['lastname'] = '';

        $objectArray['cls'] = 'pupdate premove pcopy';
        unset($objectArray['password'], $objectArray['cachepwd'], $objectArray['salt']);
        return $objectArray;
    }


    public function getData()
    {
        $data = array();
        if ($this->modx->user->hasSessionContext('mgr') && $this->modx->user->isMember('Administrator')) {
            $limit = intval($this->getProperty('limit'));
            $start = intval($this->getProperty('start'));
        } else {
            $limit = 25;
            $start = 0;

        }


        /* query for chunks */
        $c = $this->modx->newQuery($this->classKey);
        $c = $this->prepareQueryBeforeCount($c);
        $data['total'] = $this->modx->getCount($this->classKey, $c);
        $c = $this->prepareQueryAfterCount($c);

        $sortClassKey = $this->getSortClassKey();

        $sortKey = $this->modx->getSelectColumns($sortClassKey, $this->getProperty('sortAlias', $sortClassKey), '', array($this->getProperty('sort')));
        if (empty($sortKey)) $sortKey = $this->getProperty('sort');
        $c->sortby($sortKey, $this->getProperty('dir'));



        $query = $this->getProperty('query', '');
        if (!empty($query)) {
            if ($limit > 0) {
                $limit=1000;
                $c->limit($limit, $start);
            }
        }else {
            if ($limit > 0) {
                $c->limit($limit, $start);
            }
        }


        $results = $this->modx->getCollection($this->classKey, $c);
        //echo $c->toSQL();

        /*echo '<pre>'.count($results);
        print_r($results);//$c->toSQL());
        echo '</pre>';
        */

        $data['results'] = $results;
        return $data;
    }


}

return 'modUserGetListProcessor';