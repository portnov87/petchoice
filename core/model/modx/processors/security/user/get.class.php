<?php
/**
 * @package modx
 * @subpackage processors.security.user
 */
/**
 * Get a user
 *
 * @param integer $id The ID of the user
 *
 * @var modX $modx
 * @var array $scriptProperties
 *
 * @package modx
 * @subpackage processors.security.user
 */
class modUserGetProcessor extends modObjectGetProcessor {
    public $classKey = 'modUser';
    public $languageTopics = array('user');
    public $permission = 'view_user';
    public $objectType = 'user';

    public function beforeOutput() {
        /*echo '<pre>';
        print_r( $this->object->getOne('Profile')->toArray());
        echo '</pre>';die();//$userArray = $this->user->toArray();
        */
        $this->getExtended();
        //$profile = $this->user->getOne('Profile');
        if ($this->getProperty('getGroups',false)) {
            $this->getGroups();
        }
        return parent::beforeOutput();
    }

    /**
     * Get all the groups for the user
     * @return array
     */
    public function getExtended() {
        $profile=$this->object->getOne('Profile')->toArray();
        $extended=$profile['extended'];

        $this->object->set('lastname',$extended['lastname']);
        $this->object->set('discount',$extended['discount']);
        $this->object->set('delivery',$extended['delivery']);
        $this->object->set('payment',$extended['payment']);

        $city='';

        //if( preg_match('/[a-zA-ZА-Яа-я\,\.]/ui', $str) ){ ... }

        //(is_int($extended['city_id']))
        if (isset($extended['city_id'])){//&&(!preg_match('/[a-zA-ZА-Яа-яЇІЄіїєёЁ\,\.]/ui', $extended['city_id']))){
            $qcity = $this->modx->getObject('msNpCities',$extended['city_id']);
            if ($qcity)
            {
                $city=$qcity->get('DescriptionRu');
                //if ($qcity->get('region')!='')	$city.=', '.$qcity->get('region');

            }
        }else $city=$extended['city_id'];



        $this->object->set('city',$city);

        //(is_int($extended['warehouse']))
        if (($extended['delivery']==2)||($extended['delivery']==3))
        {
            if (($extended['city_id']!='')&&(isset($extended['warehouse']))&&(isset($extended['warehouse'])&&(!preg_match('/[a-zA-ZА-Яа-яЇІЄіїєёЁ\,\.]/ui', $extended['warehouse'])))&&($extended['warehouse']!=''))
            {
                $idware=$extended['warehouse'];
                $nameware='';
                $delivery = $this->modx->getObject('msDelivery', $extended['delivery']);
                $warehouses = $delivery->getWarehouses($extended['city_id']);

                foreach ($warehouses as $warehouse) {
                    if ($warehouse['Number']==$idware)
                    {
                        $nameware=$warehouse['DescriptionRu'];
                    }
                }

                $extended['warehouse']=$nameware;
            }
        }//else $extended['warehouse']='';


        $this->object->set('warehouse',$extended['warehouse']);


        $this->object->set('street',$extended['street']);

        $this->object->set('floor',$extended['floor']);
        $this->object->set('parade',$extended['parade']);
        $this->object->set('doorphone',$extended['doorphone']);
        $this->object->set('housing',$extended['housing']);


        if (isset($extended['fathername']))
            $this->object->set('fathername',$extended['fathername']);






        $this->object->set('house',$extended['house']);
        $this->object->set('room',$extended['room']);


        $q = $this->modx->newQuery('msOrder', array('user_id' => $this->object->get('id')));
        $q->where(array('msOrder.status' => 2));
        $q->select(array('msOrder.id', 'msOrder.cart_cost'));
        $total=0;
        if ($q->prepare() && $q->stmt->execute()) {
            $orders = $q->stmt->fetchAll(PDO::FETCH_ASSOC);
            foreach($orders as $order) {
                $ids[] = $order['id'];
                $total += $order['cart_cost'];
            }
        }

        $this->object->set('sales',$total);//$extended['warehouse']);
        $adr[]=$extended['street'];
        if ($extended['house']!='' ) $adr[]='д.'.$extended['house'];
        if ($extended['room']!='') $adr[]='кв.'.$extended['room'];

        if ($extended['housing']!='') $adr[]='корпус '.$extended['housing'];
        if ($extended['doorphone']!='') $adr[]='домофон '.$extended['doorphone'];
        if ($extended['parade']!='') $adr[]='парадная '.$extended['parade'];
        if ($extended['floor']!='') $adr[]='этаж '.$extended['floor'];


        $this->object->set('adress',implode(',',$adr));

        return $data;
    }
    public function getGroups() {
        $c = $this->modx->newQuery('modUserGroupMember');
        $c->select($this->modx->getSelectColumns('modUserGroupMember','modUserGroupMember'));
        $c->select(array(
            'role_name' => 'UserGroupRole.name',
            'user_group_name' => 'UserGroup.name',
        ));
        $c->leftJoin('modUserGroupRole','UserGroupRole');
        $c->innerJoin('modUserGroup','UserGroup');
        $c->where(array(
            'member' => $this->object->get('id'),
        ));
        $c->sortby('modUserGroupMember.rank','ASC');
        $members = $this->modx->getCollection('modUserGroupMember',$c);

        $data = array();
        /** @var modUserGroupMember $member */
        foreach ($members as $member) {
            $roleName = $member->get('role_name');
            if ($member->get('role') == 0) { $roleName = $this->modx->lexicon('none'); }
            $data[] = array(
                $member->get('user_group'),
                $member->get('user_group_name'),
                $member->get('member'),
                $member->get('role'),
                empty($roleName) ? '' : $roleName,
                $this->object->get('primary_group') == $member->get('user_group') ? true : false,
                $member->get('rank'),
            );
        }
        $this->object->set('groups','(' . $this->modx->toJSON($data) . ')');
        return $data;
    }

    public function cleanup() {
        $userArray = $this->object->toArray();

        $profile = $this->object->getOne('Profile');
        if ($profile) {
            $userArray = array_merge($profile->toArray(),$userArray);
        }

        $userArray['dob'] = !empty($userArray['dob']) ? strftime('%m/%d/%Y',$userArray['dob']) : '';
        $userArray['blockeduntil'] = !empty($userArray['blockeduntil']) ? strftime('%Y-%m-%d %H:%M:%S',$userArray['blockeduntil']) : '';
        $userArray['blockedafter'] = !empty($userArray['blockedafter']) ? strftime('%Y-%m-%d %H:%M:%S',$userArray['blockedafter']) : '';
        $userArray['lastlogin'] = !empty($userArray['lastlogin']) ? strftime('%m/%d/%Y',$userArray['lastlogin']) : '';

        /*if ($userArray['subscribe']=='Подписан') $userArray['subscribe']=1;
        else  $userArray['subscribe']=0;*/

        if ($userArray['subscribe']=='Подписан') $userArray['subscribe']=1;
        elseif ($userArray['subscribe']=='Не подписан')  $userArray['subscribe']=0;
        /*[registr] => 1
    [registr_date] => -1-11-30 00:00:00
    [subscribe] => Подписан

        echo '<pre>$userArray';
        print_r($userArray);
        echo '</pre>';*/

        unset($userArray['password'],$userArray['cachepwd']);
        return $this->success('',$userArray);
    }
}
return 'modUserGetProcessor';
