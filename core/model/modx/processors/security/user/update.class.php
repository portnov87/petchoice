<?php
/*ini_set('display_errors', 1);
error_reporting(E_ALL | E_STRICT);*/
require_once(dirname(__FILE__) . '/_validation.php');

/**
 * Update a user.
 *
 * @param integer $id The ID of the user
 *
 * @package modx
 * @subpackage processors.security.user
 */
class modUserUpdateProcessor extends modObjectUpdateProcessor
{
    public $classKey = 'modUser';
    public $languageTopics = array('user');
    public $permission = 'save_user';
    public $objectType = 'user';
    public $beforeSaveEvent = 'OnBeforeUserFormSave';
    public $afterSaveEvent = 'OnUserFormSave';

    /** @var boolean $activeStatusChanged */
    public $activeStatusChanged = false;
    /** @var boolean $newActiveStatus */
    public $newActiveStatus = false;

    /** @var modUser $object */
    public $object;
    /** @var modUserProfile $profile */
    public $profile;
    /** @var modUserValidation $validator */
    public $validator;
    /** @var string $newPassword */
    public $newPassword = '';


    /**
     * Allow for Users to use derivative classes for their processors
     *
     * @static
     * @param modX $modx
     * @param $className
     * @param array $properties
     * @return modProcessor
     */
    public static function getInstance(modX &$modx, $className, $properties = array())
    {
        $classKey = !empty($properties['class_key']) ? $properties['class_key'] : 'modUser';
        $object = $modx->newObject($classKey);

        if (!in_array($classKey, array('modUser', ''))) {
            $className = $classKey . 'UpdateProcessor';
            if (!class_exists($className)) {
                $className = 'modUserUpdateProcessor';
            }
        }
        /** @var modProcessor $processor */
        $processor = new $className($modx, $properties);
        return $processor;
    }

    /**
     * {@inheritDoc}
     * @return boolean|string
     */
    public function initialize()
    {
        $this->setDefaultProperties(array(
            'class_key' => $this->classKey,
        ));
        return parent::initialize();
    }

    /**
     * {@inheritDoc}
     * @return boolean
     */
    public function beforeSet()
    {

        if (!isset($_REQUEST['pageId'])) {
            $bonuese_old = $this->object->get('bonuses');
            $bonuses = $this->getProperty('bonuses', 0);
            //if ($_REQUEST['pageId']==7) {
               /* echo '<pre>request';
                print_r($_REQUEST);
                echo '</pre>';
                echo 'before set old' . $bonuese_old . ' ' . $bonuses . '<br/>';
                die();*/


                $profile = $this->object->getOne('Profile');
                if ($this->object->get('id')) {

                    if ($bonuese_old != $bonuses) {
                        $rang = $bonuses - $bonuese_old;

                        $miniShop2 = $this->modx->getService('minishop2');
                        if ($rang < 0) {
                            $amount = $rang;
                        } else {
                            $amount = '+' . $rang;
                            //$this->
                            $bonuses_user = $rang;
                            $date_burn = date('Y-m-d H:i:s', strtotime('+ 45 day'));


                            //$bonuses = $bonuses + $bonuses_user;
                            $miniShop2->sendEmailBonuses($profile, $bonuses_user, $bonuses, $date_burn);

                        }
                        $user_id = $this->object->get('id');
                        $manager_id = false;
                        //if ($isAuthenticated = $this->modx->user->isAuthenticated())
                        $manager_id = $this->modx->user->id;// $this->modx->user->get('id');
                        //echo 'mamager ' . $manager_id . ' user ' . $user_id . ' amount' . $amount;

                        $miniShop2->saveLogBonuses(['manager_id' => $manager_id, 'user_id' => $user_id, 'amount' => $amount]);
                    }
                }
            //}
        }
        $this->setCheckbox('blocked');
        $this->setCheckbox('active');
        $this->setCheckbox('sudo');
        return parent::beforeSet();
    }

    /**
     * {@inheritDoc}
     * @return boolean
     */
    public function beforeSave()
    {
        $this->setProfile();
        $this->setRemoteData();

        $sudo = $this->getProperty('sudo', null);

        $registr = $this->getProperty('registr', null);


        $subscribe = $this->getProperty('subscribe', null);
        if (!isset($_REQUEST['pageId'])) {
            $bonuese_old = $this->object->get('bonuses');
            $bonuses = $this->getProperty('bonuses', 0);
            $this->object->set('bonuses', $bonuses);
        }



        if ($this->getProperty('subscribe_viber', 'Да')=='Да')
            $this->object->set('subscribe_viber', 1);
        else $this->object->set('subscribe_viber', 0);

        if ($this->getProperty('subscribe_sms', 'Да')=='Да')
            $this->object->set('subscribe_sms', 1);
        else $this->object->set('subscribe_sms', 0);

        if ($this->getProperty('subscribe_email_trans', 'Да')=='Да')
            $this->object->set('subscribe_email_trans', 1);
        else $this->object->set('subscribe_email_trans', 0);

        if ($this->getProperty('subscribe_email_info', 'Да')=='Да')
            $this->object->set('subscribe_email_info', 1);
        else $this->object->set('subscribe_email_info', 0);



        if ($registr == 'Не зарегистрирован') $this->object->set('registr', 0);
        elseif ($registr == 'Зарегистрирован') {
            $this->object->set('registr', 1);
            $this->object->set('registr_date', date('Y-m-d H:i:s'));


            $profil = $this->profile->toArray();
            $extended = $profil['extended'];// $this->modx->fromJSON($this->profile->get('extended'));
            $extendednew = $this->getProperty('extended', null);

            $extendednewar = $this->modx->fromJSON($extendednew);
            $discountnew = $extendednewar['discount'];

            $user_id = $this->object->get('id');
            if ($extended['discount'] == '') $user_discount = 5;//$extended['discount'];
            else $user_discount = $extended['discount'];
            //$discountnew
            //$extended['discount']=$user_discount;
            $q = $this->modx->newQuery('msUserDiscount', array('percent:>' => $user_discount));
            $q->sortby('percent', 'ASC');
            $q->select('`msUserDiscount`.`percent`,`msUserDiscount`.`total`');
            $q->limit(1);
            if ($q->prepare() && $q->stmt->execute()) {
                while ($row = $q->stmt->fetch(PDO::FETCH_ASSOC)) {
                    $next_discount = $row['percent'];
                    $next_discount_total += $row['total'];
                }
            }
            $q = $this->modx->newQuery('msOrder', array('user_id' => $user_id));
            $q->where(array('msOrder.status' => 2));
            $q->select(array('msOrder.id', 'msOrder.cart_cost'));
            $total = 0;
            if ($q->prepare() && $q->stmt->execute()) {
                $orders = $q->stmt->fetchAll(PDO::FETCH_ASSOC);
                foreach ($orders as $order) {
                    $ids[] = $order['id'];
                    $total += $order['cart_cost'];
                }
            }


            if ($discountnew != $extended['discount'])//&&($discountnew!=''))
            {
                //$extended['discount']=$extended['discount'];//$discountnew;
            } else {
                if (isset($next_discount_total)) {
                    if ($total > $next_discount_total) {
                        if (isset($next_discount)) {
                            if ($user_discount < $next_discount) {
                                $extended['discount'] = $next_discount;
                            }
                        }

                    }
                }
            }

            $extendedjson = $this->modx->toJSON($extended);
            $this->profile->set('extended', $extendedjson);
            $this->profile->save();

        }
        //die();
        if ($subscribe == 'Подписан') $this->object->set('subscribe', 1);//$values['subscribe']=1;
        elseif ($subscribe == 'Не подписан') $this->object->set('subscribe', 0);//$values['subscribe']=0;

        if ($sudo !== null) {
            $this->object->setSudo(!empty($sudo));
        }


        $this->validator = new modUserValidation($this, $this->object, $this->profile);
        $this->validator->validate();
        $canChangeStatus = $this->checkActiveChange();
        if ($canChangeStatus !== true) {
            $this->addFieldError('active', $canChangeStatus);
        }
        return parent::beforeSave();
    }

    /**
     * Check for an active/inactive status change
     * @return boolean|string
     */
    public function checkActiveChange()
    {
        $this->activeStatusChanged = $this->object->isDirty('active');
        $this->newActiveStatus = $this->object->get('active');
        if ($this->activeStatusChanged) {
            $event = $this->newActiveStatus == true ? 'OnBeforeUserActivate' : 'OnBeforeUserDeactivate';
            $OnBeforeUserActivate = $this->modx->invokeEvent($event, array(
                'id' => $this->object->get('id'),
                'user' => &$this->object,
                'mode' => modSystemEvent::MODE_UPD,
            ));
            $canChange = $this->processEventResponse($OnBeforeUserActivate);
            if (!empty($canChange)) {
                return $canChange;
            }
        }
        return true;
    }

    /**
     * Set the profile data for the user
     * @return modUserProfile
     */
    public function setProfile()
    {
        $this->profile = $this->object->getOne('Profile');
        $values = $this->getProperties();
        $extended = $values['extended'];

        $extendedar = $this->modx->fromJSON($extended);
        if (isset($values['street'])) $extendedar['street'] = $values['street'];

        if (isset($values['floor'])) $extendedar['floor'] = $values['floor'];
        if (isset($values['parade'])) $extendedar['parade'] = $values['parade'];
        if (isset($values['doorphone'])) $extendedar['doorphone'] = $values['doorphone'];
        if (isset($values['housing'])) $extendedar['housing'] = $values['housing'];

        if (isset($values['fathername'])) $extendedar['fathername'] = $values['fathername'];


        if (isset($values['house'])) $extendedar['house'] = $values['house'];
        if (isset($values['room'])) $extendedar['room'] = $values['room'];
        if (isset($values['lastname'])) $extendedar['lastname'] = $values['lastname'];
        //if (isset($values['warehouse'])) $extendedar['warehouse']=$values['warehouse'];
        if (isset($values['delivery'])) {


            $delivery = '';

            switch ($values['delivery']) {
                case 'Самовывоз':
                    $delivery = '1';
                    break;
                case 'Новая почта':
                    $delivery = '2';
                    break;
                case 'Интайм':
                    $delivery = '3';
                    break;
                case 'Курьерская доставка':
                    $delivery = 4;//'Курьерская доставка';
                    break;
                case 'Курьерская доставка Новой почты':
                    $delivery = '5';
                    break;

            }
            if ($delivery != '') $extendedar['delivery'] = $delivery;
            else $extendedar['delivery'] = $values['delivery'];
        }
        if (isset($values['payment'])) {
            $payment = '';
            switch ($values['payment']) {
                case 'Оплата курьеру':
                    $payment = '1';
                    break;
                case 'Приват 24':
                    $payment = '3';
                    break;
                case 'Visa или MasterCard':
                    $payment = '4';
                    break;
                case 'Наложенный платеж':
                    $payment = '6';
                    break;
                case 'Наличными при получении':
                    $payment = '7';
                    break;
            }
            if ($payment != '') $extendedar['payment'] = $payment;
            else $extendedar['payment'] = $values['payment'];
        }
        if (isset($values['discount'])) $extendedar['discount'] = $values['discount'];


        if ($values['registr'] == 'Не зарегистрирован') $values['registr'] = 0;
        elseif ($values['registr'] == 'Зарегистрирован') $values['registr'] = 1;


        if ($values['subscribe'] == 'Подписан') $values['subscribe'] = 1;
        elseif ($values['subscribe'] == 'Не подписан') $values['subscribe'] = 0;


        if (empty($this->profile)) {
            $this->profile = $this->modx->newObject('modUserProfile');


            $this->profile->set('internalKey', $this->object->get('id'));
            $this->profile->save();
            $this->object->addOne($this->profile, 'Profile');
        }


        $extendedjson = $this->modx->toJSON($extendedar);
        $values['extended'] = $extendedjson;
        /*echo '<pre>';
        print_r($values);
        echo '</pre>';*/
        //die();
        $this->profile->fromArray($values);//$this->getProperties());

        return $this->profile;
    }

    /**
     * Set the remote data for the user
     * @return mixed
     */
    public function setRemoteData()
    {
        $remoteData = $this->getProperty('remoteData', null);
        if ($remoteData !== null) {
            $remoteData = is_array($remoteData) ? $remoteData : $this->modx->fromJSON($remoteData);
            $this->object->set('remote_data', $remoteData);
        }
        return $remoteData;
    }

    /**
     * Set user groups for the user
     * @return array
     */
    public function setUserGroups()
    {
        $memberships = array();
        $groups = $this->getProperty('groups', null);
        if ($groups !== null) {
            $primaryGroupId = 0;
            /* remove prior user group links */
            $oldMemberships = $this->object->getMany('UserGroupMembers');
            /** @var modUserGroupMember $membership */
            foreach ($oldMemberships as $membership) {
                $membership->remove();
            }

            /* create user group links */
            $groupsAdded = array();
            $groups = is_array($groups) ? $groups : $this->modx->fromJSON($groups);
            $idx = 0;
            foreach ($groups as $group) {
                if (in_array($group['usergroup'], $groupsAdded)) continue;
                $membership = $this->modx->newObject('modUserGroupMember');
                $membership->set('user_group', $group['usergroup']);
                $membership->set('role', $group['role']);
                $membership->set('member', $this->object->get('id'));
                $membership->set('rank', isset($group['rank']) ? $group['rank'] : $idx);
                if (empty($group['rank'])) {
                    $primaryGroupId = $group['usergroup'];
                }
                $memberships[] = $membership;
                $groupsAdded[] = $group['usergroup'];
                $idx++;
            }
            $this->object->addMany($memberships, 'UserGroupMembers');
            $this->object->set('primary_group', $primaryGroupId);
            $this->object->save();
        }
        return $memberships;
    }

    /**
     * {@inheritDoc}
     * @return boolean
     */
    public function afterSave()
    {
        $this->setUserGroups();
        $this->sendNotificationEmail();
        if ($this->activeStatusChanged) {
            $this->fireAfterActiveStatusChange();
        }
        $this->updateEsputnik();
        return parent::afterSave();
    }


    public function updateEsputnik()
    {
        $esputnikPath = MODX_CORE_PATH.'components/minishop2/model/minishop2/esputnik.class.php';

        require_once $esputnikPath;


        $userData=[];
        $profile=$this->object->getOne('Profile');
        $userData['user']=$this->object->toArray();
        $userData['profile']=$profile->toArray();//$this->modx->toArray($profile);

        $modelEsputnik = new Esputnik($this->modx);
        $email=$profile->get('email');
        $modelEsputnik->updateContact($email,$userData);//$email,$data);
    }
    /**
     * Fire the active status change event
     */
    public function fireAfterActiveStatusChange()
    {
        $event = $this->newActiveStatus == true ? 'OnUserActivate' : 'OnUserDeactivate';
        $this->modx->invokeEvent($event, array(
            'id' => $this->object->get('id'),
            'user' => &$this->object,
            'mode' => modSystemEvent::MODE_UPD,
        ));
    }

    /**
     * Send notification email for changed password
     */
    public function sendNotificationEmail()
    {
        if ($this->getProperty('passwordnotifymethod') == 'e') {
            $message = $this->modx->getOption('signupemail_message');
            $placeholders = array(
                'uid' => $this->object->get('username'),
                'pwd' => $this->newPassword,
                'ufn' => $this->profile->get('fullname'),
                'sname' => $this->modx->getOption('site_name'),
                'saddr' => $this->modx->getOption('emailsender'),
                'semail' => $this->modx->getOption('emailsender'),
                'surl' => $this->modx->getOption('url_scheme') . $this->modx->getOption('http_host') . $this->modx->getOption('manager_url'),
            );
            foreach ($placeholders as $k => $v) {
                $message = str_replace('[[+' . $k . ']]', $v, $message);
            }
            $this->object->sendEmail($message);
        }
    }

    /**
     * {@inheritDoc}
     * @return array|string
     */
    public function cleanup()
    {
        $passwordNotifyMethod = $this->getProperty('passwordnotifymethod');

        if (!empty($passwordNotifyMethod) && !empty($this->newPassword) && $passwordNotifyMethod == 's') {
            return $this->success($this->modx->lexicon('user_updated_password_message', array(
                'password' => $this->newPassword,
            )), $this->object);
        } else {
            return $this->success('', $this->object);
        }
    }
}

return 'modUserUpdateProcessor';
