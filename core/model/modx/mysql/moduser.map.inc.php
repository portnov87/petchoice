<?php
/**
 * @package modx
 * @subpackage mysql
 */
$xpdo_meta_map['modUser'] = array(
    'package' => 'modx',
    'version' => '1.1',
    'table' => 'users',
    'extends' => 'modPrincipal',
    'fields' =>
        array(
            'username' => '',
            'password' => '',
            'cachepwd' => '',
            'class_key' => 'modUser',
            'active' => 1,
            'remote_key' => NULL,
            'remote_data' => NULL,
            'hash_class' => 'hashing.modPBKDF2',
            'salt' => '',
            'primary_group' => 0,
            'session_stale' => NULL,
            'sudo' => 0,
            'registr' => 1,
            'registr_date' => NULL,
            'subscribe' => '',
            'created' => NULL,
            'submitofferup' => 0,
            'mailnoregistrnobuy' => 0,
            'mailnoregistrnobuy30' => 0,
            'mailnoregistrnobuy7' => 0,
            'mailregistrnobuy' => 0,
            'mailmore45' => 0,
            'mailmore30' => 0,
            'mailmore60' => 0,
            'mailrequestreview' => 0,
            'mailafterdaysorder' => 0,
            'daterequestreview' => NULL,
            'last_order' => NULL,
            'total_orders' => 0,
            'last_mailmore60' => NULL,
            'lastnoregistrnobuy' => NULL,
            'last_mailmore45' => NULL,
            'last_mailmore30' => NULL,
            'lastafterdaysorder' => NULL,
            'last_offerup' => NULL,
            'count_orders_close' => 0,
            'purchaseReminder' => 0,
            'purchasedate' => NULL,
            'summa_close_orders' => 0,
            'summa_all_orders' => 0,
            'bonuses' => 0,
            'date_clear_bonuses' => NULL,
            'mailmore180' => 0,
            'mailmore120' => 0,
            'mailmore90' => 0,
            'mail_birsday' => 0,
            'last_birsday' => NULL,
            'last_mailmore180' => NULL,
            'last_mailmore90' => NULL,
            'last_mailmore120' => NULL,
            'last_mail_birsday_pet' => NULL,
            'first_mail_birsday_pet' => 0,
            'count_mail_birsday_pets' => 0,
            'mailsubmitviews' => 0,
            'last_mailviews' => NULL,
            'last_submitafter_first_order'=>NULL,
            'submitafter_first_order'=>0,

            'subscribe_viber'=>1,
            'viber_status'=>1,
            'subscribe_sms'=>1,
            'subscribe_email_trans'=>1,
            'subscribe_email_info'=>1,
            'lastresetpassword'=>0
        ),
    'fieldMeta' =>
        array(

            'submitafter_first_order' =>
                array(
                    'dbtype' => 'int',
                    'precision' => '10',
                    'phptype' => 'integer',
                    'null' => false,
                    'index' => 'unique',
                ),
            'mailsubmitviews' =>
                array(
                    'dbtype' => 'int',
                    'precision' => '10',
                    'phptype' => 'integer',
                    'null' => false,
                    'index' => 'unique',
                ),
            'mailmore180' =>
                array(
                    'dbtype' => 'int',
                    'precision' => '10',
                    'phptype' => 'integer',
                    'null' => false,
                    'index' => 'unique',
                ),
            'mailmore120' =>
                array(
                    'dbtype' => 'int',
                    'precision' => '10',
                    'phptype' => 'integer',
                    'null' => false,
                    'index' => 'unique',
                ),
            'mailmore90' =>
                array(
                    'dbtype' => 'int',
                    'precision' => '10',
                    'phptype' => 'integer',
                    'null' => false,
                    'index' => 'unique',
                ),
            'mail_birsday' =>
                array(
                    'dbtype' => 'int',
                    'precision' => '10',
                    'phptype' => 'integer',
                    'null' => false,
                    'index' => 'unique',
                ),

            'last_mailviews' => array(
                'dbtype' => 'datetime',
                'phptype' => 'datetime',
                'null' => true,
                'default' => '0000-00-00 00:00:00',
            ),
            'lastafterdaysorder' => array(
                'dbtype' => 'datetime',
                'phptype' => 'datetime',
                'null' => true,
                'default' => '0000-00-00 00:00:00',
            ),

            'last_birsday' => array(
                'dbtype' => 'datetime',
                'phptype' => 'datetime',
                'null' => true,
                'default' => '0000-00-00 00:00:00',
            ),
            'last_submitafter_first_order' => array(
                'dbtype' => 'datetime',
                'phptype' => 'datetime',
                'null' => true,
                'default' => '0000-00-00 00:00:00',
            ),

            'lastnoregistrnobuy' => array(
                'dbtype' => 'datetime',
                'phptype' => 'datetime',
                'null' => true,
                'default' => '0000-00-00 00:00:00',
            ),

            'last_mailmore180' => array(
                'dbtype' => 'datetime',
                'phptype' => 'datetime',
                'null' => true,
                'default' => '0000-00-00 00:00:00',
            ),
            'last_mailmore90' => array(
                'dbtype' => 'datetime',
                'phptype' => 'datetime',
                'null' => true,
                'default' => '0000-00-00 00:00:00',
            ),
            'last_mailmore120' => array(
                'dbtype' => 'datetime',
                'phptype' => 'datetime',
                'null' => true,
                'default' => '0000-00-00 00:00:00',
            ),
            'last_mail_birsday_pet' => array(
                'dbtype' => 'datetime',
                'phptype' => 'datetime',
                'null' => true,
                'default' => '0000-00-00 00:00:00',
            ),
            'first_mail_birsday_pet' =>
                array(
                    'dbtype' => 'int',
                    'precision' => '10',
                    'phptype' => 'integer',
                    'null' => false,
                    'index' => 'unique',
                ),
            'count_mail_birsday_pets' =>
                array(
                    'dbtype' => 'int',
                    'precision' => '10',
                    'phptype' => 'integer',
                    'null' => false,
                    'index' => 'unique',
                ),

            'username' =>
                array(
                    'dbtype' => 'varchar',
                    'precision' => '100',
                    'phptype' => 'string',
                    'null' => false,
                    'default' => '',
                    'index' => 'unique',
                ),
            'password' =>
                array(
                    'dbtype' => 'varchar',
                    'precision' => '100',
                    'phptype' => 'string',
                    'null' => false,
                    'default' => '',
                ),
            'cachepwd' =>
                array(
                    'dbtype' => 'varchar',
                    'precision' => '100',
                    'phptype' => 'string',
                    'null' => false,
                    'default' => '',
                ),
            'class_key' =>
                array(
                    'dbtype' => 'varchar',
                    'precision' => '100',
                    'phptype' => 'string',
                    'null' => false,
                    'default' => 'modUser',
                    'index' => 'index',
                ),
            'active' =>
                array(
                    'dbtype' => 'tinyint',
                    'precision' => '1',
                    'phptype' => 'boolean',
                    'attributes' => 'unsigned',
                    'null' => false,
                    'default' => 1,
                ),
            'remote_key' =>
                array(
                    'dbtype' => 'varchar',
                    'precision' => '255',
                    'phptype' => 'string',
                    'null' => true,
                    'index' => 'index',
                ),
            'remote_data' =>
                array(
                    'dbtype' => 'text',
                    'phptype' => 'json',
                    'null' => true,
                ),
            'hash_class' =>
                array(
                    'dbtype' => 'varchar',
                    'precision' => '100',
                    'phptype' => 'string',
                    'null' => false,
                    'default' => 'hashing.modPBKDF2',
                ),
            'salt' =>
                array(
                    'dbtype' => 'varchar',
                    'precision' => '100',
                    'phptype' => 'string',
                    'null' => false,
                    'default' => '',
                ),
            'primary_group' =>
                array(
                    'dbtype' => 'int',
                    'precision' => '10',
                    'phptype' => 'integer',
                    'attributes' => 'unsigned',
                    'null' => false,
                    'default' => 0,
                    'index' => 'index',
                ),
            'session_stale' =>
                array(
                    'dbtype' => 'text',
                    'phptype' => 'array',
                    'null' => true,
                ),
            'sudo' =>
                array(
                    'dbtype' => 'tinyint',
                    'precision' => '1',
                    'phptype' => 'boolean',
                    'attributes' => 'unsigned',
                    'null' => false,
                    'default' => 0,
                ),
            'registr' =>
                array(
                    'dbtype' => 'tinyint',
                    'precision' => '1',
                    'phptype' => 'boolean',
                    'attributes' => 'unsigned',
                    'null' => false,
                    'default' => 0,
                ),
            'subscribe' =>
                array(
                    'dbtype' => 'varchar',
                    'precision' => '255',
                    'phptype' => 'string',
                    'null' => false,
                    'default' => '',
                ),
            'submitofferup' =>
                array(
                    'dbtype' => 'tinyint',
                    'precision' => '1',
                    'phptype' => 'boolean',
                    'null' => false,
                    'default' => 0,
                ),
            'mailnoregistrnobuy7' =>
                array(
                    'dbtype' => 'tinyint',
                    'precision' => '1',
                    'phptype' => 'boolean',
                    'null' => false,
                    'default' => 0,
                ),
            'mailnoregistrnobuy30' =>
                array(
                    'dbtype' => 'tinyint',
                    'precision' => '1',
                    'phptype' => 'boolean',
                    'null' => false,
                    'default' => 0,
                ),
            'mailnoregistrnobuy' =>
                array(
                    'dbtype' => 'tinyint',
                    'precision' => '1',
                    'phptype' => 'boolean',
                    'null' => false,
                    'default' => 0,
                ),
            'mailregistrnobuy' =>
                array(
                    'dbtype' => 'tinyint',
                    'precision' => '1',
                    'phptype' => 'boolean',
                    'null' => false,
                    'default' => 0,
                ),
            'mailmore60' =>
                array(
                    'dbtype' => 'tinyint',
                    'precision' => '1',
                    'phptype' => 'boolean',
                    'null' => false,
                    'default' => 0,
                ),
            'mailmore45' =>
                array(
                    'dbtype' => 'tinyint',
                    'precision' => '1',
                    'phptype' => 'boolean',
                    'null' => false,
                    'default' => 0,
                ),
            'mailmore30' =>
                array(
                    'dbtype' => 'tinyint',
                    'precision' => '1',
                    'phptype' => 'boolean',
                    'null' => false,
                    'default' => 0,
                ),

            'mailrequestreview' =>
                array(
                    'dbtype' => 'tinyint',
                    'precision' => '1',
                    'phptype' => 'boolean',
                    'null' => false,
                    'default' => 0,
                ),
            'mailafterdaysorder' =>
                array(
                    'dbtype' => 'tinyint',
                    'precision' => '1',
                    'phptype' => 'boolean',
                    'null' => false,
                    'default' => 0,
                ),

            'purchaseReminder' => array(
                'dbtype' => 'tinyint',
                'precision' => '1',
                'phptype' => 'boolean',
                'null' => false,
                'default' => 0,
            ),
            'purchasedate' => array(
                'dbtype' => 'datetime',
                'phptype' => 'datetime',
                'null' => true,
                'default' => '0000-00-00 00:00:00',
            ),
            'last_order' =>
                array(
                    'dbtype' => 'datetime',
                    'phptype' => 'datetime',
                    'null' => true,
                    'default' => '0000-00-00 00:00:00',
                ),
            'last_mailmore60' =>
                array(
                    'dbtype' => 'datetime',
                    'phptype' => 'datetime',
                    'null' => true,
                    'default' => '0000-00-00 00:00:00',
                ),
            'last_mailmore30' => array(
                'dbtype' => 'datetime',
                'phptype' => 'datetime',
                'null' => true,
                'default' => '0000-00-00 00:00:00',
            ),
            'last_mailmore45' => array(
                'dbtype' => 'datetime',
                'phptype' => 'datetime',
                'null' => true,
                'default' => '0000-00-00 00:00:00',
            ),
            'last_offerup' => array(
                'dbtype' => 'datetime',
                'phptype' => 'datetime',
                'null' => true,
                'default' => '0000-00-00 00:00:00',
            ),

            'total_orders' => array(
                'dbtype' => 'decimal',
                'precision' => '12,2',
                'phptype' => 'float',
                'null' => true,
                'default' => 0,
            ),

            'summa_all_orders' => array(
                'dbtype' => 'decimal',
                'precision' => '12,2',
                'phptype' => 'float',
                'null' => true,
                'default' => 0,
            ),
            'summa_close_orders' => array(
                'dbtype' => 'decimal',
                'precision' => '12,2',
                'phptype' => 'float',
                'null' => true,
                'default' => 0,
            ),
            'bonuses' => array(
                'dbtype' => 'int',
                'precision' => '10',
                'phptype' => 'integer',
                'null' => true,
                'default' => 0,
            ),
            'count_orders_close' => array(
                'dbtype' => 'int',
                'precision' => '10',
                'phptype' => 'integer',
                'null' => true,
                'default' => 0,
            ),
            'daterequestreview' =>
                array(
                    'dbtype' => 'datetime',
                    'phptype' => 'datetime',
                    'null' => true,
                    'default' => '0000-00-00 00:00:00',
                ),
            'registr_date' =>
                array(
                    'dbtype' => 'datetime',
                    'phptype' => 'datetime',
                    'null' => true,
                    'default' => '0000-00-00 00:00:00',
                ),
            'created' =>
                array(
                    'dbtype' => 'datetime',
                    'phptype' => 'datetime',
                    'null' => true,
                    'default' => date('Y-m-d H:i:s'),//'0000-00-00 00:00:00',
                ),
            'date_clear_bonuses' =>
                array(
                    'dbtype' => 'datetime',
                    'phptype' => 'datetime',
                    'null' => true,
                    'default' => date('Y-m-d H:i:s'),
                ),

            'subscribe_viber' => array(
                'dbtype' => 'int',
                'precision' => '10',
                'phptype' => 'integer',
                'null' => true,
                'default' => 1,
            ),
            'viber_status' => array(
                'dbtype' => 'int',
                'precision' => '10',
                'phptype' => 'integer',
                'null' => true,
                'default' => 1,
            ),
            'subscribe_sms' => array(
                'dbtype' => 'int',
                'precision' => '10',
                'phptype' => 'integer',
                'null' => true,
                'default' => 1,
            ),
            'subscribe_email_trans' => array(
                'dbtype' => 'int',
                'precision' => '10',
                'phptype' => 'integer',
                'null' => true,
                'default' => 1,
            ),
            'subscribe_email_info' => array(
                'dbtype' => 'int',
                'precision' => '10',
                'phptype' => 'integer',
                'null' => true,
                'default' => 1,
            ),

            'lastresetpassword' => array(
                'dbtype' => 'int',
                'precision' => '10',
                'phptype' => 'integer',
                'null' => true,
                'default' => 0,
            ),
        ),
    'indexes' =>
        array(
            'username' =>
                array(
                    'alias' => 'username',
                    'primary' => false,
                    'unique' => true,
                    'type' => 'BTREE',
                    'columns' =>
                        array(
                            'username' =>
                                array(
                                    'length' => '',
                                    'collation' => 'A',
                                    'null' => false,
                                ),
                        ),
                ),
            'class_key' =>
                array(
                    'alias' => 'class_key',
                    'primary' => false,
                    'unique' => false,
                    'type' => 'BTREE',
                    'columns' =>
                        array(
                            'class_key' =>
                                array(
                                    'length' => '',
                                    'collation' => 'A',
                                    'null' => false,
                                ),
                        ),
                ),
            'remote_key' =>
                array(
                    'alias' => 'remote_key',
                    'primary' => false,
                    'unique' => false,
                    'type' => 'BTREE',
                    'columns' =>
                        array(
                            'remote_key' =>
                                array(
                                    'length' => '',
                                    'collation' => 'A',
                                    'null' => false,
                                ),
                        ),
                ),
            'primary_group' =>
                array(
                    'alias' => 'primary_group',
                    'primary' => false,
                    'unique' => false,
                    'type' => 'BTREE',
                    'columns' =>
                        array(
                            'primary_group' =>
                                array(
                                    'length' => '',
                                    'collation' => 'A',
                                    'null' => false,
                                ),
                        ),
                ),
        ),
    'composites' =>
        array(
            'Profile' =>
                array(
                    'class' => 'modUserProfile',
                    'local' => 'id',
                    'foreign' => 'internalKey',
                    'cardinality' => 'one',
                    'owner' => 'local',
                ),
            'UserSettings' =>
                array(
                    'class' => 'modUserSetting',
                    'local' => 'id',
                    'foreign' => 'user',
                    'cardinality' => 'many',
                    'owner' => 'local',
                ),
            'UserGroupMembers' =>
                array(
                    'class' => 'modUserGroupMember',
                    'local' => 'id',
                    'foreign' => 'member',
                    'cardinality' => 'many',
                    'owner' => 'local',
                ),
        ),
    'aggregates' =>
        array(
            'CreatedResources' =>
                array(
                    'class' => 'modResource',
                    'local' => 'id',
                    'foreign' => 'createdby',
                    'cardinality' => 'many',
                    'owner' => 'local',
                ),
            'EditedResources' =>
                array(
                    'class' => 'modResource',
                    'local' => 'id',
                    'foreign' => 'editedby',
                    'cardinality' => 'many',
                    'owner' => 'local',
                ),
            'DeletedResources' =>
                array(
                    'class' => 'modResource',
                    'local' => 'id',
                    'foreign' => 'deletedby',
                    'cardinality' => 'many',
                    'owner' => 'local',
                ),
            'PublishedResources' =>
                array(
                    'class' => 'modResource',
                    'local' => 'id',
                    'foreign' => 'publishedby',
                    'cardinality' => 'many',
                    'owner' => 'local',
                ),
            'SentMessages' =>
                array(
                    'class' => 'modUserMessage',
                    'local' => 'id',
                    'foreign' => 'sender',
                    'cardinality' => 'many',
                    'owner' => 'local',
                ),
            'ReceivedMessages' =>
                array(
                    'class' => 'modUserMessage',
                    'local' => 'id',
                    'foreign' => 'recipient',
                    'cardinality' => 'many',
                    'owner' => 'local',
                ),
            'PrimaryGroup' =>
                array(
                    'class' => 'modUserGroup',
                    'local' => 'primary_group',
                    'foreign' => 'id',
                    'cardinality' => 'one',
                    'owner' => 'foreign',
                ),
        ),
);
