<?php
/**
 * modExtra
 *
 * Copyright 2010 by Shaun McCormick <shaun+modextra@modx.com>
 *
 * modExtra is free software; you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 2 of the License, or (at your option) any later
 * version.
 *
 * modExtra is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * modExtra; if not, write to the Free Software Foundation, Inc., 59 Temple
 * Place, Suite 330, Boston, MA 02111-1307 USA
 *
 * @package modextra
 */
/**
 * Get a list of Items
 *
 * @package modextra
 * @subpackage processors
 */
 
 //require_once '../config.core.php';

 
 //error_reporting(E_ALL | E_STRICT);
//ini_set('display_errors', 1);
 
 //require_once dirname(dirname(dirname(dirname(__FILE__)))) . '/config/config.inc.php';

//require_once MODX_CORE_PATH.'model/modx/modx.class.php';
//$modx = new modX();
//$modx->initialize('mgr');


		function rus2translit($string) {
			$converter = array(
				'а' => 'a',   'б' => 'b',   'в' => 'v',
				'г' => 'g',   'д' => 'd',   'е' => 'e',
				'ё' => 'e',   'ж' => 'zh',  'з' => 'z',
				'и' => 'i',   'й' => 'y',   'к' => 'k',
				'л' => 'l',   'м' => 'm',   'н' => 'n',
				'о' => 'o',   'п' => 'p',   'р' => 'r',
				'с' => 's',   'т' => 't',   'у' => 'u',
				'ф' => 'f',   'х' => 'h',   'ц' => 'c',
				'ч' => 'ch',  'ш' => 'sh',  'щ' => 'sch',
				'ь' => '\'',  'ы' => 'y',   'ъ' => '\'',
				'э' => 'e',   'ю' => 'yu',  'я' => 'ya',
				
				'А' => 'A',   'Б' => 'B',   'В' => 'V',
				'Г' => 'G',   'Д' => 'D',   'Е' => 'E',
				'Ё' => 'E',   'Ж' => 'Zh',  'З' => 'Z',
				'И' => 'I',   'Й' => 'Y',   'К' => 'K',
				'Л' => 'L',   'М' => 'M',   'Н' => 'N',
				'О' => 'O',   'П' => 'P',   'Р' => 'R',
				'С' => 'S',   'Т' => 'T',   'У' => 'U',
				'Ф' => 'F',   'Х' => 'H',   'Ц' => 'C',
				'Ч' => 'Ch',  'Ш' => 'Sh',  'Щ' => 'Sch',
				'Ь' => '\'',  'Ы' => 'Y',   'Ъ' => '\'',
				'Э' => 'E',   'Ю' => 'Yu',  'Я' => 'Ya',
			);
			return strtr($string, $converter);
		}
		function str2url($str) {
			// переводим в транслит
			$str = rus2translit($str);
			// в нижний регистр
			$str = strtolower($str);
			// заменям все ненужное нам на "-"
			$str = preg_replace('~[^-a-z0-9_]+~u', '-', $str);
			// удаляем начальные и конечные '-'
			$str = trim($str, "-");
			return $str;
		}


 $array=array();
 if (isset($_REQUEST['csv']))
 {
			$file=$_REQUEST['csv'];
			
			//if (strpos($csv,'.csv')!==false){// значит файл цсв
				
				$fields='artpost;brand;name;currency_supplier;price_supplier;markup';
				$keys = array_map('trim', explode(';', strtolower($fields)));
				$delimeter = ';';
				/*
				- артикул опции
				- название товара
				- цена поставщика
				- накрутка (то же самое что ручная установка на уровне опции)*/
				/*echo '<pre>';
				print_r($keys);
				echo '</pre>';*/

				$result=import($file,$delimeter,$keys);
				/*echo '<pre>';
print_r($result);
echo '</pre>';*/
				$array=array();
			$array[]=array('file'=>$result);//$csv);
			
			return $this->outputArray($array,1);

			
 }else return $this->outputArray($array,0);
 /*
 function toJson($mixed) {
       return preg_replace_callback(
                "/\\\\u([a-f0-9]{4})/",
                function($matches) {
                    return iconv('UCS-4LE','UTF-8',pack('V', hexdec('U' . $matches[0])));
                },
                json_encode($mixed)
                );
    }
 */
 
 
 function json_fix_cyr($json_str) {
		$cyr_chars = array (
		'а' => '\u0430', 'А' => '\u0410',
		'б' => '\u0431', 'Б' => '\u0411',
		'в' => '\u0432', 'В' => '\u0412',
		'г' => '\u0433', 'Г' => '\u0413',
		'д' => '\u0434', 'Д' => '\u0414',
		'е' => '\u0435', 'Е' => '\u0415',
		'ё' => '\u0451', 'Ё' => '\u0401',
		'ж' => '\u0436', 'Ж' => '\u0416',
		'з' => '\u0437', 'З' => '\u0417',
		'и' => '\u0438', 'И' => '\u0418',
		'й' => '\u0439', 'Й' => '\u0419',
		'к' => '\u043a', 'К' => '\u041a',
		'л' => '\u043b', 'Л' => '\u041b',
		'м' => '\u043c', 'М' => '\u041c',
		'н' => '\u043d', 'Н' => '\u041d',
		'о' => '\u043e', 'О' => '\u041e',
		'п' => '\u043f', 'П' => '\u041f',
		'р' => '\u0440', 'Р' => '\u0420',
		'с' => '\u0441', 'С' => '\u0421',
		'т' => '\u0442', 'Т' => '\u0422',
		'у' => '\u0443', 'У' => '\u0423',
		'ф' => '\u0444', 'Ф' => '\u0424',
		'х' => '\u0445', 'Х' => '\u0425',
		'ц' => '\u0446', 'Ц' => '\u0426',
		'ч' => '\u0447', 'Ч' => '\u0427',
		'ш' => '\u0448', 'Ш' => '\u0428',
		'щ' => '\u0449', 'Щ' => '\u0429',
		'ъ' => '\u044a', 'Ъ' => '\u042a',
		'ы' => '\u044b', 'Ы' => '\u042b',
		'ь' => '\u044c', 'Ь' => '\u042c',
		'э' => '\u044d', 'Э' => '\u042d',
		'ю' => '\u044e', 'Ю' => '\u042e',
		'я' => '\u044f', 'Я' => '\u042f',
		'' => '\r',
		'<br />' => '\n',
		'' => '\t'
		);

		foreach ($cyr_chars as $cyr_char_key => $cyr_char) {
		$json_str = str_replace($cyr_char_key, $cyr_char, $json_str);
		}
		return $json_str;
}

function utf8_urldecode($str) {
$str = preg_replace("/%u([0-9a-f]{3,4})/i","&#x\\1;",urldecode($str));
return html_entity_decode($str,null,'UTF-8');;
}



 function import($file,$delimeter,$keys){
	 global $modx;
	 $brand='';
			// Import!
			//echo $_SERVER['DOCUMENT_ROOT'].'/'.$file;
			$handle = fopen($_SERVER['DOCUMENT_ROOT'].'/'.$file, "r");
			$rows = $created = $updated = 0;
			//$csv = fgetcsv($handle, 0, $delimeter);
			$errors=$skusdata=array();
			/*echo '<pre>';
			print_r($keys);
			echo '</pre>';*/
			$num=$changpr=0;
			$change20=array();
			$i=0;
				$arraysave=$arraynosave=array();
				
				
			while (($csv = fgetcsv($handle, 0, $delimeter)) !== false) {
				
				$rows ++;
				$data = $gallery = array();
				//$modx->error->reset();
				//$modx->log(modX::LOG_LEVEL_INFO, "Raw data for import: \n".print_r($csv,1));
				foreach ($keys as $k => $v) {
					/*if (!isset($csv[$k])) {
						exit('Field "' . $v . '" not exists in file. Please fix import file or parameter "fields".');
					}*/
					if (isset($data[$v]) && !is_array($data[$v])) {
						$data[$v] = array($data[$v], $csv[$k]);
					}
					elseif (isset($data[$v]) && is_array($data[$v])) {
						$data[$v][] = $csv[$k];
					}
					else {
						$data[$v] = $csv[$k];
					}
				}/*echo '<pre>';
					print_r($data);
					echo '</pre>';*/
				if ($i>0){
					$dataall[]=$data;
					$skusdata[]=$data['artpost'];
					
					
					$data["artpost"]=str2url($data["artpost"]);
					
					
					//$jsonatr=$data["artpost"];//utf8_urldecode($data["artpost"]);
					$jsonatr=$data["artpost"];
					//str2url($jsonatr);// json_fix_cyr($data["artpost"]);
					
					
					
					//json_encode(iconv("CP1251","UTF-8",$jsonatr));
					//json_encode($data["artpost"]);//utf8_urldecode();//json_fix_cyr($data["artpost"]);
					//$jsonatr=str_replace('\\','\',$jsonatr);
					//echo '$jsonatr'.utf8_urldecode(json_encode($data["artpost"]));//array(
					//echo '$jsonatr'.$jsonatr.'<br/>';
					if ((!isset($data['brand']))||($data['brand']=='')) {$errors[]="- Ошибка в строке ".$rows;continue;}
								if ((!isset($data['artpost']))||($data['artpost']=='')) {$errors[]="- Ошибка в строке ".$rows;continue;}
								if ((!isset($data['name']))||($data['name']=='')) {$errors[]="- Ошибка в строке ".$rows;continue;}
								if ((!isset($data['currency_supplier']))||($data['currency_supplier']=='')) {$errors[]="- Ошибка в строке ".$rows;continue;}//|| (gettype($res['currency_supplier'])!='integer')
								if ((!isset($data['price_supplier']))||($data['price_supplier']=='')) {$errors[]="- Ошибка в строке ".$rows;continue;}
								if ((!isset($data['markup']))||($data['markup']=='')) {$errors[]="- Ошибка в строке ".$rows;continue;}//|| (gettype($res['currency_supplier'])!='integer')
					$q = $modx->newQuery('modTemplateVarResource');
					//$q->innerJoin('msProductData', 'Data', $data['class_key'].'.id = Data.id');
					$q->select('*');
					$q->where(array('tmplvarid' => 1,'value:LIKE'=>'%"artpost":"'.$jsonatr.'",%'));//$data["artpost"]
					//$q->where(array('tmplvarid' => 1,'value:LIKE'=>"%".str_replace('"', '', json_encode($data["artpost"]))."%"));//$data["artpost"]
					$q->prepare();					
					$q->stmt->execute();
					$sql = $q->toSQL();
					$result1 = $q->stmt->fetchAll(PDO::FETCH_ASSOC);	
					$id_product=false;
			//	echo $sql.'<br/>';
				//'%"artpost":"'.$data["artpost"].'",%<br/>';
					/*echo '<pre>result1';
					print_r($result1);
					echo '</pre>';*/
					
					
					if ($result1&&(count($result1)>0))
					{
						
						$brandexist=false;
						foreach ($result1 as $res)
						{
							$id_product=$res['modTemplateVarResource_contentid'];
							
							$id_object=$res['modTemplateVarResource_id'];
							if ($id_product)
							{
								
									//artpost;brand;name;currency_supplier;price_supplier;markup
								$q = $modx->newQuery('msProduct');
								$q->select('msProduct.id');
								$q->innerJoin('msProductData', 'Data', 'msProduct.id = Data.id');
								$q->innerJoin('msVendor', 'Vendor', 'Data.vendor = Vendor.id');
								$q->select('*');
								$q->where(array('msProduct.id'=>$id_product,'Vendor.name'=>$data["brand"]));
								//$q->where(array('tmplvarid' => 1,'value:LIKE'=>'%"artpost":"'.$data["artpost"].'",%'));
								$q->prepare();					
								$q->stmt->execute();
								//echo $id_product.' '.$data["brand"].'<br/>';
								$result2 = $q->stmt->fetch(PDO::FETCH_ASSOC);	
								if ($result2&&(count($result2)>0)) // нашли данный товар данного бренда
								{
									$brandexist=true;
									// формируем массив для обновления
									$options_old=$modx->fromJSON($res['modTemplateVarResource_value']);//, true);
									$options=$options_old;
									/*echo '<pre>$option1';
									print_R($options);
									echo '</pre>';*/
									foreach ($options_old as $key=>$opt)
									{
										//$options[$key]=
										//echo $options[$key]["markup"].' '.$data["markup"].' '.$opt['artpost'].' '.$data["artpost"];
										if ($opt['artpost']==$data["artpost"]){
											
											if (($options[$key]["markup"]!=$data["markup"])||($options[$key]["price_supplier"]!=$data["price_supplier"]))
											{// цена изменилась												
												$changpr++;
												$markup=$data["price_supplier"]-$options[$key]["price_supplier"];
												$markup1=(float)($markup*100)/$options[$key]["price_supplier"];
												$markup1=abs($markup1);
												//echo $markup1."\r\n";
												if ($markup1>20)$change20[]=$data['artpost'];//(($markup<(-20)||($markup>20))
												$options[$key]["price_supplier"]=$data["price_supplier"];
												//- Цена изменена у n товаров из N
											}else{//выведем где не изменилась
												$nochange[]=$data;
											}
											$options[$key]["markup"]=$data["markup"];
											$options[$key]["currency_supplier"]=$data["currency_supplier"];
											$num++;
										}
									}
									//$option['markup']=$data["markup"];
									//$option['price_supplier']=$data["price_supplier"];
									//$option['currency_supplier']=$data["currency_supplier"];
									
									$optionsar=$modx->toJSON($options);
									
									
									$option_save = $modx->getObject('modTemplateVarResource',array('id'=>$id_object,'contentid'=>$id_product));
									if ($option_save){
										$option_save->set('value',$optionsar);//$option);
										if ($option_save->save()){
											$arraysave[]=$data;//$res;
											
										}else{
											$arraynosave[]=$data['artpost'];
										}
										
									}else{										
										$arraynosave[]=$data['artpost'];//array_merge($data,array('nosave'=>'ошибка сохранения'));
									}
									//break;
								}else{
									$arraynosave[]=$data['artpost'];
								}
							}
						}
						
					}else{// товара нет в магазине, ндао отобразить в отчёте
						$arraynosave[]=$data['artpost'];//array_merge($res,array('nosave'=>'Нет в магазине. Надо добавить'));
					}
					
					
				}
				$brand=$data["brand"];
				$i++;
			}
			fclose($handle);
			
			$file='/assets/reportimport/report'.time().'.txt';
			$filenos=$_SERVER['DOCUMENT_ROOT'].$file;
			$handle = fopen($filenos, "w+");//$_SERVER['DOCUMENT_ROOT'].'/'.
			
		//	echo '$filenos'.$filenos;
			$reporttxt="Дата:".date('Y-d-m H:m:s')."\r\n\r\n";
			if(count($arraynosave)>0){
				$reporttxt.="\r\n- На сайте не найдены товары с артикулами:\r\n";
				/*foreach ($arraynosave as $ar)
				{
					$reporttxt.=$ar['artpost'].", ";	
				}*/
				$reporttxt.=implode(',',$arraynosave);//$ar['artpost'].", ";	
				//Ошибки:
				//- Ошибка в строке N. Прайс не загружен
				//- В прайсе отсутствуют товары с артикулами: 1, 2, 3
				//- На сайте не найдены товары с артикулами: 1, 2, 3

				//Сообщения: 
				//- Цена изменена у n товаров из N
				//- Цена изменилась более чем на 20% для товаров с артикулами: 1, 2 ,3
				
			}
			
			if(count($errors)>0){
				$reporttxt.="\r\n";
				/*foreach ($errors as $ar)
				{
					$reporttxt.=$ar."\r\n";	
				}*/
				$reporttxt.=implode("\r\n",$errors);
				
			}
			
			if(count($arraysave)>0){
				$reporttxt.="\r\nУспешное сохранение\r\n";
				$reporttxt.="- Цена изменена у ".$changpr." товаров из ".$num."\r\n";
				if (count($nochange)>0){
					$reporttxt.="- Цена не изменена:\r\n";
					foreach ($nochange as $no)	
					{
						//" у ".$changpr." товаров из ".$num;
						$reporttxt.=$no['artpost']."\r\n";
						
					}
					$reporttxt.="\r\n";
				}
				if (count($change20)>0){
					$reporttxt.="\r\n- Цена изменилась более чем на 20% для товаров с артикулами: \r\n";
					/*foreach ($change20 as $no)	
					{
						//" у ".$changpr." товаров из ".$num;
						
						
					}*/
					$reporttxt.=implode(',',$change20);
					$reporttxt.="\r\n";
				}
				
				
				/*foreach ($arraysave as $ar)
				{
					$reporttxt.=$ar['name']."\r\n";	
				}*/
			}
			
			
				
				//получим все товары чтобы проверить каких нет в прайсе - т.е. их нет в наличии
			if ($brand!=''){
				$q = $modx->newQuery('msProduct');
								$q->select('msProduct.id');
								$q->innerJoin('msProductData', 'Data', 'msProduct.id = Data.id');
								$q->innerJoin('msVendor', 'Vendor', 'Data.vendor = Vendor.id');
								$q->innerJoin('modTemplateVarResource', 'TV', 'msProduct.id = TV.contentid');
								
								$q->select('msProduct.id,TV.value,msProduct.pagetitle,Vendor.name');
								$q->where(array('Vendor.name'=>$data["brand"]));
								//$q->where(array('tmplvarid' => 1,'value:LIKE'=>'%"artpost":"'.$data["artpost"].'",%'));
								$q->prepare();					
								$q->stmt->execute();
								$result2 = $q->stmt->fetchAll(PDO::FETCH_ASSOC);	
								if ($result2&&(count($result2)>0)) // нашли данный товар данного бренда
								{
									
									$skus=$notproduct=array();
									foreach ($result2 as $res)
									{
										$tv=$modx->fromJSON($res['value']);
										foreach ($tv as $t)
										{
											$skus[]=$t['artpost'];
										}
									}
									if (count($skus)>0){//$dataall
										foreach ($skus as $sku)
										{
											if (!in_array($sku,$skusdata))
											{
												$notproduct[]=$sku;//$data['artpost'];
											}
										}
										if (count($notproduct)>0){
											
											$reporttxt.="\r\n- В прайсе отсутствуют товары с артикулами: ".implode(",",$notproduct);
											
											
										}
										
									}else{
											$reporttxt.="\r\n-Товаров нет : \r\n";
											
											
											$reporttxt.="\r\n";
									}
									
									/*echo '<pre>';
									print_r($skus);
									echo '</pre>';*/
									/*value
									
									$q = $modx->newQuery('modTemplateVarResource');
									
									$q->select('*');
									$q->where(array('tmplvarid' => 1,'value:LIKE'=>'%"artpost":"'.$data["artpost"].'",%'));*/
								}
			
			
				}
			
			
			fwrite($handle,header('Content-Type: text/html; charset=utf8'));
			fwrite($handle, $reporttxt); //iconv("WINDOWS-1251", "UTF-8", 
			fclose($handle);//$handle);
			/*echo '<pre>';
			print_r($arraynosave);//$arraynosave);
			echo '</pre>';*/
			

	/*		if (!XPDO_CLI_MODE) {echo '<pre>';}
			echo "\nImport complete in ".number_format(microtime(true) - $modx->startTime, 7) . " s\n";
			echo "\nTotal rows:	$rows\n";
			echo "Created:	$created\n";
			echo "Updated:	$updated\n";
			if (!XPDO_CLI_MODE) {echo '</pre>';}
*/
		return '<a href="http://petchoice.com.ua'.$file.'" target="_blank">Скачать отчёт</a>';

	 
 }
 /*
$isLimit = !empty($_REQUEST['limit']);
$start = $modx->getOption('start',$_REQUEST,0);
$limit = $modx->getOption('limit',$_REQUEST,20);
$sort = $modx->getOption('sort',$_REQUEST,'name');
$dir = $modx->getOption('dir',$_REQUEST,'ASC');

$c = $modx->newQuery('modExtraItem');
$count = $modx->getCount('modExtraItem',$c);

$c->sortby($sort,$dir);
if ($isLimit) $c->limit($limit,$start);
$items = $modx->getCollection('modExtraItem',$c);

$list = array();
foreach ($items as $item) {
    $itemArray = $item->toArray();
    $list[]= $itemArray;
}
return $this->outputArray($list,$count);*/