<?php
/**
 * modExtra
 *
 * Copyright 2010 by Shaun McCormick <shaun+modextra@modx.com>
 *
 * modExtra is free software; you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 2 of the License, or (at your option) any later
 * version.
 *
 * modExtra is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * modExtra; if not, write to the Free Software Foundation, Inc., 59 Temple
 * Place, Suite 330, Boston, MA 02111-1307 USA
 *
 * @package modextra
 */
/**
 * Get a list of Items
 *
 * @package modextra
 * @subpackage processors
 */

 //require_once '../config.core.php';


 //error_reporting(E_ALL | E_STRICT);
//ini_set('display_errors', 1);
 
 //require_once dirname(dirname(dirname(dirname(__FILE__)))) . '/config/config.inc.php';

//require_once MODX_CORE_PATH.'model/modx/modx.class.php';
//$modx = new modX();
//$modx->initialize('mgr');

set_time_limit(0);
ini_set('MAX_EXECUTION_TIME', 0);
//echo ini_get('max_execution_time'); // 10
//die();
//ini_set('MAX_EXECUTION_TIME', 8640000);
//set_time_limit(5565000);
//ini_set('MAX_EXECUTION_TIME', -1);

function rus2translit($string) {
    $converter = array(
        'а' => 'a',   'б' => 'b',   'в' => 'v',
        'г' => 'g',   'д' => 'd',   'е' => 'e',
        'ё' => 'e',   'ж' => 'zh',  'з' => 'z',
        'и' => 'i',   'й' => 'y',   'к' => 'k',
        'л' => 'l',   'м' => 'm',   'н' => 'n',
        'о' => 'o',   'п' => 'p',   'р' => 'r',
        'с' => 's',   'т' => 't',   'у' => 'u',
        'ф' => 'f',   'х' => 'h',   'ц' => 'c',
        'ч' => 'ch',  'ш' => 'sh',  'щ' => 'sch',
        'ь' => '\'',  'ы' => 'y',   'ъ' => '\'',
        'э' => 'e',   'ю' => 'yu',  'я' => 'ya',

        'А' => 'A',   'Б' => 'B',   'В' => 'V',
        'Г' => 'G',   'Д' => 'D',   'Е' => 'E',
        'Ё' => 'E',   'Ж' => 'Zh',  'З' => 'Z',
        'И' => 'I',   'Й' => 'Y',   'К' => 'K',
        'Л' => 'L',   'М' => 'M',   'Н' => 'N',
        'О' => 'O',   'П' => 'P',   'Р' => 'R',
        'С' => 'S',   'Т' => 'T',   'У' => 'U',
        'Ф' => 'F',   'Х' => 'H',   'Ц' => 'C',
        'Ч' => 'Ch',  'Ш' => 'Sh',  'Щ' => 'Sch',
        'Ь' => '\'',  'Ы' => 'Y',   'Ъ' => '\'',
        'Э' => 'E',   'Ю' => 'Yu',  'Я' => 'Ya',
    );
    return strtr($string, $converter);
}
function str2url($str) {
    // переводим в транслит
    $str = rus2translit($str);
    // в нижний регистр
    //$str = strtoupper($str);
    //$str = mb_strtoupper($str);//strtolower
    //$str = strtoupper($str);
    // заменям все ненужное нам на "-"
    //$str = preg_replace('~[^-a-z0-9_]+~u', '-', $str);
    // удаляем начальные и конечные '-'
    //$str = trim($str, "-");
    return $str;
}


 $array=array();

if (isset($_REQUEST['updateRelated'])) {

    ob_start();
    $q = $modx->newQuery('msProduct');
    $q->where(array('class_key' => 'msProduct', 'published' => 1, 'deleted' => 0));//'parent:IN' => $pids,
    $q->innerJoin('msProductData', 'Data', 'msProduct.id = Data.id');
    $q->select('`Data`.`vendor`, `msProduct`.`id`,`msProduct`.`parent`');
    if ($q->prepare() && $q->stmt->execute()) {
        $products = $q->stmt->fetchAll(PDO::FETCH_ASSOC);
    }
    $limit = 10;
    $count_rel_ar=[];
    foreach ($products as $product) {

        $count_rel=0;
        $id = $product['id'];

        $parent = $product['parent'];
        $vendor=$product['vendor'];
        $count_rel_ar=[];
        $ids = [];

        $sql = "SELECT attribute.id, group.id as groupid, group.sort_related
            FROM `modx_ms2_attributes` as `attribute`
            INNER JOIN `modx_ms2_attribute_groups` as `group` ON (`attribute`.`attribute_group`=`group`.`id`)
            INNER JOIN `modx_ms2_attributes_product` as `prod_atr` ON (`attribute`.`id`=`prod_atr`.`attribute_id`)
            WHERE `prod_atr`.`product_id` ='".$id."'";//" ORDER BY sortatr ASC";

        $q = $modx->prepare($sql);
        $q->execute();
        $res_attr = $q->fetchAll(PDO::FETCH_ASSOC);

        $sql = "SELECT `product`.id,`product`.vendor 
        FROM `modx_site_content` as `content`
        LEFT JOIN `modx_ms2_products` as product ON (`content`.id=`product`.id)
        WHERE `content`.`parent`='".$parent."' AND `product`.`id`!='".$id."'";


        $q = $modx->prepare($sql);
        $q->execute();
        $res_products = $q->fetchAll(PDO::FETCH_ASSOC);
        /*echo "<pre>";
        print_r($res_products);
        echo "</pre>";die();*/
        foreach ($res_products as $id_rel)
        {
            $count_rel=0;
//, attribute.name, group.name as groupname,group.sort as sortatr
            //получим остальные атрибуты
            $sql = "SELECT attribute.id, `group`.id as groupid, `group`.sort_related
 FROM `modx_ms2_attributes` as `attribute`
            INNER JOIN `modx_ms2_attribute_groups` as `group` ON (`attribute`.`attribute_group`=`group`.`id`)
            INNER JOIN `modx_ms2_attributes_product` as `prod_atr` ON (`attribute`.`id`=`prod_atr`.`attribute_id`)
            WHERE `prod_atr`.`product_id` ='".$id_rel['id']."'";

            $q = $modx->prepare($sql);
            $q->execute();
            $res_attr_other_pr = $q->fetchAll(PDO::FETCH_ASSOC);
            foreach ($res_attr as $attr_current)
            {//echo 'sort'.$attr_current['sort_related']."<br/>";
                foreach ($res_attr_other_pr as $attr)
                {
                    if ((($attr_current['group_id']==$attr['group_id'])
                            &&($attr_current['id']==$attr['id']))||($vendor==$id_rel['vendor']))
                    {

                        if ($attr['sort_related']==1) {
                            $count_rel = $count_rel + 3;
                        }
                        else $count_rel++;
                    }
                }
            }
            $count_rel_ar[$id_rel['id']]=$count_rel;

            flush();
            ob_flush();

        }
        arsort($count_rel_ar);
        $ids=[];
        if (count($count_rel_ar)>0) {

            $i=0;
            foreach ($count_rel_ar as $key=>$value)
            {
                $i++;
                $ids[]=$key;
                if ($i==$limit) break;
            }
        }


        if (count($ids)>0) {
            $value = implode(",", $ids);

            $q = $modx->newQuery('modTemplateVarResource');
            $where = array(
                'contentid' => $id
            , 'tmplvarid' => 9
            );
            $q->command('update');
            $q->set(array(
                'value' => $value
            ));
            $q->where($where);
            $q->prepare();
            //..if ($q->stmt->execute()) echo $id . "ok\r\n";

            $q->stmt->execute();


            /*echo "<pre>".$id;
            print_r($ids);
            echo "</pre>";
                die();*/

        }

        flush();
        ob_flush();
    }
    ob_end_flush();
    $array=[];
    $array[]=array('file'=>'Готово. Очисти кеш и проверяй на сайте! :)');//$csv);

    return $this->outputArray($array,1);
}
elseif (isset($_REQUEST['createCsv']))
 {
	 /*$q = $modx->newQuery('msProduct');
		$q->where(array('class_key' => 'msProduct'));//,'published' => 1,'deleted' => 0));//'parent:IN' => $pids,
		//$q->select('`msProduct`.`id`');
		if ($q->prepare() && $q->stmt->execute()) {
			$ids = $q->stmt->fetchAll(PDO::FETCH_COLUMN);
		}*/
		
		
		$q = $modx->newQuery('msProduct');
								$q->select(array('msProduct.*','Vendor.name as vendorname','Data.price','Data.image','Data.thumb','Data.price_max'));//msProduct.id'. 'msProduct.pagetitle'));//msProduct.*, Data.*, msVendor.*');//array('msProduct.*','msProductData.*');msProduct.*,Data.*, msVendor.
								
								
								$q->innerJoin('msProductData', 'Data', 'msProduct.id = Data.id');
								$q->innerJoin('msVendor', 'Vendor', 'Data.vendor = Vendor.id');
								
								//$q->where(array('Vendor.name'=>$vendor));
								$q->where(array('msProduct.published'=>1,'msProduct.deleted'=>0));
								
								//$q->limit(5);
								$q->prepare();					
								$q->stmt->execute();
							
								$result2 = $q->stmt->fetchAll(PDO::FETCH_ASSOC);	
							
								$list=array();$i=0;
								//"ID",ID2,Item title,Final URL,Image URL,Item subtitle,Item description,Item category,Price,Sale price,Contextual keywords,Item address,Tracking template,Custom parameter
								foreach ($result2 as $pr)
								{	
									$i++;

									$q = $modx->newQuery('modTemplateVarResource');					
									$q->select('*');
									$q->where(array('tmplvarid' => 1,'contentid'=>$pr['id']));									
									$q->prepare();					
									$q->stmt->execute();
									$sql = $q->toSQL();
									$result1 = $q->stmt->fetchAll(PDO::FETCH_ASSOC);									
									foreach ($result1 as $res)
									{
										$val=$modx->fromJSON($res['modTemplateVarResource_value']);
										$artposts=array();
										foreach ($val as $option) 
										{
											if ($option['artpost']!='')$artposts[]=$option['artpost'];
										}
									}
									if ($pr['price_max']>=499)
									$text="Бесплатная доставка";
									else $text="Скидка 5% на первый заказ";
									
									//$category=$pr['parent'];
									$cat = $modx->getObject('msCategory',$pr['parent']);//newQuery('modResource');
									if ($cat){
										$h1=$cat->get('pagetitle');
									}
									
									  switch ($pr['parent'])
										{
											case '14':
												$categorypet='для кошек';
											break;
											case 13:
												$categorypet='для собак';
											break;
											case 26:
												$categorypet='для рыб';
											break;
											case 27:
												$categorypet='для птиц';
											break;
											case 28:
												$categorypet='для грызунов';
											break;
											case 1148:
												$categorypet='для хорьков';
											break;
											default:
												$categorypet='';
											break;
											
										}
										$h1=$h1.' '.$categorypet;
									   
									   //{snippet name=pthumb params="input=`{$img.url}`&options=`w=600&h=600&far=1`&fltr[]=wmi|/assets/images/water_logo.png|C|80" assign=full}
                                                                                $params = array(
										'input' =>$pr['image'],
										'options' => 'w=500&h=500&far=1',
										'useResizer'=>'1'
									);
									$image=$modx->runSnippet('pthumb', $params);
									   //echo $image;die();
									$product=array('"'.$pr['id'].'"',
									'"'.$pr['vendorname'].'"',
									'"https://petchoice.ua/'.$pr['uri'].'"',
									'"https://petchoice.ua'.$image.'"',									
									'"'.$text.'"',									
									'"'.$h1.'"',
									'"'.number_format($pr['price'], 2, '.', ',').' UAH"');
									$list[]=$product;
									//if ($i==5) break;
								}
															
							$file='/assets/reportimport/google-csv.csv';
							$filenos=$_SERVER['DOCUMENT_ROOT'].$file;							
							$fp = fopen($filenos, 'w+');
							$f='';
							$f.="ID,Item title,Final URL,Image URL,Item subtitle,Item category,Price\r\n";
							foreach ($list as $fields) {
								//fputcsv($fp, $fields,',','"');
								$g=implode(',',$fields);								
								$f.=$g."\r\n";
							}	

					fwrite($fp,header('Content-Type: text/html; charset=utf8'));
					fwrite($fp, $f); //iconv("WINDOWS-1251", "UTF-8", 
					fclose($fp);//$handle);							
							
							
							
							$array=array();
			$array[]=array('file'=>'<a href="http://petchoice.com.ua'.$file.'" target="_blank">Скачать продукты</a>');//$csv);
			
			return $this->outputArray($array,1);
			
			
 }
 elseif (isset($_REQUEST['generate']))
 {

						
		$q = $modx->newQuery('msProduct');
		$q->where(array('class_key' => 'msProduct'));//,'published' => 1,'deleted' => 0));//'parent:IN' => $pids,
		//$q->select('`msProduct`.`id`');
		if ($q->prepare() && $q->stmt->execute()) {
			$ids = $q->stmt->fetchAll(PDO::FETCH_COLUMN);
		}
		$i=0;
		foreach ($ids as $id)
		{
			$prices=array();
			$price=0;
			$product=$modx->getObject('msProduct',$id);
			// надо получить опции
			$tv_id=1;
			$options=array();
			$q = $modx->newQuery('modTemplateVar');
			$q->innerJoin('modTemplateVarResource', 'v', 'v.tmplvarid = modTemplateVar.id');
			$q->where(array(
				'v.contentid'  =>$id, 
				'modTemplateVar.id'  => $tv_id, 
			));
			$q->limit(1);
			$q->select(array('modTemplateVar.*', 'v.value'));
			$value=array();
			$array=array();
			if ($q->prepare() && $q->stmt->execute()) {
				while ($tv = $q->stmt->fetch(PDO::FETCH_ASSOC)) {
					$value =  $tv['value'];
				}
			}
			
			// Получаем массив из JSON-строки
			if (count($value)>0)  $options = $modx->fromJSON($value);
			$options1=array();
			foreach ($options as $option)
			{								
				$artpost=$option['artpost'];
				if(preg_match('/[^0-9a-zA-Z]/', $artpost)){
					$artpost=str2url($artpost);
					$option['artpost']=$artpost;					
				} 
				
				$options1[]=$option;
				
			}
			$tvvar = $modx->getObject('modTemplateVarResource',array('tmplvarid'=>1,'contentid'=>$id));
			if ($tvvar)
			{
				$tvvar->set('value',$modx->toJSON($options1));
				$tvvar->save();//) echo 'ok';
			}
			
			$i++;
		}
		$array=array();
			$array[]=array('file'=>'Артиклы обновились');//$csv);
			
			return $this->outputArray($array,1);
		//return true;//'Артиклы обновились';
 die();
 }
 
 if (isset($_REQUEST['brand']))// выгрузим продукты бренда
 {
	 $vendor=$_REQUEST['brand'];
	 
		//- артикул товара
		//- название товараф
		//- артикул поставщика
	 	$q = $modx->newQuery('msProduct');
								
								$q->innerJoin('msProductData', 'Data', 'msProduct.id = Data.id');
								$q->innerJoin('msVendor', 'Vendor', 'Data.vendor = Vendor.id');
								$q->select('msProduct.*, Data.*');//array('msProduct.*','msProductData.*');
								$q->where(array('Vendor.name'=>$vendor));
								
								$q->prepare();					
								$q->stmt->execute();
								
								$result2 = $q->stmt->fetchAll(PDO::FETCH_ASSOC);	
								$list=array();
								foreach ($result2 as $pr)
								{									
									$q = $modx->newQuery('modTemplateVarResource');					
									$q->select('*');
									$q->where(array('tmplvarid' => 1,'contentid'=>$pr['id']));									
									$q->prepare();					
									$q->stmt->execute();
									$sql = $q->toSQL();
									$result1 = $q->stmt->fetchAll(PDO::FETCH_ASSOC);									
									foreach ($result1 as $res)
									{
										$val=$modx->fromJSON($res['modTemplateVarResource_value']);
										$artposts=array();
										foreach ($val as $option) 
										{
											if ($option['artpost']!='')$artposts[]=$option['artpost'];
										}
									}
									
									$product=array('"'.$pr['pagetitle'].'"','"'.$pr['article'].'"', '"'.implode(';',$artposts).'"');
									$list[]=$product;
								}
															
							$file='/assets/reportimport/brand-'.str2url($vendor).'.csv';
							$filenos=$_SERVER['DOCUMENT_ROOT'].$file;							
							$fp = fopen($filenos, 'w+');
							
							foreach ($list as $fields) {
								//fputcsv($fp, $fields,',','"');
								$g=implode(',',$fields);								
								$f.=$g."\r\n";
							}	

					fwrite($fp,header('Content-Type: text/html; charset=utf8'));
					fwrite($fp, $f); //iconv("WINDOWS-1251", "UTF-8", 
					fclose($fp);//$handle);							
							
							
							
							$array=array();
			$array[]=array('file'=>'<a href="http://petchoice.com.ua'.$file.'" target="_blank">Скачать продукты</a>');//$csv);
			
			return $this->outputArray($array,1);
							
							
 }
 
 
 if (isset($_REQUEST['csvbob']))// обновление Марина Бобровник
 {
	 
	 
	 $file=$_REQUEST['csvbob'];			
			$fields='artpost;name;price_supplier';
			$keys = array_map('trim', explode(';', strtolower($fields)));
			$delimeter = ';';
				
			$result=importbob($file,$delimeter,$keys);				
			$array=array();
			$array[]=array('file'=>$result);			
			return $this->outputArray($array,1);			
 } 
 elseif (isset($_REQUEST['csv']))
 {
			$file=$_REQUEST['csv'];			
			$fields='artpost;artman;brand;name;currency_supplier;price_supplier;markup;price_site';
			$keys = array_map('trim', explode(';', strtolower($fields)));
			$delimeter = ';';
				
			$result=import($file,$delimeter,$keys);				
			$array=array();
			$array[]=array('file'=>$result);			
			return $this->outputArray($array,1);			
 }else return $this->outputArray($array,0);
 
 
 function json_fix_cyr($json_str) {
		$cyr_chars = array (
		'а' => '\u0430', 'А' => '\u0410',
		'б' => '\u0431', 'Б' => '\u0411',
		'в' => '\u0432', 'В' => '\u0412',
		'г' => '\u0433', 'Г' => '\u0413',
		'д' => '\u0434', 'Д' => '\u0414',
		'е' => '\u0435', 'Е' => '\u0415',
		'ё' => '\u0451', 'Ё' => '\u0401',
		'ж' => '\u0436', 'Ж' => '\u0416',
		'з' => '\u0437', 'З' => '\u0417',
		'и' => '\u0438', 'И' => '\u0418',
		'й' => '\u0439', 'Й' => '\u0419',
		'к' => '\u043a', 'К' => '\u041a',
		'л' => '\u043b', 'Л' => '\u041b',
		'м' => '\u043c', 'М' => '\u041c',
		'н' => '\u043d', 'Н' => '\u041d',
		'о' => '\u043e', 'О' => '\u041e',
		'п' => '\u043f', 'П' => '\u041f',
		'р' => '\u0440', 'Р' => '\u0420',
		'с' => '\u0441', 'С' => '\u0421',
		'т' => '\u0442', 'Т' => '\u0422',
		'у' => '\u0443', 'У' => '\u0423',
		'ф' => '\u0444', 'Ф' => '\u0424',
		'х' => '\u0445', 'Х' => '\u0425',
		'ц' => '\u0446', 'Ц' => '\u0426',
		'ч' => '\u0447', 'Ч' => '\u0427',
		'ш' => '\u0448', 'Ш' => '\u0428',
		'щ' => '\u0449', 'Щ' => '\u0429',
		'ъ' => '\u044a', 'Ъ' => '\u042a',
		'ы' => '\u044b', 'Ы' => '\u042b',
		'ь' => '\u044c', 'Ь' => '\u042c',
		'э' => '\u044d', 'Э' => '\u042d',
		'ю' => '\u044e', 'Ю' => '\u042e',
		'я' => '\u044f', 'Я' => '\u042f',
		'' => '\r',
		'<br />' => '\n',
		'' => '\t'
		);

		foreach ($cyr_chars as $cyr_char_key => $cyr_char) {
		$json_str = str_replace($cyr_char_key, $cyr_char, $json_str);
		}
		return $json_str;
}

function utf8_urldecode($str) {
$str = preg_replace("/%u([0-9a-f]{3,4})/i","&#x\\1;",urldecode($str));
return html_entity_decode($str,null,'UTF-8');;
}




 function importbob($file,$delimeter,$keys){
	 global $modx;
	 $brand='';
			
			$handle = fopen($_SERVER['DOCUMENT_ROOT'].'/'.$file, "r");
			$rows = $created = $updated = 0;
			
			$errors=$skusdata=array();
			
			$num=$changpr=0;
			$change20=array();
			$i=0;
				$arraysave=$arraynosave=array();
				$allproducts=array();
				$resultall=array();
				ob_start();
			while (($csv = fgetcsv($handle, 0, $delimeter)) !== false) {
				 
				$rows ++;
				$data = $gallery = array();
				
				foreach ($keys as $k => $v) {
					
					if (isset($data[$v]) && !is_array($data[$v])) {
						$data[$v] = array($data[$v], $csv[$k]);
					}
					elseif (isset($data[$v]) && is_array($data[$v])) {
						$data[$v][] = $csv[$k];
					}
					else {
						$data[$v] = $csv[$k];
					}
				}
				/*echo '<pre>';
				print_r($data);
				echo '</pre>';
				echo '<pre>';
				print_r($csv);
				echo '</pre>';*/
				if ($i>0){
					$dataall[]=$data;
					
					$skusdata[]=$data['artpost'];//strtolower();
					$jsonatr=$data["artpost"];
					
					if ((!isset($data['artpost']))||($data['artpost']=='')) {$errors[]="- Ошибка в строке ".$rows;continue;}
					if ((!isset($data['name']))||($data['name']=='')) {$errors[]="- Ошибка в строке ".$rows;continue;}
					if ((!isset($data['price_supplier']))||($data['price_supplier']=='')) {$errors[]="- Ошибка в строке ".$rows;continue;}
									
					$q = $modx->newQuery('modTemplateVarResource');
					$q->innerJoin('msProductData', 'Data', 'modTemplateVarResource.contentid = Data.id');
					$q->innerJoin('msVendor', 'Vendor', 'Data.vendor = Vendor.id');
								
								
					$q->select('*');
					
					$q->where(array('Vendor.post'=>'mar','tmplvarid' => 1,'value:LIKE'=>'%"artpost":"'.$jsonatr.'",%'));//$data["artpost"]
					
					$q->prepare();					
					$q->stmt->execute();
					$sql = $q->toSQL();
					$result1 = $q->stmt->fetchAll(PDO::FETCH_ASSOC);	
					$id_product=false;
					$changeprice=array();
					$publishedprice=array();
					//echo 'count'.$jsonatr.' '.count($result1).'<br/>';
					
					if ($result1&&(count($result1)>0))
					{
						
						
						$brandexist=false;
						foreach ($result1 as $res)
						{
							$id_product=$res['modTemplateVarResource_contentid'];
							
							$id_object=$res['modTemplateVarResource_id'];
							if ($id_product)
							{					
											/*$product_save = $modx->getObject('msProduct',$productid);
							$product_save->set('published',1);
							$product_save->save();	*/	
								$q = $modx->newQuery('msProduct');
								$q->select('msProduct.id');
								$q->innerJoin('msProductData', 'Data', 'msProduct.id = Data.id');
								$q->innerJoin('msVendor', 'Vendor', 'Data.vendor = Vendor.id');
								$q->select('*');
								$q->where(array('msProduct.id'=>$id_product,'Vendor.post'=>'mar'));
								//$q->where(array('tmplvarid' => 1,'value:LIKE'=>'%"artpost":"'.$data["artpost"].'",%'));
								$q->prepare();					
								$q->stmt->execute();
								//echo $id_product.' '.$data["brand"].'<br/>';
								$result2 = $q->stmt->fetch(PDO::FETCH_ASSOC);	
								//echo 'sdss'.count($result2).'<br/>';
								if ($result2&&(count($result2)>0)) // нашли данный товар данного бренда
								{
									$brandexist=true;
									
									// формируем массив для обновления
									$options_old=$modx->fromJSON($res['modTemplateVarResource_value']);//, true);
									$options=$options_old;
									
									foreach ($options_old as $key=>$opt)
									{
								//	echo $opt['artpost'].' '.$data["artpost"].'<br/>';
										if ($opt['artpost']==$data["artpost"]){
												
											if ($options[$key]["price_supplier"]!=$data["price_supplier"])
											{// цена изменилась												
												$changpr++;
												/*$markup=$data["price_supplier"]-$options[$key]["price_supplier"];
												$markup1=(float)($markup*100)/$options[$key]["price_supplier"];
												$markup1=abs($markup1);
												//echo $markup1."\r\n";
												if ($markup1>25)$change20[]=$data['artpost'];//(($markup<(-20)||($markup>20))
													*/
												$options[$key]["price_supplier"]=$data["price_supplier"];
												if ($options[$key]['artpost']!=''){
													$options[$key]["instock"]=1;
													
												
												
													if ($result2['msProduct_published']==0) // т.е. цена изменилась и товар был неопубликован то необходимо опубликовать товар
													{
														$product_save = $modx->getObject('msProduct',$id_product);
														$product_save->set('published',1);
														$product_save->save();
														//$publishedprice[]=$data;	
														//$publishedprice[]=$data['artpost'];	
													}
													$changeprice[]=$data;	
												}												
											}else{//выведем где не изменилась
												if ($options[$key]['artpost']!=''){
													$options[$key]["instock"]=1;
													
														if ($result2['msProduct_published']==0) // т.е. цена изменилась и товар был неопубликован то необходимо опубликовать товар
														{
															$product_save = $modx->getObject('msProduct',$id_product);
															$product_save->set('published',1);
															$product_save->save();
															//$publishedprice[]=$data['artpost'];	
															
														}
													$nochange[]=$data;
												}
											}
											
											if (isset($data["price_site"]))
												{
													if (($data["price_site"]!='')&&($data["price_site"]!=0))
													{
														$options[$key]["price"]=$data["price_site"];
													}
													elseif($data["price_site"]==0) $options[$key]["price"]='';
													
												
												}
											
											$num++;
										}
									}									
									$optionsar=$modx->toJSON($options);
									$option_save = $modx->getObject('modTemplateVarResource',array('id'=>$id_object,'contentid'=>$id_product));
                                    $product_save = $modx->getObject('msProduct',$id_product);
                                    $product_save->set('editedon',time());
                                    $product_save->save();


									if ($option_save){
										$option_save->set('value',$optionsar);//$option);
										if ($option_save->save()) $arraysave[]=$data;
										else $arraynosave[]=$data['artpost'];										
									}else $arraynosave[]=$data['artpost'];
								}else $arraynosave[]=$data['artpost'];
							}
						}
						
					}else{// товара нет в магазине, ндао отобразить в отчёте
						//if ($options[$key]['artpost']!='')
						$arraynosave[]=$data['artpost'];//array_merge($res,array('nosave'=>'Нет в магазине. Надо добавить'));
					}
					
					
				}
				$brand=$data["brand"];
				$allproducts[]=$data['artpost'];
				$i++;
				  flush();
    ob_flush();
    sleep(1);
			}
			ob_end_flush();
			
			$noart=$noartid=array();
				$q = $modx->newQuery('modTemplateVarResource');
					$q->innerJoin('msProductData', 'Data', 'modTemplateVarResource.contentid = Data.id');
					$q->innerJoin('msVendor', 'Vendor', 'Data.vendor = Vendor.id');
					$q->select('*');
					$q->where(array('Vendor.post'=>'mar','tmplvarid' => 1));//$data["artpost"]					
					$q->prepare();					
					$q->stmt->execute();
					$sql = $q->toSQL();
					$result1 = $q->stmt->fetchAll(PDO::FETCH_ASSOC);	
					$id_product=false;
					//$changeprice=array();
					//$publishedprice=array();
					
					if ($result1&&(count($result1)>0))
					{
						$brandexist=false;
						foreach ($result1 as $res)
						{
							$id_product=$res['modTemplateVarResource_contentid'];
							
							$id_object=$res['modTemplateVarResource_id'];
							$options_old=$modx->fromJSON($res['modTemplateVarResource_value']);//, true);
							$options=$options_old;
							foreach ($options as $key=>$opt)
							{
								if (!in_array($opt['artpost'],$allproducts))
								{
									$noart[]=$opt['artpost'];
									$noartid[]=$res['modTemplateVarResource_contentid'];
									if ($opt['artpost']!='') $options[$key]["instock"]=0;
									
								}else{
									if ($opt['artpost']!='') $options[$key]["instock"]=1;
								}
							}
							
							$optionsar=$modx->toJSON($options);
										$option_save = $modx->getObject('modTemplateVarResource',array('id'=>$id_object,'contentid'=>$id_product));

                            $product_save = $modx->getObject('msProduct',$id_product);
                            $product_save->set('editedon',time());
                            $product_save->save();


										if ($option_save){
											$option_save->set('value',$optionsar);//$option);
											if ($option_save->save()) $arraysave[]=$data;
											else $arraynosave[]=$data['artpost'];										
										}else $arraynosave[]=$data['artpost'];
						}
					}
					/*if(count($noartid)>0)
					{
						foreach ($noartid as $key=>$productid)
						{
							$product_save = $modx->getObject('msProduct',$productid);
							$product_save->set('published',0);
							$product_save->save();
						}
					}*/
			
			
			fclose($handle);
			
			$file='/assets/reportimport/bob_report'.time().'.txt';
			$filenos=$_SERVER['DOCUMENT_ROOT'].$file;
			$handle = fopen($filenos, "w+");//$_SERVER['DOCUMENT_ROOT'].'/'.
			
			$reporttxt="Дата:".date('Y-d-m H:m:s')."\r\n\r\n";
			if(count($arraynosave)>0){
				//$reporttxt.="\r\n\r\n- На сайте не найдены товары с артикулами:\r\n";				
				//$reporttxt.=implode("\r\n",$arraynosave);//$ar['artpost'].", ";	
				
			}
			
			if(count($errors)>0){
				$reporttxt.="\r\n\r\n";
				/*foreach ($errors as $ar)
				{
					$reporttxt.=$ar."\r\n";	
				}*/
				$reporttxt.=implode("\r\n",$errors);
				
			}
			
			if(count($arraysave)>0){
				$reporttxt.="\r\n\r\nУспешное сохранение\r\n";
				$reporttxt.="\r\n- Цена изменена у ".$changpr." товаров из ".$num."\r\n";
				
				
				if (count($changeprice)>0){
					$reporttxt.="\r\n- Цена изменена:\r\n";
					foreach ($changeprice as $no)	
					{
						//" у ".$changpr." товаров из ".$num;
						$reporttxt.=$no['artpost']."\r\n";
						
					}
					$reporttxt.="\r\n";
				}
				
				
				
				
				if (count($change20)>0){
					$reporttxt.="\r\n- Цена изменилась более чем на 25% для товаров с артикулами: \r\n";
					
					$reporttxt.=implode("\r\n",$change20);
					$reporttxt.="\r\n";
				}
				
			}
				
				//получим все товары чтобы проверить каких нет в прайсе - т.е. их нет в наличии
			//if ($brand!=''){
				$q = $modx->newQuery('msProduct');
								$q->select('msProduct.id');
								$q->innerJoin('msProductData', 'Data', 'msProduct.id = Data.id');
								$q->innerJoin('msVendor', 'Vendor', 'Data.vendor = Vendor.id');
								$q->innerJoin('modTemplateVarResource', 'TV', 'msProduct.id = TV.contentid AND TV.tmplvarid= 1');
								
								$q->select('msProduct.id,TV.value,msProduct.published,msProduct.pagetitle,Vendor.name');
								$q->where(array('Vendor.post'=>'mar'));
								$q->groupby('msProduct.id');
								//$q->where(array('tmplvarid' => 1,'value:LIKE'=>'%"artpost":"'.$data["artpost"].'",%'));
								$q->prepare();					
								$q->stmt->execute();
								$result2 = $q->stmt->fetchAll(PDO::FETCH_ASSOC);	
								if ($result2&&(count($result2)>0)) // нашли данный товар данного бренда
								{
									
									$skus=$notproduct=array();
									foreach ($result2 as $res)
									{
										$tv=$modx->fromJSON($res['value']);
										//if ($res['published']==1){
												$productid=$res['id'];
											$skusoutstock=$skusinstock=array();
											foreach ($tv as $t)
											{
												if ($t['artpost']!=''){
													if ($t['instock']==1) {
														$skusinstock[]=$t['artpost'];
														$publishedprice[]=$t['artpost'];
													}else{
														$skusoutstock[]=$t['artpost'];
														$noart[]=$t['artpost'];
													}
													$skus[]=$t['artpost'];
												}
												
											}
											/*echo '<pre>$tv ';
											print_r($tv );
											echo '</pre>';
											echo '<pre>$skusinstock';
											print_r($skusinstock);
											echo '</pre>';
											
											echo '<pre>$skusoutstock';
											print_r($skusoutstock);
											echo '</pre>';*/
											/*if ($productid==2850)
											{
												echo 	$productid.'<pre>$tv ';
											print_r($res );
											echo '</pre>';
											echo '<pre>$skusinstock';
											print_r($skusinstock);
											echo '</pre>';
											
											echo '<pre>$skusoutstock';
											print_r($skusoutstock);
											echo '</pre><br/>';
											}*/
											
											if ((count($skusoutstock)>0)&&(count($skusoutstock)==count($tv))&&(count($skusinstock)==0)) // начит все опции отсутствуют
												{
												
													
													$product_save = $modx->getObject('msProduct',$productid);
													$product_save->set('published',0);
													$product_save->save();
												}else{
													//$productid=$res['id'];
													$product_save = $modx->getObject('msProduct',$productid);
													$product_save->set('published',1);
													$product_save->save();
													
												}
										//}
									}
									if (count($skus)>0){//$dataall
										foreach ($skus as $sku)
										{
											if (!in_array($sku,$skusdata))
											{
												if ($sku!='')$notproduct[]=$sku;//$data['artpost'];
											}
										}
										
										if (count($notproduct)>0){											
											//$reporttxt.="\r\n\r\n- В прайсе отсутствуют товары с артикулами:\r\n ".implode("\r\n",$notproduct);
										}
										
									}else{
										$reporttxt.="\r\n\r\n-Товаров нет : \r\n";
										$reporttxt.="\r\n";
									}
								}
								
								
								if (count($publishedprice)>0){
					$reporttxt.="\r\n\r\n- Включенные товары:\r\n";
					foreach ($publishedprice as $no)	
					{
						//" у ".$changpr." товаров из ".$num;
						$reporttxt.=$no."\r\n";
						
					}
					$reporttxt.="\r\n";
				}
				
				
								if (count($noart)>0){
					$reporttxt.="\r\n- Выключенные товары:\r\n";
					foreach ($noart as $no)	
					{
						//" у ".$changpr." товаров из ".$num;
						$reporttxt.=$no."\r\n";
						
					}
					$reporttxt.="\r\n";
				}
				//}
			
			fwrite($handle,header('Content-Type: text/html; charset=utf8'));
			fwrite($handle, $reporttxt);
			fclose($handle);			
		return '<a href="http://petchoice.com.ua'.$file.'" target="_blank">Скачать отчёт</a>';	 
 }


 function import($file,$delimeter,$keys){
	 global $modx;
	 $brand='';
			
			$handle = fopen($_SERVER['DOCUMENT_ROOT'].'/'.$file, "r");
			$rows = $created = $updated = 0;
			
			$errors=$skusdata=array();
			
			$num=$changpr=0;
			$change20=array();
			$i=0;
				$arraysave=$arraynosave=array();
     $nochange=[];


			while (($csv = fgetcsv($handle, 0, $delimeter)) !== false) {
				
				$rows ++;
				$data = $gallery = array();
				
				foreach ($keys as $k => $v) {
					
					if (isset($data[$v]) && !is_array($data[$v])) {
						$data[$v] = array($data[$v], $csv[$k]);
					}
					elseif (isset($data[$v]) && is_array($data[$v])) {
						$data[$v][] = $csv[$k];
					}
					else {
						$data[$v] = $csv[$k];
					}
				}
				if ($i>0){
					$dataall[]=$data;
					
					
					$data["artpost"]=str2url($data["artpost"]);

                    $data["artman"]=str2url($data["artman"]);
					$skusdata[]=strtolower($data['artpost']);


					
					if ((!isset($data['brand']))||($data['brand']=='')) {$errors[]="- Ошибка в строке ".$rows;continue;}


//					if ((!isset($data['artpost']))||($data['artpost']=='')) {$errors[]="- Ошибка в строке ".$rows;continue;}
//
//                    if ((!isset($data['artpost']))&&(!isset($data['artman']))) {$errors[]="- Ошибка в строке ".$rows;continue;}

                    if ((isset($data['artpost']))&&(isset($data['artman']))) {

					    if ((empty(trim($data['artpost'])))&&(empty(trim($data['artman'])))) {
                            $errors[] = "- Ошибка в строке " . $rows;
                            continue;
                        }

					}



					if ((!isset($data['name']))||($data['name']=='')) {$errors[]="- Ошибка в строке ".$rows;continue;}
					if ((!isset($data['currency_supplier']))||($data['currency_supplier']=='')) {$errors[]="- Ошибка в строке ".$rows;continue;}//|| (gettype($res['currency_supplier'])!='integer')
					if ((!isset($data['price_supplier']))||($data['price_supplier']=='')) {$errors[]="- Ошибка в строке ".$rows;continue;}
					if ((!isset($data['markup']))||($data['markup']=='')) {$errors[]="- Ошибка в строке ".$rows;continue;}//|| (gettype($res['currency_supplier'])!='integer')

                    $artpostFromPrice=trim($data["artpost"]);
                    $artmanFromPrice=trim($data["artman"]);



                    $sqlWhere='%"artpost":"'.$artpostFromPrice.'",%';
                    if (!empty($artpostFromPrice)) {
                        $jsonatr = $artpostFromPrice;//$data["artpost"];
                        $sqlWhere='%"artpost":"'.$jsonatr.'",%';
                    }

                    if (!empty($artmanFromPrice)) {
                        $jsonatr = $artmanFromPrice;//$data["artman"];
                        $sqlWhere='%"artman":"'.$jsonatr.'",%';
                    }

					$q = $modx->newQuery('modTemplateVarResource');
					$q->innerJoin('msProductData', 'Data', 'modTemplateVarResource.contentid = Data.id');
								$q->innerJoin('msVendor', 'Vendor', 'Data.vendor = Vendor.id');
								
								
					$q->select('*');
					
					$q->where(array('Vendor.name'=>$data["brand"],'tmplvarid' => 1,'value:LIKE'=>$sqlWhere));//$data["artpost"]
					
					$q->prepare();					
					$q->stmt->execute();
					$sql = $q->toSQL();

//                    echo  '<pre>$artpostFromPrice='.$artpostFromPrice.' $artmanFromPrice='.$artmanFromPrice;
//                    print_r($data);
//                    echo '</pre>';
//
//					echo  $sql;
//                    die();
					$result1 = $q->stmt->fetchAll(PDO::FETCH_ASSOC);	
					$id_product=false;
					$changeprice=array();


					if ($result1&&(count($result1)>0))
					{


						$brandexist=false;
						foreach ($result1 as $res)
						{
							$id_product=$res['modTemplateVarResource_contentid'];
							
							$id_object=$res['modTemplateVarResource_id'];
							if ($id_product)
							{								
								$q = $modx->newQuery('msProduct');
								$q->select('msProduct.id');
								$q->innerJoin('msProductData', 'Data', 'msProduct.id = Data.id');
								$q->innerJoin('msVendor', 'Vendor', 'Data.vendor = Vendor.id');
								$q->select('*');
								$q->where(array('msProduct.id'=>$id_product,'Vendor.name'=>$data["brand"]));
								//$q->where(array('tmplvarid' => 1,'value:LIKE'=>'%"artpost":"'.$data["artpost"].'",%'));
								$q->prepare();					
								$q->stmt->execute();
								//echo $id_product.' '.$data["brand"].'<br/>';
								$result2 = $q->stmt->fetch(PDO::FETCH_ASSOC);	
								if ($result2&&(count($result2)>0)) // нашли данный товар данного бренда
								{
									$brandexist=true;
									
									// формируем массив для обновления
									$options_old=$modx->fromJSON($res['modTemplateVarResource_value']);//, true);
									$options=$options_old;
									
									foreach ($options_old as $key=>$opt)
									{
									    if (!empty($artpostFromPrice)) {
                                            if (strtolower($opt['artpost']) == strtolower($data["artpost"])) {
                                                list($options,$changpr,$changeprice,$nochange, $change20,$num)=updateOptionsFromPrice($options,$key,$data,$changpr,$result2,$changeprice,$nochange, $change20,$num,'artpost',$id_product);
                                            }
                                        }elseif(!empty($artmanFromPrice))
                                        {
                                            if (strtolower($opt['artman']) == strtolower($data["artman"])) {
                                                list($options,$changpr,$changeprice,$nochange, $change20,$num)=updateOptionsFromPrice($options,$key,$data,$changpr,$result2,$changeprice,$nochange, $change20,$num,'artman',$id_product);
                                            }
                                        }
									}									
									$optionsar=$modx->toJSON($options);
									$option_save = $modx->getObject('modTemplateVarResource',array('id'=>$id_object,'contentid'=>$id_product));


                                    $product_save = $modx->getObject('msProduct',$id_product);
                                    $product_save->set('editedon',time());
                                    $product_save->save();

                                    if ($option_save){
										$option_save->set('value',$optionsar);//$option);
										if ($option_save->save()) $arraysave[]=$data;
										else $arraynosave[]=$data['artpost'];										
									}else $arraynosave[]=$data['artpost'];
								}else $arraynosave[]=$data['artpost'];
							}
						}
						
					}else{// товара нет в магазине, ндао отобразить в отчёте
						$arraynosave[]=$data['artpost'];//array_merge($res,array('nosave'=>'Нет в магазине. Надо добавить'));
					}
					
					
				}
				$brand=$data["brand"];
				$i++;
			}
			fclose($handle);
			
			$file='/assets/reportimport/report'.time().'.txt';
			$filenos=$_SERVER['DOCUMENT_ROOT'].$file;
			$handle = fopen($filenos, "w+");//$_SERVER['DOCUMENT_ROOT'].'/'.
			
			$reporttxt="Дата:".date('Y-d-m H:m:s')."\r\n\r\n";
			if(count($arraynosave)>0){
				$reporttxt.="\r\n\r\n- На сайте не найдены товары с артикулами:\r\n";				
				$reporttxt.=implode("\r\n",$arraynosave);//$ar['artpost'].", ";
			}
			
			if(count($errors)>0){
				$reporttxt.="\r\n\r\n";
				/*foreach ($errors as $ar)
				{
					$reporttxt.=$ar."\r\n";	
				}*/
				$reporttxt.=implode("\r\n",$errors);
				
			}
			
			if(count($arraysave)>0){
				$reporttxt.="\r\n\r\nУспешное сохранение\r\n";
				$reporttxt.="\r\n- Цена изменена у ".$changpr." товаров из ".$num."\r\n";
				
				
				if (count($changeprice)>0){
					$reporttxt.="\r\n- Цена изменена:\r\n";
					foreach ($changeprice as $no)	
					{
						//" у ".$changpr." товаров из ".$num;
						$reporttxt.=$no['artpost']."\r\n";
						
					}
					$reporttxt.="\r\n";
				}
				
				if (count($change20)>0){
					$reporttxt.="\r\n- Цена изменилась более чем на 20% для товаров с артикулами: \r\n";
					
					$reporttxt.=implode("\r\n",$change20);
					$reporttxt.="\r\n";
				}
				
			}
				
				//получим все товары чтобы проверить каких нет в прайсе - т.е. их нет в наличии
			if ($brand!=''){
				$q = $modx->newQuery('msProduct');
								$q->select('msProduct.id');
								$q->innerJoin('msProductData', 'Data', 'msProduct.id = Data.id');
								$q->innerJoin('msVendor', 'Vendor', 'Data.vendor = Vendor.id');
								$q->innerJoin('modTemplateVarResource', 'TV', 'msProduct.id = TV.contentid');
								
								$q->select('msProduct.id,TV.value,msProduct.published,msProduct.pagetitle,Vendor.name');
								$q->where(array('Vendor.name'=>$data["brand"]));
								//$q->where(array('tmplvarid' => 1,'value:LIKE'=>'%"artpost":"'.$data["artpost"].'",%'));
								$q->prepare();					
								$q->stmt->execute();
								$result2 = $q->stmt->fetchAll(PDO::FETCH_ASSOC);	
								if ($result2&&(count($result2)>0)) // нашли данный товар данного бренда
								{
									
									$skus=$notproduct=array();
									foreach ($result2 as $res)
									{
										$tv=$modx->fromJSON($res['value']);
										if ($res['published']==1){
											foreach ($tv as $t)
											{
												if ($t['instock']==1) $skus[]=strtolower($t['artpost']);
											}
										}
									}
									if (count($skus)>0){//$dataall
										foreach ($skus as $sku)
										{
											if (!in_array($sku,$skusdata))
											{
												if ($sku!='')$notproduct[]=$sku;//$data['artpost'];
											}
										}
										
										if (count($notproduct)>0){											
											$reporttxt.="\r\n\r\n- В прайсе отсутствуют товары с артикулами:\r\n ".implode("\r\n",$notproduct);
										}
										
									}else{
										$reporttxt.="\r\n\r\n-Товаров нет : \r\n";
										$reporttxt.="\r\n";
									}
								}
				}


            //$reporttxt=iconv('windows1251','UTF-8',$reporttxt);
     $reporttxt=iconv('UTF-8','windows-1251',$reporttxt);

			fwrite($handle,header('Content-Type: text/html; charset=utf8'));
			fwrite($handle, $reporttxt);
			fclose($handle);			
		return '<a href="http://petchoice.com.ua'.$file.'" target="_blank">Скачать отчёт</a>';	 
 }


 function updateOptionsFromPrice($options,$key,$data,$changpr,$result2,$changeprice,$nochange, $change20,$num, $type,$id_product)
 {
     global $modx;
     if (($options[$key]["markup"] != $data["markup"]) || ($options[$key]["price_supplier"] != $data["price_supplier"])) {// цена изменилась
         $changpr++;
         $markup = $data["price_supplier"] - $options[$key]["price_supplier"];
         $markup1 = (float)($markup * 100) / $options[$key]["price_supplier"];
         $markup1 = abs($markup1);
         if ($markup1 > 20) $change20[] = $data[$type];
         $options[$key]["price_supplier"] = $data["price_supplier"];

         $options[$key]["instock"] = 1;


         if ($result2['msProduct_published'] == 0) // т.е. цена изменилась и товар был неопубликован то необходимо опубликовать товар
         {

             $product_save = $modx->getObject('msProduct', $id_product);
             $product_save->set('published', 1);
             $product_save->save();

         }
         //$product_save->set('editedon',time());

         $changeprice[] = $data;
     } else {//выведем где не изменилась
         $options[$key]["instock"] = 1;


         if ($result2['msProduct_published'] == 0) // т.е. цена изменилась и товар был неопубликован то необходимо опубликовать товар
         {
             $product_save = $modx->getObject('msProduct', $id_product);
             $product_save->set('published', 1);
             $product_save->save();
         }


         $nochange[] = $data;
     }

     if (isset($data["price_site"])) {
         if (($data["price_site"] != '') && ($data["price_site"] != 0)) {
             $options[$key]["price"] = $data["price_site"];
         } elseif ($data["price_site"] == 0) $options[$key]["price"] = '';
     }

     $options[$key]["markup"] = $data["markup"];
     $options[$key]["currency_supplier"] = $data["currency_supplier"];
     $num++;

     return [$options,$changpr,$changeprice,$nochange, $change20,$num];
 }
 
 /*
$isLimit = !empty($_REQUEST['limit']);
$start = $modx->getOption('start',$_REQUEST,0);
$limit = $modx->getOption('limit',$_REQUEST,20);
$sort = $modx->getOption('sort',$_REQUEST,'name');
$dir = $modx->getOption('dir',$_REQUEST,'ASC');

$c = $modx->newQuery('modExtraItem');
$count = $modx->getCount('modExtraItem',$c);

$c->sortby($sort,$dir);
if ($isLimit) $c->limit($limit,$start);
$items = $modx->getCollection('modExtraItem',$c);

$list = array();
foreach ($items as $item) {
    $itemArray = $item->toArray();
    $list[]= $itemArray;
}
return $this->outputArray($list,$count);*/