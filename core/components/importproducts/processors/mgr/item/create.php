<?php
/**
 * modExtra
 *
 * Copyright 2010 by Shaun McCormick <shaun+modextra@modx.com>
 *
 * modExtra is free software; you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 2 of the License, or (at your option) any later
 * version.
 *
 * modExtra is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * modExtra; if not, write to the Free Software Foundation, Inc., 59 Temple
 * Place, Suite 330, Boston, MA 02111-1307 USA
 *
 * @package modextra
 */
/**
 * Create an Item
 * 
 * @package modextra
 * @subpackage processors
 */
/*$alreadyExists = $modx->getObject('modExtraItem',array(
    'name' => $_POST['name'],
));
if ($alreadyExists) {
    $modx->error->addField('name',$modx->lexicon('modextra.item_err_ae'));
}

if ($modx->error->hasError()) {
    return $modx->error->failure();
}

$item = $modx->newObject('modExtraItem');
$item->fromArray($_POST);

if ($item->save() == false) {
    return $modx->error->failure($modx->lexicon('modextra.item_err_save'));
}

return $modx->error->success('',$item);*/




		function rus2translit($string) {
			$converter = array(
				'а' => 'a',   'б' => 'b',   'в' => 'v',
				'г' => 'g',   'д' => 'd',   'е' => 'e',
				'ё' => 'e',   'ж' => 'zh',  'з' => 'z',
				'и' => 'i',   'й' => 'y',   'к' => 'k',
				'л' => 'l',   'м' => 'm',   'н' => 'n',
				'о' => 'o',   'п' => 'p',   'р' => 'r',
				'с' => 's',   'т' => 't',   'у' => 'u',
				'ф' => 'f',   'х' => 'h',   'ц' => 'c',
				'ч' => 'ch',  'ш' => 'sh',  'щ' => 'sch',
				'ь' => '\'',  'ы' => 'y',   'ъ' => '\'',
				'э' => 'e',   'ю' => 'yu',  'я' => 'ya',
				
				'А' => 'A',   'Б' => 'B',   'В' => 'V',
				'Г' => 'G',   'Д' => 'D',   'Е' => 'E',
				'Ё' => 'E',   'Ж' => 'Zh',  'З' => 'Z',
				'И' => 'I',   'Й' => 'Y',   'К' => 'K',
				'Л' => 'L',   'М' => 'M',   'Н' => 'N',
				'О' => 'O',   'П' => 'P',   'Р' => 'R',
				'С' => 'S',   'Т' => 'T',   'У' => 'U',
				'Ф' => 'F',   'Х' => 'H',   'Ц' => 'C',
				'Ч' => 'Ch',  'Ш' => 'Sh',  'Щ' => 'Sch',
				'Ь' => '\'',  'Ы' => 'Y',   'Ъ' => '\'',
				'Э' => 'E',   'Ю' => 'Yu',  'Я' => 'Ya',
			);
			return strtr($string, $converter);
		}
		function str2url($str) {
			// переводим в транслит
			$str = rus2translit($str);
			// в нижний регистр
			$str = strtolower($str);
			// заменям все ненужное нам на "-"
			$str = preg_replace('~[^-a-z0-9_]+~u', '-', $str);
			// удаляем начальные и конечные '-'
			$str = trim($str, "-");
			return $str;
		}


						
		$q = $modx->newQuery('msProduct');
		$q->where(array('class_key' => 'msProduct'));//,'published' => 1,'deleted' => 0));//'parent:IN' => $pids,
		//$q->select('`msProduct`.`id`');
		if ($q->prepare() && $q->stmt->execute()) {
			$ids = $q->stmt->fetchAll(PDO::FETCH_COLUMN);
		}
		$i=0;
		foreach ($ids as $id)
		{
			$prices=array();
			$price=0;
			$product=$modx->getObject('msProduct',$id);
			// надо получить опции
			$tv_id=1;
			$options=array();
			$q = $modx->newQuery('modTemplateVar');
			$q->innerJoin('modTemplateVarResource', 'v', 'v.tmplvarid = modTemplateVar.id');
			$q->where(array(
				'v.contentid'  =>$id, 
				'modTemplateVar.id'  => $tv_id, 
			));
			$q->limit(1);
			$q->select(array('modTemplateVar.*', 'v.value'));
			$value=array();
			$array=array();
			if ($q->prepare() && $q->stmt->execute()) {
				while ($tv = $q->stmt->fetch(PDO::FETCH_ASSOC)) {
					$value =  $tv['value'];
				}
			}
			
			// Получаем массив из JSON-строки
			if (count($value)>0)  $options = $modx->fromJSON($value);
			$options1=array();
			foreach ($options as $option)
			{								
				$artpost=$option['artpost'];
				if(preg_match('/[^0-9a-zA-Z]/', $artpost)){
					$artpost=str2url($artpost);
					$option['artpost']=$artpost;					
				} 
				
				$options1[]=$option;
				
			}
			$tvvar = $modx->getObject('modTemplateVarResource',array('tmplvarid'=>1,'contentid'=>$id));
			if ($tvvar)
			{
				$tvvar->set('value',$modx->toJSON($options1));
				$tvvar->save();
			}
			
			$i++;
		}
		