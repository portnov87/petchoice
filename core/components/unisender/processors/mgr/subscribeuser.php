<?php
//error_reporting(E_ALL | E_STRICT);
//ini_set('display_errors', 1);

$modx->setDebug(E_ALL & ~E_NOTICE); // sets error_reporting to everything except NOTICE remarks
$modx->setLogLevel(modX::LOG_LEVEL_DEBUG);
$modx->log(modX::LOG_LEVEL_DEBUG, 'SubscribeUser active');


if (empty($scriptProperties['list_id'])) {
	$modx->error->addField('list_id', $modx->lexicon('Укажите список рассылки'));
}

if ($modx->error->hasError()) {
	return $modx->error->failure();
}

/*
$sp='';
foreach($scriptProperties as $key=>$val){
	$sp.='['.$key.']='.$val.' ';
}*/


function get_subscribe_type($subscr_sms, $subscr_email)
{ //Выдает цифровой код для типа подписки
	if ($subscr_sms == 'on' && $subscr_email == 'on')
		return 3;
	if ($subscr_sms == 'on' && $subscr_email == '')
		return 2;
	if ($subscr_sms == '' && $subscr_email == 'on')
		return 1;
	if ($subscr_sms == '' && $subscr_email == '')
		return 0;
}

function unsubscribe_phone($phone)
{ //Отписывает телефон

}


$api_path = $this->modx->getOption('unisender.api_path');
require_once($api_path . 'User.php');
require_once($api_path . 'ListBook.php');
$api_key = $this->modx->getOption('unisender_api');


$usersIdArr = array();
if ($scriptProperties['users'] != '')//Много юзеров
{
	$usersIdArr = explode(',', $scriptProperties['users']);

} else {
	$usersIdArr[] = $scriptProperties['id']; //Для 1 юзера

}

$list_id = $scriptProperties['list_id'];

$listArr = array();
if ($list_id == -2) //Подписка на все списки
{
	$c = $modx->newQuery('UnisenderLists');
	$all_lists = $modx->getIterator('UnisenderLists', $c);
	foreach ($all_lists as $ulist)
		$listArr[] = $ulist->get('unisender_id');
} else {
	$listArr[] = $list_id;
}


// Лимит в 500 записей
$usersIdArr_500 = array_chunk($usersIdArr, 500);
foreach ($usersIdArr_500 as $usersIdArr) {
	$data = array();
	foreach ($usersIdArr as $user_id) {

		$c = $modx->newQuery('UnisenderSubscribers');
		$c->where(array(
			'user_id' => $user_id
		));
		$current_subscribtions = $modx->getIterator('UnisenderSubscribers', $c); //Смотрим сначала все его подписки
		$subscribtionArr = array();

		/*echo '<pre>';
		print_r($current_subscribtions);
		echo '</pre>';*/

		//Получаем всё по юзеру
		$user = $modx->getObject('modUser', $user_id);
		$profile = $user->getOne('Profile');

		foreach ($listArr as $ulist_id) { //Идем по всем спискам, на которые надо подписаться

$cats=array();
			$subscribed = false; //Сначала предполагаем что он ни на 1 список не подписан
			foreach ($current_subscribtions as $subscribtion_obj) {
				//echo $subscribtion_obj->list_id.' '.
				if ($subscribtion_obj->list_id == $ulist_id) {//Если нужный нам список у него уже есть

					$user_obj = new User($api_key);//Unisender User

					//Обновляем телефон и мыло
					$phone = $profile->get('phone');
					$email = $profile->get('email');
					$date_registr = $user->get('created');
					$registr= $user->get('registr');
					if ($date_registr=='-1-11-30 00:00:00') $date_registr= date('Y-m-d H:i:s');
					//if ($registr==0)$registr='Нет';
					//else $registr='Да';
					$podpiska= $user->get('subscribe');
					
					$user = $modx->newQuery('msOrder', $user_id);
					
					$q = $modx->newQuery('msOrder');
					$q->where(array(
						'user_id' => $user_id
					));
				$q->sortby('createdon', 'DESC');
					//$q->limit(10);    // Добавим лимит записей

					$q->prepare();
					$sql = $q->toSQL();

					$query = $modx->prepare($sql);
					$query->execute();

					$result = $query->fetchAll(PDO::FETCH_ASSOC);
					$total_order=0;$categories='';
					$last_order=$result[0]['msOrder_createdon'];
					$resultcat=array();
					$counts=array();
					foreach ($result as $order)
					{
						$total_order+=$order['msOrder_cost'];
						
						
						$q = $modx->newQuery('msOrderProduct');
								$q->where(array(
									'order_id' => $order['msOrder_id']
								));
								$q->prepare();
								$sql = $q->toSQL();

								$query = $modx->prepare($sql);
								$query->execute();

								$result_products = $query->fetchAll(PDO::FETCH_ASSOC);
								
								foreach ($result_products as $product)
								{
									$productid=$product['msOrderProduct_product_id'];									
									$resultcat=array();
																	
									$q = $modx->newQuery('msCategory');									
									$q->leftJoin('msProduct', 'msProduct', array(
										'`msCategory`.`id` = `msProduct`.`parent`'
									));
									$q->sortby('msCategory.pagetitle','ASC');
									$q->select(array('msCategory.id','msCategory.parent','msCategory.pagetitle'));
									$q->where('`msProduct`.`id` = '.$productid);
									$q->prepare();
									$sql = $q->toSQL();
									$result = array();
											if ($q->prepare() && $q->stmt->execute()) {
												while ($row = $q->stmt->fetch(PDO::FETCH_ASSOC)) {
													$cat=false;
													if ($row['parent']!=10){
													$cat = $modx->getObject('msCategory',$row['parent']);														
													
													}
													
													if ($cat)$name=$cat->get('pagetitle').'.'.$row['pagetitle'];
													else $name=$row['pagetitle'];
													
													
													if ($cat)$resultcat[]=$cat->get('pagetitle');
													
													if (isset($counts[$name]))$counts[$name]=$counts[$name]+$product['msOrderProduct_count'];
													else $counts[$name]=1;
													$resultcat[] = $row['pagetitle'];//.'('.$counts[$row['pagetitle']].')';
													$cats[$name]=implode('-',$resultcat).'('.$counts[$name].')';
												}
											}
											
											
								}
								
								
								
								
					}
					$categories=implode("\r\n",$cats);
					

					if ($phone == '') $scriptProperties['subscribe_sms'] = ''; //Если нет телефона, нельзя подписать на SMS


					$email_subscribed_list_id = $ulist_id;
					$phone_subscribed_list_id = $ulist_id;

					$type = get_subscribe_type(
						!empty($scriptProperties['subscribe_sms']) ? $scriptProperties['subscribe_sms'] : '',
						!empty($scriptProperties['subscribe_email']) ? $scriptProperties['subscribe_email'] : ''
					);
//echo $type.'<br/>';
					switch ($type) { //Меняем тип подписки на юнисендере
						case 0:
							$phone_subscribed_list_id = 0;
							/*if ($phone != '') {
								$list_res = $user_obj->exclude('phone', $phone, array($ulist_id));
								if ($list_res['error'] == 1)
									return $modx->error->failure($modx->lexicon('Ошибка изменения подписки:' . $list_res['text']));

								$phone = '';
							}*/

							$email_subscribed_list_id = 0;
							if ($email != '') {
								$list_res = $user_obj->exclude('email', $email, array($ulist_id));
								if ($list_res['error'] == 1)
									return $modx->error->failure($modx->lexicon('Ошибка изменения подписки:' . $list_res['text']));

								$email = '';
							}

							$subscribtion_obj->remove(); //Удаляем из базы
							break;
						case 1:
							$phone_subscribed_list_id = 0;
							//if ($phone != '')
							//	$list_res = $user_obj->exclude('phone', $phone, array($ulist_id)); //Убираем телефон из подписки
							//$phone = '';
							break;
						case 2:
							$email_subscribed_list_id = 0;
							$list_res = $user_obj->exclude('email', $email, array($ulist_id)); //Убираем мыло из подписки
							$email = '';
							break;
						case 3:
							break;
					}

					/*echo '<pre>';
					print_r($list_res);
					echo '</pre>';*/
					/*if (empty($lists_res) || $list_res['error'] == 1) {
						return $modx->error->failure($modx->lexicon('Ошибка изменения подписки:' . $list_res['text']));
					}*/

					$data[] = array($email, $email_subscribed_list_id, $phone, $phone_subscribed_list_id, $profile->get('fullname'),$total_order,$date_registr,$last_order,$registr,$podpiska,$user_id,$categories);

					//Меняем тип подписки в базе
					$subscribtion_obj->set('subscribe_type', get_subscribe_type($scriptProperties['subscribe_sms'], $scriptProperties['subscribe_email']));


					if ($subscribtion_obj->save() == false) {
						return $modx->error->failure($modx->lexicon("Ошибка при сохранении подписки!"));
					}

					$subscribtion = $subscribtion_obj;

					$subscribed = true;
					break;

				}
			}


			if ($subscribed == false)//Не нашелся, создаем новую запись
			{
				$subscription = $modx->newObject('UnisenderSubscribers');
				$subscription->list_id = $ulist_id;
				$subscription->user_id = $user_id;

				//Обновляем телефон и мыло
				$phone = $profile->get('phone');
				$email = $profile->get('email');
				$id_cms=$user_id;
				$phone = $profile->get('phone');
					$email = $profile->get('email');
					$date_registr = $user->get('created');
					if ($date_registr=='-1-11-30 00:00:00') $date_registr= date('Y-m-d H:i:s');
					$registr= $user->get('registr');
					//if ($registr==0)$registr='Нет';
					//else $registr='Да';
					$podpiska= $user->get('subscribe');
					
					$user = $modx->newQuery('msOrder', $user_id);
					$categories='';
					$q = $modx->newQuery('msOrder');
					$q->where(array(
						'user_id' => $user_id
					));
				$q->sortby('createdon', 'DESC');
					//$q->limit(10);    // Добавим лимит записей

					$q->prepare();
					$sql = $q->toSQL();

					$query = $modx->prepare($sql);
					$query->execute();

					$result = $query->fetchAll(PDO::FETCH_ASSOC);
					$total_order=0;
					$last_order=$result[0]['msOrder_createdon'];
					
						foreach ($result as $order)
					{
						$total_order+=$order['msOrder_cost'];
						
						
						$q = $modx->newQuery('msOrderProduct');
								$q->where(array(
									'order_id' => $order['msOrder_id']
								));
								$q->prepare();
								$sql = $q->toSQL();

								$query = $modx->prepare($sql);
								$query->execute();

								$result_products = $query->fetchAll(PDO::FETCH_ASSOC);
								
								foreach ($result_products as $product)
								{
									$productid=$product['msOrderProduct_product_id'];									
									$resultcat=array();
																	
									$q = $modx->newQuery('msCategory');									
									$q->leftJoin('msProduct', 'msProduct', array(
										'`msCategory`.`id` = `msProduct`.`parent`'
									));
									$q->sortby('msCategory.pagetitle','ASC');
									$q->select(array('msCategory.id','msCategory.parent','msCategory.pagetitle'));
									$q->where('`msProduct`.`id` = '.$productid);
									$q->prepare();
									$sql = $q->toSQL();
									$result = array();
											if ($q->prepare() && $q->stmt->execute()) {
												while ($row = $q->stmt->fetch(PDO::FETCH_ASSOC)) {
													$cat=false;
													if ($row['parent']!=10){
													$cat = $modx->getObject('msCategory',$row['parent']);														
													
													}
													
													if ($cat)$name=$cat->get('pagetitle').'.'.$row['pagetitle'];
													else $name=$row['pagetitle'];
													
													
													if ($cat)$resultcat[]=$cat->get('pagetitle');
													
													if (isset($counts[$name]))$counts[$name]=$counts[$name]+$product['msOrderProduct_count'];
													else $counts[$name]=1;
													$resultcat[] = $row['pagetitle'];//.'('.$counts[$row['pagetitle']].')';
													$cats[$name]=implode('-',$resultcat).'('.$counts[$name].')';
												}
											}
											
											
								}
								
								
								
								
					}
					$categories=implode("\r\n",$cats);
					
					
				
				if ($phone == '') $scriptProperties['subscribe_sms'] = ''; //Если нет телефона, нельзя подписать на SMS
				$subscription->subscribe_type = get_subscribe_type($scriptProperties['subscribe_sms'], $scriptProperties['subscribe_email']);


				$email_subscribed_list_id = $ulist_id;
				$phone_subscribed_list_id = $ulist_id;

				switch ($subscription->subscribe_type) {
					case 0:
						$phone_subscribed_list_id = 0;
						/*if ($phone != '') {
							$list_res = $user_obj->exclude('phone', $phone, array($ulist_id));
							if ($list_res['error'] == 1) {
								return $modx->error->failure($modx->lexicon('Ошибка изменения подписки:' . $list_res['text']));
							}
							$phone = '';
						}*/
						$email_subscribed_list_id = 0;
						$list_res = $user_obj->exclude('email', $email, array($ulist_id));
						$email = '';
						break;
					case 1:
						//$phone = '';
						//$phone_subscribed_list_id = 0;
						break;
					case 2:
						$email = '';
						$email_subscribed_list_id = 0;
						break;
					case 3:
						break;
				}
//$registr='Да';
					//$podpiska

				$data[] = array($email, $email_subscribed_list_id, $phone, $phone_subscribed_list_id, $profile->get('fullname'),$total_order,$date_registr,$last_order,$registr,$podpiska,$id_cms,$categories);

				if ($subscription->save() == false) {
					return $modx->error->failure($modx->lexicon("Ошибка при сохранении подписки!"));
				}


			}

		}
	}


	$imports = new ListBook($api_key);
	$field_names = array('email', 'email_list_ids', 'phone', 'phone_list_ids', 'Name','total_order','date_registr','last_order','registration','podpiska','id_cms','categories');
	$list_res = $imports->importContacts($field_names, $data, 3, 1, 1);

	if ($list_res['error'] == 1) {
		return $modx->error->failure($modx->lexicon('Ошибка подписки:' . $list_res['text']));
	}

}


return $modx->error->success('', $subscription);
