<?php

class Unisender
{


    public $list_subscribe = 5709898;
    public $list_unsubscribe = 5714110;
    public $is_unsubscribe = 'Не подписан';
    public $is_subscribe = 'Подписан';

    function __construct(modX &$modx, array $config = array())
    {
        $this->modx =& $modx;

        $namespace = $this->modx->getObject('modNamespace', 'unisender');

        $basePath = MODX_CORE_PATH . 'components/unisender/';
        $assetsUrl = $this->modx->getOption('assets_url') . 'components/unisender/';

        $this->config = array_merge(array(
            'basePath' => $basePath,
            'corePath' => $basePath,
            'modelPath' => $basePath . 'model/',
            'processorsPath' => $basePath . 'processors/',
            //'chunksPath' => $basePath.'elements/chunks/',
            'jsUrl' => $assetsUrl . 'js/',
            'cssUrl' => $assetsUrl . 'css/',
            'assetsUrl' => $assetsUrl,
            'connectorUrl' => $assetsUrl . 'connector.php',
        ), $config);

        $this->modx->addPackage('unisender', $this->config['modelPath']/*
 , 'modx_unisender_'
 */);
    }

    /**
     * Initializes the class into the proper context
     *
     * @access public
     * @param string $ctx
     */

    public function initialize($ctx = 'web')
    {

        switch ($ctx) {
            case 'mgr':
                $this->modx->lexicon->load('unisender:default');
                //require $this->config['modelPath'] . 'unisender/request/unisenderControllerRequest.class.php';
                // echo $this->config['modelPath'] . 'unisender/request/'; exit;
                if (!$this->modx->loadClass('unisender.request.unisendercontrollerrequest', $this->config['modelPath'], true, true)) {
                    return 'Could not load controller request handler.';
                }

                $this->request = new unisenderControllerRequest($this);

                return $this->request->handleRequest();
                break;

            case 'connector':
                if (!$this->modx->loadClass('unisender.request.unisenderconnectorrequest', $this->config['modelPath'], true, true)) {
                    die('Could not load connector request handler2.');
                }

                $this->request = new unisenderConnectorRequest($this);

                return $this->request->handle();
                break;
            default:
                break;
        }
        return true;
    }

    public function getSubscribeTypeByUserId($id)
    {

        $email = 1;
        $phone = 2;
        $subscribeType = array('1' => 'Email', '2' => 'SMS');
        $c = $this->modx->newQuery('Subscribers');
        $c->where(array('user_id' => $id));
        $result = $this->modx->getCollection('Subscribers', $c);
        $out = array();
        foreach ($result as $res) {
            if (isset($subscribeType[$res->get(subscribe_type)])) {
                $out[] = $subscribeType[$res->get(subscribe_type)];
            }
        }
        if (count($out) > 0)
            return implode(', ', $out);
        else
            return '';
    }

    public function UnSubscribeUserId($id, $data)
    {
        $api_path = $this->modx->getOption('unisender.api_path');
        require_once($api_path . 'User.php');
        require_once($api_path . 'ListBook.php');
        $api_key = $this->modx->getOption('unisender_api');
        //$subscription = $this->modx->newObject('UnisenderSubscribers');

        //		$subscription->user_id = $id;
        if ($data['subscribe'] == 1)//$this->is_subscribe) // отправляем $list_subscribe
        {
            $subscription->list_id = $this->list_subscribe;

            $email_subscribed_list_id = $this->list_subscribe;
            $phone_subscribed_list_id = $this->list_subscribe;

        } else {//отправляем в другой список $list_unsubscribe
            $subscription->list_id = $this->list_unsubscribe;
            $email_subscribed_list_id = $this->list_unsubscribe;
            $phone_subscribed_list_id = $this->list_unsubscribe;
        }


        $data1 = array();
        //$data1[] = array($data['email'], $email_subscribed_list_id,  $data['phone'], $phone_subscribed_list_id, $data['fullname'],$data['created'],$data['last_order'],$data['registr'],$data['podpiska'],$id);


        $user = new User($api_key);
        /*$field_names = array('email', 'email_list_ids', 'phone', 'phone_list_ids', 'Name','date_registr','last_order',
        'registration','podpiska','id_cms');
        $list_res = $imports->importContacts($field_names, $data1, 1,1,1);//3, 1, 1);
*/
        $user->unsubscribe('email', $data['email']);
        if ($list_res['error'] == 1) {
            return false;//$modx->error->failure($modx->lexicon('Ошибка подписки:' . $list_res['text']));
        }

        //$subscription->subscribe_type=3;
        //if ($subscription->save()) return true;

        return false;
        // надо подписать пользователя

        //$newsubscribe = $this->modx->newObject('Subscribers');
    }

    public function ExistSubscribeUserId($id, $data)
    {
        $api_path = $this->modx->getOption('unisender.api_path');
        require_once($api_path . 'User.php');
        require_once($api_path . 'ListBook.php');
        $api_key = $this->modx->getOption('unisender_api');

        if ($data['subscribe'] == 1)//$this->is_subscribe) // отправляем $list_subscribe
        {
            $subscription->list_id = $this->list_subscribe;

            $email_subscribed_list_id = $this->list_subscribe;
            $phone_subscribed_list_id = $this->list_subscribe;

        } else {//отправляем в другой список $list_unsubscribe
            $subscription->list_id = $this->list_unsubscribe;
            $email_subscribed_list_id = $this->list_unsubscribe;
            $phone_subscribed_list_id = $this->list_unsubscribe;
        }


        $data1 = array();

        $user = new User($api_key);

        $list_res = $user->subscribe(array($this->list_subscribe), $data);
        echo '<pre>';
        print_r($list_res);
        echo '</pre>';
        if ($list_res['error'] == 1) {
            return false;//$modx->error->failure($modx->lexicon('Ошибка подписки:' . $list_res['text']));
        }

        return false;

    }

    //
    public function SubscribeUserId($id, $data)
    {

        $api_path = $this->modx->getOption('unisender.api_path');
        require_once($api_path . 'User.php');
        require_once($api_path . 'ListBook.php');
        $api_key = $this->modx->getOption('unisender_api');
        $subscription = $this->modx->newObject('UnisenderSubscribers');

        $subscription->user_id = $id;
        if ($data['subscribe'] == 1)//$this->is_subscribe) // отправляем $list_subscribe
        {
            $subscription->list_id = $this->list_subscribe;


            $email_subscribed_list_id = $this->list_subscribe;
            $phone_subscribed_list_id = $this->list_subscribe;


        } else {//отправляем в другой список $list_unsubscribe
            $subscription->list_id = $this->list_unsubscribe;
            $email_subscribed_list_id = $this->list_unsubscribe;
            $phone_subscribed_list_id = $this->list_unsubscribe;
        }


        $data1 = array();
        $data1[] = array($data['email'], $email_subscribed_list_id, $data['phone'], $phone_subscribed_list_id, $data['fullname'], $data['created'], $data['last_order'], $data['registr'], $data['podpiska'], $id);


        $imports = new ListBook($api_key);
        $field_names = array('email', 'email_list_ids', 'phone', 'phone_list_ids', 'Name', 'date_registr', 'last_order',
            'registration', 'podpiska', 'id_cms');
        $list_res = $imports->importContacts($field_names, $data1, 1, 1, 1);//3, 1, 1);

        if ($list_res['error'] == 1) {
            return false;//$modx->error->failure($modx->lexicon('Ошибка подписки:' . $list_res['text']));
        }

        $subscription->subscribe_type = 3;
        if ($subscription->save()) return true;

        return false;

    }

    public function UpdateSubscribeUser($user,$profile)
    {
        $api_path = $this->modx->getOption('unisender.api_path');
        require_once($api_path . 'User.php');
        require_once($api_path . 'ListBook.php');
        if ((count($user)>0)&&(count($profile)>0)) {
            $phone = $profile['phone'];
            if (strpos($phone,'+380')===false)
            {
                //return false;
                //continue;
            }


            $api_key = $this->modx->getOption('unisender_api');
            $user_id = $user['id'];
            $data = [];

            $list_id = '5709898';

            $fullname = $profile['fullname'];

            $email = $profile['email'];
            $city = '';
            if (isset($profile['extended'])){
                if (isset($profile['extended']['city_id'])) {
                    $city_id =    $profile['extended']['city_id'];

                    $q = $this->modx->newQuery('msLocalities');
                    $q->select('`msLocalities`.`title`');
                    $q->where(array('msLocalities.id' => $city_id));
                    if ($q->prepare() && $q->stmt->execute()) {
                        $cities = $q->stmt->fetch(PDO::FETCH_ASSOC);
                        if (count($cities)>0)
                            $city=$cities['title'];
                    }
                 }
            }




            $date_registr = $user['registr_date'];
            $registr = $user['registr'];
            if ($date_registr=='0000-00-00 00:00:00')
                $date_registr = $user['created'];
            //if ($date_registr == '-1-11-30 00:00:00')
              //  $date_registr = date('Y-m-d H:i:s');

            $subscribe = $user['subscribe'];
            if ($subscribe == 'Подписан') $podpiska = 1;
            elseif ($subscribe == '') $podpiska = 0;
            elseif ($subscribe == '1') $podpiska = 1;
            else $podpiska = 0;


            //$user = $this->modx->newQuery('msOrder', $user_id);

            $q = $this->modx->newQuery('msOrder');
           /* $q->where(array(
                'user_id' => $user_id
            ));*/
            $q->where(['user_id:=' => $user_id,'status'=>2]);
            $q->sortby('createdon', 'DESC');
            //$q->limit(10);    // Добавим лимит записей

            $q->prepare();
            $sql = $q->toSQL();

            $query = $this->modx->prepare($sql);
            $query->execute();

            $result = $query->fetchAll(PDO::FETCH_ASSOC);
            $total_order = 0;
            $categories = '';
            $last_order = $result[0]['msOrder_createdon'];
            $resultcat = array();
            $counts = array();
            $brands = [];
            $total_order=$user['summa_close_orders'];
            foreach ($result as $order) {
                //$total_order += $order['msOrder_cost'];


                $q = $this->modx->newQuery('msOrderProduct');
                $q->where(array(
                    'order_id' => $order['msOrder_id']
                ));
                $q->prepare();
                $sql = $q->toSQL();

                $query = $this->modx->prepare($sql);
                $query->execute();

                $result_products = $query->fetchAll(PDO::FETCH_ASSOC);

                /*echo "<pre>res produ";
                print_r($result_products);
                echo "</pre>";*/
                foreach ($result_products as $product) {
                    $productid = $product['msOrderProduct_product_id'];
                    $resultcat = array();

                    $q = $this->modx->newQuery('msCategory');
                    $q->leftJoin('msProduct', 'msProduct', array(
                        '`msCategory`.`id` = `msProduct`.`parent`'
                    ));
                    $q->leftJoin('msProductData', 'msProductData', array(
                        '`msProduct`.`id` = `msProductData`.`id`'
                    ));
                    $q->leftJoin('msVendor', 'msVendor', array(
                        '`msProductData`.`vendor` = `msVendor`.`id`'
                    ));
                    $q->sortby('msCategory.pagetitle', 'ASC');
                    $q->select(array('msCategory.id', 'msCategory.parent', 'msCategory.pagetitle', 'msVendor.name','msProductData.id as product_id', ));
                    $q->where('`msProduct`.`id` = ' . $productid);
                    $q->prepare();
                    $sql = $q->toSQL();

                    $result = array();

                    if ($q->prepare() && $q->stmt->execute()) {

                        while ($row = $q->stmt->fetch(PDO::FETCH_ASSOC)) {

                            $vendor = $row['name'];
                            $cat = false;
                            if ($row['parent'] != 10) {
                                $cat = $this->modx->getObject('msCategory', $row['parent']);

                            }

                            if ($cat) $name = $cat->get('pagetitle') . '.' . $row['pagetitle'];
                            else $name = $row['pagetitle'];


                            if ($cat) $resultcat[] = $cat->get('pagetitle');

                            if (isset($counts[$name])) $counts[$name] = $counts[$name] + $product['msOrderProduct_count'];
                            else $counts[$name] = 1;
                            $resultcat[] = $row['pagetitle'];//.'('.$counts[$row['pagetitle']].')';
                            $cats[$name] = implode('-', $resultcat) . '(' . $counts[$name] . ')';
                            $brands[$vendor] = $vendor;
                        }
                    }


                }
            }
            $categories = implode(", ", $cats);

            $email_subscribed_list_id = $list_id;
            $phone_subscribed_list_id = $list_id;
            $brand = implode(',', $brands);
            //$data[] = array($email, $email_subscribed_list_id, $phone, $phone_subscribed_list_id, $fullname, $total_order, $date_registr, $last_order, $registr, $podpiska, $user_id, $categories, $brand,$city);

            ///$pets=

            $sql = "SELECT p.*, p_t.name as name_type FROM modx_users u 
                    	LEFT JOIN modx_ms2_pets p
                    	LEFT JOIN modx_ms2_pet_types p_t  
                    				ON p.type=p_t.id 
WHERE p.user_id ='".$user_id."'";

            $q = $this->modx->prepare($sql);
            $q->execute(array(0));
            $user_pets_not_myself = $q->fetchAll(PDO::FETCH_ASSOC);

            $pets_array=[];
            foreach ($user_pets_not_myself as $pet)
            {
                $pet_id=$pet['id'];

                $pets_array[$pet['name_type']]=$pet['name_type'];

            }


            $data[] = array($email, $email_subscribed_list_id, $phone_subscribed_list_id, $fullname, $total_order, $date_registr, $last_order, $registr, $podpiska, $user_id, $categories, $brand,$city, implode(',',$pets_array));



            $imports = new ListBook($api_key);
            //$field_names = array('email', 'email_list_ids', 'phone', 'phone_list_ids', 'Name', 'total_order', 'date_registr', 'last_order', 'registration', 'podpiska', 'id_cms', 'categories', 'brand','city');
            $field_names = array('email', 'email_list_ids','phone_list_ids', 'Name', 'total_order', 'date_registr', 'last_order', 'registration', 'podpiska', 'id_cms', 'categories', 'brand','city','category_board');


           /* echo '<pre>';
            print_r($field_names);
            echo '</pre>';
            echo '<pre>';
            print_r($data);
            echo '</pre>';
*/
            $list_res = $imports->importContacts($field_names, $data, 3, 1, 1);
            /*if ($_SERVER['HTTP_CF_CONNECTING_IP'] == '188.130.177.18') {
                echo "<pre>data";
                print_r($data);
                echo "</pre>";

            }*/
           /* if (isset($list_res['result']['log']))
            {
                if ($list_res['result']['inserted']==1) echo 'insert '.$data[0][0];
                elseif ($list_res['result']['updated']==1) echo 'update '.$data[0][0];
                foreach ($list_res['result']['log'] as $log)
                {

                    echo ', '.$log['message'].' ';
                }
                echo "\r\n";
            }*/
            /*echo "<pre>data";
            print_r($data);
            echo "</pre>";
            echo "<pre>listunisender";
            print_r($list_res);
            echo "</pre>";*/
        }

        return true;
    }


    /*
         function refreshForecast() {
             $res =  file_get_contents('http://xml.weather.co.ua/1.2/forecast/23?dayf=27');
             $xml = simplexml_load_string($res);
             $arr = $this->object2array($xml);

             $arr = $arr['forecast']['day'];
             if (count($arr) > 0) {
                 global $modx;
                 foreach($arr as $v) {
                     $tmp = array();
                     $tmp['date'] = $v['@attributes']['date'];
                     $tmp['hour'] = $v['@attributes']['hour'];
                     $tmp['t_min'] = $v['t']['min'];
                     $tmp['t_max'] = $v['t']['max'];
                     $tmp['p_min'] = $v['p']['min'];
                     $tmp['p_max'] = $v['p']['max'];
                     $tmp['wind_min'] = $v['wind']['min'];
                     $tmp['wind_max'] = $v['wind']['max'];

                     $res = $modx->newObject('Forecast');
                     $res->fromArray($tmp);
                     $res->save();
                 }
             }
             else {return 'Непонятная ошибка!';}
         }

         function object2array($Object) {
             return @json_decode(@json_encode($Object), 1);
         }

         function deleteForecast() {
             global $modx;
             $modx->removeCollection('Forecast', array('id:>' => 0));
         }
     */
}

?>