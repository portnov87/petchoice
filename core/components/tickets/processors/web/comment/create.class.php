<?php
//ini_set('display_errors', 1);
//error_reporting(E_ALL | E_STRICT);
class TicketCommentCreateProcessor extends modObjectCreateProcessor
{
    /** @var TicketComment $object */
    public $object;
    /* @var TicketThread $thread */
    private $thread;
    public $objectType = 'TicketComment';
    public $classKey = 'TicketComment';
    public $languageTopics = array('tickets:default');
    public $permission = 'comment_save';
    public $beforeSaveEvent = 'OnBeforeCommentSave';
    public $afterSaveEvent = 'OnCommentSave';
    private $guest = false;


    /** {@inheritDoc} */
    public function checkPermissions()
    {
        $this->guest = (boolean)$this->getProperty('allowGuest', false);
        $this->unsetProperty('allowGuest');
        $this->unsetProperty('allowGuestEdit');
        $this->unsetProperty('captcha');
        if ($this->guest) return false; else return true;
        return !empty($this->permission) && !$this->guest
            ? $this->modx->hasPermission($this->permission)
            : true;
    }


    /** {@inheritDoc} */
    public function beforeSet()
    {
        $tid = $this->getProperty('thread');
        if (!$this->thread = $this->modx->getObject('TicketThread', array('name' => $tid, 'deleted' => 0, 'closed' => 0))) {
            return $this->modx->lexicon('ticket_err_wrong_thread');
        } elseif ($pid = $this->getProperty('parent')) {
            if (!$parent = $this->modx->getObject('TicketComment', array('id' => $pid, 'published' => 1, 'deleted' => 0))) {
                return $this->modx->lexicon('ticket_comment_err_parent');
            }
        }

        // Required fields
        $requiredFields = array_map('trim', explode(',', $this->getProperty('requiredFields', 'name,email')));
        foreach ($requiredFields as $field) {
            $value = $this->modx->stripTags(trim($this->getProperty($field)));
            if (empty($value)) {
                $this->addFieldError($field, $this->modx->lexicon('field_required'));
            } elseif ($field == 'email' && !preg_match('/.+@.+\..+/i', $value)) {
                $this->setProperty('email', '');
                $this->addFieldError($field, $this->modx->lexicon('ticket_comment_err_email'));
            } else {
                if ($field == 'email') {
                    $value = strtolower($value);
                }
                $this->setProperty($field, $value);
            }
        }
        if (!$text = trim($this->getProperty('text'))) {
            return $this->modx->lexicon('ticket_comment_err_empty');
        }
        if (!$this->getProperty('email') && $this->modx->user->isAuthenticated($this->modx->context->key)) {
            return $this->modx->lexicon('ticket_comment_err_no_email');
        }

        // Additional properties
        $properties = $this->getProperties();
        $add = array();
        $meta = $this->modx->getFieldMeta('TicketComment');
        foreach ($properties as $k => $v) {
            if (!isset($meta[$k])) {
                $add[$k] = $this->modx->stripTags($v);
            }
        }
        if (!$this->getProperty('published')) {
            $add['was_published'] = false;
        }
        if ($this->thread->id == 463) $published = 1;
        else $published = 1;
        // Comment values
        $ip = $this->modx->request->getClientIp();


        if ($this->modx->user->isAuthenticated($this->modx->context->key))
        {
            $user_id=$this->modx->user->id;
        }
        $this->setProperties(array(
            'text' => $text,
            'thread' => $this->thread->id,
            'ip' => $ip['ip'],
            'createdon' => date('Y-m-d H:i:s'),
            'createdby' => $this->modx->user->isAuthenticated($this->modx->context->key)
                ? $this->modx->user->id
                : 0,
            'editedon' => '',
            'editedby' => 0,
            'deleted' => 0,
            'deletedon' => '',
            'deletedby' => 0,
            'published' => $published,
            'properties' => $add,
        ));

        return parent::beforeSet();
    }
/*
    public function beforeSave() {


        if ($this->modx->user->isAuthenticated()) {
            $bonuses = '3%';//$this->modx->getOption('bonuses_for_review', '3%');
            $product_id = $this->thread->resource;

            if (strpos($bonuses, '%')!==false){
                $bonuses=str_replace('%','',$bonuses);
                $bonuses=(int)$bonuses;

                //$product_object=$this->modx->getObject('msProduct',$product_id);
                $miniShop2 = $this->modx->getService('minishop2');
                $miniShop2->initialize($this->modx->context->key);
                $in_order=$miniShop2->product_in_order($product_id);


                if ($in_order) {

                    $price_product = $in_order;//$product_object->get('price');
                    $bonuses = ceil(($bonuses / 100) * $price_product);
                }else $bonuses=false;


            }

            $ticket_id=$this->object->get('id');
            $user =$this->modx->user;

            $isset_ticket=$this->object->is_comment($product_id,$user);
            if (!$isset_ticket) {


                $this->charge_bonuses($user, $bonuses, $product_id, $ticket_id);
            }

        }



        return parent::beforeSave();
    }*.


    /** {@inheritDoc} */
    public function afterSave()
    {
        $this->thread->fromArray(array(
            'comment_last' => $this->object->get('id'),
            'comment_time' => $this->object->get('createdon'),
        ));
        $this->thread->save();

        if ($this->guest) {
            if (!isset($_SESSION['TicketComments'])) {
                $_SESSION['TicketComments'] = array('ids' => array());
            }
            $_SESSION['TicketComments']['name'] = $this->object->get('name');
            $_SESSION['TicketComments']['email'] = $this->object->get('email');
            $_SESSION['TicketComments']['ids'][$this->object->get('id')] = 1;
        }

        //$message="Имя: ".$this->object->get('name')."\r\nОтзыв: \r\n".$this->object->get('text')."\r\n\r\n<a href="http://petchoice.com.ua/manager/?a=83">Смотреть отзыв</a>";
        //mail('petchoice.info@gmail.com','Новый отзыв о товаре '.$this->thread->resource, $message);

        if ($this->modx->user->isAuthenticated()) {
            $bonuses = '3%';//$this->modx->getOption('bonuses_for_review', '3%');
            $product_id = $this->thread->resource;

            if (strpos($bonuses, '%')!==false){
                $bonuses=str_replace('%','',$bonuses);
                $bonuses=(int)$bonuses;

                //$product_object=$this->modx->getObject('msProduct',$product_id);
                $miniShop2 = $this->modx->getService('minishop2');
                $miniShop2->initialize($this->modx->context->key);
                $in_order=$miniShop2->product_in_order($product_id);


                if ($in_order) {

                    $price_product = $in_order;//$product_object->get('price');
                    $bonuses = ceil(($bonuses / 100) * $price_product);
                }else $bonuses=false;

               /* if ($_SERVER['HTTP_CF_CONNECTING_IP']=='78.46.62.217'){
                    echo '<pre>$in_order';
                    print_R($in_order);
                    echo '</pre>';
                    //die();
                }*/
            }

            $ticket_id=$this->object->get('id');
            $user =$this->modx->user;

            $isset_ticket=$this->object->is_comment($product_id,$user, $ticket_id);
            if (!$isset_ticket) {


                $this->charge_bonuses($user, $bonuses, $product_id, $ticket_id);
            }

            /*if ($_SERVER['HTTP_CF_CONNECTING_IP']=='78.46.62.217'){
                echo '<pre>$in_order'.$product_id.' '.$user->get('id').' '.$ticket_id.' '.$bonuses.' is ticket';
                print_R($isset_ticket);
                echo '</pre>';
                die();
            }*/
        }

        $this->thread->updateCommentsCount();
        $this->object->clearTicketCache();

        return parent::afterSave();
    }


    public function charge_bonuses($user,$bonuses,$product_id,$ticket_id)
    {

        if ($bonuses) {
            $user_object = $this->modx->getObject('modUser', $user->id);
            if ($user_object->get('registr')==1) {
                //if ($user_object)
                if ($bonuses < 5) $bonuses = 5;
                $miniShop2 = $this->modx->getService('minishop2');
                $miniShop2->initialize($this->modx->context->key);
                // отзывы за последнии 30 дней
                $thead = 463;
                $count_tickets = $this->object->counttickets($user, $thead);
                //if ($count_tickets>3) return false;

                // проверяем купленный ли товар
                //$in_order = $miniShop2->product_in_order($product_id);
                //проверяем есть ли уже отзыв на товар от тек пользваотеля


                // размер отзыва
                $text = $this->object->get('text');
                $strlen = mb_strlen($text);
                if ($strlen < 100) return false;


                //if ($in_order) {
                // начисляем бонусы

                if ($user_object) {
                    $exist_bonuses = $user_object->get('bonuses');
                    $bonuses_add = $exist_bonuses + $bonuses;
                    $user_object->set('bonuses', $bonuses_add);
                    $date_burn = date('Y-m-d H:i:s', strtotime('+ 45 day'));

                    $user_object->set('date_clear_bonuses', $date_burn);
                    if ($user_object->save()) {

                        $miniShop2->saveLogBonuses(['amount' => '+' . $bonuses, 'user_id' => $user->id, 'ticket_id' => $ticket_id]);
                        $profile = $user_object->getOne('Profile');
                        //$miniShop2->sendEmailBonuses($profile, $bonuses, $bonuses_add, $date_burn);

                        $miniShop2->eventNotification(3, $user_object, [
                            'bonus_total'=>$bonuses_add,
                            'fullname'=>$profile->get('fullname'),
                            'bonus_now'=>$bonuses,//$bonuses_add,
                            'date_burn'=>$date_burn
                        ]);

                    }
                }
                return true;
            }
           // }
        }



        return false;
    }
}

return 'TicketCommentCreateProcessor';