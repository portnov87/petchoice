<?php

class TicketCommentGetProcessor extends modObjectGetProcessor {
	public $objectType = 'TicketComment';
	public $classKey = 'TicketComment';
	public $languageTopics = array('tickets:default');


	/** {@inheritDoc} */
	public function cleanup() {
		$comment = $this->object->toArray();
		$comment['text'] = html_entity_decode($comment['text']);
		$comment['raw'] = html_entity_decode($comment['raw']);

		
		  $rat='';
		//$comment['idx'] = $i ++;
		if ($comment['rating']!=0)
		{
		    
		    for ($d = 1; $d <= $comment['rating']; $d++) {
                	$rat.= '<span class="icon star-little-icon star-little-icon-check"></span>';
            }
            $noact=5-(int)$comment['rating'];
            for ($d = 1; $d <= $noact; $d++) {
                $rat.= '<span class="icon star-little-icon"></span>';
            }

		}
		
		$comment['rat']= $rat;
		
		
		
		return $this->success('', $comment);
	}
}

return 'TicketCommentGetProcessor';