<?php

class TicketCommentGetProcessor extends modObjectGetProcessor {
	public $objectType = 'TicketComment';
	public $classKey = 'TicketComment';
	public $languageTopics = array('tickets:default');

	public function cleanup() {
		$comment = $this->object->toArray();
		$comment['createdon'] = $this->formatDate($comment['createdon']);
		$comment['editedon'] = $this->formatDate($comment['editedon']);
		$comment['deletedon'] = $this->formatDate($comment['deletedon']);
		$comment['text'] = !empty($comment['raw']) ? html_entity_decode($comment['raw']) : html_entity_decode($comment['text']);
  $rat='';
		//$comment['idx'] = $i ++;
		if ($comment['rating']!=0)
		{
		    
		    for ($d = 1; $d <= $comment['rating']; $d++) {
                	$rat.= '<span class="icon star-little-icon star-little-icon-check"></span>';//$pdoFetch->getChunk($tplRatact);
            }
            $noact=5-(int)$comment['rating'];
            for ($d = 1; $d <= $noact; $d++) {
                $rat.= '<span class="icon star-little-icon"></span>';
            }

		}
		
		$comment['rat']= $rat;
		
		return $this->success('',$comment);
	}

	public function formatDate($date = '') {
		if (empty($date) || $date == '0000-00-00 00:00:00') {
			return $this->modx->lexicon('no');
		}
		return strftime('%d %b %Y %H:%M', strtotime($date));
	}
}

return 'TicketCommentGetProcessor';