<?php
/**
 * @package tickets
 */
class TicketComment extends xPDOSimpleObject {
	public $class_key = 'TicketComment';

	/**
	 * {@inheritDoc}
	 */
	public function & getMany($alias, $criteria= null, $cacheFlag= true) {
		if ($alias == 'Attachments' || $alias == 'Votes') {
			$criteria = array('class' => $this->class_key);
		}
		return parent::getMany($alias, $criteria, $cacheFlag);
	}

	/**
	 * {@inheritDoc}
	 */
	public function addMany(& $obj, $alias= '') {
		$added= false;
		if (is_array($obj)) {
			foreach ($obj as $o) {
				if (is_object($o)) {
					$o->set('class', $this->class_key);
					$added = parent::addMany($obj, $alias);
				}
			}
			return $added;
		}
		else {
			return parent::addMany($obj, $alias);
		}
	}


	/**
	 * Try to clear cache of ticket
	 *
	 * @return bool
	 */
	public function clearTicketCache() {
		$clear = $this->xpdo->getOption('tickets.clear_cache_on_comment_save');
		if (!empty($clear) && $clear != 'false') {
			/** @var TicketThread $thread */
			$thread = $this->getOne('Thread');
			/** @var modResource|Ticket $ticket */
			if ($ticket = $this->xpdo->getObject('modResource', $thread->get('resource'))) {
				if (method_exists($ticket, 'clearCache')) {
					$ticket->clearCache();
					return true;
				}
			}
		}
		return false;
	}


	/**
	 * Move comment from one thread to another and clear cache of its tickets
	 *
	 * @param int $from
	 * @param int $to
	 *
	 * @return bool
	 */
	public function changeThread($from, $to) {
		/** @var TicketThread $old_thread */
		$old_thread = $this->xpdo->getObject('TicketThread', $from);
		/** @var TicketThread $new_thread */
		$new_thread = $this->xpdo->getObject('TicketThread', $to);

		if ($new_thread && $old_thread) {
			$this->set('thread', $to);
			$this->save();

			$children = $this->getMany('Children');
			/** @var TicketComment $child */
			foreach ($children as $child) {
				$child->set('parent', $to);
				$child->save();
			}

			$old_thread->updateLastComment();
			/** @var modResource|Ticket $ticket */
			if ($ticket = $this->xpdo->getObject('modResource', $old_thread->get('resource'))) {
				if (method_exists($ticket, 'clearCache')) {
					$ticket->clearCache();
				}
			}

			$new_thread->updateLastComment();
			/** @var modResource|Ticket $ticket */
			if ($ticket = $this->xpdo->getObject('modResource', $new_thread->get('resource'))) {
				if (method_exists($ticket, 'clearCache')) {
					$ticket->clearCache();
				}
			}

			return true;
		}
		return false;
	}


	/**
	 * Update comment rating
	 *
	 * @return array
	 */
	public function updateRating() {
		$votes = array('rating' => 0, 'rating_plus' => 0, 'rating_minus' => 0);

		$q = $this->xpdo->newQuery('TicketVote', array('id' => $this->id, 'class' => 'TicketComment'));
		$q->innerJoin('modUser', 'modUser', '`modUser`.`id` = `TicketVote`.`createdby`');
		$q->select('value');
		if ($q->prepare() && $q->stmt->execute()) {
			while ($value = $q->stmt->fetch(PDO::FETCH_COLUMN)) {
				$votes['rating'] += $value;
				if ($value > 0) {
					$votes['rating_plus'] += $value;
				}
				else {
					$votes['rating_minus'] += $value;
				}
			}
			$this->fromArray($votes);
			$this->save();
		}

		return $votes;
	}

	public function counttickets($user,$thead=463)
    {
        $user_id=$user->id;//get('id');
        $q = $this->xpdo->newQuery('TicketComment');
        //$q->innerJoin('TicketThread', 'TicketThread', '`TicketThread`.`id` = `TicketComment`.`thread`');
        //$q->innerJoin('modUser', 'modUser', '`modUser`.`id` = `TicketVote`.`createdby`');
        $q->select('id');
        $day30 = 86400 * 30;
        $daydate = date("Y-m-d", time() - $day30);    // H:m:s
        $daydatebegin = $daydate . ' 00:00:00';
        $daydateend = $daydate . ' 23:59:59';
        $where = array(
            //'TicketComment.createdon:<=' => $daydateend,
            //'TicketComment.createdon:>=' => $daydatebegin,
            'TicketComment.thread:!='=>$thead,// 463 - отзывы по сайту
            'TicketComment.createdby:='=>$user_id,
            'TicketComment.published:='=>1,
            'TicketComment.deleted:='=>0,
        );
        $q->where($where);
        //$q->where(['TicketThread.resource']);
        if ($q->prepare() && $q->stmt->execute()) {
            $all=$q->stmt->fetchAll();
            return count($all);
        }

        return 0;
    }

    public function is_comment($product_id,$user, $ticket_id)
    {
        $user_id=$user->id;//get('id');
        //$product_id=$product->get('id');
        $q = $this->xpdo->newQuery('TicketComment', array('createdby' => $user_id,'published'=>1,'deleted'=>0));
        $q->innerJoin('TicketThread', 'TicketThread', '`TicketThread`.`id` = `TicketComment`.`thread`');
        //$q->innerJoin('modUser', 'modUser', '`modUser`.`id` = `TicketVote`.`createdby`');
        $q->select('TicketComment.id');
        $q->where(['TicketThread.resource'=>$product_id,'TicketComment.id:!='=>$ticket_id]);// 463 - отзывы по сайту
        if ($q->prepare() && $q->stmt->execute()) {

            $product=$q->stmt->fetch();
          /*  echo '<pre>$product';
            print_r($product);
            echo '</pre>';*/
            if ($product) return true;
        }else{
            //echo $q->toSQL();
            //echo 'error';
        }
        return false;
    }






}