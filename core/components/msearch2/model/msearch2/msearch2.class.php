<?php
/**
 * The base class for mSearch2.
 *
 * @package msearch2
 */

class mSearch2
{
    /** @var modX $modx */
    public $modx;
    /** @var array $config */
    public $config = array();
    /** @var mSearch2ControllerRequest $request */
    protected $request;
    /** @var mse2FiltersHandler $filtersHandler */
    public $filtersHandler = null;
    /** @var array $phpMorphy */
    public $phpMorphy = array();
    /** Cache for filters methods */
    public $methods = array();
    /** Cache for filters values */
    public $filters = array();
    public $filters_n = array();
    /** @var integer Total number of filter operations */
    public $filter_operations = 0;
    /** @var string Current search query */
    public $query = '';
    /** @var null|\pdoTools $pdoTools */
    public $pdoTools = null;
    /** @var array $fields */
    public $fields = array();


    public function __construct(modX &$modx, array $config = array(), pdoTools &$pdoTools = null)
    {
        $this->modx =& $modx;

        $corePath = $this->modx->getOption('msearch2.core_path', $config, $this->modx->getOption('core_path') . 'components/msearch2/');
        $assetsPath = $this->modx->getOption('msearch2.assets_path', $config, $this->modx->getOption('assets_path') . 'components/msearch2/');
        $assetsUrl = $this->modx->getOption('msearch2.assets_url', $config, $this->modx->getOption('assets_url') . 'components/msearch2/');
        $actionUrl = $this->modx->getOption('msearch2.action_url', $config, $assetsUrl . 'action.php');
        $connectorUrl = $assetsUrl . 'connector.php';

        $this->config = array_merge(array(
            'assetsUrl' => $assetsUrl,
            'cssUrl' => $assetsUrl . 'css/',
            'jsUrl' => $assetsUrl . 'js/',
            'jsPath' => $assetsPath . 'js/',
            'imagesUrl' => $assetsUrl . 'images/',
            'customPath' => $corePath . 'custom/',
            'dictsPath' => $corePath . 'phpmorphy/dicts/',

            'connectorUrl' => $connectorUrl,
            'actionUrl' => $actionUrl,

            'corePath' => $corePath,
            'modelPath' => $corePath . 'model/',
            'templatesPath' => $corePath . 'elements/templates/',
            'processorsPath' => $corePath . 'processors/',

            'cacheTime' => 1800,
            'min_word_length' => $this->modx->getOption('mse2_index_min_words_length', null, 3, true),
            'exact_match_bonus' => $this->modx->getOption('mse2_search_exact_match_bonus', null, 5, true),
            'like_match_bonus' => $this->modx->getOption('mse2_search_like_match_bonus', null, 3, true),
            'all_words_bonus' => $this->modx->getOption('mse2_search_all_words_bonus', null, 5, true),
            'introCutBefore' => 50,
            'introCutAfter' => 250,
            'filter_delimeter' => '|',
            'method_delimeter' => ':',
            'values_delimeter' => ',',
            'split_words' => $this->modx->getOption('mse2_search_split_words', null, '#\s#', true),
            'split_all' => '#\s|[,.:;!?"\'(){}\\/\#]#',
            'suggestionsRadio' => array(),

            'autocomplete' => 0,
            'queryVar' => 'query',
            'minQuery' => 3,
            'onlyIndex' => false,
            'config_file' => $assetsPath . 'js/web/config.js',
        ), $config);

        if (!is_array($this->config['suggestionsRadio'])) {
            $this->config['suggestionsRadio'] = array_map('trim', explode(',', $this->config['suggestionsRadio']));
        }

        $this->modx->addPackage('msearch2', $this->config['modelPath']);
        $this->modx->lexicon->load('msearch2:default');

        if (!empty($pdoTools) && $pdoTools instanceof pdoTools) {
            $this->pdoTools = $pdoTools;
        }

        $this->getWorkFields();
    }


    /**
     * Prepares working fields of resources for search
     *
     * @param array $config
     *
     * @return array
     */
    public function getWorkFields($config = array())
    {
        $config = array_merge($this->config, $config);
        $setting = $this->modx->getOption('mse2_index_fields', null, 'content:3,description:2,introtext:2,pagetitle:3,longtitle:3', true);
        $fields = $default = array();

        // Preparing default fields for work
        $tmp = array_map('trim', explode(',', strtolower($setting)));
        foreach ($tmp as $v) {
            $tmp2 = explode(':', $v);
            $default[$tmp2[0]] = !empty($tmp2[1])
                ? $tmp2[1]
                : 1;
        }
        if ($this->modx->getOption('mse2_index_comments', null, true)) {
            $default['comment'] = $this->modx->getOption('mse2_index_comments', null, 1, true);
        }

        if (!empty($config['fields'])) {
            $tmp = array_map('trim', explode(',', strtolower($config['fields'])));
            foreach ($tmp as $v) {
                $tmp2 = explode(':', $v);
                $fields[$tmp2[0]] = !empty($tmp2[1])
                    ? $tmp2[1]
                    : (isset($default[$tmp[0]])
                        ? $default[$tmp[0]]
                        : 1
                    );
            }
        } else {
            $fields = $default;
        }
        $fields = $default;
        $this->fields = $fields;
        /*echo '<pre>$this->fields';
        print_r($this->fields);
        echo '</pre>';*/
        return $fields;
    }


    /**
     * Initializes mSearch2 into different contexts.
     *
     * @param string $ctx The context to load. Defaults to web.
     * @param array $scriptProperties
     *
     * @return boolean
     */
    public function initialize($ctx = 'web', $scriptProperties = array())
    {
        switch ($ctx) {
            case 'mgr':
                if (!$this->modx->loadClass('msearch2.request.mSearch2ControllerRequest', $this->config['modelPath'], true, true)) {
                    return 'Could not load controller request handler.';
                }
                $this->request = new mSearch2ControllerRequest($this);
                return $this->request->handleRequest();
                break;
            default:
                $this->config = array_merge($this->config, $scriptProperties);
                $this->config['ctx'] = $ctx;

                if (!defined('MODX_API_MODE') || !MODX_API_MODE) {
                    $config = $this->makePlaceholders($this->config);
                    if ($css = trim($this->modx->getOption('mse2_frontend_css'))) {
                        $this->modx->regClientCSS(str_replace($config['pl'], $config['vl'], $css));
                    }


                    /*echo '<pre>';
                    print_R($_SERVER);
                    echo '</pre>';*/
                    if ($_SERVER['SERVER_NAME'] == 'm.petchoice.com.ua') {
                        if ($js = trim($this->modx->getOption('mse2_frontend_m_js'))) {
                            //$this->modx->regClientScript(str_replace($config['pl'], $config['vl'], $js));
                        }
                    } else {
                        if ($js = trim($this->modx->getOption('mse2_frontend_js'))) {
                            //$this->modx->regClientScript(str_replace($config['pl'], $config['vl'], $js));
                        }
                    }
                }
        }

        return true;
    }


    /**
     * Method loads custom classes from specified directory
     *
     * @var string $dir Directory for load classes
     * @return void
     */
    public function loadCustomClasses($dir)
    {
        $files = scandir($this->config['customPath'] . $dir);
        foreach ($files as $file) {
            if (preg_match('/.*?\.class\.php$/i', $file)) {
                include_once($this->config['customPath'] . $dir . '/' . $file);
            }
        }
    }


    /**
     * Initializes phpMorphy for needed language
     *
     * @return boolean
     */
    public function loadPhpMorphy()
    {
        if (!class_exists('phpMorphy_Exception')) {
            require_once $this->config['corePath'] . 'phpmorphy/src/common.php';
        }

        $dicts = $this->getDictionaries();
        foreach (array_keys($dicts) as $lang) {
            if (!empty($this->phpMorphy[$lang]) && $this->phpMorphy[$lang] instanceof phpMorphy) {
                continue;
            } else {
                try {
                    $this->phpMorphy[$lang] = new phpMorphy(
                        $this->config['corePath'] . 'phpmorphy/dicts/'
                        , $lang
                        , array(
                            'storage' => 'file'
                        )
                    );
                } catch (phpMorphy_Exception $e) {
                    $this->modx->log(modX::LOG_LEVEL_ERROR, '[mSearch2] Could not initialize phpMorphy for language .' . $lang . ': .' . $e->getMessage());
                }
            }
        }

        if (!count($this->phpMorphy)) {
            $this->modx->log(modX::LOG_LEVEL_ERROR, '[mSearch2] Could not find any phpMorphy dictionary.');
            return false;
        } else {
            return true;
        }
    }


    /**
     * Get dictionaries from phpMorphy directory
     *
     * @return array
     */
    public function getDictionaries()
    {
        $dicts = array();

        $path = $this->config['dictsPath'];
        if (!file_exists($path)) {
            return $dicts;
        }

        $files = scandir($path);
        foreach ($files as $file) {
            if (preg_match('/\.([a-z]{2,2}\_[a-z]{2,2})\./i', $file, $matches)) {
                $dicts[$matches[1]][] = $file;
            }
        }

        return $dicts;
    }


    /**
     * Returns array with words for search
     *
     * @param string $text
     * @param string $pcre
     * @param bool $with_count
     *
     * @return array
     */
    public function getBulkWords($text = '', $pcre = '', $with_count = false)
    {
        if (empty($pcre)) {
            $pcre = $this->config['split_words'];
        }
        $words = preg_split($pcre, $text, -1, PREG_SPLIT_NO_EMPTY);
        $bulk_words = array();
        foreach ($words as $v) {
            if (preg_match('/^[0-9]{2,}$/', $v) || mb_strlen($v, 'UTF-8') >= $this->config['min_word_length']) {
                $word = mb_strtoupper($v, 'UTF-8');
                if (!$with_count) {
                    $bulk_words[$word] = $word;
                } elseif (isset($bulk_words[$word])) {
                    $bulk_words[$word] += 1;
                } else {
                    $bulk_words[$word] = 1;
                }
            }
        }
        return $bulk_words;
    }


    /**
     * Gets base form of the words
     *
     * @param array|string $text
     * @param boolean $only_words
     *
     * @return array|string
     */
    public function getBaseForms($text, $only_words = true)
    {
        $result = array();
        if (is_array($text)) {
            foreach ($text as $v) {
                $result = array_merge($result, $this->getBaseForms($v, $only_words));
            }
        } else {
            $text = str_ireplace('ё', 'е', $this->modx->stripTags($text));
            $text = preg_replace('#\[.*\]#isU', '', $text);

            $bulk_words = $this->getBulkWords($text, $this->config['split_all']);
            $this->loadPhpMorphy();
            /* @var phpMorphy $phpMorphy */
            $base_forms = array();
            foreach ($this->phpMorphy as $phpMorphy) {
                $locale = $phpMorphy->getLocale();
                $base_forms[$locale] = $phpMorphy->getBaseForm($bulk_words);
            }

            $result = array();
            foreach ($base_forms as $lang => $array) {
                if (!empty($array)) {
                    foreach ($array as $word => $forms) {
                        //if (!$forms) {$forms = array($word);}
                        if (!$forms) {
                            continue;
                        }
                        foreach ($forms as $form) {
                            if (preg_match('/^[0-9]{2,}$/', $form) || mb_strlen($form, 'UTF-8') >= $this->config['min_word_length']) {
                                $result[$form] = $lang == 'en_EN'
                                    ? @iconv('WINDOWS-1250', 'UTF-8//IGNORE', $word)
                                    : $word;
                            }
                        }
                    }
                }
            }
            if ($only_words) {
                $result = array_keys($result);
            }
        }

        return $result;
    }


    /**
     * Gets all morphological forms of the words
     *
     * @param array|string $text
     *
     * @return array|string
     */
    public function getAllForms($text)
    {
        $result = array();
        if (is_array($text)) {
            foreach ($text as $v) {
                $result = array_merge($result, $this->getAllForms($v));
            }
        } else {
            $text = str_ireplace('ё', 'е', $this->modx->stripTags($text));

            $bulk_words = $this->getBulkWords($text);
            $this->loadPhpMorphy();
            /* @var phpMorphy $phpMorphy */
            $all_forms = array();
            foreach ($this->phpMorphy as $phpMorphy) {
                $locale = $phpMorphy->getLocale();
                $all_forms[$locale] = $phpMorphy->getAllForms($bulk_words);
            }

            $result = array();
            if (!empty($all_forms)) {
                foreach ($all_forms as $lang => $array) {
                    if (!empty($array)) {
                        foreach ($array as $word => $forms) {
                            if (!empty($forms)) {
                                if ($lang == 'en_EN') {
                                    foreach ($forms as &$v) {
                                        $v = iconv('WINDOWS-1250', 'UTF-8//IGNORE', $v);
                                    }
                                }
                                $result[$word] = isset($result[$word])
                                    ? array_merge($result[$word], $forms)
                                    : $forms;
                            }
                        }
                    }
                }
            }
        }

        return $result;
    }


    /**
     * Search and return array with resources ids as a key and sum of weight as value
     *
     * @param $query
     *
     * @return array
     */
    public function Search($query)
    {
        $string = preg_replace('/[^_-а-яёa-z0-9\s\.\/]+/iu', ' ', $this->modx->stripTags($query));
        $this->log('Filtered search query: "' . mb_strtolower($query . '"', 'UTF-8'));
        $string = $this->query = $this->addAliases($string);
        $this->log('Search query with processed aliases: "' . mb_strtolower($string . '"', 'UTF-8'));
        $words = $this->getBaseForms($string, false);
        $result = $all_words = $found_words = array();


        $q = $this->modx->newQuery('mseAlias', array('word:IN' => array_merge($words, array_keys($forms))));
        $q->select('word,alias,replace');
        $tstart = microtime(true);
        if ($q->prepare() && $q->stmt->execute()) {
            $this->modx->queryTime += microtime(true) - $tstart;
            $this->modx->executedQueries++;
            while ($row = $q->stmt->fetch(PDO::FETCH_ASSOC)) {
                if ($row['replace']) {
                    $all = current($this->getAllForms($row['word']));
                    $all[] = $row['word'];
                    foreach ($all as $word) {
                        $key = array_search(mb_strtolower($word), $words);
                        if ($key !== false) {
                            $words[$key] = $row['alias'];
                        }
                    }
                } else {
                    $words[] = $row['alias'];
                }
            }
        }

        if (!empty($words)) {
            $q = $this->modx->newQuery('mseWord');
            $q->select($this->modx->getSelectColumns('mseWord', 'mseWord'));
            $q->where(array('word:LIKE' => '%' . $string . '%', 'OR:word:IN' => array_keys($words), 'field:IN' => array_keys($this->fields)));
            $tstart = microtime(true);
            if ($q->prepare() && $q->stmt->execute()) {
                $this->modx->queryTime += microtime(true) - $tstart;
                $this->modx->executedQueries++;
                while ($row = $q->stmt->fetch(PDO::FETCH_ASSOC)) {


                    if (isset($result[$row['resource']])) {
                        $result[$row['resource']] += $this->fields[$row['field']] * $row['count'];
                    } else {
                        $result[$row['resource']] = $this->fields[$row['field']] * $row['count'];
                    }

                    if (isset($words[$row['word']])) {
                        $all_words[$row['resource']][$words[$row['word']]] = 1;
                        $found_words[$words[$row['word']]] = 1;
                    }
                }
            }


            /*
            $q = $this->modx->newQuery('mseAlias', array('word:IN' => array_merge($words, array_keys($forms))));
            $q->select('word,alias,replace');
            $tstart = microtime(true);
            if ($q->prepare() && $q->stmt->execute()) {
                $this->modx->queryTime += microtime(true) - $tstart;
                $this->modx->executedQueries++;
                while ($row = $q->stmt->fetch(PDO::FETCH_ASSOC)) {
                    if ($row['replace']) {
                        $all = current($this->getAllForms($row['word']));
                        $all[] = $row['word'];
                        foreach ($all as $word) {
                            $key = array_search(mb_strtolower($word), $words);
                            if ($key !== false) {
                                $words[$key] = $row['alias'];
                            }
                        }
                    }
                    else {
                        $words[] = $row['alias'];
                    }
                }
            }*/

        }
        $added = 0;

        if (!empty($found_words)) {
            $this->log('Found results by words INDEX (' . implode(',', array_keys($words)) . '): ' . count($result));
        } else {
            $this->log('Nothing found by words INDEX');
        }

        // Add bonuses
        if (empty($this->config['onlyIndex'])) {
            $bulk_words = $this->getBulkWords($query);
            $tmp_words = preg_split($this->config['split_words'], $query, -1, PREG_SPLIT_NO_EMPTY);
            if (count($bulk_words) > 1 || count($tmp_words) > 1 || empty($result)) {
                if (!empty($this->config['exact_match_bonus']) || !empty($this->config['all_words_bonus'])) {
                    $exact = $this->simpleSearch($query);

                    // Exact match bonus
                    if (!empty($this->config['exact_match_bonus'])) {
                        foreach ($exact as $v) {
                            if (isset($result[$v])) {
                                $result[$v] += $this->config['exact_match_bonus'];
                                //$this->log('Added "exact match bonus" for a resource '.$v.' for words "'.implode(', ', $words).'": +'.$this->config['exact_match_bonus']);
                            } else {
                                $result[$v] = $this->config['exact_match_bonus'];
                                //$this->log('Found resource '.$v.' by LIKE search with all words "'.implode(', ',$words).'": +'.$this->config['exact_match_bonus']);
                                $added++;
                            }
                        }
                    }

                    if (!empty($this->config['all_words_bonus'])) {
                        if (count($bulk_words) > 1) {
                            // All words bonus
                            foreach ($all_words as $k => $v) {
                                if (count($bulk_words) == count($v)) {
                                    $result[$k] += $this->config['all_words_bonus'];
                                    //$this->log('Added "all words bonus" for a resource '.$k.' for words "'.implode(', ', $words).'": +'.$this->config['all_words_bonus']);
                                }
                            }
                        }
                    }
                }
            } elseif (!empty($this->config['like_match_bonus'])) {
                // Add the whole possible results for query
                $all_results = $this->simpleSearch($query);
                foreach ($all_results as $v) {
                    if (!isset($result[$v])) {
                        $weight = round($this->config['like_match_bonus']);
                        $result[$v] = $weight;
                        //$this->log('Found resource '.$v.' by LIKE search with all words "'.$query.'": +'.$weight);
                        $added++;
                    }
                }
            }

            // Add matches by %LIKE% search
            if (!empty($this->config['like_match_bonus']) && $not_found = array_diff($bulk_words, $words)) {
                foreach ($not_found as $word) {
                    $found = $this->simpleSearch($word);
                    foreach ($found as $v) {
                        $weight = round($this->config['like_match_bonus']);
                        if (!isset($result[$v])) {
                            $result[$v] = $weight;
                            //$this->log('Found resource '.$v.' by LIKE search with single word "'.$word.'": +'.$weight);
                            $added++;
                        } else {
                            $result[$v] += $weight;
                            //$this->log('Added "LIKE bonus" for a resource '.$v.' for word "'.$word.'": +'.$weight);
                        }

                    }
                }
            }
            $this->log('Added resources by LIKE search: ' . $added);
        }
        /*
$res=$this->simpleSearch($query);
                    foreach ($res as $r)
                    {
                        $result[$r]=100000;
                    }*/
        // Log the search query
        /** @var mseQuery $object */
        if ($object = $this->modx->getObject('mseQuery', array('query' => $query))) {
            $object->set('quantity', $object->get('quantity') + 1);
        } else {
            $object = $this->modx->newObject('mseQuery');
            $object->set('query', $query);
            $object->set('quantity', 1);
        }
        $object->set('found', count($result));
        $object->save();

        arsort($result);
        /*echo '<pre>';
        print_r($result);
        echo '</pre>';*/

        return $result;
    }


    /**
     * Search and return array with resources that matched for LIKE search
     *
     * @param $query
     *
     * @return array
     */
    public function simpleSearch($query)
    {
        $string = preg_replace('/[^_-а-яёa-z0-9\s\.\/]+/iu', ' ', $this->modx->stripTags($query));

        $result = array();
        $q = $this->modx->newQuery('mseIntro');
        $q->select('`resource`');
        $q->where(array('intro:LIKE' => '%' . $string . '%'));
        $tstart = microtime(true);
        if ($q->prepare() && $q->stmt->execute()) {
            $this->modx->queryTime += microtime(true) - $tstart;
            $this->modx->executedQueries++;
            $result = $q->stmt->fetchAll(PDO::FETCH_COLUMN);
        }

        return $result;
    }


    /**
     * @param $string
     *
     * @return mixed
     */
    public function addAliases($string)
    {
        $string = mb_strtoupper(str_ireplace('ё', 'е', $this->modx->stripTags($string)), 'UTF-8');
        $string = preg_replace('#\[.*\]#isU', '', $string);

        $pcre = $this->config['split_words'];
        $words = preg_split($pcre, $string, -1, PREG_SPLIT_NO_EMPTY);
        $words = array_unique($words);
        $forms = $this->getBaseForms($words, false);
        $forms[mb_strtoupper($string)] = mb_strtoupper($string);
        $ar = array_merge($words, array_keys($forms));
        $ar[] = $string;
        $q = $this->modx->newQuery('mseAlias', array('word:IN' => $ar));//
        $q->select('word,alias,replace');
        /*echo '<pre>';
        print_r($forms );//array_merge($words, array_keys($forms)));
        echo '</pre>';*/
        $tstart = microtime(true);
        if ($q->prepare() && $q->stmt->execute()) {
            $this->modx->queryTime += microtime(true) - $tstart;
            $this->modx->executedQueries++;
            while ($row = $q->stmt->fetch(PDO::FETCH_ASSOC)) {

                if ($row['replace']) {
                    $all = current($this->getAllForms($row['word']));
                    $all[] = $row['word'];
                    $words = array();
                    /*echo '<pre>';
                print_r($all);
                echo '</pre>';*/
                    foreach ($all as $word) {
                        $key = array_search(mb_strtolower($word), $words);
                        //echo $key.'<br/>';
                        if (($key !== false) || (!in_array(mb_strtolower($word), $words))) {
                            $words[$key] = $row['alias'];
                        }
                    }
                    //$words[$key] = $row['alias'];

                } else {
                    $words[] = $row['alias'];
                }
            }
        }
        /*echo '<pre>';
        print_R($words);//$words);
        echo '</pre>';//die();
        */
        $words = array_unique($words);
        return implode(' ', $words);
    }


    /**
     * Highlight search string in given text string
     *
     * @param $text
     * @param string $query
     * @param string $htag_open
     * @param string $htag_close
     * @param boolean $strict
     *
     * @return mixed
     */
    public function Highlight($text, $query, $htag_open = '<b>', $htag_close = '</b>', $strict = true)
    {
        if (empty($query)) {
            return $text;
        }
        $from = $to = array();

        $tmp_words = preg_split($this->config['split_words'], $query, -1, PREG_SPLIT_NO_EMPTY);
        // Exact match
        $pcre = $strict ? '/\b' . $query . '\b/imu' : '/' . $query . '/imu';
        if (count($tmp_words) > 1 && preg_match($pcre, $text, $matches)) {
            $pos = mb_stripos($text, $matches[0], 0, 'UTF-8');
            if ($pos >= $this->config['introCutBefore']) {
                $text_cut = '... ';
                $pos -= $this->config['introCutBefore'];
            } else {
                $text_cut = '';
                $pos = 0;
            }
            $text_cut .= mb_substr($text, $pos, $this->config['introCutAfter'], 'UTF-8');
            if (mb_strlen($text, 'UTF-8') > $this->config['introCutAfter']) {
                $text_cut .= ' ...';
            }

            foreach ($matches as $v) {
                $from[$v] = $v;
                $to[$v] = $htag_open . $v . $htag_close;
            }
        } // Matching by separate words
        else {
            if (empty($this->query)) {
                $this->query = $this->addAliases($query);
            }

            $tmp = array_merge(
                $tmp_words
                , explode(' ', $this->query)
            );
            $tmp = array_unique($tmp);
            $tmp = $this->getAllForms($tmp);

            $words = array_keys($tmp);
            foreach ($tmp as $v) {
                $words = array_merge($words, $v);
            }

            if (empty($words)) {
                $words = array($query => $query);
            }

            $text_cut = '';
            foreach ($words as $key => $word) {
                /*
                if (!preg_match('/^[0-9]{2,}$/', $word) && mb_strlen($word,'UTF-8') < $this->config['min_word_length']) {
                    unset($words[$key]);
                    continue;
                }
                */
                $word = preg_quote($word, '/');
                $words[$key] = $word;

                // Cutting text on first occurrence
                $pcre = $strict ? '/\b' . $word . '\b/imu' : '/' . $word . '/imu';
                if (empty($text_cut) && !empty($word) && preg_match($pcre, $text, $matches)) {
                    $pos = mb_stripos($text, $matches[0], 0, 'UTF-8');
                    if ($pos >= $this->config['introCutBefore']) {
                        $text_cut = '... ';
                        $pos -= $this->config['introCutBefore'];
                    } else {
                        $text_cut = '';
                        $pos = 0;
                    }
                    $text_cut .= mb_substr($text, $pos, $this->config['introCutAfter'], 'UTF-8');
                    if (mb_strlen($text, 'UTF-8') > $this->config['introCutAfter']) {
                        $text_cut .= ' ...';
                    }
                }
            }

            if (empty($text_cut) && $strict) {
                return $this->Highlight($text, $query, $htag_open, $htag_close, false);
            }

            $pcre = $strict ? '/\b(' . implode('|', $words) . ')\b/imu' : '/(' . implode('|', $words) . ')/imu';
            if (preg_match_all($pcre, $text_cut, $matches)) {
                foreach ($matches[0] as $v) {
                    $from[$v] = $v;
                    $to[$v] = $htag_open . $v . $htag_close;
                }
            }

            if (!empty($matches[1])) {
                foreach ($matches[1] as $v) {
                    $from[$v] = $v;
                    $to[$v] = $htag_open . $v . $htag_close;
                }
            } elseif ($strict) {
                return $this->Highlight($text, $query, $htag_open, $htag_close, false);
            }
        }

        return str_replace($from, $to, $text_cut);
    }


    /**
     * Return array with filters
     *
     * @param array|string $ids
     * @param boolean $build
     *
     * @return array
     */
    public function getFilters($ids, $build = true)
    {
        // prepare ids

        if (!is_array($ids)) {
            $ids = array_map('trim', explode(',', $ids));
        }
        if (empty($ids)) {
            return false;
        }

        //$lang
        $polylang = $this->modx->getService('polylang', 'Polylang');
        $polyLangTools = $polylang->getTools();
        $language = false;
        $language = $polyLangTools->detectLanguage(true);


        $culture_key=$language->get('culture_key');


        // Return results from cache
        if ($build && $prepared = $this->modx->cacheManager->get('msearch2/prep_' . md5(implode(',', $ids) . $culture_key. $this->config['filters']))) {

            return $prepared;
        } elseif (!$build && !empty($this->filters)) {
            return $this->filters;
        } elseif (!$build && $this->filters = $this->modx->cacheManager->get('msearch2/fltr_'  . $culture_key.  md5(implode(',', $ids)))) {
            return $this->filters;
        }
//echo 'msearch2/prep_' . md5(implode(',',$ids) . $this->config['filters']);
        if (!is_object($this->filtersHandler)) {
            $this->loadHandler();
        }

        // Preparing filters
        $filters = $built = array();
        $tmp_filters = array_map('trim', explode(',', $this->config['filters']));
        foreach ($tmp_filters as $v) {
            $v = strtolower($v);
            if (empty($v)) {
                continue;
            } elseif (strpos($v, $this->config['filter_delimeter']) !== false) {
                @list($table, $filter) = explode($this->config['filter_delimeter'], $v);
                //echo $table.' dd '. $filter.'<br/>';
            } else {
                $table = 'resource';
                $filter = $v;
            }

            $tmp = explode($this->config['method_delimeter'], $filter);
            $filters[$table][$tmp[0]] = array();

            $built[$table . $this->config['filter_delimeter'] . $tmp[0]] = !empty($tmp[1]) ? $tmp[1] : 'default';
        }

        $filters['attributes'] = array();

        $method = 'getAttributeValues';//get'.ucfirst($table).'Values';
        //echo $method."\r\n";
        //$keys = array_keys($fields);
        if (method_exists($this->filtersHandler, $method)) {
            $fields = call_user_func_array(array($this->filtersHandler, $method), array($ids));
            //$fields=$fields['filter'];
            //$names=$fields['names'];
            $filters['attributes'] = $fields;
        }


        // Retrieving filters
        foreach ($filters as $table => &$fields) {
            //if ($_SERVER['REMOTE_ADDR'] == '188.130.177.18') {

            //if (isset($fields['price'])){
            $method = 'get' . ucfirst($table) . 'Values';
            //echo $method."\r\n";

            $keys = array_keys($fields);
            if (method_exists($this->filtersHandler, $method)) {
                //echo '$method'.$method.'<br/>';
                $fields = call_user_func_array(array($this->filtersHandler, $method), array($keys, $ids));
                //$fields=$fields['filter'];
                //$names=$fields['names'];

            } else {
                $this->modx->log(modX::LOG_LEVEL_ERROR, '[mSearch2] Method "' . $method . '" not exists in class "' . get_class($this->filtersHandler) . '". Could not retrieve filters from "' . $table . '"');
            }//break;
            //}
        }

        if (!$build) {
            return $filters;
        }


        $this->filters = $filters;
        $prepared = array();
        foreach ($this->filters as $table => $filters) {
            foreach ($filters as $key => $values) {
                $new_key = $table . $this->config['filter_delimeter'] . $key;
                $filter = !empty($built[$new_key])
                    ? $built[$new_key]
                    : 'default';
                if ($table == 'attributes') $filter = 'attributes';
                if ($table == 'tv' && $filter == 'default') {
                    $filter = $key;
                    $method = 'buildTVsFilter';
                } else {
                    $method = 'build' . ucfirst($filter) . 'Filter';
                }
                //echo $method.'<br/>';
                if (method_exists($this->filtersHandler, $method)) {
                    $prepared[$new_key] = call_user_func_array(array($this->filtersHandler, $method), array($values, $filter));
                    //echo $method.'<br/>';
                } elseif (method_exists($this->filtersHandler, 'buildDefaultFilter')) {
                    $prepared[$new_key] = call_user_func_array(array($this->filtersHandler, 'buildDefaultFilter'), array($values, $filter));
                } else {
                    $this->modx->log(modX::LOG_LEVEL_ERROR, '[mSearch2] Method "' . $method . '" not exists in class "' . get_class($this->filtersHandler) . '". Could not build filter for "' . $new_key . '"');
                    $prepared[$new_key] = $values;
                }
            }
        }


        //	//getAttributeValues
        //	echo '<pre>$prepared';
        //			print_r($prepared);
        //			echo '</pre>';
        //        echo '<pre>$filters';
        //        print_r($filters);
        //        echo '</pre>';
        //
        //
        //
        //			die();
        // Sort filters
        foreach ($built as $key => &$values) {
            if (isset($prepared[$key])) {
                $values = $prepared[$key];
                unset($prepared[$key]);
            }
        }
        // Add new generated filters to the end of list
        $built = array_merge($built, $prepared);

        // Set cache
        $this->modx->cacheManager->set('msearch2/prep_' . md5(implode(',', $ids) . $culture_key.  $this->config['filters']), $built, $this->config['cacheTime']);

        return $built;
    }

    public
    function getRangePrice($parent)
    {
        /*
        $q = $this->modx->newQuery('modResource');
            $q->select('id,pagetitle');
            $q->where(array('word:IN' => array_keys($words), 'field:IN' => array_keys($this->fields)));
            */
        //}

        /*$q = $this->modx->newQuery('msProductData');//'parent'=>$parent,
        $q->where(array('class_key' => 'msProduct','published' => 1,'deleted' => 0));//'parent:IN' => $pids,
        $q->select('`msProduct`.`price_kg`');*/


        $q = $this->modx->newQuery('msProduct');
        //$q->leftJoin('msCategoryMember', 'Member', '`Member`.`product_id` = `msProduct`.`id`');
        $q->leftJoin('msProductData', 'Data', '`msProduct`.`id` = `Data`.`id`');
        $q->where(array('class_key' => 'msProduct', 'msProduct.parent' => $parent, 'msProduct.deleted' => 0));//'msProduct.published' => 1,
        $q->select('`Data`.`price_kg`');
        $prices = array();
        if ($q->prepare() && $q->stmt->execute()) {
            $prices = $q->stmt->fetchAll(PDO::FETCH_COLUMN);
            foreach ($prices as $pr) {
                if (($pr) && ($pr != '') && ($pr !== NULL)) $prices_new[] = $pr;
            }
        }
        /*if ($_SERVER['REMOTE_ADDR']=='188.130.177.18'){
        echo '<pre>$prices';
        print_R($prices_new);
        echo '</pre>';
        }*/
        $prices = $prices_new;
        $min = round(min($prices));
        if ((!$min) || ($min == '') || ($min == NULL)) $min = 0;
        return array($min, ceil(max($prices)));
    }

    /**
     * Filters resources by given params
     *
     * @param array|string $ids
     * @param array $request
     *
     * @return array
     */
    public
    function Filter($ids, array $request)
    {
        if (!is_array($ids)) {
            $ids = explode(',', $ids);
        }
        if (!is_object($this->filtersHandler)) {
            $this->loadHandler();
        }


        if (count($this->filters_n) == 0) {
            $filters = $this->getFilters($ids, false);
            $this->filters_n = $filters;
        } else {
            $filters = $this->filters_n;
        }
        $methods = $this->getMethods();

        $idskidka = array();
        /*
                if ($_SERVER['REMOTE_ADDR']=='188.130.177.18') {
                    echo "<pre>req";
                    print_r($filters);
                    echo "</pre>";
                }*/

        foreach ($request as $filter => $requested) {
            if (isset($filters['attributes'][$filter])) {
                $method = 'filterAttribute';
                $table = 'attributes';
            } elseif (isset($filters['ms'][$filter]) && ($filter == 'price_kg'))//$requested][$filter]))//'adiscount'
            {
                $method = 'filterPrice';
                $table = 'ms';
            } elseif (isset($filters['ms'][$filter]))//$requested][$filter]))//'adiscount'
            {
                $method = 'filterDefault';
                $table = 'ms';
            } elseif (isset($filters['ms']['vendor'][$requested]))//$filter
            {
                $method = 'filterDefault';//filterVendors';
                $table = 'ms';
            } else {
                $method = !empty($methods[$filter]) ? 'filter' . ucfirst($methods[$filter]) : 'filterAttribute';
            }
            if (isset($filters[$table][$filter])) {
                $values = $filters[$table][$filter];
                $requested = explode($this->config['values_delimeter'], $requested);

                if (method_exists($this->filtersHandler, $method)) {
                    $values1 = array();
                    if ($method != 'filterPrice') {
                        foreach ($values as $key => $req) {
                            foreach ($req['values'] as $key1 => $req1) {
                                $values1[$key1] = $req1['products'];
                            }
                        }
                    }
                    /*echo $method.'<pre>';
                    print_r($values);
                    echo '</pre>';
                    echo '<pre>';
                    print_r($requested);
                    echo '</pre>';*/
                    //echo $method.' '.$requested.' '. $values1. ' '. $ids;
                    if ($values1 && (count($values1) > 0))
                        $ids = call_user_func_array(array($this->filtersHandler, $method), array($requested, $values1, $ids));
                    else $ids = call_user_func_array(array($this->filtersHandler, $method), array($requested, $values, $ids));
                } else {
                    //$this->modx->log(modX::LOG_LEVEL_ERROR, '[mSearch2] Method "'.$method.'" not exists in class "'.get_class($this->filtersHandler).'". Could not build filter "'.$table.$this->config['filter_delimeter'].$filter.'"');
                    $ids = @call_user_func_array(array($this->filtersHandler, 'filterDefault'), array($requested, $values, $ids));
                }
                if ($filter == 'askidka') {
                    $idskidka = $values[1];
                }

            }

            /*echo '<pre>$filters';
            print_r($ids);
            echo '</pre>';*/

            $this->filter_operations++;
            /*	}else{

                    list($table, $filter) = explode($this->config['filter_delimeter'], $filter);
                    if (isset($filters['ms']['vendor'][$filter])){
                        //$table='ms';
                    }
                    //echo $method.' '.$table.' '.$filter.'<br/>';

                    if (isset($filters[$table][$filter])) {
                        $values = $filters[$table][$filter];

                        $requested = explode($this->config['values_delimeter'], $requested);

                        if (method_exists($this->filtersHandler, $method)) {
                            $ids = call_user_func_array(array($this->filtersHandler, $method), array($requested, $values, $ids));
                        }
                        else {
                            $ids = @call_user_func_array(array($this->filtersHandler, 'filterDefault'), array($requested, $values, $ids));
                        }
                        if ($filter=='askidka')$idskidka=$values[1];
                    }


                    $this->filter_operations++;
                }*/

        }

        if (isset($request['adiscount']) && ($request['adiscount'] == 1))//$_GET[]$values)
        {
            //$ids
            //array_merge($ids,$idskidka);
            if (count($idskidka) > 0) $ids = $ids + $idskidka;
            /*echo '<pre>s';
                print_r($ids);//$values);//array($requested, $values, $ids));//array($requested, $values, $ids));
                echo '</pre>';*/
        }
        /*if ($_SERVER['REMOTE_ADDR']=='188.130.177.18'){
            echo '<pre>s';
                print_r($ids);//$idskidka);//$values);//array($requested, $values, $ids));//array($requested, $values, $ids));
                echo '</pre>';
        }*/
        /*if ($_SERVER['REMOTE_ADDR']=='188.130.177.18'){
        echo 'filt '.$this->filter_operations;
        }*/
        return $ids;
    }


    /**
     * This method returns preliminary results for each filter
     *
     * @param $ids
     * @param array $request
     * @param array $current
     *
     * @return array
     */
    public
    function getSuggestions($ids, array $request, array $current = array(), $filters = array())
    {
        if (!is_array($ids)) {
            $ids = explode(',', $ids);
        }
        if (!is_object($this->filtersHandler)) {
            $this->loadHandler();
        }

        if (method_exists($this->filtersHandler, 'getSuggestions')) {

            return $this->filtersHandler->getSuggestions($ids, $request, $current);
        } else {
            $current = array_flip($current);
            //$filters = $filters;//$this->getFilters($ids, false);
            $radio = $this->config['suggestionsRadio'];
            /*
            echo '<pre>';
            print_r($filters);
            echo '</pre>';*/
            $suggestions = array();
            foreach ($filters as $table => $fields) {
                foreach ($fields as $field => $values) {
                    $keys1 = array_keys($values);

                    /*echo $table;
                    echo '<pre>';
                    print_r($values);
                    echo '</pre>';*/
                    if ($table == 'attributes') $values = $values[$keys1[0]]['values'];
                    foreach ($values as $value1 => $resources1) {

                        $value = $value1;
                        $suggest = $request;
                        //$key = $table.$this->config['filter_delimeter'].$field;
                        $key = $field;

                        $added = 0;
                        if (isset($request[$key])) {
                            //echo $key.' '.$request[$key].'<br/>';
                            // Types of suggestion can depend from method
                            if (!empty($radio) && in_array($key, $radio)) {
                                $suggest[$key] = $value;
                            } else {
                                $tmp2 = explode($this->config['values_delimeter'], $request[$key]);
                                if (!in_array($value, $tmp2)) {
                                    $suggest[$key] .= $this->config['values_delimeter'] . $value;
                                    $added = 1;
                                }
                            }
                            $res = $this->Filter($ids, $suggest);
                            /*echo '<pre>$res';
                            print_r($res);
                            echo '</pre>';*/
                            if ($added && !empty($res)) {
                                foreach ($res as $k => $v) {
                                    if (isset($current[$v])) {
                                        unset($res[$k]);
                                    }
                                }
                                $count = count($res);
                                if ($count != 0) {
                                    $count = '+' . $count;
                                }
                            } else {
                                /*foreach ($res as $k => $v) {
                                    if(isset($current[$v])) {
                                    //if (in_array($v,$current)){

                                        unset($res[$k]);

                                    }
                                }*/


                                $count = count($res);
                            }

                        } else {
                            $suggest[$key] = $value;
                            $res = $this->Filter($ids, $suggest);
                            /*
                            echo '<pre>$res'.$key.' '.$value;
                                        print_r($res);
                                        echo '</pre>';*/
                            $count = count($res);
                        }

                        $suggestions[$key][$value] = $count;
                    }
                }
            }
            return $suggestions;
        }
    }


    /**
     * Returns methods names, that will filter values
     *
     * @return array
     */
    public
    function getMethods()
    {
        if (!empty($this->methods)) {
            return $this->methods;
        }

        $tmp_filters = array_map('trim', explode(',', $this->config['filters']));
        foreach ($tmp_filters as $v) {
            $v = strtolower($v);
            if (strpos($v, $this->config['filter_delimeter']) !== false) {
                @list($table, $filter) = explode($this->config['filter_delimeter'], $v);
            } else {
                $table = 'resource';
                $filter = $v;
            }

            $tmp = explode($this->config['method_delimeter'], $filter);
            if ($tmp[0] == 'adiscount') {
                $this->methods[$table . $this->config['filter_delimeter'] . $tmp[0]] = 'default';
            } else {
                $this->methods[$table . $this->config['filter_delimeter'] . $tmp[0]] = !empty($tmp[1]) ? $tmp[1] : 'attribute';
            }
        }


        //$this->methods[$table.$this->config['filter_delimeter'].$tmp[0]] = !empty($tmp[1]) ? $tmp[1] : 'default';
        return $this->methods;
    }

    /**
     * Method for transform array to placeholders
     *
     * @var array $array With keys and values
     * @var string $prefix
     *
     * @return array $array Two nested arrays With placeholders and values
     */
    public
    function makePlaceholders(array $array = array(), $prefix = '')
    {
        $result = array(
            'pl' => array(),
            'vl' => array(),
        );
        foreach ($array as $k => $v) {
            if (is_array($v)) {
                $result = array_merge_recursive($result, $this->makePlaceholders($v, $prefix . $k . '.'));
            } else {
                $result['pl'][$prefix . $k] = '[[+' . $prefix . $k . ']]';
                $result['vl'][$prefix . $k] = $v;
            }
        }
        return $result;
    }


    /**
     * Returns string for insert into sorting properties of pdoTools snippet
     *
     * @param string
     *
     * @return string
     */
    public
    function getSortFields($sort, $sortdir)
    {
        $this->loadHandler();
        if (strpos($sort, '|') !== false) {
            $sort_ex = explode('|', $sort);
            if (isset($sort_ex[1])) {
                $sort_e = explode(':', $sort_ex[1]);
                if (isset($sort_e[1]))
                    $sortdir = $sort_e[1];
            }

        }
        return $this->filtersHandler->getSortFields($sort, $sortdir);
    }


    /**
     * Loads custom filters handler class
     *
     * @return bool
     */
    public
    function loadHandler()
    {
        if (!is_object($this->filtersHandler)) {
            require_once 'filters.class.php';
            $filters_class = $this->modx->getOption('mse2_filters_handler_class', null, 'mse2FiltersHandler', true);
            if ($filters_class != 'mse2FiltersHandler') {
                $this->loadCustomClasses('filters');
            }
            if (!class_exists($filters_class)) {
                $filters_class = 'mse2FiltersHandler';
            }

            $this->filtersHandler = new $filters_class($this, $this->config);
            if (!($this->filtersHandler instanceof mse2FiltersHandler)) {
                $this->modx->log(modX::LOG_LEVEL_ERROR, '[mSearch2] Could not initialize filters handler class: "' . $filters_class . '"');
                return false;
            }
        }

        return true;
    }


    /**
     * Checks for exists of miniShop2
     *
     * @return bool
     */
    public
    function checkMS2()
    {
        return file_exists(MODX_CORE_PATH . 'components/minishop2/model/minishop2/msproduct.class.php');
    }


    /**
     * @param $entry
     */
    public
    function log($entry)
    {
        if ($this->pdoTools && empty($this->config['hideSearchLog'])) {
            $this->pdoTools->addTime('[mSearch2] ' . $entry);
        }
    }

}
