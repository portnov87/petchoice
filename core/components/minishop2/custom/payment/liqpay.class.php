<?php
if (!class_exists('msPaymentInterface')) {
    require_once dirname(dirname(dirname(__FILE__))) . '/model/minishop2/mspaymenthandler.class.php';
}

class Liqpay extends msPaymentHandler implements msPaymentInterface
{

    private $_api_url = 'https://www.liqpay.ua/api/';

    function __construct(xPDOObject $object, $config = array())
    {
        $this->modx = &$object->xpdo;

        $siteUrl = $this->modx->getOption('site_url');
        $assetsUrl = $this->modx->getOption('assets_url') . 'components/minishop2/';
        /*$paymentUrl = $siteUrl . substr($assetsUrl, 1) . 'payment/liqpay.php';

                $this->config = array_merge(array(
                    'paymentUrl' => $paymentUrl
                    , 'apiUrl' => $this->modx->getOption('ms2_payment_paypal_api_url', null, 'https://api-3t.paypal.com/nvp')
                    , 'checkoutUrl' => $this->modx->getOption('ms2_payment_paypal_checkout_url', null, 'https://www.paypal.com/webscr?cmd=_express-checkout&token=')
                    , 'currency' => $this->modx->getOption('ms2_payment_paypal_currency', null, 'USD')
                    , 'user' => $this->modx->getOption('ms2_payment_paypal_user')
                    , 'password' => $this->modx->getOption('ms2_payment_paypal_pwd')
                    , 'signature' => $this->modx->getOption('ms2_payment_paypal_signature')
                    , 'json_response' => false,
                ), $config);*/
    }

    /* @inheritdoc} */
    public function send(msOrder $order, $submitpayment=false)
    {
        $link = $this->getPaymentLink($order);
        $id = $order->get('id');

        if (!$this->modx->loadClass('pdofetch', MODX_CORE_PATH . 'components/pdotools/model/pdotools/', false, true)) {
            return false;
        }
        $pdoFetch = new pdoFetch($this->modx);

        $kurs = $this->modx->getOption('kurs');
        $ord = $order->toArray();

        $delivery_cost = $ord['delivery_cost'];
        // Fields to select
        $resourceColumns = $this->modx->getSelectColumns('msProduct', 'msProduct');
        $dataColumns = $this->modx->getSelectColumns('msProductData', 'Data', '', array('id'), true) . ',`Data`.`price` as `original_price`';
        $vendorColumns = $this->modx->getSelectColumns('msVendor', 'Vendor', 'vendor.', array('id'), true);
        $orderProductColumns = $this->modx->getSelectColumns('msOrderProduct', 'msOrderProduct', '', array('id'), true);


        // Tables for joining
        $leftJoin = '{"class":"msProduct","alias":"msProduct","on":"msProduct.id=msOrderProduct.product_id"},{"class":"msProductData","alias":"Data","on":"msProduct.id=Data.id"},{"class":"msVendor","alias":"Vendor","on":"Data.vendor=Vendor.id"}';
        if (!empty($thumbsLeftJoin)) {
            $leftJoin .= $thumbsLeftJoin;
        }
        $select = '"msProduct":"' . $resourceColumns . '","Data":"' . $dataColumns . '","OrderProduct":"' . $orderProductColumns . '","Vendor":"' . $vendorColumns . '"';
        if (!empty($thumbsSelect)) {
            $select .= ',' . implode(',', $thumbsSelect);
        }

        $default = array(
            'class' => 'msOrderProduct'
        , 'where' => '{"msOrderProduct.order_id":"' . $id . '"}'
        , 'leftJoin' => '[' . $leftJoin . ']'
        , 'select' => '{' . $select . '}'
        , 'sortby' => 'id'
        , 'sortdir' => 'ASC'
        , 'groupby' => 'msOrderProduct.id'
        , 'fastMode' => false
        , 'limit' => 0
        , 'return' => 'data'
        , 'nestedChunkPrefix' => 'minishop2_'
        );
        // Merge all properties and run!

        $pdoFetch->setConfig($default);
        $rows = $pdoFetch->run();
        $totalcart = 0;
        foreach ($rows as $row) {
            $catid = $row['parent'];
            $cat_model = $this->modx->getObject('msCategory', $catid);
            $category = '';
            //$product_model=$this->modx->getObject('msProductData',$row['id']);
            //$product_model

            $q = $this->modx->newQuery('modTemplateVar');
            $q->innerJoin('modTemplateVarResource', 'v', 'v.tmplvarid = modTemplateVar.id');
            $q->where(array(
                'v.contentid' => $row['product_id'],
                'modTemplateVar.id' => 1,
            ));
            $q->limit(1);
            $q->select(array('modTemplateVar.*', 'v.value'));
            $value1 = array();
            $array = array();
            $options1 = array();
            if ($q->prepare() && $q->stmt->execute()) {
                while ($tv = $q->stmt->fetch(PDO::FETCH_ASSOC)) {
                    $value1 = $tv['value'];
                }
            }
            // Получаем массив из JSON-строки
            if (count($value1) > 0) $options1 = $this->modx->fromJSON($value1);
            // $opt= $this->modx->fromJSON($row['options']);
            $size = $row['options']['size'];//$opt['size'];
            $price = str_replace(',', '.', $row['price']);
            $price = str_replace(' ', '', $price);

            $cena = $price;
            foreach ($options1 as $o) {

                if ($size == $o['weight'] . ' ' . $o['weight_prefix']) {


                    //$o['weight']
                    $suppliers_pr = str_replace(',', '.', $o['price_supplier']);
                    $suppliers_pr = str_replace(' ', '', $suppliers_pr);
                    if ($o['currency_supplier'] == 1)// грн
                    {
                        $pr = $suppliers_pr;
                    } else {
                        $pr = $suppliers_pr * $kurs;
                        //$data['price']=$miniShop2->formatPrice($data['price_supplier']+($data['price_supplier']*($data['markup']/100)));//, 2, '.', '');
                    }
                    $cena = $price - $pr;


                    //1475,3 1476.30 1 204.71


                }
            }


            if ($cat_model) {
                $category = $cat_model->get('pagetitle');
                $cat_model_parent = $this->modx->getObject('msCategory', $cat_model->get('parent'));
                if ($cat_model_parent) {
                    $category = $cat_model_parent->get('pagetitle') . ' - ' . $category;
                }

            }
            $cena = str_replace(',', '.', $cena);

            $totalcart += $cena * $row['count'];

            $vendorname = '';
            $sql = "SELECT name,url,country FROM `modx_ms2_vendors` WHERE `id` ='" . $row['vendor'] . "' LIMIT 1";
            $q = $this->modx->prepare($sql);
            $q->execute();
            $res = $q->fetchAll(PDO::FETCH_ASSOC);

            foreach ($res as $v) $vendorname = $v['name'];

            $name_product = addslashes($row['name']);
            $vendorname = addslashes($vendorname);
            $price_common = $cena * $row['count'];
            $product_data[] = "
                  {                           
                            'name': '" . $name_product . "',     
                            'id': '" . $row['product_id'] . "',
                            'price': '" . $price_common . "',
                            'brand': '" . $vendorname . "',
                            'category': '" . $category . "',
                            'variant': '" . $size . "',
                            'quantity': '" . $row['count'] . "',
                            'coupon': '" . $order->get('promocode') . "'  
                           }";


            $idsproduct[] = "'" . $row['article'] . "'";
        }
        $totalcart = $totalcart + $delivery_cost;
        $cartcost = str_replace(',', '.', $totalcart);


        $ecom_datal = "dataLayer.push({
            
                      'ecommerce': {
                         'currencyCode': 'UAH',
                         'purchase': {
                          'actionField': {
                            'id': '" . $order->get('num') . "', 
                            'affiliation': 'PetChoice',
                            'revenue': " . $cartcost . ",
                            'shipping': '" . $delivery_cost . "',
                            'coupon': '" . $order->get('promocode') . "'
                          },
                          'products': [";
        $ecom_datal .= implode(',', $product_data);
        $ecom_datal .= "]
                        }
                      },
                                   'event': 'gtm-ee-event',
                     'gtm-ee-event-category': 'Enhanced Ecommerce',
                     'gtm-ee-event-action': 'Purchase',
                     'gtm-ee-event-non-interaction': 'False',
        
                    });";
        $cartcost = $totalcart;//$outer['cart_cost']-$totalcart;


        $idsproductstr = '[' . implode(',', $idsproduct) . ']';

        $ecom .= "ga('ecommerce:send');";
        ////$this->modx->smarty->assign('ecomerce', $ecom);
        ///
        //$ecom_datal
        //$this->modx->smarty->assign('ecomerce', $ecom_datal);//$ecom);

        //if ($submitpayment)
            return $this->success('', array('redirect' => $link, 'msorder' => $order->get('id'), 'ecommerc' => $ecom_datal));
//        else
//            return $this->success('', array('redirect' => false, 'msorder' => $order->get('id'), 'ecommerc' => $ecom_datal));

        //return $this->success('', array('redirect' => false, 'msorder' => $order->get('id'), 'ecommerc' => $ecom_datal));

    }

    /* @inheritdoc} */
    public function receive(msOrder $order, $params = array())
    {

        return $this->success('');

        return true;
    }

    /**
     * Building query
     *
     * @param array $params Query params
     * @return array/boolean
     */
    public function request($params = array())
    {
        $requestParams = array_merge(array(
            'USER' => $this->config['user']
        , 'PWD' => $this->config['password']
        , 'SIGNATURE' => $this->config['signature']
        , 'VERSION' => '74.0',
        ), $params);

        $request = http_build_query($requestParams);
        $curlOptions = array(
            CURLOPT_URL => $this->config['apiUrl']
        , CURLOPT_VERBOSE => 1
        , CURLOPT_SSL_VERIFYPEER => true
        , CURLOPT_SSL_VERIFYHOST => 2
        , CURLOPT_CAINFO => dirname(__FILE__) . '/lib/paypal/cacert.pem'
        , CURLOPT_RETURNTRANSFER => 1
        , CURLOPT_POST => 1
        , CURLOPT_POSTFIELDS => $request,
        );

        $ch = curl_init();
        curl_setopt_array($ch, $curlOptions);

        $response = curl_exec($ch);
        if (curl_errno($ch)) {
            $result = curl_error($ch);
        } else {
            $result = array();
            parse_str($response, $result);
        }

        curl_close($ch);
        return $result;
    }

    /**
     * Returns a direct link for continue payment process of existing order
     *
     * @param msOrder $order
     *
     * @return string
     */
    public function getPaymentLink(msOrder $order)
    {


        $private_key = $this->modx->getOption('privat_keyliqpay');
        $public_key = $this->modx->getOption('public_keyliqpay');

        $amount = $order->cost;
        $order_id = $order->get('num');

        $currency = 'UAH';


        $description = 'Order #' . $order_id;
        $result_url = 'https://petchoice.ua/';//'http://'.htmlspecialchars($_SERVER['HTTP_HOST'], ENT_COMPAT, 'UTF-8');//.__PS_BASE_URI__.'index.php?controller=history';
        $server_url = 'https://petchoice.ua/assets/components/minishop2/getorder.php?action=payliqpay';//'http://'.htmlspecialchars($_SERVER['HTTP_HOST'], ENT_COMPAT, 'UTF-8');//.$liqpay->getPath().'validation.php';

        $type = 'buy';

        $version = '3';
        $language = 'ru';

        $data = base64_encode(
            json_encode(
                array('version' => $version,
                    'action' => 'pay',
                    'public_key' => $public_key,
                    'amount' => $amount,
                    'currency' => $currency,
                    'description' => $description,
                    'order_id' => $order_id,
                    'type' => $type,
                    'sandbox' => 0,
                    'result_url' => $result_url,
                    'server_url' => $server_url,
                    'language' => $language)
            )
        );

        $signature = base64_encode(sha1($private_key . $data . $private_key, 1));
        $link = 'https://www.liqpay.ua/api/3/checkout?data=' . $data . '&signature=' . $signature;
        return $link;

    }



    /**
     * Call API
     *
     * @param string $path
     * @param array $params
     * @param int $timeout
     *
     * @return stdClass
     */
    public function api($path, $params = array(), $timeout = 5)
    {
        if (!isset($params['version'])) {
            throw new InvalidArgumentException('version is null');
        }
        $url         = $this->_api_url . $path;

        $private_key = $this->modx->getOption('privat_keyliqpay');
        $public_key = $this->modx->getOption('public_keyliqpay');

        $data        = $this->encode_params(array_merge(compact('public_key'), $params));
        $signature   = $this->str_to_sign($private_key.$data.$private_key);
        $postfields  = http_build_query(array(
            'data'  => $data,
            'signature' => $signature
        ));

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, true); // Avoid MITM vulnerability http://phpsecurity.readthedocs.io/en/latest/Input-Validation.html#validation-of-input-sources
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 2);    // Check the existence of a common name and also verify that it matches the hostname provided
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT ,$timeout);   // The number of seconds to wait while trying to connect
        curl_setopt($ch, CURLOPT_TIMEOUT, $timeout);          // The maximum number of seconds to allow cURL functions to execute
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $postfields);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $server_output = curl_exec($ch);
        curl_close($ch);
        return json_decode($server_output);
    }


}