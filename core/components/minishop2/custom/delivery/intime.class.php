<?php
class Intime extends msDeliveryHandler {

	public function getCost(msOrderInterface $order, msDelivery $delivery, $cost = 0) {
		$cart = $this->ms2->cart->status();
		if ($cart['total_cost'] < 10000) {
			$add_price = $delivery->get('price');
			if (preg_match('/%$/', $add_price)) {
				$add_price = str_replace('%', '', $add_price);
				$add_price = $cost / 100 * $add_price;
			}
			$cost += $add_price;
		}
		return $cost;
	}

	public function isAvailableForCity($city_id = 0) {
	//==30
		if ($city_id==$this->cityOdessa) return false;
		else{
		
			$q = $this->modx->newQuery('msLocalities');
			$q->select('`msLocalities`.`kyrier`');
			$q->where(array('msLocalities.id' => $city_id));
			if ($q->prepare() && $q->stmt->execute()) {
				$city = $q->stmt->fetchAll(PDO::FETCH_ASSOC);
			}
			if ($city[0]['kyrier']==1) return false;		
		}
		return true;
	}

	public function getWarehouses($city_id = 0, $poshtomat=false) {
		$c = $this->modx->newQuery('msIntime');
		switch($city_id)
		{
			case $this->cityKiev:
			    $c->where(array('city' => 'Киев'));
			break;
			default:
			    $c->where(array('locality_id' => $city_id));
			break;
		}
		
		$c->sortby('warehouse_no', 'ASC');
		$warehouses = $this->modx->getCollection('msIntime', $c);
		$output = array();
		foreach ($warehouses as $warehouse) {
			$output[] = array(
				'Number' => $warehouse->get('id'),
				'DescriptionRu' => 'Склад №' . $warehouse->get('warehouse_no') . ' ' . $warehouse->get('warehouse'),
			);
		}

		return $output;

	}
}