<?php

class NovaPoshtaPoshtomat extends msDeliveryHandler
{


    public function getCost(msOrderInterface $order, msDelivery $delivery, $cost = 0)
    {
        $cart = $this->ms2->cart->status();
        if ($cart['total_cost'] < 10000) {
            $add_price = $delivery->get('price');
            if (preg_match('/%$/', $add_price)) {
                $add_price = str_replace('%', '', $add_price);
                $add_price = $cost / 100 * $add_price;
            }
            $cost += $add_price;
        }
        return $cost;
    }

    public function isAvailableForCity($city_id = 0)
    {


        if ($city_id == $this->cityOdessa) return true;//false;
        else {

            $q = $this->modx->newQuery('msNpCities');
            $q->select('`msNpCities`.`kyrier`');
            $q->where(array('msNpCities.id' => $city_id));
            if ($q->prepare() && $q->stmt->execute()) {
                $city = $q->stmt->fetchAll(PDO::FETCH_ASSOC);
            }
            if ($city[0]['kyrier'] == 1) return false;
        }
        return true;
    }

    public function getStreet($city_id, $street_string = '')
    {
        $key = $this->modx->getOption('ms2_delivery_novaposhta_api_key');
        $np = new NovaPoshtaApi2($key,
            'ru', // Язык возвращаемых данных: ru (default) | ua | en
            FALSE, // При ошибке в запросе выбрасывать Exception: FALSE (default) | TRUE
            'curl' // Используемый механизм запроса: curl (defalut) | file_get_content);
        );

        $street = $np->getStreet($city_id, $street_string);//$city['data'][0]['Ref']
        return $street['data'];
    }

    public function getCity($city_id)
    {

        $key = $this->modx->getOption('ms2_delivery_novaposhta_api_key');
        $np = new NovaPoshtaApi2($key,
            'ru', // Язык возвращаемых данных: ru (default) | ua | en
            FALSE, // При ошибке в запросе выбрасывать Exception: FALSE (default) | TRUE
            'curl' // Используемый механизм запроса: curl (defalut) | file_get_content);
        );

        $city = $np->getCity($city_id);//$city['data'][0]['Ref']
        if (isset($city['data'][0])) return $city['data'][0];//$street['data'];
        return false;
    }

    /**
     * Returns an additional cost depending on the method of delivery
     *
     * @param msOrderInterface|msOrderHandler $order
     * @param float $cost Current cost of order
     *
     * @return array|string
     */
    public function getWarehouses($city_id, $poshtomat = false)
    {

        $data = [];

        $c = $this->modx->newQuery('msNpWarehouse');

        $c->where("CategoryOfWarehouse = 'Postomat'");

        $c->where("CityRef = '$city_id'");

        $c->select('*');//Ref,DescriptionRu,CityRef,Number');

        $c->sortby('cast(Number as unsigned)', 'ASC');

        if ($c->prepare() && $c->stmt->execute()) {
            //echo $c->toSql();

            $results = $c->stmt->fetchAll(PDO::FETCH_ASSOC);
            foreach ($results as $res) {
                $nameWarehouse=htmlspecialchars($res['msNpWarehouse_DescriptionRu']);
                $nameWarehouseUa=htmlspecialchars($res['msNpWarehouse_Description']);
                $data[] = ['Ref' => $res['msNpWarehouse_Ref'], 'name' => $nameWarehouse, 'name_ua' => $nameWarehouseUa];
            }
        }
        return $data;

    }
}