<?php
class Kurier extends msDeliveryHandler {

	public function getCost(msOrderInterface $order, msDelivery $delivery, $cost = 0) {
		$cart = $this->ms2->cart->status();
		if ($cart['total_cost'] < 10000) {
			$add_price = $delivery->get('price');
			if (preg_match('/%$/', $add_price)) {
				$add_price = str_replace('%', '', $add_price);
				$add_price = $cost / 100 * $add_price;
			}
			$cost += $add_price;
		}
		return $cost;
	}

	public function isAvailableForCity($city){// = 0) {
		
		$elementObj=$this->modx->getObject('msNpCities',array('id' => $city,'kyrier'=>1));
		//$city = $this->modx->getCollection('msLocalities', $l);
		/*
		$treturn=false;
		if ($l->prepare() && $l->stmt->execute()) {
			while ($row = $l->stmt->fetch(PDO::FETCH_ASSOC)) {
			$treturn=true;
			}
		}
		$output = array();
		*/
		$treturn=false;
		  if ($elementObj!='') $treturn=true;
		/*foreach ($city as $c) {
			//$output[] = array(''=>$c->get('title));
			return true;
		}*/
		return $treturn;//false;
	}
/*
	public function getWarehouses($city_id = 0) {
		$c = $this->modx->newQuery('msIntime');
		$c->where(array('locality_id' => $city_id));
		$c->sortby('warehouse_no', 'ASC');
		$warehouses = $this->modx->getCollection('msIntime', $c);
		$output = array();
		foreach ($warehouses as $warehouse) {
			$output[] = array(
				'Number' => $warehouse->get('id'),
				'DescriptionRu' => 'Склад №' . $warehouse->get('warehouse_no') . ' ' . $warehouse->get('warehouse'),
			);
		}

		return $output;

	}*/
}