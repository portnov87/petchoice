<?php

Header("Expires: Mon, 26 Jul 1997 05:00:00 GMT"); //Дата в прошлом  
Header("Cache-Control: no-cache, must-revalidate"); // HTTP/1.1  
Header("Pragma: no-cache"); // HTTP/1.1  
Header("Last-Modified: " . gmdate("D, d M Y H:i:s") . "GMT");
define('MODX_CACHE_DISABLED', true);


//echo 'eeeee';die();
//error_reporting(E_ALL | E_STRICT);
ini_set('display_errors', 0);
$no_availabel=$yes_available=$no_available_some=[];
$no_availabel_stock=$yes_available_stock=$no_available_some_stock=[];

function num2str($num)
{
    $nul = 'ноль';
    $ten = array(
        array('', 'один', 'два', 'три', 'четыре', 'пять', 'шесть', 'семь', 'восемь', 'девять'),
        array('', 'одна', 'две', 'три', 'четыре', 'пять', 'шесть', 'семь', 'восемь', 'девять'),
    );
    $a20 = array('десять', 'одиннадцать', 'двенадцать', 'тринадцать', 'четырнадцать', 'пятнадцать', 'шестнадцать', 'семнадцать', 'восемнадцать', 'девятнадцать');
    $tens = array(2 => 'двадцать', 'тридцать', 'сорок', 'пятьдесят', 'шестьдесят', 'семьдесят', 'восемьдесят', 'девяносто');
    $hundred = array('', 'сто', 'двести', 'триста', 'четыреста', 'пятьсот', 'шестьсот', 'семьсот', 'восемьсот', 'девятьсот');
    $unit = array(
        array('копейка', 'копейки', 'копеек', 1),
        array('рубль', 'рубля', 'рублей', 0),
        array('тысяча', 'тысячи', 'тысяч', 1),
        array('миллион', 'миллиона', 'миллионов', 0),
    );
    list($rub, $kop) = explode('.', sprintf("%015.2f", floatval($num)));
    $out = array();
    if (intval($rub) > 0) {
        foreach (str_split($rub, 3) as $uk => $v) {
            if (!intval($v)) continue;
            $uk = sizeof($unit) - $uk - 1;
            $gender = $unit[$uk][3];
            list($i1, $i2, $i3) = array_map('intval', str_split($v, 1));
            $out[] = $hundred[$i1]; # 1xx-9xx
            if ($i2 > 1) $out[] = $tens[$i2] . ' ' . $ten[$gender][$i3]; # 20-99
            else $out[] = $i2 > 0 ? $a20[$i3] : $ten[$gender][$i3]; # 10-19 | 1-9
            if ($uk > 1) $out[] = morph($v, $unit[$uk][0], $unit[$uk][1], $unit[$uk][2]);
        }
    } else $out[] = $nul;
    $out[] = morph(intval($rub), $unit[1][0], $unit[1][1], $unit[1][2]); // rub
    $out[] = $kop . ' ' . morph($kop, $unit[0][0], $unit[0][1], $unit[0][2]); // kop
    return trim(preg_replace('/ {2,}/', ' ', join(' ', $out)));
}

function morph($n, $f1, $f2, $f5)
{
    $n = abs(intval($n)) % 100;
    if ($n > 10 && $n < 20) return $f5;
    $n = $n % 10;
    if ($n > 1 && $n < 5) return $f2;
    if ($n == 1) return $f1;
    return $f5;
}

define('MODX_API_MODE', true);
require '../../../../../index.php';
$modx->getService('error', 'error.modError');
$modx->setLogLevel(modX::LOG_LEVEL_INFO);
$modx->setLogTarget(XPDO_CLI_MODE ? 'ECHO' : 'HTML');


if (in_array(1, $modx->user->getUserGroups()) || in_array(7, $modx->user->getUserGroups()) || $modx->user->isMember('Manager') || $modx->user->isMember('Administrator')) {
    $chunk_name = "waybill.print";
    if (isset($_GET["kurier"])) {
        //$chunk_name = "waybill.kurier.print";
        $chunk_name = "waybill.available.print";
    }

        $profile_manager = $modx->user->getOne('Profile')->get('fullname');
    $order = $modx->getObject('msOrder', $_GET["order_id"]);
    $q = $modx->newQuery('msOrderProduct');
    $q->innerJoin('msProduct', 'product', 'product.id = msOrderProduct.product_id');
    $q->innerJoin('msProductData', 'data', 'data.id = msOrderProduct.product_id');
    $q->where(array(
        'order_id' => $_GET["order_id"],
    ));
    $q->select(array(
        //'product.',
        'msOrderProduct.count',
        'msOrderProduct.stock',
        'msOrderProduct.price',
        'msOrderProduct.product_id',
        'msOrderProduct.price_old',
        'msOrderProduct.name',
        'msOrderProduct.cost',
        'msOrderProduct.options',
        //'v.value',
        'data.article'));

    $discountloalnost = 0;
    if ($order->get('promocode') != '') {
        $object_promocode = $modx->getObject('PromocodeItem', array(
            'code' => $order->get('promocode'),
        ));
        if ($object_promocode->get('removeloylnost') == 1) {
            $discountloalnost = 1;
        }


    }
    $value = array();
    $array = array();
    $products = '';
    $basovay = 0;
    $user = $order->getOne('User');
    if ($user) $user = $user->getOne('Profile');
    $user = $user->toArray();
    $no_available_stocks=$yes_available_stocks=[];
    if ($q->prepare() && $q->stmt->execute()) {
        $i = 0;
        $discount_n = false;
        while ($prod = $q->stmt->fetch(PDO::FETCH_ASSOC)) {
            $i++;
            $name = '';
            $stock=$prod['stock'];
            $options = $modx->fromJSON($prod['options']);
            $size = $options['size'];
            if ($size != '') $name = $prod['name'] . ', ' . $size;
            else $name = $prod['name'];
            $discount = '';
            $cost = $prod['cost'];
            if ((float)$prod['price_old'] > (float)$prod['price']) {
                $discount1 = (float)$prod['price_old'] - (float)$prod['price'];
                $discount = round((float)$discount1 * 100 / (float)$prod['price_old']);
                $discount .= '%';
            }

            if ($discountloalnost == 1) {

                $discount .= ' + ' . $user['extended']['discount'] . '%';
                $cost = round($cost - (($user['extended']['discount'] / 100) * $cost), 2);
            }
            if ($discount != '') $discount_n = true;

            if (isset($_GET["kurier"])) {


                $q1 = $modx->newQuery('modTemplateVar');
                $q1->innerJoin('modTemplateVarResource', 'v', 'v.tmplvarid = modTemplateVar.id');
                $q1->where(array(
                    'v.contentid' => $prod['product_id'],
                    'modTemplateVar.id' => 1,
                ));
                $q1->limit(1);
                $q1->select(array('modTemplateVar.*', 'v.value', 'v.id'));
                if ($q1->prepare() && $q1->stmt->execute()) {
                    while ($tv = $q1->stmt->fetch(PDO::FETCH_ASSOC)) {
                        $value = $tv['value'];

                    }
                }

                $value = $modx->fromJson($value);

                $art_post = '';
                $art_proiz = '';
                $pricepost = '';
                $all_aval=0;
                foreach ($value as $option) {
                    $sizeop = $option['weight'] . ' ' . $option['weight_prefix'];
/*echo "<pre>";
print_r($option);
echo "</pre>";*/
                    $art_proiz=$option['artman'];
                  //  echo $sizeop.' '.$size.'<br/>';
                    if ($sizeop == $size) {

                        $key_availability_1='Склад';
                        $key_availability_2='Магазин';
                        $key_availability_3='Высоцкого 12';

                        $key=$prod['product_id'].'_'.$sizeop;
                        //echo $key;

                        if (isset($option['availability_1'])) {
                            $availability_1 = $option['availability_1'];
                            $all_aval+=$availability_1;

                            if (!isset($yes_available_stock[$key_availability_1]))
                                $yes_available_stock[$key_availability_1]=[];

                            $yes_available_stock[$key_availability_1][$key]=['name'=> $name,'art_proiz'=>$art_proiz,'option'=>$sizeop,'count'=>$prod['count']];
                           // echo $key_availability_1.' '.$key."<br/>";

                        }
                        else {
                            $availability_1=false;

                            if (!isset($no_availabel_stock[$key_availability_1]))
                                $no_availabel_stock[$key_availability_1]=[];

                            $no_availabel_stock[$key_availability_1][$key]=['name'=> $name,'art_proiz'=>$art_proiz,'option'=>$sizeop,'count'=>$prod['count']];
                        }

                        if (isset($option['availability_2'])) {
                            $availability_2 = $option['availability_2'];
                            $all_aval+=$availability_2;

                            if (!isset($yes_available_stock[$key_availability_2]))
                                $yes_available_stock[$key_availability_2]=[];

                            $yes_available_stock[$key_availability_2][$key]=['name'=> $name,'art_proiz'=>$art_proiz,'option'=>$sizeop,'count'=>$prod['count']];
                            //echo $key_availability_2.' '.$key."<br/>";

                        }
                        else {
                            $availability_2=false;

                            if (!isset($no_availabel_stock[$key_availability_2]))
                                $no_availabel_stock[$key_availability_2]=[];

                            $no_availabel_stock[$key_availability_2][$key]=['name'=> $name,'art_proiz'=>$art_proiz,'option'=>$sizeop,'count'=>$prod['count']];
                        }

                        if (isset($option['availability_3'])) {
                            $availability_3 = $option['availability_3'];

                            //$all_aval+=$availability_3;
                            if (!isset($yes_available_stock[$key_availability_3]))
                                $yes_available_stock[$key_availability_3]=[];

                            $yes_available[$key_availability_3][$key]=['name'=> $name,'art_proiz'=>$art_proiz,'option'=>$sizeop,'count'=>$prod['count']];

                            //echo $key_availability_3.' '.$key."<br/>";

                        }
                        else {
                            $availability_3=false;

                            if (!isset($no_availabel_stock[$key_availability_3]))
                                $no_availabel_stock[$key_availability_3]=[];

                            $no_availabel_stock[$key_availability_3][$key]=['name'=> $name,'art_proiz'=>$art_proiz,'option'=>$sizeop,'count'=>$prod['count']];
                        }

                        if ($stock!='') {
                            if (!isset($yes_available_stocks[$stock]))
                                $yes_available_stocks[$stock] = [];

                            $yes_available_stocks[$stock][] = ['name' => $name, 'art_proiz' => $art_proiz, 'option' => $sizeop, 'count' => $prod['count']];
                        }else {


                            $no_available_stocks[]= ['name' => $name, 'art_proiz' => $art_proiz, 'option' => $sizeop, 'count' => $prod['count']];
                        }


                        /*if (isset($option['availability_3'])) {
                            $availability_3 = $option['availability_3'];
                            $all_aval+=$availability_3;
                        }
                        else $availability_3=false;*/





                        //echo $sizeop.' '.$size.' '.$all_aval.' '.$prod['count'].'<br/>';
                        if ($all_aval=='0')
                        {
                            $no_availabel[$key]=['name'=> $name,'art_proiz'=>$art_proiz,'option'=>$sizeop,'count'=>$prod['count']];
                        }
                        elseif ($all_aval>=$prod['count']){
                            $yes_available[$key]=['name'=> $name,'art_proiz'=>$art_proiz,'option'=>$sizeop,'count'=>$prod['count']];
                        }
                        elseif ($all_aval<$prod['count']){
                            $no_available_some[$key]=['name'=> $name,'art_proiz'=>$art_proiz,'option'=>$sizeop,'count'=>$prod['count']];
                        }


                        /*if (($all_aval>0))
                        {

                            $yes_available[]=['name'=> $name,'art_proiz'=>$art_proiz,'option'=>$sizeop,'count'=>$prod['count']];
                        }*/

                        //echo '$sizeop'.$sizeop;

                            $art_post = $option['artpost'];
                            $art_proiz = $option['artman'];
                            $pricepost = $option['price_supplier'];
                    }

                }
/*
                echo "<pre>no_availabel";
                print_r($no_availabel);
                echo "</pre>";
                echo "<pre>yes_available";
                print_r($yes_available);
                echo "</pre>";
                echo "<pre>no_available_some";
                print_r( $no_available_some);
                echo "</pre>";*/



                $products_array[] = array(
                    'num' => $i,
                    'name' => $name,
                    'stock' => $stock,
                    'article' => $prod['article'],
                    'art_post' => $art_post,
                    'art_proiz' => $art_proiz,
                    'count' => $prod['count'] . " шт.",
                    'pricepost' => $pricepost,
                    'price_old' => $prod['price_old'] . " грн.",
                    'discount' => $discount,
                    'cost' => $cost . " грн.",
                );
                $basovay += $prod['price_old'] * $prod['count'];





            } else {
                $art_proiz = '';

                $q1 = $modx->newQuery('modTemplateVar');
                $q1->innerJoin('modTemplateVarResource', 'v', 'v.tmplvarid = modTemplateVar.id');
                $q1->where(array(
                    'v.contentid' => $prod['product_id'],
                    'modTemplateVar.id' => 1,
                ));
                $q1->limit(1);
                $q1->select(array('modTemplateVar.*', 'v.value', 'v.id'));
                if ($q1->prepare() && $q1->stmt->execute()) {
                    while ($tv = $q1->stmt->fetch(PDO::FETCH_ASSOC)) {
                        $value = $tv['value'];

                    }
                }

                $value = $modx->fromJson($value);

                foreach ($value as $option) {
                    $sizeop = $option['weight'] . ' ' . $option['weight_prefix'];

                    if ($sizeop == $size) {

                        $art_post = $option['artpost'];


                        $name = $option['artman'] . ' ' . $name;
                        if (isset($option['text_action'])) {
                            if ($option['text_action']!='') {
                                $name .= '<br/><span style="font-size:10px;margin-top:5px;">' . $option['text_action'] . '</span>';
                            }
                        }
                        $pricepost = $option['price_supplier'];
                        break;
                    }
                }



                $products_array[] = array(
                    'num' => $i,
                    'name' => $name,
                    'stock' => $stock,
                    'article' => $prod['article'],
                    'count' => $prod['count'] . " шт.",
                    'count_int'=>$prod['count'],
                    'price_old' => $prod['price_old'] . " грн.",
                    'discount' => $discount,
                    'cost' => $cost . " грн.",
                );
                $basovay += $prod['price_old'] * $prod['count'];
            }
        }

        $basovay=round($basovay,0);

        if (isset($_GET["kurier"])) {
            $products = '<tr>
							<td style="text-align:center;">№</td>
							<td></td>
							<td>Товар</td>
							<td style="text-align:center;width:50px;">Артикул</td>
							<td style="text-align:center;width:50px;">Артикул поставщика</td>
							<td style="text-align:center;width:50px;">Артикул производителя</td>
							<td style="text-align:center;width:50px;">Кол-во</td>
							<td style="text-align:center;width:80px;">Цена поставщика</td>
							<td style="text-align:center;width:80px;">Цена</td>' .
                ($discount_n === true ? '<td style="text-align:center;width:50px;">Скидка</td>' : '') .
                '<td style="text-align:center; width:90px;">Сумма</td>
						</tr>';


            foreach ($products_array as $key => $value) {

                if ( $value['count_int']>1) $style='font-weight:700;';
                else $style='';

                $products .= "<tr><td style='text-align:center;".$style."'>" . $value['num'] . "</td>";

                $stock=$value['stock'];
                $stock_button=mb_substr($stock,0,1,'UTF-8');
                $products .= "<td>" . $stock_button . "</td>";
                $products .= "<td>" . $value['name'] . "</td>";
                $products .= "<td style='text-align:center;'>" . $value['article'] . "</td>";

                $products .= "<td style='text-align:center;'>" . $value['art_post'] . "</td>";
                $products .= "<td style='text-align:center;'>" . $value['art_proiz'] . "</td>";

                $products .= "<td style='text-align:center;'>" . $value['count'] . "</td>";
                $products .= "<td style='text-align:center;'>" . $value['pricepost'] . "</td>";

                $products .= "<td style='text-align:center;'>" . $value['price_old'] . "</td>";
                if ($discount_n === true) $products .= "<td style='text-align:center;'>" . ($value['discount'] != '' ? $value['discount'] : '-') . "</td>";
                //if ($value[discount!='')$products.="<td style='text-align:center;'>$discount</td>";
                $products .= "<td style='text-align:left;'>" . $value['cost'] . "</td></tr>";
            }

        } else {
            $products = '<tr>
						<td style="text-align:center;">№</td>
						<td></td>
						<td>Товар</td>
						<td style="text-align:center;width:50px;">Артикул</td>
						<td style="text-align:center;width:50px;">Кол-во</td>
						<td style="text-align:center;width:80px;">Цена</td>' .
                ($discount_n === true ? '<td style="text-align:center;width:50px;">Скидка</td>' : '') .
                '<td style="text-align:center; width:90px;">Сумма</td>
					</tr>';//.$products;


            foreach ($products_array as $key => $value) {

                if ( (int)$value['count_int']>1) $style='font-weight:700;';
                else $style='';
                $products .= "<tr><td style='text-align:center;".$style."'>" . $value['num'] . "</td>";

                $stock=$value['stock'];
                $stock_button=mb_substr($stock,0,1,'UTF-8');
                $products .= "<td>" . $stock_button . "</td>";

                $products .= "<td style='".$style."'>" . $value['name'] . "</td>";
                $products .= "<td style='text-align:center;'>" . $value['article'] . "</td>";
                $products .= "<td style='text-align:center;'>" . $value['count'] . "</td>";
                $products .= "<td style='text-align:center;'>" . $value['price_old'] . "</td>";
                //$products.="<td style='text-align:center;'>".$value['price_old']." грн.</td>";
                if ($discount_n === true) $products .= "<td style='text-align:center;'>" . ($value['discount'] != '' ? $value['discount'] : '-') . "</td>";

                $products .= "<td style='text-align:left;'>" . $value['cost'] . "</td></tr>";
            }

        }

    }
    $pls_order = $order->toArray();
    $pls_address = $order->getOne('Address')->toArray('address.');
    $pls_payment = $order->getOne('Payment')->toArray();

    $pls_delivery = $order->getOne('Delivery')->toArray();
    $deliveryprice = '';
    $delivery = '';


    function phone_format($phone, $format, $mask = '#')
    {
        $phone = preg_replace('/[^0-9]/', '', $phone);

        if (is_array($format)) {
            if (array_key_exists(strlen($phone), $format)) {
                $format = $format[strlen($phone)];
            } else {
                return false;
            }
        }

        $pattern = '/' . str_repeat('([0-9])?', substr_count($format, $mask)) . '(.*)/';

        $format = preg_replace_callback(
            str_replace('#', $mask, '/([#])/'),
            function () use (&$counter) {
                return '${' . (++$counter) . '}';
            },
            $format
        );

        return ($phone) ? trim(preg_replace($pattern, $format, $phone, 1)) : false;
    }

//+380976449677
//380976449677
//9261112233
    $formats = array(
        '10' => '+38 (###) ### ## ##',
    );

    $phone = $pls_address["address.phone"];
    $phone = str_replace('+38', '', $phone);
    //echo $phone;

    $phone = phone_format($phone, $formats, '#');
    $pls_address["address.phone"] = $phone;


    if ($user['mobilephone'] != '') {
        $user['mobilephone'] = str_replace('+38', '', $user['mobilephone']);
        $user['mobilephone'] = phone_format($user['mobilephone'], $formats, '#');
        $dophone = ', ' . $user['mobilephone'];
    } else $dophone = '';

    if (strpos($pls_address["address.city"], 'Одесса') !== false) {
        //echo 'ddd '.strpos($pls_address["address.city"],'Одесса');
        if (strpos($pls_address["address.city"], 'Одесса') == 0) {
            $pls_address["address.city"] = 'Одесса';
        }
    }


    switch ($pls_delivery['id']) {
        case 5:// Новая почта курьерская

            if ($pls_address['address.street'] != '') {
                $delivery = $pls_delivery['name'];
                $addressdelivery = '';
                if ($pls_address["address.city"] != '') $addressdelivery .= $pls_address["address.city"] . ', ';
                $addressdelivery .= $pls_address['address.street'];
                if ($pls_address['address.building'] != '') $addressdelivery .= ', дом ' . $pls_address['address.building'];
                if ($pls_address['address.room'] != '') $addressdelivery .= ', кв. ' . $pls_address['address.room'];

            } else {
                $extended = $user['extended'];
                $delivery = $pls_delivery['name'];//.': ';
                $addressdelivery = '';
                if ($pls_address["address.city"] != '') $addressdelivery .= $pls_address["address.city"] . ', ';
                $addressdelivery .= $extended['street'] . ', ' . $extended['house'];//.($extended['room']!='');
                if ($extended['room'] != '') $addressdelivery .= ', кв. ' . $extended['room'];

                $city_id = $extended['city_id'];
            }


            break;
        case 2:// Новая почта

            if ($pls_address['address.warehouse'] != '') {
                $delivery = $pls_delivery['name'];
                $addressdelivery = '';
                if ($pls_address["address.city"] != '') $addressdelivery .= $pls_address["address.city"] . ', ';
                $addressdelivery .= $pls_address['address.warehouse'];
            } else {

                $extended = $user['extended'];
                $city_id = $extended['city_id'];
                $warehouse = $extended['warehouse'];

                $delivery = $pls_delivery['name'];
                $textwarehouse = '';
                $deliverym = $modx->getObject('msDelivery', $pls_delivery['id']);
                $warehouses = $deliverym->getWarehouses($city_id);
                foreach ($warehouses as $warehouse) {

                    if ($warehouse['Number'] == $extended['warehouse']) {
                        $textwarehouse = $warehouse['DescriptionRu'];
                    }

                }
                $addressdelivery = '';
                if ($pls_address["address.city"] != '') $addressdelivery .= $pls_address["address.city"] . ', ';
                $addressdelivery .= $textwarehouse;
                //}
                $deliveryprice = '';
            }


            break;
        case 3:// Интайм
            if ($pls_address['address.warehouse'] != '') {
                $delivery = $pls_delivery['name'];
                $addressdelivery = '';
                if ($pls_address["address.city"] != '') $addressdelivery .= $pls_address["address.city"] . ', ';
                $addressdelivery .= $pls_address['address.warehouse'];
            } else {
                $extended = $user['extended'];
                $delivery = $pls_delivery['name'];//.': ';
                $addressdelivery = '';
                if ($pls_address["address.city"] != '') $addressdelivery .= $pls_address["address.city"] . ', ';
                if ($extended['delivery'] == $pls_delivery['id']) {
                    $warehouse = $modx->getObject('msIntime', $extended['warehouse']);
                    if ($warehouse) {
                        $war = $warehouse->toArray();
                        $addressdelivery .= "Склад №" . $war['warehouse_no'] . ', ' . $war['warehouse'];
                    }
                }
                $deliveryprice = '';
            }
            break;
        case 4: //Курьерская
            //	echo 'str'.$pls_address['street'];
            if ($pls_address['address.street'] != '') {
                $delivery = $pls_delivery['name'];
                $addressdelivery = '';
                if ($pls_address["address.city"] != '') $addressdelivery .= $pls_address["address.city"] . ', ';
                $addressdelivery .= $pls_address['address.street'];
                if ($pls_address['address.building'] != '') $addressdelivery .= ', дом ' . $pls_address['address.building'];
                if ($pls_address['address.room'] != '') $addressdelivery .= ', кв. ' . $pls_address['address.room'];

                if ($pls_address['address.housing'] != '') $addressdelivery .= ', корпус ' . $pls_address['address.housing'];
                if ($pls_address['address.parade'] != '') $addressdelivery .= ', парадная ' . $pls_address['address.parade'];
                if ($pls_address['address.doorphone'] != '') $addressdelivery .= ', домофон ' . $pls_address['address.doorphone'];
                if ($pls_address['address.floor'] != '') $addressdelivery .= ', этаж ' . $pls_address['address.floor'];

                $deliveryprice = 'Курьерская доставка: ' . $pls_order['delivery_cost'] . ' грн';
            } else {

                $extended = $user['extended'];
                $delivery = $pls_delivery['name'];//.': ';
                $addressdelivery = $extended['street'] . ', ' . $extended['house'];//.($extended['room']!='');
                if ($extended['room'] != '') $addressdelivery .= ', кв. ' . $extended['room'];


                if ($extended['housing'] != '') $addressdelivery .= ', корпус ' . $extended['housing'];
                if ($extended['parade'] != '') $addressdelivery .= ', парадная ' . $extended['parade'];
                if ($extended['doorphone'] != '') $addressdelivery .= ', домофон ' . $extended['doorphone'];
                if ($extended['floor'] != '') $addressdelivery .= ', этаж ' . $extended['floor'];


                $city_id = $extended['city_id'];

                $qcity = $modx->getObject('msLocalities', $city_id);
                if ($qcity) {
                    $region = $qcity->get('region');
                    if ($region == 'Одесская') $deliveryprice = 'Курьерская доставка: 50 грн';//$cost_delivery=50;
                    else $deliveryprice = 'Курьерская доставка: 45 грн';

                }else{
                    $qcity = $modx->getObject('msNpCities', $city_id);
                    if ($qcity) {
                        $region = $qcity->get('AreaDescriptionRu');
                        if ($region == 'Одеська') $deliveryprice = 'Курьерская доставка: 50 грн';//$cost_delivery=50;
                        else $deliveryprice = 'Курьерская доставка: 45 грн';
                    }
                }

                if ($pls_order["cart_cost"] >= $modx->getOption('free_delivery_Odessa')) {

                    //delivery_cost
                    $deliveryprice = 'Курьерская доставка: 0 грн';
                }

                $deliveryprice = 'Курьерская доставка: ' . $pls_order['delivery_cost'] . ' грн';

            }

            break;
        case 1:
            $delivery = $pls_delivery['name'];

            $pickup_id = $pls_order['pickup_id'];
            $addressdelivery='';
            if ($pickup_id > 0) {
                $q = $modx->newQuery('msPickup');
                $q->where(array('msPickup.id' => $pickup_id));
                if ($q->prepare() && $q->stmt->execute()) {
                    $num = 1;
                    while ($row = $q->stmt->fetch(PDO::FETCH_ASSOC)) {
                        $addressdelivery = $row['msPickup_address'];
                    }
                }
            } else {
                $addressdelivery = $modx->getOption('ms2_adress_samovivoz');
            }
            //$addressdelivery = $modx->getOption('ms2_adress_samovivoz');
            break;
    }



//    if ($pls_order["comment"] != '') $comment = "<b>Примечание:</b> " . nl2br($pls_order["comment"]) . "<br/>"; else $comment = '';
    if ($pls_address["address.comment"] != '') $comment = "<b>Примечание:</b> " . nl2br($pls_address["address.comment"]) . "<br/>"; else $comment = '';

    $date = explode(' ', $pls_order["createdon"]);

    $user = $order->getOne('User');
    if ($user) $user = $user->getOne('Profile');
    $user = $user->toArray();
    $extended = $user['extended'];
    $lastname = '';
    if ($extended['lastname'] != '') $lastname = $extended['lastname'] . ' ';

    if ((float)$basovay != (float)$pls_order["cart_cost"]) {
        $skidka = (float)$basovay - (float)$pls_order["cart_cost"];//round(,2);//$pls_order["cost"];
        $skidka = round((float)$skidka, 2);
        if ($skidka == 0) $skidka = '';
        //  echo  '$skidka'.$skidka;
    } else $skidka = '';


    if (($pls_delivery['id'] == 4) && ($deliveryprice == '')) {
        if ($pls_order['delivery_cost'] > 0) $deliveryprice = 'Курьерская доставка: ' . $pls_order['delivery_cost'] . ' грн';
    }
    //$deliveryprice
    if ($pls_address["address.lastname"] != '')
        $fio = $pls_address["address.lastname"] . ' ' . $pls_address["address.receiver"];
    else $fio = $lastname . ' ' . $pls_address["address.receiver"];

    $action = "print";
    $user_id = $modx->user->id;

    $ips = array('ip' => $_SERVER['REMOTE_ADDR'], 'suspected' => $_SERVER['REMOTE_ADDR'], 'network' => $_SERVER['REMOTE_ADDR']);

    $entry = 'Открыто на печать';
    $log = $modx->newObject('msOrderLog', array(
        'order_id' => $pls_order['id']
    , 'user_id' => $user_id
    , 'timestamp' => time()
    , 'action' => $action
    , 'message' => $entry
    , 'ip' => $modx->toJson($ips)
    ));


    $shipping_date = '';


    $date = $order->get('date_shipping');
    $date = str_replace(' 00:00:00', '', $date);
    $date = date('d.m', strtotime($date));
    $shipping_date = '<b>Дата доставки:</b> ' . $date;
    if (($pls_delivery['id'] == 5) || ($pls_delivery['id'] == 2) || ($pls_delivery['id'] == 3))
        $shipping_date = '<b>Дата отправки:</b> ' . $date;
    elseif ($pls_delivery['id'] == 1) $shipping_date = '<b>Самовывоз:</b> ' . $date;
    $shipping_time = '';


    if (strpos($order->get('time_shipping_from'), ':00:00') !== false)
        $shipping_time_from = str_replace(':00:00', ':00', $order->get('time_shipping_from'));
    else $shipping_time_from = str_replace(':00', '', $order->get('time_shipping_from'));

    if (strpos($order->get('time_shipping_to'), ':00:00') !== false)
        $shipping_time_to = str_replace(':00:00', ':00', $order->get('time_shipping_to'));
    else $shipping_time_to = str_replace(':00', '', $order->get('time_shipping_to'));

    $shipping_time_from1 = explode(' ', $shipping_time_from);
    $shipping_time = $shipping_time_from1[1];//$order->get('time_shipping_from');

    $shipping_time_to1 = explode(' ', $shipping_time_to);
    $shipping_time_to = $shipping_time_to1[1];

    if ($order->get('time_shipping_to') != '00:00:00') $shipping_time .= ' - ' . $shipping_time_to;//order->get('time_shipping_to');
    $shipping_date .= ' <span style="margin-left:8px;">' . $shipping_time . '</span> <br/>';


    $log->save();

    //$bonuses
    $bonuses=0;

    if (($pls_order["bonuses"]>0)&&($pls_order["apply_bonuses"]=='1'))
    {
        $bonuses=$pls_order["bonuses"];

    }
    if (($skidka!='')&&($bonuses>0))
    {
        $skidka=$skidka-$bonuses;
    }

    $no_availabel_str=$yes_available_str=$no_available_some_str='';
    if (count($no_availabel)>0) {
        $no_availabel_str ='<h3> Нет на складе </h3><p style="font-size:14px;">';
        foreach ($no_availabel as $str) {
            //$no_availabel_str .= $str['art_proiz'] . ' ' . $str['name'] . ', ' . $str['option'] . ' - ' . $str['count'] . 'шт.<br />';
            $no_availabel_str .= $str['art_proiz'] . ' ' . $str['name'];// . ' - ' . $str['count'] . 'шт.<br />';
            if ($str['count']>1)$no_availabel_str .= ' - ' . $str['count'] . 'шт.<br />';
            else $no_availabel_str .='<br />';
        }
        $no_availabel_str .= '</p>';
    }

    if (count($no_available_some)>0) {
        $no_available_some_str = '<h3>Есть на складе, количество не достаточно</h3><p style="font-size:14px;">';
        foreach ($no_available_some as $str) {
            //$no_available_some_str .= $str['art_proiz'] . ' ' . $str['name'] . ', ' . $str['option'] . ' - ' . $str['count'] . 'шт.<br />';
            $no_available_some_str .= $str['art_proiz'] . ' ' . $str['name'];//  . ' - ' . $str['count'] . 'шт.<br />';
            if ($str['count']>1)$no_available_some_str .= ' - ' . $str['count'] . 'шт.<br />';
            else $no_available_some_str .='<br />';
        }
        $no_available_some_str .='</p>';
    }
    if (count($yes_available)>0) {
        $yes_available_str = '<h3>Есть на складе</h3><p style="font-size:14px;">';
        foreach ($yes_available as $str) {
            //$yes_available_str .= $str['art_proiz'] . ' ' . $str['name'] . ', ' . $str['option'] . ' - ' . $str['count'] . 'шт.<br />';
            $yes_available_str .= $str['art_proiz'] . ' ' . $str['name'];
            if ($str['count']>1)$yes_available_str .= ' - ' . $str['count'] . 'шт.<br />';
            else $yes_available_str .='<br />';
        }
        $yes_available_str .= '</p>';
    }

    /*if (count($no_available_stock)>0) {
        $no_available_str_stock = '<h3>Есть В наличии по складам</h3><p style="font-size:14px;">';
        foreach ($no_available_stock as $name_stock=>$str) {
            $no_available_str_stock .='<h4>'.$name_stock.'</h4>';
            foreach ($str as $_str) {
                $no_available_str_stock .= $_str['art_proiz'] . ' ' . $_str['name'];
                if ($_str['count'] > 1) $no_available_str_stock .= ' - ' . $_str['count'] . 'шт.<br />';
                else $no_available_str_stock .= '<br />';
            }
        }
        $no_available_str_stock .= '</p>';
    }*/

    if (count($yes_available_stock)>0) {
        $yes_available_str_stock = '<h3>Есть В наличии по складам</h3><p style="font-size:14px;">';

        foreach ($yes_available_stocks as $name_stock=>$products) {
            $yes_available_str_stock .= '<h4>' . $name_stock . '</h4>';
            foreach ($products as $product) {
                    $yes_available_str_stock .= $product['art_proiz'] . ' ' . $product['name'];
                    if ($product['count'] > 1) $yes_available_str_stock .= ' - ' . $product['count'] . 'шт.<br />';
                    else $yes_available_str_stock .= '<br />';
            }

        }


//        foreach ($yes_available_stock as $name_stock=>$str) {
//            if (count($str)>0) {
//                $yes_available_str_stock .= '<h4>' . $name_stock . '</h4>';
//                foreach ($str as $_str) {
//                    $yes_available_str_stock .= $_str['art_proiz'] . ' ' . $_str['name'];
//                    if ($_str['count'] > 1) $yes_available_str_stock .= ' - ' . $_str['count'] . 'шт.<br />';
//                    else $yes_available_str_stock .= '<br />';
//                }
//            }
//        }
        $yes_available_str_stock .= '</p>';
    }

    if (count($no_available_stocks)>0)
    {
        $yes_available_str_stock = '<h3>Не выбран источник</h3><p style="font-size:14px;">';
        foreach ($no_available_stocks as $product) {
            $yes_available_str_stock .= $product['art_proiz'] . ' ' . $product['name'];
            if ($product['count'] > 1) $yes_available_str_stock .= ' - ' . $product['count'] . 'шт.<br />';
            else $yes_available_str_stock .= '<br />';
        }
        $yes_available_str_stock .= '</p>';
    }



    //date('Y-m-d H:i:s')
    $createdon=date('Y-m-d H:i',time($pls_order['createdon']));
    $time_createdon=date('H:i',time($pls_order['createdon']));
    $output = $modx->getChunk($chunk_name, array(
        'no_available_some'=> $no_available_some_str,
        'yes_available'=>$yes_available_str,
        'no_availabel'=>$no_availabel_str,
        'yes_available_str_stock'=>$yes_available_str_stock,
        'createdon'=>$createdon,
        'time'=>$time_createdon,
        'fullprice' => num2str($pls_order["cost"]),
        'price' => $pls_order["cost"],
        'delivery_id' => $pls_delivery['id'],
        'delivery' => $delivery,
        'products' => $products,
        'deliveryprice' => $deliveryprice,
        'payment' => $pls_payment['name'],
        //'createdon' => $date[0],
        'shipping_date' => $shipping_date,
        'number' => $pls_order["num"],
        'addressdelivery' => $addressdelivery,
        'fullname' => $fio,
        'phone' => $pls_address["address.phone"] . $dophone,
        'street' => $pls_address["address.street"],
        'building' => $pls_address["address.building"],
        'room' => $pls_address["address.room"],
        'city' => $pls_address["address.city"],
        'region' => $pls_address["address.region"],
        'country' => $pls_address["address.country"],
        'index' => $pls_address["address.index"],
        'comment' => $comment,
        'skidka' => $skidka,
        'bonuses'=>$bonuses,
        'profile_manager'=>$profile_manager
    ));
    echo $output;
} else {
    header('HTTP/1.1 403 Forbidden');
    echo "Access denied!";
}
?>