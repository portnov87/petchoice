<?php
if (!class_exists('msOrderInterface')) {
	require_once dirname(dirname(dirname(__FILE__))) . '/model/minishop2/msorderhandler.class.php';
}

class Intime extends msOrderHandler implements msOrderInterface {

	public function getCost($with_cart = true, $only_cost = false) {
		$cart = $this->ms2->cart->status();
		$cost = $with_cart
		? $cart['total_cost']
		: 0;
		$deliveryCost = 0;

		/* @var msDelivery $delivery */
		if ($delivery = $this->modx->getObject('msDelivery', $this->order['delivery'])) {
			$cost = $delivery->getCost($this, $cost);
			$deliveryCost = $delivery->getCost($this, 0);
		}

		/* @var msPayment $payment */
		if ($payment = $this->modx->getObject('msPayment', $this->order['payment'])) {
			$cost = $payment->getCost($this, $cost);
		}

		return $only_cost
		? $cost
		: $this->success('', array(
			'cost' => $cost
			, 'delivery_cost' => $deliveryCost
			, 'delivery_id' => $this->order['delivery']['id'],
		)
		);
	}
}