<?php


define('MODX_API_MODE', true);
require '../../../../../index.php';
$modx->getService('error', 'error.modError');
$modx->setLogLevel(modX::LOG_LEVEL_INFO);
$modx->setLogTarget(XPDO_CLI_MODE ? 'ECHO' : 'HTML');

$array = array();

$orders = $_REQUEST['orders'];
$orders = explode(',', $orders);
/*echo '<pre>';
print_R($orders);
echo '</pre>';*/
$sales = count($orders);
/*Поля:

    Дата доставки
    Номер заказа
    Способ доставки
    Способ оплаты
    Адрес, номер отлеления
    Комментарий (http://ipic.su/apxvo.jpg)*/
//msDelivery
$chunk_name = "ttn.print";
$c = $modx->newQuery('msOrder');
$c->leftJoin('msOrderStatus', 'msOrderStatus', '`msOrder`.`status` = `msOrderStatus`.`id`');
$c->leftJoin('msDelivery', 'msDelivery', '`msOrder`.`delivery` = `msDelivery`.`id`');
$c->leftJoin('msPayment', 'msPayment', '`msOrder`.`payment` = `msPayment`.`id`');
$c->leftJoin('msOrderAddress', 'msOrderAddress', '`msOrder`.`id` = `msOrderAddress`.`id`');

$c->select(array('msOrder.*', 'msDelivery.name as deliveryname', 'msPayment.name as paymentname',
    'msOrderAddress.*'));
//$c->select('*');//array('msOrder.*','msDelivery.name as deliveryname'));

//msOrder.*,msPayment.id as paymentid,`msDelivery`.`id` as deliveryid');
$c->where(array('msOrder.id:IN' => $orders));

if ($c->prepare() && $c->stmt->execute()) {
    $results = $c->stmt->fetchAll(PDO::FETCH_ASSOC);
    $products = '';
    foreach ($results as $res) {
        if ($res['delivery'] == 1) {


            $pickup_id = $res['pickup_id'];
            $address='';
            if ($pickup_id > 0) {
                $q = $modx->newQuery('msPickup');
                $q->where(array('msPickup.id' => $pickup_id));
                if ($q->prepare() && $q->stmt->execute()) {
                    $num = 1;
                    while ($row = $q->stmt->fetch(PDO::FETCH_ASSOC)) {
                        $address = $row['msPickup_address'];
                    }
                }
            } else {
                $address = $modx->getOption('ms2_adress_samovivoz');
            }

            //$address=$modx->getOption('ms2_adress_samovivoz');
        } elseif (isset($res['warehouse']) && ($res['warehouse'] != '')) {
            $address = $res['city'] . '. ' . $res['warehouse'];
        } elseif (isset($res['city']) && ($res['city'] != '')) {
            $address = $res['city'] . ' ул. ' . $res['street'] . ' ' . $res['building'];
        }
        $comment = $res['comment'];
       


        $products .= "<tr><td style='text-align:center;'>" . $res['date_shipping'] . "</td>";
        $products .= "<td>" . $res['num'] . "</td>";
        $products .= "<td style='text-align:center;'>" . $res['deliveryname'] . "</td>";

        $products .= "<td style='text-align:center;'>" . $res['paymentname'] . "</td>";
        $products .= "<td style='text-align:center;'>" . $address . "</td>";
        $products .= "<td style='text-align:center;'>" . $comment . "</td>";
        $products .= "</tr>";


    }


}


$output = $modx->getChunk($chunk_name, array(
    /*  'fullprice' => num2str($pls_order["cost"]),
      'price' => $pls_order["cost"],
      'delivery'=>$delivery,
      'products'=>$products,
      'deliveryprice'=>$deliveryprice,
      'payment'=>$pls_payment['name'],
      'createdon'=> $date[0],
           'shipping_date'=>$shipping_date,
      'number'=> $pls_order["num"],
      'addressdelivery'=>$addressdelivery,
      'fullname' => $fio,
      'phone' => $pls_address["address.phone"].$dophone,
      'street' => $pls_address["address.street"],
      'building' => $pls_address["address.building"],
      'room' => $pls_address["address.room"],
      'city' => $pls_address["address.city"],
      'region' => $pls_address["address.region"],
      'country' => $pls_address["address.country"],
      'index' => $pls_address["address.index"],
      'comment' =>$comment,
      'skidka'=>$skidka*/
    'products' => $products
));
echo $output;
/*
echo '<pre>';
print_r($results);
echo '</pre>';*/

$total = 0;
$cache = 0;
$carts = 0;
$nal = 0;