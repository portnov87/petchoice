<?php

class msPaymentGetListProcessor extends modObjectGetListProcessor {
	public $classKey = 'msPayment';
	public $defaultSortField = 'rank';
	public $defaultSortDirection  = 'asc';
	public $permission = 'mssetting_list';


	/** {@inheritDoc} */
	public function initialize() {
		if (!$this->modx->hasPermission($this->permission)) {
			return $this->modx->lexicon('access_denied');
		}
		return parent::initialize();
	}


	/** {@inheritDoc} */
	public function prepareQueryBeforeCount(xPDOQuery $c) {
		if ($this->getProperty('combo')) {
			$c->select('id,name');
			$c->where(array('active' => 1));
			if ($delivery_id = $this->getProperty('delivery_id')) {
				/* @var msDelivery $delivery */
				if ($delivery = $this->modx->getObject('msDelivery', $delivery_id)) {
					$q = $this->modx->newQuery('msDeliveryMember', array('delivery_id' => $delivery_id));
					$q->select('payment_id');
					if ($q->prepare() && $q->stmt->execute()) {
						$ids = $q->stmt->fetchAll(PDO::FETCH_COLUMN);
						$c->where(array('id:IN' => $ids));
					}
				}
			}
		}
		return $c;
	}


	/** {@inheritDoc} */
	public function prepareRow(xPDOObject $object) {
		$array = $object->toArray();
		return $array;
	}
        
        
        
       
	  public function iterate(array $data) {
            $list = array();
            $list = $this->beforeIteration($list);
            $this->currentIndex = 0;
            /** @var xPDOObject|modAccessibleObject $object */
            $list[]=array('id'=>0,'name'=>'Все');
            foreach ($data['results'] as $object) {
                if ($this->checkListPermission && $object instanceof modAccessibleObject && !$object->checkPolicy('list')) continue;
                $objectArray = $this->prepareRow($object);
                if (!empty($objectArray) && is_array($objectArray)) {
                    $list[] = $objectArray;
                    $this->currentIndex++;
                }
            }
             
            $list = $this->afterIteration($list);
            return $list;
        }

}

return 'msPaymentGetListProcessor';