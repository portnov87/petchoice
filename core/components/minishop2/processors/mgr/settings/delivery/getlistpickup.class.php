<?php

class msDeliveryGetListPickupProcessor extends modObjectGetListProcessor {
	public $classKey = 'msPickup';
	public $defaultSortField = 'name';
	public $defaultSortDirection  = 'asc';
	//public $permission = 'mssetting_list';


	/** {@inheritDoc} */
	public function initialize() {
		if (!$this->modx->hasPermission($this->permission)) {
			return $this->modx->lexicon('access_denied');
		}
		return parent::initialize();
	}


	/** {@inheritDoc} */
	public function prepareQueryBeforeCount(xPDOQuery $c) {
		if ($this->getProperty('combo')) {
			$c->select('id,name');
			$c->where(array('status' => 1));
		}
		return $c;
	}


	/** {@inheritDoc} */
	public function prepareRow(xPDOObject $object) {
		$array = $object->toArray();

		/*$payments = $object->getMany('Payments');
		$enabled = array();
		foreach ($payments as $payment) {
			$enabled[$payment->payment_id] = 1;
		}

		$array['payments'] = $enabled;*/

		return $array;
	}

}

return 'msDeliveryGetListPickupProcessor';