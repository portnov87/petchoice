<?php

class msNotificationsGetProcessor extends modObjectGetProcessor {
	public $classKey = 'msNotifications';
	public $languageTopics = array('minishop2');
	public $permission = 'mssetting_view';

	/** {@inheritDoc} */
	public function initialize() {
		if (!$this->modx->hasPermission($this->permission)) {
			return $this->modx->lexicon('access_denied');
		}
		return parent::initialize();
	}

}

return 'msNotificationsGetProcessor';