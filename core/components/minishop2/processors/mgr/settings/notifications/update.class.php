<?php

class msNotificationsUpdateProcessor extends modObjectUpdateProcessor {
	public $classKey = 'msNotifications';
	public $languageTopics = array('minishop2');
	public $permission = 'mssetting_save';

	/** {@inheritDoc} */
	public function initialize() {
		if (!$this->modx->hasPermission($this->permission)) {
			return $this->modx->lexicon('access_denied');
		}
		return parent::initialize();
	}



    /** {@inheritDoc} */
    public function beforeSet() {
        if ($this->modx->getObject('msNotifications',array('name' => $this->getProperty('name'), 'id:!=' => $this->getProperty('id') ))) {
            $this->modx->error->addField('name', $this->modx->lexicon('ms2_err_ae'));
        }

        return parent::beforeSet();
    }


    public function beforeSave() {


        $sms = $this->getProperty('sms') == 'Да' ? 1 : 0;
        $this->object->set('sms', $sms);
        $email = $this->getProperty('email') == 'Да' ? 1 : 0;
        $this->object->set('email', $email);

        $viber = $this->getProperty('viber') == 'Да' ? 1 : 0;
        $this->object->set('viber', $viber);


        $is_transactional = $this->getProperty('is_transactional') == 'Да' ? 1 : 0;
        $this->object->set('is_transactional', $is_transactional);


//        $po_nilishiy = $this->getProperty('po_nilishiy') == 'Да' ? 1 : 0;
//        $this->object->set('po_nilishiy', $po_nilishiy);
//
//
//        $show_icon_cachback= $this->getProperty('show_icon_cachback') == 'Да' ? 1 : 0;
//        $this->object->set('show_icon_cachback', $show_icon_cachback);
//
//        $show_icon_delivery= $this->getProperty('show_icon_delivery') == 'Да' ? 1 : 0;
//        $this->object->set('show_icon_delivery', $show_icon_delivery);

        //$this->modx->log(modX::LOG_LEVEL_ERROR, print_r($this->getProperties(),1));
        return !$this->hasErrors();
    }
}

return 'msNotificationsUpdateProcessor';