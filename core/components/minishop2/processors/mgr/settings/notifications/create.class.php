<?php

class msNotificationsCreateProcessor extends modObjectCreateProcessor {
	public $classKey = 'msNotifications';
	public $languageTopics = array('minishop2');
	public $permission = 'mssetting_save';

	/** {@inheritDoc} */
	public function initialize() {
		if (!$this->modx->hasPermission($this->permission)) {
			return $this->modx->lexicon('access_denied');
		}
		return parent::initialize();
	}

	/** {@inheritDoc} */
    public function beforeSet() {
        if ($this->modx->getObject('msNotifications',array('name' => $this->getProperty('name')))) {
            $this->modx->error->addField('name', $this->modx->lexicon('ms2_err_ae'));
        }
//        echo '<pre>';
//        print_r($this->hasErrors());
//        echo '</pre>';
        return !$this->hasErrors();
    }



    public function beforeSave() {



       /* $sms = $this->getProperty('sms') == 'Да' ? 1 : 0;
        $this->object->set('sms', $sms);
        $email = $this->getProperty('email') == 'Да' ? 1 : 0;
        $this->object->set('email', $email);

        $viber = $this->getProperty('viber') == 'Да' ? 1 : 0;
        $this->object->set('viber', $viber);

        $image_include = $this->getProperty('image_include') == 'Да' ? 1 : 0;
        $this->object->set('image_include', $image_include);*/


        return !$this->hasErrors();
    }


}

return 'msNotificationsCreateProcessor';