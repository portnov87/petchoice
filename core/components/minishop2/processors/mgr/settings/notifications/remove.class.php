<?php

class msNotificationsRemoveProcessor extends modObjectRemoveProcessor {
	public $classKey = 'msNotifications';
	public $languageTopics = array('minishop2');
	public $permission = 'mssetting_save';

	/** {@inheritDoc} */
	public function initialize() {
		if (!$this->modx->hasPermission($this->permission)) {
			return $this->modx->lexicon('access_denied');
		}
		return parent::initialize();
	}

}
return 'msNotificationsRemoveProcessor';