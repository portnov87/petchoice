<?php

//class msProductSortProcessor extends modObjectProcessor {
	class msAttributeGroupSortProcessor extends modObjectProcessor {
	
public $classKey = 'msAttributeGroup';
public function process() {
		/* @var msOrderStatus $source */
		$source = $this->modx->getObject($this->classKey, $this->getProperty('source'));
		/* @var msOrderStatus $target */
		$target = $this->modx->getObject($this->classKey, $this->getProperty('target'));

		if (empty($source) || empty($target)) {
			return $this->modx->error->failure();
		}

		if ($source->get('sort') < $target->get('sort')) {
			$this->modx->exec("UPDATE {$this->modx->getTableName($this->classKey)}
				SET sort = sort - 1 WHERE
					sort <= {$target->get('sort')}
					AND sort > {$source->get('sort')}
					AND sort > 0
			");

		} else {
			$this->modx->exec("UPDATE {$this->modx->getTableName($this->classKey)}
				SET sort = sort + 1 WHERE
					sort >= {$target->get('sort')}
					AND sort < {$source->get('sort')}
			");
		}
		$newRank = $target->get('sort');
		$source->set('sort',$newRank);
		$source->save();

		if (!$this->modx->getCount($this->classKey, array('rsort' => 0))) {
			$this->setRanks();
		}
		return $this->modx->error->success();
	}


	/** {@inheritDoc} */
	public function setRanks() {
		$q = $this->modx->newQuery($this->classKey);
		$q->select('id');
		$q->sortby('sort ASC, id', 'ASC');

		if ($q->prepare() && $q->stmt->execute()) {
			$ids = $q->stmt->fetchAll(PDO::FETCH_COLUMN);
			$sql = '';
			$table = $this->modx->getTableName($this->classKey);
			foreach ($ids as $k => $id) {
				$sql .= "UPDATE {$table} SET `sort` = '{$k}' WHERE `id` = '{$id}';";
			}
			$this->modx->exec($sql);
		}
	}


}

//return 'msProductSortProcessor';
return 'msAttributeGroupSortProcessor';