<?php

class msAttributeUpdateProcessor extends modObjectUpdateProcessor {
	public $classKey = 'msAttribute';
	public $languageTopics = array('minishop2');
	public $permission = 'mssetting_save';


	/** {@inheritDoc} */
	public function initialize() {
		if (!$this->modx->hasPermission($this->permission)) {
			return $this->modx->lexicon('access_denied');
		}
		return parent::initialize();
	}


	/** {@inheritDoc} */
	public function beforeSet() {
		//if ($this->modx->getObject('msAttribute',array('name' => $this->getProperty('name'), 'id:!=' => $this->getProperty('id') ))) {
		//	$this->modx->error->addField('name', $this->modx->lexicon('ms2_err_ae'));
		//}
		return parent::beforeSet();
	}

    public function beforeSave() {



        $sort_related = $this->getProperty('sort_related') == 'Да' ? 1 : 0;
        $this->object->set('sort_related', $sort_related);

        $sort_related = $this->getProperty('sef') == 'Да' ? 1 : 0;
        $this->object->set('sef', $sort_related);


        return !$this->hasErrors();
    }
}

return 'msAttributeUpdateProcessor';