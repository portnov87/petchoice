<?php

class msPaymentStatusGetListProcessor extends modObjectGetListProcessor
{
    public $classKey = 'msPaymentStatus';
    public $defaultSortField = 'name';
    public $defaultSortDirection = 'asc';
    public $permission = 'mssetting_list';


    /** {@inheritDoc} */
    public function initialize()
    {
        if (!$this->modx->hasPermission($this->permission)) {
            return $this->modx->lexicon('access_denied');
        }
        return parent::initialize();
    }


    /** {@inheritDoc} */
    public function prepareQueryBeforeCount(xPDOQuery $c)
    {
        if ($this->getProperty('combo')) {
            $c->select('id,name');
            $c->where(array('active' => 1));
            //echo 'order3';
            if ($order_id = $this->getProperty('order_id')) {
                /* @var msOrder $order */
                //echo 'order1';
                if ($order = $this->modx->getObject('msOrder', $order_id)) {
                    //echo 'order2';
                    /* @var msOrderStatus $status */
                    $status = $order->getOne('PaymentStatus');
                    /*if ($status->get('final') == 1) {
                        $c->where(array('id' => $status->get('id')));
                    } else if ($status->get('fixed') == 1) {
                        $c->where(array('rank:>=' => $status->get('rank')));
                    }*/
                }
            }
        }
        return $c;
    }


    /** {@inheritDoc} */
    public function prepareRow(xPDOObject $object)
    {
        if ($this->getProperty('combo')) {
            $array = array(
                'id' => $object->get('id')
            , 'name' => $object->get('name')
            );
        } else {
            $array = $object->toArray();
        }
        return $array;
    }


    /** {@inheritDoc} */
    public function outputArray(array $array, $count = false)
    {
        //if ($this->getProperty('addall')) {
//        $array = array_merge_recursive(array(array(
//            'id' => 0
//        , 'name' => $this->modx->lexicon('ms2_all')
//        )), $array);
        //}
        return parent::outputArray($array, $count);
    }

    public function iterate(array $data)
    {
        $list = array();
        $list = $this->beforeIteration($list);
        $this->currentIndex = 0;
        /** @var xPDOObject|modAccessibleObject $object */
        $list[] = array('id' => 0, 'name' => 'Все');
        foreach ($data['results'] as $object) {
            if ($this->modx->user->hasSessionContext('mgr') && !$this->modx->user->isMember('Administrator')) {
                    continue;
            }
            //if ($this->checkListPermission && $object instanceof modAccessibleObject && !$object->checkPolicy('list')) continue;
            $objectArray = $this->prepareRow($object);
            if (!empty($objectArray) && is_array($objectArray)) {
                $list[] = $objectArray;
                $this->currentIndex++;
            }
        }

        $list = $this->afterIteration($list);
        return $list;
    }

}

return 'msPaymentStatusGetListProcessor';