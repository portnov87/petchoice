<?php

class msVendorUpdateProcessor extends modObjectUpdateProcessor {
	public $classKey = 'msVendor';
	public $languageTopics = array('minishop2');
	public $permission = 'mssetting_save';


	/** {@inheritDoc} */
	public function initialize() {
		if (!$this->modx->hasPermission($this->permission)) {
			return $this->modx->lexicon('access_denied');
		}
		return parent::initialize();
	}


	/** {@inheritDoc} */
	public function beforeSet() {
		if ($this->modx->getObject('msVendor',array('name' => $this->getProperty('name'), 'id:!=' => $this->getProperty('id') ))) {
			$this->modx->error->addField('name', $this->modx->lexicon('ms2_err_ae'));
		}
		   //$this->setCheckbox('/top');
       // $this->setCheckbox('showinmain');
       // $this->setCheckbox('new');
	   
	   //if  ()
	   //$this->object->set('top',1);
   //$this->object->set('new',1);
		return parent::beforeSet();
	}
	
	public function beforeSave() {
		
		    
        $top = $this->getProperty('top') == 'Да' ? 1 : 0; 
        $this->object->set('top', $top);
		$new = $this->getProperty('new') == 'Да' ? 1 : 0; 
        $this->object->set('new', $new);
		$showinmain = $this->getProperty('showinmain') == 'Да' ? 1 : 0; 
        $this->object->set('showinmain', $showinmain);

        $po_nilishiy = $this->getProperty('po_nilishiy') == 'Да' ? 1 : 0;
        $this->object->set('po_nilishiy', $po_nilishiy);


        $show_icon_cachback= $this->getProperty('show_icon_cachback') == 'Да' ? 1 : 0;
        $this->object->set('show_icon_cachback', $show_icon_cachback);

        $show_icon_delivery= $this->getProperty('show_icon_delivery') == 'Да' ? 1 : 0;
        $this->object->set('show_icon_delivery', $show_icon_delivery);
		
		//$this->modx->log(modX::LOG_LEVEL_ERROR, print_r($this->getProperties(),1));
        return !$this->hasErrors();
    }

}

return 'msVendorUpdateProcessor';