<?php

class msUserDiscountUpdateProcessor extends modObjectUpdateProcessor {
	public $classKey = 'msUserDiscount';
	public $languageTopics = array('minishop2');
	public $permission = 'mssetting_save';

	/** {@inheritDoc} */
	public function initialize() {
		if (!$this->modx->hasPermission($this->permission)) {
			return $this->modx->lexicon('access_denied');
		}
		return parent::initialize();
	}

	/** {@inheritDoc} */
	public function beforeSet() {
		if ($this->modx->getObject('msUserDiscount', array('percent' => $this->getProperty('percent'), 'id:!=' => $this->getProperty('id')))) {
			$this->modx->error->addField('percent', $this->modx->lexicon('ms2_err_ae'));
		}
		return parent::beforeSet();
	}

}

return 'msUserDiscountUpdateProcessor';