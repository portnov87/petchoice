<?php

class msCityRemoveProcessor extends modObjectRemoveProcessor  {
	public $classKey = 'msNpCities';
	public $languageTopics = array('minishop2');
	public $permission = 'mssetting_save';


	/** {@inheritDoc} */
	public function initialize() {
		if (!$this->modx->hasPermission($this->permission)) {
			return $this->modx->lexicon('access_denied');
		}
		return parent::initialize();
	}

}
return 'msCityRemoveProcessor';