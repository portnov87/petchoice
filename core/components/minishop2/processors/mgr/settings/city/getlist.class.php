<?php

class msCityGetListProcessor extends modObjectGetListProcessor {
	public $classKey = 'msNpCities';//Localities';
	public $defaultSortField = 'DescriptionRU';
	public $defaultSortDirection  = 'asc';
	public $permission = 'mssetting_list';


	/** {@inheritDoc} */
	public function initialize() {
		if (!$this->modx->hasPermission($this->permission)) {
			return $this->modx->lexicon('access_denied');
		}
		return parent::initialize();
	}


	/** {@inheritDoc} */
	public function prepareQueryBeforeCount(xPDOQuery $c) {
		/*if ($this->getProperty('combo')) {
			$c->select('id,title');
			//$c->where(array('active' => 1));
		}*/
		return $c;
	}


	/** {@inheritDoc} */
	public function prepareRow(xPDOObject $object) {
		$array = $object->toArray();
		
		return $array;
	}

}

return 'msCityGetListProcessor';