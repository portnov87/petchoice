<?php

require_once MODX_CORE_PATH.'model/modx/modprocessor.class.php';
require_once MODX_CORE_PATH.'model/modx/processors/resource/update.class.php';

class msProductUpdateProcessor extends modResourceUpdateProcessor {
	public $classKey = 'msProduct';
	public $languageTopics = array('resource','minishop2:default');
	public $permission = 'msproduct_save';
	public $beforeSaveEvent = 'OnBeforeDocFormSave';
	public $afterSaveEvent = 'OnDocFormSave';
	/** @var msProduct $object */
	public $object;


	/** {inheritDoc} */
	public function initialize() {
		$primaryKey = $this->getProperty($this->primaryKeyField,false);
		if (empty($primaryKey)) return $this->modx->lexicon($this->classKey.'_err_ns');

		if (!$this->modx->getCount($this->classKey, array('id' => $primaryKey, 'class_key' => $this->classKey)) && $res = $this->modx->getObject('modResource', $primaryKey)) {
			$res->set('class_key', $this->classKey);
			$res->save();
		}

		return parent::initialize();
	}


	/** {inheritDoc} */
	public function handleCheckBoxes() {
		parent::handleCheckBoxes();
		
		
		
		$this->setCheckbox('new');
		$this->setCheckbox('popular');
		$this->setCheckbox('adiscount');
		$this->setCheckbox('favorite');
	}


	/** {inheritDoc} */
	public function checkPublishedOn() {
		$published = $this->getProperty('published',null);
		if ($published !== null && $published != $this->object->get('published')) {
			if (empty($published)) { /* if unpublishing */
				$this->setProperty('publishedon',0);
				$this->setProperty('publishedby',0);
			} else { /* if publishing */
				$publishedOn = $this->getProperty('publishedon',null);
				$this->setProperty('publishedon',!empty($publishedOn) ? strtotime($publishedOn) : time());
				$this->setProperty('publishedby',$this->modx->user->get('id'));
			}
		} else { /* if no change, unset publishedon/publishedby */
			if (empty($published)) { /* allow changing of publishedon date if resource is published */
				$this->unsetProperty('publishedon');
				$this->unsetProperty('publishedby');
			}
		}
		return $this->getProperty('publishedon');
	}


	/** {inheritDoc} */
	public function checkFriendlyAlias() {
		if ($this->workingContext->getOption('ms2_product_id_as_alias')) {
			$alias = $this->object->id;
			$this->setProperty('alias', $alias);
		}
		else {
			$alias = parent::checkFriendlyAlias();
		}
		return $alias;
	}


	/** {inheritDoc} */
	public function beforeSave() {
		$this->object->set('isfolder', 0);
		
		$where = array(
					'contentid' => $this->object->get('id')
				  , 'tmplvarid' => 1
				);
			$tv = $this->modx->getObject('modTemplateVarResource', $where);
			$action=0;
			$askidka=0;
			if ($tv){
				$optionspr=$tv->get('value');
				 $optionsar=$this->modx->fromJSON($optionspr);
				 
				
				  foreach ($optionsar as $option)
				 {
						 if (($option['action']!='')||($option['plusone']==true)||($option['plusone']==1)||($option['podarok']==true)||($option['podarok']==1)){
						 $action=1;
						 
						 }
						 if (trim($option['action'])!='')
						 {
							 $askidka=1;
						 }
				 }
			 
		
			}
			
			
			
		$productdata=$this->object->getOne('Data');
			$new_old=$productdata->get('new');//$this->object->get('new');//
			
			if (isset( $_POST['new'])&&(( $_POST['new']=='on')||( $_POST['new']==1)))
			$new_now= 1;
			else $new_now= 0;
			
				if ($this->object->get('new')==1)
				{
					if ($productdata->get('data_new')==NULL){// ||($productdata->get('data_new')=='0000-00-00 00:00:00')){
						$productdata->set('data_new',date("Y-m-d H:m:s"));
						$productdata->save();
					}
				}else{
					$productdata->set('data_new',NULL);//'0000-00-00 00:00:00');
					$productdata->save();
				}
				
			
				
			$article=$this->object->get('article');
			if ($article==''){
			$group=0;
                    			    if ($this->object->get('parent')!=0){
                    			    
                        			    $q1 = $this->modx->newQuery('modResource');//, array('id:IN' => array_keys($values), 'published' => 1));
                                		$q1->select('id,parent');
                                		$q1->where(array('id'=>$this->object->get('parent')));
                        			     if ($q1->prepare() && $q1->stmt->execute()) {
                        			        while ($row1 = $q1->stmt->fetch(PDO::FETCH_ASSOC)) {                        			          
                        			         $group=$row1['parent'];break;
                        			        }
                        			     }
                    			    }
									$article=$group.$this->object->get('id');
								
							$this->object->set('article', $article);
			}
			
			
			
			// сначала удалим старые значения
			
			$c = $this->modx->newQuery('msAttributeProduct');
			$c->command('delete');
			
			$c->where(array(
				'product_id'=>$this->object->get('id'),    
			));
			$c->prepare();
			$c->stmt->execute();


			
			if (isset($_POST['attribute'])&&(count($_POST['attribute'])>0))
			{
				
				foreach ($_POST['attribute'] as $key=>$attr)
				{
					foreach ($attr as $key1=>$atr)
					{

                        ini_set('display_errors', 1);
                        error_reporting(E_ALL || E_STRICT);
						$attribute = $this->modx->newObject('msAttributeProduct');
						$attribute->set('attribute_id',$key1);
						$attribute->set('product_id',$this->object->get('id'));
						$attribute->set('attr_group',$key);

                        $attribute->set('old_value','');
                        $attribute->set('group_name','');

						if ($attribute->save()) {

                         
                        }
					}
				}
			}
			
							
					//$this->object->setTVValue('related_products', '');//implode(",", $ids));
					
			
		return parent::beforeSave();
	}

	
	
		/** {@inheritDoc} */
	public function afterSave() {
		$this->saveTemplateVariables();
		$alias=$this->object->get('uri');
		$published=$this->object->get('published');
			
			$alias=str_replace('!','-',$alias);
					$alias=str_replace('"','-',$alias);
					$alias=str_replace("'",'-',$alias);
					$alias=str_replace("%",'-',$alias);
					$alias=str_replace(";",'-',$alias);
					$alias=str_replace("?",'-',$alias);
					$alias=str_replace("(",'-',$alias);
					$alias=str_replace(")",'-',$alias);
					$alias=str_replace("+",'-',$alias);
					$alias=str_replace(".",'-',$alias);
					$alias=str_replace(",",'-',$alias);
					$alias=str_replace("_",'-',$alias);
					$alias=str_replace("*",'-',$alias);
					$alias=str_replace("№",'-',$alias);
					$alias=str_replace("&",'-',$alias);
					$alias=str_replace('--','-',$alias);
					
					
					 if (substr($alias, -1) == '/') {
						 $alias=substr($alias, 0, -1) ;
					 }
					 if ((strpos($alias,'catalog/')!==false)&&($alias!='catalog'))
					 {
						 $alias=str_replace('catalog/','',$alias);
					 }
					 
			$this->object->set('uri', $alias);
			
			
		
			 if ($this->object->get('parent')!=0){
                    			    
                        			    $q1 = $this->modx->newQuery('modResource');//, array('id:IN' => array_keys($values), 'published' => 1));
                                		$q1->select('id,parent');
                                		$q1->where(array('id'=>$this->object->get('parent')));
                        			     if ($q1->prepare() && $q1->stmt->execute()) {
                        			        while ($row1 = $q1->stmt->fetch(PDO::FETCH_ASSOC)) {                        			          
                        			         $group=$row1['parent'];break;
                        			        }
                        			     }
                    			    }
									$article=$group.$this->object->get('id');
			if ($this->object->get('article')!='')					
			{
				
				$this->object->set('article', $article);
			}
			
			
			
			
			
		
		$where = array(
					'contentid' => $this->object->get('id')
				  , 'tmplvarid' => 1
				);
			$tv = $this->modx->getObject('modTemplateVarResource', $where);
			$action=0;
			if ($tv){
				$optionspr=$tv->get('value');
				 $optionsar=$this->modx->fromJSON($optionspr);
				 
				$optionsar1=$optionsar;
				$allinstock=array();
				$alloutstock=array();
				  foreach ($optionsar as $key=>$option)
				 {
					 if ($option['instock']==1)
						 $allinstock[]= $option['artpost'];
						 else{
							$alloutstock[]= $option['artpost'];
						 }
						 
				 }
				
				 if (count($alloutstock)==count($optionsar))
				 {
					 $this->object->set('published', 0);
				 }
				 else  $this->object->set('published', 1);
			 
		
			}
			
			
			$where = array(
					'contentid' => $this->object->get('id')
				  , 'tmplvarid' => 1
				);
			$tv = $this->modx->getObject('modTemplateVarResource', $where);
			$action=0;
			$askidka=0;
            $markdown=0;
			if ($tv){
				$optionspr=$tv->get('value');
				 $optionsar=$this->modx->fromJSON($optionspr);
				 
				
				  foreach ($optionsar as $option)
				 {
				     if ($option['disable_option']!=1) {
                         if (($option['action'] != '') || ($option['plusone'] == true) || ($option['plusone'] == 1) || ($option['podarok'] == true) || ($option['podarok'] == 1)) {
                             $action = 1;
                         }
                         if (trim($option['action']) != '') {
                             $askidka = 1;
                         }
                     }
                     if ($option['disable_option']!='1')
                     {
                         if ($option['markdown']=='1')
                            $markdown=1;
                     }

				 }
			 
		
			}
		$where = array(
					'contentid' => $this->object->get('id')
				  , 'tmplvarid' => 64
				);
			$tv = $this->modx->getObject('modTemplateVarResource', $where);
			//$askidka=0;
			if ($tv){
				$optionspr=$tv->get('value');
				if ($optionspr!='')//&&($optionspr==)
				{
					if ($optionspr>0) $askidka=1;
				}
		
			}
			//echo $askidka;
			$this->object->set('askidka', $askidka);

            $this->object->set('markdown', $markdown);
			
			
			
			//132854
			
			  //if(empty($tv_related_products)) {
				
					
				/*} else {
					$ids = explode(",", $tv_related_products);
				}*/
			
			$this->object->save();	//die();
			
			// по неведомой причине перестал сохраняться вендор
			if ($_POST['vendor']){
				
									
					$c = $this->modx->newQuery('msProductData');
					$c->command('update');
					$c->set(array(
						'vendor'  => (int)$_POST['vendor']    
					));
					$c->where(array(
						'id'    => $this->object->get('id'),    
					));
					$c->prepare();
					
					$c->stmt->execute(); 
				
			}
			/*
			
			  $ids=array();$limit=10;
					$category_id = $this->object->get('parent');
					$q = $this->modx->newQuery('msProduct', array('parent' => $category_id));
					$q->sortby('RAND()');
					$q->limit($limit);
					if ($q->prepare() && $q->stmt->execute()) {
						$ids = $q->stmt->fetchAll(PDO::FETCH_COLUMN);
					}
					
					//related_products
					$where = array(
					'contentid' => $this->object->get('id')
						  , 'tmplvarid' => 9
						);
					$tv = $this->modx->getObject('modTemplateVarResource', $where);
					$action=0;
					$askidka=0;
					//$ids=array();
					if ($tv){
						$tv->set('value',implode(",", $ids));
						$tv->save();
						
					}
			*/
			
		return parent::afterSave();
	}
	
	
	
	  public function saveTemplateVariables() {
        $tvs = $this->getProperty('tvs',null);
        if (!empty($tvs)) {
            $tmplvars = array();

            $tvs = $this->object->getTemplateVars();
            /** @var modTemplateVar $tv */
            foreach ($tvs as $tv) {
                if (!$tv->checkResourceGroupAccess()) {
                    continue;
                }

                $tvKey = 'tv'.$tv->get('id');
				
				if ($tv->get('id')==9)
				{
					  $ids=array();$limit=10;
					$category_id = $this->object->get('parent');
					$q = $this->modx->newQuery('msProduct', array('parent' => $category_id));
					$q->sortby('RAND()');
					$q->limit($limit);
					if ($q->prepare() && $q->stmt->execute()) {
						$ids = $q->stmt->fetchAll(PDO::FETCH_COLUMN);
					}
					$value=implode(",", $ids);
				}else{
		
					
					
					$value = $this->getProperty($tvKey,null);
					/* set value of TV */
					if ($tv->get('type') != 'checkbox') {
						$value = $value !== null ? $value : $tv->get('default_text');
					} else {
						$value = $value ? $value : '';
					}

					/* validation for different types */
					switch ($tv->get('type')) {
						case 'url':
							$prefix = $this->getProperty($tvKey.'_prefix','');
							if ($prefix != '--') {
								$value = str_replace(array('ftp://','http://'),'', $value);
								$value = $prefix.$value;
							}
							break;
						case 'date':
							$value = empty($value) ? '' : strftime('%Y-%m-%d %H:%M:%S',strtotime($value));
							break;
						/* ensure tag types trim whitespace from tags */
						case 'tag':
						case 'autotag':
							$tags = explode(',',$value);
							$newTags = array();
							foreach ($tags as $tag) {
								$newTags[] = trim($tag);
							}
							$value = implode(',',$newTags);
							break;
						default:
							/* handles checkboxes & multiple selects elements */
							if (is_array($value)) {
								$featureInsert = array();
								while (list($featureValue, $featureItem) = each($value)) {
									if(empty($featureItem)) { continue; }
									$featureInsert[count($featureInsert)] = $featureItem;
								}
								$value = implode('||',$featureInsert);
							}
							break;
					}
				}
                /* if different than default and set, set TVR record */
                $default = $tv->processBindings($tv->get('default_text'),$this->object->get('id'));
                if (strcmp($value,$default) != 0) {
                    /* update the existing record */
                    $tvc = $this->modx->getObject('modTemplateVarResource',array(
                        'tmplvarid' => $tv->get('id'),
                        'contentid' => $this->object->get('id'),
                    ));
                    if ($tvc == null) {
                        /** @var modTemplateVarResource $tvc add a new record */
                        $tvc = $this->modx->newObject('modTemplateVarResource');
                        $tvc->set('tmplvarid',$tv->get('id'));
                        $tvc->set('contentid',$this->object->get('id'));
                    }
                    $tvc->set('value',$value);
                    $tvc->save();

                /* if equal to default value, erase TVR record */
                } else {
                    $tvc = $this->modx->getObject('modTemplateVarResource',array(
                        'tmplvarid' => $tv->get('id'),
                        'contentid' => $this->object->get('id'),
                    ));
                    if (!empty($tvc)) {
                        $tvc->remove();
                    }
                }
            }
        }
        return $tvs;
    }

	

	/** {inheritDoc} */
	public function fixParents() {
		if (!empty($this->oldParent) && !($this->oldParent instanceof msCategory)) {
			$oldParentChildrenCount = $this->modx->getCount('modResource', array('parent' => $this->oldParent->get('id')));
			if ($oldParentChildrenCount <= 0 || $oldParentChildrenCount == null) {
				$this->oldParent->set('isfolder', false);
				$this->oldParent->save();
			}
		}

		if (!empty($this->newParent)) {
			$this->newParent->set('isfolder', true);
		}
	}


	/** {inheritDoc} */
	public function clearCache() {
		parent::clearCache();
		$this->object->clearCache();
	}

}

return 'msProductUpdateProcessor';