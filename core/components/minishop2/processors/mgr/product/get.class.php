<?php

class msProductGetProcessor extends modObjectGetProcessor {
	public $classKey = 'msProduct';
	public $languageTopics = array('minishop2:default');

	
	public function cleanup() {
		$array = $this->object->toArray();
		$product = new msProduct($this->modx);
		//$array['options']='';
		$option=$this->getProperty('option');
		//if ($this->getProperty('option'))//&&($this->getProperty('option')!=''))
		if ($option) {
		//{
			//$option=$this->getProperty('option');
			$valaction=$product->getvalueaction($array['id'],$array['parent']);
			$q = $this->modx->newQuery('modTemplateVarResource');
						
					$q->select('*');
					$q->where(array('tmplvarid' => 1,'contentid'=>$array['id']));
					if ($q->prepare() && $q->stmt->execute()) {
						$result1 = $q->stmt->fetchAll(PDO::FETCH_ASSOC);	
						/*echo '<pre>';
						print_r($result1);
						echo '</pre>';*/
						if (count($result1)>0)
						{
							$value=$result1[0]['modTemplateVarResource_value'];
							$value_options=$this->modx->fromJSON($value);
							foreach ($value_options as $opt)
							{
								if ($option==$opt['MIGX_id'])
								{
									$optionforprice=$opt;
									//$valaction=$productmodel->getvalueaction($row['id'],$idcat);
									$optionforprice['valaction']=$valaction;
									$optionforprice['idcategory']=$array['parent'];
									
										$priceall=$this->object->getAllPrice($optionforprice);
										$newprice=$priceall['price'];
										$price=$priceall['oldprice'];
										
										/*echo '<pre>';
										print_r($priceall);
										echo '</pre>';
										echo '<pre>';
										print_r($optionforprice);
						echo '</pre>';*/
						
									$array['price']=$newprice;
									$array['price_old']=$price;
									$array['skidka']=0;
									
									if (($valaction!='')||($opt['action']!='')) $array['skidka']=1;
									
									
									$size=array('size'=>$opt['weight'].' '.$opt['weight_prefix']);
									$array['options']=$this->modx->toJSON($size);
								}
							}
							
						}
					}
			
		}

		return $this->success('', $array);
	}
	
}

return 'msProductGetProcessor';