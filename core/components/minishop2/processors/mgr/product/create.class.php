<?php

require_once MODX_CORE_PATH.'model/modx/modprocessor.class.php';
require_once MODX_CORE_PATH.'model/modx/processors/resource/create.class.php';

class msProductCreateProcessor extends modResourceCreateProcessor {
	public $classKey = 'msProduct';
	public $languageTopics = array('resource','minishop2:default');
	public $permission = 'msproduct_save';
	public $beforeSaveEvent = 'OnBeforeDocFormSave';
	public $afterSaveEvent = 'OnDocFormSave';
	/* @var msProduct $object */
	public $object;


	/** {@inheritDoc} */
	public function prepareAlias() {
		if ($this->workingContext->getOption('ms2_product_id_as_alias')) {
			$alias = 'empty-resource-alias';
			$this->setProperty('alias', $alias);
		}
		else {
			$alias = parent::prepareAlias();
		}
		return $alias;
	}


	/** {@inheritDoc} */
	public function beforeSet() {
		$this->setDefaultProperties(array(
			'show_in_tree' => $this->modx->getOption('ms2_product_show_in_tree_default', null, false)
			,'hidemenu' => $this->modx->getOption('hidemenu_default', null, true)
			,'source' => $this->modx->getOption('ms2_product_source_default', null, 1)
			,'template' => $this->modx->getOption('ms2_template_product_default', null, $this->modx->getOption('default_template'))
		));

		return parent::beforeSet();
	}


	/** {@inheritDoc} */
	public function beforeSave() {
		$this->object->set('isfolder', 0);

		
		
		return parent::beforeSave();
	}


	/** {@inheritDoc} */
	public function afterSave() {
		if ($this->object->alias == 'empty-resource-alias') {
			$this->object->set('alias', $this->object->id);
			
			
			
			
			$this->object->save();
		}

		
                    			    $group=0;
                    			    if ($this->object->get('parent')!=0){
                    			    
                        			    $q1 = $this->modx->newQuery('modResource');//, array('id:IN' => array_keys($values), 'published' => 1));
                                		$q1->select('id,parent');
                                		$q1->where(array('id'=>$this->object->get('parent')));
                        			     if ($q1->prepare() && $q1->stmt->execute()) {
                        			        while ($row1 = $q1->stmt->fetch(PDO::FETCH_ASSOC)) {                        			          
                        			         $group=$row1['parent'];break;
                        			        }
                        			     }
                    			    }
									$article=$group.$this->object->get('id');
								
							$this->object->set('article', $article);
							
							$alias=$this->object->get('uri');			
							$alias=str_replace('!','-',$alias);
									$alias=str_replace('"','-',$alias);
									$alias=str_replace("'",'-',$alias);
									$alias=str_replace("%",'-',$alias);
									$alias=str_replace(";",'-',$alias);
									$alias=str_replace("?",'-',$alias);
									$alias=str_replace("(",'-',$alias);
									$alias=str_replace(")",'-',$alias);
									$alias=str_replace("+",'-',$alias);
									$alias=str_replace(".",'-',$alias);
									$alias=str_replace(",",'-',$alias);
									$alias=str_replace("_",'-',$alias);
									$alias=str_replace("*",'-',$alias);
									$alias=str_replace("№",'-',$alias);
									$alias=str_replace("&",'-',$alias);
									$alias=str_replace('--','-',$alias);
									
									
									 if (substr($alias, -1) == '/') {
										 $alias=substr($alias, 0, -1) ;
									 }
									 if ((strpos($alias,'catalog/')!==false)&&($alias!='catalog'))
									 {
										 $alias=str_replace('catalog/','',$alias);
									 }
									 
$this->object->set('uri', $alias);
$alias=$this->object->get('alias');			
							$alias=str_replace('!','-',$alias);
							$alias=str_replace('/','-',$alias);
									$alias=str_replace('"','-',$alias);
									$alias=str_replace("'",'-',$alias);
									$alias=str_replace("%",'-',$alias);
									$alias=str_replace(";",'-',$alias);
									$alias=str_replace("?",'-',$alias);
									$alias=str_replace("(",'-',$alias);
									$alias=str_replace(")",'-',$alias);
									$alias=str_replace("+",'-',$alias);
									$alias=str_replace(".",'-',$alias);
									$alias=str_replace(",",'-',$alias);
									$alias=str_replace("_",'-',$alias);
									$alias=str_replace("*",'-',$alias);
									$alias=str_replace("№",'-',$alias);
									$alias=str_replace("&",'-',$alias);
									$alias=str_replace('--','-',$alias);
									
									 $this->object->set('alias', $alias);
									 
								// запишем атрибуты
			// сначала удалим старые значения
			
			$c = $this->modx->newQuery('msAttributeProduct');
			$c->command('delete');
			/*$c->set(array(
				'hidemenu'  => 1    
			));*/
			$c->where(array(
				'product_id'=>$this->object->get('id'),    
			));
			$c->prepare();
			// print $c->toSQL();
			$c->stmt->execute(); 
			
			//$attribute = $modx->getObject('msAttributeProduct',array('attribute_id'=>(int)trim($valueid),'product_id'=>(int)trim($res['contentid'])));//$attroldar[1]);
			if (isset($_POST['attribute'])&&(count($_POST['attribute'])>0))
			{
				/*echo '<pre>';
				print_r($_POST['attribute']);
				echo '</pre>';
				die();	*/
				foreach ($_POST['attribute'] as $key=>$attr)
				{
					foreach ($attr as $key1=>$atr)
					{
						$attribute = $this->modx->newObject('msAttributeProduct');
						$attribute->set('attribute_id',$key1);
						$attribute->set('product_id',$this->object->get('id'));
						$attribute->set('attr_group',$key);
						$attribute->save();
					}
				}
			}
			//$_POST['attribute'][{$attribute.id}][{$attr.id}]
			
							
							$this->object->save();	
		// Updating resourceMap before OnDocSaveForm event
		$results = $this->modx->cacheManager->generateContext($this->object->context_key);
		$this->modx->context->resourceMap = $results['resourceMap'];
		$this->modx->context->aliasMap = $results['aliasMap'];

		
		
		$productdata=$this->object->getOne('Data');
		if ($this->object->get('new')==1)
				{
					$productdata->set('data_new',date("Y-m-d H:m:s"));
					$productdata->save();
					
				}
		
		return parent::afterSave();
	}


	/** {@inheritDoc} */
	public function clearCache() {
		$clear = parent::clearCache();
		/* @var msCategory $category */
		if ($category = $this->object->getOne('Category')) {
			$category->clearCache();
		}

		return $clear;
	}

}

return 'msProductCreateProcessor';