<?php

require_once MODX_CORE_PATH.'model/modx/modprocessor.class.php';
require_once MODX_CORE_PATH.'model/modx/processors/resource/unpublish.class.php';

class msProductUnPublishProcessor extends modResourceUnPublishProcessor {
	public $classKey = 'msProduct';

	
	/** {@inheritDoc} */
	public function afterSave() {
		/*if ($this->object->alias == 'empty-resource-alias') {
			$this->object->set('alias', $this->object->id);
			
			
			
		
			
			$this->object->save();
		}*/
		
			
		$where = array(
					'contentid' => $this->object->get('id')
				  , 'tmplvarid' => 1
				);
			$tv = $this->modx->getObject('modTemplateVarResource', $where);
			$action=0;
			if ($tv){
				$optionspr=$tv->get('value');
				 $optionsar=$this->modx->fromJSON($optionspr);
				 
				$optionsar1=$optionsar;
				  foreach ($optionsar as $key=>$option)
				 {
						 
						 $action=1;
						 
						 $optionsar1[$key]['instock']=0;
				 }
				 $optionsar=$this->modx->fromJSON($optionsar1);
				 $tv->set('value',$optionsar);
				 $tv->save();
			 
		
			}
			

		
                
		return parent::afterSave();
	}
}

return 'msProductUnPublishProcessor';