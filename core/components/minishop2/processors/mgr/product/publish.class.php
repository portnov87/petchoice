<?php

require_once MODX_CORE_PATH.'model/modx/modprocessor.class.php';
require_once MODX_CORE_PATH.'model/modx/processors/resource/publish.class.php';

class msProductPublishProcessor extends modResourcePublishProcessor {
	public $classKey = 'msProduct';
/** {@inheritDoc} */
	public function afterSave() {
		
		$where = array(
					'contentid' => $this->object->get('id')
				  , 'tmplvarid' => 1
				);
			$tv = $this->modx->getObject('modTemplateVarResource', $where);
			$action=0;
			if ($tv){
				$optionspr=$tv->get('value');
				 $optionsar=$this->modx->fromJSON($optionspr);
				 
				$optionsar1=$optionsar;
				  foreach ($optionsar as $key=>$option)
				 {
						 
						 $action=1;
						 
						 $optionsar1[$key]['instock']=1;
				 }
				 $optionsar=$this->modx->fromJSON($optionsar1);
				 $tv->set('value',$optionsar);
				 $tv->save();
			 
		
			}
			

		
                
		return parent::afterSave();
	}
}

return 'msProductPublishProcessor';