<?php

//error_reporting(E_ALL | E_STRICT);
//ini_set('display_errors', 1);
//ini_set('error_reporting', E_ALL);

//ini_set('display_startup_errors', 1);

class msGetpetsbyuserProcessor extends modObjectGetListProcessor
{
    public $classKey = 'msPet';
    //public $languageTopics = array('minishop2:product');
    public $defaultSortField = 'msPet.created';
    public $defaultSortDirection = 'DESC';
   // public $permission = 'msorder_list';


    /** {@inheritDoc} */
    public function initialize()
    {

        /* if (!$this->modx->hasPermission($this->permission)) {
             return $this->modx->lexicon('access_denied');
         }*/
        return parent::initialize();
    }


    /** {@inheritDoc} */
    public function prepareQueryBeforeCount(xPDOQuery $c)
    {

        //$c->leftJoin('msOrder', 'msOrder', '`msBonusesLog`.`order_id` = `msOrder`.`id`');
        //$c->leftJoin('modUser', 'modUser', '`msBonusesLog`.`manager_id` = `modUser`.`id`');
        $c->where(array(
            'user_id' => $this->getProperty('user_id'),
        ));
        $c->select($this->modx->getSelectColumns('msPet', 'msPet', 'pets_'));

       // $c->select($this->modx->getSelectColumns('msOrder', 'msOrder', 'ord_'));
        //$c->select($this->modx->getSelectColumns('modUser', 'modUser', 'manager_'));
        return $c;

    }
    /** {@inheritDoc} */
    public function iterate(array $data) {
        $list = array();
        $list = $this->beforeIteration($list);
        $this->currentIndex = 0;
        /** @var xPDOObject|modAccessibleObject $object */
        foreach ($data['results'] as $array) {
            $list[] = $this->prepareArray($array);
            $this->currentIndex++;
        }

        $list = $this->afterIteration($list);

        return $list;
    }


    /** {@inheritDoc} */
    public function prepareArray(array $resourceArray) {

        return $resourceArray;
    }


    public function getData() {
        $data = array();
        $limit = intval($this->getProperty('limit'));
        $start = intval($this->getProperty('start'));

        /* query for chunks */
        $c = $this->modx->newQuery($this->classKey);
        $c = $this->prepareQueryBeforeCount($c);
        $data['total'] = $this->modx->getCount($this->classKey,$c);
        $c = $this->prepareQueryAfterCount($c);
        if ($c->prepare() && $c->stmt->execute()) {
            $result = $c->stmt->fetchAll(PDO::FETCH_ASSOC);

            $new_result=[];//$result;

           /* echo '<pre>';
            print_r($result);
            echo '</pre>';

            die();*/
            foreach ($result as $r) {
                $day = $r['pets_bday'];
                $month = $r['pets_bmonth'];
                if ((int)$day < 10) $day = '0' . $day;
                if ((int)$month < 10) $month = '0' . $month;
                $birsday = $day . '.' . $month . '.' . $r['pets_byear'];
                $genre=$r['pets_genre'];
                $genre_name='';
                switch ($genre){
                    case '1':
                        $genre_name='Мальчик';
                        break;
                    case '2':
                        $genre_name='Девочка';
                        break;
                }


                $type_name='';
                $type=$r['pets_type'];
                $q = $this->modx->newQuery('msPetType');
                $q->where(array('id' => $type));
                $q->sortby('name');
                $q->prepare();
                $q->stmt->execute();
                $result_type = $q->stmt->fetchAll(PDO::FETCH_ASSOC);

                foreach ($result_type as $res) {
                    $type_name=$res['msPetType_name'];
                    break;
                }

                $breed_name='';

                $breed=$r['pets_breed'];
                $q = $this->modx->newQuery('msBreed');
                $q->where(array('id' => $breed));
                $q->sortby('name');

                $q->prepare();
                $q->stmt->execute();
                $result_breed = $q->stmt->fetchAll(PDO::FETCH_ASSOC);

                foreach ($result_breed as $res) {
                    $breed_name=$res['msBreed_name'];
                    break;
                }

                $new_result[] = [
                    'id' => $r['pets_id'],
                    'user_id' => $r['pets_user_id'],
                    'type' => $type_name,//$r['pets_type'],
                    'breed' => $breed_name,
                    'genre' => $genre_name,
                    'name' => $r['pets_name'],
                    'comment' => $r['pets_comment'],
                    'created' => $r['pets_created'],
                    'updated' => $r['pets_updated'],
                    'photo' => $r['pets_photo'],
                    'birsday' => $birsday
                ];
            }

            $data['results']=$new_result;
        }



        return $data;
    }





}

return 'msGetpetsbyuserProcessor';