<?php

//error_reporting(E_ALL | E_STRICT);
//ini_set('display_errors', 1);
//ini_set('error_reporting', E_ALL);

//ini_set('display_startup_errors', 1);
class msGetListBonusesUserProcessor extends modObjectGetListProcessor
{
    public $classKey = 'msBonusesLog';
    //public $languageTopics = array('minishop2:product');
    public $defaultSortField = 'msBonusesLog.created';
    public $defaultSortDirection = 'DESC';
   // public $permission = 'msorder_list';


    /** {@inheritDoc} */
    public function initialize()
    {

        /* if (!$this->modx->hasPermission($this->permission)) {
             return $this->modx->lexicon('access_denied');
         }*/
        return parent::initialize();
    }


    /** {@inheritDoc} */
    public function prepareQueryBeforeCount(xPDOQuery $c)
    {

       // echo 'objec'.$this->getProperty('user_id');
        //die();
         $c->leftJoin('msOrder', 'msOrder', '`msBonusesLog`.`order_id` = `msOrder`.`id`');
        $c->leftJoin('modUser', 'modUser', '`msBonusesLog`.`manager_id` = `modUser`.`id`');

        $c->leftJoin('TicketComment', 'TicketComment', '`msBonusesLog`.`ticket_id` = `TicketComment`.`id`');
        //$c->leftJoin('modUser', 'modUsers', '`msBonusesLog`.`user_id` = `modUsers`.`id`');


        //$c->leftJoin('msProduct', 'msProduct', '`msOrderProduct`.`product_id` = `msProduct`.`id`');
        //$c->leftJoin('msProductData', 'msProductData', '`msOrderProduct`.`product_id` = `msProductData`.`id`');
        //$c->leftJoin('msOrderStatus', 'msOrderStatus', '`msOrder`.`status` = `msOrderStatus`.`id`');
        $c->where(array(
            'user_id' => $this->getProperty('user_id'),
        ));
        $c->select($this->modx->getSelectColumns('msBonusesLog', 'msBonusesLog', 'bonuses_'));

        $c->select($this->modx->getSelectColumns('msOrder', 'msOrder', 'ord_'));
//        $c->select($this->modx->getSelectColumns('modUsers_manager', 'modUsers_manager', 'manager_'));
        $c->select($this->modx->getSelectColumns('modUser', 'modUser', 'manager_'));
        $c->select($this->modx->getSelectColumns('TicketComment', 'TicketComment', 'ticket_'));

        //$c->select($this->modx->getSelectColumns('msOrderStatus', 'msOrderStatus', 'status_'));
        //$c->select($this->modx->getSelectColumns('msOrderProduct', 'msOrderProduct'));
        //$c->select($this->modx->getSelectColumns('msProduct', 'msProduct', 'product_'));
        //$c->select($this->modx->getSelectColumns('msProductData', 'msProductData', 'product_', array('id'), true));


        return $c;

    }
    /** {@inheritDoc} */
    public function iterate(array $data) {
        $list = array();
        $list = $this->beforeIteration($list);
        $this->currentIndex = 0;
        /** @var xPDOObject|modAccessibleObject $object */
        foreach ($data['results'] as $array) {
            $list[] = $this->prepareArray($array);
            $this->currentIndex++;
        }

        $list = $this->afterIteration($list);

        return $list;
    }


    /** {@inheritDoc} */
    public function prepareArray(array $resourceArray) {

        return $resourceArray;
    }


    public function getData() {
        $data = array();
        $limit = intval($this->getProperty('limit'));
        $start = intval($this->getProperty('start'));

        /* query for chunks */
        $c = $this->modx->newQuery($this->classKey);
        $c = $this->prepareQueryBeforeCount($c);
        $data['total'] = $this->modx->getCount($this->classKey,$c);
        $c = $this->prepareQueryAfterCount($c);
        //$c->select('*');
      //  $c->select('Ref,DescriptionRu,CityRef,Number');
        //$c->sortby('cast(Number as unsigned)',$this->getProperty('asc'));

        if ($c->prepare() && $c->stmt->execute()) {
            // $sql = $c->toSQL();

            $result = $c->stmt->fetchAll(PDO::FETCH_ASSOC);

            $new_result=[];//$result;
            foreach ($result as $r)
            {

                $product_name='';
                if ($r['ticket_id']!=0){

                    $tickets = $this->modx->newQuery('TicketComment');
                    $tickets->innerJoin('TicketThread', 'TicketThread', '`TicketThread`.`id` = `TicketComment`.`thread`');
                    $tickets->innerJoin('msProduct', 'msProduct', '`msProduct`.`id` = `TicketThread`.`resource`');
                    $tickets->where(array('TicketComment.id' =>  $r['ticket_id']));
                    $tickets->select(array('msProduct.id', 'msProduct.pagetitle'));


                    if ($tickets->prepare() && $tickets->stmt->execute()) {
                        $tickets_array = $tickets->stmt->fetchAll(PDO::FETCH_ASSOC);
                        foreach ($tickets_array as $ticket) {
                            $product_name=$ticket['pagetitle'];
                            //$ids[] = $order_i['id'];
                            //$total += $order_i['pagetitle'];
                        }
                    }

                }
                $new_result[]=[
                    'order_num'=>$r['ord_num'],
                    'manager'=>$r['manager_username'],
                    'amount'=>$r['bonuses_amount'],
                    'product_name'=>$product_name,//r['bonuses_amount'],
                    'created'=>$r['bonuses_created'],
                    'reason'=>$r['bonuses_reason'],
                ];
            }

            $data['results']=$new_result;
        }




        return $data;
    }



    /** {@inheritDoc} */
    /*public function prepareRow(xPDOObject $object) {

        $data = array();
       // $data[$v] = $object->get($v);

       // $array=$object->toArray();

        $data['order_num'] = $object->get('ord_num');
        $data['manager'] = $object->get('manager_username');
        $data['amount'] = $object->get('bonuses_amount');
        $data['created'] = $object->get('bonuses_created');

        return $data;
    }
*/

    /*public function prepareRow(xPDOObject $object)
    {
        //$fields = array_map('trim', explode(',', $this->modx->getOption('ms2_order_product_fields', null, '')));
        //$fields = array_values(array_unique(array_merge($fields, array('id', 'product_id', 'name', 'product_pagetitle'))));


        $data = array();
        $array=$object->toArray();
        echo '<pre>';
        print_r($array);
        echo '</pre>';
        die();
       // echo 'amount'.$object->get('amount');die();

        $data['order_num'] = $object->get('ord_num');
        $data['manager'] = $object->get('manager_username');
        $data['amount'] = $object->get('bonuses_amount');
        $data['created'] = $object->get('bonuses_created');

        return $data;
    }*/



    /** {@inheritDoc} */
   /* public function getData() {
        $data = array();
  //      $limit = intval($this->getProperty('limit'));
//        $start = intval($this->getProperty('start'));


        $c = $this->modx->newQuery($this->classKey);
        $c = $this->prepareQueryBeforeCount($c);
        $data['total'] = $this->modx->getCount($this->classKey,$c);
        $c = $this->prepareQueryAfterCount($c);


        if ($c->prepare() && $c->stmt->execute()) {
            // $sql = $c->toSQL();

            $data['results'] = $c->stmt->fetchAll(PDO::FETCH_ASSOC);
        }

        return $data;
    }*/

}

return 'msGetListBonusesUserProcessor';