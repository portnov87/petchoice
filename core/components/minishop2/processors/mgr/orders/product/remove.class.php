<?php

class msOrderProductRemoveProcessor extends modObjectRemoveProcessor  {
	public $classKey = 'msOrderProduct';
	public $languageTopics = array('minishop2:default');
	public $permission = 'msorder_save';
	/* @var msOrder $order */
	protected $order;


	/** {@inheritDoc} */
	public function beforeRemove() {
		if (!$this->order = $this->object->getOne('Order')) {
			return $this->modx->lexicon('ms2_err_order_nf');
		}

		if ($status = $this->order->getOne('Status')) {
			if ($status->get('final')) {
				return $this->modx->lexicon('ms2_err_status_final');
			}
		}

		$this->setProperty('cost', $this->getProperty('price') * $this->getProperty('count'));
		/*
		if ($options = $this->getProperty('options')) {
			$tmp = $this->modx->fromJSON($options);
			if (!is_array($tmp)) {
				$tmp = array('size'=>$options);//
				if (!is_array($tmp)) {
				$this->modx->error->addField('options', $this->modx->lexicon('ms2_err_json'));
				}else $this->setProperty('options', $tmp);
			}
			else {
				$this->setProperty('options', $tmp);
			}
		}
		*/
		
		/*echo '<pre>options';
		print_R($this->object->get('options'));
		echo '</pre>';die();*/
		
		$user_id = $this->modx->user->id;
				//($action == 'status' && $entry == 1) || !$this->modx->user->id ? $order->get('user_id') : 
				$action="remove_product";
				
				
				
				$options1=$this->object->get('options');
				$entry=$this->object->get('name');
					if (isset($options1)){
						$entry.=", ".$options1['size'].'<br/>';
					}
					
					$log = $this->modx->newObject('msOrderLog', array(
						'order_id' =>$this->order->get('id')
						,'user_id' => $user_id
						,'timestamp' => time()
						,'action' => $action
						,'message' => $entry
						,'ip' => $this->modx->request->getClientIp()
					));

					$log->save();
					
					
		return !$this->hasErrors();
	}


	/** {@inheritDoc} */
	public function afterRemove() {
		$this->order->updateProducts();
	}

}
return 'msOrderProductRemoveProcessor';