<?php

class msOrderProductUpdateProcessor extends modObjectUpdateProcessor {
	public $classKey = 'msOrderProduct';
	public $languageTopics = array('minishop2:default');
	public $permission = 'msorder_save';
	/* @var msOrder $order */
	protected $order;


	/** {@inheritDoc} */
	public function initialize() {
		if (!$this->modx->hasPermission($this->permission)) {
			return $this->modx->lexicon('access_denied');
		}
		return parent::initialize();
	}


	/** {@inheritDoc} */
	public function beforeSet() {
		$count = $this->getProperty('count');
		if ($count <= 0) {
			$this->modx->error->addField('count', $this->modx->lexicon('ms2_err_ns'));
		}

		if ($options = $this->getProperty('options')) {
			$tmp = $this->modx->fromJSON($options);
			if (!is_array($tmp)) {
				$tmp = array('size'=>$options);//
				if (!is_array($tmp)) {
				$this->modx->error->addField('options', $this->modx->lexicon('ms2_err_json'));
				}else{
					$this->setProperty('options', $tmp);
					}
				//$this->modx->error->addField('options', $this->modx->lexicon('ms2_err_json'));
			}
			else {
				$this->setProperty('options', $tmp);
			}
		}

		if (!$this->order = $this->object->getOne('Order')) {
			return $this->modx->lexicon('ms2_err_order_nf');
		}

		/* @var msOrderStatus $status */
		if ($status = $this->order->getOne('Status')) {
			if ($status->get('final')) {
				return $this->modx->lexicon('ms2_err_status_final');
			}
		}
		
		if ($this->getProperty('price')!=$this->getProperty('price_old'))
		{
		$price_old=$this->getProperty('price_old');	
		}else $price_old=$this->getProperty('price');	
		
		
		$cost_old=$price_old* $this->getProperty('count');
		
		// необходимо пеерсчитать цену с учётом скидки клиента
		
		$userprofile=$this->order->getOne('UserProfile');//('user_id');
		$profile=$userprofile->toArray();
		
		$price=$this->getProperty('price');

        $new_stock=$this->getProperty('stock');
        $old_stock=$this->object->get('stock');
		
		if ($new_stock==$old_stock) {

            $this->setProperty('cost_old', $cost_old);
            $this->setProperty('price_old', $price_old);

            if (($this->getProperty('discount') != 0) && ($this->getProperty('discount') != '')) {
                $discount = $this->getProperty('discount');
                $price = round($price_old - ($price_old * ($discount / 100)), 2);
                $cost = $price * $this->getProperty('count');
            } else {
                if ($this->getProperty('skidka') == 0) {
                    if (isset($profile['extended']['discount'])) {
                        $discount = $profile['extended']['discount'];
                        $price = round($price_old - ($price_old * ($discount / 100)), 2);
                    }
                }
                $cost = $price * $this->getProperty('count');
            }
            //echo $this->getProperty('discount')."skidka".$this->getProperty('skidka');
            //echo "\r\nskidka".$profile['extended']['discount'];
            $this->setProperty('cost', $cost);
            $this->setProperty('price', $price);

            //$this->setProperty('product_id', $this->getProperty('id'));
            $up = false;
            if ($cost != $this->object->get('cost')) {
                $entry_up[] = 'Изменена стоимость';//,<br/>';
                $up = true;
            }
            if ($_POST['price'] != $this->object->get('price')) {
                $entry_up[] = 'Изменена цена';//,<br/>';
                $up = true;
            }
            if ($this->getProperty('count') != $this->object->get('count')) {
                $entry_up[] = 'Изменено количество';//,<br/>';
                $up = true;
            }
        }
				 
				 //echo $this->getProperty('count').' '.$this->object->get('count');
		if ($up){
			$user_id = $this->modx->user->id;
				//($action == 'status' && $entry == 1) || !$this->modx->user->id ? $order->get('user_id') : 
				$action="update_product";
				$entry='';
				
					$options1=$tmp;//$this->getProperty('options');
					//$options1=$this->modx->fromJSON($options);
					$entry=$this->object->get('name');
					
					if (isset($tmp)){
						$entry.=", ".$options1['size'].'<br/>';
					}
					$entry.=implode(',<br/>',$entry_up);//
				
					$log = $this->modx->newObject('msOrderLog', array(
						'order_id' =>$this->getProperty('order_id')
						,'user_id' => $user_id
						,'timestamp' => time()
						,'action' => $action
						,'message' => $entry
						,'ip' => $this->modx->request->getClientIp()
					));

					$log->save();
		}
		//$this->setProperty('cost', $this->getProperty('price') * $this->getProperty('count'));

		return !$this->hasErrors();
	}


	/** {@inheritDoc} */
	public function afterSave() {
		$this->order->updateProducts();
	}

}

return 'msOrderProductUpdateProcessor';