<?php

class msOrderProductCreateProcessor extends modObjectCreateProcessor {
	public $classKey = 'msOrderProduct';
	public $languageTopics = array('minishop2:default');
	public $permission = 'msorder_save';
	/* @var msOrder $order */
	protected $order;


	/** {@inheritDoc} */
	public function initialize() {
		if (!$this->modx->hasPermission($this->permission)) {
			return $this->modx->lexicon('access_denied');
		}
		return parent::initialize();
	}


	/** {@inheritDoc} */
	public function beforeSet() {
		$count = $this->getProperty('count');
		if ($count <= 0) {
			$this->modx->error->addField('count', $this->modx->lexicon('ms2_err_ns'));
		}

		if ($options = $this->getProperty('options')) {
			//$options=
			
			
			$tmp = $this->modx->fromJSON($options);
			if (!is_array($tmp)) {
				$tmp = array('size'=>$options);//
				if (!is_array($tmp)) {
				$this->modx->error->addField('options', $this->modx->lexicon('ms2_err_json'));
				}else{
					$this->setProperty('options', $tmp);
					}
			}
			else {
				
				$this->setProperty('options', $tmp);
			}
		}
		
		if ($name = $this->getProperty('pagetitle')) {
			$this->setProperty('name', $name);
		}

		if (!$this->order = $this->modx->getObject('msOrder', $this->getProperty('order_id'))) {
			return $this->modx->lexicon('ms2_err_order_nf');
		}

		/* @var msOrderStatus $status */
		if ($status = $this->order->getOne('Status')) {
			if ($status->get('final')) {
				return $this->modx->lexicon('ms2_err_status_final');
			}
		}
		
		if ($this->getProperty('price')!=$this->getProperty('price_old'))
		{
		$price_old=$this->getProperty('price_old');	
		}else $price_old=$this->getProperty('price');	
		
		//$price_old=$this->getProperty('price_old');
		$cost_old=$price_old* $this->getProperty('count');
		
		$this->setProperty('cost_old', $cost_old);
		$this->setProperty('price_old', $price_old);
		// необходимо пеерсчитать цену с учётом скидки клиента
		
		$userprofile=$this->order->getOne('UserProfile');//('user_id');
		$profile=$userprofile->toArray();
		/*
		$price=$this->getProperty('price');
		if ($this->getProperty('skidka')==0){
			if (isset($profile['extended']['discount'])){
				$discount=$profile['extended']['discount'];
				$price=round($price_old-($price_old*($discount/100)),2);
			}
		}
		$cost=$price * $this->getProperty('count');
		
		$this->setProperty('cost', $cost);
		$this->setProperty('price', $price);
		*/
		
		$price=$this->getProperty('price');
		
		if ($this->getProperty('discount')!='')
		{
			$discount=$this->getProperty('discount');
			$price=round($price_old-($price_old*($discount/100)),2);
			$cost=$price * $this->getProperty('count');
		}else{
			if ($this->getProperty('skidka')==0){
				if (isset($profile['extended']['discount'])){
					$discount=$profile['extended']['discount'];
					$price=round($price_old-($price_old*($discount/100)),2);
				}
			}
			$cost=$price * $this->getProperty('count');
		}
		
		$this->setProperty('cost', $cost);
		$this->setProperty('price', $price);
		
		
		$this->setProperty('product_id', $this->getProperty('id'));
		
		
		
		$user_id = $this->modx->user->id;
				//($action == 'status' && $entry == 1) || !$this->modx->user->id ? $order->get('user_id') : 
				$action="add_product";
				$options1=$tmp;//$this->getProperty('options');
				//$options1=$this->modx->fromJSON($options);
				$entry=$name.", ".$options1['size'];
					$log = $this->modx->newObject('msOrderLog', array(
						'order_id' =>$this->getProperty('order_id')
						,'user_id' => $user_id
						,'timestamp' => time()
						,'action' => $action
						,'message' => $entry
						,'ip' => $this->modx->request->getClientIp()
					));

					$log->save();
		
		return !$this->hasErrors();
	}


	/** {@inheritDoc} */
	public function beforeSave() {
		//$options
		/*echo 'options'.$this->getProperty('name');//die();
		$options=array('size'=>$this->getProperty('options'));//$this->object->get('options');
		$this->object->set('options', $this->modx->toJSON($options));*/
		
		
		
		
		$this->object->fromArray(array(
			'rank' => $this->modx->getCount('msOrderProduct')
		));
		return parent::beforeSave();
	}
	
	/** {@inheritDoc} */
	public function afterSave() {
		$this->order->updateProducts();
	}

}

return 'msOrderProductCreateProcessor';