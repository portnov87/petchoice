<?php

class msProductGetListProcessor extends modObjectGetListProcessor
{
    public $classKey = 'msOrderProduct';
    public $languageTopics = array('minishop2:product');
    public $defaultSortField = 'id';
    public $defaultSortDirection = 'ASC';
    public $permission = 'msorder_list';


    /** {@inheritDoc} */
    public function initialize()
    {

        if (!$this->modx->hasPermission($this->permission)) {
            return $this->modx->lexicon('access_denied');
        }
        return parent::initialize();
    }


    /** {@inheritDoc} */
    public function prepareQueryBeforeCount(xPDOQuery $c)
    {
        $c->innerJoin('msOrder', 'msOrder', '`msOrderProduct`.`order_id` = `msOrder`.`id`');
        $c->leftJoin('msProduct', 'msProduct', '`msOrderProduct`.`product_id` = `msProduct`.`id`');
        $c->leftJoin('msProductData', 'msProductData', '`msOrderProduct`.`product_id` = `msProductData`.`id`');
        $c->where(array(
            'order_id' => $this->getProperty('order_id')
        ));

        $c->select($this->modx->getSelectColumns('msOrderProduct', 'msOrderProduct'));
        $c->select($this->modx->getSelectColumns('msProduct', 'msProduct', 'product_'));
        $c->select($this->modx->getSelectColumns('msProductData', 'msProductData', 'product_', array('id'), true));

        if ($query = $this->getProperty('query', null)) {
            $c->where(array(
                'msProduct.pagetitle:LIKE' => '%' . $query . '%'
            , 'OR:msProduct.description:LIKE' => '%' . $query . '%'
            , 'OR:msProduct.introtext:LIKE' => '%' . $query . '%'
            , 'OR:msProductData.article:LIKE' => '%' . $query . '%'
            , 'OR:msProductData.vendor:LIKE' => '%' . $query . '%'
            , 'OR:msProductData.made_in:LIKE' => '%' . $query . '%'
            ));
        }


        return $c;
    }


    /** {@inheritDoc} */
    public function prepareRow(xPDOObject $object)
    {
        $fields = array_map('trim', explode(',', $this->modx->getOption('ms2_order_product_fields', null, '')));
        $fields = array_values(array_unique(array_merge($fields, array('id', 'product_id', 'name', 'product_pagetitle'))));

        $data = array();
        foreach ($fields as $v) {
            if ($v == 'product_options') {
                $option = $this->modx->fromJSON($object->get('options'));
                /*echo '<pre>';
                print_r($option);
                echo '<pre>';*/

                if (isset($option['size']))
                    $data[$v] = $option['size'];//$this->modx->fromJSON($option);

                //$data['option_artman'] = 'ddd';//$opt['size'];
                //$data['option_artpost'] = 'ddd';//$opt['size'];
            } else {
                $data[$v] = $object->get($v);
                if ($v == 'price_old' || $v == 'product_price' || $v == 'product_old_price') {
                    $data[$v] = round($data[$v], 2);
                } else if ($v == 'product_weight') {
                    $data[$v] = round($data[$v], 3);
                }
            }
        }


        $product_id = $object->get('product_id');
        $id1c = $object->get('id_1c');

        $product = $this->modx->getObject('msProductData', $product_id);


        $q = $this->modx->newQuery('modTemplateVar');
        $q->innerJoin('modTemplateVarResource', 'v', 'v.tmplvarid = modTemplateVar.id');
        $q->where(array(
            'v.contentid' => $product_id,
            'modTemplateVar.id' => 1,
        ));
        $q->limit(1);
        $q->select(array('modTemplateVar.*', 'v.value'));
        $value = array();
        $array = array();
        if ($q->prepare() && $q->stmt->execute()) {
            while ($tv = $q->stmt->fetch(PDO::FETCH_ASSOC)) {
                $value_options = $tv['value'];
            }
        }
        $option = $object->get('options');//$this->modx->fromJSON($object->get('options'));
        $array_options = $this->modx->fromJSON($value_options);
        /*echo '<pre>size'.$option['size'];
        print_r($object->get('options'));//$array_options);
        echo '</pre>';*/
        $data['availability_2'] = 0;
        $data['availability_3'] = 0;
        $data['availability_1'] = 0;
        $data['available'] = '';
        $data['action_hide']=0;
        foreach ($array_options as $opt) {

            $si = $opt['weight'] . ' ' . $opt['weight_prefix'];

            if (($opt['id_1c']==$id1c)&&($option['size'] == $si)) {
                $data['option_artman'] = $opt['artman'];
                $data['option_artpost'] = $opt['artpost'];
                $data['availability_1'] = (isset($opt['availability_1']) ? $opt['availability_1'] : '0');
                $data['availability_2'] = (isset($opt['availability_2']) ? $opt['availability_2'] : '0');
                $data['availability_3'] = (isset($opt['availability_3']) ? $opt['availability_3'] : '0');
                if ($opt['action_hide']=='1')
                    $data['action_hide'] = $opt['action_hide'];
                if (isset($opt['text_action']))
                $data['option_action']=$opt['text_action'];
                else $data['option_action']='';



                if (isset($opt['spec'])) {
                    if ($opt['spec'] == 1) {
                        $data['option_action']='Акция';
                    }
                }
                if (isset($opt['gift'])) {
                    if ($opt['gift'] == 1) {
                        $data['option_action']='Подарок';
                    }
                }
                if (isset($opt['markdown'])) {
                    if ($opt['markdown'] == 1) {
                        $data['option_action']='Уценка';
                    }
                }
                if (isset($opt['by_weight'])) {
                    if ($opt['by_weight'] == 1) {
                        $data['option_action']='Развес';
                    }
                }

                break;
            }
        }


        if ($data['availability_2'] == '') $data['availability_2'] = 0;
        if ($data['availability_3'] == '') $data['availability_3'] = 0;
        if ($data['availability_1'] == '') $data['availability_1'] = 0;
        $data['available'] = $data['availability_1'] . '/' . $data['availability_2']. '/' . $data['availability_3'];
        //if (($data['availability_2']>0)||($data['availability_1']>0)){


        $data['stock'] = '';

        $data['stock'] =  $object->get('stock');
        //}
        /* echo "<pre>";
         print_r($data);
         echo "</pre>";*/
        $data['skidka'] = $object->get('skidka');
        $data['discount'] = $object->get('discount');

        $data['name'] = !$object->get('name')
            ? $object->get('product_pagetitle')
            : $object->get('name');

        $options = $object->get('options');
        if (!empty($options) && is_array($options)) {
            $tmp = array();
            foreach ($options as $k => $v) {
                $tmp[] = $this->modx->lexicon('ms2_' . $k) . ': ' . $v;
                $data['option_' . $k] = $v;
            }
            $data['options'] = implode('; ', $tmp);
        }

        if ($data['option_action']) {
            if ($data['option_action']!='') {
                $data['option_size'] = $data['option_size'] . '<br/>' . $data['option_action'];
            }
        }


        /*
        echo '<pre>$data';
        print_r($data);
        echo '</pre>';*/

        return $data;
    }


}

return 'msProductGetListProcessor';