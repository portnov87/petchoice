<?php
//ini_set('display_errors', 1);
//error_reporting(E_ALL | E_STRICT);

class msProductGetListUserProcessor extends modObjectGetListProcessor
{
    public $classKey = 'msOrder';//Product';
    public $languageTopics = array('minishop2:product');
    public $defaultSortField = 'msOrder.createdon';
    public $defaultSortDirection = 'DESC';
    public $permission = 'msorder_list';


    /** {@inheritDoc} */
    public function initialize()
    {

        if (!$this->modx->hasPermission($this->permission)) {
            return $this->modx->lexicon('access_denied');
        }
        return parent::initialize();
    }


    /** {@inheritDoc} */
    public function prepareQueryBeforeCount(xPDOQuery $c)
    {
        //echo 'user'.$this->getProperty('user_id');
        //$c->innerJoin('msOrder', 'msOrder', '`msOrderProduct`.`order_id` = `msOrder`.`id`');
        $c->leftJoin('msOrderProduct', 'msOrderProduct', '`msOrder`.`id` = `msOrderProduct`.`order_id`');
        $c->leftJoin('msProduct', 'msProduct', '`msOrderProduct`.`product_id` = `msProduct`.`id`');
        $c->leftJoin('msProductData', 'msProductData', '`msOrderProduct`.`product_id` = `msProductData`.`id`');
        $c->leftJoin('msOrderStatus', 'msOrderStatus', '`msOrder`.`status` = `msOrderStatus`.`id`');
        $c->where(array(
            //'order_id' => 889//$this->getProperty('order_id')
            'msOrder.user_id' => $this->getProperty('user_id'),
            //'msOrder.status' => 2,
        ));

        $c->select($this->modx->getSelectColumns('msOrder', 'msOrder', 'ord_'));
        $c->select($this->modx->getSelectColumns('msOrderStatus', 'msOrderStatus', 'status_'));
        $c->select($this->modx->getSelectColumns('msOrderProduct', 'msOrderProduct'));
        $c->select($this->modx->getSelectColumns('msProduct', 'msProduct', 'product_'));
        $c->select($this->modx->getSelectColumns('msProductData', 'msProductData', 'product_', array('id'), true));


        return $c;

    }


    /** {@inheritDoc} */
    public function prepareRow(xPDOObject $object)
    {
        $fields = array_map('trim', explode(',', $this->modx->getOption('ms2_order_product_fields', null, '')));
        $fields = array_values(array_unique(array_merge($fields, array('id', 'product_id', 'name', 'product_pagetitle'))));
        $data = array();
        if ($object->get('ord_num') == 'Магазин') {
            foreach ($fields as $v) {
                if ($v == 'product_options') {
                    $option = $this->modx->fromJSON($object->get('options'));

                    if (isset($option['size']))
                        $data[$v] = $option['size'];
                } else {
                    $data[$v] = $object->get($v);
                    if ($v == 'price_old' || $v == 'product_price' || $v == 'product_old_price') {
                        $data[$v] = round($data[$v], 2);
                    } else if ($v == 'product_weight') {
                        $data[$v] = round($data[$v], 3);
                    }
                }
            }

            $data['skidka'] = $object->get('skidka');
            $data['discount'] = $object->get('discount');
// дата покупки
            $data['createdon'] = $object->get('ord_createdon');

            $data['order_num'] = $object->get('ord_num');
            $data['option_artman'] = $data['option_artpost'] = '';

            $data['name'] = '';

            $data['status'] = 'Доставлен';
            $data['cost'] = $object->get('ord_cost');
            $data['price_old'] = '';
            $data['product_price'] = '';
            $data['product_old_price'] = '';

            $data['options'] = '';
        } else {

            foreach ($fields as $v) {
                if ($v == 'product_options') {
                    $option = $this->modx->fromJSON($object->get('options'));

                    if (isset($option['size']))
                        $data[$v] = $option['size'];
                } else {
                    $data[$v] = $object->get($v);
                    if ($v == 'price_old' || $v == 'product_price' || $v == 'product_old_price') {
                        $data[$v] = round($data[$v], 2);
                    } else if ($v == 'product_weight') {
                        $data[$v] = round($data[$v], 3);
                    }
                }
            }


            $product_id = $object->get('product_id');

            $q = $this->modx->newQuery('modTemplateVar');
            $q->innerJoin('modTemplateVarResource', 'v', 'v.tmplvarid = modTemplateVar.id');
            $q->where(array(
                'v.contentid' => $product_id,
                'modTemplateVar.id' => 1,
            ));
            $q->limit(1);
            $q->select(array('modTemplateVar.*', 'v.value'));
            $value = array();
            $array = array();
            $value_options = '';
            if ($q->prepare() && $q->stmt->execute()) {
                while ($tv = $q->stmt->fetch(PDO::FETCH_ASSOC)) {
                    $value_options = $tv['value'];
                }
            }

            $array_order = $object->toArray();

            //$option = $object->get('options');//
            $option = $this->modx->fromJSON($object->get('options'));
            if ($value_options != '')
                $array_options = $this->modx->fromJSON($value_options);
            else $array_options = [];
            /*echo '<pre>$array_order';
            print_r($array_order);
            echo '</pre>';
            die();*/

            foreach ($array_options as $opt) {
                $si = $opt['weight'] . ' ' . $opt['weight_prefix'];
                if ($option['size'] == $si) {
                    $data['option_artman'] = $opt['artman'];
                    $data['option_artpost'] = $opt['artpost'];
                    break;
                }
            }

            $data['skidka'] = $object->get('skidka');
            $data['discount'] = $object->get('discount');
// дата покупки
            $data['createdon'] = $object->get('ord_createdon');

            $data['order_num'] = $object->get('ord_num');

            $data['status'] = $object->get('status_name');
            $data['name'] = !$object->get('name')
                ? $object->get('product_pagetitle')
                : $object->get('name');

            //$options = $object->get('options');
            $options = $this->modx->fromJSON($object->get('options'));
            if (!empty($options) && is_array($options)) {
                $tmp = array();
                foreach ($options as $k => $v) {
                    $tmp[] = $this->modx->lexicon('ms2_' . $k) . ': ' . $v;
                    $data['option_' . $k] = $v;
                }
                $data['options'] = implode('; ', $tmp);
            }


        }

        return $data;
    }
/*
    public function iterate(array $data)
    {
        $list = array();
        $list = $this->beforeIteration($list);
        $this->currentIndex = 0;

        $list[] = array('id' => 0, 'name' => 'Все');
        foreach ($data['results'] as $object) {
            if ($this->modx->user->hasSessionContext('mgr') && !$this->modx->user->isMember('Administrator')) {
                //echo $object->get('id').'<br/>';
                if ($object->get('id') == 2)
                    continue;
            }
            if ($this->checkListPermission && $object instanceof modAccessibleObject && !$object->checkPolicy('list')) continue;
            $objectArray = $this->prepareRow($object);
            if (!empty($objectArray) && is_array($objectArray)) {
                $list[] = $objectArray;
                $this->currentIndex++;
            }
        }

        $list = $this->afterIteration($list);
        return $list;
    }
*/




}

return 'msProductGetListUserProcessor';