<?php
/*ini_set('display_errors', 1);
ini_set('error_reporting', E_ALL);

ini_set('display_startup_errors', 1);*/
class msOrderGetProcessor extends modObjectGetProcessor
{
    public $classKey = 'msOrder';
    public $languageTopics = array('minishop2:default');
    public $permission = 'msorder_view';


    /** {@inheritDoc} */
    public function initialize()
    {
        if (!$this->modx->hasPermission($this->permission)) {
            return $this->modx->lexicon('access_denied');
        }
        return parent::initialize();
    }


    /** {@inheritDoc} */
    public function cleanup()
    {

        $_SESSION['city_id'] = false;

        //error_reporting(E_ALL | E_STRICT);
        //ini_set('display_errors', 1);

        $array = $this->object->toArray();
        /*   echo "<pre>";
           print_r($array);
           echo "</pre>";*/
        if ($array['status'] == 0) {
            $this->object->set('status', 1);
            $this->object->save();
            $array = $this->object->toArray();
        }
        /*echo '<pre>$array';
        print_r($array);
        echo '</pre>';*/
        if ($address = $this->object->getOne('Address')) {
            $array = array_merge($array, $address->toArray('addr_'));
        }


        //$weight
        /*echo '<pre>$array';
        print_r($array);
        echo '</pre>';
        echo '<pre>$address ';
        print_r($address->toArray());
        echo '</pre>';*/

        if ($profile = $this->object->getOne('UserProfile')) {
            $array['fullname'] = $profile->get('fullname');
            $array['addr_email'] = $profile->get('email');
            $discount_customer = '-';
            $profilearray = $profile->toArray();
            $attribute = $profilearray['extended'];

            if (isset($attribute['discount'])) {
                $discount_customer = $attribute['discount'];
                if ($discount_customer != '') $discount_customer .= '%'; else $discount_customer = '-';
            } else $discount_customer = '-';
            $array['discount_customer'] = $discount_customer;


            $array['email'] = $profilearray['email'];
            $address_array = array();
            if (isset($attribute['street']) && ($attribute['street'] != '')) $address_array[] = $attribute['street'];
            if (isset($attribute['house']) && ($attribute['house'] != '')) $address_array[] = 'д. ' . $attribute['house'];
            if (isset($attribute['room']) && ($attribute['room'] != '')) $address_array[] = 'кв. ' . $attribute['room'];
            $array['address'] = implode(', ', $address_array);

            if (isset($attribute['warehouse']) && ($attribute['warehouse'] != '') && ($array['addr_warehouse'] == '')) {
                $array['add_warehouse'] = $attribute['warehouse'];
                $array['warehouse'] = $attribute['warehouse'];
            }


            if (isset($attribute['city_id']) && ($attribute['city_id'] != '')) $city_id = $attribute['city_id'];
            else $city_id = '';


            if (($array['addr_street'] == '') && isset($attribute['street']) && ($attribute['street'] != '')) $array['addr_street'] = $attribute['street'];
            if (($array['addr_room'] == '') && isset($attribute['room']) && ($attribute['room'] != '')) $array['addr_room'] = $attribute['room'];
            if (($array['addr_building'] == '') && isset($attribute['house']) && ($attribute['house'] != '')) $array['addr_building'] = $attribute['house'];


            if (($array['addr_doorphone'] == '') && isset($attribute['doorphone']) && ($attribute['doorphone'] != '')) $array['addr_doorphone'] = $attribute['doorphone'];
            if (($array['addr_housing'] == '') && isset($attribute['housing']) && ($attribute['housing'] != '')) $array['addr_housing'] = $attribute['housing'];
            if (($array['addr_parade'] == '') && isset($attribute['parade']) && ($attribute['parade'] != '')) $array['addr_parade'] = $attribute['parade'];
            if (($array['addr_floor'] == '') && isset($attribute['floor']) && ($attribute['floor'] != '')) $array['addr_floor'] = $attribute['floor'];


        } else {
            $array['fullname'] = $this->modx->lexicon('no');
            $array['discount_customer'] = '-';
            $array['email'] = '';
            $city_id = '';
        }

        // необходимо вычислить скидку общую по заказу - сумма базовых цен - цены по скидкам
        $products = $this->object->getMany('Products');
        $costold = 0;
        $weight = 0;

        foreach ($products as $product) {
            $productsar = $product->toArray();
            $costold += $productsar['cost_old'];
            /*echo "<pre>";
            print_r($productsar);
            echo "</pre>";*/
            if (isset($productsar['options'])) {
                if (isset($productsar['options']['size'])) {

                    $size = explode(' ', $productsar['options']['size']);

                    if (isset($size[1])) {
                        if ($size[1] == 'кг') {
                            $weight += (float)$size[0];
                        } elseif ($size[1] == 'гр') {
                            $weight += round($size[0] / 1000, 2);
                        }

                    }
                }
                //$weight
            }
            /*echo '<pre>';
                print_r($productsar);
                echo '</pre>';*/
        }
        /*echo "<pre>";
        print_r($weight);
        echo "</pre>";*/
        if ($weight < 0.1)
            $weight = 0.1;

        $nameware = '';
        if (($array['delivery'] == 2) || ($array['delivery'] == 3)) {
            //echo 'delivery'.$array['delivery'];
            $delivery = $this->modx->getObject('msDelivery', $array['delivery']);
            if ($delivery&&($city_id != '') && ($array['warehouse'] != '') && (isset($array['warehouse'])) && ($array['addr_warehouse'] == '')) {
                $idware = $array['warehouse'];
                $nameware = '';

                $warehouses = $delivery->getWarehouses($city_id);

                // echo '$city_id'.$city_id;
                //if ()
                foreach ($warehouses as $warehouse) {
                    if ($warehouse['Number'] == $idware) {
                        $nameware = $warehouse['DescriptionRu'];
                        $idware = $warehouse['Ref'];
                    }
                }

                if ($nameware = '') {
                    foreach ($warehouses as $warehouse) {
                        if ($warehouse['DescriptionRu'] == $array['warehouse']) {
                            $nameware = $warehouse['DescriptionRu'];
                            $idware = $warehouse['Ref'];
                        }

                    }
                }
                $array['addr_warehouse'] = $nameware;
                $array['addr_warehouse_id'] = $idware;
                $array['np_warehouse'] = $idware;


            }


        }


        $array['phone_sms'] = $array['addr_phone'];


        $cart_cost = round($array['cart_cost'], 0);
        $cost = round($array['cost'], 0);

        $skidka = $costold - $array['cart_cost'];
        $discount_db = $this->object->get('discount');
        if ($skidka < 0) $skidka = 0;
        if ($discount_db > 0) $array['skidka'] = $discount_db;
        else $array['skidka'] = $skidka;


        if ($array['promocode'] == '') $array['promocode'] = '-';
        else {
            $promocode = $array['promocode'];
            $item_promocode = $this->modx->getObject('PromocodeItem', array('code' => $promocode));
            if ($item_promocode) {
                $promocode = $array['promocode'] . ' (' . $item_promocode->discount . ')';
                $array['promocode'] = $promocode;
            }
        }

        $array['createdon'] = $this->modx->miniShop2->formatDate($array['createdon']);
        $array['updatedon'] = $this->modx->miniShop2->formatDate($array['updatedon']);

        $array['cart_cost_new'] = $cart_cost . ' / ' . $cost;
        $user = $this->object->getOne('User');
        if ($user) $array['bonuses_user'] = $user->get('bonuses');


        /**
         * поля для новой почты
         */
        $array['np_firstname'] = $array['addr_receiver'];
        $array['np_lastname'] = $array['addr_lastname'];
        $array['np_phone'] = $array['addr_phone'];


        $array['np_city'] = $array['city_id_np'];//['title'=>'ffff','id'=>$array['city_id_np']];

        //$array['addr_city_name_np']='fffff';
        $city_name = $array['addr_city'];
        if (strpos($city_name, '-') !== false) {
            $city_name = substr($city_name, 0, strpos($city_name, '-'));
        }

        if ($array['city_id_np'] != '') {
            $city_model = $this->modx->getObject('msNpCities', ['id' => $array['city_id_np']]);
            if ($city_model) {
                $addr_city_id_np = $city_model->get('id');
                $addr_city_id_name = $city_model->get('DescriptionRu');
                $city_name = $addr_city_id_name;
            }

        }
        if (($city_name != '') && ($addr_city_id_np == '')) {
            $city_model = $this->modx->getObject('msNpCities', ['DescriptionRu' => $city_name]);
            if ($city_model) {
                $addr_city_id_np = $city_model->get('id');
                //$addr_city_id_name = $city_model->get('DescriptionRu');
                //$city_name=$addr_city_id_name;
            }


        }


        if (($array['delivery'] == 5) ||($array['delivery'] == 2)) {// || ($array['delivery'] == 5)
            $delivery = $this->modx->getObject('msDelivery', $array['delivery']);
           // echo '$city_id=' . $city_name. ' '. $array['addr_street'];
            //if (($array['delivery'] == 5) ||($array['delivery'] == 2)
            if ($delivery&&($city_name!='')&&($addr_city_id_np!='')) {
                $street = $delivery->getStreet($addr_city_id_np, $array['addr_street']);
                /*echo '<pre>$street';
                print_r($street);
                echo '</pre>';*/
                if (isset($street[0])){
                    if (isset($street[0]['Ref'])){
                        $array['city_street_np']= $street[0]['Ref'];
                        $array['np_steet']= $street[0]['Description'];

                    }
                }
            }
        }
        $array['np_city_name'] = $city_name;
        $array['np_city'] = $city_name;//$addr_city_id_np;//$addr_city_id_np;//
        $array['addr_city_id_np'] = $addr_city_id_np;//$city_name;//$addr_city_id_np;//['title'=>'ffff','id'=>$array['addr_city_id_np']];


        $array['addr_city_name_np'] = $city_name;
        //addr_street
        //np_city
        //addr_city_id_np

        $array['np_warehouse'] = $array['addr_warehouse_id'];
        $array['np_address_street'] = $array['addr_street'];
        $array['np_address_building'] = $array['addr_building'];
        $array['np_address_flat'] = $array['addr_room'];
        //$array['np_address']=implode(', ',$adr_np);
        $array['np_summa'] = $array['cart_cost'];


        //


        switch ($array['delivery']) {
            case  '2':
                $array['np_receiving'] = 'otdelenie';
                $free_del = $this->modx->getOption('free_delivery_2');
                if ($array['cart_cost'] >= $free_del) {
                    $array['np_whopay'] = 'sender';
                } else {
                    $array['np_whopay'] = 'reciever';
                }


                break;
            case  '5':

                $free_del = $this->modx->getOption('free_delivery_5');
                if ($array['cart_cost'] >= $free_del) {
                    $array['np_whopay'] = 'sender';
                } else {
                    $array['np_whopay'] = 'reciever';
                }

                $array['np_receiving'] = 'address';
                break;

        }

        switch ($array['payment']) {
            case '3':
            case '4':
                $array['np_method_payment'] = 'card';
                break;
            case '6':
                $array['np_method_payment'] = 'nal';
                break;

        }
        //echo 'payment'.$array['payment']."\r\n";
        /* echo  '<pre>$array';
         print_R($array);
         echo  '</pre>';
         */

        /*echo '<pre>$array';
        print_R($array);
        echo '</pre>';*/
        //np_address_street
        //При выбранном "Приват 24" или "Visa и MasterCard" = "Оплата на карту", "Наложенный платеж" = "Наложенный платеж"


        //В отделение бесплатно (Отправитель) при сумме заказа >=999 грн, бесплатно при сумме заказа >=1199

        //city_street_np

        $array['np_weight'] = $weight;
        $array['np_countmesta'] = 1;
        $array['np_date_shipping'] = $array['date_shipping'];

        //$array['number_ttn']=$array['number_ttn'];
        //$array['date_create_ttn']=$array['date_create_ttn'];
        //$array['date_shipping_np']
        //$array['cost_shipping_np']


        return $this->success('', $array);
    }
}

return 'msOrderGetProcessor';