<?php

class msOrderGetListProcessor extends modObjectGetListProcessor {
	public $classKey = 'msOrder';
	public $languageTopics = array('default','minishop2:manager');
	public $defaultSortField = 'id';
	public $defaultSortDirection  = 'DESC';
	public $permission = 'msorder_list';
	/** @var  miniShop2 $ms2 */
	protected $ms2;

	/** {@inheritDoc} */
	public function initialize() {
		$this->ms2 = $this->modx->getService('miniShop2');

		if (!$this->modx->hasPermission($this->permission)) {
			return $this->modx->lexicon('access_denied');
		}
		return parent::initialize();
	}


	/** {@inheritDoc} */
	public function prepareQueryBeforeCount(xPDOQuery $c) {
		$c->leftJoin('modUser','modUser', '`msOrder`.`user_id` = `modUser`.`id`');
		$c->leftJoin('modUserProfile','modUserProfile', '`msOrder`.`user_id` = `modUserProfile`.`internalKey`');
		$c->leftJoin('msOrderStatus','msOrderStatus', '`msOrder`.`status` = `msOrderStatus`.`id`');
        $c->leftJoin('msPaymentStatus','msPaymentStatus', '`msOrder`.`payment_status` = `msPaymentStatus`.`id`');
		//paymentstatus
		$c->leftJoin('msDelivery','msDelivery', '`msOrder`.`delivery` = `msDelivery`.`id`');
		$c->leftJoin('msPayment','msPayment', '`msOrder`.`payment` = `msPayment`.`id`');
		$c->leftJoin('msOrderAddress','msOrderAddress', '`msOrder`.`address` = `msOrderAddress`.`id`');
		$c->leftJoin('msOrderProduct', 'msOrderProduct', 'msOrder.id = msOrderProduct.order_id');
		$orderColumns = $this->modx->getSelectColumns('msOrder', 'msOrder', '', array('status','delivery','payment'), true);
		$c->select($orderColumns . ', `modUserProfile`.`fullname` as `customer`,`msOrderAddress`.`receiver`, `msOrderAddress`.`lastname`, `modUserProfile`.`extended` as `extended`, `modUser`.`username` as `customer_username`, `msPaymentStatus`.`name` as `paymentstatus`, `msPaymentStatus`.`color` as `paymentcolor`,  `msOrderStatus`.`name` as `status`, `msOrderStatus`.`color`, `msDelivery`.`name` as `delivery`, `msPayment`.`name` as `payment`');
//`modUserProfile`.`fullname` as `customer`,
        $c->where(array('num:!='=>'Магазин'));

        $c->where(array('kkm:!='=>1));
		if ($query = $this->getProperty('query')) {
			$query=trim($query);
			$c->where(array(
				'num:LIKE' => '%'.$query.'%'
				,'OR:comment:LIKE' => '%'.$query.'%'
				,'OR:modUserProfile.fullname:LIKE' => '%'.$query.'%'
				,'OR:msOrderProduct.name:LIKE' => '%'.$query.'%'
				
				
				//,'`msOrder`.`status`!='=>8
			));
		}
		if ($status = $this->getProperty('status')) {
			$c->where(array('status' => $status));
		}else{
			$c->where(array('status:!=' => 8));
		}
		
		//echo 'status';
		if ($delivery = $this->getProperty('delivery')) {
			$c->where(array('delivery' => $delivery));
		}
		if ($payment = $this->getProperty('payment')) {
			$c->where(array('payment' => $payment));
		}
                
                if ($updatedon = $this->getProperty('updatedon')) {
                    $updatedon=date('Y-m-d',strtotime($updatedon));
			
		
                     if ($updatedonto = $this->getProperty('updatedonto')) {
                    
                    //if ($updatedonto){
                        $updatedonto=date('Y-m-d',strtotime($updatedonto));
                        
                        $c->where(array('updatedon:>=' => $updatedon.' 00:00:00'));
                        $c->where(array('updatedon:<=' => $updatedonto.' 23:59:59'));
                    }else{
                        
			$c->where(array('updatedon:>=' => $updatedon.' 00:00:00'));
                        $c->where(array('updatedon:<=' => $updatedon.' 23:59:59'));
                    }
                    
		}elseif ($updatedonto = $this->getProperty('updatedonto')) {
                    $updatedonto=date('Y-m-d',strtotime($updatedonto));
                    $c->where(array('updatedon:>=' => $updatedonto.' 00:00:00'));
                        $c->where(array('updatedon:<=' => $updatedonto.' 23:59:59'));
                }
                
                
                
                
                if ($date_shipping = $this->getProperty('date_shipping')) {
                    $date_shipping=date('Y-m-d',strtotime($date_shipping));
                    
                    $date_shipping=str_replace(' 00:00:00','',$date_shipping);
                    $date_shipping=str_replace(' 00:00','',$date_shipping);
                    if ($date_shipping_to = $this->getProperty('date_shipping_to')) {
                        $date_shipping_to=date('Y-m-d',strtotime($date_shipping_to));
                     
                            $date_shipping_to=str_replace(' 00:00:00','',$date_shipping_to);
                            $date_shipping_to=str_replace(' 00:00','',$date_shipping_to);
                           
                            $c->where(array('date_shipping:>=' => $date_shipping));
                            $c->where(array('date_shipping:<=' => $date_shipping_to));
                    }else{
                        $c->where(array('date_shipping' => $date_shipping));
                    }
                      
		}
               
                
                if ($time_shipping = $this->getProperty('time_shipping')) {
                    $time_shipping=date('Y-m-d',strtotime($time_shipping));
                    $time_shipping=str_replace(' 00:00:00','',$time_shipping);
			$c->where(array('time_shipping' => $time_shipping));
		}
                

		return $c;
	}


	/** {@inheritDoc} */
	public function getData() {
		$data = array();
		$limit = intval($this->getProperty('limit'));
		$start = intval($this->getProperty('start'));

		/* query for chunks */
		$c = $this->modx->newQuery($this->classKey);
		$c = $this->prepareQueryBeforeCount($c);
		$data['total'] = $this->modx->getCount($this->classKey,$c);
		$c = $this->prepareQueryAfterCount($c);

		$sortClassKey = $this->getSortClassKey();
		$sortKey = $this->modx->getSelectColumns($sortClassKey,$this->getProperty('sortAlias',$sortClassKey),'',array($this->getProperty('sort')));
		if (empty($sortKey)) $sortKey = $this->getProperty('sort');
                
                if ($updatedonto = $this->getProperty('updatedonto')) {
                $c->sortby('updatedon','DESC');
                }else{
		$c->sortby($sortKey,$this->getProperty('dir'));
                }
		if ($limit > 0) {
			$c->limit($limit,$start);
                       // echo $limit.' st '.$start.'<br/>';
		}
                $c->groupBy('id');
		if ($c->prepare() && $c->stmt->execute()) {
			$data['results'] = $c->stmt->fetchAll(PDO::FETCH_ASSOC);
		}

		return $data;
	}


	/** {@inheritDoc} */
	public function iterate(array $data) {
		$list = array();
		$list = $this->beforeIteration($list);
		$this->currentIndex = 0;
		/** @var xPDOObject|modAccessibleObject $object */
		foreach ($data['results'] as $array) {
			$list[] = $this->prepareArray($array);
			$this->currentIndex++;
		}
		$list = $this->afterIteration($list);
		return $list;
	}


	/** {@inheritDoc} */
	public function prepareArray(array $data) {
		if (empty($data['customer'])) {
			$data['customer'] = $data['customer_username'];
		}
		
		/*echo '<pre>extended';
		print_r($data['extended']);
		echo '</pre>';*/
		$extended=$this->modx->fromJSON($data['extended']);
		if (isset($extended['lastname'])&&($extended['lastname']!='')){
			//$data['customer']=$data['customer'];//$extended['lastname'].' '.;
			if ($data['lastname']!='')
				$data['customer']=$data['receiver'].' '.$data['lastname'];//, `msOrderAddress`.`lastname`,
			else $data['customer']=$extended['lastname'].' '.$data['customer'];
		}
			
		$data['status'] = '<span style="color:#'.$data['color'].';">'.$data['status'].'</span>';
                $data['time_shipping']='';
                //if ($data['time_shipping_from']!='00:00:00'){
                    
                    if (strpos($data['time_shipping_from'],':00:00')!==false)
                       $data['time_shipping_from']=str_replace(':00:00',':00',$data['time_shipping_from']);
                        else $data['time_shipping_from']=str_replace(':00','',$data['time_shipping_from']);
                        
                    if (strpos($data['time_shipping_to'],':00:00')!==false)
                       $data['time_shipping_to']=str_replace(':00:00',':00',$data['time_shipping_to']);
                        else $data['time_shipping_to']=str_replace(':00','',$data['time_shipping_to']);
                    
                $data['time_shipping'].=$data['time_shipping_from'];
                    if ($data['time_shipping_to']!='00:00:00')$data['time_shipping'].=' - '.$data['time_shipping_to'];
                    
                    
                    //$data['time_shipping']=str_replace(':00','',$data['time_shipping']);
               // }


        $payment='<span style="color:#'.$data['color'].';">'.$data['payment'].'</span>';

        $payment.='<br/><span style="color:#'.$data['paymentcolor'].';">'.$data['paymentstatus'].'</span>';
        $data['payment'] = $payment;
		$data['actions'] = array(
			array(
				'className' => 'update',
				'text' => $this->modx->lexicon('ms2_menu_update'),
			),
			/*array(
				'className' => 'delete',
				'text' => $this->modx->lexicon('ms2_menu_remove'),
			),*/
		);


        $user_update = $this->modx->newQuery('msOrderLog');
        //$user_update->leftJoin('modUser','modUserclient', '`msOrder`.`user_id` = `modUserclient`.`id`');
        $user_update->leftJoin('msOrder','msOrder', '`msOrder`.`id` = `msOrderLog`.`order_id`');
        $user_update->leftJoin('modUser','modUser', '`msOrderLog`.`user_id` = `modUser`.`id`');
        $user_update->leftJoin('modUserProfile','modUserProfile', '`msOrderLog`.`user_id` = `modUserProfile`.`internalKey`');
        $user_update->where(array('order_id' => $data['id']));
        $user_update->limit(1);


        $exclude = array();
        $add_select = '`msOrder`.`user_id`,`modUser`.`id` as `user_manager`, `modUser`.`username`, `modUserProfile`.`fullname`';

        $user_update->leftJoin('msOrderStatus','msOrderStatus', '`msOrderLog`.`entry` = `msOrderStatus`.`id`');
            $exclude[] = 'entry';
            $add_select .= ', `msOrderLog`.`order_id`, `msOrderLog`.`message`, `msOrderStatus`.`name` as `entry`, `msOrderStatus`.`color`';


        //$select = $this->modx->getSelectColumns('msOrderLog', 'msOrderLog', '', $exclude, true);
        $select = $add_select;

        $user_update->select($select);
        $user_update->sortby('timestamp','DESC');
        if ($user_update->prepare() && $user_update->stmt->execute()) {
            while ($row = $user_update->stmt->fetch(PDO::FETCH_ASSOC)) {
                   if ($row['user_id']!=$row['user_manager']) {
                       $data['status'] = $data['status'] . '<br/><span class="gray">' . $row['fullname'] . '</span>';
                   }
            }
        }



		if (isset($data['cost'])) $data['cost'] = $this->ms2->formatPrice(round($data['cost'],0),0);
		if (isset($data['cart_cost'])) $data['cart_cost'] = $this->ms2->formatPrice(round($data['cart_cost'],0),0);
		if (isset($data['delivery_cost'])) $data['delivery_cost'] = $this->ms2->formatPrice($data['delivery_cost']);
		if (isset($data['weight'])) $data['weight'] = $this->ms2->formatWeight($data['weight']);

		if ($data['number_ttn']!='') {
            $data['delivery'] = $data['delivery'] . "<br/><span class=\"gray\">" . $data['status_np'] . ' </span><br/><span class="gray">(на ' . $data['date_update_status'] . ')</span>';
        }
        /*
		echo "<pre>";
		print_r($data);
		echo "</pre>";*/

		return $data;
	}


}

return 'msOrderGetListProcessor';
