<?php

class msOrderLogGetListProcessor extends modObjectGetListProcessor {
	public $classKey = 'msOrderLog';
	public $languageTopics = array('default','minishop2:manager');
	public $defaultSortField = 'id';
	public $defaultSortDirection  = 'DESC';
	public $permission = 'msorder_view';


	/** {@inheritDoc} */
	public function initialize() {
		if (!$this->modx->hasPermission($this->permission)) {
			return $this->modx->lexicon('access_denied');
		}
		return parent::initialize();
	}


	/** {@inheritDoc} */
	public function prepareQueryBeforeCount(xPDOQuery $c) {
		$type = $this->getProperty('type');
		if (!empty($type)) {
			$c->where(array('action' => $type));
		}
		else
		{
			//$c->where(array('action' => 'status'));
			$type='status';
		}
		$order_id = $this->getProperty('order_id');
		if (!empty($order_id)) {
			$c->where(array('order_id' => $order_id));
		}

		$c->leftJoin('modUser','modUser', '`msOrderLog`.`user_id` = `modUser`.`id`');
		$c->leftJoin('modUserProfile','modUserProfile', '`msOrderLog`.`user_id` = `modUserProfile`.`internalKey`');
		$exclude = array();
		$add_select = ' , `modUser`.`username`, `modUserProfile`.`fullname`';
		if ($type == 'status') {
			$c->leftJoin('msOrderStatus','msOrderStatus', '`msOrderLog`.`entry` = `msOrderStatus`.`id`');
			$exclude[] = 'entry';
			$add_select .= ', `msOrderLog`.`message`, `msOrderStatus`.`name` as `entry`, `msOrderStatus`.`color`';
		}
		/*elseif ($type == 'paymentstatus') {
        $c->leftJoin('msPaymentStatus','msPaymentStatus', '`msOrderLog`.`entry` = `msPaymentStatus`.`id`');
        $exclude[] = 'entry';
        $add_select .= ', `msOrderLog`.`message`, `msPaymentStatus`.`name` as `entry`, `msPaymentStatus`.`color`';
    }*/

		$select = $this->modx->getSelectColumns('msOrderLog', 'msOrderLog', '', $exclude, true);
		$select .= $add_select;
//echo '$select'.$select;
		$c->select($select);
		return $c;
	}


	/** {@inheritDoc} */
	public function getData() {
		$data = array();
		$limit = intval($this->getProperty('limit'));
		$start = intval($this->getProperty('start'));

		/* query for chunks */
		$c = $this->modx->newQuery($this->classKey);
		$c = $this->prepareQueryBeforeCount($c);
		$data['total'] = $this->modx->getCount($this->classKey,$c);
		$c = $this->prepareQueryAfterCount($c);

		$sortClassKey = $this->getSortClassKey();
		$sortKey = $this->modx->getSelectColumns($sortClassKey,$this->getProperty('sortAlias',$sortClassKey),'',array($this->getProperty('sort')));
		if (empty($sortKey)) $sortKey = $this->getProperty('sort');
		$c->sortby($sortKey,$this->getProperty('dir'));
		if ($limit > 0) {
			$c->limit($limit,$start);
		}

		if ($c->prepare() && $c->stmt->execute()) {
			$data['results'] = $c->stmt->fetchAll(PDO::FETCH_ASSOC);
			$results=$data['results'];
			foreach ($results as $key=>$res)
			{
				if ($res['action']=='status'){
					$data['results'][$key]['action']='Смена статуса';
				}
                elseif ($res['action']=='1c_chnage'){
                    $data['results'][$key]['action']='Изменение чеез 1с';
                    $data['results'][$key]['entry']=$res['message'];
                }
                elseif ($res['action']=='bonuses'){
                    $data['results'][$key]['action']='Применение бонусов';
                    $data['results'][$key]['entry']=$res['message'];
                }
				elseif ($res['action']=='sms'){
					$data['results'][$key]['action']='Отправка смс';
					$data['results'][$key]['entry']='<strong>Текст сообщения:</strong> "'.$res['message'].'"';
				}
				elseif ($res['action']=='add_comment'){
					$data['results'][$key]['action']='Добавлен комментарий';
					$data['results'][$key]['entry']='<strong>Текст комментария:</strong> "'.$res['message'].'"';
				}
                elseif ($res['action']=='add_comment_client'){
                    $data['results'][$key]['action']='Добавлен комментарий от клиента';
                    $data['results'][$key]['entry']='<strong>Текст комментария клиента:</strong> "'.$res['message'].'"';
                }
                elseif ($res['action']=='update_comment_client'){
                    $data['results'][$key]['action']='Изменен комментарий от клиента';
                    $data['results'][$key]['entry']='<strong>Текст комментария клиента:</strong> "'.$res['message'].'"';
                }
				elseif ($res['action']=='update_comment'){
					$data['results'][$key]['action']='Изменён комментарий';
					$data['results'][$key]['entry']='<strong>Текст комментария:</strong> "'.$res['message'].'"';
				}
				elseif ($res['action']=='add_product'){
					$data['results'][$key]['action']='Добавлен продукт';
					$data['results'][$key]['entry']=$res['message'];
				}
				elseif ($res['action']=='update_product'){
					$data['results'][$key]['action']='Обновлён продукт';
					$data['results'][$key]['entry']=$res['message'];
				}
				elseif ($res['action']=='remove_product'){
					$data['results'][$key]['action']='Удалён продукт';
					$data['results'][$key]['entry']=$res['message'];
				}
				elseif ($res['action']=='print'){
					$data['results'][$key]['action']='Печать';
					$data['results'][$key]['entry']=$res['message'];
				}
                elseif ($res['action']=='add_dateshipping'){
					$data['results'][$key]['action']='Добавлена дата доставки';
					$data['results'][$key]['entry']=$res['message'];
				}
                elseif ($res['action']=='update_dateshipping'){
					$data['results'][$key]['action']='Изменена дата доставки';
					$data['results'][$key]['entry']=$res['message'];
				}elseif ($res['action']=='paymentstatus'){
                    $data['results'][$key]['action']='Смена статуса оплаты';
                    $data['results'][$key]['entry']=$res['message'];
                }
				
					
			}
			/*echo '<pre>$results';
			print_r($data['results']);
			echo '</pre>';*/
		}

		return $data;
	}


	/** {@inheritDoc} */
	public function iterate(array $data) {
		$list = array();
		$list = $this->beforeIteration($list);
		$this->currentIndex = 0;
		/** @var xPDOObject|modAccessibleObject $object */
		foreach ($data['results'] as $array) {
			$list[] = $this->prepareArray($array);
			$this->currentIndex++;
		}
		$list = $this->afterIteration($list);
		return $list;
	}


	/** {@inheritDoc} */
	public function prepareArray(array $data) {
		if (!empty($data['color'])) {
			$data['entry'] = '<span style="color:#'.$data['color'].';">'.$data['entry'].'</span>';
		}

		return $data;
	}

}

return 'msOrderLogGetListProcessor';