<?php
//error_reporting(E_ALL | E_STRICT);
//ini_set('display_errors', 1);
//ini_set('error_reporting', E_ALL);

//ini_set('display_startup_errors', 1);
/**
 * Get a list of Products
 *
 * @package minishop2
 * @subpackage processors
 */
class msNpGetcityProcessor extends modObjectGetListProcessor {
	public $classKey = 'msNpCities';
	//public $languageTopics = array('default','minishop2:product');
	public $defaultSortField = 'DescriptionRu';
	public $defaultSortDirection  = 'ASC';
	public $parent = 0;


	/** {@inheritDoc} */
	public function initialize() {
		if (!$this->getProperty('limit')) {$this->setProperty('limit', 20);}

		return parent::initialize();
	}

	/** {@inheritDoc} */
	public function prepareQueryBeforeCount(xPDOQuery $c) {


        $order=$this->modx->getObject('msOrder',$this->getProperty('order_id'));
        if ($query = $this->getProperty('query',null)) {

            $queryWhere = array(
                'msNpCities.DescriptionRu:LIKE' => $query.'%'
            );
            $c->where($queryWhere);
        }
        elseif ($order)
        {
            $id_c = $this->getProperty('id',false);
            if ($id_c&&($id_c!='')){
                $queryWhere = array(
                    'msNpCities.id:=' => $id_c
                );
                $c->where($queryWhere);
            }else {

                $address = $this->modx->getObject('msOrderAddress', $order->get('address'));

                if ($address) {
                    $queryWhere = [];
                    $city = trim($address->get('city_id_np'));
                    if ($city == '') {
                        $city_name = trim($address->get('city'));//$array['addr_city'];
                        if (strpos($city_name, '-') !== false) {
                            $city_name = trim(substr($city_name, 0, strpos($city_name, '-')));
                        }
                        $queryWhere = array(
                            'msNpCities.DescriptionRu:LIKE' => $city_name . '%'
                        );


                    } else {


                        $queryWhere = array(
                            'msNpCities.id:=' => $city// . '%'
                        );
                    }
                    /*echo '<pre>$queryWhere';
                    print_r($queryWhere);
                    echo  '</pre>';*/
                    $c->where($queryWhere);
                }
            }
        }



		return $c;
	}


	/** {@inheritDoc} */
	public function prepareQueryAfterCount(xPDOQuery $c) {
		$c->groupby($this->classKey.'.id');
		return $c;
	}


	/** {@inheritDoc} */
	public function getData() {
		$data = array();
		$limit = intval($this->getProperty('limit'));
		$start = intval($this->getProperty('start'));

		/* query for chunks */
		$c = $this->modx->newQuery($this->classKey);
		$c = $this->prepareQueryBeforeCount($c);
		$data['total'] = $this->modx->getCount($this->classKey,$c);
		$c = $this->prepareQueryAfterCount($c);

        $c->limit($limit,0);

		if ($c->prepare() && $c->stmt->execute()) {

			$data['results'] = $c->stmt->fetchAll(PDO::FETCH_ASSOC);
		}

		/*
		if ($query = $this->getProperty('query',null)) {
				$query1=$this->str2url($query);			
				$q = $this->modx->newQuery('modTemplateVarResource');
							
					$q->select('*');
					$q->where(array('tmplvarid' => 1,'value:LIKE'=>'%"artpost":"'.$query1.'"%'));
					
					if ($q->prepare() && $q->stmt->execute()) {
						$result1 = $q->stmt->fetchAll(PDO::FETCH_ASSOC);	

						$ids=array();
						foreach ($result1 as $id) $ids[]=$id['modTemplateVarResource_contentid'];
						
						$d = $this->modx->newQuery($this->classKey);
						$d->where(array('class_key' => 'msProduct'));
						$d->leftJoin('msProductData','Data', 'msProduct.id = Data.id');
						$d->leftJoin('msCategoryMember','Member', 'msProduct.id = Member.product_id');
						$d->leftJoin('msVendor','Vendor', 'Data.vendor = Vendor.id');
						$d->leftJoin('msCategory','Category', 'Category.id = msProduct.parent');
						$c->leftJoin('modTemplateVarResource','Option', 'msProduct.id = Option.contentid');
						if ($this->getProperty('combo')) {
							$d->select('msProduct.id,msProduct.pagetitle,msProduct.context_key,Option.value');
						}
						else {
							$d->select($this->modx->getSelectColumns('msProduct','msProduct'));
							$d->select($this->modx->getSelectColumns('msProductData','Data', '', array('id'), true));
							$d->select($this->modx->getSelectColumns('msVendor','Vendor', 'vendor_', array('name')));
							$d->select($this->modx->getSelectColumns('msCategory','Category', 'category_', array('pagetitle')));							
						}
						
						$queryWhere = array(
							'msProduct.id:in' => $ids,
							'Option.tmplvarid' => 1
						);
						$d->where($queryWhere);
						$d->prepare();					
					$d->stmt->execute();
						$sql = $d->toSQL();
						
							$result2 = $d->stmt->fetchAll(PDO::FETCH_ASSOC);
						
							$data['total']=(int)$data['total']+count($result2);
						
							$data['results']=array_merge($data['results'],$result2);
						
					}
		}

		$result=array();$resultnew=array();
		foreach($data['results'] as $res)
		{
			$value=$res['value'];
			$id=$res['id'];
			$title=$res['pagetitle'];
			$value_array=$this->modx->fromJSON($value);
			foreach($value_array as $opt){
				$res['option']=$opt['weight'].' '.$opt['weight_prefix'];
				$title1=$title.', '.$res['option'];
				$id1=$id.'_'.$opt['MIGX_id'];
				$res['id']=$id1;
				$res['id_option']=$id;
				$res['option']=$opt['MIGX_id'];
				$res['pagetitle']=$title1;
				unset($res['value']);
				$resultnew[]=$res;
			}
			
		}*/
        $resultnew=[];
        foreach($data['results'] as $res)
        {
            $title=$res['msNpCities_DescriptionRu'].', '.$res['msNpCities_AreaDescriptionRu'];
            $res_new=[];
            $res_new['title']=$title;
            $res_new['id']=$res['msNpCities_id'];
            $resultnew[]=$res_new;
        }
		$data['results']=$resultnew;
		/*	echo '<pre>';
		print_r($data);//$resultnew);
		echo '</pre>';*/
			
		

		return $data;
	}


	/** {@inheritDoc} */
	public function iterate(array $data) {
		$list = array();
		$list = $this->beforeIteration($list);
		$this->currentIndex = 0;
		/** @var xPDOObject|modAccessibleObject $object */
		foreach ($data['results'] as $array) {
			$list[] = $this->prepareArray($array);
			$this->currentIndex++;
		}
		
		$list = $this->afterIteration($list);
		
		return $list;
	}


	/** {@inheritDoc} */
	public function prepareArray(array $resourceArray) {
		if ($this->getProperty('combo')) {
			/*$resourceArray['parents'] = array();
			$parents = $this->modx->getParentIds($resourceArray['id'], 2, array('context' => $resourceArray['context_key']));
			if (empty($parents[count($parents) - 1])) {
				unset($parents[count($parents) - 1]);
			}
			if (!empty($parents) && is_array($parents)) {
				$q = $this->modx->newQuery('msCategory', array('id:IN' => $parents));
				$q->select('id,pagetitle');
				if ($q->prepare() && $q->stmt->execute()) {
					while ($row = $q->stmt->fetch(PDO::FETCH_ASSOC)) {
						$key = array_search($row['id'], $parents);
						if ($key !== false) {
							$parents[$key] = $row;
						}
					}
				}
				$resourceArray['parents'] = array_reverse($parents);
			}*/
		}
		/*else {
			if ($resourceArray['parent'] != $this->parent) {
				$resourceArray['cls'] = 'multicategory';
				$resourceArray['category_name'] = $resourceArray['category_pagetitle'];
			}
			else {
				$resourceArray['cls'] = $resourceArray['category_name'] = '';
			}

			$resourceArray['price'] = round($resourceArray['price'],2);
			$resourceArray['old_price'] = round($resourceArray['old_price'],2);
			$resourceArray['weight'] = round($resourceArray['weight'],3);

			$this->modx->getContext($resourceArray['context_key']);
			$resourceArray['preview_url'] = $this->modx->makeUrl($resourceArray['id'],$resourceArray['context_key']);

			$resourceArray['actions'] = array();

			$resourceArray['actions'][] = array(
				'className' => 'edit',
				'text' => $this->modx->lexicon('ms2_product_edit'),
			);

			$resourceArray['actions'][] = array(
				'className' => 'view',
				'text' => $this->modx->lexicon('ms2_product_view'),
			);
			if (!empty($resourceArray['deleted'])) {
				$resourceArray['actions'][] = array(
					'className' => 'undelete green',
					'text' => $this->modx->lexicon('ms2_product_undelete'),
				);
			} else {
				$resourceArray['actions'][] = array(
					'className' => 'delete',
					'text' => $this->modx->lexicon('ms2_product_delete'),
				);
			}
			if (!empty($resourceArray['published'])) {
				$resourceArray['actions'][] = array(
					'className' => 'unpublish',
					'text' => $this->modx->lexicon('ms2_product_unpublish'),
				);
			} else {
				$resourceArray['actions'][] = array(
					'className' => 'publish orange',
					'text' => $this->modx->lexicon('ms2_product_publish'),
				);
			}
		}
*/
		return $resourceArray;
	}

}

return 'msNpGetcityProcessor';//msProductGetListProcessor';