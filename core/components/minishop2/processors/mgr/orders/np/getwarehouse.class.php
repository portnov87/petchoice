<?php
//error_reporting(E_ALL | E_STRICT);
//ini_set('display_errors', 1);
//ini_set('error_reporting', E_ALL);

//ini_set('display_startup_errors', 1);
/**
 * Get a list of Products
 *
 * @package minishop2
 * @subpackage processors
 */
class msNpGetwarehouseProcessor extends modObjectGetListProcessor {
	public $classKey = 'msNpWarehouse';
	//public $languageTopics = array('default','minishop2:product');
	public $defaultSortField = 'DescriptionRu';
	public $defaultSortDirection  = 'ASC';
	public $parent = 0;


	/** {@inheritDoc} */
	public function initialize() {
		if (!$this->getProperty('limit')) {$this->setProperty('limit', 20);}

		return parent::initialize();
	}

	/** {@inheritDoc} */
	public function prepareQueryBeforeCount(xPDOQuery $c) {


        $city='';
        $order=$this->modx->getObject('msOrder',$this->getProperty('order_id'));
        if ($order)
        {

            $address=$this->modx->getObject('msOrderAddress',$order->get('address'));

            if ($address) {
                $queryWhere=[];
                $city = trim($address->get('city_id_np'));
                if   ($city==''){

                    $city_name = trim($address->get('city'));//$array['addr_city'];

                    if (strpos($city_name, '-') !== false) {
                        $city_name = trim(substr($city_name, 0, strpos($city_name, '-')));
                    }
                    $where = array(
                        'DescriptionRu:LIKE' => $city_name . '%'
                    );


                    $q = $this->modx->newQuery('msNpCities');
                    $q->where($where);//array('msOrder.status' => 2, 'msOrder.user_id:=' => $user_id));

                    $ids = [];
                    $total = 0;
                    if ($q->prepare() && $q->stmt->execute()) {
                        $cities = $q->stmt->fetchAll(PDO::FETCH_ASSOC);
                        //$street
                        foreach ($cities as $city_ar) {
                            $city=$city_ar['msNpCities_id'];
                        }
                    }
                    /*
                    if (strpos($city_name, '-') !== false) {
                        $city_name = trim(substr($city_name, 0, strpos($city_name, '-')));
                    }
                    $queryWhere = array(
                        'msNpCities.DescriptionRu:LIKE' => $city_name.'%'
                    );*/


                }else {



                }

               // $c->where($queryWhere);
            }


            /*if ($address) {
                $city = $address->get('city_id_np');
            }*/
        }

        $queryWhere = array(
            'msNpWarehouse.CityRef:=' => $city
        );
        $c->where($queryWhere);


		return $c;
	}


	/** {@inheritDoc} */
	public function prepareQueryAfterCount(xPDOQuery $c) {
		$c->groupby($this->classKey.'.Ref');
		return $c;
	}


	/** {@inheritDoc} */
	public function getData() {
		$data = array();
		$limit = intval($this->getProperty('limit'));
		$start = intval($this->getProperty('start'));

        $poshtomat = intval($this->getProperty('poshtomat',false));

		/* query for chunks */
		$c = $this->modx->newQuery($this->classKey);
		$c = $this->prepareQueryBeforeCount($c);
		$data['total'] = $this->modx->getCount($this->classKey,$c);
		$c = $this->prepareQueryAfterCount($c);
        //$c->select('*');
//        if ($poshtomat > 0){
//            $c->where("CategoryOfWarehouse='Postomat'");
//        }else{
//            $c->where("CategoryOfWarehouse != 'Postomat'");
//        }
        $c->select('Ref,DescriptionRu,CityRef,Number');
        $c->sortby('cast(Number as unsigned)',$this->getProperty('asc'));

		if ($c->prepare() && $c->stmt->execute()) {
           // $sql = $c->toSQL();

			$data['results'] = $c->stmt->fetchAll(PDO::FETCH_ASSOC);
		}


        $resultnew=[];
        foreach($data['results'] as $res)
        {

            if ((strpos($res['DescriptionRu'],'Почтомат')===false)&&(strpos($res['DescriptionRu'],'Поштомат')===false)) {

                $res_new = [];
                $res_new['title'] = $res['DescriptionRu'];
                $res_new['id'] = $res['Ref'];
                $resultnew[] = $res_new;
            }
        }
		$data['results']=$resultnew;

			
		

		return $data;
	}


	/** {@inheritDoc} */
	public function iterate(array $data) {
		$list = array();
		$list = $this->beforeIteration($list);
		$this->currentIndex = 0;
		/** @var xPDOObject|modAccessibleObject $object */
		foreach ($data['results'] as $array) {
			$list[] = $this->prepareArray($array);
			$this->currentIndex++;
		}
		
		$list = $this->afterIteration($list);
		
		return $list;
	}


	/** {@inheritDoc} */
	public function prepareArray(array $resourceArray) {
		if ($this->getProperty('combo')) {
			/*$resourceArray['parents'] = array();
			$parents = $this->modx->getParentIds($resourceArray['id'], 2, array('context' => $resourceArray['context_key']));
			if (empty($parents[count($parents) - 1])) {
				unset($parents[count($parents) - 1]);
			}
			if (!empty($parents) && is_array($parents)) {
				$q = $this->modx->newQuery('msCategory', array('id:IN' => $parents));
				$q->select('id,pagetitle');
				if ($q->prepare() && $q->stmt->execute()) {
					while ($row = $q->stmt->fetch(PDO::FETCH_ASSOC)) {
						$key = array_search($row['id'], $parents);
						if ($key !== false) {
							$parents[$key] = $row;
						}
					}
				}
				$resourceArray['parents'] = array_reverse($parents);
			}*/
		}
		/*else {
			if ($resourceArray['parent'] != $this->parent) {
				$resourceArray['cls'] = 'multicategory';
				$resourceArray['category_name'] = $resourceArray['category_pagetitle'];
			}
			else {
				$resourceArray['cls'] = $resourceArray['category_name'] = '';
			}

			$resourceArray['price'] = round($resourceArray['price'],2);
			$resourceArray['old_price'] = round($resourceArray['old_price'],2);
			$resourceArray['weight'] = round($resourceArray['weight'],3);

			$this->modx->getContext($resourceArray['context_key']);
			$resourceArray['preview_url'] = $this->modx->makeUrl($resourceArray['id'],$resourceArray['context_key']);

			$resourceArray['actions'] = array();

			$resourceArray['actions'][] = array(
				'className' => 'edit',
				'text' => $this->modx->lexicon('ms2_product_edit'),
			);

			$resourceArray['actions'][] = array(
				'className' => 'view',
				'text' => $this->modx->lexicon('ms2_product_view'),
			);
			if (!empty($resourceArray['deleted'])) {
				$resourceArray['actions'][] = array(
					'className' => 'undelete green',
					'text' => $this->modx->lexicon('ms2_product_undelete'),
				);
			} else {
				$resourceArray['actions'][] = array(
					'className' => 'delete',
					'text' => $this->modx->lexicon('ms2_product_delete'),
				);
			}
			if (!empty($resourceArray['published'])) {
				$resourceArray['actions'][] = array(
					'className' => 'unpublish',
					'text' => $this->modx->lexicon('ms2_product_unpublish'),
				);
			} else {
				$resourceArray['actions'][] = array(
					'className' => 'publish orange',
					'text' => $this->modx->lexicon('ms2_product_publish'),
				);
			}
		}
*/
		return $resourceArray;
	}

}

return 'msNpGetwarehouseProcessor';//msProductGetListProcessor';