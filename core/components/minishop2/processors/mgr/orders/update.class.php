<?php
/*
error_reporting(E_ALL | E_STRICT);
ini_set('display_errors', false);
ini_set('error_reporting', E_ALL);
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);*/
class msOrderUpdateProcessor extends modObjectUpdateProcessor
{
    public $classKey = 'msOrder';
    public $languageTopics = array('minishop2:default');
    public $beforeSaveEvent = 'msOnBeforeUpdateOrder';
    public $afterSaveEvent = 'msOnUpdateOrder';
    public $permission = 'msorder_save';
    protected $status;
    protected $delivery;
    protected $payment;


    /** {@inheritDoc} */
    public function initialize()
    {
        if (!$this->modx->hasPermission($this->permission)) {
            return $this->modx->lexicon('access_denied');
        }
        return parent::initialize();
    }


    /** {@inheritDoc} */
    public function beforeSet()
    {
        foreach (array('status', 'delivery', 'payment', 'date_shipping') as $v) {
            $this->$v = $this->object->get($v);
            if (!$this->getProperty($v)) {
                $this->addFieldError($v, $this->modx->lexicon('ms2_err_ns'));
            }
        }

        if ($status = $this->modx->getObject('msOrderStatus')) {
            if ($status->get('final')) {

                //echo $status->get('final');die();
                return $this->modx->lexicon('ms2_err_status_final');
            }
        }
        $date_shipping1 = explode(' ', str_replace(' 00:00:00', '', $this->getProperty('date_shipping')));
        $date_shipping = $date_shipping1[0];
        $time_from1 = explode(' ', str_replace('1970-01-01 ', '', $this->getProperty('time_shipping_from')));
        $time_from = $time_from1[1];
        $time_to1 = explode(' ', str_replace('1970-01-01 ', '', $this->getProperty('time_shipping_to')));
        $time_to = $time_to1[1];

        if (strpos($time_from, ':00:00') !== false)
            $time_from = str_replace(':00:00', ':00', $time_from);
        else $time_from = str_replace(':00', '', $time_from);

        if (strpos($time_to, ':00:00') !== false)
            $time_to = str_replace(':00:00', ':00', $time_to);
        else $time_to = str_replace(':00', '', $time_to);

        $data_shipping = $date_shipping . ' ' . $time_from . '-' . $time_to;

        if (($this->object->get('date_shipping') == '') && ($this->getProperty('date_shipping') != '')) {
            $user_id = $this->modx->user->id;
            $action = "add_dateshipping";
            $entry = $_GET['date_shipping'];
            $log = $this->modx->newObject('msOrderLog', array(
                'order_id' => $this->object->get('id')
            , 'user_id' => $user_id
            , 'timestamp' => time()
            , 'action' => $action
            , 'message' => $data_shipping//$this->getProperty('date_shipping')//$entry
            , 'entry' => ''
            , 'ip' => $this->modx->request->getClientIp()
            ));

            $log->save();
        } elseif ($this->object->get('date_shipping') != $this->getProperty('date_shipping')) {

            $user_id = $this->modx->user->id;
            $action = "update_dateshipping";
            $entry = $_GET['date_shipping'];

            $log = $this->modx->newObject('msOrderLog', array(
                'order_id' => $this->object->get('id')
            , 'user_id' => $user_id
            , 'timestamp' => time()
            , 'action' => $action
            , 'message' => $data_shipping//$this->getProperty('date_shipping')//$entry
            , 'entry' => ''
            , 'ip' => $this->modx->request->getClientIp()
            ));

            $log->save();
        } elseif (($this->object->get('time_shipping_from') == '') && ($this->getProperty('time_shipping_from') != '')) {
            $user_id = $this->modx->user->id;
            //($action == 'status' && $entry == 1) || !$this->modx->user->id ? $order->get('user_id') :
            $action = "add_dateshipping";
            $entry = $_GET['time_shipping_from'];
            $log = $this->modx->newObject('msOrderLog', array(
                'order_id' => $this->object->get('id')
            , 'user_id' => $user_id
            , 'timestamp' => time()
            , 'action' => $action
            , 'message' => $data_shipping//$this->getProperty('time_shipping_from')//$entry
            , 'entry' => ''
            , 'ip' => $this->modx->request->getClientIp()
            ));

            $log->save();
        } elseif ($this->object->get('time_shipping_from') != $this->getProperty('time_shipping_from')) {

            $user_id = $this->modx->user->id;
            $action = "update_dateshipping";
            $entry = $_GET['time_shipping_from'];

            $log = $this->modx->newObject('msOrderLog', array(
                'order_id' => $this->object->get('id')
            , 'user_id' => $user_id
            , 'timestamp' => time()
            , 'action' => $action
            , 'message' => $data_shipping//$this->getProperty('time_shipping_from')//$entry
            , 'entry' => ''
            , 'ip' => $this->modx->request->getClientIp()
            ));

            $log->save();
        } elseif (($this->object->get('time_shipping_to') == '') && ($this->getProperty('time_shipping_to') != '')) {
            $user_id = $this->modx->user->id;
            //($action == 'status' && $entry == 1) || !$this->modx->user->id ? $order->get('user_id') :
            $action = "add_dateshipping";
            $entry = $_GET['time_shipping_to'];
            $log = $this->modx->newObject('msOrderLog', array(
                'order_id' => $this->object->get('id')
            , 'user_id' => $user_id
            , 'timestamp' => time()
            , 'action' => $action
            , 'message' => $data_shipping//$this->getProperty('time_shipping_to')//$entry
            , 'entry' => ''
            , 'ip' => $this->modx->request->getClientIp()
            ));

            $log->save();
        } elseif ($this->object->get('time_shipping_to') != $this->getProperty('time_shipping_to')) {

            $user_id = $this->modx->user->id;
            $action = "update_dateshipping";
            $entry = $_GET['time_shipping_to'];

            $log = $this->modx->newObject('msOrderLog', array(
                'order_id' => $this->object->get('id')
            , 'user_id' => $user_id
            , 'timestamp' => time()
            , 'action' => $action
            , 'message' => $data_shipping//$this->getProperty('time_shipping_to')//$entry
            , 'entry' => ''
            , 'ip' => $this->modx->request->getClientIp()
            ));

            $log->save();
        }

        //$address = $this->object->getOne('Address');
        /*echo '<pre>';
        print_r($address);//$this->object->getData());//$this->modx->toArray($this->object->getData()));
        echo '</pre>';*/
        //else
        if (($this->object->get('comment') == '') && ($this->getProperty('comment') != '')) {

            $user_id = $this->modx->user->id;
            $action = "add_comment";
            $entry = $_GET['comment'];
            $log = $this->modx->newObject('msOrderLog', array(
                'order_id' => $this->object->get('id')
            , 'user_id' => $user_id
            , 'timestamp' => time()
            , 'action' => $action
            , 'message' => $this->getProperty('comment')//$entry
            , 'entry' => ''
            , 'ip' => $this->modx->request->getClientIp()
            ));

            $log->save();
        } elseif ($this->object->get('comment') != $this->getProperty('comment')) {

            $user_id = $this->modx->user->id;
            $action = "update_comment";
            $entry = $_GET['comment'];
            $log = $this->modx->newObject('msOrderLog', array(
                'order_id' => $this->object->get('id')
            , 'user_id' => $user_id
            , 'timestamp' => time()
            , 'action' => $action
            , 'message' => $this->getProperty('comment')//$entry
            , 'entry' => ''
            , 'ip' => $this->modx->request->getClientIp()
            ));

            $log->save();
        }

        return parent::beforeSet();
    }


    /** {@inheritDoc} */
    public function beforeSave()
    {

        if ($this->object->get('status') != $this->status) {
            $change_status = $this->modx->miniShop2->changeOrderStatus($this->object->get('id'), $this->object->get('status'), $this->object->get('bonuses'));
            if ($change_status !== true) {
                return $this->object->get('status') . ' ' . $change_status;
            }
        }

        $date_shipping = $this->getProperty('date_shipping');
        $date_shipping = trim(str_replace(' 00:00:00', '', $date_shipping));
        $this->object->set('date_shipping', $date_shipping);

        $time_shipping_from = $this->getProperty('time_shipping_from');
        $time_shipping_from = trim(str_replace('1970-01-01 ', '', $time_shipping_from));
        $this->object->set('time_shipping_from', $time_shipping_from);

        $time_shipping_to = $this->getProperty('time_shipping_to');
        $time_shipping_to = trim(str_replace('1970-01-01', '', $time_shipping_to));
        $this->object->set('time_shipping_to', $time_shipping_to);

        $cost_delivery = (float)$this->getProperty('delivery_cost');
        $this->object->set('delivery_cost', $cost_delivery);




        $bonuses = (float)$this->getProperty('bonuses');
        if (($bonuses>0)&&($this->object->get('apply_bonuses')!='1')) {
            $this->object->set('bonuses', $bonuses);
            $this->object->set('with_bonuses', 1);
            if ($this->getProperty('status')==5)
            $this->object->set('apply_bonuses', 1);
           // $cost = $this->object->get('cart_cost') + $cost_delivery - $bonuses;
          //  $cart_cost=$this->object->get('cart_cost') - $bonuses;
        }else{
            //$cost = $this->object->get('cart_cost') + $cost_delivery;
            //$cart_cost=$this->object->get('cart_cost');
        }


        $this->object->set('updatedon', time());


        //echo '$cost '.$cost.' '.$this->object->get('cart_cost').' ghghg ';
        //die();

        return parent::beforeSave();
    }


    /** {@inheritDoc} */
    public function afterSave()
    {

        if ($address = $this->object->getOne('Address')) {
            $old_comment=$address->get('comment');
            foreach ($this->getProperties() as $k => $v) {
                if ((strpos($k, 'email') === false) && (strpos($k, 'addr_') !== false)) {
                    $k_key=substr($k, 5);
                    //$address->get('comment').' '.
                    //echo $k_key.' '.$v."\r\n";
                    if ($k_key=='comment') {
                        if (($old_comment =='')&&($v!='')) {

                            $user_id = $this->modx->user->id;
                            $action = "add_comment_client";
                            $entry = $v;
                            $log = $this->modx->newObject('msOrderLog', array(
                                'order_id' => $this->object->get('id')
                            , 'user_id' => $user_id
                            , 'timestamp' => time()
                            , 'action' => $action
                            , 'message' => $v//$this->getProperty('comment')//$entry
                            , 'entry' => ''
                            , 'ip' => $this->modx->request->getClientIp()
                            ));

                            $log->save();
                        }
                        elseif ($old_comment !=$v) {

                            $user_id = $this->modx->user->id;
                            $action = "update_comment_client";
                            $entry = $v;
                            $log = $this->modx->newObject('msOrderLog', array(
                                'order_id' => $this->object->get('id')
                            , 'user_id' => $user_id
                            , 'timestamp' => time()
                            , 'action' => $action
                            , 'message' => $v//$this->getProperty('comment')//$entry
                            , 'entry' => ''
                            , 'ip' => $this->modx->request->getClientIp()
                            ));

                            $log->save();
                        }
                    }

                    $address->set($k_key, $v);

                } elseif ($k == 'addr_email') {
                    $user = $this->modx->getObject('modUserProfile', array('internalKey' => $this->object->get('user_id')));
                    if ($user) {
                        $user->set('email', $v);
                        $user->save();
                    }
                }
            }
            $address->save();
        }
        $status = $this->getProperty('status');
        $this->object->updateProducts($status);

        $this->object->save();
    }

}

return 'msOrderUpdateProcessor';