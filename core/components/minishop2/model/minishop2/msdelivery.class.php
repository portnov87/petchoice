<?php
//
//define('MODX_API_MODE', true);
//error_reporting(E_ALL | E_STRICT);
//ini_set('display_errors', 1);

class msDelivery extends xPDOSimpleObject
{

    var $cityOdessa;
    var $cityKiev;


    /* @var msDeliveryHandler $handler */
    var $handler;
    /* @var miniShop2 $ms2 */
    var $ms2;

    /**
     * Loads delivery handler class
     *
     * @return bool
     */
    public function loadHandler()
    {
        require_once dirname(__FILE__) . '/msdeliveryhandler.class.php';

        if (!$class = $this->get('class')) {
            $class = 'msDeliveryHandler';
        }
        $this->cityOdessa = 'db5c88d0-391c-11dd-90d9-001a92567626';
        $this->cityKiev = '8d5a980d-391c-11dd-90d9-001a92567626';

        if ($class != 'msDeliveryHandler') {
            if (!is_object($this->ms2) || !($this->ms2 instanceof miniShop2)) {
                require_once dirname(__FILE__) . '/minishop2.class.php';
                $this->ms2 = new miniShop2($this->xpdo, array());
            }
            $this->ms2->loadCustomClasses('delivery');
        }

        if (!class_exists($class)) {
            $this->xpdo->log(modX::LOG_LEVEL_ERROR, 'Delivery handler class "' . $class . '" not found.');
            $class = 'msDeliveryHandler';
        }

        $this->handler = new $class($this, array());
        if (!($this->handler instanceof msDeliveryInterface)) {
            $this->xpdo->log(modX::LOG_LEVEL_ERROR, 'Could not initialize delivery handler class: "' . $class . '"');
            return false;
        }

        return true;
    }

    /**
     * Returns an additional cost depending on the method of delivery
     *
     * @param msOrderInterface|msOrderHandler $order
     * @param float $cost Current cost of order
     *
     * @return float|integer
     */
    public function getCost(msOrderInterface $order, $cost = 0)
    {
        if (is_object($order->ms2) && $order->ms2 instanceof miniShop2) {
            $this->ms2 = &$order->ms2;
        }
        if (!is_object($this->handler) || !($this->handler instanceof msDeliveryInterface)) {
            if (!$this->loadHandler()) {
                return 0;
            }
        }
        return $this->handler->getCost($order, $this, $cost);
    }

    /**
     * Returns an additional cost depending on the method of delivery
     *
     * @param msOrderInterface|msOrderHandler $order
     * @param float $cost Current cost of order
     *
     * @return float|integer
     */
    public function isAvailableForCity($city_id = 0)
    {
        if (!is_object($this->handler) || !($this->handler instanceof msDeliveryInterface)) {
            if (!$this->loadHandler()) {
                return 0;
            }
        }
        return $this->handler->isAvailableForCity($city_id);//false);
    }

    /**
     * Returns an additional cost depending on the method of delivery
     *
     * @param msOrderInterface|msOrderHandler $order
     * @param float $cost Current cost of order
     *
     * @return float|integer
     */
    public function getWarehouses($city_id, $poshtomat = false)
    {
        if (!is_object($this->handler) || !($this->handler instanceof msDeliveryInterface)) {
            if (!$this->loadHandler()) {
                return 0;
            }
        }
        return $this->handler->getWarehouses($city_id, $poshtomat);
    }

    public function getStreet($city_id, $street = '')
    {
        if (!is_object($this->handler) || !($this->handler instanceof msDeliveryInterface)) {
            if (!$this->loadHandler()) {
                return 0;
            }
        }
        return $this->handler->getStreet($city_id, $street);
    }

    public function getCity($city_id)
    {
        if (!is_object($this->handler) || !($this->handler instanceof msDeliveryInterface)) {
            if (!$this->loadHandler()) {
                return 0;
            }
        }
        return $this->handler->getCity($city_id);
    }

    /**
     * Returns id of first active payment method for this delivery
     *
     * @return int|mixed
     */
    public function getFirstPayment()
    {
        $id = 0;
        $q = $this->xpdo->newQuery('msPayment');
        $q->leftJoin('msDeliveryMember', 'Member', '`msPayment`.`id` = `Member`.`payment_id`');
        $q->leftJoin('msDelivery', 'Delivery', '`Member`.`delivery_id` = `Delivery`.`id`');
        $q->sortby('`msPayment`.`rank`', 'ASC');
        $q->select('`msPayment`.`id`');
        $q->where(array('msPayment.active' => 1, 'Delivery.id' => $this->get('id')));
        $q->limit(1);
        if ($q->prepare() && $q->stmt->execute()) {
            $id = $q->stmt->fetch(PDO::FETCH_COLUMN);
        }
        return $id;
    }

    /** {@inheritdoc} */
    public function remove(array $ancestors = array())
    {
        $id = $this->get('id');
        $table = $this->xpdo->getTableName('msDeliveryMember');
        $sql = "DELETE FROM {$table} WHERE `delivery_id` = '$id';";
        $this->xpdo->exec($sql);

        return parent::remove();
    }

    // by portnov
    public function getcostdelivery($id, $cityidorder, $total_order, $platnay = 0)

    {
        switch ($id) {
            case '1':// самовывоз
                $free = 0;
                $infodelivery = "Самовывоз";
                $cost_delivery = 'Бесплатно';
                break;
            case '5':

                if ($total_order > $platnay) {

                    $freecost = $total_order - $platnay;

                    $free = $this->xpdo->getOption('free_delivery_5');
                    $infodelivery = "Доставка на отделение «Новая Почта»";
                    $descdelivery = "Согласно тарифам перевозчика";
                    $cost_delivery = '';

                    if ($freecost >= $free) {

                        $nofree = $this->xpdo->getOption('nofree');
                        $infodelivery = $nofree;
                        $descdelivery = "Бесплатно";

                    }
                } elseif ($total_order == $platnay) { // начит в корзине есть только товары кот платные для доставки

                    $free = $this->xpdo->getOption('free_delivery_5');
                    $infodelivery = "Доставка на отделение «Новая Почта»";
                    $descdelivery = "Согласно тарифам перевозчика";
                    $cost_delivery = '';
                    $cost_delivery = '';
                    $nofree = $this->xpdo->getOption('nofree');
                    $infodelivery = $nofree;
                } else {

                }

                break;
            case '2':

                if ($total_order > $platnay) {

                    $freecost = $total_order - $platnay;

                    $free = $this->xpdo->getOption('free_delivery_2');
                    $infodelivery = "Доставка на отделение «Новая Почта»";
                    $descdelivery = "Согласно тарифам перевозчика";
                    $cost_delivery = '';

                    if ($freecost >= $free) {
                        /*$cost_delivery='0';
                    }elseif ($platnay!=0)//(($freecost!=0)
                    {*/
                        $nofree = $this->xpdo->getOption('nofree');
                        $infodelivery = $nofree;
                        $descdelivery = "Бесплатно";
                        //$total_order.' '.$platnay.
                    }
                    //}else {
                    //echo $total_order.' '.$platnay;

                } elseif ($total_order == $platnay) { // начит в корзине есть только товары кот платные для доставки

                    $free = $this->xpdo->getOption('free_delivery_2');
                    $infodelivery = "Доставка на отделение «Новая Почта»";
                    $descdelivery = "Согласно тарифам перевозчика";
                    $cost_delivery = '';
                    $cost_delivery = '';
                    $nofree = $this->xpdo->getOption('nofree');
                    $infodelivery = $nofree;
                } else {

                }


                break;
            case '3'://интайм

                if ($total_order > $platnay) {

                    $freecost = $total_order - $platnay;

                    $free = $this->xpdo->getOption('free_delivery_3');
                    $infodelivery = "Доставка на отделение «Интайм»";
                    $descdelivery = "Согласно тарифам перевозчика";
                    $cost_delivery = '';
                    if ($freecost >= $free) {
                        $nofree = $this->xpdo->getOption('nofree');
                        $infodelivery = $nofree;
                        $descdelivery = "Бесплатно";
                    }

                } elseif ($total_order == $platnay) { // начит в корзине есть только товары кот платные для доставки

                    $free = $this->xpdo->getOption('free_delivery_3');
                    $infodelivery = "Доставка на отделение «Интайм»";
                    $descdelivery = "Согласно тарифам перевозчика";
                    $cost_delivery = '';
                    $cost_delivery = '';
                    $nofree = $this->xpdo->getOption('nofree');
                    $infodelivery = $nofree;
                } else {

                }

                break;
            case '4'://курьерская доставка
                $free = $this->xpdo->getOption('free_delivery_Odessa');
                $infodelivery = "Доставка до двери";
                $total_order = (float)$total_order;
                if ($cityidorder == $this->cityOdessa) // елси Одесса ylfj epyfnm ghbujhjl
                {
                    $cost_delivery = $this->xpdo->getOption('delivery_odessa_kurier');
                } else {
                    // елси пригород одессы
                    $free = (int)$this->xpdo->getOption('free_delivery_region');
                    $cost_delivery = 0;
                    $qcity = $this->xpdo->getObject('msLocalities', $cityidorder);
                    if ($qcity) {
                        $region = $qcity->get('region');
                        //echo ' $region'.$region;Одесская
                        if ($region == 'Одесская')
                            $cost_delivery = $this->xpdo->getOption('delivery_odessa_prigorod_kurier');// 50;

                    } else {
                        $qcity = $this->xpdo->getObject('msNpCities', $cityidorder);

                        if ($qcity) {
                            $region = $qcity->get('AreaDescriptionRu');
                            //echo ' $region'.$region;Одесская
                            if ($region == 'Одеська')
                                $cost_delivery = $this->xpdo->getOption('delivery_odessa_prigorod_kurier');// 50;
                        }
                    }
                }
                //echo $total_order.' order '.$free;
                if ($total_order >= $free) $cost_delivery = 'Бесплатно';
                break;

            case '6'://почтомат
                // $free= $this->xpdo->getOption('free_delivery_Odessa');
                $infodelivery = "Почтомат";
                $total_order = (float)$total_order;

                $cost_delivery = 0;


                if ($total_order > $platnay) {

                    $freecost = $total_order - $platnay;

                    $free = $this->xpdo->getOption('free_delivery_6');
                    $infodelivery = "Доставка на почтомат «Новапошта»";
                    $descdelivery = "Согласно тарифам перевозчика";
                    $cost_delivery = '';
                    if ($freecost >= $free) {
                        $nofree = $this->xpdo->getOption('nofree');
                        $infodelivery = $nofree;
                        $descdelivery = "Бесплатно";
                    }

                } elseif ($total_order == $platnay) { // начит в корзине есть только товары кот платные для доставки

                    $free = $this->xpdo->getOption('free_delivery_6');
                    $infodelivery = "Доставка на почтомат «Новапошта»";
                    $descdelivery = "Согласно тарифам перевозчика";
                    $cost_delivery = '';
                    $cost_delivery = '';
                    $nofree = $this->xpdo->getOption('nofree');
                    $infodelivery = $nofree;
                } else {

                }


                /*$this->xpdo->getOption('delivery_odessa_poshtomat');
                if ($cityidorder==$this->cityOdessa)
                {
                    $cost_delivery=$this->xpdo->getOption('delivery_odessa_poshtomat');
                }else{// елси пригород одессы
                    $free= (int)$this->xpdo->getOption('free_delivery_region');
                    $cost_delivery=0;
                    //echo '$cityidorder'.$cityidorder;
                    $qcity =  $this->xpdo->getObject('msLocalities',$cityidorder);
                    if ($qcity)
                    {
                        $region=$qcity->get('region');
                        //echo ' $region'.$region;Одесская
                        if ($region=='Одесская')
                            $cost_delivery=$this->xpdo->getOption('delivery_odessa_prigorod_kurier');// 50;

                    }else{
                        $qcity =  $this->xpdo->getObject('msNpCities',$cityidorder);
                        if ($qcity)
                        {
                            $region=$qcity->get('AreaDescriptionRu');
                            //echo ' $region'.$region;Одесская
                            if ($region=='Одеська')
                                $cost_delivery=$this->xpdo->getOption('delivery_odessa_prigorod_kurier');// 50;

                        }
                    }
                }
                //echo $total_order.' order '.$free;
                if ($total_order>=$free) $cost_delivery='Бесплатно';*/


                break;

        }
        $free = (float)$free;
//echo '$free'.$free;


        return array('cost_delivery' => $cost_delivery, 'infodelivery' => $infodelivery, 'descdelivery' => $descdelivery);
    }

}