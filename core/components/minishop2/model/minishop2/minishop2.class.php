<?php

if (!function_exists('mb_ucfirst')) {
    function mb_ucfirst($string, $enc = 'UTF-8')
    {
        return mb_strtoupper(mb_substr($string, 0, 1, $enc), $enc) .
            mb_substr($string, 1, mb_strlen($string, $enc), $enc);
    }
}

/**
 * The base class for miniShop2.
 *
 * @package minishop2
 */
class miniShop2
{
    public $cityOdessa = 'db5c88d0-391c-11dd-90d9-001a92567626';
    public $cityKiev = '8d5a980d-391c-11dd-90d9-001a92567626';

    public $path1c = '/assets/1c/';
    /** @var modX $modx */
    public $modx;
    /** @var msCartHandler $cart */
    public $cart;
    /** @var msOrderHandler $order */
    public $order;

    /** @var array $initialized */
    public $initialized = array();

    public function resetpurchase()
    {
        $q = $this->modx->newQuery('modUser');

        $day21 = 86400 * 21;
        $daydate = date("Y-m-d", time() - $day21);

        $daydatebegin = $daydate . ' 00:00:00';
        $daydateend = $daydate . ' 23:59:59';

        $where = array(
            'purchasedate:<=' => $daydateend,
            'purchaseReminder:=' => 1,
        );
        $q->command('update');
        $q->set(array(
            'purchaseReminder' => 0,
            'purchasedate' => '00-00-00 00:00:00',
        ));
        $q->where($where);

        $q->prepare();
        if ($q->stmt->execute()) return true;
        else return false;

    }

    public function getpurchase()
    {
        $q = $this->modx->newQuery('modUser');
        $where = array(
            'modUser.purchaseReminder:=' => 0
        );

        $q->select(array(
            'modUserProfile.email',
            'modUserProfile.phone',
            'modUser.created',
            'modUser.submitofferup',
            'modUser.registr',
            'modUserProfile.fullname',
            'modUser.id'
        ));

        $q->innerJoin('modUserProfile', 'modUserProfile', 'modUser.id=modUserProfile.internalKey');

        $q->sortby('modUser.created', 'ASC');
        $q->groupby('modUserProfile.email');//msOrder.user_id,
        $q->where($where);
        $q->prepare();
        $q->stmt->execute();
        $result = $q->stmt->fetchAll(PDO::FETCH_ASSOC);

        foreach ($result as $res) {


            $date_letter_to_manager = false;
            $q = $this->modx->newQuery('msOrder');
            $where = array(
                'msOrder.user_id:=' => $res['id'],
            );

            $q->select(array(
                'msOrder.id',
                'msOrder.createdon',
                /* 'msOrderProduct.product_id',
                 'msOrderProduct.name',
                                              'msOrderProduct.options',
                 'msOrderProduct.count',
                 'msOrderProduct.price',
                 'msOrderProduct.cost',
                 'msOrderProduct.cost_old',
                 'msOrderProduct.price_old'*/
            ));

            //$q->innerJoin('msOrderProduct', 'msOrderProduct', 'msOrder.id=msOrderProduct.order_id');
            $q->sortby('msOrder.createdon', 'DESC');
            $q->limit(2);
            $q->where($where);
            $q->prepare();
            $q->stmt->execute();
            $orders = $q->stmt->fetchAll(PDO::FETCH_ASSOC);
            $last = false;
            $penultimate = false;


            foreach ($orders as $order) {

                $q = $this->modx->newQuery('msOrderProduct');
                $where = array(
                    'msOrderProduct.order_id:=' => $order['id'],
                    'msProduct.parent:IN' => array(15, 19, 57, 59) // только корма
                );

                $q->select(array(
                    'msOrder.createdon',
                    'msOrderProduct.product_id',
                    'msOrderProduct.name',
                    'msOrderProduct.options',
                    'msOrderProduct.count',
                    'msOrderProduct.price',
                    'msOrderProduct.cost',
                    'msOrderProduct.cost_old',
                    'msOrderProduct.price_old',
                    'msOrderProduct.order_id',
                    'msOrderProduct.id_1c'
                ));
                $q->innerJoin('msOrder', 'msOrder', 'msOrderProduct.order_id=msOrder.id');
                $q->innerJoin('msProduct', 'msProduct', 'msOrderProduct.product_id=msProduct.id');
                $q->where($where);
                $q->prepare();
                $q->stmt->execute();
                $order_products = $q->stmt->fetchAll(PDO::FETCH_ASSOC);

                if (count($order_products) > 0) {
                    if (!$last) $last = $order_products; //$order['id']
                    elseif (($last) && (!$penultimate)) $penultimate = $order_products;//[$order['id']]
                }
            }


            foreach ($last as $l_p) {
                $difference = false;
                foreach ($penultimate as $p_p) {
                    if ($l_p['product_id'] == $p_p['product_id']) {

                        $difference = strtotime($l_p['createdon']) - strtotime($p_p['createdon']);
                        $difference = round($difference / (3600 * 24), 0);
                        //echo 'difference' . $difference . '<br/><br/>';
                        $date_letter_to_manager = $this->counting_rate($difference, $l_p, $p_p);
                        if ($date_letter_to_manager) {
                            // вычислим разницу между последним заказов и сегодняшней датой
                            $difference = time() - strtotime($l_p['createdon']);
                            $days = round($difference / (3600 * 24), 0);//date('Y-m-d',$difference);
                            //echo 'days' . $days . ' ' . $date_letter_to_manager . '<br/>';
                            if ($days == $date_letter_to_manager) {
                                $this->sendmailremindmanager($res, $l_p, $res['id']);
                            }
                        }

                    }
                }
            }


            //echo '<br/><br/>';
        }

    }

    public function sendmailremindmanager($date_letter_to_manager, $order, $user_id)
    {
        // send mail about review shop
        $linkprofile = 'https://petchoice.ua/backend/?a=security/user/update&id=' . $user_id;
        //[product_id] => 2800
        $user_model = $this->modx->getObject('modUser', $user_id);
        $fullname = '';
        $email = $phone = '';
        $discount = 0;
        if ($user_model) {
            $profile = $user_model->getOne('Profile');
            $extended = $profile->get('extended');

            $fullname = $profile->get('fullname');
            $email = $profile->get('email');
            if (isset($extended['discount']))
                $discount = $extended['discount'];

            $phone = $profile->get('phone');
        }


        $link = $this->modx->makeUrl($order['product_id'], '', '', 'full');
        $title_product = $order['name'];


        $size = $order['options'];
        $size = $this->modx->fromJson($size);

        $optionOrder = $size['size'];
        $option_id = $order['id_1c'];
        $product_model = $this->modx->getObject('msProduct', $order['product_id']);
        $image = $product_model->get('image');

        $_where = array(
            'contentid' => $order['product_id'],
            'tmplvarid' => 1
        );
        $tvOptions = $this->modx->getObject('modTemplateVarResource', $_where);
        $price = $order['cost'];
        if ($tvOptions) {
            $_options = json_decode($tvOptions->get('value'), true);

            foreach ($_options as $_op) {
                if (!empty($option_id) && ($option_id == $_op['id_1c'])) {
                    $price = $_op['price'];
                } elseif ($size['size'] == $_op['weight'] . ' ' . $_op['weight_prefix']) {
                    $price = $_op['price'];
                }
            }
        }


        $good = '	<tr bgcolor="#F9FBFD">
                        <td title="photo" bgcolor="#ffffff"><!-- PHOTO  -->

                                <a href="' . $link . '?utm_source=site-newsletter&utm_medium=email&utm_campaign=purchase-reminder" target="_blank" title="купить">'
            . '<img width="90" src="https://petchoice.ua' . $image . '" /></a>
                        </td>
                        <td bgcolor="#ffffff"><img src="https://petchoice.ua/assets/mail/imgs/sp.gif" width="20" /></td><td><img src="https://petchoice.ua/assets/mail/imgs/sp.gif" width="20" /></td>
                        <td title="name" width="100%"><!-- NAME  -->
                                ' . $title_product . ', ' . $size['size'] . ' (' . $order['count'] . ' шт.)
                        </td>
                        <td><img src="https://petchoice.ua/assets/mail/imgs/sp.gif" width="20" /></td>
                        <td title="price"><!-- PRICE  cost old_price-->                	
                                <font size="5"><b>' . $price . '</b></font><font size="2">грн</font>
                        </td>
                        <td><img src="https://petchoice.ua/assets/mail/imgs/sp.gif" width="20" /></td>
                        <td title="button">
                        <a href="' . $link . '?utm_source=site-newsletter&utm_medium=email&utm_campaign=purchase-reminder" target="_blank" title="купить">
                            <img src="https://petchoice.ua/assets/mail/imgs/btn_see.png" height="50">
                            </a></td><!-- SEE BTN  -->
                        <td><img src="https://petchoice.ua/assets/mail/imgs/sp.gif" width="20" /></td>
                    </tr>

                <tr><td colspan="8"><img src="https://petchoice.ua/assets/mail/imgs/sp.gif" width="100%" height="10" /></td></tr> ';
        $chunk = 'purchaseReminder';
        $body = $this->modx->getChunk($chunk, array(
            'linkprofile' => $linkprofile,
            'good' => $good,
            'fullname' => $fullname));

        $chunk_manager = 'purchaseReminder_manager';
        $body_manager = 'Почта: ' . $email . ' Шаблон: </br></br>' . $this->modx->getChunk($chunk_manager, array('linkprofile' => $linkprofile,
                'good' => $good,
                'fullname' => $fullname));
        $subject = 'Напоминание о покупке корма';

        if ($phone != '') {
            $data_product = $product_model->getOne('Data');
            $vendor = $data_product->getOne('Vendor');
            $vendor_name = $vendor->get('name');

            $dataNotification = [
                'vendor' => $vendor_name,
                'fullname' => $fullname,
                'linkprofile' => $linkprofile,
                'good' => $good,
                'option' => $optionOrder,
                'price' => $price,
                'discount' => $discount,
                'phone' => $user_model->get('username')
            ];

            $resultevent = $this->eventNotification(5, $user_model, $dataNotification, [
                'product' => $product_model,
                'link' => $link
            ]);

            echo '<pre>resultevent';
            print_r($resultevent);
            echo '</pre>';

        }
        echo 'submit';
    }

    public function counting_rate($difference, $order_last, $order_penultimate)
    {
        $date_letter_to_manager = false;
        //$rate=
        $options = $this->modx->fromJson($order_penultimate['options']);
        $size = $options['size'];
        $size_ex = explode(' ', $size);
        $size_ex[0] = str_replace(',', '.', $size_ex[0]);

        $options_last = $this->modx->fromJson($order_last['options']);
        $size_last = $options_last['size'];
        $size_last_ex = explode(' ', $size_last);
        //$size_last_ex
        $size_last_ex[0] = str_replace(',', '.', $size_last_ex[0]);
        $size_last_ex[0] = (float)$size_last_ex[0];

        //echo (float)$size_ex[0]. ' '.$size_ex[1].'<br/>';
        if (($size_ex[0] >= 1) && ($size_ex[1] == 'кг') && ($size_last_ex[1] == 'кг')) {
            $ratetoday = floor(((float)$size_ex[0] * 1000) / $difference);
            echo '$ratetoday' . $ratetoday . '<br/>';


            if (($size_last_ex[0] >= 1) && ($size_last_ex[1] == 'кг')) {
                $date_letter_to_manager = floor($size_last_ex[0] * 1000 / $ratetoday);
            } else
                $date_letter_to_manager = floor($size_last_ex[0] / $ratetoday);

            $date_letter_to_manager = $date_letter_to_manager - 3;// через столько дней отправить письмо менеджеру
            //$date_letter_to_manager=
            echo '<br/>$date_letter_to_manager' . $date_letter_to_manager . '<br/>';
        }
        return $date_letter_to_manager;
    }


    public function getusersFirstOrder()
    {

        //echo floor(($today - $moon) / 86400)." дней назад.";
        //$q = $this->modx->newQuery('modUser');
        $q = $this->modx->newQuery('modUser');

        //ROUND((UNIX_TIMESTAMP()-UNIX_TIMESTAMP('date_pay'))/86400) AS days

        $day7 = 86400 * 10;
        $daydate = date("Y-m-d", time() - $day7);    // H:m:s
        $daydatebegin = $daydate . ' 00:00:00';
        //echo $daydate;
        $daydateend = $daydate . ' 23:59:59';
        //echo $daydatebegin.' '.$daydateend.'<br/>';
        $where = array(
            'modUserProfile.email:!=' => '',
            'modUser.registr:=' => 1,
            'modUser.submitafter_first_order:=' => 0
        );

        $q->select(array(
            'modUserProfile.email',
            'modUser.created',
            'modUser.submitofferup',
            'modUser.registr',
            'modUserProfile.fullname',
            'modUser.id'
        ));

        $q->innerJoin('modUserProfile', 'modUserProfile', 'modUser.id=modUserProfile.internalKey');

        $q->sortby('modUser.created', 'ASC');
        $q->groupby('modUserProfile.email');//msOrder.user_id,
        $q->where($where);
        $q->prepare();
        $q->stmt->execute();

        $result = $q->stmt->fetchAll(PDO::FETCH_ASSOC);


        if (count($result) > 0) {
            foreach ($result as $us) {

                $user_id = $us['id'];
                $q = $this->modx->newQuery('msOrderProduct');
                $where = array(
                    'msOrder.user_id:=' => $user_id,
                    'msOrder.createdon:>=' => $daydatebegin,
                    'msOrder.createdon:<=' => $daydateend,
                    'msOrder.status:=' => 2,
                );

                $q->select(array(
                    'msOrder.id as orderid',
                ));

                $q->innerJoin('msOrder', 'msOrder', 'msOrderProduct.order_id=msOrder.id');

                $q->where($where);
                $q->prepare();
                $q->stmt->execute();
                $result_order = $q->stmt->fetchAll(PDO::FETCH_ASSOC);
                if (count($result_order) == 1) {
                    return $result;
                }
            }

        }

        return false;//$result;//$usersnoorder;//$result;
    }


    public function getusersofferup()
    {

        //echo floor(($today - $moon) / 86400)." дней назад.";
        //$q = $this->modx->newQuery('modUser');
        $q = $this->modx->newQuery('modUser');

        //ROUND((UNIX_TIMESTAMP()-UNIX_TIMESTAMP('date_pay'))/86400) AS days

        $day7 = 86400 * 7;//10;
        $daydate = date("Y-m-d", time() - $day7);    // H:m:s
        $daydatebegin = $daydate . ' 00:00:00';
        //echo $daydate;
        $daydateend = $daydate . ' 23:59:59';
        //echo $daydatebegin.' '.$daydateend.'<br/>';
        $where = array(
            'modUserProfile.email:!=' => '',
            'modUser.created:>=' => $daydatebegin,
            'modUser.created:<=' => $daydateend,
            'modUser.registr:=' => 0,
            'modUser.submitofferup:=' => 0
        );

        $q->select(array(
            'modUserProfile.email',
            'modUser.created',
            'modUser.submitofferup',
            'modUser.registr',
            'modUserProfile.fullname',
            'modUser.id'
        ));

        $q->innerJoin('modUserProfile', 'modUserProfile', 'modUser.id=modUserProfile.internalKey');

        $q->sortby('modUser.created', 'ASC');
        $q->groupby('modUserProfile.email');//msOrder.user_id,
        $q->where($where);
        $q->prepare();
        //echo $q->toSQL();
        $q->stmt->execute();

        $result = $q->stmt->fetchAll(PDO::FETCH_ASSOC);
        $users = [];
        foreach ($result as $user) {
            $user_id = $user['id'];
            $q = $this->modx->newQuery('msOrderProduct');
            $where = array(
                'msOrder.user_id:=' => $user_id,
            );

            $q->select(array(
                'msOrder.id as orderid',
            ));


            $q->where($where);
            $q->prepare();
            $q->stmt->execute();
            $result_order = $q->stmt->fetchAll(PDO::FETCH_ASSOC);
            if (count($result_order) > 0) {
                $users[$user_id] = $user;
            }
        }

        return $users;//$usersnoorder;//$result;
    }

    public function getorderproductbyuser($user_id)
    {
        $q = $this->modx->newQuery('msOrder');
        /*$day7=86400*7;
        $daydate=date("Y-m-d",time()-$day7);

        $daydatebegin=$daydate.' 00:00:00';
        $daydateend=$daydate.' 23:59:59';
        */
        $where = array(
            'msOrder.user_id:=' => $user_id,
        );

        $q->select(array(
            //'modUserProfile.email',
            //'modUser.mailrequestreview',
            'modUser.id as user_id',
            'msOrder.id',//
            'msOrder.createdon',
            'orderproduct.product_id',
            'orderproduct.name',
            'orderproduct.count',
            'orderproduct.price',
            'orderproduct.cost',
            'orderproduct.cost_old',
            'orderproduct.price_old'
        ));


        $q->innerJoin('modUser', 'modUser', 'msOrder.user_id=modUser.id');
        $q->innerJoin('modUserProfile', 'modUserProfile', 'modUser.id=modUserProfile.internalKey');
        $q->innerJoin('msOrderProduct', 'orderproduct', 'msOrder.id=orderproduct.order_id');

        $q->sortby('msOrder.createdon', 'DESC');
        $q->where($where);
        $q->prepare();
        $q->stmt->execute();
        $result = $q->stmt->fetchAll(PDO::FETCH_ASSOC);

        return $result;

    }

    public function getproductbyorder($order_id, $user_id)
    {
        $q = $this->modx->newQuery('msOrderProduct');
        $where = array(
            'msOrderProduct.order_id:=' => $order_id,
            'msOrder.user_id:=' => $user_id,
        );

        $q->select(array(
            'msOrder.id as orderid',
            'msOrder.createdon',
            'msOrderProduct.product_id',
            'msOrderProduct.name',
            'msOrderProduct.count',
            'msOrderProduct.price',
            'msOrderProduct.cost',
            'msOrderProduct.cost_old',
            'msOrderProduct.price_old'
        ));

        $q->innerJoin('msOrder', 'msOrder', 'msOrderProduct.order_id=msOrder.id');

        $q->where($where);
        $q->prepare();
        $q->stmt->execute();
        $result = $q->stmt->fetchAll(PDO::FETCH_ASSOC);

        return $result;

    }

    public function getuserbyemail($email)
    {
        $q = $this->modx->newQuery('modUser');//msOrder');

        $where = array(
            'modUserProfile.email:=' => $email,
        );

        // $q->where("order.createdon > CAST('" . $daydate . "' AS DATETIME) " , xPDOQuery::SQL_AND);

        $q->select(array(
            'modUserProfile.email',
            'modUser.username',
            'modUser.id',
            'modUserProfile.phone',
            // 'modUser.mailrequestreview',
            'modUserProfile.fullname',
            'modUserProfile.extended'
        ));


        //$q->innerJoin('modUser', 'modUser', 'msOrder.user_id=modUser.id');
        $q->innerJoin('modUserProfile', 'modUserProfile', 'modUser.id=modUserProfile.internalKey');
        //$q->limit(10);
        //$q->sortby('msOrder.createdon', 'DESC');
        $q->groupby('modUserProfile.phone');//msOrder.user_id,
        $q->where($where);
        $q->prepare();
        $q->stmt->execute();

        $result = $q->stmt->fetch(PDO::FETCH_ASSOC);
        return $result;
    }

    public function getuserbyphone($phone)
    {
        $q = $this->modx->newQuery('modUser');//msOrder');
        $q->innerJoin('modUserProfile', 'modUserProfile', 'modUser.id=modUserProfile.internalKey');
        $where = array(
            'modUserProfile.phone:=' => $phone,
        );

        $q->select(array(
            'modUserProfile.email',
            'modUserProfile.phone',
            'modUser.username',
            'modUser.id',
            // 'modUser.mailrequestreview',
            'modUserProfile.fullname',
            'modUserProfile.extended'
        ));


        $q->where($where);
        $q->prepare();
        $q->stmt->execute();

        $result = $q->stmt->fetch(PDO::FETCH_ASSOC);
        return $result;
    }


    public function updatereviewdata()
    {
        $q = $this->modx->newQuery('modUser');

        $day60 = 86400 * 61;
        $daydate = date("Y-m-d", time() - $day60);

        $daydatebegin = $daydate . ' 00:00:00';
        $daydateend = $daydate . ' 23:59:59';

        $where = array(
            'daterequestreview:<=' => $daydateend,
            'mailrequestreview:=' => 1,
        );
        $q->command('update');
        $q->set(array(
            'mailrequestreview' => 0,
            'daterequestreview' => '00-00-00 00:00:00',
        ));
        $q->where($where);

        $q->prepare();
        if ($q->stmt->execute()) return true;
        else return false;

    }

    public function updatesubmitMail($type, $user_id, $object_id = false)
    {
        switch ($type) {
            case 'birsday':
                if ($object_id) {
                    $c = $this->modx->newQuery('msPet');
                    $c->command('update');
                    $c->set(array(
                        'submit_mail_birsday' => 1,
                    ));

                    $c->where(array(
                        'id' => $object_id,
                        'user_id' => $user_id
                    ));
                    $c->prepare();
                    if ($c->stmt->execute()) return true;
                    else return false;
                }

                break;
            case '90':
                $c = $this->modx->newQuery('modUser');
                $c->command('update');
                $c->set(array(
                    'mailmore90' => 1,
                    'last_mailmore90' => date("Y-m-d H:m:s"),
                ));
                $c->where(array(
                    'id' => $user_id,
                ));
                $c->prepare();
                if ($c->stmt->execute()) return true;
                else return false;

                break;
            case '120':
                $c = $this->modx->newQuery('modUser');
                $c->command('update');
                $c->set(array(
                    'mailmore120' => 1,
                    'last_mailmore120' => date("Y-m-d H:m:s"),
                ));
                $c->where(array(
                    'id' => $user_id,
                ));
                $c->prepare();
                if ($c->stmt->execute()) return true;
                else return false;
                break;
            case '180':
                $c = $this->modx->newQuery('modUser');
                $c->command('update');
                $c->set(array(
                    'mailmore180' => 1,
                    'last_mailmore180' => date("Y-m-d H:m:s"),
                ));
                $c->where(array(
                    'id' => $user_id,
                ));
                $c->prepare();
                if ($c->stmt->execute()) return true;
                else return false;
                break;

        }

    }

    public function updatesubmit90($user_id)
    {
        $c = $this->modx->newQuery('modUser');
        $c->command('update');
        $c->set(array(
            'mailmore90' => 1,
            'last_mailmore90' => date("Y-m-d H:m:s"),
        ));
        $c->where(array(
            'id' => $user_id,
        ));
        $c->prepare();
        if ($c->stmt->execute()) return true;
        else return false;
    }

    public function updatesubmit60($user_id)
    {

        $c = $this->modx->newQuery('modUser');
        $c->command('update');
        $c->set(array(
            'mailmore60' => 1,
            'last_mailmore60' => date("Y-m-d H:m:s"),
        ));
        $c->where(array(
            'id' => $user_id,
        ));
        $c->prepare();
        if ($c->stmt->execute()) return true;
        else return false;
    }

    public function updatesubmit45($user_id)
    {

        $c = $this->modx->newQuery('modUser');
        $c->command('update');
        $c->set(array(
            'mailmore45' => 1,
            'last_mailmore45' => date("Y-m-d H:m:s"),
        ));
        $c->where(array(
            'id' => $user_id,
        ));
        $c->prepare();
        if ($c->stmt->execute()) return true;
        else return false;
    }

    public function updatesubmit30($user_id)
    {

        $c = $this->modx->newQuery('modUser');
        $c->command('update');
        $c->set(array(
            'mailmore30' => 1,
            'last_mailmore30' => date("Y-m-d H:m:s"),
        ));
        $c->where(array(
            'id' => $user_id,
        ));
        $c->prepare();
        if ($c->stmt->execute()) return true;
        else return false;
    }


    public function updatesubmitfirstorder($user_id)
    {

        $c = $this->modx->newQuery('modUser');
        $c->command('update');
        $c->set(array(
            'submitafter_first_order' => 1,
            'last_submitafter_first_order' => date("Y-m-d H:m:s"),

        ));
        $c->where(array(
            'id' => $user_id,
        ));
        $c->prepare();
        if ($c->stmt->execute()) return true;
        else return false;


    }

    public function updatesubmitofferup($user_id)
    {

        $c = $this->modx->newQuery('modUser');
        $c->command('update');
        $c->set(array(
            'submitofferup' => 1,
            'last_offerup' => date("Y-m-d H:m:s"),
            //'daterequestreview'=>date("Y-m-d H:m:s")
        ));
        $c->where(array(
            'id' => $user_id,
        ));
        $c->prepare();
        if ($c->stmt->execute()) return true;
        else return false;
    }

    public function updatesubmitregistrnobuy($user_id)
    {

        $c = $this->modx->newQuery('modUser');
        $c->command('update');
        $c->set(array(
            'mailregistrnobuy' => 1,
            //'daterequestreview'=>date("Y-m-d H:m:s")
        ));
        $c->where(array(
            'id' => $user_id,
        ));
        $c->prepare();
        if ($c->stmt->execute()) return true;
        else return false;
        //echo 'sql'.$c->toSQL();
    }

    public function updatesubmitnoregistrnobuy($user_id)
    {

        $c = $this->modx->newQuery('modUser');
        $c->command('update');
        $c->set(array(
            'mailnoregistrnobuy' => 1,
            'lastnoregistrnobuy' => date("Y-m-d H:m:s")
        ));
        $c->where(array(
            'id' => $user_id,
        ));
        $c->prepare();
        if ($c->stmt->execute()) return true;
        else return false;
    }

    public function updatesubmitreview($user_id)
    {

        $c = $this->modx->newQuery('modUser');
        $c->command('update');
        $c->set(array(
            'mailrequestreview' => 1,
            'daterequestreview' => date("Y-m-d H:m:s")
        ));
        $c->where(array(
            'id' => $user_id,
        ));
        $c->prepare();
        if ($c->stmt->execute()) return true;
        else return false;
        //echo 'sql'.$c->toSQL();
    }

    public function updatesubmitdaysFromorder($user_id)
    {

        $c = $this->modx->newQuery('modUser');
        $c->command('update');
        $c->set(array(
            'mailafterdaysorder' => 1,
            'lastafterdaysorder' => date("Y-m-d H:m:s")
            //'daterequestreview'=>date("Y-m-d H:m:s")
        ));
        $c->where(array(
            'id' => $user_id,
        ));
        $c->prepare();
        if ($c->stmt->execute()) return true;
        else return false;
        //echo 'sql'.$c->toSQL();
    }


    public function isproduct_review($product_id = '', $user_id = 0)
    {
        $arr = false;
        if ($user_id && ($user_id != 0) && ($product_id != '')) {
            $sql = "SELECT C.* FROM modx_tickets_comments C 
                    	LEFT JOIN modx_tickets_threads D
			ON C.thread=D.id WHERE C.published=1 AND C.createdby=$user_id AND C.deleted=0 AND D.resource =  '" . $product_id . "'";

            $q = $this->modx->prepare($sql);
            $q->execute(array(0));
            $arr = $q->fetch(PDO::FETCH_ASSOC);
            if ($arr) return true;
        }
        return false;

    }


    public function getuserrequestorder($user_id = false)
    {
        $q = $this->modx->newQuery('msOrder');
        $day80 = 86400 * 80;
        $daydate = date("Y-m-d", time() - $day80);

        $daydatebegin = $daydate . ' 00:00:00';
        $daydateend = $daydate . ' 23:59:59';

        $where = array(
            //'msOrder.createdon:>='=>$daydatebegin,
            'modUser.last_order:>=' => $daydatebegin,
            //'msOrder.status:='=>2,
            //'msOrder.createdon:<='=>$daydateend,
            'modUser.mailrequestreview:=' => 0,
            'modUserProfile.email:!=' => '',
        );
        if ($user_id) $where['modUser.id:='] = $user_id;

        // $q->where("order.createdon > CAST('" . $daydate . "' AS DATETIME) " , xPDOQuery::SQL_AND);

        $q->select(array(
            'modUserProfile.email',
            'modUserProfile.extended',
            'modUser.username',
            'modUser.id as userid',
            'modUserProfile.fullname',
            'modUser.mailrequestreview',
            'msOrder.*',//createdon'
        ));


        $q->innerJoin('modUser', 'modUser', 'msOrder.user_id=modUser.id');
        $q->innerJoin('modUserProfile', 'modUserProfile', 'modUser.id=modUserProfile.internalKey');
        //$q->limit(10);
        $q->sortby('msOrder.createdon', 'DESC');
        $q->groupby('msOrder.user_id');//modUser.id');//msOrder.id');
        $q->where($where);
        $q->prepare();
        $q->stmt->execute();

        $result = $q->stmt->fetchAll(PDO::FETCH_ASSOC);
        //print_r($result);
        return $result;
    }

    public function getuserrequestreviewText($user_id = false)
    {


        $q = $this->modx->newQuery('msOrder');
        $day7 = 86400 * 7;
        $daydate = date("Y-m-d", time() - $day7);

        $daydatebegin = $daydate . ' 00:00:00';
        $daydateend = $daydate . ' 23:59:59';
        //'modUser.last_order:>='=>$daydatebegin,
        $where = array(

            'msOrder.status:=' => 2,
            //'msOrder.createdon:>=' => $daydatebegin,
            //'msOrder.createdon:<=' => $daydateend,
            'modUser.mailrequestreview:=' => 0,
            'modUserProfile.email:!=' => '',
        );
        if ($user_id) {
            $where['modUser.id:='] = $user_id;
            $where['msOrder.id:!='] = 4763;
        }

        // $q->where("order.createdon > CAST('" . $daydate . "' AS DATETIME) " , xPDOQuery::SQL_AND);

        $q->select(array(
            'modUserProfile.email',
            'modUserProfile.extended',
            'modUser.username',
            'modUser.id as userid',
            'modUserProfile.fullname',
            'modUser.mailrequestreview',
            'msOrder.*',//createdon'
        ));


        $q->innerJoin('modUser', 'modUser', 'msOrder.user_id=modUser.id');
        $q->innerJoin('modUserProfile', 'modUserProfile', 'modUser.id=modUserProfile.internalKey');
        //$q->limit(10);
        $q->sortby('msOrder.createdon', 'DESC');
        $q->groupby('msOrder.user_id');//modUser.id');//msOrder.id');
        $q->where($where);
        $q->prepare();
        $q->stmt->execute();

        $result = $q->stmt->fetchAll(PDO::FETCH_ASSOC);

        return $result;
    }

    public function getuserrequestreview($user_id = false)
    {


        $q = $this->modx->newQuery('msOrder');
        $day7 = 86400 * 7;
        $daydate = date("Y-m-d", time() - $day7);

        $daydatebegin = $daydate . ' 00:00:00';
        $daydateend = $daydate . ' 23:59:59';
        //'modUser.last_order:>='=>$daydatebegin,
        $where = array(
            'msOrder.status:=' => 2,
            'msOrder.createdon:>=' => $daydatebegin,
            'msOrder.createdon:<=' => $daydateend,
            'modUser.mailrequestreview:=' => 0,
            'modUserProfile.email:!=' => '',
        );
        if ($user_id) $where['modUser.id:='] = $user_id;

        // $q->where("order.createdon > CAST('" . $daydate . "' AS DATETIME) " , xPDOQuery::SQL_AND);

        $q->select(array(
            'modUserProfile.email',
            'modUserProfile.extended',
            'modUser.username',
            'modUser.id as userid',
            'modUserProfile.fullname',
            'modUser.mailrequestreview',
            'msOrder.*',//createdon'
        ));


        $q->innerJoin('modUser', 'modUser', 'msOrder.user_id=modUser.id');
        $q->innerJoin('modUserProfile', 'modUserProfile', 'modUser.id=modUserProfile.internalKey');
        //$q->limit(10);
        $q->sortby('msOrder.createdon', 'DESC');
        $q->groupby('msOrder.user_id');//modUser.id');//msOrder.id');
        $q->where($where);
        $q->prepare();
        $q->stmt->execute();

        $result = $q->stmt->fetchAll(PDO::FETCH_ASSOC);

        return $result;
    }

    public function createShortUrl($url)
    {

        $turbosms_api = 'dee64278ba5eee2096c78b6c334dbb1118dfa71a';
        $turbosms_link = 'https://api-ssl.bitly.com/v4/bitlinks';

        $ch = curl_init($turbosms_link);

        $post = [
            'long_url' => $url
        ];

        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            'Content-Type: application/json',
            'Authorization: Bearer ' . $turbosms_api
        ));

        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($post));//http_build_query($post));
        $result = curl_exec($ch);

        $resultArray = json_decode($result, true);
        if (isset($resultArray['link']))
            return $resultArray['link'];

        return $url;
    }

    public function getUserNobuyNoreg($days = 1, $user_id = false)
    {
        $today = time();
        $moon = mktime(0, 0, 0, 2, 15, 2016);
        //$days = floor(($today - $moon) / 86400);
        //echo floor(($today - $moon) / 86400)." дней назад.";
        $q = $this->modx->newQuery('modUser');

        //ROUND((UNIX_TIMESTAMP()-UNIX_TIMESTAMP('date_pay'))/86400) AS days

        $day = 86400 * $days;
        $daydate = date("Y-m-d", time() - $day);    // H:m:s
        $daydatebegin = $daydate . ' 00:00:00';
        //echo $daydate;
        $daydateend = $daydate . ' 23:59:59';
        echo $daydatebegin . ' ' . $daydateend;
        if ($days == 1)
            $field = 'modUser.mailnoregistrnobuy';
        else $field = 'modUser.mailnoregistrnobuy' . $days;
        $where = array(
            //'modUserProfile.email:!=' => '',
            'modUser.created:>=' => $daydatebegin,
            'modUser.created:<=' => $daydateend,
            'modUser.registr:=' => 0,
            $field . ':=' => 0 // проверяем отправлялось ли ранее письмо это  0 - нет, 1 - да
        );

        if ($user_id) {
            $where['modUser.id:='] = $user_id;
        }
        // $q->where("order.createdon > CAST('" . $daydate . "' AS DATETIME) " , xPDOQuery::SQL_AND);

        $q->select(array(
            'modUserProfile.email',
            'modUserProfile.phone',
            'modUserProfile.fullname',
            'modUser.created',
            'modUser.username',
            'modUser.registr',
            'modUser.registr_date',
            'modUser.mailnoregistrnobuy',
            'modUser.lastnoregistrnobuy',
            'modUser.id'
        ));

        $q->innerJoin('modUserProfile', 'modUserProfile', 'modUser.id=modUserProfile.internalKey');

        $q->sortby('modUser.created', 'ASC');
        //$q->groupby('modUserProfile.email');//msOrder.user_id,
        $q->where($where);
        $q->prepare();
        $q->stmt->execute();
        //echo $q->toSql()."\r\n\r\n";

        $result = $q->stmt->fetchAll(PDO::FETCH_ASSOC);
        $usersnoorder = array();
        foreach ($result as $r) {
            $q = $this->modx->newQuery('msOrder');
            $where = array(
                'msOrder.user_id:=' => $r['id']
            );
            $q->where($where);
            $q->select('*');
            $q->prepare();
            $q->stmt->execute();
            $resultorders = $q->stmt->fetchAll(PDO::FETCH_ASSOC);
            if (count($resultorders) == 0) {
                $usersnoorder[] = $r;
            }
        }


//        echo '<pre>';
//        print_r($usersnoorder);
//        echo '</pre>';
//        echo 'count = '.count($usersnoorder);
//        die();
        return $usersnoorder;
    }

    public function getusersregistrnobuy()
    {
        $today = time();
        $moon = mktime(0, 0, 0, 2, 15, 2016);
        $days = floor(($today - $moon) / 86400);
        //echo floor(($today - $moon) / 86400)." дней назад.";
        $q = $this->modx->newQuery('modUser');

        //ROUND((UNIX_TIMESTAMP()-UNIX_TIMESTAMP('date_pay'))/86400) AS days

        $day5 = 86400 * 3;
        $daydate = date("Y-m-d", time() - $day5);    // H:m:s
        $daydatebegin = $daydate . ' 00:00:00';
        //echo $daydate;
        $daydateend = $daydate . ' 23:59:59';
        echo $daydatebegin . ' ' . $daydateend;
        $where = array(
            'modUserProfile.email:!=' => '',
            'modUser.registr_date:>=' => $daydatebegin,
            'modUser.registr_date:<=' => $daydateend,
            'modUser.registr:=' => 1,
            'modUser.mailregistrnobuy' => 0 // проверяем отправлялось ли ранее письмо это  0 - нет, 1 - да
        );

        // $q->where("order.createdon > CAST('" . $daydate . "' AS DATETIME) " , xPDOQuery::SQL_AND);

        $q->select(array(
            'modUserProfile.email',
            'modUserProfile.fullname',
            'modUser.created',
            'modUser.registr',
            'modUser.registr_date',
            'modUser.mailregistrnobuy',
            'modUser.id'
        ));

        $q->innerJoin('modUserProfile', 'modUserProfile', 'modUser.id=modUserProfile.internalKey');

        $q->sortby('modUser.registr_date', 'ASC');
        $q->groupby('modUserProfile.email');//msOrder.user_id,
        $q->where($where);
        $q->prepare();
        $q->stmt->execute();
        $result = $q->stmt->fetchAll(PDO::FETCH_ASSOC);
        $usersnoorder = array();
        foreach ($result as $r) {
            $q = $this->modx->newQuery('msOrder');
            $where = array(
                'msOrder.user_id:=' => $r['id']
            );
            $q->where($where);
            $q->select('*');
            $q->prepare();
            $q->stmt->execute();
            $resultorders = $q->stmt->fetchAll(PDO::FETCH_ASSOC);
            if (count($resultorders) == 0) {
                $usersnoorder[] = $r;
            }
        }

        return $usersnoorder;
    }


    public function getusersafterBuy($user_id_debug = false)
    {
        $today = time();
        $moon = mktime(0, 0, 0, 2, 15, 2016);
        $days = floor(($today - $moon) / 86400);
        $q = $this->modx->newQuery('modUser');

        //ROUND((UNIX_TIMESTAMP()-UNIX_TIMESTAMP('date_pay'))/86400) AS days

        $day5 = 86400 * 14;
        $daydate = date("Y-m-d", time() - $day5);    // H:m:s
        $daydatebegin = $daydate . ' 00:00:00';

        $daydateend = $daydate . ' 23:59:59';
        echo $daydatebegin . ' ' . $daydateend;
        $where = array(
            'modUserProfile.email:!=' => '',
//            'modUser.registr_date:>=' => $daydatebegin,
//            'modUser.registr_date:<=' => $daydateend,
//            'modUser.registr:=' => 1,
            //'msOrder.createdon:>=' => $daydatebegin,
            'msOrder.status:=' => 2,
            //'msOrder.createdon:<=' => $daydateend,
            'modUser.mailafterdaysorder' => 0
            //'modUser.mailregistrnobuy' => 0 // проверяем отправлялось ли ранее письмо это  0 - нет, 1 - да
        );

        if ($user_id_debug) {
            $where['modUser.id:='] = $user_id_debug;
        } else {
            $where['msOrder.createdon:>='] = $daydatebegin;
            $where['msOrder.createdon:<='] = $daydateend;
        }
        // $q->where("order.createdon > CAST('" . $daydate . "' AS DATETIME) " , xPDOQuery::SQL_AND);

        $q->select(array(
            'modUserProfile.email',
            'modUserProfile.fullname',
            'modUser.created',
            'modUser.registr',
            'modUser.registr_date',
            'modUser.mailafterdaysorder',
            'modUser.id'
        ));

        $q->innerJoin('msOrder', 'msOrder', 'msOrder.user_id=modUserProfile.internalKey');
        $q->innerJoin('modUserProfile', 'modUserProfile', 'modUser.id=modUserProfile.internalKey');

        $q->sortby('modUser.registr_date', 'ASC');
        $q->groupby('modUserProfile.email');//msOrder.user_id,
        $q->where($where);
        $q->prepare();
        $q->stmt->execute();
//echo 'sql'.$q->toSQL();
        $result = $q->stmt->fetchAll(PDO::FETCH_ASSOC);
        $usersorder = array();
        foreach ($result as $r) {
            $q = $this->modx->newQuery('msOrder');
            $q->innerJoin('modUser', 'modUser', 'modUser.id=msOrder.user_id');

            $q->innerJoin('modUserProfile', 'modUserProfile', 'modUser.id=modUserProfile.internalKey');

            $where = array(
                'msOrder.user_id:=' => $r['id']
            );
            $q->where($where);
            $q->select('*');
            $q->prepare();
            $q->stmt->execute();
            $resultorders = $q->stmt->fetchAll(PDO::FETCH_ASSOC);
            /*echo '<pre>$resultorders';
            print_r($resultorders);
            echo '</pre>';*/
            if (count($resultorders) == 1) {
                $usersorder[] = $r;
            }
        }
        /*
        echo '<pre>$result';
        print_R($result);
        echo '</pre>';*/
        //print_r($result);
        return $usersorder;//$result;
    }


    public function getusersmore60()
    {

        $q = $this->modx->newQuery('modUser');

        $day60 = 86400 * 60;
        $daydate = date("Y-m-d", time() - $day60);    // H:m:s
        $daydatebegin = $daydate . ' 00:00:00';
        $daydateend = $daydate . ' 23:59:59';
        $where = array(
            'modUser.last_order:<=' => $daydateend,
            'modUser.last_order:>=' => $daydatebegin,
            'modUser.last_order:!=' => NULL,
            'modUser.last_order:!=' => '',
            //'modUserProfile.email:!=' => '',
            //'modUser.mailmore60:!=' => 1,
            //'OR:modUser.mailmore60:=' => NULL,
        );

        $q->select(array(
            'modUserProfile.email',
            'modUserProfile.phone',
            //'msOrder.createdon'
            'modUser.last_order',
            'modUserProfile.extended',
        ));

        $q->innerJoin('modUserProfile', 'modUserProfile', 'modUser.id=modUserProfile.internalKey');
        //$q->innerJoin('modUser', 'modUser', 'modUserProfile.internalKey=modUser.id');
        //$q->sortby('msOrder.createdon', 'DESC');
        //$q->groupby('msOrder.user_id,modUserProfile.email');


        $q->where($where, xPDOQuery::SQL_AND);


        $q->where(array(array('modUser.mailmore60:=' => 0), array('modUser.mailmore60:=' => NULL)), xPDOQuery::SQL_OR);
        $q->prepare();
        $q->stmt->execute();
        echo "\r\nsql" . $q->toSQL() . "\r\n";
        $result = $q->stmt->fetchAll(PDO::FETCH_ASSOC);
        return $result;
    }

    public function getUsersByLastOrder($daydate, $period = 60)
    {
        // $daydate = date("Y-m-d", strtotime('-90 day'));
        $q = $this->modx->newQuery('modUser');
        $daydatebegin = $daydate . ' 00:00:00';
        $daydateend = $daydate . ' 23:59:59';

        switch ($period) {
            case '90':
            case '180':
                $where = array(
                    'modUser.last_order:<=' => $daydateend,
                    'modUser.last_order:>=' => $daydatebegin,
                    'modUser.last_order:!=' => NULL,
                    'modUser.last_order:!=' => '',
                    'modUser.summa_all_orders:>=' => 1000
                );
                break;
            default:
                $where = array(
                    'modUser.last_order:<=' => $daydateend,
                    'modUser.last_order:>=' => $daydatebegin,
                    'modUser.last_order:!=' => NULL,
                    'modUser.last_order:!=' => '',
                );
                break;
        }
        /*$where = array(
            'modUser.last_order:<=' => $daydateend,
            'modUser.last_order:>=' => $daydatebegin,
            'modUser.last_order:!=' => NULL,
            'modUser.last_order:!=' => '',
            //'modUser.mailmore'.$period.':!=' => 1,
            //'OR:modUser.mailmore'.$period.':=' => NULL,
        );*/

        $q->select(array(
            'modUserProfile.email',
            'modUserProfile.phone',
            'modUser.last_order',
            'modUserProfile.extended',
        ));

        $q->innerJoin('modUserProfile', 'modUserProfile', 'modUser.id=modUserProfile.internalKey');

        $q->where($where, xPDOQuery::SQL_AND);


        $q->where(array(array('modUser.mailmore' . $period . ':=' => 0), array('modUser.mailmore' . $period . ':=' => NULL)), xPDOQuery::SQL_OR);
        $q->prepare();
        $q->stmt->execute();
        echo "\r\nperiod " . $period . " sql " . $q->toSQL() . "\r\n";
        $result = $q->stmt->fetchAll(PDO::FETCH_ASSOC);
        return $result;
    }

    public function getusersmore90()
    {


        $daydate = date("Y-m-d", strtotime('-90 day'));
        return $this->getUsersByLastOrder($daydate, 90);
    }

    public function getusersmore120()
    {


        $daydate = date("Y-m-d", strtotime('-120 day'));
        return $this->getUsersByLastOrder($daydate, 120);
    }

    public function getusersmore180()
    {


        $daydate = date("Y-m-d", strtotime('-180 day'));
        return $this->getUsersByLastOrder($daydate, 180);
    }


    public function validatecode()
    {
        //global $modx;
        //if ($code=='')
        $code = rand(10000, 99999);
        $item = $this->modx->getObject('PromocodeItem', array('code' => $code));
        if (!$item) return $code;
        else return $this->validatecode();
    }

    public function generatepromocode($discount_promocode)
    {

        $code = $this->validatecode();
        $created = date('Y-m-d');

        $after7day = mktime(0, 0, 0, date("m"), date("d") + 7, date("Y"));
        $term = $after7day;


        /*скидка 5% или не зарегистрирован - промокод 7%
    скидка 7% - промокод 10%
    скидка 10% - промокод 12%
    */

        $status = 1;
        $arraypost = array(
            'code' => $code,
            'name' => 'код с почты',
            'term' => $term,
            'category' => 10,
            'type' => 'dinamic',
            'status' => $status,
            'discount' => $discount_promocode,//'7%',
            'removeloylnost' => 0,
            'created' => $created,
            'email' => 1,
        );


        $item = $this->modx->newObject('PromocodeItem', $arraypost);

        if ($item->save()) {
            //if(isset($_POST['category']))
            //{
            $item_category = $this->modx->newObject('PromocodeCategory');

            $item_category->set('category', 10);
            $item_category->set('promocode', (int)$item->get('id'));
            $item_category->save();
            //}
            return $code;
        } else return false;

    }

    public function getUsersHappyBirsdayPets($userId = false)
    {


        //$q = $this->modx->newQuery('modUser');
        $year = date('Y');
        $daydate = $year . '-01-01';
        $daydatebegin = $daydate . ' 00:00:00';

        $daydate = $year . '-12-31';
        $daydateend = $daydate . ' 23:59:59';

        $created_pet = date("Y-m-d", strtotime('-30 day'));
        $daycreatedbegin = $created_pet . ' 00:00:00';
        $daycreatedend = $created_pet . ' 23:59:59';


        //echo '$daycreatedbegin'.$daycreatedbegin.' '.$daycreatedend."\r\n\r\n";
        //die();
        $sql = "SELECT modx_ms2_pets.* FROM modx_ms2_pets 
WHERE modx_ms2_pets.submit_mail_birsday=0
AND modx_ms2_pets.created <= '" . $daycreatedend . "'";
        if ($userId) {
            $sql .= " AND user_id='$userId'";
        }
        //BETWEEN CAST('".$daycreatedbegin."' AS DATE) AND CAST('".$daycreatedend."' AS DATE) ";


        //' LEFT JOIN ';
        $q = $this->modx->prepare($sql);
        $q->execute();
        $result = $q->fetchAll(PDO::FETCH_ASSOC);

        foreach ($result as $res) {
            $pet = $this->modx->getObject('msPet', $res['id']);
            if ($pet) {
                if (($pet->get('date_mail') != '0000-00-00 00:00:00') && ($pet->get('date_mail') != '')) {
                    $d = (date('Y') - 1) . date('-m-d');
                    $date_mail = strtotime($pet->get('date_mail'));
                    if ($d >= $date_mail) {
                        $pet->set('submit_mail_birsday', 0);
                        $pet->save();
                        $model_user = $this->modx->getObject('modUser', $res['user_id']);
                        $model_user->set('count_mail_birsday_pets', 0);
                        $model_user->save();
                    }
                }

            }
        }


        $sql = "SELECT modx_ms2_pets.* FROM modx_ms2_pets 
WHERE modx_ms2_pets.submit_mail_birsday=0
AND modx_ms2_pets.created <= '" . $daycreatedend . "'";
        if ($userId) {
            $sql .= " AND user_id='$userId'";
        }

        //BETWEEN CAST('".$daycreatedbegin."' AS DATE) AND CAST('".$daycreatedend."' AS DATE) ";

        //echo '$sql '.$sql;
        //' LEFT JOIN ';
        $q = $this->modx->prepare($sql);
        $q->execute();
        $result = $q->fetchAll(PDO::FETCH_ASSOC);


        //$day_current = date("Y-m-d");
        $day_current = date("m-d");
        foreach ($result as $res) {
            $user_id = $res['user_id'];
            $model_user = $this->modx->getObject('modUser', $user_id);

            $date_current = date('Y-m-d');// H:i:s');
            $day = $res['bday'];
            $length = (int)log10($day) + 1;
            if ($length == 1) $day = '0' . $day;
            $month = $res['bmonth'];
            $length = (int)log10($month) + 1;
            if ($length == 1) $month = '0' . $month;

            $birsday_day = $month . '-' . $day;


            //if ($model_user) {


            // }

            //$res['byear'] . '-' .
            /* echo '<pre>$res';
             print_r($res);
             echo '</pre>';*/

//echo '$birsday_day'.$birsday_day. ' '.$day_current."\r\n\r\n";
            if ($birsday_day == $day_current) {
                // echo $user_id . ' ' . $birsday_day . ' ' . $day_current . "\r\n";

                $q = $this->modx->newQuery('modUser');
                $where = array(
                    'modUser.count_mail_birsday_pets:<' => 3,
                    'modUser.id' => $user_id
                );
                $q->select(array(
                    'modUserProfile.email',
                    'modUserProfile.phone',
                    'modUserProfile.fullname',
                    'modUserProfile.extended',
                ));

                $q->innerJoin('modUserProfile', 'modUserProfile', 'modUser.id=modUserProfile.internalKey');

                $q->where($where);
                $q->prepare();
                $q->stmt->execute();
                $user_for_submit = $q->stmt->fetch(PDO::FETCH_ASSOC);

                if (count($user_for_submit) > 0) {
                    $extended = $user_for_submit['extended'];
                    $extended_array = $this->modx->fromJSON($extended);
                    if (isset($extended_array['discount'])) $discount = $extended_array['discount'];
                    else $discount = 0;


                    $discount_promocode = '10%';

                    if ($discount == '10')
                        $discount_promocode = '12%';


                    $email = $user_for_submit['email'];
                    //Пользователю приходит промо-код на скидку 10% (12% если у него уже дисконт 10%)
//Тема: Поздравляем {имя-питомца} с Днем Рождения!
                    //$model_user = $this->modx->getObject('modUser', $user_id);
                    $count_mail_birsday_pets = $model_user->get('count_mail_birsday_pets');
                    //echo "\r\n" . $model_user->get('count_mail_birsday_pets') . ' ' . $model_user->get('id') . "\r\n";
                    if ($count_mail_birsday_pets < 3) {
                        if ($email != '') {

                            $data = [];
                            $code = $this->generatepromocode($discount_promocode);
                            if ($code) $data['promocode'] = $code;
                            else $data['promocode'] = '';
                            $data['discount'] = $discount_promocode;
                            $subject = 'Поздравляем ' . $res['name'] . ' с Днем Рождения!';

                            $data['fullname'] = $user_for_submit['fullname'];
                            $data['namepet'] = $res['name'];
                            $data = array_merge($data, $this->addVariablesForMail($model_user->get('id')));
                            //$body = $this->modx->getChunk('happy_birsday_pet', $data);

                            $data['phoneSms'] = $data['phone'] = $model_user->get('username');
                            $data['email'] = $email;


                            if ($this->eventNotification(6, $model_user, $data)) {
                                //$this->sendEmail($email, $subject, $body)) {

                                if ($model_user) {

                                    $count_mail_birsday_pets = $count_mail_birsday_pets + 1;
                                    $model_user->set('count_mail_birsday_pets', $count_mail_birsday_pets);
                                    $model_user->save();
                                }
                                $pet->set('date_mail', $date_current);
                                $pet->save();
                                //$this->sendEmail('petchoice.info@gmail.com', 'отправилось письмо клиенту. Тема: "' . $subject . '"', 'Почта: ' . $email . '. Шаблон письма: ' . $body);
                                if ($this->updatesubmitMail('birsday', $user_id, $res['id'])) echo 'update submite mail<br />';
                                else echo 'error submite mail<br />';
                            } else $this->sendEmail('petchoice.info@gmail.com', 'Ошибка отправки письма. Тема: "' . $subject . '"', 'Почта: ' . $email . '. Шаблон письма: ' . $body);
                        }
                    }

                }

            }

            //}
        }

        /*
                $where = array(
                    'modUser.first_mail_birsday_pet:<=' => $daydateend,
                    'modUser.first_mail_birsday_pet:>=' => $daydatebegin,
                    //'modUser.last_order:!=' => NULL,
                    //'modUser.last_order:!=' => '',
                    //'modUser.mailmore60:=' => 0,
                );

                $q->select(array(
                    'modUserProfile.email',
                    'modUserProfile.phone',
                    'modUser.last_order',
                    'modUserProfile.extended',
                ));

                $q->innerJoin('modUserProfile', 'modUserProfile', 'modUser.id=modUserProfile.internalKey');
                $q->innerJoin('msPet', 'msPet', 'modUser.id=msPet.user_id');

                $q->where($where);
                $q->prepare();
                $q->stmt->execute();
                $result = $q->stmt->fetchAll(PDO::FETCH_ASSOC);
                return $result;*/


    }

    public function getuser_zero_bonus_all()
    {

        /*$q = $this->modx->newQuery('modUser');
        $q->innerJoin('modUserProfile', 'modUserProfile', 'modUser.id=modUserProfile.internalKey');

        $day45 = 86400 * 48;// за 3 дня
        //$daydate = date("Y-m-d", time() - $day45);    // H:m:s
        $daydate = date("Y-m-d");// strtotime('+45 days'));//ime() - $day45);    // H:m:s

        //$daydate='2018-10-14';
        $daydatebegin = $daydate . ' 00:00:00';
        $daydateend = $daydate . ' 23:59:59';
        $where = array(
            //'modUser.date_clear_bonuses:<=' => $daydateend,
            //'modUser.date_clear_bonuses:>=' => $daydatebegin,
            //'modUserProfile.email:!=' => '',
            'modUser.registr:=' => 0,
            'modUser.bonuses:>' => 0

        );
        $q->select(array(
            'modUserProfile.email',
            'modUser.id',
        ));
        $q->where($where);
        $q->prepare();
        $sql = $q->toSQL();
        echo '$sql'.$sql;

        $q->stmt->execute();
        $result = $q->stmt->fetchAll(PDO::FETCH_ASSOC);
        echo '<pre>$result' . $daydateend . ' ' . $daydatebegin;
        print_r($result);
        echo '</pre>';
        foreach ($result as $_user) {
            $model_user = $this->modx->getObject('modUser', $_user['id']);
            if ($model_user) {
                $model_user->set('bonuses', 0);
                //$model_user->set('date_clear_bonuses', date('Y-m-d H:i:s'));
                $model_user->set('date_clear_bonuses', NULL);
                $model_user->save();
                //$date_zero=date("Y-m-d");
                $date_zero = date('Y-m-d H:i:s');
                $this->saveLogBonuses(['amount' => 0, 'user_id' => $_user['id'], 'date_zero' => $date_zero, 'reason' => 'Списаны автоматически']);

            }

        }*/

        $q = $this->modx->newQuery('modUser');
        $q->innerJoin('modUserProfile', 'modUserProfile', 'modUser.id=modUserProfile.internalKey');

        // $day45 = 86400 * 48;// за 3 дня
        //$daydate = date("Y-m-d", time() - $day45);    // H:m:s
        $daydate = date("Y-m-d");//, strtotime('-48 days'));//ime() - $day45);    // H:m:s

        //$daydate='2018-10-14';
        $daydatebegin = $daydate . ' 00:00:00';
        $daydateend = $daydate . ' 23:59:59';
        $where = array(
            'modUser.date_clear_bonuses:<=' => $daydateend,
            //'modUser.date_clear_bonuses:>=' => $daydatebegin,
            //'modUserProfile.email:!=' => '',
            //'modUser.registr:=' => 0,
            //'modUser.bonuses:>' => 0

        );
        $q->select(array(
            'modUserProfile.email',
            'modUser.id',
        ));
        $q->where($where);
        $q->prepare();
        $sql = $q->toSQL();
        echo '$sql' . $sql;

        $q->stmt->execute();
        $result = $q->stmt->fetchAll(PDO::FETCH_ASSOC);
        echo '<pre>$result' . $daydateend . ' ' . $daydatebegin;
        print_r($result);
        echo '</pre>';
        foreach ($result as $_user) {
            $model_user = $this->modx->getObject('modUser', $_user['id']);
            if ($model_user) {
                $model_user->set('bonuses', 0);
                $model_user->set('date_clear_bonuses', NULL);
                $model_user->save();
                //$date_zero=date("Y-m-d");
                $date_zero = date('Y-m-d H:i:s');
                $this->saveLogBonuses(['amount' => 0, 'user_id' => $_user['id'], 'date_zero' => $date_zero, 'reason' => 'Списаны автоматически']);

            }

        }


        return $result;
    }

    public function getuser_zero_bonus()
    {

        $q = $this->modx->newQuery('modUser');
        $q->innerJoin('modUserProfile', 'modUserProfile', 'modUser.id=modUserProfile.internalKey');

        $day45 = 86400 * 48;// за 3 дня
        //$daydate = date("Y-m-d", time() - $day45);    // H:m:s
        $daydate = date("Y-m-d");// strtotime('+45 days'));//ime() - $day45);    // H:m:s

        //$daydate='2018-10-14';
        $daydatebegin = $daydate . ' 00:00:00';
        $daydateend = $daydate . ' 23:59:59';
        $where = array(
            'modUser.date_clear_bonuses:<=' => $daydateend,
            'modUser.date_clear_bonuses:>=' => $daydatebegin,
            'modUserProfile.email:!=' => '',
            'modUser.bonuses:>' => 0

        );
        $q->select(array(
            'modUserProfile.email',
            'modUser.id',
        ));
        $q->where($where);
        $q->prepare();
        $sql = $q->toSQL();
//        echo '$sql'.$sql;
//        die();

        $q->stmt->execute();
        $result = $q->stmt->fetchAll(PDO::FETCH_ASSOC);
        echo '<pre>$result' . $daydateend . ' ' . $daydatebegin;
        print_r($result);
        echo '</pre>';
        foreach ($result as $_user) {
            $model_user = $this->modx->getObject('modUser', $_user['id']);
            if ($model_user) {
                $model_user->set('bonuses', 0);
                $model_user->set('date_clear_bonuses', NULL);
                //$model_user->set('date_clear_bonuses', date('Y-m-d H:i:s'));
                $model_user->save();
                //$date_zero=date("Y-m-d");
                $date_zero = date('Y-m-d H:i:s');
                $this->saveLogBonuses(['amount' => 0, 'user_id' => $_user['id'], 'date_zero' => $date_zero, 'reason' => 'Обнуление бонусов']);

            }

        }
        return $result;
    }

    public function getuser_before_zero_bonus()
    {

        $q = $this->modx->newQuery('modUser');
        $q->innerJoin('modUserProfile', 'modUserProfile', 'modUser.id=modUserProfile.internalKey');

        $day45 = 86400 * 48;// за 3 дня
        $daydate = date("Y-m-d", strtotime('+3 days'));//ime() - $day45);    // H:m:s

        //$daydate='2018-10-14';
        echo '$daydate = ' . $daydate . "\r\n\r\n";
        $daydatebegin = $daydate . ' 00:00:00';
        $daydateend = $daydate . ' 23:59:59';
        $where = array(
            'modUser.date_clear_bonuses:<=' => $daydateend,
            'modUser.date_clear_bonuses:>=' => $daydatebegin,
            //'modUserProfile.email:!=' => '',
            'modUser.bonuses:>' => 0
        );
        $q->select(array(
            'modUserProfile.email',
            'modUser.id',

        ));
        $q->where($where);
        $q->prepare();
        $sql = $q->toSQL();
        $q->stmt->execute();
        $result = $q->stmt->fetchAll(PDO::FETCH_ASSOC);
        echo '<pre>$result' . $daydateend . ' ' . $daydatebegin;
        print_r($result);
        echo '</pre>';
        foreach ($result as $_user) {
            $model_user = $this->modx->getObject('modUser', $_user['id']);
            echo 'user_id' . $_user['id'] . '<br/>';
            if ($model_user) {
                $profile = $model_user->getOne('Profile');
                //echo $profile->;
                $fullname = $profile->get('fullname');
                $bonus_total = $model_user->get('bonuses');
                $date_burn = $model_user->get('date_clear_bonuses');
                $this->sendEmailBonusesAttention($model_user, $bonus_total, $date_burn);

                /*
                            $phone = $user['phone'];
                            $text = $modx->getChunk('smsMore45', $data);
                            if ($miniShop2->sendsms($phone, $text))
                            {
                                if ($miniShop2->updatesubmit45($user['id'])) echo 'update submite mail<br />';
                            }*/
            }
        }
        return $result;
    }

    public function getusersmore30()
    {
        /*$today = time();
        $moon = mktime(0, 0, 0, 2, 15, 2016);
        $days=floor(($today - $moon) / 86400);	*/
        $q = $this->modx->newQuery('modUser');
        $q->innerJoin('modUserProfile', 'modUserProfile', 'modUser.id=modUserProfile.internalKey');

        $day45 = 86400 * 30;
        $daydate = date("Y-m-d", time() - $day45);    // H:m:s
        //$daydate = date("Y-m-d", time() - $day45);    // H:m:s
        $daydatebegin = $daydate . ' 00:00:00';
        $daydateend = $daydate . ' 23:59:59';
        $where = array(
            'modUser.last_order:!=' => '',
            'modUser.last_order:!=' => NULL,
            'modUser.last_order:<=' => $daydateend,
            'modUser.last_order:>=' => $daydatebegin,
            //'modUserProfile.email:!=' => '',
            'modUser.mailmore30:=' => 0,
        );
        $q->select(array(
            'modUserProfile.email',
            'modUserProfile.phone',

            'modUser.last_order',
        ));
        $q->where($where);
        $q->prepare();
        $sql = $q->toSQL();
        $q->stmt->execute();
        $result = $q->stmt->fetchAll(PDO::FETCH_ASSOC);
        return $result;
    }

    public function getusersmore45()
    {
        /*$today = time();
        $moon = mktime(0, 0, 0, 2, 15, 2016);
        $days=floor(($today - $moon) / 86400);	*/
        $q = $this->modx->newQuery('modUser');
        $q->innerJoin('modUserProfile', 'modUserProfile', 'modUser.id=modUserProfile.internalKey');

        $day45 = 86400 * 45;
        $daydate = date("Y-m-d", time() - $day45);    // H:m:s
        //$daydate = date("Y-m-d", time() - $day45);    // H:m:s
        $daydatebegin = $daydate . ' 00:00:00';
        $daydateend = $daydate . ' 23:59:59';
        $where = array(
            'modUser.last_order:!=' => '',
            'modUser.last_order:!=' => NULL,
            'modUser.last_order:<=' => $daydateend,
            'modUser.last_order:>=' => $daydatebegin,
            //'modUserProfile.email:!=' => '',
            'modUser.mailmore45:=' => 0,
        );
        $q->select(array(
            'modUserProfile.email',
            'modUserProfile.phone',

            'modUser.last_order',
        ));
        $q->where($where);
        $q->prepare();
        $sql = $q->toSQL();
        $q->stmt->execute();
        $result = $q->stmt->fetchAll(PDO::FETCH_ASSOC);
        return $result;
    }


    public function getNotificationByType($type, $typeSend = '')
    {
        $q = $this->modx->newQuery('msNotifications');
        //$q->innerJoin('modUserProfile', 'modUserProfile', 'modUser.id=modUserProfile.internalKey');

        $where = array(
            'msNotifications.type:=' => $type
        );
        if ($typeSend != '') {
            switch ($typeSend) {
                case 'email':
                    $where['email:='] = 1;
                    break;
                case 'viber':
                    $where['viber:='] = 1;
                    break;
                case 'sms':
                    $where['sms:='] = 1;
                    break;
            }

        }
        $q->select('*');

        $q->where($where);
        $q->prepare();
        $sql = $q->toSQL();
        $q->stmt->execute();
        $result = $q->stmt->fetch(PDO::FETCH_ASSOC);

        return $result;


    }

    public function getAllTypeNotification()
    {

    }


    public function sendsms($phone, $text, $image_url = false, $type = 'gibrid', $caption = '', $action = '', $textSms = '', $phoneSms = '', $user = false, $is_transactional = 0, $ttl = false)
    {
        if ($phoneSms != '')
            $phone = $phoneSms;

        // Все данные возвращаются в кодировке UTF-8
        //header('Content-type: text/html; charset=utf-8');

        $turbosms_api = $this->modx->getOption('turbosms_api');
        $turbosms_link = 'https://api.turbosms.ua/message/send.json';

        $ch = curl_init($turbosms_link);

        $post = [];

        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            'Content-Type: application/x-www-form-urlencoded',
            'Authorization: Basic ' . $turbosms_api
        ));
//'Content-Type: '.($this->format == 'xml' ? 'text/xml' : 'application/json'

        $post['viber']['image_url'] = 'https://petchoice.ua/assets/images/logo.png';

        $sender = 'PetChoice';//mobibon';
        $senderSms = 'PetChoice';

        switch ($type) {
            case 'gibrid':

                if ($this->modx->getOption('debug_notification')) {
                    $phone = $this->modx->getOption('debug_notification_phone');
                }
                if (strpos($phone, ',') !== false) {

                    $post['recipients'] = explode(',', $phone);
//                    echo '<pre>recipients';
//                    print_r($post['recipients']);
//                    echo '</pre>';
                } else {
                    $post['recipients'] = [
                        $phone
                    ];
                }

                $post['sms'] = [
                    'sender' => $senderSms,//$sender,
                    'text' => ($textSms != '' ? $textSms : $text)
                ];
                $post['viber'] = [
                    'sender' => $sender,
                    'text' => $text,
                    //'caption'=>$caption,
                    //'action'=>$action,
                    'count_clicks' => 1,
                    'is_transactional' => $is_transactional
                ];

                if ($ttl) {
                    $post['viber']['ttl'] = $ttl;
                }
                if (($action != '') && ($caption != '')) {
                    $post['viber']['action'] = $action;
                    $post['viber']['caption'] = $caption;
                }
                if ($image_url) {
                    $post['viber']['image_url'] = 'https://petchoice.ua' . $image_url;
                }
                //$post['sms']=[];
                break;
            case 'sms':
                $post['recipients'] = [
                    $phone
                ];
                $post['sms'] = [
                    'sender' => $senderSms,
                    'text' => ($textSms != '' ? $textSms : $text)
                ];
                break;
            case 'viber':
                if ($this->modx->getOption('debug_notification')) {
                    $phone = $this->modx->getOption('debug_notification_phone');
                }
                if (strpos($phone, ',') !== false) {

                    $post['recipients'] = explode(',', $phone);
//                    echo '<pre>recipients';
//                    print_r($post['recipients']);
//                    echo '</pre>';
                } else {
                    $post['recipients'] = [
                        $phone
                    ];
                }


                $post['viber'] = [
                    'sender' => $sender,
                    'text' => $text,
                    //'caption'=>$caption,
                    //'action'=>$action,
                    //'count_clicks'=>1
                ];

                if ($ttl) {
                    $post['viber']['ttl'] = $ttl;
                }

                if (($action != '') && ($caption != '')) {
                    $post['viber']['action'] = $action;
                    $post['viber']['caption'] = $caption;
                }
                $post['viber']['is_transactional'] = $is_transactional;

//                $post['sms'] = [
//                    'sender' => $senderSms,//$sender,
//                    'text' => ($textSms != '' ? $textSms : $text)
//                ];

                if ($image_url) {
                    $post['viber']['image_url'] = 'https://petchoice.ua' . $image_url;
                }
                break;

        }
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 5);
        curl_setopt($ch, CURLOPT_TIMEOUT, 10);

        curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($post));
        $result = curl_exec($ch);

        $resultArray = json_decode($result, true);
//        echo '<pre>';
//        print_r($post);
//        echo '</pre>';
//        echo '<pre>';
//        print_r($resultArray);
//        echo '</pre>';


        if (isset($resultArray['response_code'])) {
            if (($type == 'gibrid') && ($resultArray['response_code'] == '404')) {
                if ($user) {
                    $user->set('viber_status', 0);
                    $user->save();
                }
                return $this->sendsms($phone, $text, $image_url, 'sms', '', '', $textSms);
            }
        }

        curl_close($ch);

        return $resultArray;


    }

    /**
     * @param modX $modx
     * @param array $config
     */
    function __construct(modX &$modx, array $config = array())
    {
        $this->modx =& $modx;
        $this->modx->lexicon->load('minishop2:cart');
        $this->modx->lexicon->load('poylang:site');

        $corePath = $this->modx->getOption('minishop2.core_path', $config, $this->modx->getOption('core_path') . 'components/minishop2/');
        $assetsPath = $this->modx->getOption('minishop2.assets_path', $config, $this->modx->getOption('assets_path') . 'components/minishop2/');
        $assetsUrl = $this->modx->getOption('minishop2.assets_url', $config, $this->modx->getOption('assets_url') . 'components/minishop2/');
        $actionUrl = $this->modx->getOption('minishop2.action_url', $config, $assetsUrl . 'action.php');
        $connectorUrl = $assetsUrl . 'connector.php';


        $amountNalosh = $this->modx->getOption('nalosh_amount_customer');// $this->modx->lexicon($this->modx->getOption('nalosh_amount_customer'));

        $textNalosh = $this->modx->lexicon($this->modx->getOption('desc_payment_nalosh'), ['amount' => $amountNalosh]);

        $textNaloshBeforeAmount = $this->modx->lexicon($this->modx->getOption('nalosh_not_allow'), ['amount' => $amountNalosh]);
        $naloshPay = $this->modx->lexicon($this->modx->getOption('nalosh_pay'), ['amount' => $amountNalosh]);


        $this->config = array_merge(array(
            'assetsUrl' => $assetsUrl
        , 'cssUrl' => $assetsUrl . 'css/'
        , 'jsUrl' => $assetsUrl . 'js/'
        , 'jsPath' => $assetsPath . 'js/'
        , 'imagesUrl' => $assetsUrl . 'images/'
        , 'customPath' => $corePath . 'custom/'

        , 'textNalosh' => $textNalosh
        , 'naloshPay' => $naloshPay
        , 'amountNalosh' => $amountNalosh
        , 'textNaloshBeforeAmount' => $textNaloshBeforeAmount

        , 'connectorUrl' => $connectorUrl
        , 'actionUrl' => $actionUrl

        , 'corePath' => $corePath
        , 'assetsPath' => $assetsPath
        , 'modelPath' => $corePath . 'model/'
        , 'ctx' => 'web'
        , 'json_response' => false

        , 'templatesPath' => $corePath . 'elements/templates/'
        ), $config);

        $this->modx->addPackage('minishop2', $this->config['modelPath']);
        $this->modx->lexicon->load('minishop2:default');
    }


    /**
     * Initializes component into different contexts.
     *
     * @param string $ctx The context to load. Defaults to web.
     * @param array $scriptProperties Properties for initialization.
     *
     * @return bool
     */
    public function initialize($ctx = 'web', $scriptProperties = array())
    {
        $this->config = array_merge($this->config, $scriptProperties);
        $this->config['ctx'] = $ctx;

        $this->modx->lexicon->load('minishop2:cart');
        $this->modx->lexicon->load('poylang:site');
        if (!empty($this->initialized[$ctx])) {
            return true;
        }
        switch ($ctx) {
            case 'mgr':
                break;
            default:
                if (!defined('MODX_API_MODE') || !MODX_API_MODE) {
                    $config = $this->makePlaceholders($this->config);
                    if ($css = $this->modx->getOption('ms2_frontend_css')) {
                        //$this->modx->regClientCSS(str_replace($config['pl'], $config['vl'], $css));
                    }


                    $amountNalosh = $this->modx->getOption('nalosh_amount_customer');//$this->modx->lexicon($this->modx->getOption('nalosh_amount_customer'));
                    $textNalosh = $this->modx->lexicon($this->modx->getOption('desc_payment_nalosh'), ['amount' => $amountNalosh]);//$this->modx->getOption('desc_payment_nalosh');

                    $textNaloshBeforeAmount = $this->modx->lexicon($this->modx->getOption('nalosh_not_allow'), ['amount' => $amountNalosh]);
                    $naloshPay = $this->modx->lexicon($this->modx->getOption('nalosh_pay'), ['amount' => $amountNalosh]);


//                    $textNaloshBeforeAmount = $this->modx->lexicon($this->modx->getOption('nalosh_not_allow'));
//                    $naloshPay =  $this->modx->lexicon($this->modx->getOption('nalosh_pay'));//$this->modx->getOption('nalosh_pay');

                    $config_js = preg_replace(array('/^\n/', '/\t{5}/'), '', '
					miniShop2 = {};
					miniShop2Config = {
						cssUrl: "' . $this->config['cssUrl'] . 'web/"
						,jsUrl: "' . $this->config['jsUrl'] . 'web/"
						,imagesUrl: "' . $this->config['imagesUrl'] . 'web/"
						,actionUrl: "' . $this->config['actionUrl'] . '"
						,textNalosh: "' . $textNalosh . '"
                        ,naloshPay: "' . $naloshPay . '"
                        ,amountNalosh: "' . $amountNalosh . '"
                        ,textNaloshBeforeAmount: "' . $textNaloshBeforeAmount . '"
						,ctx: "' . $this->modx->context->get('key') . '"
						,close_all_message: "' . $this->modx->lexicon('ms2_message_close_all') . '"
						,price_format: ' . $this->modx->getOption('ms2_price_format', null, '[2, ".", " "]') . '
						,price_format_no_zeros: ' . $this->modx->getOption('ms2_price_format_no_zeros', null, true) . '
						,weight_format: ' . $this->modx->getOption('ms2_weight_format', null, '[3, ".", " "]') . '
						,weight_format_no_zeros: ' . $this->modx->getOption('ms2_weight_format_no_zeros', null, true) . '
						,callbacksObjectTemplate: function() {
							return {
								before: function() {/*return false to prevent send data*/}
								,response: {success: function(response) {},error: function(response) {}}
								,ajax: {done: function(xhr) {},fail: function(xhr) {},always: function(xhr) {}}
							};
						}
					};
					miniShop2.Callbacks = miniShop2Config.Callbacks = {
						Cart: {
							add: miniShop2Config.callbacksObjectTemplate()
							,remove: miniShop2Config.callbacksObjectTemplate()
							,change: miniShop2Config.callbacksObjectTemplate()
							,clean: miniShop2Config.callbacksObjectTemplate()
						}
						,Order: {
							add: miniShop2Config.callbacksObjectTemplate()
							,getcost: miniShop2Config.callbacksObjectTemplate()
							,clean: miniShop2Config.callbacksObjectTemplate()
							,promocode: miniShop2Config.callbacksObjectTemplate()
							,submit: miniShop2Config.callbacksObjectTemplate()
							,getRequired: miniShop2Config.callbacksObjectTemplate()
						}
					};');
                    $this->modx->regClientStartupScript("<script type=\"text/javascript\">\n" . $config_js . "\n</script>", true);
                    if (($_SERVER['SERVER_NAME'] == 'm.petchoice.pp.ua')||($_SERVER['SERVER_NAME'] == 'm.petchoice.ua')) {
                        if ($js = trim($this->modx->getOption('mse2_frontend_m_js'))) {
                            //$this->modx->regClientScript(str_replace($config['pl'], $config['vl'], $js));
                        }
                    } else {
                        if ($js = trim($this->modx->getOption('ms2_frontend_js'))) {
                            if (!empty($js) && preg_match('/\.js/i', $js)) {
                                $this->modx->regClientScript(preg_replace(array('/^\n/', '/\t{7}/'), '', '
								<script type="text/javascript">
									if(typeof jQuery == "undefined") {
										document.write("<script src=\"' . $this->config['jsUrl'] . 'web/lib/jquery.min.js\" type=\"text/javascript\"><\/script>");
									}
								</script>
								'), true);
                                //$this->modx->regClientScript(str_replace($config['pl'], $config['vl'], $js));
                            }
                        }
                    }
                }

                require_once dirname(__FILE__) . '/mscarthandler.class.php';
                $cart_class = $this->modx->getOption('ms2_cart_handler_class', null, 'msCartHandler');
                if ($cart_class != 'msCartHandler') {
                    $this->loadCustomClasses('cart');
                }
                if (!class_exists($cart_class)) {
                    $cart_class = 'msCartHandler';
                }

                $this->cart = new $cart_class($this, $this->config);
                if (!($this->cart instanceof msCartInterface) || $this->cart->initialize($ctx) !== true) {
                    $this->modx->log(modX::LOG_LEVEL_ERROR, 'Could not initialize miniShop2 cart handler class: "' . $cart_class . '"');
                    return false;
                }


                require_once dirname(__FILE__) . '/msorderhandler.class.php';
                $order_class = $this->modx->getOption('ms2_order_handler_class', null, 'msOrderHandler');
                if ($order_class != 'msOrderHandler') {
                    $this->loadCustomClasses('order');
                }
                if (!class_exists($order_class)) {
                    $order_class = 'msOrderHandler';
                }

                $this->order = new $order_class($this, $this->config);
                if (!($this->order instanceof msOrderInterface) || $this->order->initialize($ctx) !== true) {
                    $this->modx->log(modX::LOG_LEVEL_ERROR, 'Could not initialize miniShop2 order handler class: "' . $order_class . '"');
                    return false;
                }

                $this->initialized[$ctx] = true;
                break;
        }
        return true;
    }


    /**
     * Method for transform array to placeholders
     *
     * @var array $array With keys and values
     * @var string $prefix Placeholders prefix
     *
     * @return array $array Two nested arrays With placeholders and values
     */
    public function makePlaceholders(array $array = array(), $prefix = '')
    {
        $result = array('pl' => array(), 'vl' => array());
        foreach ($array as $k => $v) {
            if (is_array($v)) {
                $result = array_merge_recursive($result, $this->makePlaceholders($v, $k . '.'));
            } else {
                $result['pl'][$prefix . $k] = '[[+' . $prefix . $k . ']]';
                $result['vl'][$prefix . $k] = $v;
            }
        }
        return $result;
    }


    /**
     * Method loads custom classes from specified directory
     *
     * @var string $dir Directory for load classes
     *
     * @return void
     */
    public function loadCustomClasses($dir)
    {
        $files = scandir($this->config['customPath'] . $dir);
        foreach ($files as $file) {
            if (preg_match('/.*?\.class\.php$/i', $file)) {
                include_once($this->config['customPath'] . $dir . '/' . $file);
            }
        }
    }


    /**
     * Returns id of current customer. If no exists - register him and returns id.
     *
     * @return integer $id
     */
    public function getCustomerId()
    {
        $order = $this->order->get();
        /*if (empty($order['email'])) {
            return false;
        }*/

        if ($this->modx->user->isAuthenticated()) {
            $profile = $this->modx->user->Profile;
            $email = $_POST['email'];
            if (!$email = $profile->get('email')) {
                $profile->set('email', $email);
                $profile->save();
            }
            $uid = $this->modx->user->id;
        } else {
            /* @var modUser $user */
            $email = $_POST['email'];//$order['email'];
            $phone = $_POST['phone'];//$order['phone'];
            /* if ($_SERVER['REMOTE_ADDR']=='188.130.177.18'){
                 echo '<pre>'.$phone;
                 print_r($_POST);//$order);
                 echo '</pre>';die();
             }*/
            if (isset($order['subscribe'])) {
                $subscribe = '1';//Подписан';//$order['subscribe'];
            } else $subscribe = '0';//Не подписан';
            if ($user = $this->modx->getObject('modUser', array('username' => $phone))) {

                $uid = $user->get('id');
                /*   if ($_SERVER['REMOTE_ADDR']=='188.130.177.18'){
                       echo '<pre>'.$uid;
                       print_r($_POST);//$order);
                       echo '</pre>';
                       die();
                   }*/
                /*$extended = array(
                    'room' => $_POST['extended']['room'],
                    'house' => $_POST['extended']['house'],
                    'street' => $_POST['extended']['street'],
                    'city_id' => $_POST['extended']['city_id'],
                    'payment' => $_POST['payments'],
                    'delivery' => $_POST['delivery'],
                    'warehouse' => $_POST['extended']['warehouse'],
                    'doorphone' => $_POST['extended']['doorphone'],
                    'housing' => $_POST['extended']['housing'],
                    'parade' => $_POST['extended']['parade'],
                    'floor' => $_POST['extended']['floor']
                );*/

                $profile = $this->modx->getObject('modUserProfile', ['internalKey' => $uid]);
                if ($profile) {
                    $oldextended = $profile->get('extended');

                    //if (isset($_POST['lastname'])&&($_POST['lastname']!='')) $oldextended['lastname'] = $_POST['lastname'];
                    if (isset($_POST['extended']['room']) && ($_POST['extended']['room'] != '')) $oldextended['room'] = $_POST['extended']['room'];
                    if (isset($_POST['extended']['house']) && ($_POST['extended']['house'] != '')) $oldextended['house'] = $_POST['extended']['house'];
                    if (isset($_POST['extended']['street']) && ($_POST['extended']['street'] != '')) $oldextended['street'] = $_POST['extended']['street'];
                    if (isset($_POST['extended']['city_id']) && ($_POST['extended']['city_id'] != '')) $oldextended['city_id'] = $_POST['extended']['city_id'];
                    if (isset($_POST['payments']) && ($_POST['payments'] != '')) $oldextended['payments'] = $_POST['payments'];
                    if (isset($_POST['delivery']) && ($_POST['delivery'] != '')) $oldextended['delivery'] = $_POST['delivery'];
                    if (isset($_POST['extended']['warehouse']) && ($_POST['extended']['warehouse'] != '')) $oldextended['warehouse'] = $_POST['extended']['warehouse'];
                    if (isset($_POST['extended']['doorphone']) && ($_POST['extended']['doorphone'] != '')) $oldextended['doorphone'] = $_POST['extended']['doorphone'];
                    if (isset($_POST['extended']['housing']) && ($_POST['extended']['housing'] != '')) $oldextended['housing'] = $_POST['extended']['housing'];
                    if (isset($_POST['extended']['parade']) && ($_POST['extended']['parade'] != '')) $oldextended['parade'] = $_POST['extended']['parade'];
                    if (isset($_POST['extended']['floor']) && ($_POST['extended']['floor'] != '')) $oldextended['floor'] = $_POST['extended']['floor'];

                    if (isset($_POST['fathername']) && ($_POST['fathername'] != '')) $oldextended['fathername'] = $_POST['fathername'];

                    $extended = $this->modx->toJSON($oldextended);
                    if (empty($profile->get('email')))
                        $profile->set('email', $email);
                    // $profile->set('email', $email);
                    $profile->set('extended', $extended);
                    if ($order['city'] != '') $profile->set('city', $order['city']);
                    // $profile->set('phone', $phone);
                    //$profile->set('fullname', $order['receiver']);
                    $profile->save();
                }
            } elseif ($profile = $this->modx->getObject('modUserProfile', array('mobilephone' => $phone))) {
                $uid = $profile->get('internalKey');
                $oldextended = $profile->get('extended');
                //$oldextended=$this->modx->fromJSON($oldextended);

                /* if ($_POST['lastname']!='') $oldextended['lastname'] = $_POST['lastname'];
                 if ($_POST['room']!='') $oldextended['room'] = $_POST['room'];
                 if ($_POST['house']!='') $oldextended['house'] = $_POST['house'];
                 if ($_POST['street']!='') $oldextended['street'] = $_POST['street'];
                 if ($_POST['city_id']!='') $oldextended['city_id'] = $_POST['city_id'];
                 if ($_POST['payments']!='') $oldextended['payments'] = $_POST['payments'];
                 if ($_POST['delivery']!='') $oldextended['delivery'] = $_POST['delivery'];
                 if ($_POST['warehouse']!='') $oldextended['warehouse'] = $_POST['warehouse'];
                 if ($_POST['doorphone']!='') $oldextended['doorphone'] = $_POST['doorphone'];
                 if ($_POST['housing']!='') $oldextended['housing'] = $_POST['housing'];
                 if ($_POST['parade']!='') $oldextended['parade'] = $_POST['parade'];
                 if ($_POST['floor']!='') $oldextended['floor'] = $_POST['floor'];
 */
                //if (isset($_POST['lastname'])&&($_POST['lastname']!='')) $oldextended['lastname'] = $_POST['lastname'];
                if (isset($_POST['extended']['room']) && ($_POST['extended']['room'] != '')) $oldextended['room'] = $_POST['extended']['room'];
                if (isset($_POST['extended']['house']) && ($_POST['extended']['house'] != '')) $oldextended['house'] = $_POST['extended']['house'];
                if (isset($_POST['extended']['street']) && ($_POST['extended']['street'] != '')) $oldextended['street'] = $_POST['extended']['street'];
                if (isset($_POST['extended']['city_id']) && ($_POST['extended']['city_id'] != '')) $oldextended['city_id'] = $_POST['extended']['city_id'];
                if (isset($_POST['payments']) && ($_POST['payments'] != '')) $oldextended['payments'] = $_POST['payments'];
                if (isset($_POST['delivery']) && ($_POST['delivery'] != '')) $oldextended['delivery'] = $_POST['delivery'];
                if (isset($_POST['extended']['warehouse']) && ($_POST['extended']['warehouse'] != '')) $oldextended['warehouse'] = $_POST['extended']['warehouse'];
                if (isset($_POST['extended']['doorphone']) && ($_POST['extended']['doorphone'] != '')) $oldextended['doorphone'] = $_POST['extended']['doorphone'];
                if (isset($_POST['extended']['housing']) && ($_POST['extended']['housing'] != '')) $oldextended['housing'] = $_POST['extended']['housing'];
                if (isset($_POST['extended']['parade']) && ($_POST['extended']['parade'] != '')) $oldextended['parade'] = $_POST['extended']['parade'];
                if (isset($_POST['extended']['floor']) && ($_POST['extended']['floor'] != '')) $oldextended['floor'] = $_POST['extended']['floor'];

                if (isset($_POST['fathername']) && ($_POST['fathername'] != '')) $oldextended['fathername'] = $_POST['fathername'];


                $extended = $this->modx->toJSON($oldextended);
                if (empty($profile->get('email')))
                    $profile->set('email', $email);

                $profile->set('extended', $extended);
                if ($order['city'] != '') $profile->set('city', $order['city']);
                // $profile->set('phone', $phone);
                //$profile->set('fullname', $order['receiver']);
                $profile->save();


            } else {


                /*$user = $this->modx->getObject('modUser', array('username' => $phone,'registr'=>0));
                if ($user)
                {
                    $uid = $user->get('id');
                }
                else{*/
                $user = $this->modx->newObject('modUser', array(
                    'username' => $phone,
                    'subscribe' => $subscribe,
                    'registr' => 0,
                    'created' => date('Y-m-d H:i:s'),
                    'password' => md5(rand())));
                $extended = array(
                    'lastname' => $_POST['lastname'],
                    'fathername' => (isset($_POST['fathername']) ? $_POST['fathername'] : ''),
                    'room' => $_POST['extended']['room'],
                    'house' => $_POST['extended']['house'],
                    'street' => $_POST['extended']['street'],
                    'city_id' => $_POST['extended']['city_id'],
                    'payment' => $_POST['payments'],
                    'delivery' => $_POST['delivery'],
                    'warehouse' => $_POST['extended']['warehouse'],
                    'doorphone' => $_POST['extended']['doorphone'],
                    'housing' => $_POST['extended']['housing'],
                    'parade' => $_POST['extended']['parade'],
                    'floor' => $_POST['extended']['floor']
                );

                $extended = $this->modx->toJSON($extended);
                $profile = $this->modx->newObject('modUserProfile', array(
                    'email' => $email,
                    'extended' => $extended,
                    'city' => $order['city'],
                    'phone' => $phone,
                    'fullname' => $order['receiver'],
                    'lastname' => $_POST['lastname']
                ));

                $user->addOne($profile);
                if ($user->save()) {

                    if ($groups = $this->modx->getOption('ms2_order_user_groups', null, false)) {
                        $groups = array_map('trim', explode(',', $groups));
                        foreach ($groups as $group) {
                            $user->joinGroup($group);
                        }
                    }
                    $uid = $user->get('id');
                } else $uid = false;
            }
        }

        return $uid;
    }


    public function sendmailclosereview($user, $profile)
    {
        // send mail about review shop

        $type = 8;

        $data = array_merge($user, $profile);
        $userObject = $this->modx->getObject('modUser', $user['id']);
        $this->eventNotification($type, $userObject, $data);
//        if ($this->eventNotification($type, $user, $data)) {
//            $chunk = 'mailFeedbackPetchoice';
//            $body = $this->modx->getChunk($chunk, array_merge($user, $profile));
//            $body = 'Почта: ' . $email . '. Шаблон письма:  ' . $body;
//            $this->sendEmail('petchoice.info@gmail.com', $subject, $body);
//        }
        /*$chunk = 'mailFeedbackPetchoice';
        $body = $this->modx->getChunk($chunk, array_merge($user, $profile));
        $subject = 'Оставьте отзыв о PetChoice, получите скидку';
        $email = $profile['email'];
        if ($email && ($email != '')) {
            $this->sendEmail($email, $subject, $body);
            $body = 'Почта: ' . $email . '. Шаблон письма:  ' . $body;
            $this->sendEmail('petchoice.info@gmail.com', $subject, $body);
        }*/
    }


    public function changebonuses($order, $new_bonuses)
    {
        $with_bonuses = $order->get('with_bonuses');
        if (($order->get('apply_bonuses') != '1') && ($new_bonuses > 0)) {
            $order->set('bonuses', $new_bonuses);
            $order->set('with_bonuses', 1);
            $bonuses = $new_bonuses;//$order->get('bonuses');
            $cart_cost = $order->get('cart_cost');
            $cost = $order->get('cost');
            $cart_cost_after_bonuses = $cart_cost;
            $cost_after_bonuses = $cost;

            $cart_cost_after_bonuses = $cart_cost_after_bonuses - $bonuses;
            $cost_after_bonuses = $cost_after_bonuses - $bonuses;

            //$order->set('cost_without_bonuses', $cart_cost_after_bonuses);
            $order->set('apply_bonuses', 1);

            $date_apply_bonuses = date('Y-m-d H:i:s');
            $order->set('date_apply_bonuses', $date_apply_bonuses);

            //$order->set('cart_cost', $cost_after_bonuses);
            //$order->set('cost', $cost_after_bonuses);

            $user = $order->getOne('User');
            $bonuses_user = $user->get('bonuses');
            $bonuses_user = $bonuses_user - $bonuses;
            $user->set('bonuses', $bonuses_user);
            if ($user->save()) {
                if ($bonuses > 0)
                    $this->saveLogBonuses(['amount' => '-' . $bonuses, 'order_id' => $order->get('id'), 'user_id' => $user->get('id')]);
            }

        }
        return $order;
    }


    public function charge_bonuses($order)
    {
        $products = $order->getMany('Products');
        $bonuses_user = 0;
        $user = $order->getOne('User');
        if ($user) {
            $registr = $user->get('registr');
            if ($registr == '1') {
                foreach ($products as $pr_item) {
                    $count = $pr_item->get('count');
                    $product_id = $pr_item->get('product_id');
                    $options_ar = $pr_item->get('options');
                    $size = $options_ar['size'];
                    $q = $this->modx->newQuery('msProduct');
                    $q->select(array(
                        'TV.*'
                    ));
                    $q->innerJoin('modTemplateVarResource', 'TV', 'msProduct.id = TV.contentid AND TV.tmplvarid= 1');
                    $q->where(array('TV.contentid' => $product_id));

                    //$q->where();
                    $q->prepare();
                    $q->stmt->execute();
                    $options_result = $q->stmt->fetchAll(PDO::FETCH_ASSOC);

                    foreach ($options_result as $opt) {
                        $value_opt = $opt['value'];
                        $value_opt_ar = $this->modx->fromJSON($value_opt);
                        foreach ($value_opt_ar as $v_opt) {
                            $bonuses = 0;
                            $weight_opt = $v_opt['weight'] . ' ' . $v_opt['weight_prefix'];

                            if ($weight_opt == $size) {
                                if (isset($v_opt['bonuses'])) {
                                    $bonuses = $count * $v_opt['bonuses'];
                                    $bonuses_user = $bonuses_user + $bonuses;
                                }
                            }
                        }
                    }
                }

                $bonuses = $user->get('bonuses') + $bonuses_user;

                $date_burn = date('Y-m-d H:i:s', strtotime('+ 45 day'));

                $user->set('date_clear_bonuses', $date_burn);
                $user->set('bonuses', $bonuses);
                if ($user->save()) {
                    // записать в историю бонусов
                    //$bonuses_user
                    if ($bonuses_user > 0) {

                        // отправить письмо про начисление бонусов
                        $profile = $user->getOne('Profile');;
                        $this->sendEmailBonuses($profile, $bonuses_user, $bonuses, $date_burn);
                        $this->saveLogBonuses(['amount' => '+' . $bonuses_user, 'order_id' => $order->get('id'), 'user_id' => $user->get('id')]);
                    }
                }
            }
        }
    }

    public function sendEmailBonusesAttention($user, $amount_bonuses_total, $date_burn)
    {

        $profile = $user->getOne('Profile');
        $fullname = $profile->get('fullname');
        $phone = $profile->get('phone');
        $email = $profile->get('email');
        echo $fullname . ' ' . $phone . ' ' . $email . "\r\n\r\n";

//        $chunk = 'email_alert_burn_bonus';
//        $body_mail = $this->modx->getChunk($chunk, array(
//            'bonus_total' => $amount_bonuses_total,
//            'bonus_date_burn' => $date_burn,
//            'fullname' => $fullname
//        ));

//        echo $date_burn . ' ' . $amount_bonuses_total . ' ' . $fullname . '<br/>';
//
//        $subject = 'Через 3 дня ваши бонусы сгорят';//Вам начислено ' . $counts_text;
//
//        if ($email != '') {
//            if ($this->sendEmail($email, $subject, $body_mail)) {
//                $body_mail = 'Почта: ' . $email . 'Шаблон: </br></br>' . $body_mail;
//                if ($this->sendEmail('petchoice.info@gmail.com', $subject, $body_mail)) {
//                }
//            }
//        }

        if ($phone != '') {
            // send sms
            $this->eventNotification(4, $user, [
                'bonus_total' => $amount_bonuses_total,
                'bonus_date_burn' => $date_burn,
                'fullname' => $fullname
            ]);
//            $chunk_sms = 'sms_alert_burn_bonus';
//            $text = $this->modx->getChunk($chunk_sms, array(
//                'bonus_total' => $amount_bonuses_total,
//                'bonus_date_burn' => $date_burn,
//                'fullname' => $fullname
//            ));
//            $this->sendsms($phone, $text);
        }

    }


    public function sendEmailBonuses($user, $amount_bonuses_now, $amount_bonuses_total, $date_burn)
    {
        $fullname = $user->get('fullname');
        $email = $user->get('email');

        $chunk = 'email_bonus_accrual';
        $body_mail = $this->modx->getChunk($chunk, array(
            'bonus_now' => $amount_bonuses_now,
            'bonus_total' => $amount_bonuses_total,
            'bonus_date_burn' => $date_burn,
            'fullname' => $fullname
        ));

        $counts_text = $amount_bonuses_now . ' ' . $this->plural_form($amount_bonuses_now, ['бонус', 'бонуса', 'бонусов']);

        $subject = 'Вам начислено ' . $counts_text;

        if ($email != '') {
            if ($this->sendEmail($email, $subject, $body_mail)) {
                $body_mail = 'Почта: ' . $email . 'Шаблон: </br></br>' . $body_mail;
                if ($this->sendEmail('petchoice.info@gmail.com', $subject, $body_mail)) {

                }
            }
        }


    }

    /**
     * сохраняем историю бонусов
     * saveLogBonuses
     * amount
     * user_id
     * order_id
     */
    public function saveLogBonuses($data = [])
    {

        $da = [];
        $created = date('Y-m-d H:i:s');
        $da['amount'] = $data['amount'];
        $da['created'] = $created;
        $da['user_id'] = $data['user_id'];
        if (isset($data['order_id']))
            $da['order_id'] = $data['order_id'];

        if (isset($data['manager_id']))
            $da['manager_id'] = $data['manager_id'];

        if (isset($data['ticket_id']))
            $da['ticket_id'] = $data['ticket_id'];

        if (isset($data['reason']))
            $da['reason'] = $data['reason'];
        elseif (isset($data['manager_id'])) {
            $da['reason'] = 'Менеджер начислил';
        } elseif (isset($data['order_id'])) {
            $da['reason'] = 'За заказ';
        } elseif (isset($data['ticket_id'])) {
            $da['reason'] = 'За отзыв';
        } elseif (!isset($data['reason'])) {
            $da['reason'] = 'Другое';
        } else {
            $da['reason'] = $data['reason'];
        }

        if (isset($data['date_zero']))
            $da['date_zero'] = $data['date_zero'];


        /* $log_bonuses->set('amount', $data['amount']);
         if (isset($data['order_id']))
             $log_bonuses->set('order_id', $data['order_id']);

         $log_bonuses->set('user_id', $data['user_id']);
         if (isset($data['manager_id']))
             $log_bonuses->set('manager_id', $data['manager_id']);

         $created = date('Y-m-d H:i:s');
         $log_bonuses->set('created', $created);
         */
        /* $keys = array_keys($da);
         $fields = '`' . implode('`,`', $keys) . '`';
         $placeholders = substr(str_repeat('?,', count($keys)), 0, -1);

         $sql = "INSERT INTO `ms_user_bonuses` ({$fields}) VALUES ({$placeholders});";
         if (!$this->modx->prepare($sql)->execute(array_values($da))) {
             echo '<pre>$sql bonuses';
             print_r($sql);
             echo '</pre>';
             //$this->modx->log(1, print_r($sql, true));
         }*/
        $log_bonuses = $this->modx->newObject('msBonusesLog', $da);
        if ($log_bonuses->save()) {

        }

    }


    public function getUsersForViewedSubmit()
    {
        $q = $this->modx->newQuery('msLastviewed');
        $q->innerJoin('msProductData', 'Data', 'msLastviewed.product_id = Data.id');
        $q->innerJoin('msProduct', 'msProduct', 'msProduct.id = Data.id');

        $where_dop = '';
        $free_hours = date('Y-m-d H:i:s', strtotime('-3 hours'));
        $four_hours = date('Y-m-d H:i:s', strtotime('-4 hours'));
        $where_dop = " AND msLastviewed.date BETWEEN '$four_hours' AND '$free_hours' ";//>='$free_hours' AND ";


        $q->where("user_id!=0 " . $where_dop);//['user_id' => $user_id]);

        $q->select(array('msLastviewed.*'));
        $q->groupBy('msLastviewed.user_id');
        $q->sortBy('msLastviewed.date', 'DESC');
        $q->prepare();

        $q->stmt->execute();
        echo $q->toSQL() . "\r\n\r\n";

        $view_products = $q->stmt->fetchAll(PDO::FETCH_ASSOC);
        $users_for_submit = [];
        foreach ($view_products as $product_viewed) {
            /*echo '<pre>$product_viewed';
            print_r($product_viewed);
            echo '</pre>';
*/

            $user_id = $product_viewed['user_id'];
            if (!isset($users[$user_id])) {
                $q = $this->modx->newQuery('msOrder');
                $where_order = "msOrder.user_id='$user_id' ";
                $date_15_days = date('Y-m-d H:i:s', strtotime('-15 days'));
                $where_order .= " AND msOrder.createdon>='$date_15_days'";
                $q->where($where_order);//array('msOrder.user_id' => $user_id));

                $q->prepare();

                $q->stmt->execute();
                echo $q->toSQL() . "\r\n\r\n";

                $order_user = $q->stmt->fetchAll(PDO::FETCH_ASSOC);
                if (count($order_user) == 0) {


                    $q = $this->modx->newQuery('msLastviewed');
                    $q->innerJoin('msProductData', 'Data', 'msLastviewed.product_id = Data.id');
                    $q->innerJoin('msProduct', 'msProduct', 'msProduct.id = Data.id');

                    $where_dop = '';
                    $free_hours = date('Y-m-d H:i:s', strtotime('-3 hours'));
                    $four_hours = date('Y-m-d H:i:s', strtotime('-4 hours'));
                    $where_dop = " AND msLastviewed.date BETWEEN '$four_hours' AND '$free_hours' ";

                    $q->where("user_id='$user_id' " . $where_dop);//['user_id' => $user_id]);
                    $q->limit(10);
                    $q->select(array('msLastviewed.*'));
                    //$q->groupBy('msLastviewed.user_id');
                    $q->sortBy('msLastviewed.date', 'DESC');
                    $q->prepare();

                    $q->stmt->execute();
                    //echo $q->toSQL()."\r\n\r\n";

                    $view_products = $q->stmt->fetchAll(PDO::FETCH_ASSOC);

                    foreach ($view_products as $pr) {
                        $users_for_submit[$user_id][$pr['product_id']] = $pr['product_id'];
                    }
                }
                $users[$user_id] = $user_id;
            } else {
                if (isset($users_for_submit[$user_id])) {
                    //$users_for_submit[$user_id][$product_viewed['product_id']]=$product_viewed['product_id'];
                }
            }
        }
        if (count($users_for_submit) > 0) {
            //foreach ()
            echo '<pre>$users_for_submit';
            print_r($users_for_submit);
            echo '</pre>';

            $subject = $this->modx->getOption('subject_mailviewed');
            foreach ($users_for_submit as $user_id => $products) {
                $this->submitMailViewed($user_id, $products);

            }

            /* echo '<pre>$users';
             print_r($users);
             echo '</pre>';*/
        }


    }


    public function submitMailViewed($user_id, $products)
    {
        $subject = $this->modx->getOption('subject_mailviewed');

        $chunk_view = 'mailViewed';
        $object_user = $this->modx->getObject('modUser', ['id:=' => $user_id, 'mailsubmitviews:!=' => 1]);
        if ($object_user) {

            $user = $object_user->toArray();

            $profile = $object_user->getOne('Profile')->toArray();


            $email = $profile['email'];


            echo '<pre>' . $email;
            print_r($user);
            echo '</pre>';
            echo '<pre>profile';
            print_r($profile);
            echo '</pre>';

            $data = array();
            $extended = $profile['extended'];
            $extended = $this->modx->fromJSON($extended);

            $lastname = $extended['lastname'];
            $data['fullname'] = $profile['fullname'];

            $personal_goods = $this->getPersonalViewed($products);
            $data['personal_goods'] = implode("\r\n", $personal_goods);


            $type = 9;
            if ($this->eventNotification($type, $object_user, $data)) {
                $this->updatesubmitMailViewed($user['id']);
            }

            /*$body = $this->modx->getChunk($chunk_view, $data);
            //$email='portnovvit@gmail.com';
            if ($this->sendEmail($email, $subject, $body)) {
                $this->sendEmail('petchoice.info@gmail.com', 'отправилось письмо клиенту. Тема: "' . $subject . '"', 'Почта: ' . $email . '. Шаблон письма: ' . $body);
                if ($this->updatesubmitMailViewed($user['id'])) echo 'update submite mail<br />';
                else echo 'error submite mail<br />';
            } else $this->sendEmail('petchoice.info@gmail.com', 'Ошибка отправки письма. Тема: "' . $subject . '"', 'Почта: ' . $email . '. Шаблон письма: ' . $body);
            */
        }
    }

    public function updatesubmitMailViewed($user_id)
    {
        $c = $this->modx->newQuery('modUser');
        $c->command('update');
        $c->set(array(
            'last_mailviews' => date("Y-m-d H:m:s"),
            'mailsubmitviews' => '1',
        ));
        $c->where(array(
            'id' => $user_id,
        ));
        $c->prepare();
        if ($c->stmt->execute()) return true;
        else return false;


    }


    public function getPersonalViewed($products)
    {

        $products_for_mail = [];
        $tpl_action = 'tpl.mailaction';

        $q = $this->modx->newQuery('msProduct');
        $q->innerJoin('msProductData', 'Data', 'msProduct.id = Data.id');

        $q->where(array('msProduct.id:IN' => $products));
        $q->select(array('Data.*', 'msProduct.*'));
        //$q->sortBy('msOrder.createdon', 'DESC');
        $q->groupBy('msProduct.id');


        $no_repeat = [];
        $ids = [];
        $total = 0;
        if ($q->prepare() && $q->stmt->execute()) {
            //echo 'sql_products' . $q->toSQL() . "\r\n\r\n";
            $products_data = $q->stmt->fetchAll(PDO::FETCH_ASSOC);

            foreach ($products_data as $product) {
                $no_repeat[$product['id']] = $product['id'];
                $products_for_mail[$product['id']] = $this->modx->getChunk($tpl_action,
                    [
                        'image' => $product['image'],
                        'old_price' => $product['old_price'],
                        'id' => $product['id'],
                        'price' => $product['price'],
                        'pagetitle' => $product['pagetitle'],
                    ]
                );
                $count_buy++;
            }


        }
        return $products_for_mail;
    }

    /**
     * Switch order status
     *
     * @param integer $order_id The id of msOrder
     * @param integer $status_id The id of msOrderStatus
     *
     * @return boolean|string
     */
    public
    function changeOrderStatus($order_id, $status_id, $new_bonuses = 0, $yes1c=false)
    {

        // This method can be overriden by order class
        if (empty($this->order) || !is_object($this->order)) {
            $ctx = !$this->modx->context->key || $this->modx->context->key == 'mgr' ? 'web' : $this->modx->context->key;
            $this->initialize($ctx);
        }
        if (is_object($this->order) && method_exists($this->order, 'changeOrderStatus')) {

            return $this->order->changeOrderStatus($order_id, $status_id);
        }

        $error = '';
        /* @var msOrder $order */
        if (!$order = $this->modx->getObject('msOrder', $order_id)) {
            $error = 'ms2_err_order_nf';
        }

        /* @var msOrderStatus $status */
        if (!$status = $this->modx->getObject('msOrderStatus', array('id' => $status_id, 'active' => 1))) {
            $error = 'ms2_err_status_nf';
        } /* @var msOrderStatus $old_status */
        else if ($old_status = $this->modx->getObject('msOrderStatus', array('id' => $order->get('status'), 'active' => 1))) {
            if ($old_status->get('final')) {
                $error = 'ms2_err_status_final';
            } else if ($old_status->get('fixed')) {
                if ($status->get('rank') <= $old_status->get('rank')) {
                    $error = 'ms2_err_status_fixed';
                }
            }
        }

        if ($order->get('status') == $status_id) {
            $error = 'ms2_err_status_same';
        }

        if (!empty($error)) {
            return $this->modx->lexicon($error);
        }

        $response = $this->invokeEvent('msOnBeforeChangeOrderStatus', array(
            'order' => $order,
            'status' => $order->get('status')
        ));


        if (!$response['success']) {
            return $response['message'];
        }

        $changebonuses = false;
        if ($status_id == 5)//($order->get('status') != $status_id)&&
        {
            //$this->orderLog($order->get('id'), 'bonuses', $status_id);
            $changebonuses = true;

        }


        $order->set('status', $status_id);


        if ($order->save()) {
            if ($changebonuses) {
                $order = $this->changebonuses($order, $new_bonuses);
                $user_id = $this->modx->user->id;

                if ($order->get('bonuses') > 0) {
                    $action = "bonuses";
                    $entry = 'Применено бонусов: ' . $order->get('bonuses') . ' грн';
                    $log = $this->modx->newObject('msOrderLog', array(
                        'order_id' => $order->get('id')
                    , 'user_id' => $user_id
                    , 'timestamp' => time()
                    , 'action' => $action
                    , 'message' => $entry
                    , 'ip' => $this->modx->request->getClientIp()
                    ));

                    $log->save();
                }
            }
            $this->orderLog($order->get('id'), 'status', $status_id);
            $response = $this->invokeEvent('msOnChangeOrderStatus', array(
                'order' => $order,
                'status' => $status_id
            ));

            if (!$response['success']) {
                return $response['message'];
            }

            /* @var modContext $context */
            if ($context = $this->modx->getObject('modContext', array('key' => $order->get('context')))) {
                $context->prepare(true);
                $lang = $context->getOption('cultureKey');
                $this->modx->setOption('cultureKey', $lang);
                $this->modx->lexicon->load($lang . ':minishop2:default', $lang . ':minishop2:cart');
            }

            $pls = $order->toArray();
            $pls['cost'] = $this->formatPrice($pls['cost']);
            $pls['cart_cost'] = $this->formatPrice($pls['cart_cost']);
            $pls['delivery_cost'] = $this->formatPrice($pls['delivery_cost']);
            $pls['weight'] = $this->formatWeight($pls['weight']);
            $pls['payment_link'] = '';

            if ($payment = $order->getOne('Payment')) {
                if ($class = $payment->get('class')) {
                    $this->loadCustomClasses('payment');
                    if (class_exists($class)) {
                        /* @var msPaymentHandler|PayPal $handler */
                        $handler = new $class($order);
                        if (method_exists($handler, 'getPaymentLink')) {
                            $link = $handler->getPaymentLink($order);
                            $pls['payment_link'] = $this->modx->lexicon('ms2_payment_link', array('link' => $link));
                        }
                    }
                }
            }

            /*if ($_SERVER['HTTP_CF_CONNECTING_IP']=='78.46.62.217'){
                echo '<pre>$response2233';
                print_R($response);
                echo '</pre>';
                die();
            }
*/

            if ($status->get('email_manager')) {
                $subject = '';
                if ($chunk = $this->modx->newObject('modChunk', array('snippet' => $status->get('subject_manager')))) {
                    $chunk->setCacheable(false);
                    $subject = $this->processTags($chunk->process($pls));
                }
                $body = 'no chunk set';
                if ($chunk = $this->modx->getObject('modChunk', $status->get('body_manager'))) {
                    $chunk->setCacheable(false);
                    $body = $this->processTags($chunk->process($pls));
                }


                $emails = array_map('trim', explode(',', $this->modx->getOption('ms2_email_manager', null, $this->modx->getOption('emailsender'))));
                if (!empty($subject)) {
                    foreach ($emails as $email) {
                        if (preg_match('/^[^@а-яА-Я]+@[^@а-яА-Я]+(?<!\.)\.[^\.а-яА-Я]{2,}$/m', $email)) {
                            $this->sendEmail($email, $subject, $body);
                        }
                    }
                }
            }

            if ($status->get('email_user')) {

                if ($profile = $this->modx->getObject('modUserProfile', array('internalKey' => $order->get('user_id')))) {
                    $pls['fullname'] = $profile->get('fullname');
                    $subject = '';
                    $body = 'no chunk set';
                    $subject = 'Заказ принят ';//$this->processTags($chunk->process($pls));
                    $body = 'Заказ принят';//$this->processTags($chunk->process($pls));

                    if ($chunk = $this->modx->newObject('modChunk', array('snippet' => $status->get('subject_user')))) {
                        $chunk->setCacheable(false);
                        $subject = $this->processTags($chunk->process($pls));
                    }


                    if ($chunk = $this->modx->getObject('modChunk', $status->get('body_user'))) {
                        $chunk->setCacheable(false);
                        /* if ($_SERVER['HTTP_CF_CONNECTING_IP']=='78.46.62.217'){
                             echo '<pre>$pls';
                             print_r($pls);
                             echo '</pre>';
                         }*/
                        $body = $this->processTags($chunk->process($pls));//$chunk->process($pls);//
                        //'Заказ принят '.$order->get('id');//
                    }


                    $email = $profile->get('email');
                    if (!empty($subject) && preg_match('/^[^@а-яА-Я]+@[^@а-яА-Я]+(?<!\.)\.[^\.а-яА-Я]{2,}$/m', $email)) {
                        //echo $email.' '.$subject.' '.$body;
                        $this->sendEmail($email, $subject, $body);
                    }
                }
            }


            //echo '$status_id = '.$status_id.' payment = '.$order->get('payment');
            if (($order->get('payment') == 4) && ($status_id == 5)&& !$yes1c) {
                $user_id = $order->get('user_id');
                $user = $this->modx->getObject('modUser', $user_id);
                $user_id = $user->get('id');
                $profile = $user->getOne('Profile');
                $extended = $profile->get('extended');
                $user_discount = $extended['discount'];

                $private_key = $this->modx->getOption('privat_keyliqpay');//'vQP1hC3r1nWHN2nhAwfLUDFhorbm1epVtZgwo30h';
                $public_key = $this->modx->getOption('public_keyliqpay');//'i63764050824';

                $amount = $order->get('cost');
                $order_id = $order->get('num');

                $currency = 'UAH';

                $description = 'Order #' . $order_id;
                $result_url = 'https://petchoice.ua/';
                $server_url = 'https://petchoice.ua/assets/components/minishop2/getorder.php?action=payliqpay';

                $type = 'buy';

                $version = '3';
                $language = 'ru';

                $data = base64_encode(
                    json_encode(
                        array('version' => $version,
                            'public_key' => $public_key,
                            'action' => 'pay',
                            'amount' => $amount,
                            'currency' => $currency,
                            'description' => $description,
                            'order_id' => $order_id,
                            'type' => $type,
                            'sandbox' => 0,
                            'result_url' => $result_url,
                            'server_url' => $server_url,
                            'language' => $language)
                    )
                );

                $signature = base64_encode(sha1($private_key . $data . $private_key, 1));
                $link = 'https://www.liqpay.ua/api/3/checkout?data=' . $data . '&signature=' . $signature;


                $link = $this->createShortUrl($link);

                $dataprofile = $user->getOne('Profile')->toArray();
                $data = array_merge(
                    $dataprofile,//$user->getOne('Profile')->toArray(),
                    $user->toArray(),
                    $order->toArray(),
                    array(
                        'nameuser' => $dataprofile['fullname'] . ' ' . $dataprofile['extended']['lastname'],
                        'fullname' => $dataprofile['fullname'],

                        'url_liqpay' => $link,
                        'num_order' => $order->get('num'),
                        'num' => $order->get('num'),
                        'order_id' => $order->get('id')
                    )
                );

                $data['phone'] = $user->get('username');//$user['username'];
                $type = 26;
                //$userObject = $this->modx->getObject('modUser', $user['id']);
                $this->eventNotification($type, $user, $data, ['action' => 'liqpay', 'liqpay_link' => $link]);


            }


            $user_id = $order->get('user_id');
            $user = $this->modx->getObject('modUser', $user_id);

            //$group_manager = (in_array(7, $this->modx->user->getUserGroups()));// проверяем группа менеджер или нет
            if ($this->modx->user->hasSessionContext('mgr') && $this->modx->user->isMember('Administrator')) {
                // echo $this->modx->user->isMember('Administrator').' '.$status_id.' '.$this->modx->user->hasSessionContext('mgr').'<br/>';

                //echo '$status_id'.$status_id;
                //if ($group_manager) {// только админу можно
                if ($status_id == 2)// доставлен, заркыт заказ
                {

                    $order->setFinishPaymentStatus();

                    $this->charge_bonuses($order);
                    $count_orders_close = (int)$user->get('count_orders_close');
                    $count_orders_close = $count_orders_close + 1;
                    $user->set('count_orders_close', $count_orders_close);


                    $summa_close_orders = 0;

                    $profile = $user->getOne('Profile');
                    if ($count_orders_close == 2) {
                        $profilearray = $profile->toArray();
                        // send mail about review for site
                        if ($profilearray['email'] != '') $this->sendmailclosereview($user->toArray(), $profilearray);
                    }
                    $q = $this->modx->newQuery('msOrder');
                    $q->where(array('msOrder.status' => 2, 'msOrder.user_id:=' => $user_id));
                    $q->select(array('msOrder.id', 'msOrder.cart_cost'));
                    $ids = [];
                    $total = 0;
                    if ($q->prepare() && $q->stmt->execute()) {
                        $orders = $q->stmt->fetchAll(PDO::FETCH_ASSOC);
                        foreach ($orders as $order_i) {
                            $ids[] = $order_i['id'];
                            $total += $order_i['cart_cost'];
                        }
                    }
                    $user->set('summa_close_orders', $total);

                    if ($user->get('registr') == '1') {

                        $extended = $profile->get('extended');
                        $user_discount = $extended['discount'];

                        $q = $this->modx->newQuery('msUserDiscount', array('percent:>' => $user_discount));
                        $q->sortby('percent', 'ASC');
                        $q->select('`msUserDiscount`.`percent`,`msUserDiscount`.`total`');
                        $q->limit(1);
                        $next_discount_total = 0;
                        $next_discount = 0;
                        if ($q->prepare() && $q->stmt->execute()) {
                            while ($row = $q->stmt->fetch(PDO::FETCH_ASSOC)) {
                                $next_discount = $row['percent'];
                                $next_discount_total = $row['total'];
                            }
                        }

                        if ($total >= $next_discount_total) {
                            if ($user_discount < $next_discount) {


                                $extended = $profile->get('extended');
                                $extended['discount'] = $next_discount;
                                $profile->set('extended', $extended);


                                if ($profile->save()) {
                                    // надо отправить пользователю письмо
                                    $tpl = 'mailDisount';
                                    //$user=$this->modx->user;
                                    $dataprofile = $user->getOne('Profile')->toArray();
                                    $content = $this->modx->getChunk($tpl,
                                        array_merge(
                                            $dataprofile,//$user->getOne('Profile')->toArray(),
                                            $user->toArray(),
                                            array(
                                                'name' => $dataprofile['fullname'] . ' ' . $dataprofile['extended']['lastname'],
                                                'fullname' => $dataprofile['fullname'],
                                                'total_order' => $total,
                                                'discount' => $next_discount,

                                            )
                                        )
                                    );


                                    $send = $user->sendEmail(
                                        $content
                                        , array(
                                            'subject' => $this->modx->getOption('subject_discount')//'Увеличилась скидка'//$this->modx->getOption('subject_password')//$this->modx->lexicon('office_auth_email_subject')
                                        )
                                    );
                                }
                            }
                        }

                    }

                }
            }


            //запишем общую сумму заказов

            $q = $this->modx->newQuery('msOrder');
            $q->select(array('msOrder.id', 'msOrder.cart_cost'));

            $q->where(['msOrder.user_id:=' => $user_id, 'msOrder.status' => 2]);

            $total_all = 0;
            if ($q->prepare() && $q->stmt->execute()) {
                $orders = $q->stmt->fetchAll(PDO::FETCH_ASSOC);
                foreach ($orders as $order) {
                    $total_all += $order['cart_cost'];
                }
            }
            $user->set('summa_all_orders', $total_all);

            if ($user->save()) {

                $corePath = MODX_CORE_PATH . 'components/unisender/';
                require_once $corePath . 'model/unisender/unisender.class.php';

                $modelunisender = new Unisender($this->modx);
                $user_array = $user->toArray();
                $profile_object = $this->modx->getObject('modUserProfile', array('internalKey' => $user_array['id']));
                $profile = [];
                if ($profile_object)
                    $profile = $profile_object->toArray();
                $result = $modelunisender->UpdateSubscribeUser($user_array, $profile);

            }

            //summa_all_orders
        }
        return true;
    }

    /**
     * Проверяем есть ли продукт у пользвоателях в заказах
     * @param $product_id
     * @return bool
     */
    public
    function product_in_order($product_id)
    {
        if ($this->modx->user->isAuthenticated()) {

            $q = $this->modx->newQuery('msOrder');
            $q->innerJoin('msOrderProduct', 'msOrderProduct', '`msOrderProduct`.`order_id` = `msOrder`.`id`');


            //   $profile = $this->modx->user->Profile;
            $user_id = $this->modx->user->id;

            $q->where(array('msOrder.status' => 2, 'msOrder.user_id:=' => $user_id, 'msOrderProduct.product_id:=' => $product_id));
            $q->select(array('msOrder.id', 'msOrderProduct.product_id', 'msOrderProduct.price', 'msOrderProduct.price_old'));
            $ids = [];
            $total = 0;
            if ($q->prepare() && $q->stmt->execute()) {
                $orders = $q->stmt->fetchAll(PDO::FETCH_ASSOC);
                if (count($orders) > 0) {
                    foreach ($orders as $order) {
                        //
                        return $order['price_old'];
                    }
                }
            }
        }
        return false;
    }

    /**
     * Collects and processes any set of tags
     *
     * @param mixed $html Source code for parse
     * @param integer $maxIterations
     *
     * @return mixed $html Parsed html
     */
    public
    function processTags($html, $maxIterations = 10)
    {
        $this->modx->getParser()->processElementTags('', $html, false, false, '[[', ']]', array(), $maxIterations);
        $this->modx->getParser()->processElementTags('', $html, true, true, '[[', ']]', array(), $maxIterations);
        return $html;
    }


    /**
     * Function for sending email
     *
     * @param string $email
     * @param string $subject
     * @param string $body
     *
     * @return void
     */
    public
    function sendEmail($email, $subject, $body = 'no body set')
    {
        if (!isset($this->modx->mail) || !is_object($this->modx->mail)) {
            $this->modx->getService('mail', 'mail.modPHPMailer');
        }
        $this->modx->mail->setHTML(true);


        $this->modx->mail->set(modMail::MAIL_SUBJECT, trim($subject));
        $this->modx->mail->set(modMail::MAIL_BODY, $body);
        $this->modx->mail->set(modMail::MAIL_FROM, $this->modx->getOption('emailsender'));//$this->modx->getOption('from', $options, $this->modx->getOption('emailsender')));
        $this->modx->mail->set(modMail::MAIL_FROM_NAME, $this->modx->getOption('site_name'));//$this->modx->getOption('fromName', $options, $this->modx->getOption('site_name')));

        $this->modx->mail->address('to', trim($email));

        if (!$this->modx->mail->send()) {
            $this->modx->log(modX::LOG_LEVEL_ERROR, 'An error occurred while trying to send the email: ' . $this->modx->mail->mailer->ErrorInfo);
            $submit = false;
        } else $submit = true;
        $this->modx->mail->reset();
        return $submit;
    }


    /**
     * Function for logging changes of the order
     *
     * @param integer $order_id The id of the order
     * @param string $action The name of action made with order
     * @param string $entry The value of action
     *
     * @return boolean
     */
    public
    function orderLog($order_id, $action = 'status', $entry)
    {
        /* @var msOrder $order */
        if (!$order = $this->modx->getObject('msOrder', $order_id)) {
            return false;
        }

        if (empty($this->modx->request)) {
            $this->modx->getRequest();
        }

        $user_id = ($action == 'status' && $entry == 1) || !$this->modx->user->id ? $order->get('user_id') : $this->modx->user->id;
        if ($order->get('id_user_created') != 0) $user_id = $order->get('id_user_created');
        $message = '';
        if ($action == 'status') {
            if ($entry == 6) {
                $message = 'Оплачен';
            }
        }
        $log = $this->modx->newObject('msOrderLog', array(
            'order_id' => $order_id
        , 'user_id' => $user_id
        , 'timestamp' => time()
        , 'action' => $action
        , 'entry' => $entry
        , 'message' => $message
            //, 'ip' => $this->modx->request->getClientIp()
        ));

        return $log->save();
    }


    public
    function recountpoints_of_product($product_data)
    {

        $product_id = $product_data->get('id');
        $count_like = $product_data->get('count_likes');
        $count_shared = $product_data->get('count_shared');


        $comments = [];

        $sql = "SELECT `C`.`id`,`C`.`deleted`, `C`.`name`,`C`.`rating`,`C`.`createdon` 
 FROM `modx_tickets_comments` as `C`
 INNER JOIN `modx_tickets_threads` AS `T` ON (`C`.`thread`=`T`.`id`)
 LEFT JOIN `modx_users` as `user` ON (`C`.`createdby`=`user`.`id`)
 LEFT JOIN `modx_user_attributes` as `profile` ON (`C`.`createdby`=`profile`.`internalKey`)
 LEFT JOIN `modx_ms2_pets` as `pets` ON (`user`.`id`=`pets`.`user_id`)
 LEFT JOIN `modx_ms2_breeds` as `breed` ON (`pets`.`breed`=`breed`.`id`)
 WHERE `C`.`published`=1 AND `C`.`deleted`=0 AND `T`.`resource`='" . $product_id . "'
 GROUP BY  `C`.`id`
 ORDER BY `C`.`createdon` DESC";

        //echo  '$sql'.$sql;


        $q = $this->modx->prepare($sql);
        $q->execute(array(0));
        $arr = $q->fetchAll(PDO::FETCH_ASSOC);
        $pointer = 0;
        foreach ($arr as $com) {
            //$comments[]=$com['rating'];

            switch ($com['rating']) {
                case '1':
                    $pointer = $pointer - 2;
                    break;
                case '2':
                    $pointer = $pointer - 1;
                    break;
                case '3':
                    //$pointer=$pointer-1;
                    break;
                case '4':
                    $pointer = $pointer + 1;
                    break;
                case '5':
                    $pointer = $pointer + 2;
                    break;

            }
        }

        $pointer = $pointer + ($count_shared * 2) + $count_like;

        $product_data->set('count_points', $pointer);
        $product_data->save();
        return true;
    }


    public function generateRecommendationProducts()
    {
        $q = $this->modx->newQuery('modUser');

        $q->select(array(
            'modUser.id',
            'modUserProfile.email'
        ));

        $q->innerJoin('modUserProfile', 'modUserProfile', 'modUser.id=modUserProfile.internalKey');
        $q->groupby('modUser.id');
        $q->prepare();
        $q->stmt->execute();

        $resultUsers = $q->stmt->fetchAll(PDO::FETCH_ASSOC);

        $return = [];
        $productIds = [];
        foreach ($resultUsers as $user) {
            $userId = $user['id'];
            $email = $user['email'];

            $c = $this->modx->newQuery('msRecomendation');
            $c->command('delete');
            $c->where(array(
                'user_id' => $userId
            ));
            $c->prepare();
            $c->stmt->execute();

            $_products = $this->generateRecommendationProductsByUser($userId, $productIds);
            $products = $_products['products'];
            $productIds = $_products['productIds'];
            $data = [];
            $created = date('Y-m-d H:i:s');
            if (count($products) > 0) {
                foreach ($products as $product) {
                    $name = $product['name'];
                    $_data = [
                        $userId,
                        $product['id'],
                        $this->modx->quote($name),
                        $this->modx->quote($product['image_url']),
                        $this->modx->quote($product['price']),
                        $this->modx->quote($product['price_old']),
                        $this->modx->quote($product['product_url']),
                        $this->modx->quote($created)
                    ];

                    $data[] = "(" . implode(',', $_data) . ")";
                }

                $fields = "`user_id`, `product_id`, `name`, `image_url`, `price`, `price_old`, `product_url`, `created`";

                $sql = "INSERT INTO `modx_ms_recomendation` (" . $fields . ") VALUES " . implode(', ', $data);


                $this->modx->prepare($sql)->execute();

                $return[$email] = $products;
            }

        }


        if (count($return) > 0) {
            //$arrayProducts=array_values($return);
//            $ret=[];
//            $ret['recommendations']=[$return];
            return json_encode($return);
        }

        return [];

    }

    public function generateTopAction()
    {
        $result = [];
        $limit = 10;
        $counter = 0;


        $q = $this->modx->newQuery('msProduct');
        $q->innerJoin('msProductData', 'Data', 'msProduct.id = Data.id');
        $q->innerJoin('PolylangContent', 'polylang_content', '`polylang_content`.`content_id`=`Data`.`id` AND polylang_content.culture_key="ua"');
        $q->leftJoin('modTemplateVarResource', 'TV', 'msProduct.id = TV.contentid AND TV.tmplvarid= 1');
        $where_dop = '';

        $q->where("
            TV.value LIKE '%\"top_action\":\"1\",%' AND 
            (Data.adiscount =1 OR Data.askidka =1) AND `msProduct`.`deleted`=0 AND `msProduct`.`published`=1 " . $where_dop);

        $q->groupBy('msProduct.id');


        $q->limit($limit);
        $q->sortby('RAND()');

        $q->select(array('Data.*', 'msProduct.*',
            'polylang_content.pagetitle as pagetitle_ua',
            'TV.contentid',
            'TV.value',
            'TV.tmplvarid'));
        $q->prepare();
        $q->stmt->execute();

        $products = $q->stmt->fetchAll(PDO::FETCH_ASSOC);
        $domain = 'https://petchoice.ua';

        $productsForSave = $productsId = [];

        foreach ($products as $product) {

            $optionName = '';
            $min_price = 0;
            $min_old_price = 0;

            $optionsJson = $product['value'];
            $optionsArray = $this->modx->fromJson($optionsJson);
            $productModel = $this->modx->getObject('msProductData', $product['id']);
            foreach ($optionsArray as $option) {


                if (($option['top_action'] == 1) && ($option['instock'] == 1) && ((!isset($option['disable_option']) || ($option['disable_option'] == 0)))) {

                    if (!isset($productsForSave[$product['id']])) {

                        $getPrices = $productModel->getAllPrice($option);
                        $min_price = $getPrices['price'];
                        $min_old_price = $getPrices['oldprice'];
                        if (!empty($option['weight']) && !empty($option['weight_prefix'])) {
                            $optionName = $option['weight'] . ' ' . $option['weight_prefix'];
                        }
                        $name = $product['pagetitle_ua'] . ($optionName ? ', ' . $optionName : '');

                        $productUrl = $domain . '/' . $product['uri'];
                        $productsForSave[$product['id']] = [
                            'id' => $product['id'],
                            'product_url' => $productUrl,
                            'image_url' => $domain . $product['image'],
                            //'price_old' => $min_old_price,//$product['old_price'],
                            'price' => $min_price,//$product['price'],
                            'name' => $name
                        ];

                        if (($min_old_price > 0) && ($min_old_price != $min_price)) {

                            $productsForSave[$product['id']]['price_old'] = $min_old_price;
                            $productsId[] = $product['id'];
                        }
                        $counter++;
                    }
                }

            }
        }

        if ($counter < $limit) {
            $where_dop = '';
            $limitExtra = $limit - $counter;
            $q = $this->modx->newQuery('msProduct');
            $q->innerJoin('msProductData', 'Data', 'msProduct.id = Data.id');
            $q->innerJoin('PolylangContent', 'polylang_content', '`polylang_content`.`content_id`=`Data`.`id` AND polylang_content.culture_key="ua"');
            $q->leftJoin('modTemplateVarResource', 'TV', 'msProduct.id = TV.contentid AND TV.tmplvarid= 1');
            if (count($productsId) > 0) {
                $where_dop = '  AND `msProduct`.`id` NOT IN (' . implode(',', $productsId) . ')';
            }

            $q->where("(Data.adiscount =1 OR Data.askidka =1) AND `msProduct`.`deleted`=0 AND `msProduct`.`published`=1 " . $where_dop);

            $q->groupBy('msProduct.id');


            $q->limit($limitExtra);
            $q->sortby('RAND()');

            $q->select(array('Data.*', 'msProduct.*',
                'polylang_content.pagetitle as pagetitle_ua',
                'TV.contentid',
                'TV.value',
                'TV.tmplvarid'));
            $q->prepare();
            $q->stmt->execute();

            $products = $q->stmt->fetchAll(PDO::FETCH_ASSOC);
            $domain = 'https://petchoice.ua';

            // $productsForSave = [];

            foreach ($products as $product) {

                $optionName = '';
                $min_price = 0;
                $min_old_price = 0;

                $optionsJson = $product['value'];
                $optionsArray = $this->modx->fromJson($optionsJson);
                $productModel = $this->modx->getObject('msProductData', $product['id']);


                foreach ($optionsArray as $option) {

                    $optionW = $option['weight'] . ',' . $option['weight_prefix'];


                    if (($option['instock'] == 1) && ((!isset($option['disable_option']) || ($option['disable_option'] == 0)))) {
                        if ($productModel) {

                            $Prices = $productModel->getAllPrice($option);

                            if (($min_price == 0) || ($Prices['price'] < $min_price)) {
                                $min_price = $Prices['price'];
                                $min_old_price = $Prices['oldprice'];
                                if (!empty($option['weight']) && !empty($option['weight_prefix'])) {
                                    $optionName = $option['weight'] . ' ' . $option['weight_prefix'];
                                }
                            }

                        }
                    }

                }

                $name = $product['pagetitle_ua'] . ($optionName ? ', ' . $optionName : '');

                $productUrl = $domain . '/' . $product['uri'];

                $min_old_price = str_replace(',', '.', $min_old_price);
                $min_price = str_replace(',', '.', $min_price);


                $productsForSave[$product['id']] = [
                    'id' => $product['id'],
                    'product_url' => $productUrl,
                    'image_url' => $domain . $product['image'],
                    //'price_old' => $min_old_price,//$product['old_price'],
                    'price' => $min_price,//$product['price'],
                    'name' => $name
                ];

                if (($min_old_price > 0) && ($min_old_price != $min_price)) {

                    $productsForSave[$product['id']]['price_old'] = $min_old_price;
                }
                $counter++;


            }

        }
//        echo '<pre>';
//        print_r($productsForSave);
//        echo '</pre>';
        if (count($productsForSave) > 0) {
            $return = [];

            $arrayProducts = array_values($productsForSave);

            $return['recommendations'] = $arrayProducts;
            return json_encode($return);
        }
        return [];
    }

    protected function generateBlog()
    {
        $return = '';
        $q = $this->modx->newQuery('Article');
        $q->innerJoin('PolylangContent', 'polylang_content', '`polylang_content`.`content_id`=`Article`.`id` AND polylang_content.culture_key="ua"');
        $q->where("  `Article`.`class_key`='Article' AND `Article`.`deleted`=0 AND `Article`.`published`=1 ");
        //array('msOrder.status' => 2, 'msOrder.user_id:=' => $userId));
        $q->leftJoin('modTemplateVarResource', 'TV', 'Article.id = TV.contentid AND TV.tmplvarid= 77');

        $q->select(array('Article.*',
            'TV.value as image',
            'polylang_content.pagetitle as pagetitle_ua'));
        $q->prepare();

        $q->stmt->execute();
        $sql = $q->toSQL();


        $articles = $q->stmt->fetchAll(PDO::FETCH_ASSOC);

        $allArticles = [];
        $domain = 'https://petchoice.ua/';
        foreach ($articles as $article) {
//            echo '<pre>$article';
//            print_r($article);
//            echo '</pre>';
            $art = [
                'title' => $article['pagetitle_ua'],//->get('pagetitle_ua'),
                'url' => $domain . $article['uri'],//->get('uri'),
                //'image'=> $domain.$article['image'],//->get('image'),
            ];
            if (!empty($article['image'])) {
                $art['image'] = $domain . $article['image'];
            }
            $allArticles[] = $art;
        }


//        echo '<pre>';
//        print_r($articles);
//        echo '</pre>';
//        die();

        if (count($allArticles) > 0) {
            $return = [];
            $return['recommendations'] = $allArticles;
            return json_encode($return);
        }

        return [];
    }


    public
    function writeFileWithJson($pathfile, $type = 'recommendation')
    {

        switch ($type) {
            case 'blog':
                $content = $this->generateBlog();
                break;
            case 'recommendation':
                $content = $this->generateRecommendationProducts();
                break;
            case 'top_action':
                $content = $this->generateTopAction();
                break;
            default:
                return false;
                break;
        }

        if (!empty($content)) {
            $handle = fopen($pathfile, "w+");

            fwrite($handle, header('Content-Type: application/json; charset=utf8'));
            if (fwrite($handle, $content)) {

                echo "ok \r\n";
                fclose($handle);
                return true;
            } else return false;
        }


    }


    public
    function generateRecommendationProductsByUser($userId, $productIds = [])
    {

        $domain = 'https://petchoice.ua';
        $products_for_mail = [];


        $userProfile = $this->modx->getObject('modUserProfile', ['internalKey' => $userId]);//newQuery('msOrderProduct');
        $email = false;
        if ($userProfile) {
            $email = $userProfile->get('email');
        }
        $common_limit = 10;


        if ($email) {
            $count_viewed = $count_buy = $count_pets_prod = 0;
            $products_for_mail = [];
            $tpl_action = 'tpl.mailaction';
            //buy
            $q = $this->modx->newQuery('msOrderProduct');
            $q->innerJoin('msOrder', 'msOrder', '`msOrderProduct`.`order_id` = `msOrder`.`id`');
            $q->innerJoin('msProductData', 'Data', 'msOrderProduct.product_id = Data.id');
            $q->innerJoin('msProduct', 'msProduct', 'msProduct.id = Data.id');
            $q->innerJoin('PolylangContent', 'polylang_content', '`polylang_content`.`content_id`=`Data`.`id` AND polylang_content.culture_key="ua"');


            $q->leftJoin('modTemplateVarResource', 'TV', 'msProduct.id = TV.contentid AND TV.tmplvarid= 1');
            $q->where(array('msOrder.status' => 2, 'msOrder.user_id:=' => $userId));
            $q->limit(3);
            $q->select(array('msOrderProduct.*', 'Data.*', 'msProduct.*',
                'polylang_content.pagetitle as pagetitle_ua',
                'TV.contentid',
                'TV.value',
                'TV.tmplvarid'));
            $q->sortBy('msOrder.createdon', 'DESC');
            $q->groupBy('msProduct.id');


            $no_repeat = [];
            $ids = [];
            $total = 0;
            if ($q->prepare() && $q->stmt->execute()) {
                $orders_products = $q->stmt->fetchAll(PDO::FETCH_ASSOC);
                foreach ($orders_products as $product) {
                    if (!isset($productIds[$product['id']])) {
                        $productModel = $this->modx->getObject('msProductData', $product['id']);
                        $productIds[$product['id']] = $productModel;
                    } else {
                        $productModel = $productIds[$product['id']];
                    }

                    $optionsJsonOrder = $product['options'];
                    $optionsOrder = $this->modx->fromJson($optionsJsonOrder);
                    if (isset($optionsOrder['size'])) {
                        $optionOrder = $optionsOrder['size'];

                        $no_repeat[$product['id']] = $product['id'];


                        $optionsJson = $product['value'];
                        $options = $this->modx->fromJson($optionsJson);


                        $optionName = false;
                        $min_price = $min_old_price = 0;
                        foreach ($options as $option) {

                            $optionW = $option['weight'] . ',' . $option['weight_prefix'];


                            if ($option['instock'] == 1) {
                                if (!isset($option['disable_option']) || ($option['disable_option'] == 0)) {
                                    if ($productModel) {

                                        $Prices = $productModel->getAllPrice($option);

                                        if ($optionOrder == $optionW) {
                                            $min_price = $Prices['price'];
                                            $min_old_price = $Prices['oldprice'];
                                            if (!empty($option['weight']) && !empty($option['weight_prefix'])) {
                                                $optionName = $option['weight'] . ' ' . $option['weight_prefix'];
                                            }
                                            break;
                                        }


                                        if (($min_price == 0) || ($Prices['price'] < $min_price)) {
                                            $min_price = $Prices['price'];
                                            $min_old_price = $Prices['oldprice'];
                                            if (!empty($option['weight']) && !empty($option['weight_prefix'])) {
                                                $optionName = $option['weight'] . ' ' . $option['weight_prefix'];
                                            }
                                        }

                                    }
                                }
                            }


                        }

                        if ($min_price > 0) {
                            $productUrl = $domain . '/' . $product['uri'];
                            $name = $product['pagetitle_ua'] . ($optionName ? ', ' . $optionName : '');

                            $min_old_price = round(str_replace(',', '.', $min_old_price),2);
                            $min_price = round(str_replace(',', '.', $min_price),2);
                            // if (!isset($productIds[$product['id']])) {

                            $ProductOne=[
                                'id' => $product['id'],
                                'product_url' => $productUrl,
                                'image_url' => $domain . $product['image'],
                                //'price_old' => $min_old_price,//$product['old_price'],
                                'price' => $min_price,//$product['price'],
                                'name' => $name
                            ];
                            if ($min_old_price!=$min_price)
                                $ProductOne['price_old']=$min_old_price;


                            $products_for_mail[] = $ProductOne;
                            /*[
                                'id' => $product['id'],
                                'product_url' => $productUrl,
                                'image_url' => $domain . $product['image'],
                                'price_old' => $min_old_price,//$product['old_price'],
                                'price' => $min_price,//$product['price'],
                                'name' => $name
                            ];*/


                            $count_buy++;
                            //}
                        }
                    }
                }


            }


            // viewed

            $q = $this->modx->newQuery('msLastviewed');
            $q->innerJoin('msProductData', 'Data', 'msLastviewed.product_id = Data.id');
            $q->innerJoin('msProduct', 'msProduct', 'msProduct.id = Data.id');
            $q->innerJoin('PolylangContent', 'polylang_content', '`polylang_content`.`content_id`=`Data`.`id` AND polylang_content.culture_key="ua"');
            $q->leftJoin('modTemplateVarResource', 'TV', 'msProduct.id = TV.contentid AND TV.tmplvarid= 1');

            $where_dop = '';
            if (count($no_repeat) > 0)
                $where_dop = ' AND `msProduct`.`id` NOT IN (' . implode(',', $no_repeat) . ')';


            $q->where("user_id=$userId " . $where_dop);//['user_id' => $user_id]);
            $q->limit(3);
            $q->select(array('Data.*', 'msProduct.*', 'TV.contentid',
                'polylang_content.pagetitle as pagetitle_ua',
                'TV.value',
                'TV.tmplvarid'));
            $q->groupBy('msProduct.id');
            $q->sortBy('msLastviewed.date', 'DESC');
            $q->prepare();

            $q->stmt->execute();
            //echo $q->toSQL();

            $view_products = $q->stmt->fetchAll(PDO::FETCH_ASSOC);

            foreach ($view_products as $product) {
                if (!isset($productIds[$product['id']])) {
                    $productModel = $this->modx->getObject('msProductData', $product['id']);
                    $productIds[$product['id']] = $productModel;
                } else {
                    $productModel = $productIds[$product['id']];
                }
                $no_repeat[$product['id']] = $product['id'];
                $optionsJson = $product['value'];
                $options = $this->modx->fromJson($optionsJson);
                //$productModel = $this->modx->getObject('msProductData', $product['id']);

                $optionName = false;
                $min_price = $min_old_price = 0;
                foreach ($options as $option) {

                    if ($option['instock'] == 1) {
                        if (!isset($option['disable_option']) || ($option['disable_option'] == 0)) {
                            if ($productModel) {

                                $Prices = $productModel->getAllPrice($option);
                                if (($min_price == 0) || ($Prices['price'] < $min_price)) {
                                    $min_price = $Prices['price'];
                                    $min_old_price = $Prices['oldprice'];
                                    if (!empty($option['weight']) && !empty($option['weight_prefix'])) {
                                        $optionName = $option['weight'] . ' ' . $option['weight_prefix'];
                                    }
                                }

                            }
                        }
                    }

                }

                if ($min_price > 0) {
                    $productUrl = $domain . '/' . $product['uri'];
                    $name = $product['pagetitle_ua'] . ($optionName ? ', ' . $optionName : '');

                    $min_old_price = round(str_replace(',', '.', $min_old_price),2);
                    $min_price = round(str_replace(',', '.', $min_price),2);
                    //if (!isset($productIds[$product['id']])) {

                    $ProductOne=[
                        'id' => $product['id'],
                        'product_url' => $productUrl,
                        'image_url' => $domain . $product['image'],
                        //'price_old' => $min_old_price,//$product['old_price'],
                        'price' => $min_price,//$product['price'],
                        'name' => $name
                    ];
                    if ($min_old_price!=$min_price)
                        $ProductOne['price_old']=$min_old_price;

                    $products_for_mail[] = $ProductOne;
                    /*
                    $products_for_mail[] = [
                        'id' => $product['id'],
                        'product_url' => $productUrl,
                        'image_url' => $domain . $product['image'],
                        'price_old' => $min_old_price,
                        'price' => $min_price,
                        'name' => $name
                    ];*/
                    $count_viewed++;
                    //}


                }
            }

            $count_pets_prod = $common_limit - $count_buy - $count_viewed;


            // for pets
            $q = $this->modx->newQuery('msPet');
            $q->where(['user_id' => $userId]);
            $q->select(['msPet.*']);
            $q->prepare();

            $q->stmt->execute();
            $pets = $q->stmt->fetchAll(PDO::FETCH_ASSOC);
            $types = [];
            $categories = [];
            foreach ($pets as $pet) {
                switch ($pet['type']) {
                    case 2:
                        $categories[] = 13;
                        break;
                    case 1:
                        $categories[] = 14;
                        break;
                    case 3:
                        $categories[] = 27;
                        break;
                    case 4:
                        $categories[] = 28;
                        break;
                    case 6:
                        $categories[] = 1148;
                        break;

                }
            }

            //исключить сухой корм  и лечебный
            //$explode_categories = [15, 19, 57, 59];


            if ($count_pets_prod > 0) {
                if (count($categories) > 0) {
                    $q = $this->modx->newQuery('msProduct');
                    $q->innerJoin('msProductData', 'Data', 'msProduct.id = Data.id');
                    $q->innerJoin('msCategory', 'msCategory', 'msProduct.parent = msCategory.id');
                    $q->leftJoin('modTemplateVarResource', 'TV', 'msProduct.id = TV.contentid AND TV.tmplvarid= 1');
                    //$q->innerJoin('msCategory', 'msCategoryparent', 'msCategory.parent = msCategoryparent.id');
                    $where_dop = '';
                    if (count($no_repeat) > 0)
                        $where_dop = ' AND `msProduct`.`id` NOT IN (' . implode(',', $no_repeat) . ')';


                    //$where_dop .= ' AND `msCategory`.`id` NOT IN (' . implode(',', $explode_categories) . ')';
                    $q->where("(Data.adiscount =1 OR Data.askidka =1) AND `msProduct`.`deleted`=0 AND `msProduct`.`published`=1 AND msCategory.parent IN (" . implode(',', $categories) . ") " . $where_dop);

                } else {
                    $q = $this->modx->newQuery('msProduct');
                    $q->innerJoin('msProductData', 'Data', 'msProduct.id = Data.id');
                    $q->leftJoin('modTemplateVarResource', 'TV', 'msProduct.id = TV.contentid AND TV.tmplvarid= 1');
                    $where_dop = '';
                    if (count($no_repeat) > 0)
                        $where_dop = ' AND `msProduct`.`id` NOT IN (' . implode(',', $no_repeat) . ')';
                    $q->where("(Data.adiscount =1 OR Data.askidka =1) AND `msProduct`.`deleted`=0 AND `msProduct`.`published`=1 " . $where_dop);
                }
                $q->groupBy('msProduct.id');

                $q->innerJoin('PolylangContent', 'polylang_content', '`polylang_content`.`content_id`=`Data`.`id` AND polylang_content.culture_key="ua"');


                $q->limit($count_pets_prod);
                $q->sortby('RAND()');

                $q->select(array('Data.*', 'msProduct.*',
                    'polylang_content.pagetitle as pagetitle_ua',
                    'TV.contentid',
                    'TV.value',
                    'TV.tmplvarid'));
                $q->prepare();
                $q->stmt->execute();

                $products_by_type = $q->stmt->fetchAll(PDO::FETCH_ASSOC);

                foreach ($products_by_type as $product) {

                    if (!isset($productIds[$product['id']])) {
                        $productModel = $this->modx->getObject('msProductData', $product['id']);
                        $productIds[$product['id']] = $productModel;
                    } else
                        $productModel = $productIds[$product['id']];


                    $optionsJson = $product['value'];
                    $options = $this->modx->fromJson($optionsJson);
                    //$productModel = $this->modx->getObject('msProductData', $product['id']);

                    $optionName = false;
                    $min_price = $min_old_price = 0;
                    foreach ($options as $option) {

                        if ($option['instock'] == 1) {
                            if (!isset($option['disable_option']) || ($option['disable_option'] == 0)) {
                                if ($productModel) {

                                    $Prices = $productModel->getAllPrice($option);
                                    if (($min_price == 0) || ($Prices['price'] < $min_price)) {
                                        $min_price = $Prices['price'];
                                        $min_old_price = $Prices['oldprice'];
                                        if (!empty($option['weight']) && !empty($option['weight_prefix'])) {
                                            $optionName = $option['weight'] . ' ' . $option['weight_prefix'];
                                        }
                                    }

                                }
                            }
                        }

                    }

                    if ($min_price > 0) {
                        $productUrl = $domain . '/' . $product['uri'];
                        $name = $product['pagetitle_ua'] . ($optionName ? ', ' . $optionName : '');
                        $min_old_price = round(str_replace(',', '.', $min_old_price),2);
                        $min_price = round(str_replace(',', '.', $min_price),2);
                        //if (!isset($productIds[$product['id']])) {
                        $ProductOne=[
                            'id' => $product['id'],
                            'product_url' => $productUrl,
                            'image_url' => $domain . $product['image'],
                            //'price_old' => $min_old_price,//$product['old_price'],
                            'price' => $min_price,//$product['price'],
                            'name' => $name
                        ];
                        if ($min_old_price!=$min_price)
                            $ProductOne['price_old']=$min_old_price;

                        $products_for_mail[] = $ProductOne;

                        $count_buy++;
                        //}
                    }
                }

            }

        }


        //        $products=$_products['products'];
        //        $productIds=$_products['productIds'];

        $_products = [
            'products' => $products_for_mail,
            'productIds' => $productIds
        ];
        return $_products;

    }

    public
    function getPersonalRecommendation($user_id)
    {

        $common_limit = 7;
        $count_viewed = $count_buy = $count_pets_prod = 0;
        $products_for_mail = [];
        $tpl_action = 'tpl.mailaction';
        //buy
        $q = $this->modx->newQuery('msOrderProduct');
        $q->innerJoin('msOrder', 'msOrder', '`msOrderProduct`.`order_id` = `msOrder`.`id`');
        $q->innerJoin('msProductData', 'Data', 'msOrderProduct.product_id = Data.id');
        $q->innerJoin('msProduct', 'msProduct', 'msProduct.id = Data.id');
        $q->leftJoin('modTemplateVarResource', 'TV', 'msProduct.id = TV.contentid AND TV.tmplvarid= 1');
        $q->where(array('msOrder.status' => 2, 'msOrder.user_id:=' => $user_id));
        $q->limit(3);
        $q->select(array('msOrderProduct.*', 'Data.*', 'msProduct.*',
            'TV.contentid',
            'TV.value',
            'TV.tmplvarid'));
        $q->sortBy('msOrder.createdon', 'DESC');
        $q->groupBy('msProduct.id');


        /*
                $q = $this->modx->newQuery('msProduct');
                $q->select(array(
                    'msProduct.id',
                    //'Data.*',
                    'TV.contentid',
                    'TV.value',
                    'TV.tmplvarid',
                    'Vendor.po_nilishiy'
                ));

                $q->innerJoin('msProductData', 'Data', 'msProduct.id = Data.id');
                $q->leftJoin('modTemplateVarResource', 'TV', 'msProduct.id = TV.contentid AND TV.tmplvarid= 1');
                $q->leftJoin('msVendor', 'Vendor', 'Data.vendor = Vendor.id');

                $q->where(['Vendor.po_nilishiy:=' => 1]);
                $q->groupby('msProduct.id');
                $q->prepare();
                $q->stmt->execute();
                $result = $q->stmt->fetchAll(PDO::FETCH_ASSOC);
        */


        $no_repeat = [];
        $ids = [];
        $total = 0;
        if ($q->prepare() && $q->stmt->execute()) {
            $orders_products = $q->stmt->fetchAll(PDO::FETCH_ASSOC);
            /* echo '<pre>$orders_products';
             print_r($orders_products);
             echo '</pre>';
             */
            foreach ($orders_products as $product) {
                $no_repeat[$product['id']] = $product['id'];

                $options = $product['value'];
                echo '<pre>';
                print_r($product);
                echo '</pre>';

                $products_for_mail[$product['id']] = $this->modx->getChunk($tpl_action,
                    [
                        //'utm_source' => 'personal-email',
                        //'utm_medium' => 'personal-email',
                        //'utm_campaign' => 'personal-email',
                        'image' => $product['image'],
                        'old_price' => $product['old_price'],
                        'id' => $product['id'],
                        'price' => $product['price'],
                        'pagetitle' => $product['pagetitle'],
                    ]
                );
                $count_buy++;
            }
            //die();


        }


        // viewed

        $q = $this->modx->newQuery('msLastviewed');
        $q->innerJoin('msProductData', 'Data', 'msLastviewed.product_id = Data.id');
        $q->innerJoin('msProduct', 'msProduct', 'msProduct.id = Data.id');

        $where_dop = '';
        if (count($no_repeat) > 0)
            $where_dop = ' AND `msProduct`.`id` NOT IN (' . implode(',', $no_repeat) . ')';


        $q->where("user_id=$user_id " . $where_dop);//['user_id' => $user_id]);
        $q->limit(3);
        $q->select(array('Data.*', 'msProduct.*'));
        $q->groupBy('msProduct.id');
        $q->sortBy('msLastviewed.date', 'DESC');
        $q->prepare();

        $q->stmt->execute();
        //echo $q->toSQL();

        $view_products = $q->stmt->fetchAll(PDO::FETCH_ASSOC);
//        $view_products
//
        //utm_source=`site-newsletter`
        //&utm_medium=`email`
        //&utm_campaign=`registration`


        /* echo '<pre>$view_products';
         print_r($view_products);
         echo '</pre>';*/
        foreach ($view_products as $product) {

            $no_repeat[$product['id']] = $product['id'];
            $products_for_mail[$product['id']] = $this->modx->getChunk($tpl_action,
                [
                    //'utm_source' => 'personal-email',
                    //'utm_medium' => 'personal-email',
                    //'utm_campaign' => 'personal-email',
                    'image' => $product['image'],
                    'old_price' => $product['old_price'],
                    'id' => $product['id'],
                    'price' => $product['price'],
                    'pagetitle' => $product['pagetitle'],
                ]
            );
            $count_viewed++;
        }


        $count_pets_prod = $common_limit - $count_buy - $count_viewed;


        // for pets
        $tpl = 'tpl.mailaction';
        //msProducts
        $q = $this->modx->newQuery('msPet');
        $q->where(['user_id' => $user_id]);
        $q->select(['msPet.*']);
        $q->prepare();

        $q->stmt->execute();
        // echo 'toSQL ' . $q->toSQL();
        $pets = $q->stmt->fetchAll(PDO::FETCH_ASSOC);
        $types = [];
        $categories = [];
        foreach ($pets as $pet) {
            switch ($pet['type']) {
                case 2:
                    $categories[] = 13;
                    break;
                case 1:
                    $categories[] = 14;
                    break;
                case 3:
                    $categories[] = 27;
                    break;
                case 4:
                    $categories[] = 28;
                    break;
                case 6:
                    $categories[] = 1148;
                    break;

            }
            // $types'        '
        }

        //исключить сухой корм  и лечебный
        $explode_categories = [15, 19, 57, 59];


        if ($count_pets_prod > 0) {
            if (count($categories) > 0) {
                $q = $this->modx->newQuery('msProduct');
                $q->innerJoin('msProductData', 'Data', 'msProduct.id = Data.id');
                $q->innerJoin('msCategory', 'msCategory', 'msProduct.parent = msCategory.id');
                //$q->innerJoin('msCategory', 'msCategoryparent', 'msCategory.parent = msCategoryparent.id');
                $where_dop = '';
                if (count($no_repeat) > 0)
                    $where_dop = ' AND `msProduct`.`id` NOT IN (' . implode(',', $no_repeat) . ')';


                $where_dop .= ' AND `msCategory`.`id` NOT IN (' . implode(',', $explode_categories) . ')';
                $q->where("(Data.adiscount =1 OR Data.askidka =1) AND `msProduct`.`deleted`=0 AND `msProduct`.`published`=1 AND msCategory.parent IN (" . implode(',', $categories) . ") " . $where_dop);

            } else {
                $q = $this->modx->newQuery('msProduct');
                $q->innerJoin('msProductData', 'Data', 'msProduct.id = Data.id');

                $where_dop = '';
                if (count($no_repeat) > 0)
                    $where_dop = ' AND `msProduct`.`id` NOT IN (' . implode(',', $no_repeat) . ')';
                $q->where("(Data.adiscount =1 OR Data.askidka =1) AND `msProduct`.`deleted`=0 AND `msProduct`.`published`=1 " . $where_dop);
            }
            $q->groupBy('msProduct.id');


            $q->limit($count_pets_prod);
            $q->sortby('RAND()');

            $q->select(array('Data.*', 'msProduct.*'));
            $q->prepare();
            $q->stmt->execute();

            $products_by_type = $q->stmt->fetchAll(PDO::FETCH_ASSOC);

            foreach ($products_by_type as $product) {

                $products_for_mail[$product['id']] = $this->modx->getChunk($tpl_action,
                    [
                        'image' => $product['image'],
                        'old_price' => $product['old_price'],
                        'id' => $product['id'],
                        'price' => $product['price'],
                        'pagetitle' => $product['pagetitle'],
                    ]
                );
                $count_buy++;
            }


        }

        $result = '<!-- TABLE 1 HORISONTAL -->
        <table width="100%" cellpadding="0" cellspacing="0">
        ' . implode("\r\n", $products_for_mail) . '
        	
        </table><br /><br />';
        return $result;

    }

    public
    function addVariablesForMail($user_id)
    {


        $personal_goods = $this->getPersonalRecommendation($user_id);
        $result = ['personal_goods' => $personal_goods];
        return $result;
    }

    /**
     * Function for formatting dates
     *
     * @param string $date Source date
     * @return string $date Formatted date
     */
    public
    function formatDate($date = '')
    {
        $df = $this->modx->getOption('ms2_date_format', null, '%d.%m.%Y %H:%M');
        return (!empty($date) && $date !== '0000-00-00 00:00:00') ? strftime($df, strtotime($date)) : '&nbsp;';
    }


    /**
     * Function for formatting price
     *
     * @param string $price
     *
     * @return string $price
     */
    public
    function formatPrice($price = '0')
    {
        $pf = $this->modx->fromJSON($this->modx->getOption('ms2_price_format', null, '[2, ".", " "]'));
        if (is_array($pf)) {
            $price = number_format($price, $pf[0], $pf[1], $pf[2]);
        }

        if ($this->modx->getOption('ms2_price_format_no_zeros', null, true)) {
            if (preg_match('/\..*$/', $price, $matches)) {
                $tmp = rtrim($matches[0], '.0');
                $price = str_replace($matches[0], $tmp, $price);
            }
        }

        return $price;
    }


    /**
     * Function for formatting weight
     *
     * @param string $weight
     *
     * @return string
     */
    public
    function formatWeight($weight = '0')
    {
        $wf = json_decode($this->modx->getOption('ms2_weight_format', null, '[3, ".", " "]'), true);
        if (is_array($wf)) {
            $weight = number_format($weight, $wf[0], $wf[1], $wf[2]);
        }

        if ($this->modx->getOption('ms2_weight_format_no_zeros', null, true)) {
            if (preg_match('/\..*$/', $weight, $matches)) {
                $tmp = rtrim($matches[0], '.0');
                $weight = str_replace($matches[0], $tmp, $weight);
            }
        }

        return $weight;
    }


    /**
     * Gets matching resources by tags. This is adapted function from miniShop1 for backward compatibility
     * @deprecated
     *
     * @param array $tags Tags for search
     * @param int $only_ids Return only ids of matched resources
     * @param int $strict 0 - goods must have at least one specified tag
     *                      1 - goods must have all specified tags, but can have more
     *                      2 - goods must have exactly the same tags.
     * @return array $ids Or array with resources with data and tags
     */
    function getTagged($tags = array(), $strict = 0, $only_ids = 0)
    {
        if (!is_array($tags)) {
            $tags = explode(',', $tags);
        }

        $q = $this->modx->newQuery('msProductOption', array('key' => 'tags', 'value:IN' => $tags));
        $q->select('product_id');
        $ids = array();
        if ($q->prepare() && $q->stmt->execute()) {
            $ids = $q->stmt->fetchAll(PDO::FETCH_COLUMN);
        }
        $ids = array_unique($ids);

        // If needed only ids of not strictly mathed items - return.
        if (!$strict && $only_ids) {
            return $ids;
        }

        // Filtering ids
        $count = count($tags);

        /* @var PDOStatement $stmt */
        if ($strict) {
            foreach ($ids as $key => $product_id) {
                if ($strict > 1) {
                    $sql = "SELECT COUNT(*) FROM {$this->modx->getTableName('msProductOption')} WHERE `product_id` = {$product_id} AND `key` = 'tags';";
                    $stmt = $this->modx->prepare($sql);
                    $stmt->execute();
                    if ($stmt->fetch(PDO::FETCH_COLUMN) != $count) {
                        unset($ids[$key]);
                        continue;
                    }
                }

                foreach ($tags as $tag) {
                    $sql = "SELECT COUNT(`product_id`) FROM {$this->modx->getTableName('msProductOption')} WHERE `product_id` = {$product_id} AND `key` = 'tags' AND `value` = '{$tag}';";
                    $stmt = $this->modx->prepare($sql);
                    $stmt->execute();
                    if (!$stmt->fetch(PDO::FETCH_COLUMN)) {
                        unset($ids[$key]);
                        break;
                    }
                }
            }
        }

        // Return strictly ids, if needed
        $ids = array_unique($ids);
        if ($only_ids) {
            return $ids;
        }

        // Process results
        $data = array();
        foreach ($ids as $id) {
            if (!$only_ids) {
                if ($res = $this->modx->getObject('msProduct', $id)) {
                    $data[$id] = $res->toArray();
                }
            }
        }

        return $data;
    }


    /**
     * Shorthand for original modX::invokeEvent() method with some useful additions.
     *
     * @param $eventName
     * @param array $params
     * @param $glue
     *
     * @return array
     */
    public
    function invokeEvent($eventName, array $params = array(), $glue = '<br/>')
    {
        if (isset($this->modx->event->returnedValues)) {
            $this->modx->event->returnedValues = null;
        }

        $response = $this->modx->invokeEvent($eventName, $params);
        if (is_array($response) && count($response) > 1) {
            foreach ($response as $k => $v) {
                if (empty($v)) {
                    unset($response[$k]);
                }
            }
        }

        $message = is_array($response) ? implode($glue, $response) : trim((string)$response);
        if (isset($this->modx->event->returnedValues) && is_array($this->modx->event->returnedValues)) {
            $params = array_merge($params, $this->modx->event->returnedValues);
        }

        return array(
            'success' => empty($message)
        , 'message' => $message
        , 'data' => $params
        );
    }


    /**
     * This method returns an error of the order
     *
     * @param string $message A lexicon key for error message
     * @param array $data .Additional data, for example cart status
     * @param array $placeholders Array with placeholders for lexicon entry
     *
     * @return array|string $response
     */
    public
    function error($message = '', $data = array(), $placeholders = array())
    {
        $response = array(
            'success' => false,
            'message' => $this->modx->lexicon($message, $placeholders),
            'data' => $data,
        );

        return $this->config['json_response'] ? $this->modx->toJSON($response) : $response;
    }


    /**
     * This method returns an success of the order
     *
     * @param string $message A lexicon key for success message
     * @param array $data .Additional data, for example cart status
     * @param array $placeholders Array with placeholders for lexicon entry
     *
     * @return array|string $response
     */
    public
    function success($message = '', $data = array(), $placeholders = array())
    {
        $response = array(
            'success' => true,
            'message' => $this->modx->lexicon($message, $placeholders),
            'data' => $data,
        );

        return $this->config['json_response'] ? $this->modx->toJSON($response) : $response;
    }

    /**
     * получение всех категорий товаров (csv)
     * @return bool
     */
    public
    function getAllCategories($root)
    {
        $file = 'categories.csv';
        $pathfile = $root . $this->path1c . $file;
        if (!file_exists($pathfile)) {
            $q = $this->modx->newQuery('msCategory');
            $q->select(array(
                'msCategory.*',
                //'Data.*',
                //'Vendor.*',
            ));

            $where['msCategory.class_key:='] = 'msCategory';
            $q->where($where);
            $q->prepare();
            $q->stmt->execute();
            $result = $q->stmt->fetchAll(PDO::FETCH_ASSOC);
            $category_csv = "название~родитель~ид;\r\n";
            foreach ($result as $item) {
                $category_csv .= $item['pagetitle'] . "~" . $item['parent'] . "~" . $item['id'] . ";\r\n";
            }

            //echo $_SERVER['DOCUMENT_ROOT'].$this->path1c.$file;
            //$this->path1c.
            $handle = fopen($pathfile, "w+");

            fwrite($handle, header('Content-Type: text/html; charset=utf8'));
            if (fwrite($handle, $category_csv)) {

                echo "ok \r\n";
                fclose($handle);
                return true;
            } else return false;
        }
        return false;
    }

    /**
     * получение всех продуктов с магазина или определённое количество (csv)
     * @param bool $limit
     * @return bool
     */
    public
    function getProducts($root, $limit = false)
    {//require_once "ImageHash.php";
        $file = 'products.csv';
        $pathfile = $root . $this->path1c . $file;
        $pathfile_photos = $root . $this->path1c . 'products_photo.csv';;
        if (!file_exists($pathfile)) {
            $q = $this->modx->newQuery('msProduct');
            $q->select(array(
                'msProduct.*',
                'Data.*',
                'Vendor.*',
                'TV.*'
            ));

            $q->innerJoin('msProductData', 'Data', 'msProduct.id = Data.id');
            $q->leftJoin('msVendor', 'Vendor', 'Data.vendor = Vendor.id');

            $q->innerJoin('modTemplateVarResource', 'TV', 'msProduct.id = TV.contentid AND TV.tmplvarid= 1');
            if ($limit) $q->limit($limit);
            $q->prepare();
            $q->stmt->execute();
            $result = $q->stmt->fetchAll(PDO::FETCH_ASSOC);

            $q2 = $this->modx->newQuery('msAttributeGroup');
            $q2->select(array(
                'msAttributeGroup.*',
                'Vendor.id as vendorid'
            ));
            $q->where(['msProduct.deleted' => 0]);
            $q2->prepare();
            $q2->stmt->execute();
            $result_group = $q2->stmt->fetchAll(PDO::FETCH_ASSOC);

            $product_csv = "id~url~наименование~артикул~минцена~максцена~цена за кг~краткое описание~родитель~дата создания~производитель~опции(в json формате)~макс скидка 5%~обновление";
            $product_foto_csv = "id~url~наименование~артикул~фото~родитель~хеш;\r\n";

            foreach ($result_group as $attr) {
                $product_csv .= "~" . $attr['name'] . '(' . $attr['key'] . ')';
            }
            $product_csv .= ";\r\n";

            foreach ($result as $item) {
                $q1 = $this->modx->newQuery('msAttributeProduct');
                $q1->select(array(
                    //'msAttributeProduct.*',
                    'Attribute.name as attr_name',
                    'Groupattr.name as group_name',
                    'Groupattr.key as group_key',
                ));
                //$q1->leftJoin('msAttributeProduct', 'AttributePr', 'Data.id = AttributePr.product_id');
                $q1->leftJoin('msAttribute', 'Attribute', 'msAttributeProduct.attribute_id = Attribute.id');
                $q1->leftJoin('msAttributeGroup', 'Groupattr', 'Attribute.attribute_group = Groupattr.id');
                $q1->where(['msAttributeProduct.product_id:=' => $item['id']]);

                $q1->prepare();
                $q1->stmt->execute();
                $result_attr = $q1->stmt->fetchAll(PDO::FETCH_ASSOC);
                $product_attr = '';
                $options = urlencode($item['value']);
                $_options = json_decode($item['value'], true);
                $_opt = $_options;
                foreach ($_options as $key => $opt) {
                    if (isset($opt['action_hide1c'])) {
                        if ($opt['action_hide1c'] == '1') {
                            $_opt[$key]['action'] = 0;
                        }
                    }

                }

                $options = urlencode(json_encode($_opt));
                $intro = urlencode($item['introtext']);
                $maxskidka = 0;
                if ($item['vendor'] == 20) {
                    $maxskidka = 1;
                }

                $content_photo = file_get_contents($root . $item['image'], FILE_USE_INCLUDE_PATH);
                $hashphoto = sha1($content_photo);


                $date = $item['editedon'];//msProduct_editedon

                $product_csv .= $item['contentid'] . "~" . $item['uri'] . "~" . $item['pagetitle'] . "~" . $item['article'] . "~" . $item['price'] . "~" . $item['price_max'] . "~" . $item['price_kg'] . "~" . $intro . "~" . $item['parent'] . "~" . $item['createdon'] . "~" . $item['name'] . "~" . $options . "~" . $maxskidka . "~" . $date;
                $product_foto_csv .= $item['contentid'] . "~" . $item['uri'] . "~" . $item['pagetitle'] . "~" . $item['article'] . "~" . $item['image'] . "~" . $item['parent'] . "~" . $hashphoto . ";\r\n";
                $count_attr_pr = 0;
                foreach ($result_group as $attr) {
                    foreach ($result_attr as $attr_pr) {
                        if ($attr_pr['group_key'] == $attr['key']) {
                            $product_csv .= "~" . $attr['name'];
                            $count_attr_pr++;
                        }
                    }
                }

                if ($count_attr_pr < count($result_group)) {

                    $for_count = count($result_group) - $count_attr_pr;

                    for ($i = 0; $for_count < $i; $i++) {
                        $product_csv .= "~";
                    }

                }
                $product_csv .= ";\r\n";
//break;
            }


            $handle = fopen($pathfile, "w+");

            fwrite($handle, header('Content-Type: text/html; charset=utf8'));
            if (fwrite($handle, $product_csv)) {
                echo 'ok';
                fclose($handle);

                $handle_photos = fopen($pathfile_photos, "w+");
                fwrite($handle_photos, header('Content-Type: text/html; charset=utf8'));
                fwrite($handle_photos, $product_foto_csv);
                fclose($handle_photos);

                return true;
            } else return false;
        }
        return false;
    }


    /**
     * получение пользвоателей для 1с
     * @param $root
     * @param bool $limit
     * @return bool
     * file - если true то необходимо сформировать и сохранит файл, если false  то необходимо в отведе выдать файл
     */
    public
    function getUsers($root, $limit = false, $create_file = true)
    {
        $file = 'usersall.csv';
        $pathfile = $root . $this->path1c . $file;

        if (!file_exists($pathfile) || (!$create_file)) {

            $q = $this->modx->newQuery('modUser');//


            $q->select(array(
                'modUserProfile.email',
                'modUser.username',
                'modUserProfile.phone',
                'modUserProfile.mobilephone',
                'modUser.id',
                'modUser.summa_all_orders',
                'modUser.total_orders',
                'modUser.registr',
                'modUser.bonuses',
                'modUserProfile.fullname',
                'modUserProfile.extended'
            ));

            $q->innerJoin('modUserProfile', 'modUserProfile', 'modUser.id=modUserProfile.internalKey');
            $q->groupby('modUser.id');
            $q->prepare();
            $q->stmt->execute();

            $result = $q->stmt->fetchAll(PDO::FETCH_ASSOC);
            $user_csv_array = [];
            $user_csv = '';
            $user_csv_array[] = "имя~телефон~доптелефон~email~лояльность~зарегистрирован/не зарегистрирован~сумма покупок~количество заказов~доставка~бонусы;";
            //$user_csv = "имя~телефон~доптелефон~email~лояльность~зарегистрирован/не зарегистрирован~сумма покупок~количество заказов~доставка~бонусы;\r\n";
            foreach ($result as $res) {
                $totalorder = 0;
                $countorder = 0;


                $totalorder = $res['summa_all_orders'];
                $countorder = $res['total_orders'];
                //total_orders
                $extended = $this->modx->fromJson($res['extended']);
                if (isset($extended['discount']))
                    $discount = $extended['discount'];
                else $discount = 0;

                $delivery = '';
                if (isset($extended['delivery'])) {
                    $delivery_id = $extended['delivery'];

                    $q = $this->modx->newQuery('msDelivery');
                    $q->select('*');
                    $q->where(array('msDelivery.id:=' => $delivery_id));
                    $q->prepare();
                    $q->stmt->execute();
                    $delivery_m = $q->stmt->fetch(PDO::FETCH_ASSOC);

                    if ($delivery_m && (count($delivery_m) > 0)) $delivery = $delivery_m['name'];

                }

                //$user_csv .= $res['fullname'] . "~" . $res['username'] . "~" . $res['mobilephone'] . "~" . $res['email'] . "~" . $discount . "~" . $res['registr'] . "~" . $totalorder . "~" . $countorder . "~" . $delivery . "~" . $res['bonuses'] . ";\r\n";
                $user_csv_array[] = $res['fullname'] . "~" . $res['username'] . "~" . $res['mobilephone'] . "~" . $res['email'] . "~" . $discount . "~" . $res['registr'] . "~" . $totalorder . "~" . $countorder . "~" . $delivery . "~" . $res['bonuses'] . ";";//\r\n";

            }


            if ($create_file) {
                $user_csv = implode("\r\n", $user_csv_array);
                $handle = fopen($pathfile, "w+");

                //fwrite($handle, header('Content-Type: text/html; charset=utf8'));
                if (fwrite($handle, $user_csv)) {
                    echo 'ok';
                    fclose($handle);
                    return true;
                }

                fclose($handle);
            } else {

                $now = gmdate("D, d M Y H:i:s");
                header("Expires: Tue, 03 Jul 2001 06:00:00 GMT");
                header("Cache-Control: max-age=0, no-cache, must-revalidate, proxy-revalidate");
                header("Last-Modified: {$now} GMT");

                // force download
                header("Content-Type: application/force-download");
                header("Content-Type: application/octet-stream");
                header("Content-Type: application/download");

                // disposition / encoding on response body
                header("Content-Disposition: attachment;filename={$file}");
                header("Content-Transfer-Encoding: binary");


                echo $user_csv = implode("\r\n", $user_csv_array);

                //echo $this->array2csv($user_csv_array);
                die();
            }
            //else return false;
        }
        return false;

    }

    /**
     * @param array $array
     * @return null|string
     * из массива в  цсв для скачивания
     */
    public
    function array2csv(array &$array)
    {
        if (count($array) == 0) {
            return null;
        }
        ob_start();
        $df = fopen("php://output", 'w');
        fputcsv($df, array_keys(reset($array)));
        foreach ($array as $row) {
            //  echo $row.'<br/>';
            fputcsv($df, $row);
        }
        fclose($df);
        return ob_get_clean();
    }


    public
    function updatePoNalishiy()
    {
        $q = $this->modx->newQuery('msProduct');
        $q->select(array(
            'msProduct.id',
            //'Data.*',
            'TV.contentid',
            'TV.value',
            'TV.tmplvarid',
            'Vendor.po_nilishiy'
        ));

        $q->innerJoin('msProductData', 'Data', 'msProduct.id = Data.id');
        $q->leftJoin('modTemplateVarResource', 'TV', 'msProduct.id = TV.contentid AND TV.tmplvarid= 1');
        $q->leftJoin('msVendor', 'Vendor', 'Data.vendor = Vendor.id');

        $q->where(['Vendor.po_nilishiy:=' => 1]);
        $q->groupby('msProduct.id');
        $q->prepare();
        $q->stmt->execute();
        $result = $q->stmt->fetchAll(PDO::FETCH_ASSOC);


        $i = 1;
        $products_for_disabled = [];

        foreach ($result as $product) {


            $avalable_for_enable_product = [];
            $options = $product['value'];
            $id_option = $this->modx->fromJSON($options);

            $new_option = $id_option;

            $valMIGX = $product['value'];

            foreach ($id_option as $key => $op) {
                //if ($op['MIGX_id'] == $MIGX_id) {

                $id_1c = $op['id_1c'];
                $store = 'availability_1';
                $available = $op['availability_1'];
                $avalable_for_enable_product[$key][$store] = $available;

                $store = 'availability_2';
                $available = $op['availability_2'];
                $avalable_for_enable_product[$key][$store] = $available;

                $store = 'availability_3';
                $available = $op['availability_3'];
                $avalable_for_enable_product[$key][$store] = 0;//$available;


            }

            if ($product['po_nilishiy'] == '1') {
                foreach ($avalable_for_enable_product as $key => $value) {


                    //availability_3
                    if (isset($value['availability_1']) && isset($value['availability_2'])) {
                        if (($value['availability_1'] == '0') && ($value['availability_2'] == '0')) {
                            $new_option[$key]['instock'] = 0;
                            $products_for_disabled[$product['contentid']][] = '0';
                        } else//if ($value['Магазин 2']>0)
                        {
                            $new_option[$key]['instock'] = 1;
                            $products_for_disabled[$product['contentid']][] = '1';
                        }
                    }

                }

            }


            if (isset($new_option[$key]['bystock'])) {
                if ($new_option[$key]['bystock'] == '1') {
                    foreach ($avalable_for_enable_product as $key => $value) {

                        //availability_3
                        if (isset($value['availability_1']) && isset($value['availability_2'])) {
                            if (($value['availability_1'] == '0') && ($value['availability_2'] == '0')) {
                                //$new_option[$key]['disable_option']=1;
                                $new_option[$key]['instock'] = 0;
                                //$products_for_disabled[$product['contentid']][] = '0';
                            } else {
                                //$new_option[$key]['disable_option']=0;
                                $new_option[$key]['instock'] = 1;
                            }

                        }

                    }


                }
            }

            $new_option = $this->modx->toJSON($new_option);

            $nodes = $this->modx->getIterator('modTemplateVarResource',
                array('contentid' => $product['contentid'], 'tmplvarid' => 1));

            foreach ($nodes as $node) {
                $node->set('value', $new_option);
                $node->save();
            }


            //}
            flush();
            ob_flush();
            $i++;
        }


        if (count($products_for_disabled) > 0) {
            foreach ($products_for_disabled as $product_id => $data_option) {

                if (in_array('1', $data_option)) {
                    // disable product by id
                    $product_save = $this->modx->getObject('msProduct', $product_id);
                    $product_save->set('published', 1);
                    $product_save->save();

                } else {
                    $product_save = $this->modx->getObject('msProduct', $product_id);
                    $product_save->set('published', 0);
                    $product_save->save();
                }
            }
        }

    }

    public
    function UploadAvailable($root)
    {
        $file = 'available.csv';
//        ИдТовара;ИдСклада;НазваниеСклада;Остаток;ИдОпции

        $stores['467eaf2b-2912-11e7-93cf-902b34b73c15'] = 'availability_2';
        $stores['12c9d983-d603-11e8-9f17-74d435d53536'] = 'availability_3';
        $stores['f843176b-2913-11e7-93cf-902b34b73c15'] = 'availability_1';

        $uploadfile = $root . $this->path1c . 'from_1c/' . $file;
        if (file_exists($uploadfile)) {
            $delimeter = ";";
            $handle = fopen($uploadfile, "r");
            $rows = 0;
            $stocks = [];
            $ids = [];
            while (($csv = fgetcsv($handle, 0, $delimeter)) !== false) {

                if ($rows > 0) {

                    $id_product = $csv[0];
                    $id_sklad = $csv[1];
                    $stock = $csv[3];
                    if (in_array($id_product, $ids) === false) $ids[] = $id_product;
                    if (isset($csv[4]) && ($csv[4] != ''))
                        $stocks[$id_product][] = ['available' => $stock, 'MIGX_id' => $csv[4], 'sklad' => $id_sklad];
                }
                $rows++;
            }
            /* echo "<pre>ids";
             print_r($ids);
             echo "</pre>";*/
            $q = $this->modx->newQuery('msProduct');
            $q->select(array(
                'msProduct.id',
                //'Data.*',
                'TV.contentid',
                'TV.value',
                'TV.tmplvarid',
                'Vendor.po_nilishiy'
            ));

            $q->innerJoin('msProductData', 'Data', 'msProduct.id = Data.id');
            $q->leftJoin('modTemplateVarResource', 'TV', 'msProduct.id = TV.contentid AND TV.tmplvarid= 1');
            $q->leftJoin('msVendor', 'Vendor', 'Data.vendor = Vendor.id');

            $q->where(['msProduct.id:IN' => $ids]);
            $q->groupby('msProduct.id');
            $q->prepare();
            $q->stmt->execute();
            $result = $q->stmt->fetchAll(PDO::FETCH_ASSOC);


            $i = 1;
            $products_for_disabled = [];
            //echo '<br/><br/>count'.count($result);//die();
            foreach ($result as $product) {
                //echo $i.' '.$product['contentid'].'<br/>';
                //echo $i.'<br/>';
                if (isset($stocks[$product['contentid']])) {


                    $avalable_for_enable_product = [];
                    $options = $product['value'];
                    $id_option = $this->modx->fromJSON($options);

                    $new_option = $id_option;
                    foreach ($stocks[$product['contentid']] as $ava) {

                        $MIGX_id = $ava['MIGX_id'];
                        $available = $ava['available'];
                        $store = $stores[$ava['sklad']];

                        foreach ($id_option as $key => $op) {
                            if ($op['MIGX_id'] == $MIGX_id) {
                                $new_option[$key][$store] = $available;

                                if ($store == 'availability_3') {
                                    $avalable_for_enable_product[$key][$store] = 0;
                                } else {
                                    $avalable_for_enable_product[$key][$store] = $available;
                                }
                            }
                        }
                    }

                    if ($product['po_nilishiy'] == '1') {
                        foreach ($avalable_for_enable_product as $key => $value) {


                            //availability_3
                            if (isset($value['availability_1']) && isset($value['availability_2'])) {
                                if (($value['availability_1'] == '0') && ($value['availability_2'] == '0')) {
                                    $new_option[$key]['instock'] = 0;
                                    $products_for_disabled[$product['contentid']][] = '0';
                                } else//if ($value['Магазин 2']>0)
                                {
                                    $new_option[$key]['instock'] = 1;
                                    $products_for_disabled[$product['contentid']][] = '1';
                                }
                            }

                        }

                    }


                    if (isset($new_option[$key]['bystock'])) {
                        if ($new_option[$key]['bystock'] == '1') {
                            foreach ($avalable_for_enable_product as $key => $value) {

                                //availability_3
                                if (isset($value['availability_1']) && isset($value['availability_2'])) {
                                    if (($value['availability_1'] == '0') && ($value['availability_2'] == '0')) {
                                        //$new_option[$key]['disable_option']=1;
                                        $new_option[$key]['instock'] = 0;
                                        //$products_for_disabled[$product['contentid']][] = '0';
                                    } else {
                                        //$new_option[$key]['disable_option']=0;
                                        $new_option[$key]['instock'] = 1;
                                    }

                                }

                            }


                        }
                    }

                    $new_option = $this->modx->toJSON($new_option);

                    $nodes = $this->modx->getIterator('modTemplateVarResource',
                        array('contentid' => $product['contentid'], 'tmplvarid' => 1));

                    foreach ($nodes as $node) {
                        $node->set('value', $new_option);
                        $node->save();
                    }


                }
                flush();
                ob_flush();
                $i++;
            }


            // disbaled products
            if (count($products_for_disabled) > 0) {
                foreach ($products_for_disabled as $product_id => $data_option) {

                    if (in_array('1', $data_option)) {
                        // disable product by id
                        $product_save = $this->modx->getObject('msProduct', $product_id);
                        $product_save->set('published', 1);
                        $product_save->save();

                    } else {
                        $product_save = $this->modx->getObject('msProduct', $product_id);
                        $product_save->set('published', 0);
                        $product_save->save();
                    }
                }
            }

            unlink($uploadfile);
            return true;
        }

        return false;
    }

    public
    function UploadSales($root)
    {
        $file = 'sales.csv';

        $uploadfile = $root . $this->path1c . 'from_1c/' . $file;
        if (file_exists($uploadfile)) {
            $delimeter = ";";

            $handle = fopen($uploadfile, "r");
            $rows = 0;

            while (($csv = fgetcsv($handle, 0, $delimeter)) !== false) {

                if ($rows > 0) {
                    $user_phone = $csv[0];
                    $id_1c = $csv[1];
                    $date_order = $csv[2];
                    $product_id = $csv[3];
                    $id_option = $csv[4];
                    $count = $csv[5];
                    $cost = $csv[6];

                    $objuser = $this->modx->getObject('modUser', ['username' => $user_phone]);

                    $objorder = $this->modx->newObject('msOrder');
                    $objorder->set('user_id', $objuser->id);
                    $objorder->set('createdon', $date_order);
                    $objorder->set('updatedon', $date_order);
                    $objorder->set('num', 'Магазин');
                    $objorder->set('cost_without_bonuses', $cost);
                    $objorder->set('cost', $cost);
                    $objorder->set('status', 2);
                    $objorder->set('cart_cost', $cost);
                    $objorder->set('cost', $cost);
                    $objorder->set('id_1c', $id_1c);

                    $objorder->save();
                    $total_max_forbonuses = 0;
                    if ($objuser) {

                        $limit_bonuses = $this->modx->getOption('limit_bonuses') / 100;
                        $total_max_forbonuses = round($cost * $limit_bonuses, 0); // максимум оплатить можно до 30% заказа


                        $totalorder = $objuser->get('summa_all_orders') + $cost;
                        $objuser->set('summa_all_orders', $totalorder);

                        $totalorder_close = $objuser->get('summa_close_orders') + $cost;
                        $objuser->set('summa_close_orders', $totalorder_close);

                        $q = $this->modx->newQuery('msProduct');
                        $q->select(array(
                            'TV.*'
                        ));
                        $q->innerJoin('modTemplateVarResource', 'TV', 'msProduct.id = TV.contentid AND TV.tmplvarid= 1');
                        $q->where(array('TV.contentid' => $product_id));

                        $q->prepare();
                        $q->stmt->execute();
                        $options_result = $q->stmt->fetchAll(PDO::FETCH_ASSOC);
                        $user_bonuses = 0;
                        foreach ($options_result as $opt) {
                            $value_opt = $opt['value'];
                            $value_opt_ar = $this->modx->fromJSON($value_opt);
                            $id = $value_opt_ar['MIGX_id'];
                            if ($id == $id_option) {
                                if (isset($value_opt_ar['bonuses'])) {
                                    $bonuses = $value_opt_ar['bonuses'];
                                    if ($bonuses > 0) {
                                        $user_bonuses = $user_bonuses + ($count * $bonuses);
                                    }

                                }

                            }

                        }
                        $bonuses_exist = $objuser->get('bonuses');
                        $us_bonuses = $bonuses_exist;

                        /*if ($total_max_forbonuses>0)
                        {
                            $us_bonuses=$bonuses_exist-$total_max_forbonuses;
                            //$us_bonuses=$bonuses_exist;
                        }*/
                        if ($user_bonuses > 0) {
                            $us_bonuses = $us_bonuses + $user_bonuses;
                        }

                        $objuser->set('bonuses', $us_bonuses);
                        $objuser->save();
                    }
                }
                $rows++;
            }


            unlink($uploadfile);
            return true;
        }

        return false;
    }

    public
    function UploadSalesBonuses($root)
    {
        $file = 'sales_bonuses.csv';
//ИдПользователя;Идчека;ДатаОперации;СуммаБонусов
        $uploadfile = $root . $this->path1c . 'from_1c/' . $file;
        if (file_exists($uploadfile)) {
            $delimeter = ";";

            $handle = fopen($uploadfile, "r");
            $rows = 0;

            while (($csv = fgetcsv($handle, 0, $delimeter)) !== false) {

                if ($rows > 0) {
                    $user_phone = $csv[0];
                    $id_1c = $csv[1];
                    $date_order = $csv[2];
                    $bonuses = $csv[3];
                    $objuser = false;
                    if ($bonuses > 0) {

                        $objorder = $this->modx->getObject('msOrder', ['id_1c' => $id_1c]);
                        if ($objorder) {
                            $objorder->set('with_bonuses', '1');
                            $objorder->set('apply_bonuses', '1');
                            $objorder->set('bonuses', $bonuses);
                            $objorder->set('date_apply_bonuses', $date_order);
                            $summa_order = $objorder->get('cart_cost');
                            $total = $summa_order - $bonuses;

                            $objorder->set('cart_cost', $total);
                            $objorder->set('cost', $total);
                            $objorder->save();
                        }
                    }

                }
                $rows++;
            }


            unlink($uploadfile);
            return true;
        }

        return false;
    }

//id_1c


    public
    function getDataForTemplate($params_for_templ = [], $attribute = false)
    {
        //'counter','min_price','top_brands'
        $idcat = $this->modx->resource->get('id');
        $q = $this->modx->newQuery('msProduct');
        $q->innerJoin('msProductData', 'msProductData', '`msProductData`.`id` = `msProduct`.`id`');
        $q->leftJoin('msVendor', 'msVendor', '`msVendor`.`id` = `msProductData`.`vendor`');

        $where = [
            'msProduct.deleted' => 0,
            'msProduct.parent' => $idcat,
            //    'msProduct.published' => 1
        ];

        if ($attribute) {
            // echo 'aaa'.$attribute->get('id');
            $q->innerJoin('msAttributeProduct', 'msAttributeProduct', '`msAttributeProduct`.`product_id` = `msProduct`.`id`');
            $where['msAttributeProduct.attribute_id'] = $attribute->get('id');

        }


        $q->groupby('msProduct.id');
        $q->select(array('msProduct.id', '`msProductData`.`price`'));
        $q->where($where);
        $counts = 0;
        $ids1 = [];
        $top_brands = '';
        $prices = [];
        if ($q->prepare() && $q->stmt->execute()) {
            while ($row = $q->stmt->fetch(PDO::FETCH_ASSOC)) {
                $ids1[] = $row['id'];
                if ($row['price'] > 0) $prices[] = $row['price'];
                $counts++;
            }
        }

        if (in_array('top_brands', $params_for_templ)) {
            $q = $this->modx->newQuery('msProduct');
            $q->innerJoin('msProductData', 'msProductData', '`msProductData`.`id` = `msProduct`.`id`');
            $q->innerJoin('msVendor', 'msVendor', '`msProductData`.`vendor` = `msVendor`.`id`');

            $where = ['msProductData.id:IN' => $ids1];
            if ($attribute) {
                $q->innerJoin('msAttributeProduct', 'msAttributeProduct', '`msAttributeProduct`.`product_id` = `msProduct`.`id`');
                $where['msAttributeProduct.attribute_id'] = $attribute->get('id');

            }

            $q->where($where);

            $q->select(array('msProductData.vendor', 'msProductData.id', 'msProductData.hits', 'msVendor.name'));
            $q->groupby('msProductData.id');

            $rowspr = array();
            $prosmotri = array();
            if ($q->prepare() && $q->stmt->execute()) {
                while ($row1 = $q->stmt->fetch(PDO::FETCH_ASSOC)) {
                    foreach ($row1 as $k1 => $v1) {

                        if ($row1['vendor'] != 0) {
                            if (isset($prosmotri[$row1['vendor']])) {
                                $prosmotri[$row1['vendor']] = $prosmotri[$row1['vendor']] + $row1['hits'];

                            } else {
                                $prosmotri[$row1['vendor']] = $row1['hits'];
                            }
                            $vendors[$row1['name']] = $prosmotri[$row1['vendor']];//array($row1['name'],$prosmotri);
                        }

                    }
                }
            }
            /*    echo "<pre>vendors";
                print_r($vendors);
                echo "</pre>";*/
            arsort($vendors);
            $i = 1;
            foreach ($vendors as $key => $ven) {
                $topbrands[] = $key;
                if ($i == 3) break;
                $i++;
            }
            $top_brands = implode(', ', $topbrands);
        } else $top_brands = false;

        $min_price = min($prices);
        $minprice = number_format($min_price, 0, '', '') . ' грн.';
        $counts_text = $counts . ' ' . $this->plural_form($counts, ['товар', 'товара', 'товаров']);

        return ['min_price' => $minprice,
            'top_brands' => $top_brands,
            'count_in_cat' => $counts_text];
    }


    public
    function plural_form($n, $forms)
    {
        return $n % 10 == 1 && $n % 100 != 11 ? $forms[0] : ($n % 10 >= 2 && $n % 10 <= 4 && ($n % 100 < 10 || $n % 100 >= 20) ? $forms[1] : $forms[2]);
    }

    public
    function filterTemplatePhone($text, $params = [])
    {
        $keysForReplace = $valuesForReplace = [];
        foreach ($params as $key => $value) {
            if (!is_array($value)) {
                //     $text=$this->filterTemplatePhone($text, $params);
                //}else {
                $keysForReplace[] = '{{' . $key . '}}';
                $valuesForReplace[] = $value;
            }
        }
        //echo '11111111'.$text."\r\n\r\n";

        $text = str_replace($keysForReplace, $valuesForReplace, $text);
        //echo '11111111'.$text."\r\n\r\n";
        return $text;
    }


    public
    function eventNotification($type, $user, $data = [], $addParams = [])
    {
        $success = true;

        $notification = $this->getNotificationByType($type);

        $phone = false;
        if (!isset($data['phone'])) {
            $phone = $user->get('username');
            $data['phone'] = $phone;
        }
        //if ($phone)
        $data['phoneSms'] = $data['phone'];

        if ($this->modx->getOption('debug_notification')) {// && ($user->get('username') != $this->modx->getOption('debug_notification_phone'))) {
            $phone = $data['phone'] = $this->modx->getOption('debug_notification_phone');
            //$data['phoneSms']=$data['phone'];
        }


        if ($notification) {
            $is_transactional = false;
            //echo $notification['msNotifications_id'];
            //$notificationModel=$this->modx->getObject('msNotifications',$notification['msNotifications_id']);

            //echo $notification['msNotifications_id'].' '.$notificationModel->get('email');
            //echo 'chank_email'.$notification['msNotifications_chank_email'];//->get('chank_email')
            if ($notification['msNotifications_email'] == 1) {


                $chunk = $notification['msNotifications_chank_email'];//->get('chank_email');

                $body = $this->modx->getChunk($chunk, $data);

                $subject = $notification['msNotifications_email_subject'];//->get('email_subject');
                $subject = $this->filterTemplatePhone($subject, $data);
                if (!isset($data['email'])) {

                    //echo '';
                    if ($profile = $user->getOne('Profile')) {

                        $email = $profile->get('email');
                    }
                } else $email = $data['email'];

//                $chunk_manager = 'purchaseReminder_manager';
//                $body_manager = 'Почта: ' . $email . ' Шаблон: </br></br>' . $this->modx->getChunk($chunk_manager, array('linkprofile' => $linkprofile,
//                        'good' => $good,
//                        'fullname' => $fullname));
//                $subject = 'Напоминание о покупке корма';


                $sendEmail = true;
                $trans_chanks = [
                    'email_bonus_accrual',
                    'email_alert_burn_bonus',
                    'tpl.msEmail.podtverg.user'
                ];
                //
                if (($user->get('subscribe_email_info') == 0) && (!in_array($chunk, $trans_chanks))) {
                    $sendEmail = false;
                }

                if (($user->get('subscribe_email_trans') == 1) && (in_array($chunk, $trans_chanks)))
                    $sendEmail = true;
                elseif (($user->get('subscribe_email_trans') == 0) && (in_array($chunk, $trans_chanks))) {
                    $sendEmail = false;
                }

                //echo '$email = '.$email."\r\n";
                if ($email != '') {
                    $emailAdmin = $this->modx->getOption('email_admin');//'$this->modx->getOption(\'debug_notification\')'
                    //if (strpos(',',$emailAdmin)!==false){
                    $_emailsAdmin = explode(',', $emailAdmin);

                    if ($sendEmail) {
                        if ($this->sendEmail($email, $subject, $body)) {
                            $success = true;
                            foreach ($_emailsAdmin as $_emailAdmin) {
//                                    echo 'success' . $_emailAdmin . "<br/>";
//                                    echo 'отправилось письмо клиенту. Тема: "' . $subject . '"', 'Почта: ' . $email . '. Шаблон письма: ' . $body;
                                $this->sendEmail($_emailAdmin, 'отправилось письмо клиенту. Тема: "' . $subject . '"', 'Почта: ' . $email . '. Шаблон письма: ' . $body);
                            }

                        } else {
                            $success = false;
                            foreach ($_emailsAdmin as $emailAdmin) {
                                // echo 'error' . $emailAdmin . "<br/>";
                                //echo 'отправилось письмо клиенту. Тема: "' . $subject . '"', 'Почта: ' . $email . '. Шаблон письма: ' . $body;

                                $this->sendEmail($emailAdmin, 'ошибка отправки письмо клиенту. Тема: "' . $subject . '"', 'Почта: ' . $email . '. Шаблон письма: ' . $body);
                            }
                        }
                    }
                }

            }

            $caption = $action = '';
            $caption = $notification['msNotifications_caption'];//->get('caption');//'Купить';//$product['name'];
            $action = $notification['msNotifications_action_button'];//->get('action');;

            $image = false;
            if ($notification['msNotifications_image_include'] == 1)///;->get('image_include')
            {
                $image = $notification['msNotifications_image_url'];//->get('image_url');
                if ($image == '') {
//                    if (isset($addParams['image'])) {
//                        $image = $addParams['image'];
//                    }
//                    else
                    if (isset($addParams['product'])) {
                        $product_model = $addParams['product'];
                        $image = $product_model->get('image');//$product['image'];
                        $caption = $notification['msNotifications_caption'];//->get('caption');//'Купить';//$product['name'];
                        $action = $notification['msNotifications_action_button'];//->get('action');;
                        if (empty($action)) {

                            if (!isset($addParams['link'])) {
                                $action = $this->modx->makeUrl($product_model->get('product_id'), '', '', 'full');
                            } else {
                                $action = $addParams['link'];
                            }
                        }


                    }
                }
            }


            if (isset($addParams['action'])) {
                if (($addParams['action'] == 'liqpay') && (isset($addParams['liqpay_link']))) {
                    //['action'=>'liqpay','liqpay_link'=>$link]
                    $caption = $notification['msNotifications_caption'];
                    $action = $notification['msNotifications_action_button'];
                    if (empty($action)) {
                        $action = $addParams['liqpay_link'];
                    }
                }


            }


            if (!isset($data['phone'])) {
                //$phone = $user->get('username');
                $data['phone'] = $phone;
                $data['phoneSms'] = $phone;
            }

            $ttl = false;
            if (isset($addParams['ttl']))
                $ttl = $addParams['ttl'];


            if (($notification['msNotifications_viber'] == 1) && ($notification['msNotifications_sms'] == 1)) {


                if ($user->get('subscribe_viber') == 1) {
                    $limit = $notification['msNotifications_limit'];
                    $counter = $notification['msNotifications_counter'];
                    if ($counter < $limit) {
                        if ($notification['msNotifications_is_transactional'] == 1)
                            $is_transactional = true;
                        $text = $this->filterTemplatePhone($notification['msNotifications_text_viber'], $data);
                        $textSms = $this->filterTemplatePhone($notification['msNotifications_text_sms'], $data);
                        if ($notificationObject = $this->modx->getObject('msNotifications', $notification['msNotifications_id'])) {
                            $counter++;
                            $notificationObject->set('counter', $counter);
                            $notificationObject->save();
                        }

                        $this->sendsms($phone, $text, $image, 'gibrid', $caption, $action, $textSms, $data['phoneSms'], $user, $is_transactional, $ttl);
                    }
                }
            } elseif ($notification['msNotifications_viber'] == 1) {
                if ($user->get('subscribe_viber') == 1) {
                    $text = $this->filterTemplatePhone($notification['msNotifications_text_viber'], $data);

                    $textSms = $this->filterTemplatePhone($notification['msNotifications_text_sms'], $data);

                    // echo '$phone viber = '.$phone."\r\n\r\n";
                    //die();

                    $limit = $notification['msNotifications_limit'];
                    $counter = $notification['msNotifications_counter'];
                    if ($counter < $limit) {
                        if ($notification['msNotifications_is_transactional'] == 1)
                            $is_transactional = true;

                        $this->sendsms($phone, $text, $image, 'viber', $caption, $action, $textSms, $data['phoneSms'], $user, $is_transactional, $ttl);

                        if ($notificationObject = $this->modx->getObject('msNotifications', $notification['msNotifications_id'])) {
                            $counter++;
                            $notificationObject->set('counter', $counter);
                            $notificationObject->save();
                        }
                    }
                }
            } elseif ($notification['msNotifications_sms'] == 1) {
                if ($user->get('subscribe_sms') == 1) {

                    $text = $this->filterTemplatePhone($notification['msNotifications_text_sms'], $data);
                    if ($notification['msNotifications_is_transactional'] == 1)
                        $is_transactional = true;
                    $this->sendsms($data['phoneSms'], $text, false, 'sms', '', '', '', '', '', $is_transactional, $ttl);
                }
            }

        }

        return $success;

    }

    public
    function clearCounterNotification()
    {

        $q = $this->modx->newQuery('msNotifications');

        $q->select('*');

        $q->prepare();
        $sql = $q->toSQL();
        $q->stmt->execute();
        $result = $q->stmt->fetchAll(PDO::FETCH_ASSOC);

        foreach ($result as $not) {

            if ($object = $this->modx->getObject('msNotifications', $not['msNotifications_id'])) {
                echo 'counter' . $object->get('counter') . "\r\n\r\n";
                $object->set('counter', 0);
                $object->save();
            }

        }

    }
}


/**
 * Класс для работы с хэшами изображений
 *
 * @author mihanentalpo
 */
class ImageHash
{
    /**
     * Построить хэш изображения из файла
     *
     * @param string $imageFile Файл изображения
     * @param integer $pHashSizeRoot Квадратный корень из размера хэша. От 4 и больше. Чем больше - тем точнее сравнение.
     * @param integer $pHashDetalization детализация хэша, от 2 до 6. Чем больше - тем точнее сравнение.
     * @param boolean $toBase64 Преобразовать результат в base64?
     * @return string хэш изображения
     */
    public function createHashFromFile($imageFile, $pHashSizeRoot = 10, $pHashDetalization = 3, $toBase64 = true)
    {
        return $this->createHashFromFileContents(file_get_contents($imageFile), $pHashSizeRoot, $pHashDetalization, $toBase64);
    }

    /**
     * Построить хэш изображения из строки, содержащей содержимое загруженного файла изображения
     *
     * @param string $imageFileContents содержимое файла
     * @param integer $pHashSizeRoot Квадратный корень из размера хэша. От 4 и больше. Чем больше - тем точнее сравнение.
     * @param integer $pHashDetalization детализация хэша, от 2 до 6. Чем больше - тем точнее сравнение.
     * @param boolean $toBase64 Преобразовать результат в base64?
     * @return string хэш изображения
     */
    public function createHashFromFileContents($imageFileContents, $pHashSizeRoot = 10, $pHashDetalization = 3, $toBase64 = true)
    {
        $image = imagecreatefromstring($imageFileContents);
        return $this->createHash($image, $pHashSizeRoot, $pHashDetalization, $toBase64);
    }

    /**
     * Ограничить значение минимумом и максимумом
     * @param integer $value
     * @param integer $min
     * @param integer $max
     * @return integer
     */
    protected function limit($value, $min, $max)
    {
        return $value > $max ? $max : ($value < $min ? $min : $value);
    }

    /**
     * Проверить, является ли переданная переменная изображением
     * @param resource $image Изображение
     * @param string $type Переменная, куда будет записано название типа переменной, если она будет не изображением
     * @return type
     */
    protected function isImage($image, &$type)
    {
        $x = @imagesx($image);
        if (!$x) {
            $type = "";
            switch (true) {
                case is_string($image):
                    $type = "string '" . substr($image, 0, 30) . "...'";
                    break;
                case is_numeric($image):
                    $type = "number '" . (float)$image . "'";
                    break;
                case is_object($image):
                    $type = "object of class '" . get_class($image) . "'";
                    break;
                case is_resource($image):
                    $type = "some other resource";
                    break;
                case is_array($image):
                    $type = "array of " . count($image) . " elements";
                    break;
                case true:
                    $type = "not";
                    break;
            }
        }
        return !!$x;
    }

    /**
     * Построить хэш изображения для быстрого сравнения схожести изображений.
     * Позволяет быстро определять схожесть изображений, которые являются одним
     * и тем-же изображением, но, например, с изменённым размером и пропорциями,
     * или с немного подкорректированными цветами.
     *
     * @param string $image Изображение
     * @param integer $pHashSizeRoot Квадратный корень из размера хэша. От 4 и больше. Чем больше - тем точнее сравнение.
     * @param integer $pHashDetalization детализация хэша, от 2 до 6. Чем больше - тем точнее сравнение.
     */
    public function createHash($image, $pHashSizeRoot = 10, $pHashDetalization = 3, $toBase64 = true)
    {
        if (!$this->isImage($image, $type)) {
            throw new Exception("\$image argument should be image resource, but it's " . $type);
        }
        $hashDetalization = $this->limit($pHashDetalization, 2, 6, true);
        $hashSizeRoot = $this->limit($pHashSizeRoot, 4, 50, true);
        $width = imagesx($image);
        $height = imagesy($image);
        $size = array($width, $height);
        $littleSize = $hashSizeRoot;
        //Цветов на один пиксел (число от 8 до 216)
        $colorsPerPixel = pow($hashDetalization, 3);
        //Цветов на один канал
        $colorsPerChannel = $hashDetalization;
        //Отрезок цветового канала, пропорциональный единице из упрощённого цветового канала
        $channelDivision = 256 / $colorsPerChannel;
        $colorSimplify = function ($color) use ($colorsPerPixel, $colorsPerChannel, $channelDivision) {
            //Разбиваем цвет на красный, синий и зелёный
            $r = ($color >> 16) & 0xFF;
            $g = ($color >> 8) & 0xFF;
            $b = $color & 0xFF;
            //Получаем упрощённые значения цветовых каналов
            $simpleR = floor($r / $channelDivision);
            $simpleG = floor($g / $channelDivision);
            $simpleB = floor($b / $channelDivision);
            $simpleColor = (int)($simpleR + $simpleG * $colorsPerChannel + $simpleB * $colorsPerChannel * $colorsPerChannel);
            if ($simpleColor < 0) $simpleColor = 0;
            if ($simpleColor >= $colorsPerPixel) $simpleColor = $colorsPerPixel - 1;
            return (int)$simpleColor;
        };
        $littleImg = imagecreatetruecolor($littleSize, $littleSize);
        imagecopyresampled($littleImg, $image, 0, 0, 0, 0, $littleSize, $littleSize, $size[0], $size[1]);
        $hash = "";
        for ($i = 0; $i < $littleSize; $i++) {
            for ($j = 0; $j < $littleSize; $j++) {
                $color = imagecolorat($littleImg, $i, $j);
                $simpleColor = $colorSimplify($color);
                $hash .= chr($simpleColor);
            }
        }
        imagedestroy($littleImg);
        imagedestroy($image);
        if ($toBase64) {
            $result = sha1($hash);//base64_encode($hash);
        } else {
            $result = $hash;
        }
        return $result;
    }

    /**
     * Сравнить два хэша изображений
     * @param string $hash1 хэш первого изображения в формате base64
     * @param string $hash2 хэш второго изображения в таком же формате
     * @param float $epsilon Максимальная относительная ошибка. 1 это 100%, 0.5 это 50% и так далее.
     * @param boolean $error Ссылка на переменную, в которую будет записана величина ошибки (число от 0 до 1)
     * @param boolean $base64decoded Флаг, указывающий, что переданные хэши уже декодированы из base64
     * @return boolean возвращает true/false в зависимости от того, соответствуют ли друг другу хэши
     */
    public function compareImageHashes($hash1, $hash2, $epsilon = 0.01, &$error = 0, $base64decoded = false)
    {
        $error = 1;
        if ($epsilon == 0) {
            return $hash1 == $hash2;
        } else {
            if ($hash1 == $hash2) return true;
            if (!$base64decoded) {
                $h1 = base64_decode($hash1);
                $h2 = base64_decode($hash2);
            } else {
                $h1 = $hash1;
                $h2 = $hash2;
            }
            if (strlen($h1) != strlen($h2)) return false;
            $l = strlen($h1);
            $error = 0;
            $bytes1 = unpack("C*", $h1);
            $bytes2 = unpack("C*", $h2);
            for ($i = 0; $i < $l; $i++) {
                $b1 = $bytes1[$i + 1];
                $b2 = $bytes2[$i + 1];
                if ($b1 != $b2) {
                    $delta = abs($b1 - $b2);
                    $mid = ($b1 + $b2) / 2;
                    if ($delta > 0) {
                        $e = $delta / $mid;
                        $error += $e / $l;
                        if ($error > $epsilon) return false;
                    }
                }
            }
            return $error <= $epsilon;
        }
    }


}
