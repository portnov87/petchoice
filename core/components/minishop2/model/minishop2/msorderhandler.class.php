<?php

interface msOrderInterface
{

    /**
     * Initializes order to context
     * Here you can load custom javascript or styles
     *
     * @param string $ctx Context for initialization
     *
     * @return boolean
     */
    public function initialize($ctx = 'web');


    /**
     * Add one field to order
     *
     * @param string $key Name of the field
     * @param string $value .Value of the field
     *
     * @return boolean
     */
    public function add($key, $value);


    /**
     * Validates field before it set
     *
     * @param string $key The key of the field
     * @param string $value .Value of the field
     *
     * @return boolean|mixed
     */
    public function validate($key, $value);


    /**
     * Removes field from order
     *
     * @param string $key The key of the field
     *
     * @return boolean
     */
    public function remove($key);


    /**
     * Returns the whole order
     *
     * @return array $order
     */
    public function get();


    /**
     * Returns the one field of order
     *
     * @param array $order Whole order at one time
     *
     * @return array $order
     */
    public function set(array $order);


    /**
     * Submit the order. It will create record in database and redirect user to payment, if set.
     *
     * @return array $status Array with order status
     */
    public function submit();

    public function promocode();

    /**
     * Cleans the order
     *
     * @return boolean
     */
    public function clean();


    /**
     * Returns the cost of delivery depending on its settings and the goods in a cart
     *
     * @return array $response
     */
    public function getCost();
}


class msOrderHandler implements msOrderInterface
{
    /** @var modX $modx */
    public $modx;
    /** @var miniShop2 $ms2 */
    public $ms2;
    /** @var array $order */
    protected $order;
    public $user_discount_first = 5;

    /**
     * @param miniShop2 $ms2
     * @param array $config
     */
    function __construct(miniShop2 & $ms2, array $config = array())
    {
        $this->ms2 = &$ms2;
        $this->modx = &$ms2->modx;

        $this->config = array_merge(array(
            'order' => & $_SESSION['minishop2']['order']
        ), $config);

        $this->order = &$this->config['order'];
        $this->modx->lexicon->load('minishop2:order');

        if (empty($this->order) || !is_array($this->order)) {
            $this->order = array();
        }
    }


    /** @inheritdoc} */
    public function initialize($ctx = 'web')
    {
        return true;
    }


    /** @inheritdoc} */
    public function add($key, $value)
    {
        $response = $this->ms2->invokeEvent('msOnBeforeAddToOrder', array(
            'key' => $key,
            'value' => $value,
            'order' => $this
        ));
        if (!$response['success']) {
            return $this->error($response['message']);
        }
        $value = $response['data']['value'];

        $message = '';
        if (empty($value)) {
            $this->order[$key] = $validated = '';
        } else {
            $validated = $this->validate($key, $value);
            $isAuthenticated = $this->modx->user->isAuthenticated();
            if (!$isAuthenticated) {
                /* switch ($key) {
                     case 'phone':

                         $return = $this->validateexistphone($value);
                         if (!$return['result']) {
                             $message = $return['message'];
                             $validated = false;
                         }
                         break;
                 }*/
            }

            if ($validated !== false) {
                $this->order[$key] = $validated;

                $response = $this->ms2->invokeEvent('msOnAddToOrder', array(
                    'key' => $key,
                    'value' => $validated,
                    'order' => $this
                ));
                if (!$response['success']) {
                    return $this->error($response['message']);
                }
                $validated = $response['data']['value'];
            } else {
                $this->order[$key] = '';
            }
        }
        $data_s = [];
        $cart = $this->ms2->cart->status();
        if ($key == 'bonuses') {
            //set(array $order)
            $data_s['total'] = round($cart['total_cost_withdelivery'] - $value, 0);
            //$this->set([['total'=>$data_s['total']]]);


            /*if ($_SERVER['REMOTE_ADDR']=='188.130.177.18') {
                echo "<pre>".$data_s['total'];
                print_r($cart);
                echo "</pre>";
            }*/
        }
        $data_s[$key] = $validated;
//        $this->set([['total'=>]]);

        return ($validated === false)
            ? $this->error($message, array($key => $value))
            : $this->success($message, $data_s);
    }


    /** @inheritdoc} */
    public function validate($key, $value)
    {
        if ($key != 'comment') {
            $value = preg_replace('/\s+/', ' ', trim($value));
        }

        $response = $this->ms2->invokeEvent('msOnBeforeValidateOrderValue', array(
            'key' => $key,
            'value' => $value,
        ));
        $value = $response['data']['value'];

        $old_value = isset($this->order[$key]) ? $this->order[$key] : '';


        switch ($key) {
            case 'email':
                $value = preg_match('/^[^@а-яА-Я]+@[^@а-яА-Я]+(?<!\.)\.[^\.а-яА-Я]{2,}$/m', $value)
                    ? $value
                    : false;
                break;
            case 'receiver':
            case 'lastname':
            case 'fullname':
                // Transforms string from "nikolaj -  coster--Waldau jr." to "Nikolaj Coster-Waldau Jr."
                $tmp = preg_replace(array('/[^-a-zа-яЇІЄіїєёЁ\s\.]/iu', '/\s+/', '/\-+/', '/\.+/'), array('', ' ', '-', '.'), $value);
                $tmp = preg_split('/\s/', $tmp, -1, PREG_SPLIT_NO_EMPTY);
                $tmp = array_map(array($this, 'ucfirst'), $tmp);
                $value = preg_replace('/\s+/', ' ', implode(' ', $tmp));
                if (empty($value)) {
                    $value = false;
                }
                break;
            case 'phone':
                $value = substr(preg_replace('/[^-+0-9]/iu', '', $value), 0, 15);
                break;
            case 'delivery':
                /* @var msDelivery $delivery */

                if (!$delivery = $this->modx->getObject('msDelivery', array('id' => $value, 'active' => 1))) {
                    $value = $old_value;
                } else if (!empty($this->order['payment'])) {
                    if (!$this->hasPayment($value, $this->order['payment'])) {
                        $this->order['payment'] = $delivery->getFirstPayment();
                    };
                }

                break;
            case 'payment':
                if (!empty($this->order['delivery'])) {
                    $value = $this->hasPayment($this->order['delivery'], $value) ? $value : $old_value;
                }
                break;
            case 'index':
                $value = substr(preg_replace('/[^-0-9]/iu', '', $value), 0, 10);
                break;
        }

        $response = $this->ms2->invokeEvent('msOnValidateOrderValue', array(
            'key' => $key,
            'value' => $value,
        ));
        $value = $response['data']['value'];

        return $value;
    }


    /**
     * Checks accordance of payment and delivery
     *
     * @param $delivery
     * @param $payment
     *
     * @return bool
     */
    public function hasPayment($delivery, $payment)
    {
        $q = $this->modx->newQuery('msPayment', array('id' => $payment, 'active' => 1));
        $q->innerJoin('msDeliveryMember', 'Member', 'Member.payment_id = msPayment.id AND Member.delivery_id = ' . $delivery);

        return $this->modx->getCount('msPayment', $q) ? true : false;
    }


    /** @inheritdoc} */
    public function remove($key)
    {
        if ($exists = array_key_exists($key, $this->order)) {
            $response = $this->ms2->invokeEvent('msOnBeforeRemoveFromOrder', array(
                'key' => $key,
                'order' => $this
            ));
            if (!$response['success']) {
                return $this->error($response['message']);
            }

            unset($this->order[$key]);
            $response = $this->ms2->invokeEvent('msOnRemoveFromOrder', array(
                'key' => $key,
                'order' => $this
            ));
            if (!$response['success']) {
                return $this->error($response['message']);
            }
        }
        return $exists;
    }


    /** @inheritdoc} */
    public function get()
    {
        return $this->order;
    }


    /** @inheritdoc} */
    public function set(array $order)
    {
        $this->remove('subscribe');
        foreach ($order as $key => $value) {

            $this->add($key, $value);
        }

        return $this->order;
    }

    public function getQuickRequiresFields($id = 0)
    {
        if (empty($id)) {
            $id = $this->order['delivery'];
        }
        /* @var msDelivery $delivery */
        if (!$delivery = $this->modx->getObject('msDelivery', array('id' => $id, 'active' => 1))) {
            return $this->error('ms2_order_err_delivery', array('delivery'));
        }
        $requires = array();
        $requires[] = 'receiver';
        $requires[] = 'phone';

        return $this->success('', array('requires' => $requires));
    }


    /**
     * Returns required fields for delivery
     *
     * @param $id
     *
     * @return array|string
     */
    public function getDeliveryRequiresFields($id = 0)
    {
        if (empty($id)) {
            $id = $this->order['delivery'];
        }
        /* @var msDelivery $delivery */
        if (!$delivery = $this->modx->getObject('msDelivery', array('id' => $id, 'active' => 1))) {
            return $this->error('ms2_order_err_delivery', array('delivery'));
        }
        $requires = $delivery->get('requires');
        $requires = empty($requires) ? array() : array_map('trim', explode(',', $requires));
        if (!in_array('email', $requires)) {
            $requires[] = 'email';
        }


        $group_manager = false;
        if ($isAuthenticated = $this->modx->user->isAuthenticated()) {

            $group_manager = (in_array(7, $this->modx->user->getUserGroups())||in_array(10, $this->modx->user->getUserGroups()));//$this->modx->user->isMember('Manager');// проверяем группа менеджер или нет
            if ($group_manager) {

                if (!isset($_POST['created_user']))// создать учётную запись
                {
                    //echo 'eee'.$requires['email'];
                    foreach ($requires as $key => $em) {
                        if ($em == 'email') unset($requires[$key]);
                    }

                }
            }


        }

        return $this->success('', array('requires' => $requires));
    }


    private function validateexistphone($phone)
    {
        $q = $this->modx->newQuery('modUser');
        $q->where(array(
            'username' => $phone,
            'registr' => 1
        ));
        $q->select(array(
            'modUser.*'
        ));


        $q->prepare();
        $q->stmt->execute();

        $user = $q->stmt->fetchAll(PDO::FETCH_ASSOC);
        if (count($user) > 0) {
            return ['result' => false, 'message' => 'Данный номер уже использован для регистрации на сайте. Используйте его для авторизации или укажите другой номер телефона'];
        } else return ['result' => true, 'message' => ''];
    }


    public function validateexistregistr($email, $phone)
    {
        $q = $this->modx->newQuery('modUser');
        $q->where(array(
            'username' => $phone,
            'registr' => 1
        ));
        $q->select(array(
            'modUser.*'
        ));


        $q->prepare();

        $userregphone = $this->modx->getObject('modUser', $q);//array('username'=>$phone,'registr'=>0));
        //echo $userregphone->get('username');
        if ($userregphone) return $userregphone->get('id');//true;
        if ($email != '') {
            $q = $this->modx->newQuery('modUserProfile');

            $q->innerJoin('modUser', 'modUser', 'modUser.id = modUserProfile.internalKey');

            $q->where(array(
                'modUserProfile.email' => $email,
                'modUser.registr' => 1
            ));
            $q->select(array(
                'modUser.*'
            ));

            $q->prepare();
            $userregemail = $this->modx->getObject('modUserProfile', $q);//array('email'=>$phone,'registr'=>0));
            if ($userregemail) return $userregemail->get('id');//true;

        }
        return false;
    }

    public function validateexistnoregistr($email, $phone)
    {
        $q = $this->modx->newQuery('modUser');
        $q->where(array(
            'username' => $phone,
            'registr' => 0
        ));
        $q->select(array(
            'modUser.*'
        ));


        $q->prepare();

        $userregphone = $this->modx->getObject('modUser', $q);//array('username'=>$phone,'registr'=>0));
        if ($userregphone) return $userregphone->get('id');
        if ($email != '') {
            $q = $this->modx->newQuery('modUserProfile');

            $q->innerJoin('modUser', 'modUser', 'modUser.id = modUserProfile.internalKey');

            $q->where(array(
                'modUserProfile.email' => $email,
                'modUser.registr' => 0
            ));
            $q->select(array(
                'modUser.*'
            ));

            $q->prepare();
            $userregemail = $this->modx->getObject('modUserProfile', $q);//array('email'=>$phone,'registr'=>0));
            //if ($userregemail||$userregphone) return true;
            if ($userregemail) return $userregemail->get('id');
            //$userregemail->get('internalKey');
        }
        return false;
    }

    /** @inheritdoc} */

    public function submitquick($data = array())
    {
        /*  if ($_SERVER['REMOTE_ADDR']=='188.130.177.18'){
              echo "<pre>user_id";
              print_r($_POST);
              echo "</pre>";
             // die();
          }*/

        $response = $this->ms2->invokeEvent('msOnSubmitOrder', array(
            'data' => $data,
            'order' => $this
        ));


        if (!$response['success']) {
            return $this->error($response['message']);
        }
        if (!empty($response['data']['data'])) {
            $this->set($response['data']['data']);
        }
        $group_manager = false;
        if ($isAuthenticated = $this->modx->user->isAuthenticated()) {

            $group_manager = (in_array(7, $this->modx->user->getUserGroups())||in_array(10, $this->modx->user->getUserGroups()));//$this->modx->user->isMember('Manager');// проверяем группа менеджер или нет
        }

        $response = $this->getQuickRequiresFields();//getDeliveryRequiresFields();

        if ($this->ms2->config['json_response']) {
            $response = $this->modx->fromJSON($response);
        }

        $requires = $response['data']['requires'];

        /*echo '<pre>';
        print_r($requires);
        echo '</pre>';
        die();*/
        foreach ($requires as $v) {
            if (!empty($v) && empty($this->order[$v])) {
                $errors[] = $v;
            }
        }
        if (!empty($errors)) {
            return $this->error('ms2_order_err_requires', $errors);
        }


        $street = '';
        $house = '';
        $room = '';
        $city = '';
        $warehouse = '';
        $floor = '';
        $parade = '';
        $doorphone = '';
        $housing = '';
        $userregistr = false;
        //if (isset($data['created_user']))$userregistr=true;
        $errors = array();

        if ($isAuthenticated = $this->modx->user->isAuthenticated()) {

            $group_manager = (in_array(7, $this->modx->user->getUserGroups())||in_array(10, $this->modx->user->getUserGroups()));//$this->modx->user->isMember('Manager');// проверяем группа менеджер или нет


            $user_id = $this->modx->user->get('id');
            $profile = $this->modx->user->getOne('Profile');
            $profiledata = $profile->toArray();
            $extended = $profiledata['extended'];//$profile->get('extended');

            $payment = $extended['payment'];
            $delivery = $extended['delivery'];
            $city = $extended['city_id'];

        } else {
            $phone = $data['phone'];
            $name = $data['receiver'];
            $email = $data['email'];


            $user_id = $this->ms2->getCustomerId();
            /* if ($_SERVER['REMOTE_ADDR']=='188.130.177.18'){
             echo "<pre>user_id";
             print_r($user_id);
             echo "</pre>";
             die();
             }*/
            $user = $this->modx->getObject('modUser', array('id' => $user_id));
            if ($user) {
                $profile = $this->modx->getObject('modUserProfile', array('internalKey' => $user_id));
                /*    if ($profile) {
                        $profiledata = $profile->toArray();
                        $extended = $profiledata['extended'];
                        if (isset($extended['discount']))
                            $discount_loyalnost = $extended['discount'];
                    }
                  */
            }
            /*$user = $this->modx->newObject('modUser');


            $password = $user->generatePassword(6);//rand();
            $user->set('password', $password);//md5($password));
            $user->set('registr', 0);
            $user->set('registr_date', date('Y-m-d H:i:s'));
            $user->set('subscribe', 0);

            $user->set('username', $phone);
            $user->set('primary_group', 3);
            if (!$user->save()) {

                $user = $this->modx->getObject('modUser', array('username' => $phone));
                $user_id = $user->get('id');


                $profile = $user->getOne('Profile');
                $profiledata = $profile->toArray();
                $extended = $profiledata['extended'];//$profile->get('extended');

                $payment = $extended['payment'];
                $delivery = $extended['delivery'];
                $city = $extended['city_id'];
                $street = $extended['street'];
                $house = $extended['house'];
                $room = $extended['room'];
                $city = $extended['city'];
                $warehouse = $extended['warehouse'];
                $floor = $extended['floor'];
                $parade = $extended['parade'];
                $doorphone = $extended['doorphone'];
                $housing = $extended['housing'];

            } else {
                $user_id = $user->get('id');

                $profile = $this->modx->newObject('modUserProfile');
                $profile->set('internalKey', $user_id);
                $profile->set('phone', $phone);
                $profile->set('fullname', $data['lastname']);
                $extended = array();

                $extended['lastname'] = $name;
                $extended = json_encode($extended);

                $profile->set('extended', $extended);
                if (!$profile->save()) {
                    $error = 'Пользователь с таким email-ом уже зарегистрирован. Авторизуйтесь чтобы использовать свою скидку';
                    $errors[] = $error;
                    return $this->error($error, array('email' => $error));
                } else {
                    $userregistr = true;
                    $newExtended = [];//$newExtended1;
                    $user_id = $user->get('id');//$_SESSION['user_for_order']['id'];
                }

                $payment = 0;
                $delivery = 0;
                $city = 0;
            }*/


        }

        if (isset($data['promocode']) && ($data['promocode'] != '')) {
            if (isset($data['created_user'])) $cart_status = $this->ms2->cart->status(array('discount' => $this->user_discount_first));
            else $cart_status = $this->ms2->cart->status();
            $object_promocode = $this->modx->getObject('PromocodeItem', array(
                'code' => $data['promocode'],
                'status'=>'1',
                'term:<='=>date('Y-m-d H:i:s')
            ));

            //if (($term == '0000-00-00 00:00:00') || ($term == '1970-01-01 00:00:00') || ($term == NULL) || (date('Y-m-d H:i:s') <= $term)) {


            //}
            if ($object_promocode) {
                if (isset($data['created_user']))// создать учётную запись
                {
                    $cart_status = $this->ms2->cart->status(array('discount' => $this->user_discount_first, 'promocode' => $object_promocode->toArray()));
                } else {
                    $cart_status = $this->ms2->cart->status(array('promocode' => $object_promocode->toArray()));
                }

                $_SESSION['minishop2']['cart_total']['economy'] = $cart_status['economy'];
            } else {
                if (isset($data['created_user']))// создать учётную запись
                {

                    $cart_status = $this->ms2->cart->status(array('discount' => $this->user_discount_first));
                } else {
                    $cart_status = $this->ms2->cart->status();
                }

            }
        } else {
            if (isset($data['created_user']))// создать учётную запись
            {
                $cart_status = $this->ms2->cart->status(array('discount' => $this->user_discount_first));

            } else {
                if ($isAuthenticated = $this->modx->user->isAuthenticated()) {
                    if ($profile) {
                        $profiledata = $profile->toArray();
                        $extended = $profiledata['extended'];
                        if (($extended['discount'] != '') && ($extended['discount'])) {
                            $cart_status = $this->ms2->cart->status(array('discount' => $extended['discount']));

                        } else {
                            $cart_status = $this->ms2->cart->status();
                        }
                    } else $cart_status = $this->ms2->cart->status(array('discount' => $this->user_discount_first));
                } else $cart_status = $this->ms2->cart->status();
            }

        }
        $delivery_cost = $this->getCost(false, true, $delivery, $city);
        $cart_cost = $cart_status['total_cost'];
        $createdon = date('Y-m-d H:i:s');
        $order = $this->modx->newObject('msOrder');
        if (($_SERVER['HTTP_HOST']=='m.petchoice.pp.ua')||($_SERVER['HTTP_HOST'] == 'm.petchoice.ua')) $num = $this->getnum() . 'mq';
        else $num = $this->getnum() . 'q';


        // Adding address
        /* @var msOrderAddress $address */
        $address = $this->modx->newObject('msOrderAddress');

        $address->fromArray(array_merge($this->order, array(
            'user_id' => $user_id
        , 'createdon' => $createdon

        )));
        $order->addOne($address);

        // Adding products
        $cart = $this->ms2->cart->get();

        $products = array();
        $cart_cost_new = 0;
        foreach ($cart as $v) {

            if ($tmp = $this->modx->getObject('msProduct', $v['id'])) {
                $name = $tmp->get('pagetitle');
            } else {
                $name = '';
            }
            if (isset($v['newprice']))
                $v['price'] = $v['newprice'];
            /*echo '<pre>$v';
            print_r($v);
            echo '</pre>';*/
            if (($v['skidka1'] != 0) && (isset($v['skidka1'])) && ($v['skidka1'] != '')) {
                $v['price'] = $v['oldprice'] - ($v['oldprice'] * ($v['skidka1'] / 100));
            } elseif (isset($data['created_user']))// создать учётную запись
            {

                if ($v['action'] == '') {
                    $v['price'] = $v['price'] - ($v['price'] * ($this->user_discount_first / 100));
                    $v['price'] = round($v['price'], 2);
                }
            } elseif ($user) {
                if ($v['action'] == '') {
                    $user_id = $user->get('id');
                    $profile = $this->modx->getObject('modUserProfile', array('internalKey' => $user_id));
                    if ($profile) {
                        $profiledata = $profile->toArray();
                        $extended = $profiledata['extended'];
                        $dis=$extended['discount'];

                        $idproduct = $v['id'];

                        $where = array(
                            'contentid' => $idproduct
                        , 'tmplvarid' => 76
                        );
                        $tv = $this->modx->getObject('modTemplateVarResource', $where);
                        $nodostavka = false;
                        if ($tv) $nodostavka = $tv->get('value');

                        if ($nodostavka=='yes') {
                            if ($dis > 5) $dis = 5;
                        }
                        $v['price'] = $v['price'] - ($v['price'] * ($dis / 100));
                    }
                }
            }


            $id_1c='';
            $_id_1cOption='';

            $idproduct = $v['id'];
            $priceOption=0;
            $_id_1cOption = (isset($v['option_id'])?$v['option_id']:false);
            if (!$_id_1cOption) {
                $_where = array(
                    'contentid' => $idproduct
                    , 'tmplvarid' => 1
                );
                $tvOptions = $this->modx->getObject('modTemplateVarResource', $_where);

                if ($tvOptions) {
                    $_options = $tvOptions->get('value');
                    if ($_options) {

                        $new_options = [];
                        $_id_1cGlobal = false;
                        $options = json_decode($_options, true);

                        foreach ($options as $_keyOpt => $opt) {
                            $_id_1c = false;
                            $optNew = $opt;

                            if (isset($opt['id_1c'])) {
                                if ($opt['id_1c'] == '')
                                    $_id_1c = false;
                                else
                                    $_id_1c = $opt['id_1c'];
                            }

                            if (!$_id_1c) {
                                $id1c = $idproduct . $opt['MIGX_id'] . time();
                                $optNew['id_1c'] = $id1c;
                                $_id_1cGlobal = true;
                            }

                            if (isset($v['options'])) {
                                if (isset($v['options']['size'])) {
                                    if ($v['options']['size'] == $opt['weight'] . ' ' . $opt['weight_prefix'])
                                        $_id_1cOption = $_id_1c;
                                    $priceOption=$opt['price'];
                                }

                            }
                            $new_options[$_keyOpt] = $optNew;


                        }

                        if ($_id_1cGlobal) {
                            $options = $new_options;
                            $new_optionsForSave = json_encode($new_options);

                            $tv = $this->modx->getObject('modTemplateVarResource', array('contentid' => $idproduct, 'tmplvarid' => 1));
                            if ($tv && ($new_optionsForSave != '')) {
                                $tv->set('value', $new_optionsForSave);
                                $tv->save();
                            }
                        }
                    }
                }
            }




            /* @var msOrderProduct $product */
            $product = $this->modx->newObject('msOrderProduct');
            $cost = $v['price'] * $v['count'];
            $v_new=array_merge($v, array(
                'product_id' => $v['id']
            , 'price_old' => round($v['oldprice'], 2)
            , 'discount' => round($v['skidka1'], 2)
            , 'name' => $name
            , 'cost' => $cost
            , 'id_1c'=> $_id_1cOption
            , 'cost_old' => round($v['oldprice'] * $v['count'], 2)
            ));
            if (!isset($v_new['price']))
            {
                $v_new['price']=$priceOption;
            }
            elseif($v_new['price']<=0){
                $v_new['price']=$priceOption;
            }
            $product->fromArray($v_new);




/*

            $optionsArray=json_decode($options,true);

            $old_pr=$v['price'];


            $product = $this->modx->newObject('msOrderProduct');
            $cost = $v['price'] * $v['count'];


            $product->fromArray(array_merge($v, array(
                'product_id' => $v['id']
            , 'price_old' => round($v['oldprice'], 2)
            , 'discount' => round($v['skidka1'], 2)
            , 'name' => $name
            , 'cost' => $cost
            , 'id_1c'=> $_id_1cOption
            , 'cost_old' => round($v['oldprice'] * $v['count'], 2)
            )));

            */



            $products[] = $product;
            $cart_cost_new += $cost;
        }

        //echo $cart_cost_new.' '.$cart_cost.'<br/>';

        $cart_cost = $cart_cost_new;

        $payment_status=1;
        $order->fromArray(array(
            'user_id' => $user_id
        , 'createdon' => $createdon
        , 'updatedon' => $createdon
        , 'num' => $num//$this->getnum()
        , 'delivery' => $delivery //$this->order['delivery']
        , 'payment' => $payment //$this->order['payments']
        , 'cart_cost' => $cart_cost
        , 'weight' => $cart_status['total_weight']
        , 'delivery_cost' => $delivery_cost
        , 'cost' => $cart_cost + $delivery_cost
        , 'status' => 0
        , 'payment_status' => $payment_status
        , 'date_shipping' => NULL
        , 'time_shipping_to' => NULL
        , 'time_shipping_from' => NULL
        , 'context' => $this->ms2->config['ctx']
        ));
        //}


        //die();
        $order->addMany($products);

        $response = $this->ms2->invokeEvent('msOnBeforeCreateOrder', array(
            'msOrder' => $order,
            'order' => $this


        ));
        if (!$response['success']) {
            return $this->error($response['message']);
        }
// отправим смс
        //$this->ms2->sendsms($data['phone'], 'Спасибо за заказ №' . $this->getnum() . '. Менеджер свяжется с Вами в ближайшее время');
        //die();
        if ($order->save()) {
            unset($_SESSION['user_for_order']);
            $response = $this->ms2->invokeEvent('msOnCreateOrder', array(
                'msOrder' => $order,
                'order' => $this
            ));
            if (!$response['success']) {
                return $this->error($response['message']);
            }

            if (isset($data['phone']) && ($data['phone'] != '')){

                $user_id = $order->get('user_id');
                $user = $this->modx->getObject('modUser', array('id' => $user_id));
                if ($user) {
                    $profile=$user->getOne('Profile');

                    $this->ms2->eventNotification(1, $user, [
                        'order_num'=>$order->get('num'),
                        'fullname'=>$profile->get('fullname'),
                        'phone'=>$data['phone']
                    ]);
                }




            }



            $this->ms2->cart->clean();
            $this->clean();
            if (empty($_SESSION['minishop2']['orders'])) {
                $_SESSION['minishop2']['orders'] = array();
            }
            $_SESSION['minishop2']['orders'][] = $order->get('id');
            if ($object_promocode) {
                if ($object_promocode->get('type') == 'dinamic') $status = 0; else $status = 1;
                $object_promocode->set('status', $status);
                $object_promocode->set('order_id', $order->get('id'));
                $object_promocode->set('order_num', $order->get('num'));

                $object_promocode->save();
            }


            // Trying to set status "new"
            $response = $this->ms2->changeOrderStatus($order->get('id'), 1);
            if ($response !== true) {
                return $this->error($response, array('msorder' => $order->get('id')));
            } /* @var msPayment $payment */
            elseif ($payment = $this->modx->getObject('msPayment', array('id' => $order->get('payment'), 'active' => 1))) {
                $response = $payment->send($order);
                if ($this->config['json_response']) {
                    @session_write_close();
                    exit(is_array($response) ? $this->modx->toJSON($response) : $response);
                } else {
                    if (!empty($response['data']['redirect'])) {
                        $this->modx->sendRedirect($response['data']['redirect']);
                        exit();
                    } elseif (!empty($response['data']['msorder'])) {
                        $this->modx->sendRedirect($this->modx->context->makeUrl($this->modx->resource->id, array('msorder' => $response['data']['msorder'])));
                        exit();
                    } else {
                        $this->modx->sendRedirect($this->modx->context->makeUrl($this->modx->resource->id));
                        exit();
                    }
                }
            } else {
                if ($this->ms2->config['json_response']) {
                    return $this->success('', array('msorder' => $order->get('id')));
                } else {
                    $this->modx->sendRedirect($this->modx->context->makeUrl($this->modx->resource->id, array('msorder' => $response['data']['msorder'])));
                    exit();
                }
            }


        }
        return $this->error();
    }

    /**
     * установление бонусов заказу
     * @param $user
     * @param $order
     */
    public function recordBonuses($bonuses, $user, $order, $total)
    {
        if (($bonuses == '') || ($bonuses == 0)) return false;
        if ($user) {
            if ($user->get('registr')==1) {
                $bonuses_for_order = $bonuses;
                $total_max_forbonuses = $user->get('bonuses');
                if (($total > 0) && ($bonuses > 0) && ($total_max_forbonuses > 0)) {
                    $bonuses_for_order = $bonuses;
                    $limit_bonuses = $this->modx->getOption('limit_bonuses') / 100;
                    $total_max_forbonuses = round($total * $limit_bonuses, 0); // максимум оплатить можно до 30% заказа

                    //if ($bonuses>$total_max_forbonuses)
                    //$bonuses_for_order=$total_max_forbonuses;
                    $order->set('with_bonuses', 1);

                }

                if ($bonuses_for_order > $total_max_forbonuses)
                    $order->set('bonuses', $total_max_forbonuses);
                else $order->set('bonuses', $bonuses_for_order);
            }

        }
        return $order;
    }

    /**
     * @param array $data
     * @return array|string
     */
    public function submit($data = array())
    {

        $apply_bonuses = false;
        if (isset($this->order['bonuses'])) {
            if ($this->order['bonuses'] > 0) {
                $apply_bonuses = $this->order['bonuses'];
            }
        }
        $discount_loyalnost = false;//'discount' => $extended['discount']
        $response = $this->ms2->invokeEvent('msOnSubmitOrder', array(
            'data' => $data,
            'order' => $this
        ));
        $user = false;
        $total = '';
        $link = '';

        if (!$response['success']) {
            return $this->error($response['message']);
        }
        if (!empty($response['data']['data'])) {
            $this->set($response['data']['data']);
        }
        $group_manager = false;
        if ($isAuthenticated = $this->modx->user->isAuthenticated()) {
            $id_user_created = $this->modx->user->id;
            $group_manager = (in_array(7, $this->modx->user->getUserGroups())||in_array(10, $this->modx->user->getUserGroups()));// проверяем группа менеджер или нет
        }

        $response = $this->getDeliveryRequiresFields();

        if ($this->ms2->config['json_response']) {
            $response = $this->modx->fromJSON($response);
        }

        $requires = $response['data']['requires'];


        foreach ($requires as $v) {
            if (!empty($v) && empty($this->order[$v])) {
                $errors[] = $v;
            }
        }
        if (!empty($errors)) {
            return $this->error('ms2_order_err_requires', $errors);
        }




        $userregistr = false;
        $errors = array();
        $city = $data['extended']['city_id'];
        $city_db = $city_np = false;
        $warehouses=false;
        if (($data['delivery'] == 6) ||($data['delivery'] == 2) || ($data['delivery'] == 3)) {
            $city_id=$city;
            $delivery = $this->modx->getObject('msDelivery', $data['delivery']);
            $warehouses = $delivery->getWarehouses($city_id);

        }


        if ($isAuthenticated = $this->modx->user->isAuthenticated()) {

            $group_manager = (in_array(7, $this->modx->user->getUserGroups())||in_array(10, $this->modx->user->getUserGroups()));// проверяем группа менеджер или нет

            if ($group_manager) {


                if (isset($data['created_user']))// создать учётную запись
                {
                    $phone = $data['phone'];
                    $email = $data['email'];
                    if ($email != '')
                        $userreg = $this->validateexistregistr($email, $phone);
                    else $userreg = false;
                    if ($userreg) {
                        $error = 'Пользователь с таким email-ом или телефон уже зарегистрирован. Авторизуйтесь чтобы использовать свою скидку';
                        $errors[] = $error;
                        return $this->error($error, array('email' => $error));
                    } else {
                        if ($email != '') $usernoreg = $this->validateexistnoregistr($email, $phone);
                        else $usernoreg = false;

                        if ($usernoreg) $user = $this->modx->getObject('modUser', $usernoreg);
                        else
                            $user = $this->modx->newObject('modUser');
                        $user->set('username', $phone);
                        $user->set('primary_group', 3);
                        $user->set('registr', 1);
                        $user->set('registr_date', date('Y-m-d H:i:s'));
                        if (isset($data['subscribe'])) {
                            $user->set('subscribe', 1);
                            $user->set('subscribe_date', date('Y-m-d H:i:s'));
                        } else $user->set('subscribe', 0);

                        $password = $user->generatePassword(6);
                        $user->set('password', $password);

                        if (!$user->save()) {
                            $error = 'Пользователь с таким телефоном уже зарегистрирован. Авторизуйтесь чтобы использовать свою скидку';
                            $errors[] = $error;
                            return $this->error($error, array('email' => $error));
                        }
                        $user_id = $user->get('id');
                        if ($usernoreg) $profile = $this->modx->getObject('modUserProfile', array('internalKey' => $user_id));
                        else $profile = $this->modx->newObject('modUserProfile');
                        $profile->set('internalKey', $user_id);
                        $profile->set('fullname', $data['receiver']);//.' '.$data['lastname']);
                        $profile->set('lastname', $data['extended']['lastname']);
                        $profile->set('email', $data['email']);
                        $profile->set('phone', $data['phone']);
                        $extended = array();
                        $extended['city_id'] = $data['extended']['city_id'];


                        if (isset($data['extended']['fathername'])) {
                            $extended['fathername'] = $data['extended']['fathername'];

                        }

                        if ($warehouses) {
                            $warehouse_np=false;

                            foreach ($warehouses as $warehouse) {
                                if ($warehouse['Ref'] == $data['extended']['warehouse']) {
                                    $warehouse_id=$warehouse['Ref'];//$data['extended']['warehouse'];
                                    $warehouse_np = $warehouse['DescriptionRu'];//DescriptionRu'];
                                    break;
                                }
                            }
                            if ($warehouse_np) {
                                $extended['warehouse_id'] = $warehouse_id;
                                $extended['warehouse_name'] = $warehouse_np;
                                $extended['warehouse'] = $warehouse_np;

                            }

                        }

                        if (!isset( $extended['warehouse'])){
                            $extended['warehouse'] = $data['extended']['warehouse'];
                        }


                        $extended['discount'] = $this->user_discount_first;
                        $extended['payment'] = $data['payment'];
                        $extended['street'] = $data['extended']['street'];

                        if (isset($data['extended']['street_np']))
                            $extended['street_np'] = $data['extended']['street_np'];

                        if (isset($data['extended']['street_np_ref']))
                            $extended['street_np_ref'] = $data['extended']['street_np_ref'];


                        $extended['house'] = $data['extended']['house'];
                        $extended['room'] = $data['extended']['room'];
                        $extended['delivery'] = $data['delivery'];
                        $extended['lastname'] = $data['lastname'];


                        if (isset($data['fathername']))
                            $extended['fathername'] = $data['fathername'];

                        $extended['doorphone'] = $data['extended']['doorphone'];
                        $extended['housing'] = $data['extended']['housing'];
                        $extended['parade'] = $data['extended']['parade'];
                        $extended['floor'] = $data['extended']['floor'];

                        $newExtended1 = $extended;

                        $extended = json_encode($extended);

                        $profile->set('city', $data['city']);
                        $profile->set('extended', $extended);
                        if (!$profile->save()) {
                            $error = 'Пользователь с таким email-ом уже зарегистрирован. Авторизуйтесь чтобы использовать свою скидку';
                            $errors[] = $error;
                            return $this->error($error, array('email' => $error));
                        } else {
                            $userregistr = true;
                            $newExtended = $newExtended1;
                            $user_id = $user->get('id');//$_SESSION['user_for_order']['id'];
                        }
                    }
                } else {
                    $phone = $data['phone'];
                    $email = $data['email'];

                    $userreg = $this->validateexistregistr($email, $phone);
                    if ($userreg) {

                        if ($userreg) $user = $this->modx->getObject('modUser', $userreg);
                        else {
                            $user = $this->modx->newObject('modUser');
                            $user->set('registr', 0);
                            $user->set('primary_group', 3);
                            $user->set('created', date('Y-m-d H:i:s'));
                            if (isset($data['subscribe'])) {
                                $user->set('subscribe', 1);
                                $user->set('subscribe_date', date('Y-m-d H:i:s'));
                            } else $user->set('subscribe', 0);
                            $user->set('username', $phone);

                            $password = $user->generatePassword(6);
                            $user->set('password', $password);
                        }
                    } else {
                        $usernoreg = $this->validateexistnoregistr($email, $phone);
                        if ($usernoreg) $user = $this->modx->getObject('modUser', $usernoreg);
                        else {
                            $user = $this->modx->newObject('modUser');
                            $user->set('registr', 0);
                            $user->set('primary_group', 3);
                            if (isset($data['subscribe'])) {
                                $user->set('subscribe', 1);
                                $user->set('subscribe_date', date('Y-m-d H:i:s'));
                            } else $user->set('subscribe', 0);
                            $user->set('username', $phone);
                            $user->set('created', date('Y-m-d H:i:s'));
                            $password = $user->generatePassword(6);
                            $user->set('password', $password);


                        }
                    }

                    if (!$user->save()) {
                        $error = 'Ошибка сохранения пользователя';//Пользователь с таким телефоном уже зарегистрирован. Авторизуйтесь чтобы использовать свою скидку';
                        $errors[] = $error;
                        return $this->error($error, array('email' => $error));
                    }
                    $user_id = $user->get('id');

                    $profile = $this->modx->getObject('modUserProfile', array('internalKey' => $user_id));
                    if (!$profile) {
                        $profile = $this->modx->newObject('modUserProfile');
                        $profile->set('internalKey', $user_id);
                        $profile->set('fullname', $data['receiver']);//.' '.$data['lastname']);
                        $profile->set('lastname', $data['lastname']);
                        $profile->set('email', $data['email']);
                        $profile->set('phone', $data['phone']);
                        $extended = array();
                        $extended['city_id'] = $data['extended']['city_id'];
                        //$extended['warehouse'] = $data['extended']['warehouse'];
                        if ($warehouses) {
                            $warehouse_np=$warehouse_id=false;

                            foreach ($warehouses as $warehouse) {
                                if ($warehouse['Ref'] == $data['extended']['warehouse']) {
                                    $warehouse_id=$warehouse['Ref'];//$data['extended']['warehouse'];
                                    $warehouse_np = $warehouse['DescriptionRu'];//DescriptionRu'];
                                    break;
                                }
                            }
                            if ($warehouse_np) {
                                $extended['warehouse_id'] = $warehouse_id;
                                $extended['warehouse_name'] = $warehouse_np;
                                $extended['warehouse'] = $warehouse_np;
                            }

                        }

                        if (!isset( $extended['warehouse'])){
                            $extended['warehouse'] = $data['extended']['warehouse'];
                        }


                        $extended['payment'] = $data['payment'];
                        $extended['street'] = $data['extended']['street'];
                        if (isset($data['extended']['street_np']))
                            $extended['street_np'] = $data['extended']['street_np'];

                        if (isset($data['extended']['street_np_ref']))
                            $extended['street_np_ref'] = $data['extended']['street_np_ref'];


                        $extended['house'] = $data['extended']['house'];
                        $extended['room'] = $data['extended']['room'];
                        $extended['delivery'] = $data['delivery'];
                        $extended['lastname'] = $data['lastname'];


                        if (isset($data['fathername']))
                            $extended['fathername'] = $data['fathername'];

                        $extended['doorphone'] = $data['doorphone'];
                        $extended['housing'] = $data['housing'];
                        $extended['parade'] = $data['parade'];
                        $extended['floor'] = $data['floor'];
                    } else {

                        $profiledata = $profile->toArray();



                        $extended = $profiledata['extended'];//$profile->get('extended');

                        if (isset($data['extended']['lastname']))
                            $profile->set('lastname', $data['extended']['lastname']);

                        $extended['city_id'] = $data['extended']['city_id'];
                        //$extended['warehouse'] = $data['extended']['warehouse'];
                        if ($warehouses) {
                            $warehouse_np=$warehouse_id=false;

                            foreach ($warehouses as $warehouse) {
                                if ($warehouse['Ref'] == $data['extended']['warehouse']) {
                                    $warehouse_id=$warehouse['Ref'];
                                    $warehouse_np = $warehouse['DescriptionRu'];//DescriptionRu'];
                                    break;
                                }
                            }
                            if ($warehouse_np) {
                                //$arrayor['warehouse_id'] = $warehouse_id;
                                $extended['warehouse'] = $warehouse_np;
                                $extended['warehouse_name'] = $warehouse_np;
                                $extended['warehouse_id'] = $warehouse_id;//$data['extended']['warehouse'];
                            }

                        }

                        if (!isset( $extended['warehouse'])){
                            $extended['warehouse'] = $data['extended']['warehouse'];
                        }

                        $extended['delivery'] = $data['delivery'];
                        $extended['payment'] = $data['payment'];
                        $extended['street'] = $data['extended']['street'];
                        if (isset($data['extended']['street_np']))
                            $extended['street_np'] = $data['extended']['street_np'];

                        if (isset($data['extended']['street_np_ref']))
                            $extended['street_np_ref'] = $data['extended']['street_np_ref'];

                        $extended['house'] = $data['extended']['house'];
                        $extended['room'] = $data['extended']['room'];


                        if (isset($data['fathername']))
                            $extended['fathername'] = $data['fathername'];

                        if (isset($data['lastname']))
                        {
                            $extended['lastname'] = $data['lastname'];
                        }
                        $extended['doorphone'] = $data['doorphone'];
                        $extended['housing'] = $data['housing'];
                        $extended['parade'] = $data['parade'];
                        $extended['floor'] = $data['floor'];
                    }

                    $newExtended1 = $extended;

                    $extended = json_encode($extended);

                    $profile->set('city', $data['city']);
                    $profile->set('extended', $extended);
                    if (!$profile->save()) {
                        $error = 'Пользователь с таким email-ом уже зарегистрирован. Авторизуйтесь чтобы использовать свою скидку';
                        $errors[] = $error;
                        return $this->error($error, array('email' => $error));
                    } else {
                        $userregistr = false;
                        $newExtended = $newExtended1;
                        $user_id = $user->get('id');//$_SESSION['user_for_order']['id'];
                    }
                }
            } else {
                $profile = $this->modx->user->getOne('Profile');
                $profiledata = $profile->toArray();
                $extended = $profiledata['extended'];//$profile->get('extended');
                $newExtended = $extended;

                if (isset($data['fathername']))
                    $newExtended['fathername'] = $data['fathername'];

//                if (isset($data['extended']['fathername']))
//                    $newExtended['fathername'] = $data['extended']['fathername'];

                if (isset($data['receiver']))
                    $profile->set('fullname', $data['receiver']);

                if (isset($data['lastname']))
                    $profile->set('lastname', $data['lastname']);

                if (isset($data['email']))
                    $profile->set('email', $data['email']);


                if (isset($data['lastname']))
                    $newExtended['lastname'] = $data['lastname'];


                if (isset($data['extended']['warehouse']) && ($data['extended']['warehouse'] != '')) $newExtended['warehouse'] = $data['extended']['warehouse'];


                if (isset($data['fathername']) && ($data['fathername'] != '')) $newExtended['fathername'] = $data['fathername'];

            }


            $newExtended['city_id'] = $data['extended']['city_id'];
            $newExtended['delivery'] = $data['delivery'];
            $newExtended['payment'] = $data['payments'];
            if (isset($data['extended']['street']) && ($data['extended']['street'] != '')) $newExtended['street'] = $data['extended']['street'];

            if (isset($data['extended']['street_np']) && ($data['extended']['street_np'] != ''))
                $newExtended['street_np'] = $data['extended']['street_np'];

            if (isset($data['extended']['street_np_ref']) && ($data['extended']['street_np_ref'] != ''))
                $newExtended['street_np_ref'] = $data['extended']['street_np_ref'];



            if (isset($data['lastname']))
                $newExtended['lastname'] = $data['lastname'];


            if (isset($data['fathername']))
                $newExtended['fathername'] = $data['fathername'];

            if (isset($data['extended']['house']) && ($data['extended']['house'] != '')) $newExtended['house'] = $data['extended']['house'];
            if (isset($data['extended']['room']) && ($data['extended']['room'] != '')) $newExtended['room'] = $data['extended']['room'];

            // if (isset($data['extended']['textwarehouse']) && ($data['extended']['textwarehouse'] != '')) $newExtended['warehouse'] = $data['extended']['textwarehouse'];

            if (isset($data['extended']['doorphone']) && ($data['extended']['doorphone'] != '')) $newExtended['doorphone'] = $data['extended']['doorphone'];
            if (isset($data['extended']['housing']) && ($data['extended']['housing'] != '')) $newExtended['housing'] = $data['extended']['housing'];
            if (isset($data['extended']['parade']) && ($data['extended']['parade'] != '')) $newExtended['parade'] = $data['extended']['parade'];
            if (isset($data['extended']['floor']) && ($data['extended']['floor'] != '')) $newExtended['floor'] = $data['extended']['floor'];

            $newExtended['city_id_np'] = $data['extended']['city_id'];
            if (isset($data['extended']['warehouse'])) {
                $warehouseObject=$this->modx->getObject('msNpWarehouse',$data['extended']['warehouse']);
                if ($warehouseObject) {
                    $newExtended['warehouse_name'] = $warehouseObject->get('Description');
                }
            }




            if (isset($data['extended']['street_np'])){
                if ($data['extended']['street_np']!='') {
                    $newExtended['street'] = $data['extended']['street_np'];
                }
            }

            if (isset($data['extended']['street_np_ref'])){
                if ($data['extended']['street_np_ref']!='')
                    $newExtended['street_id_np'] = $data['extended']['street_np_ref'];
            }

            if (isset($data['city'])&&(empty($newExtended['city_id_np']))) {
                if ($data['city']!='') {
                    $city_exist = $this->modx->getObject('msNpCities', ['DescriptionRu' => $data['city']]);//array('id' => $ref));

                    if ($city_exist) {
                        $newExtended['city_id_np'] = $city_exist->get('id');
                    }
                }
            }
//


            $extended = $newExtended;//is_array($extended) ? array_merge($extended,$newExtended) : $newExtended;
            if (count($errors) == 0) {
                $profile->set('city', $data['city']);
                $profile->set('extended', $extended);
                $profile->save();
            }
        } else {
            $phone = $data['phone'];
            $email = $data['email'];


        }


        if (!$group_manager) {

            if (isset($this->modx->user->id)) {

                if ($this->modx->user->isAuthenticated())
                    $user_id = $this->modx->user->id;//$this->ms2->getCustomerId();
                else {
                    $user_id = $this->ms2->getCustomerId();
                    $user = $this->modx->getObject('modUser', array('id' => $user_id));
                    if ($user) {
                        $profile = $this->modx->getObject('modUserProfile', array('internalKey' => $user_id));
                        if ($profile) {
                            $profiledata = $profile->toArray();
                            $extended = $profiledata['extended'];
                            if (isset($extended['discount']))
                                $discount_loyalnost = $extended['discount'];
                        }
                    }
                }

            } else {
                $user_id = $this->ms2->getCustomerId();
                $user = $this->modx->getObject('modUser', array('id' => $user_id));
                if ($user) {
                    $profile = $this->modx->getObject('modUserProfile', array('internalKey' => $user_id));
                    if ($profile) {
                        $profiledata = $profile->toArray();
                        $extended = $profiledata['extended'];
                        if (isset($extended['discount']))
                            $discount_loyalnost = $extended['discount'];


                        if (isset($data['extended']['fathername'])) {
                            $extended['fathername'] = $data['extended']['fathername'];
                            $extended = json_encode($extended);
                            $profile->set('extended', $extended);
                        }

                        if (isset($data['fathername'])) {
                            $extended['fathername'] = $data['fathername'];
                            $extended = json_encode($extended);
                            $profile->set('extended', $extended);
                        }

                        $profile->set('fullname', $data['receiver']);//.' '.$data['lastname']);
                        $profile->set('lastname', $data['lastname']);
                        $profile->set('email', $data['email']);
                        $profile->save();


                    }



                    /*
                    if ($profile = $user->getOne('profile')) {
                        $profiledata = $profile->toArray();
                        $extended = $profiledata['extended'];

                        if (isset($extended['discount']))
                            $discount_loyalnost = $extended['discount'];
                    }*/
                }
            }
            $id_user_created = $user_id;
        }
        /*
                if ($_SERVER['REMOTE_ADDR']=='188.130.177.18')
                {
                    echo "<pre>PPPRP";
                    print_r($profiledata);
                    echo "</pre>";
                    echo "<pre>discount";
                    print_r($discount_loyalnost);
                    echo "</pre>";
                    die();
                }*/


        if (isset($data['promocode']) && ($data['promocode'] != '')) {
            if (!$this->promocode_cart($data['promocode'])) $data['promocode'] = '';
        }

        if (isset($data['promocode']) && ($data['promocode'] != '')) {

            if (isset($data['created_user']))
                $cart_status = $this->ms2->cart->status(array('discount' => $this->user_discount_first));
            else {
                if ($discount_loyalnost)
                    $cart_status = $this->ms2->cart->status(['discount_loyalnost' => $discount_loyalnost]);
                else $cart_status = $this->ms2->cart->status();
            }
            $object_promocode = $this->modx->getObject('PromocodeItem', array(
                'code' => $data['promocode'],
                'status'=>'1',
                'term:>='=>date('Y-m-d H:i:s')
            ));
            /*if ($_SERVER['HTTP_CF_CONNECTING_IP']=='78.46.62.217'){
                echo "<pre>data_oredr".date('Y-m-d H:i:s').' '.$data['promocode'];
                print_r($data);
                echo "</pre>";
                echo "<pre>object_promocode";
                print_r($object_promocode);
                echo "</pre>";

                die();
            }*/
            if ($object_promocode) {
                if (isset($data['created_user']))// создать учётную запись
                {
                    $cart_status = $this->ms2->cart->status(array('discount' => $this->user_discount_first, 'promocode' => $object_promocode->toArray()));
                } else {
                    if ($discount_loyalnost)
                        $cart_status = $this->ms2->cart->status(['promocode' => $object_promocode->toArray(), 'discount_loyalnost' => $discount_loyalnost]);
                    else $cart_status = $this->ms2->cart->status(array('promocode' => $object_promocode->toArray()));
                }

                $_SESSION['minishop2']['cart_total']['economy'] = $cart_status['economy'];
            } else {
                if (isset($data['created_user']))// создать учётную запись
                {
                    $cart_status = $this->ms2->cart->status(array('discount' => $this->user_discount_first));
                } else {
                    if ($discount_loyalnost)
                        $cart_status = $this->ms2->cart->status(['discount_loyalnost' => $discount_loyalnost]);
                    else $cart_status = $this->ms2->cart->status();

                }

            }
        } else {
            if (isset($data['created_user']))// создать учётную запись
            {
                $cart_status = $this->ms2->cart->status(array('discount' => $this->user_discount_first));

            } else {
                if ($discount_loyalnost)
                    $cart_status = $this->ms2->cart->status(['noguid'=>1,'discount_loyalnost' => $discount_loyalnost]);
                else $cart_status = $this->ms2->cart->status(['noguid'=>1]);

                /*  if ($_SERVER['REMOTE_ADDR']=='188.130.177.18'){
                      echo "<pre>status";
                      print_r($cart_status);
                      echo "</pre>";die();
                  }*/
            }

        }
        $delivery_cost = $this->getCost(false, true, $data['delivery'], $city);

        $cart_cost = $cart_status['total_cost'];//$this->getCost(true, true);// + $delivery_cost;
        $createdon = date('Y-m-d H:i:s');
        /* @var msOrder $order */
        $order = $this->modx->newObject('msOrder');

        if (($_SERVER['HTTP_HOST']=='m.petchoice.pp.ua')||($_SERVER['HTTP_HOST'] == 'm.petchoice.ua')) $num = $this->getnum() . 'm';
        else $num = $this->getnum();


        $samovivoz='';
        if ($data['delivery'] == 1) {
            $samovivoz=$data['samovivoz'];
        }

       /* if ($_SERVER['HTTP_CF_CONNECTING_IP']=='78.46.62.217') {
            echo '<pre>economypercent';
            print_r($data);//$data['promocode']);
            echo '</pre>';
            die();
        }*/
        //$this->order['payments']
        $payment_status=1;
        if (isset($data['promocode'])) {




            if ($object_promocode) {
                $order->fromArray(array(
                    'user_id' => $user_id
                , 'createdon' => $createdon
                , 'updatedon' => $createdon
                , 'num' => $num///$this->getnum()
                , 'delivery' => $this->order['delivery']
                , 'payment' => $this->order['payments']
                , 'cart_cost' => $cart_cost
                , 'cost_without_bonuses' => $cart_cost
                , 'weight' => $cart_status['total_weight']
                , 'delivery_cost' => $delivery_cost
                , 'cost' => $cart_cost + $delivery_cost
                , 'status' => 0
                , 'payment_status' => $payment_status
                , 'promocode' => $object_promocode->get('code')
                , 'promocode_id' => $object_promocode->get('id')
                , 'id_user_created' => $id_user_created
                , 'context' => $this->ms2->config['ctx']
                , 'date_shipping' => NULL
                    ,'pickup_id'=>$samovivoz
                , 'time_shipping_to' => NULL
                , 'time_shipping_from' => NULL
                , 'city_id_np'=>(isset($data['extended']['city_id'])?$data['extended']['city_id']:'')
                ));
            } else {
                $order->fromArray(array(
                    'user_id' => $user_id
                , 'createdon' => $createdon
                , 'updatedon' => $createdon
                , 'num' => $num//$this->getnum()
                , 'delivery' => $this->order['delivery']
                , 'payment' => $this->order['payments']
                , 'cart_cost' => $cart_cost
                , 'cost_without_bonuses' => $cart_cost
                , 'weight' => $cart_status['total_weight']
                , 'delivery_cost' => $delivery_cost
                , 'cost' => $cart_cost + $delivery_cost
                , 'status' => 0
                , 'payment_status' => $payment_status
                , 'id_user_created' => $id_user_created
                , 'context' => $this->ms2->config['ctx']
                , 'date_shipping' => NULL
                ,'pickup_id'=>$samovivoz
                , 'time_shipping_to' => NULL
                , 'time_shipping_from' => NULL
                , 'city_id_np'=>(isset($data['extended']['city_id'])?$data['extended']['city_id']:'')
                ));
            }

        } else {
            $order->fromArray(array(
                'user_id' => $user_id
            , 'createdon' => $createdon
            , 'updatedon' => $createdon
            , 'num' => $num//$this->getnum()
            , 'delivery' => $this->order['delivery']
            , 'payment' => $this->order['payments']
            , 'cart_cost' => $cart_cost
            , 'weight' => $cart_status['total_weight']
            , 'delivery_cost' => $delivery_cost
            , 'cost' => $cart_cost + $delivery_cost
            , 'cost_without_bonuses' => $cart_cost
            , 'status' => 0
            , 'payment_status' => $payment_status
            ,'pickup_id'=>$samovivoz
            , 'id_user_created' => $id_user_created
            , 'context' => $this->ms2->config['ctx']
            , 'date_shipping' => NULL
            , 'time_shipping_to' => NULL
            , 'time_shipping_from' => NULL
            , 'city_id_np'=>(isset($data['extended']['city_id'])?$data['extended']['city_id']:'')
            ));
        }


        // Adding address
        /* @var msOrderAddress $address */
        $address = $this->modx->newObject('msOrderAddress');
        $arrayor = array_merge($this->order, array(
            'user_id' => $user_id
        , 'createdon' => $createdon
        ));



        $arrayor['city_id_np'] = (isset($data['extended']['city_id'])?$data['extended']['city_id']:'');
        if ($this->order['delivery'] == 1)// самовывоз
        {
            $arrayor['warehouse'] = '';
            $arrayor['warehouse_id'] = '';
            $arrayor['street'] = '';
            $arrayor['building'] = '';
            $arrayor['room'] = '';
        } elseif (($this->order['delivery'] == 6) || ($this->order['delivery'] == 2) || ($this->order['delivery'] == 3)) {
            if (isset($data['textwarehouse']))
                $arrayor['warehouse'] = $data['textwarehouse'];
            else $arrayor['warehouse'] = '';

            $arrayor['warehouse_id'] = (!empty($data['extended']['warehouse'])?$data['extended']['warehouse']:$data['warehouse']);


            $warehouse_exist = $this->modx->getObject('msNpWarehouse', $arrayor['warehouse_id']);
            if ($warehouse_exist) {
                $arrayor['warehouse'] =$warehouse_exist->get('Description');// (!empty($data['textwarehouse']) ? $data['textwarehouse'] : '');
            }

            $arrayor['street'] = '';
            $arrayor['building'] = '';
            $arrayor['room'] = '';


//            if (isset($arrayor['city'])) {
//                if (!empty($arrayor['city'])) {
//                    $cityName=$arrayor['city'];
//                    $_cityName=explode('-',$cityName);
//                    if (isset($_cityName[0]))
//                        $cityName=trim($_cityName[0]);
//
//                    $city_exist = $this->modx->getObject('msNpCities', ['DescriptionRu' => $cityName]);
//
//                    if ($city_exist) {
//                        $arrayor['city_id_np'] = $city_exist->get('id');
//                    }
//                }
//            }
//            else
            if (isset($arrayor['city_id_np'])){
                if (!empty($arrayor['city_id_np'])) {
                    $city_exist = $this->modx->getObject('msNpCities', $arrayor['city_id_np']);
                    if ($city_exist)
                        $arrayor['city'] = $city_exist->get('Description');

                }

            }



            $root = dirname(__FILE__);

            if (empty( $arrayor['warehouse'])&&($data['textwarehouse']=='')) {
                if ($warehouses) {
                    $warehouse_id=false;
                    $warehouse_np='';
                    foreach ($warehouses as $warehouse) {
                        if ($warehouse['Ref'] == $arrayor['warehouse_id']) {
                            $warehouse_id=$warehouse['Ref'];//$arrayor['warehouse_id'];
                            $warehouse_np = $warehouse['DescriptionRu'];//DescriptionRu'];
                        }
                    }
                     $arrayor['warehouse_id'] = $warehouse_id;
                    $arrayor['warehouse'] = $warehouse_np;
                }
            }

            if (($arrayor['warehouse_id']=='')&&($arrayor['warehouse']!='')){
                if ($warehouses) {
                    $warehouse_id=false;
                    $warehouse_np='';
                    foreach ($warehouses as $warehouse) {
                        if ($warehouse['DescriptionRu'] == $arrayor['warehouse']) {
                            $warehouse_id=$warehouse['Ref'];
                            $arrayor['warehouse_id'] = $warehouse_id;
                            $warehouse_np = $warehouse['DescriptionRu'];//DescriptionRu'];
                        }
                    }

                    $arrayor['warehouse'] = $warehouse_np;
                }
            }

        } elseif ($this->order['delivery'] == 4) {
            $arrayor['warehouse'] = '';
            $arrayor['warehouse_id'] = '';
            $arrayor['street'] = $data['extended']['street'];
            $arrayor['building'] = $data['extended']['house'];


            $arrayor['room'] = $data['extended']['room'];

            $arrayor['housing'] = $data['extended']['housing'];
            $arrayor['parade'] = $data['extended']['parade'];
            $arrayor['doorphone'] = $data['extended']['doorphone'];
            $arrayor['floor'] = $data['extended']['floor'];


            if (!empty($data['extended']['city_id_np']))
            {
                $arrayor['city_id_np'] = $data['extended']['city_id_np'];

                $city_exist = $this->modx->getObject('msNpCities', $data['extended']['city_id_np']);
                if ($city_exist)
                    $arrayor['city']=$city_exist->get('Description');

            }


        } elseif (($this->order['delivery'] == 5)) {

            $arrayor['warehouse'] = '';
            $arrayor['warehouse_id'] = '';
            $arrayor['street'] = $data['extended']['street'];
            $arrayor['building'] = $data['extended']['house'];
            $arrayor['room'] = $data['extended']['room'];

            if (isset($data['extended']['street_np'])){
                if ($data['extended']['street_np']!='') {
                    $arrayor['street'] = $data['extended']['street_np'];
                }
            }

            if (isset($data['extended']['street_np_ref'])){
                if ($data['extended']['street_np_ref']!='')
                    $arrayor['street_id_np'] = $data['extended']['street_np_ref'];
            }

            if (isset($arrayor['city'])) {
                if ($arrayor['city']!='') {
                    $city_exist = $this->modx->getObject('msNpCities', ['DescriptionRu' => $arrayor['city']]);//array('id' => $ref));

                    if ($city_exist) {
                        $arrayor['city_id_np'] = $city_exist->get('id');
                    }
                }
            }else if (isset($arrayor['city_id_np'])){
                $city_exist = $this->modx->getObject('msNpCities', $arrayor['city_id_np']);
                if ($city_exist)
                {
                    $arrayor['city']=$city_exist->get('Description');
                }

            }






        }


        $arrayor['lastname'] = $data['lastname'];
        if ($arrayor['receiver'] == '') $arrayor['receiver'] = $data['lastname'] . ' ' . $arrayor['receiver'];

//        echo '<pre>';
//        print_r($arrayor);
//        echo '</pre>';
        $address->fromArray($arrayor);
        $order->addOne($address);

        // Adding products
        $cart = $this->ms2->cart->get();
        $total = 0;
        $products = array();
        $discount = 0;
        $costOrder=0;
        foreach ($cart as $v) {

            if ($tmp = $this->modx->getObject('msProduct', $v['id'])) {
                $name = $tmp->get('pagetitle');
            } else {
                $name = '';
            }
            if (isset($v['newprice'])) $v['price'] = $v['newprice'];

            $actionOption= $v['actionOption'];
            $idproduct = $v['id'];

            $where = array(
                'contentid' => $idproduct
            , 'tmplvarid' => 76
            );
            $tv = $this->modx->getObject('modTemplateVarResource', $where);
            $nodostavka = false;
            if ($tv) $nodostavka = $tv->get('value');

            if (isset($data['created_user']))// создать учётную запись
            {
                if ($v['action'] == '') {
                    $v['price'] = $v['oldprice'] - ($v['oldprice'] * ($this->user_discount_first / 100));
                    $v['price'] = round($v['price'], 2);
                }
            } elseif ($user) {
                if ($v['action'] == '') {
                    $user_id = $user->get('id');
                    $profile = $this->modx->getObject('modUserProfile', array('internalKey' => $user_id));
                    if ($profile) {
                        $profiledata = $profile->toArray();
                        $extended = $profiledata['extended'];
                        $loyalnost=$extended['discount'];
                        if ($actionOption==0) {
                            if ($nodostavka == 'yes') {
                                if ($loyalnost > 5)
                                    $loyalnost = 5;
                            }
                            if ($v['action'] < $loyalnost)
                                $v['price'] = $v['oldprice'] - ($v['oldprice'] * ($loyalnost / 100));
                        }
                    }
                }
            } else {


                //$v['price']=$v['oldprice'] - 0.05

                if ($nodostavka == 'yes') {
                    if ($actionOption==0) {
                        if (($v['action'] > 5) || (isset($extended['discount']) && ($extended['discount'] > 5))) {
                            //if (isset($extended['discount']) && ($extended['discount'] > 5)) {
                            $v['skidka1'] = 5;
                            $v['price'] = $v['oldprice'] - 0.05 * $v['oldprice'];
                            //}
                        }
                    }

                }
            }

            if ($nodostavka != 'yes') {
                if (($v['skidka1'] != 0) && (isset($v['skidka1'])) && ($v['skidka1'] != '')) {
                    $v['price'] = $v['oldprice'] - ($v['oldprice'] * ($v['skidka1'] / 100));
                    /*if ($_SERVER['HTTP_CF_CONNECTING_IP']=='78.46.62.217') {
                        echo 'price '.$v['price'].' '.($v['skidka1'] / 100).' '.$v['oldprice'];
                    }*/
                }
            }

            $id_1c='';
            $_id_1cOption='';

            $priceOption=$v['price'];
            $idproduct = $v['id'];
            $_id_1cOption = (isset($v['option_id'])?$v['option_id']:false);
            if (!$_id_1cOption) {
                $_where = array(
                    'contentid' => $idproduct
                , 'tmplvarid' => 1
                );
                $tvOptions = $this->modx->getObject('modTemplateVarResource', $_where);

                if ($tvOptions) {
                    $_options = $tvOptions->get('value');
                    if ($_options) {

                        $new_options = [];
                        $_id_1cGlobal = false;
                        $options = json_decode($_options, true);

                        foreach ($options as $_keyOpt => $opt) {
                            $_id_1c = false;
                            $optNew = $opt;

                            if (isset($opt['id_1c'])) {
                                if ($opt['id_1c'] == '')
                                    $_id_1c = false;
                                else
                                    $_id_1c = $opt['id_1c'];
                            }

                            if (!$_id_1c) {
                                $id1c = $idproduct . $opt['MIGX_id'] . time();
                                $optNew['id_1c'] = $id1c;
                                $_id_1cGlobal = true;
                            }

                            if (isset($v['options'])) {
                                if (isset($v['options']['size'])) {
                                    if ($v['options']['size'] == $opt['weight'] . ' ' . $opt['weight_prefix']) {
                                        $_id_1cOption = $_id_1c;
                                        $priceOption=$opt['price'];
                                    }
                                }

                            }
                            $new_options[$_keyOpt] = $optNew;
                        }

                        if ($_id_1cGlobal) {
                            $options = $new_options;
                            $new_optionsForSave = json_encode($new_options);

                            $tv = $this->modx->getObject('modTemplateVarResource', array('contentid' => $idproduct, 'tmplvarid' => 1));
                            if ($tv && ($new_optionsForSave != '')) {
                                $tv->set('value', $new_optionsForSave);
                                $tv->save();
                            }
                        }
                    }
                }
            }


            $optionsArray=json_decode($options,true);

            $old_pr=$v['price'];

            /* @var msOrderProduct $product */
            $product = $this->modx->newObject('msOrderProduct');
            $cost = $v['price'] * $v['count'];

            $v_new=array_merge($v, array(
                'product_id' => $v['id']
            , 'price_old' => round($v['oldprice'], 2)
            , 'discount' => round($v['skidka1'], 2)
            , 'name' => $name
            , 'cost' => $cost
            , 'id_1c'=> $_id_1cOption
            , 'cost_old' => round($v['oldprice'] * $v['count'], 2)
            ));
            if (!isset($v_new['price']))
            {
                $v_new['price']=$priceOption;
            }
            elseif($v_new['price']<=0){
                $v_new['price']=$priceOption;
            }
            $costOrder=$costOrder+$cost;

            $product->fromArray($v_new);
           /* $product->fromArray(array_merge($v, array(
                'product_id' => $v['id']
                , 'price_old' => round($v['oldprice'], 2)
                , 'discount' => round($v['skidka1'], 2)
                , 'name' => $name
                , 'cost' => $cost
                , 'id_1c'=> $_id_1cOption
                , 'cost_old' => round($v['oldprice'] * $v['count'], 2)
            )));*/
            $discount_temp = round($v['oldprice'], 2) - round($v['price'], 2);
            $discount += $discount_temp;
            $products[] = $product;
            $total = $total + $cost;
        }
        $order->set('discount', $discount);
        $order->set('cost', $costOrder+$delivery_cost);
        $order->set('cart_cost', $costOrder);

        //die();
        $order->addMany($products);

        $response = $this->ms2->invokeEvent('msOnBeforeCreateOrder', array(
            'msOrder' => $order,
            'order' => $this
        ));
//        echo '<pre>$response';
//        print_r($response);
//        echo '</pre>';
//        die();

        if (!$response['success']) {

            return $this->error($response['message']);
        }

        if ($apply_bonuses) {
            $order = $this->recordBonuses($apply_bonuses, $this->modx->user, $order, $total);
        }


        // отправим смс
        if ($order->save()) {

            if (isset($data['phone']) && ($data['phone'] != '')){

                $user_id = $order->get('user_id');//$this->ms2->getCustomerId();
                $user = $this->modx->getObject('modUser', array('id' => $user_id));
                $Profile=$user->getOne('Profile');
                $this->ms2->eventNotification(1, $user, [
                        'order_num'=>$order->get('num'),
                        'fullname'=>$Profile->get('fullname'),
                        'phone'=>$data['phone']
                    ]);
                //eventNotification
            }
                //$this->ms2->sendsms($data['phone'], 'Спасибо за заказ №' . $order->get('num') . '. Менеджер свяжется с Вами в ближайшее время');

            unset($_SESSION['user_for_order']);
            $response = $this->ms2->invokeEvent('msOnCreateOrder', array(
                'msOrder' => $order,
                'order' => $this
            ));

//        echo '<pre>$response';
//        print_r($response);femi
//        echo '</pre>';
//        die();

            if (!$response['success']) {
                return $this->error($response['message']);
            }

            $this->ms2->cart->clean();
            $this->clean();
            if (empty($_SESSION['minishop2']['orders'])) {
                $_SESSION['minishop2']['orders'] = array();
            }
            $_SESSION['minishop2']['orders'][] = $order->get('id');
            if ($object_promocode) {
                if ($object_promocode->get('type') == 'dinamic') $status = 0; else $status = 1;
                $object_promocode->set('status', $status);
                $object_promocode->set('order_id', $order->get('id'));
                $object_promocode->set('order_num', $order->get('num'));
                $object_promocode->save();
            }

            if ($userregistr) {
                $tpl = 'registrationEmailTpl';
                $dataprofile = $user->getOne('Profile')->toArray();
                $content = $this->modx->getChunk($tpl,
                    array_merge(
                        $dataprofile,
                        $user->toArray(),
                        $order->toArray(),
                        array(
                            'name' => $dataprofile['fullname'] . ' ' . $dataprofile['extended']['lastname'],
                            'login' => $user->get('username'),
                            'total_order' => $total,
                            'url_liqpay' => $link,
                            'num' => $order->get('num'),
                            'order_id' => $order->get('id'),
                            'password' => $password
                        )
                    )
                );

                $send = $user->sendEmail(
                    $content
                    , array(
                        'subject' => $this->modx->getOption('subject_registr')
                    )
                );
            }


            //$response =true;
            // Trying to set status "new"
            $response = $this->ms2->changeOrderStatus($order->get('id'), 1);


            // submit info to unisender

            $corePath = MODX_CORE_PATH.'components/unisender/';
            require_once $corePath.'model/unisender/unisender.class.php';



            $modelunisender = new Unisender($this->modx);

            $q = $this->modx->newQuery('modUser');
            $q->select([
                'modUser.username',
                'modUser.id',
                'modUser.registr',
                'modUser.created',
                'modUser.registr_date',
                'modUser.subscribe',
                'modUser.last_order',
                'modUser.total_orders',
            ]);
            $q->where(['modUser.id'=>$user_id]);

            $q->prepare();
            $q->stmt->execute();
            $result = $q->stmt->fetchAll(PDO::FETCH_ASSOC);

            foreach ($result as $res) {
                $user=$res;
                $profile_object=$this->modx->getObject('modUserProfile', array('internalKey'=> $res['id']));
                $profile=[];
                if ($profile_object)
                    $profile=$profile_object->toArray();


                $result=$modelunisender->UpdateSubscribeUser($user,$profile);
                //if ($result) break;
                sleep(0.5);
            }


           /* if ($_SERVER['HTTP_CF_CONNECTING_IP']=='78.46.62.217'){
                echo '<pre>$response';
                print_R($response);
                echo '</pre>';
            }*/
//            echo '<pre>';
//            print_r($data);
//            echo '</pre>';
            if ($response !== true) {
                return $this->error($response, array('msorder' => $order->get('id')));
            } /* @var msPayment $payment */
            elseif ($payment = $this->modx->getObject('msPayment', array('id' => $order->get('payment'), 'active' => 1))) {
               if (($data['ms2_action']=='order/submitpayment')&&( $order->get('payment')==4)) {

                    $response = $payment->send($order,'submitpayment');


//                    echo '<pre>$response'.$order->get('payment');
//                    print_r($response);
//                    echo '</pre>';
//
//
//
//                    die();
                    if ($this->config['json_response']) {
                        @session_write_close();
                        exit(is_array($response) ? $this->modx->toJSON($response) : $response);
                    } else {


                        if (!empty($response['data']['redirect'])) {
                            $this->modx->sendRedirect($this->modx->context->makeUrl($this->modx->resource->id, array('msorder' => $response['data']['msorder'])));
                            exit();
                            //$this->modx->sendRedirect($response['data']['redirect']);
                            //exit();
                        } elseif (!empty($response['data']['msorder'])) {
                            $this->modx->sendRedirect($this->modx->context->makeUrl($this->modx->resource->id, array('msorder' => $response['data']['msorder'])));
                            exit();
                        } else {
                            $this->modx->sendRedirect($this->modx->context->makeUrl($this->modx->resource->id));
                            exit();
                        }
                    }
                }else{
                    $response = $payment->send($order);

                    if ($this->config['json_response']) {
                        @session_write_close();
                        exit(is_array($response) ? $this->modx->toJSON($response) : $response);
                    } else {


                        if (!empty($response['data']['redirect'])) {
                            $this->modx->sendRedirect($response['data']['redirect']);
                            exit();
                        } elseif (!empty($response['data']['msorder'])) {
                            $this->modx->sendRedirect($this->modx->context->makeUrl($this->modx->resource->id, array('msorder' => $response['data']['msorder'])));
                            exit();
                        } else {
                            $this->modx->sendRedirect($this->modx->context->makeUrl($this->modx->resource->id));
                            exit();
                        }
                    }
                }
            } else {

                if ($this->ms2->config['json_response']) {
                    return $this->success('', array('msorder' => $order->get('id')));
                } else {
                    /*if ($_SERVER['HTTP_CF_CONNECTING_IP']=='78.46.62.217') {
                        echo '<pre>$response3';
                        print_R($response);
                        echo '</pre>';
                    }*/
                    $this->modx->sendRedirect($this->modx->context->makeUrl($this->modx->resource->id, array('msorder' => $response['data']['msorder'])));
                    exit();
                }
            }
            /*if ($_SERVER['HTTP_CF_CONNECTING_IP']=='78.46.62.217'){
                die();
            }*/


        }
        return $this->error();
    }

    /** @inheritdoc} */
    public function clean()
    {
        $response = $this->ms2->invokeEvent('msOnBeforeEmptyOrder', array('order' => $this));
        if (!$response['success']) {
            return $this->error($response['message']);
        }
        unset($_SESSION['minishop2']['promocode']);
        unset($_SESSION['minishop2']['cart_total']);
        $this->order = array();
        $response = $this->ms2->invokeEvent('msOnEmptyOrder', array('order' => $this));
        if (!$response['success']) {
            return $this->error($response['message']);
        }

        return $this->success('', array());
    }

    // проверка промокода
    public function promocode($data = array())
    {

        $cart = $this->ms2->cart->status();
        $order = $this->get();
        $_SESSION['minishop2']['cart_total']['economy'] = 0;
        // ищем промокод в базе
        $promocode = $data['promocode'];

        $object_promocode = $this->modx->getObject('PromocodeItem', array(
            'code' => $promocode,
        ));
        if ($object_promocode) {
            if ($object_promocode->get('status') == 0) {
                $response['message'] = 'Этот промо-код уже был использован';
                $response['success'] = false;

            } else {

                $term = $object_promocode->get('term');
                $discount = $object_promocode->get('discount');
                $summa = $object_promocode->get('summa');
                $category = $object_promocode->get('category');
                $total_cost = $cart['total_cost'];


                if (($term == '0000-00-00 00:00:00') || ($term == '1970-01-01 00:00:00') || ($term == NULL) || (date('Y-m-d H:i:s') <= $term)) {
                    //echo $summa.' '.$cart['total_cost'];

                    if (($summa > 0) && ($summa != '')) {
                        if (($total_cost >= $summa) && ($category == 10))
                            $response = $this->use_promocode($object_promocode->toArray());
                        elseif ($category == 10) {
                            $response['message'] = 'Не выполнены условия по минимальной стоимости заказа';
                            $response['success'] = false;
                        } elseif (($total_cost >= $summa) && ($category != 0)) {// если применение к категории
                            $response = $this->use_promocode($object_promocode->toArray());//$code,$discount);
                        } elseif (($cart['total_cost'] >= $summa) && ($category == 0)) {
                            $response = $this->use_promocode($object_promocode->toArray());//$code,$discount);
                        } else {
                            $response['message'] = 'Не выполнены условия по минимальной стоимости заказа';
                            $response['success'] = false;
                            $promocode = '';
                            $_SESSION['minishop2']['promocode'] = '';
                        }
                        //  проверим сумма заказа

                    } else {

                        $response = $this->use_promocode($object_promocode->toArray());//$object_promocode);//$promocode,$discount);

                    }
                } else {
                    $response['message'] = 'Срок действия этого промо-кода истек';
                    $response['success'] = false;
                    $promocode = '';
                    $_SESSION['minishop2']['promocode'] = '';
                }
            }
            if ($response['success'] == true) {
                $_SESSION['minishop2']['promocode'] = $promocode;
            }

        } else {
            $response['message'] = 'Промо-код не найден. Проверьте написание или обратитесь к менеджеру';
            $response['success'] = false;//Этот промо-код уже был использован
        }
        if (!$response['success']) return $this->error($response['message']);
        else return $this->success($response['message'], array(
            'promocode' => $promocode,
            'cost_delivery' => $response['data']['cost_delivery'],
            'total_cost' => $response['data']['total_cost'],
            'economy' => $response['economy'],
            'discount' => $discount,
            'cart' => $response['data']['items'],
            'total_cost_withdelivery' => $response['data']['total_cost_withdelivery']));

        return $response;

    }


    public function promocode_cart($promocode)
    {
        $cart = $this->ms2->cart->status();
        $object_promocode = $this->modx->getObject('PromocodeItem', array(
            'code' => $promocode,
        ));
        $promous = true;
        if ($object_promocode) {
            if ($object_promocode->get('status') == 0) {
                $promous = false;

            } else {

                $term = $object_promocode->get('term');
                $discount = $object_promocode->get('discount');
                $summa = $object_promocode->get('summa');
                $category = $object_promocode->get('category');
                $total_cost = $cart['total_cost'];


                if (($term == '0000-00-00 00:00:00') || ($term == '1970-01-01 00:00:00') || ($term == NULL) || (date('Y-m-d H:i:s') <= $term)) {


                    if (($summa > 0) && ($summa != '')) {
                        if (($total_cost >= $summa) && ($category == 10)) {

                        } elseif ($category == 10) {
                            //$promous=false;
                        } elseif (($total_cost >= $summa) && ($category != 0)) {// если применение к категории

                        } elseif (($cart['total_cost'] >= $summa) && ($category == 0)) {

                        } else {
                            //$promous=false;
                        }

                    } else {
                        $response = $this->use_promocode($object_promocode->toArray());//$object_promocode);//$promocode,$discount);

                    }
                } else {
                    $promous=false;
                }
            }
        } else {
            $promous = false;
        }


        return $promous;
    }


    private function use_promocode($promocode)//$code='',$discount=0)
    {
        $code = $promocode['code'];
        $discount = $promocode['discount'];
        if ($code != '') {
            $promous = true;


            $object_promocode = $this->modx->getObject('PromocodeItem', array(
                'code' => $code,
            ));
            if ($object_promocode) {
                if ($object_promocode->get('status') == 0) {
                    $promous = false;
                } else {
                    $cart = $this->ms2->cart->status();
                    $term = $object_promocode->get('term');
                    $discount = $object_promocode->get('discount');
                    $summa = $object_promocode->get('summa');
                    $category = $object_promocode->get('category');
                    $total_cost = $cart['total_cost'];


                    if (($term == '0000-00-00 00:00:00') || ($term == '1970-01-01 00:00:00') || ($term == NULL) || (date('Y-m-d H:i:s') <= $term)) {


                        if (($summa > 0) && ($summa != '')) {
                            if (($total_cost >= $summa) && ($category == 10)) {

                            } elseif ($category == 10) {
                                //$promous=false;
                            } elseif (($total_cost >= $summa) && ($category != 0)) {// если применение к категории

                            } elseif (($cart['total_cost'] >= $summa) && ($category == 0)) {

                            } else {

                                //$promous=false;
                            }

                        } else {
                            //$response=$this->use_promocode($object_promocode->toArray());//$object_promocode);//$promocode,$discount);

                        }
                    } else {
                        $promous=false;
                    }
                }
            } else {
                $promous = false;
                //echo 'wewewe';
            }


            if ($promous) {

                $cart = $this->ms2->cart->status(array('promocode' => $promocode));//array('code'=>$code,'discount'=>$discount)


                if ((isset($cart['successpromocode'])) && ($cart['successpromocode'] == true)) {
                    if (isset($_SESSION['minishop2']['cart_total']['error'])) {
                        $response['message'] = $_SESSION['minishop2']['cart_total']['error'];
                        $response['success'] = false;
                    } else {
                        $response['message'] = 'Промо-код успешно применен';
                        $response['success'] = true;
                        $response['discount'] = $discount;

                        $response['economy'] = $cart['economy'];
                        $response['data'] = $cart;
                    }
                } else {
                    if (isset($_SESSION['minishop2']['cart_total']['error']))
                        $response['message'] = $_SESSION['minishop2']['cart_total']['error'];
                    else
                        $response['message'] = 'Промо-код не действует на товары в корзине';
                    $response['success'] = false;
                }
            } else {
                $response['message'] = '';
                $response['success'] = false;
            }
        } else {
            $response['message'] = 'Ошибка промо-кода';
            $response['success'] = false;

        }
        return $response;

    }

    /** @inheritdoc} */
    public function getCost($with_cart = true, $only_cost = false, $delivery = '', $city = '')
    {
        $cart = $this->ms2->cart->status();
        $cost = $with_cart
            ? $cart['total_cost']
            : 0;
        $cost_delivery = 0;
        if ($delivery == 4) {// если курьерская доставка
            $free = (float)$this->modx->getOption('free_delivery_Odessa');
            //echo
            $cart['total_cost'] = (float)$cart['total_cost'];//.' '.$free;die();

            if ($city == $this->ms2->cityOdessa) // если Одесса
            {
                $cost_delivery = $this->modx->getOption('delivery_odessa_kurier');
                if ($cart['total_cost'] >= $free) $cost_delivery = 0;
            } else {
                $free = (float)$this->modx->getOption('free_delivery_region');

                // елси пригород одессы
                $cost_delivery = 0;
                $qcity = $this->modx->getObject('msLocalities', $city);
                if ($qcity) {
                    $region = $qcity->get('region');

                    if ($region == 'Одесская') {

                        $cost_delivery = $this->modx->getOption('delivery_odessa_prigorod_kurier');//70;
                        if ($cart['total_cost'] >= $free) $cost_delivery = 0;
                    } else {
                        if ($cart['total_cost'] >= $free) $cost_delivery = 0;
                    }
                }else{
                    $qcity = $this->modx->getObject('msNpCities', $city);
                     if ($qcity) {
                         $region = $qcity->get('AreaDescriptionRu');

                         if ($region == 'Одеська') {

                             $cost_delivery = $this->modx->getOption('delivery_odessa_prigorod_kurier');//70;
                             if ($cart['total_cost'] >= $free) $cost_delivery = 0;
                         } else {
                             if ($cart['total_cost'] >= $free) $cost_delivery = 0;
                         }
                     }
                }

            }
        }
        /* @var msDelivery $delivery */
        if (!empty($this->order['delivery']) && $delivery = $this->modx->getObject('msDelivery', $this->order['delivery'])) {
            $cost = $delivery->getCost($this, $cost);
        }


        /* @var msPayment $payment */
        if (!empty($this->order['payment']) && $payment = $this->modx->getObject('msPayment', $this->order['payment'])) {
            $cost = $payment->getCost($this, $cost);
        }
        if ($cost_delivery != 0) $cost = $cost_delivery;
        return $only_cost
            ? $cost
            : $this->success('', array('cost' => $cost));
    }


    /**
     * Return current number of order
     *
     * @return string
     */
    public function getnum()
    {
        $table = $this->modx->getTableName('msOrder');
        $cur = date('md');

        $sql = $this->modx->query("SELECT `num` FROM {$table} WHERE `num` LIKE '{$cur}%' ORDER BY `id` DESC LIMIT 1");
        $num = $sql->fetch(PDO::FETCH_COLUMN);

        if (empty($num)) {
            $num = date('md') . '0';
        }
        $num = explode(date('md'), $num);
        $num = $cur . ($num[1] + 1);//'/'.

        return $num;
    }


    /**
     * Shorthand for MS2 error method
     *
     * @param string $message
     * @param array $data
     * @param array $placeholders
     *
     * @return array|string
     */
    public function error($message = '', $data = array(), $placeholders = array())
    {
        return $this->ms2->error($message, $data, $placeholders);
    }


    /**
     * Shorthand for MS2 success method
     *
     * @param string $message
     * @param array $data
     * @param array $placeholders
     *
     * @return array|string
     */
    public function success($message = '', $data = array(), $placeholders = array())
    {
        return $this->ms2->success($message, $data, $placeholders);
    }


    /**
     * Ucfirst function with support of cyrillic
     *
     * @param string $str
     *
     * @return string
     */
    public function ucfirst($str = '')
    {
        if (!preg_match('/[a-zа-яіїєёЁ]/iu', $str)) {
            return '';
        } elseif (strpos($str, '-') !== false) {
            $tmp = array_map(array($this, __FUNCTION__), explode('-', $str));
            return implode('-', $tmp);
        }

        if (function_exists('mb_substr') && preg_match('/[а-я]/iu', $str)) {
            $tmp = mb_strtolower($str, 'utf-8');
            $str = mb_substr(mb_strtoupper($tmp, 'utf-8'), 0, 1, 'utf-8') . mb_substr($tmp, 1, mb_strlen($tmp) - 1, 'utf-8');
        } else {
            $str = ucfirst(strtolower($str));
        }

        return $str;
    }
}