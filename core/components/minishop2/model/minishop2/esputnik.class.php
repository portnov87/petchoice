<?php

/**
 * The base class for miniShop2.
 *
 * @package minishop2
 */
class Esputnik
{
    protected $apiUrl = 'https://esputnik.com/api';
    protected $user = 'petchoice.info@gmail.com';
    protected $password = 'Sput58EmaiLL';
    protected $apiToken = 'CB9507932D0D74D3335A4BF6F93CF59A';

//
    function __construct(modX &$modx, array $config = array())
    {

        $this->modx =& $modx;

    }

    public function getPromocodes()
    {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET");
        //curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($data));

        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            'Accept: application/json', 'Content-Type: application/json'

        ));
        curl_setopt($ch, CURLOPT_URL, $this->apiUrl . '/v1/promocodes');
        curl_setopt($ch, CURLOPT_USERPWD, $this->user . ':' . $this->password);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_SSLVERSION, 6);


        $response = curl_exec($ch);
        $err = curl_error($ch);

        curl_close($ch);

        $fieldsResult = [];
        return $response;
    }

    public function sentPreprocessor($filePath, $type)
    {

        $request_entity = new stdClass();
        $data=[];
        //echo '$type = '.$type. ' $filePath= '.$filePath;
        //$content=file_get_contents($filePath);
        switch ($type)
        {
            case 'recommendation':
                $eSputnik_file=$this->modx->getOption('eSputnik_file_recomend',false);
                $request_entity->preprocessorType='Key';
                break;
            case 'top_action':
                $eSputnik_file=$this->modx->getOption('eSputnik_file_top_action',false);
                $request_entity->preprocessorType='Random';
                break;

            case 'blog':
                $eSputnik_file=$this->modx->getOption('eSputnik_file_blog',false);
                $request_entity->preprocessorType='Random';
                break;

        }

        if ($eSputnik_file)
        {

            $method='preprocessor/file/'.$eSputnik_file;
        }else
            $method='preprocessor/file/upload';


        $request_entity->link=$filePath;
//        $data['preprocessorType']='Key';
//        $data['link']=$filePath;


        //echo '$method = '.$method;



        $response=$this->sentRequest($method,$request_entity);

        $output = $this->modx->fromJSON($response);
        echo '<pre>$output';
        print_r($output);
        echo '</pre>';
        if ($output['isValid']) {
            if (isset($output['fileId']))
                return ['result' => true, 'fileId' => $output['fileId'], 'message' => ''];
            else return ['result' => true, 'fileId' => false, 'message' => 'update '.$eSputnik_file];
        }
        else
            return ['result' => false, 'fileId' => false, 'message' => ''];
        //return false;

//        echo '<pre>';
//        print_r($response);
//        echo '</pre>';
    }

    public function sentRequest($method,$request_entity)
    {
        echo json_encode($request_entity);
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($request_entity));
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Accept: application/json', 'Content-Type: application/json'));
        curl_setopt($ch, CURLOPT_URL, $this->apiUrl . '/v1/'.$method);
        curl_setopt($ch, CURLOPT_USERPWD, $this->user . ':' . $this->password);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_SSLVERSION, 6);
        echo $this->apiUrl . '/v1/'.$method;
        echo '<pre>';
        print_r($request_entity);
        echo '</pre>';
        $response = curl_exec($ch);
        echo '<pre>$response';
        print_r($response);
        echo '</pre>';
        $err = curl_error($ch);
        echo '<pre>$err';
        print_r($err);
        echo '</pre>';
        curl_close($ch);
        return $response;
    }
    public function sentPromocode($data)
    {
        //$fields=['file'=>$data];

        $request_entity = new stdClass();
        $request_entity->promocodes = $data['promocodes'];//array('TEST001', 'TEST002');
        if (isset($data['type']))
            $request_entity->type = $data['type'];

        if (isset($data['expirationDate']))
            $request_entity->expirationDate = $data['expirationDate'];

        if (isset($data['discount']))
            $request_entity->discount = $data['discount'];

        if (isset($data['inUse']))
            $request_entity->inUse = $data['inUse'];


        echo json_encode($request_entity);

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($request_entity));
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Accept: application/json', 'Content-Type: application/json'));
        curl_setopt($ch, CURLOPT_URL, $this->apiUrl . '/v1/promocodes');
        curl_setopt($ch, CURLOPT_USERPWD, $this->user . ':' . $this->password);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_SSLVERSION, 6);
        $response = curl_exec($ch);
        curl_close($ch);
        //echo $response;

        $output = $this->modx->fromJSON($response);
        return $output;

    }


    public function getAdditionFieldsEsputnik()
    {

        $fieldsResult = [];

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Accept: application/json', 'Content-Type: application/json'));
        curl_setopt($ch, CURLOPT_URL, $this->apiUrl . '/v1/addressbooks');
        curl_setopt($ch, CURLOPT_USERPWD, $this->user . ':' . $this->password);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_SSLVERSION, 6);

        $output = curl_exec($ch);

        curl_close($ch);

        $fieldsResult = [];
        $output = $this->modx->fromJSON($output);
        $fieldsResult['main'] = [];
        $fieldsResult['pets'] = [];
        if (isset($output['addressBook'])) {
            if (isset($output['addressBook']['fieldGroups'])) {
                foreach ($output['addressBook']['fieldGroups'] as $grp) {
                    switch ($grp['name']) {
                        case 'Main':
                            $fieldsResult['main'] = $grp['fields'];
                            break;
                        case 'Pet1':
                            $fieldsResult['pets'][1] = $grp['fields'];
                            break;
                        case 'Pet2':
                            $fieldsResult['pets'][2] = $grp['fields'];
                            break;
                        case 'Pet3':
                            $fieldsResult['pets'][3] = $grp['fields'];
                            break;
                        case 'Pet4':
                            $fieldsResult['pets'][4] = $grp['fields'];
                            break;
                        case 'Pet5':
                            $fieldsResult['pets'][5] = $grp['fields'];
                            break;
                    }
                }
            }
        }
        return $fieldsResult;

    }


    public function getOrdersDelivery($userId)
    {
        $sql = "SELECT orders.createdon 
FROM modx_ms2_orders orders 
WHERE user_id='$userId' and `status`=2 ORDER BY createdon DESC LIMIT 1";

        $q = $this->modx->prepare($sql);
        $q->execute();
        $result = $q->fetchAll(PDO::FETCH_ASSOC);
        foreach ($result as $item)
            return $item['createdon'];

        return false;
    }

    public function getPetsByUserId($userId)
    {
        //die();
        $sql = "SELECT pets.name, pets.bday, pets.bmonth, pets.byear, pet_types.name as type_name FROM modx_ms2_pets pets
LEFT JOIN modx_ms2_pet_types pet_types ON (pets.type=pet_types.id) 
WHERE user_id='$userId'";

        $q = $this->modx->prepare($sql);
        $q->execute();
        $result = $q->fetchAll(PDO::FETCH_ASSOC);

        return $result;

    }

    public function searchContact($email)
    {


        $ch = curl_init();
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Accept: application/json', 'Content-Type: application/json'));
        curl_setopt($ch, CURLOPT_URL, $this->apiUrl . '/v1/contacts?email=' . $email);
        curl_setopt($ch, CURLOPT_USERPWD, $this->user . ':' . $this->password);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_SSLVERSION, 6);
        $output = curl_exec($ch);
        $output = json_decode($output, true);

//        echo '<pre>$output search';
//        print_r($output);
//        echo '</pre>';
//        die();
        //echo($output);
        curl_close($ch);

        foreach ($output as $out) {
            if (isset($out['id'])) {
                $contactId = $out['id'];
                return $contactId;
            }
        }

        return false;

    }

    public function updateContact($email, $postData)
    {
        $contactId = $this->searchContact($email);
        if ($contactId) {
            $esputnikFields = $this->getAdditionFieldsEsputnik();
            $json_value = [];

            $result = $this->setDataObject($postData, $esputnikFields, []);
//            echo '<pre>$contact';
//            print_r($contact);
//            echo '</pre>';

            $contact = $result['contact'];
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "PUT");
            curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($contact));//json_encode($json_value));
            curl_setopt($ch, CURLOPT_HEADER, 1);
            curl_setopt($ch, CURLOPT_HTTPHEADER, array('Accept: application/json', 'Content-Type: application/json'));

            curl_setopt($ch, CURLOPT_URL, $this->apiUrl . '/v1/contact/' . $contactId);
            curl_setopt($ch, CURLOPT_USERPWD, $this->user . ':' . $this->password);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_SSLVERSION, 6);
            $output = curl_exec($ch);
            curl_close($ch);

            return $output;

        }
        return false;
    }

    public function createContact($_u)
    {


        $esputnikFields = $this->getAdditionFieldsEsputnik();

        $contacts = [];
        //$contact=$this->setDataObject($_u,$esputnikFields);


        $result = $this->setDataObject($_u, $esputnikFields, []);


        $contact = $result['contact'];
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($contact));
        curl_setopt($ch, CURLOPT_HEADER, 1);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Accept: application/json', 'Content-Type: application/json'));

        curl_setopt($ch, CURLOPT_URL, $this->apiUrl . '/v1/contact');
        curl_setopt($ch, CURLOPT_USERPWD, $this->user . ':' . $this->password);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_SSLVERSION, 6);
        $output = curl_exec($ch);
        curl_close($ch);
        return $output;


    }

    public function deleteContact($email)
    {
        $contactId = $this->searchContact($email);
        if ($contactId) {
            //$contactId
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_HEADER, 0);
            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "DELETE");
            curl_setopt($ch, CURLOPT_HTTPHEADER, array('Accept: application/json', 'Content-Type: application/json'));
            curl_setopt($ch, CURLOPT_URL, $this->apiUrl . '/v1/contact/' . $contactId);
            curl_setopt($ch, CURLOPT_USERPWD, $this->user . ':' . $this->password);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_SSLVERSION, 6);

            $output = curl_exec($ch);

            //echo($output);
            curl_close($ch);
        }

    }

    public function setDataObject($_u, $esputnikFields, $customFieldsIDs = array())
    {

//        echo '<pre>$_u';
//        print_r($_u);
//        echo '</pre>';
        //$customFieldsIDs=[];
        $contact = new stdClass();
        $contact->firstName = $_u['profile']['fullname'];
        $contact->lastName = $_u['profile']['lastname'];
        $contact->channels = array(
            array('type' => 'email', 'value' => $_u['profile']['email']),
            array('type' => 'sms', 'value' => $_u['user']['username'])
        );
        $extendedJson = $_u['profile']['extended'];
        $extended = $this->modx->fromJSON($extendedJson);
        if (!empty($_u['profile']['city']) && (isset($extended['street']))) {
            $address = new stdClass();
            $address->town = $_u['profile']['city'];
            if (!empty($extended['street']))
                $address->address = $extended['street'];//(isset($extended['street']) ? $extended['street'] : '');

            $contact->address = $address;
        }

        $fields = [];


        $pets = $this->getPetsByUserId($_u['user']['id']);
        foreach ($esputnikFields as $espField) {
            foreach ($espField as $_espFieldKey => $_espField) {

                if (isset($_espField['fieldKey'])) {
                    /**/
                    switch ($_espField['fieldKey']) {
                        case 'REGISTRACIYA':

                            $field = new stdClass();
                            $field->id = $_espField['id'];
                            $field->value = ($_u['user']['registr'] == 1 ? 'Да' : 'Нет');//html_entity_decode();
                            $fields[] = $field;
                            if (!in_array($_espField['id'], $customFieldsIDs))
                                $customFieldsIDs[] = $_espField['id'];
                            break;
                        case 'SKIDKA':
                            if (isset($extended['discount'])) {
                                $field = new stdClass();
                                $field->id = $_espField['id'];
                                $field->value = $extended['discount'];
                                $fields[] = $field;
                                if (!in_array($_espField['id'], $customFieldsIDs))
                                    $customFieldsIDs[] = $_espField['id'];
                            }
                            break;
                        case 'SUMMA_POKUPOK':

                            if (isset($_u['user']['summa_close_orders'])) {
                                $field = new stdClass();
                                $field->id = $_espField['id'];
                                $summa = (float)str_replace(',', '.', $_u['user']['summa_all_orders']);
                                $summa = round($summa, 0);
                                $field->value = $summa;
                                $fields[] = $field;
                                if (!in_array($_espField['id'], $customFieldsIDs))
                                    $customFieldsIDs[] = $_espField['id'];
                            }
                            break;
                        case 'KOLICHESTVO_ZAKAZOV':

                            if (isset($_u['user']['total_orders'])) {
                                $field = new stdClass();
                                $field->id = $_espField['id'];//count_orders_close

                                $total_orders = (float)$_u['user']['total_orders'];
                                $field->value = (int)round($total_orders, 0);//total_orders
                                $fields[] = $field;
                                if (!in_array($_espField['id'], $customFieldsIDs))
                                    $customFieldsIDs[] = $_espField['id'];
                            }
                            break;
                        case 'DATA_ZAKRYTOGO_ZAKAZA':

                            $dateZacrOrder = $this->getOrdersDelivery($_u['user']['id']);


                            if ($dateZacrOrder != '0000-00-00 00:00:00') {


                                $day = explode(' ', $dateZacrOrder);
                                if (isset($day[0])) {
                                    if ($day[0] != '0000-00-00') {
                                        $field = new stdClass();
                                        $field->id = $_espField['id'];//count_orders_close
                                        $field->value = $day[0];//total_orders
                                        $fields[] = $field;
                                        if (!in_array($_espField['id'], $customFieldsIDs))
                                            $customFieldsIDs[] = $_espField['id'];
                                    }
                                }


                            }

                            //if (isset($_u['user']['total_orders'])) {

                            //}
                            break;
                        case 'DATA_REGISTRACII':


                            if (isset($_u['user']['registr'])) {
                                if ($_u['user']['registr'] == 1) {
                                    if ($_u['user']['registr_date'] != '0000-00-00 00:00:00') {


                                        $day = explode(' ', $_u['user']['registr_date']);
                                        if (isset($day[0])) {
                                            if ($day[0] != '0000-00-00') {
                                                $field = new stdClass();
                                                $field->id = $_espField['id'];//count_orders_close
                                                $field->value = $day[0];//$_u['user']['registr_date'];//total_orders
                                                $fields[] = $field;
                                                if (!in_array($_espField['id'], $customFieldsIDs))
                                                    $customFieldsIDs[] = $_espField['id'];
                                            }
                                        }


                                    }
                                }
                            }
                            break;


                        case 'DATA_POSLEDNEGO_ZAKAZA':

                            if (isset($_u['user']['last_order'])) {
                                $day = explode(' ', $_u['user']['last_order']);
                                if (isset($day[0])) {
                                    if ($day[0] != '0000-00-00') {


                                        $field = new stdClass();
                                        $field->id = $_espField['id'];

                                        $field->value = $day[0];
                                        $fields[] = $field;
                                        if (!in_array($_espField['id'], $customFieldsIDs))
                                            $customFieldsIDs[] = $_espField['id'];
                                    }
                                }
                            }
                            break;
                        case 'BONUSY':


                            if (isset($_u['user']['bonuses'])) {
                                $field = new stdClass();
                                $field->id = $_espField['id'];
                                $field->value = $_u['user']['bonuses'];
                                $fields[] = $field;
                                if (!in_array($_espField['id'], $customFieldsIDs))
                                    $customFieldsIDs[] = $_espField['id'];
                            }
                            break;
                        case 'DATA_SGORANIYA_BONUSOV':


                            if (isset($_u['user']['date_clear_bonuses'])) {

                                if ($_u['user']['date_clear_bonuses'] != '-001-11-30 00:00:00') {
                                    $day = explode(' ', $_u['user']['date_clear_bonuses']);
                                    if (isset($day[0])) {
                                        if ($day[0] != '0000-00-00') {
                                            $field = new stdClass();
                                            $field->id = $_espField['id'];

                                            $field->value = $day[0];
                                            $fields[] = $field;
                                            if (!in_array($_espField['id'], $customFieldsIDs))
                                                $customFieldsIDs[] = $_espField['id'];
                                        }
                                    }
//                                    $field = new stdClass();
//                                    $field->id = $_espField['id'];
//                                    $field->value = $_u['user']['date_clear_bonuses'];
//                                    $fields[] = $field;
                                }
                            }
                            break;

                    }

                }

            }
        }

        if (count($pets) > 0) {
            if (isset($esputnikFields['pets'])) {
                foreach ($pets as $_key => $pet) {
                    $keyPets = $_key + 1;
                    if (isset($esputnikFields['pets'][$keyPets])) {
                        foreach ($esputnikFields['pets'][$keyPets] as $_espField) {

                            switch ($_espField['fieldKey']) {
                                case 'TIP_PITOMCA':
                                    $field = new stdClass();
                                    $field->id = $_espField['id'];
                                    $field->value = $pet['type_name'];
                                    $fields[] = $field;
                                    if (!in_array($_espField['id'], $customFieldsIDs))
                                        $customFieldsIDs[] = $_espField['id'];
                                    break;
                                case 'IMYA_PITOMCA':
                                    if (!empty($pet['name'])) {
                                        $field = new stdClass();
                                        $field->id = $_espField['id'];
                                        $field->value = $pet['name'];
                                        $fields[] = $field;
                                        if (!in_array($_espField['id'], $customFieldsIDs))
                                            $customFieldsIDs[] = $_espField['id'];
                                    }
                                    break;
                                case 'DR_PITOMCA':
                                    if (($pet['byear'] > 0) && ($pet['bmonth'] > 0) && ($pet['bday'] > 0)) {
                                        $petMonth = $pet['bmonth'];
                                        $petDay = $pet['bday'];
                                        if (strlen((string)$petMonth) < 2)
                                            $petMonth = '0' . $petMonth;

                                        if (strlen((string)$petDay) < 2)
                                            $petDay = '0' . $petDay;

                                        $field = new stdClass();
                                        $field->id = $_espField['id'];
                                        $dateBD = $pet['byear'] . '-' . $petMonth . '-' . $petDay;
                                        $field->value = $dateBD;
                                        $fields[] = $field;
                                        if (!in_array($_espField['id'], $customFieldsIDs))
                                            $customFieldsIDs[] = $_espField['id'];
                                    }
                                    break;
                            }
                        }


                    }
                }
            }
        }


        $contact->fields = $fields;
        $result = [];
        $result['contact'] = $contact;
        $result['customFieldsIDs'] = $customFieldsIDs;
        return $result;

    }

    public function uploadCustomers($users)
    {

        $esputnikFields = $this->getAdditionFieldsEsputnik();
//        echo '<pre>$esputnikFields';
//        print_r($esputnikFields);
//        echo '</pre>';
//        die();
        $contacts = [];
        $customFieldsIDs = [];
        foreach ($users as $_u) {
            $contact = $this->setDataObject($_u, $esputnikFields, $customFieldsIDs);
            $customFieldsIDs = $contact['customFieldsIDs'];
            $contact = $contact['contact'];
            //setDataObject
            $contacts[] = $contact;
        }

        if (count($contacts) > 0) {
            $import_contacts_url = $this->apiUrl . '/v1/contacts';


            $request_entity = new stdClass();
            $request_entity->contacts = $contacts;//array($contact);
            $request_entity->dedupeOn = 'sms';//email';
            $request_entity->contactFields = array('firstName', 'lastName', 'email', 'sms');
            $request_entity->customFieldsIDs = $customFieldsIDs;
            $request_entity->groupNames = array('Petchoice');

            $resultApi = $this->send_request($import_contacts_url, $request_entity);
            $_resultApi = json_decode($resultApi, true);//$this->modx->fromJSON($extendedJson);


            if (isset($_resultApi['failedContacts'])) {


            }


            echo '<pre>$customFieldsIDs';
            print_r($customFieldsIDs);
            echo '</pre>';
            echo '<pre>$_resultApi ';
            print_r($_resultApi);
            echo '</pre>';

            return $resultApi;
        }

        return false;

    }

    public function getCustomers()
    {

//        echo '<pre>';
//        print_r($result);
//        echo '</pre>';
//        die();
        $stopwhile = true;
        $limit = 1000;//0;
        $step = 0;
        while ($stopwhile) {

            //while {
            $q = $this->modx->newQuery('modUser');
            $q->select([
                'modUser.username',
                'modUser.id',
                'modUser.registr',
                'modUser.created',
                'modUser.registr_date',
                'modUser.subscribe',
                'modUser.last_order',
                'modUser.total_orders',
                'modUser.bonuses',
                'modUser.summa_close_orders',
                'modUser.summa_all_orders',
                'modUser.date_clear_bonuses',
                'modUser.count_orders_close'

            ]);
            $q->sortby('modUser.created', 'DESC');

//            $q->where([
//                'modUser.id:=' => 66931
//            ]);
            $q->limit($limit, $step);

            $q->prepare();
            $sql = $q->toSQL();

            //echo $sql."\r\n\r\n";
//            die();
            $q->stmt->execute();

            $result = $q->stmt->fetchAll(PDO::FETCH_ASSOC);

            if (count($result) > 0) {

                $users = [];
                foreach ($result as $res) {
                    // $user = $res;
                    $profile = $this->getProfileUser($res['id']);

//                    if (isset($profile['email'])) {
//                        if ($profile['email'] != '') {

                    $users[] = ['user' => $res, 'profile' => $profile];
//                        }
//                    }

                    //sleep(0.5);
                }


                $this->uploadCustomers($users);

                $step = $step + $limit;

                // break;
            } else {
                $stopwhile = false;
                break;
            }
        }


    }

    public function getProfileUser($userId)
    {
        $profile_object = $this->modx->getObject('modUserProfile', array('internalKey' => $userId));
        $profile = [];
        if ($profile_object)
            $profile = $profile_object->toArray();


        return $profile;


    }

    /*public function sendRequestCustomer()
    {
        ///v1/contacts
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: ' . ($this->format == 'xml' ? 'text/xml' : 'application/json')));
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $post);
        $result = curl_exec($ch);
        curl_close($ch);
    }*/


    protected function send_request($url, $json_value)
    {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($json_value));
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Accept: application/json', 'Content-Type: application/json'));
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_USERPWD, $this->user . ':' . $this->password);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_SSLVERSION, 6);
        $output = curl_exec($ch);
        curl_close($ch);
        return $output;
    }
}