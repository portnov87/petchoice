<?php
//error_reporting(E_ALL | E_STRICT);
//ini_set('display_errors', 1);
class msOrder extends xPDOSimpleObject
{

    public $cityOdessa='db5c88d0-391c-11dd-90d9-001a92567626';
    public $cityKiev='8d5a980d-391c-11dd-90d9-001a92567626';


    public function updateProducts($status=0)
    {
        $delivery_cost = $this->get('delivery_cost');

        $discountloalnost = 0;
        if ($this->get('promocode') != '') {
            $object_promocode = $this->xpdo->getObject('PromocodeItem', array(
                'code' => $this->get('promocode'),
            ));
            if ($object_promocode->get('removeloylnost') == 1) {
                $discountloalnost = 1;
            }
        }
        $user = $this->getOne('User');
        if ($user) {
            $user = $user->getOne('Profile');
                if ($user) {
                    $user = $user->toArray();
                }
        }

        $cart_cost = $cost = $weight = 0;
        $delivery_cost=$this->get('delivery_cost');
        $products = $this->getMany('Products');
        /* @var msOrderProduct $product */
        foreach ($products as $product) {
            $count = $product->get('count');
            $price = $product->get('price');
            if ($discountloalnost == 1) {
                $disc=(isset($user['extended']['discount'])?$user['extended']['discount']:false);
                if ($disc && ($disc != ''))
                    $price = round($price - (($disc / 100) * $price), 2);
            }

            $cart_cost += $price * $count;
            $weight += $product->get('weight') * $count;


           /* $id_1c='';
            $_id_1cOption='';

            $idproduct = $product->get('product_id');//['product_id'];
            $_where = array(
                'contentid' => $idproduct
                ,'tmplvarid' => 1
            );
            $tvOptions = $this->modx->getObject('modTemplateVarResource', $_where);

            if ($tvOptions){
                $_options=$tvOptions->get('value');
                if ($_options){

                    $new_options=[];
                    $_id_1cGlobal=false;
                    $options=json_decode($_options,true);

                    foreach ($options as $_keyOpt=>$opt)
                    {
                        $_id_1c=false;
                        $optNew=$opt;

                        if (isset($opt['id_1c']))
                        {
                            if ($opt['id_1c']=='')
                                $_id_1c=false;
                            else
                                $_id_1c=$opt['id_1c'];
                        }

                        if (!$_id_1c) {
                            $id1c = $idproduct . $opt['MIGX_id'] . time();
                            $optNew['id_1c'] = $id1c;
                            $_id_1cGlobal = true;
                        }

                        $optionsOrderItem=$product->get('options');
                        if ($optionsOrderItem)
                        {
                            $size=json_decode($optionsOrderItem,true);
                            if ($size)
                            {
                                if ($size['size']==$opt['weight'].' '.$opt['weight_prefix'])
                                    $_id_1cOption=$_id_1c;
                            }

                        }
                        $new_options[$_keyOpt]=$optNew;
                    }

                    if ($_id_1cGlobal)
                    {
                        $options=$new_options;
                        if (count($new_options)>0) {
                            $new_optionsForSave = json_encode($new_options);

                            $tv = $this->modx->getObject('modTemplateVarResource', array('contentid' => $idproduct, 'tmplvarid' => 1));
                            if ($tv && ($new_optionsForSave != '')) {
                                $tv->set('value', $new_optionsForSave);
                                $tv->save();
                            }
                        }
                    }
                }
            }


            if ($_id_1cOption!='')
            {
                $product->set('id_1c',$_id_1cOption);
                $product->save();

            }
            */

        }


        if ($this->get('delivery') == 4) {// елси курьерская доставка
            $free = (float)$this->xpdo->getOption('free_delivery_Odessa');
            $city = 0;
            $userprofile = $this->getOne('UserProfile');//('user_id');
            if ($userprofile)
                $profile = $userprofile->toArray();


            if (isset($profile['extended']['city_id'])) {
                $city = $profile['extended']['city_id'];
            }
            $total_cost = $cart_cost;
            if ($city != 0) {
                if ($city == $this->cityOdessa) // если Одесса
                {
                   // $delivery_cost = 35;
                   // if ($cart_cost >= $free) $delivery_cost = 0;
                } else {
                    //елси пригород одессы
                   // $delivery_cost = 0;
                    $qcity = $this->xpdo->getObject('msNpCities', $city);
                    if ($qcity) {
                        $region = $qcity->get('AreaDescriptionRu');
                        if ($region == 'Одеська') {
                          /* $delivery_cost = 50;
                            if ($cart_cost >= $free) $delivery_cost = 0;*/
                        } else {
                            //if ($cart_cost >= $free) $delivery_cost = 0;
                        }
                    }
                }

            }
        }

        //cost_without_bonuses

        $cost=$cart_cost+$delivery_cost;

        $bonuses=$this->get('bonuses');
        $apply_bonuses=$this->get('apply_bonuses');
        //$cart_cost=$this->get('cart_cost');
        //$cost=$this->get('cost');

        if (($status==5)&&($bonuses>0)){//($apply_bonuses=='1')&&
            $cart_cost=$cart_cost-$bonuses;
            $cost=$cost - $bonuses;//$delivery_cost;
            $this->set('cost',$cost);
            $this->set('cart_cost',$cart_cost);

        }elseif (($apply_bonuses==1)&&($bonuses>0)){
            $cart_cost=$cart_cost-$bonuses;
            $cost=$cost - $bonuses;//$delivery_cost;
            $this->set('cost',$cost);
            $this->set('cart_cost',$cart_cost);
        }

        $this->set('cart_cost', round($cart_cost,0));
        $this->set('cost', round($cost,0));
        $this->set('weight', $weight);
        $this->set('delivery_cost', $delivery_cost);


        return $this->save();
    }

    public function liqpayHoldCompletion()
    {
        $private_key = $this->modx->getOption('privat_keyliqpay');//'vQP1hC3r1nWHN2nhAwfLUDFhorbm1epVtZgwo30h';
        $public_key = $this->modx->getOption('public_keyliqpay');//'i63764050824';

        $liqpay = new LiqPay($public_key, $private_key);
        $res = $liqpay->api("request", array(
            'action'        => 'hold_completion',
            'version'       => '3',
            'order_id'      => 'order_id_1'
        ));
    }

    public function setFinishPaymentStatus()
    {

        /*if ($payment = $order->getOne('Payment')) {
            if ($class = $payment->get('class')) {
                $this->loadCustomClasses('payment');
                if (class_exists($class)) {

                    $handler = new $class($order);
                    if (method_exists($handler, 'getPaymentLink')) {
                        $link = $handler->getPaymentLink($order);
                        $pls['payment_link'] = $this->modx->lexicon('ms2_payment_link', array('link' => $link));
                    }
                }
            }
        }*/


//        $paymentMethod=$this->get('payment');
//        if ($paymentMethod==4)
//        {
//
//        }
        $this->set('payment_status',4);
        $this->save();

//        echo 'payment_status finish';
//        die();
    }

    public function save($cacheFlag = false)
    {
        $user_id = $this->get('user_id');

        if ($this->isNew()) {
            $user = $this->xpdo->getObject('modUser', array('id' => $user_id));
            if ($user) {
                $user->set('mailmore60', 0);
                $user->set('mailmore45', 0);
                $user->set('last_order', $this->get('createdon'));
                $user->set('total_orders', $user->get('total_orders') + 1);
                $user->save();
            }
        }

        if ($this->get('status')==2)
        {
            $this->set('payment_status',4);
        }

        $version=$this->get('version')+1;
        $this->set('version',$version);


        //$this->get('status'),3);

        $saved = parent::save($cacheFlag);
        return $saved;
    }

    //mailmore60

}