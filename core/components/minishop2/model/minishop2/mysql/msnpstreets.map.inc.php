<?php
$xpdo_meta_map['msNpStreets'] = array(
	'package' => 'minishop2',
	'version' => '1.1',
	'table' => 'ms2_np_streets',
	'extends' => 'xPDOSimpleObject',
	'fields' => array(
		//'id' => 0,
		//'id' => NULL,
		'DescriptionRu' => NULL,
        'Description' => NULL,
        'CityRef'=>'',
        'StreetsType'=>'',
        'StreetsTypeRef'=>'',
        //'Ref'=>'',
		//'Area' => 0,
		//'AreaDescriptionRu'=>'',
        'created' => '0000-00-00 00:00:00',
        'updated' => '0000-00-00 00:00:00',
		//'warehouse'=>1,
	),
	'fieldMeta' => array(

		'id' => array(
			'dbtype' => 'varchar',
			'precision' => '255',
			'phptype' => 'string',
			'null' => false,
		),
		'DescriptionRu' => array(
            'dbtype' => 'varchar',
            'precision' => '255',
            'phptype' => 'string',
            'null' => false,
		),
        'Description' => array(
            'dbtype' => 'varchar',
            'precision' => '255',
            'phptype' => 'string',
            'null' => false,
        ),
        'CityRef' => array(
            'dbtype' => 'varchar',
            'precision' => '255',
            'phptype' => 'string',
            'null' => false,
        ),
        'StreetsType' => array(
            'dbtype' => 'varchar',
            'precision' => '255',
            'phptype' => 'string',
            'null' => false,
        ),
        'StreetsTypeRef' => array(
            'dbtype' => 'varchar',
            'precision' => '255',
            'phptype' => 'string',
            'null' => false,
        ),

        'created' =>
            array (
                'dbtype' => 'datetime',
                'phptype' => 'datetime',
                'null' => true,
                'default' => '0000-00-00 00:00:00',
            ),
        'updated' =>
            array (
                'dbtype' => 'datetime',
                'phptype' => 'datetime',
                'null' => true,
                'default' => '0000-00-00 00:00:00',
            ),

    ),
);
