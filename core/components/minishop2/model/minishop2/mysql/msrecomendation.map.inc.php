<?php
$xpdo_meta_map['msRecomendation'] = array(
	'package' => 'minishop2',
	'version' => '1.1',
	'table' => 'ms_recomendation',
	'extends' => 'xPDOSimpleObject',
	'fields' => array(
		'user_id' => 0,
		'product_id'=>0,
        'name' => '',
		'image_url'=>'',
		'price'=>'',
		'price_old'=>'',
		'product_url'=>'',
		'created' => NULL
	),
	'fieldMeta' => array(
		'user_id' => array(
			'dbtype' => 'int',
			'precision' => '10',
			'attributes' => 'unsigned',
			'phptype' => 'integer',
			'null' => true,
			'default' => 0,
		),
        'name' => array(
            'dbtype' => 'varchar',
            'precision' => '255',
            'phptype' => 'string',
            'null' => true,
        ),
        'image_url' => array(
            'dbtype' => 'varchar',
            'precision' => '255',
            'phptype' => 'string',
            'null' => true,
        ),
        'price' => array(
            'dbtype' => 'decimal',
            'precision' => '12,2',
            'phptype' => 'float',
            'null' => true,
        ),
        'price_old' => array(
            'dbtype' => 'decimal',
            'precision' => '12,2',
            'phptype' => 'float',
            'null' => true,
        ),
        'product_url' => array(
            'dbtype' => 'varchar',
            'precision' => '255',
            'phptype' => 'string',
            'null' => true,
        ),
        'product_id' => array(
			'dbtype' => 'int',
			'precision' => '10',
			'attributes' => 'unsigned',
			'phptype' => 'integer',
			'null' => true,
			'default' => 0,
		),
        'created' => array (
            'dbtype' => 'datetime',
            'phptype' => 'datetime',
            'null' => true,
            'default' => '0000-00-00 00:00:00',
        )
	),
);
