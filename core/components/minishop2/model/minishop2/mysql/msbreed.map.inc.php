<?php
$xpdo_meta_map['msBreed'] = array(
	'package' => 'minishop2',
	'version' => '1.1',
	'table' => 'ms2_breeds',
	'extends' => 'xPDOSimpleObject',
	'fields' => array(
		'name' => NULL,
		'pet_type' => 0,
	),
	'fieldMeta' => array(
		'name' => array(
			'dbtype' => 'varchar',
			'precision' => '100',
			'phptype' => 'string',
			'null' => false,
		),
		'pet_type' => array(
			'dbtype' => 'int',
			'precision' => '10',
			'breeds' => 'unsigned',
			'phptype' => 'integer',
			'null' => true,
			'default' => 0,
		),
	),
);
