<?php
$xpdo_meta_map['msIntime'] = array(
	'package' => 'minishop2',
	'version' => '1.1',
	'table' => 'ms2_intime',
	'extends' => 'xPDOSimpleObject',
	'fields' => array(
		'locality_id' => 0,
		'warehouse' => NULL,
		'phone' => NULL,
		'warehouse_no' => 0,
		'state'=>'',
		'grafik'=>'',
		'city'=>'',
		'correct'=>0,
	),
	'fieldMeta' => array(
		'locality_id' => array(
			'dbtype' => 'int',
			'precision' => '10',
			'breeds' => 'unsigned',
			'phptype' => 'integer',
			'null' => true,
			'default' => 0,
		),
		'warehouse' => array(
			'dbtype' => 'varchar',
			'precision' => '255',
			'phptype' => 'string',
			'null' => false,
		),
		'phone' => array(
			'dbtype' => 'varchar',
			'precision' => '255',
			'phptype' => 'string',
			'null' => false,
		),
		'warehouse_no' => array(
			'dbtype' => 'int',
			'precision' => '10',
			'breeds' => 'unsigned',
			'phptype' => 'integer',
			'null' => true,
			'default' => 0,
		),
		'state' => array(
			'dbtype' => 'varchar',
			'precision' => '255',
			'phptype' => 'string',
			'null' => false,
		),
		'grafik' => array(
			'dbtype' => 'varchar',
			'precision' => '255',
			'phptype' => 'string',
			'null' => false,
		),
		'city' => array(
			'dbtype' => 'varchar',
			'precision' => '255',
			'phptype' => 'string',
			'null' => false,
		),
		'correct' => array(
			'dbtype' => 'int',
			'precision' => '2',
			'phptype' => 'integer',
			'null' => true,
			'default' => 0,
		),
	),
);
