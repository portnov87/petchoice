<?php
$xpdo_meta_map['msCity'] = array(
	'package' => 'minishop2',
	'version' => '1.1',
	'table' => 'ms2_city',
	'extends' => 'xPDOSimpleObject',
	'fields' => array(
		'region_id' => 0,
		'name' => NULL,
		'phone_code' => NULL,
		'kyrier' => 0,
		'samov' => 0,
	),
	'fieldMeta' => array(
		'region_id' => array(
			'dbtype' => 'int',
			'precision' => '10',
			'breeds' => 'unsigned',
			'phptype' => 'integer',
			'null' => true,
			'default' => 0,
		),
		'name' => array(
			'dbtype' => 'varchar',
			'precision' => '255',
			'phptype' => 'string',
			'null' => false,
		),
		'phone_code' => array(
			'dbtype' => 'varchar',
			'precision' => '255',
			'phptype' => 'string',
			'null' => false,
		),
		'kyrier' => array(
			'dbtype' => 'int',
			  'precision' => '1',
			  'attributes' => 'unsigned',
			  //'phptype' => 'boolean',
			  'phptype' => 'integer',
			  'null' => true,
			  'default' => 0,
		),
		'samov' => array(
			'dbtype' => 'int',
			  'precision' => '1',
			  'attributes' => 'unsigned',
			  //'phptype' => 'boolean',
			  'phptype' => 'integer',
			  'null' => false,
			  'default' => 0,
		),
		
	),
);
