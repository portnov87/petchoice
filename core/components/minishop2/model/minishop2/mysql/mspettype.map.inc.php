<?php
$xpdo_meta_map['msPetType'] = array(
	'package' => 'minishop2',
	'version' => '1.1',
	'table' => 'ms2_pet_types',
	'extends' => 'xPDOSimpleObject',
	'fields' => array(
		'name' => NULL,
	),
	'fieldMeta' => array(
		'name' => array(
			'dbtype' => 'varchar',
			'precision' => '100',
			'phptype' => 'string',
			'null' => false,
		),
	),
);
