<?php
$xpdo_meta_map['msNpAddress'] = array(
	'package' => 'minishop2',
	'version' => '1.1',
	'table' => 'ms2_np_address',
	'extends' => 'xPDOSimpleObject',
	'fields' => array(
		//'id' => 0,
		'Ref' => '',
		'Description' => NULL,
	),
	'fieldMeta' => array(

		'Ref' => array(
			'dbtype' => 'varchar',
			'precision' => '255',
			'phptype' => 'string',
			'null' => false,
		),
		'Description' => array(
            'dbtype' => 'varchar',
            'precision' => '255',
            'phptype' => 'string',
            'null' => false,
		),


    ),
);
