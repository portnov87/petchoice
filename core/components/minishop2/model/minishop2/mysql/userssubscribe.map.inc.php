<?php
$xpdo_meta_map['UsersSubscribe']= array (
  'package' => 'minishop2',
  'version' => '1.1',
  'table' => 'users_subscribe',
  'extends' => 'xPDOSimpleObject',
  'fields' => 
  array (    
    'name' => '',
    'email' =>'',
    'subscribe' => 1,
    'user_id' => NULL,
   
  ),
  'fieldMeta' => 
  array (
    'name' => 
    array (
      'dbtype' => 'varchar',
      'precision' => '255',
      'phptype' => 'varchar',
      'null' => true,
    ),
    'email' => 
    array (
      'dbtype' => 'varchar',
      'precision' => '255',
      'phptype' => 'varchar',
      'null' => true,
    ),	
    'subscribe' => 
    array (
      'dbtype' => 'int',
      'precision' => '10',
      'attributes' => 'unsigned',
      'phptype' => 'integer',
      'null' => true,
      'default' => 1,
    ),
    'user_id' => 
    array (
       'dbtype' => 'int',
      'precision' => '10',
      'attributes' => 'unsigned',
      'phptype' => 'integer',
      'null' => true,
      'default' => NULL,
    ),
  ),
 
);

