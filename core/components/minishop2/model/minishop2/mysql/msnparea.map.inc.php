<?php
$xpdo_meta_map['msNpArea'] = array(
    'package' => 'minishop2',
    'version' => '1.1',
    'table' => 'ms2_np_area',
    'extends' => 'xPDOSimpleObject',
    'fields' => array(
        'DescriptionRu' => NULL,
        'Description' => NULL,
        'Ref' => '',
        'created' => '0000-00-00 00:00:00',
        'updated' => '0000-00-00 00:00:00'
    ),
    'fieldMeta' => array(


        'Description' => array(
            'dbtype' => 'varchar',
            'precision' => '255',
            'phptype' => 'string',
            'null' => false,
        ),
        'Ref' => array(
            'dbtype' => 'varchar',
            'precision' => '255',
            'phptype' => 'string',
            'null' => false,
        ),
        'DescriptionRu' => array(
            'dbtype' => 'varchar',
            'precision' => '255',
            'phptype' => 'string',
            'null' => false,
        ),
        'created' =>
            array(
                'dbtype' => 'datetime',
                'phptype' => 'datetime',
                'null' => true,
                'default' => '0000-00-00 00:00:00',
            ),
        'updated' =>
            array(
                'dbtype' => 'datetime',
                'phptype' => 'datetime',
                'null' => true,
                'default' => '0000-00-00 00:00:00',
            ),

    ),
);
