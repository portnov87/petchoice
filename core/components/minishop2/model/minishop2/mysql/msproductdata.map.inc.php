<?php
$xpdo_meta_map['msProductData'] = array(
    'package' => 'minishop2',
    'version' => '1.1',
    'table' => 'ms2_products',
    'extends' => 'xPDOSimpleObject',
    'fields' =>
        array(
            'article' => NULL,
            'price' => 0,
            'old_price' => 0,
            'weight' => 0,
            'image' => NULL,
            'thumb' => NULL,
            'vendor' => 0,
            'made_in' => '',
            'new' => 0,
            'data_new' => '0000-00-00 00:00:00',
            'toorder' => 0,
            'helloween' => 0,
            'markdown' => 0,

            'protein' => '',
            'clutch_bag' => '',
            'shir' => '',
            'zola' => '',
            'yglevodi' => '',


            'black_friday' => 0,
            'den_shivotnyh' => 0,
            'february' => 0,
            'newyear' => 0,

            'day_cat' => 0,
            'day_dog' => 0,
            'day_safeanimals' => 0,

            'count_shared' => 0,
            'count_likes' => 0,
            'count_points' => 0,
            'popular' => 0,
            'adiscount' => 0,
            'askidka' => 0,
            'date_action' => NULL,
            'favorite' => 0,
            'hits' => 0,
            'tags' => NULL,
            'color' => NULL,
            'size' => NULL,
            'source' => 1,
            'price_max' => 0,
            'price_kg' => 0
        ),
    'fieldMeta' =>
        array(
            'article' =>
                array(
                    'dbtype' => 'varchar',
                    'precision' => '50',
                    'phptype' => 'varchar',
                    'null' => true,
                ),
            'price' =>
                array(
                    'dbtype' => 'decimal',
                    'precision' => '12,2',
                    'phptype' => 'float',
                    'null' => true,
                    'default' => 0,
                ),
            'price_max' => array(
                'dbtype' => 'decimal',
                'precision' => '12,2',
                'phptype' => 'float',
                'null' => true,
                'default' => 0,
            ),
            'price_kg' => array(
                'dbtype' => 'decimal',
                'precision' => '12,2',
                'phptype' => 'float',
                'null' => true,
                'default' => 0,
            ),
            'old_price' =>
                array(
                    'dbtype' => 'decimal',
                    'precision' => '12,2',
                    'phptype' => 'float',
                    'null' => true,
                    'default' => 0,
                ),
            'weight' =>
                array(
                    'dbtype' => 'decimal',
                    'precision' => '13,3',
                    'phptype' => 'float',
                    'null' => true,
                    'default' => 0,
                ),
            'image' =>
                array(
                    'dbtype' => 'varchar',
                    'precision' => '255',
                    'phptype' => 'string',
                    'null' => true,
                ),
            'thumb' =>
                array(
                    'dbtype' => 'varchar',
                    'precision' => '255',
                    'phptype' => 'string',
                    'null' => true,
                ),
            'vendor' =>
                array(
                    'dbtype' => 'int',
                    'precision' => '10',
                    'attributes' => 'unsigned',
                    'phptype' => 'integer',
                    'null' => true,
                    'default' => 0,
                ),

            'protein' =>
                array(
                    'dbtype' => 'varchar',
                    'precision' => '255',
                    'phptype' => 'string',
                    'null' => true,
                    'default' => '',
                ),
            'clutch_bag' =>
                array(
                    'dbtype' => 'varchar',
                    'precision' => '255',
                    'phptype' => 'string',
                    'null' => true,
                    'default' => '',
                ),
            'shir' =>
                array(
                    'dbtype' => 'varchar',
                    'precision' => '255',
                    'phptype' => 'string',
                    'null' => true,
                    'default' => '',
                ),
            'zola' =>
                array(
                    'dbtype' => 'varchar',
                    'precision' => '255',
                    'phptype' => 'string',
                    'null' => true,
                    'default' => '',
                ),
            'yglevodi' =>
                array(
                    'dbtype' => 'varchar',
                    'precision' => '255',
                    'phptype' => 'string',
                    'null' => true,
                    'default' => '',
                ),

            'made_in' =>
                array(
                    'dbtype' => 'varchar',
                    'precision' => '100',
                    'phptype' => 'string',
                    'null' => true,
                    'default' => '',
                ),
            'new' =>
                array(
                    'dbtype' => 'tinyint',
                    'precision' => '1',
                    'attributes' => 'unsigned',
                    'phptype' => 'boolean',
                    'null' => true,
                    'default' => 0,
                ),
            'data_new' =>
                array(
                    'dbtype' => 'datetime',
                    'phptype' => 'datetime',
                    'null' => true,
                    'default' => '0000-00-00 00:00:00',
                ),
            'date_action' =>
                array(
                    'dbtype' => 'datetime',
                    'phptype' => 'datetime',
                    'null' => true,
                    'default' => '0000-00-00 00:00:00',
                ),
            'toorder' =>
                array(
                    'dbtype' => 'tinyint',
                    'precision' => '1',
                    'attributes' => 'unsigned',
                    'phptype' => 'boolean',
                    'null' => true,
                    'default' => 0,
                ),
            'count_shared' => array(
                'dbtype' => 'tinyint',
                'precision' => '100',
                'attributes' => 'unsigned',
                'phptype' => 'integer',
                'null' => true,
                'default' => 0,
            ),

            'count_likes' =>
                array(
                    'dbtype' => 'tinyint',
                    'precision' => '100',
                    'attributes' => 'unsigned',
                    'phptype' => 'integer',
                    'null' => true,
                    'default' => 0,
                ),

            'count_points' =>
                array(
                    'dbtype' => 'tinyint',
                    'precision' => '100',
                    'attributes' => 'unsigned',
                    'phptype' => 'integer',
                    'null' => true,
                    'default' => 0,
                ),

            'markdown' =>
                array(
                    'dbtype' => 'tinyint',
                    'precision' => '1',
                    'attributes' => 'unsigned',
                    'phptype' => 'boolean',
                    'null' => true,
                    'default' => 0,
                ),
            'helloween' =>
                array(
                    'dbtype' => 'tinyint',
                    'precision' => '1',
                    'attributes' => 'unsigned',
                    'phptype' => 'boolean',
                    'null' => true,
                    'default' => 0,
                ),

            'den_shivotnyh' =>
                array(
                    'dbtype' => 'tinyint',
                    'precision' => '1',
                    'attributes' => 'unsigned',
                    'phptype' => 'boolean',
                    'null' => true,
                    'default' => 0,
                ),
            'black_friday' =>
                array(
                    'dbtype' => 'tinyint',
                    'precision' => '1',
                    'attributes' => 'unsigned',
                    'phptype' => 'boolean',
                    'null' => true,
                    'default' => 0,
                ),
            'february' =>
                array(
                    'dbtype' => 'tinyint',
                    'precision' => '1',
                    'attributes' => 'unsigned',
                    'phptype' => 'boolean',
                    'null' => true,
                    'default' => 0,
                ),
            'newyear' =>
                array(
                    'dbtype' => 'tinyint',
                    'precision' => '1',
                    'attributes' => 'unsigned',
                    'phptype' => 'boolean',
                    'null' => true,
                    'default' => 0,
                ),

            'day_safeanimals' =>
                array(
                    'dbtype' => 'tinyint',
                    'precision' => '1',
                    'attributes' => 'unsigned',
                    'phptype' => 'boolean',
                    'null' => true,
                    'default' => 0,
                ),
            'day_dog' =>
                array(
                    'dbtype' => 'tinyint',
                    'precision' => '1',
                    'attributes' => 'unsigned',
                    'phptype' => 'boolean',
                    'null' => true,
                    'default' => 0,
                ),
            'day_cat' =>
                array(
                    'dbtype' => 'tinyint',
                    'precision' => '1',
                    'attributes' => 'unsigned',
                    'phptype' => 'boolean',
                    'null' => true,
                    'default' => 0,
                ),
            'hits' =>
                array(
                    'dbtype' => 'tinyint',
                    'precision' => '1',
                    'attributes' => 'unsigned',
                    'phptype' => 'integer',
                    'null' => true,
                    'default' => 0,
                ),
            'popular' =>
                array(
                    'dbtype' => 'tinyint',
                    'precision' => '1',
                    'attributes' => 'unsigned',
                    'phptype' => 'boolean',
                    'null' => true,
                    'default' => 0,
                ),
            'adiscount' =>
                array(
                    'dbtype' => 'tinyint',
                    'precision' => '1',
                    'attributes' => 'unsigned',
                    'phptype' => 'boolean',
                    'null' => true,
                    'default' => 0,
                ),
            'askidka' =>
                array(
                    'dbtype' => 'tinyint',
                    'precision' => '1',
                    'attributes' => 'unsigned',
                    'phptype' => 'boolean',
                    'null' => true,
                    'default' => 0,
                ),
            'favorite' =>
                array(
                    'dbtype' => 'tinyint',
                    'precision' => '1',
                    'attributes' => 'unsigned',
                    'phptype' => 'boolean',
                    'null' => true,
                    'default' => 0,
                ),
            'tags' =>
                array(
                    'dbtype' => 'text',
                    'phptype' => 'json',
                    'null' => true,
                ),
            'color' =>
                array(
                    'dbtype' => 'text',
                    'phptype' => 'json',
                    'null' => true,
                ),
            'size' =>
                array(
                    'dbtype' => 'decimal',//text',
                    'phptype' => 'json',
                    'null' => true,
                ),
            'source' =>
                array(
                    'dbtype' => 'int',
                    'precision' => '10',
                    'attributes' => 'unsigned',
                    'phptype' => 'integer',
                    'null' => true,
                    'default' => 1,
                ),
        ),
    'indexes' =>
        array(
            'article' =>
                array(
                    'alias' => 'article',
                    'primary' => false,
                    'unique' => false,
                    'type' => 'BTREE',
                    'columns' =>
                        array(
                            'article' =>
                                array(
                                    'length' => '',
                                    'collation' => 'A',
                                    'null' => false,
                                ),
                        ),
                ),
            'price' =>
                array(
                    'alias' => 'price',
                    'primary' => false,
                    'unique' => false,
                    'type' => 'BTREE',
                    'columns' =>
                        array(
                            'price' =>
                                array(
                                    'length' => '',
                                    'collation' => 'A',
                                    'null' => false,
                                ),
                        ),
                ),
            'old_price' =>
                array(
                    'alias' => 'old_price',
                    'primary' => false,
                    'unique' => false,
                    'type' => 'BTREE',
                    'columns' =>
                        array(
                            'old_price' =>
                                array(
                                    'length' => '',
                                    'collation' => 'A',
                                    'null' => false,
                                ),
                        ),
                ),
            'vendor' =>
                array(
                    'alias' => 'vendor',
                    'primary' => false,
                    'unique' => false,
                    'type' => 'BTREE',
                    'columns' =>
                        array(
                            'vendor' =>
                                array(
                                    'length' => '',
                                    'collation' => 'A',
                                    'null' => false,
                                ),
                        ),
                ),
            'new' =>
                array(
                    'alias' => 'new',
                    'primary' => false,
                    'unique' => false,
                    'type' => 'BTREE',
                    'columns' =>
                        array(
                            'new' =>
                                array(
                                    'length' => '',
                                    'collation' => 'A',
                                    'null' => false,
                                ),
                        ),
                ),
            'hits' =>
                array(
                    'alias' => 'hits',
                    'primary' => false,
                    'unique' => false,
                    'type' => 'BTREE',
                    'columns' =>
                        array(
                            'hits' =>
                                array(
                                    'length' => '',
                                    'collation' => 'A',
                                    'null' => false,
                                ),
                        ),
                ),

            'favorite' =>
                array(
                    'alias' => 'favorite',
                    'primary' => false,
                    'unique' => false,
                    'type' => 'BTREE',
                    'columns' =>
                        array(
                            'favorite' =>
                                array(
                                    'length' => '',
                                    'collation' => 'A',
                                    'null' => false,
                                ),
                        ),
                ),
            'popular' =>
                array(
                    'alias' => 'popular',
                    'primary' => false,
                    'unique' => false,
                    'type' => 'BTREE',
                    'columns' =>
                        array(
                            'popular' =>
                                array(
                                    'length' => '',
                                    'collation' => 'A',
                                    'null' => false,
                                ),
                        ),
                ),
            'adiscount' =>
                array(
                    'alias' => 'adiscount',
                    'primary' => false,
                    'unique' => false,
                    'type' => 'BTREE',
                    'columns' =>
                        array(
                            'adiscount' =>
                                array(
                                    'length' => '',
                                    'collation' => 'A',
                                    'null' => false,
                                ),
                        ),
                ),
            'made_in' =>
                array(
                    'alias' => 'made_in',
                    'primary' => false,
                    'unique' => false,
                    'type' => 'BTREE',
                    'columns' =>
                        array(
                            'made_in' =>
                                array(
                                    'length' => '',
                                    'collation' => 'A',
                                    'null' => false,
                                ),
                        ),
                ),
        ),
    'composites' =>
        array(
            'Options' =>
                array(
                    'class' => 'msProductOption',
                    'local' => 'id',
                    'foreign' => 'product_id',
                    'cardinality' => 'many',
                    'owner' => 'local',
                ),
            'Files' =>
                array(
                    'class' => 'msProductFile',
                    'local' => 'id',
                    'foreign' => 'product_id',
                    'cardinality' => 'many',
                    'owner' => 'local',
                ),
            'Categories' =>
                array(
                    'class' => 'msCategoryMember',
                    'local' => 'id',
                    'foreign' => 'product_id',
                    'cardinality' => 'many',
                    'owner' => 'local',
                ),
        ),
    'aggregates' =>
        array(
            'Product' =>
                array(
                    'class' => 'msProduct',
                    'local' => 'id',
                    'foreign' => 'id',
                    'cardinality' => 'one',
                    'owner' => 'foreign',
                ),
            'Vendor' =>
                array(
                    'class' => 'msVendor',
                    'local' => 'vendor',
                    'foreign' => 'id',
                    'cardinality' => 'one',
                    'owner' => 'foreign',
                ),
        ),
);


if (!class_exists('ms2Plugins') || !is_object($this->ms2Plugins)) {
    require_once(dirname(dirname(__FILE__)) . '/plugins.class.php');
    $this->ms2Plugins = new ms2Plugins($this, array());
}
$xpdo_meta_map['msProductData'] = $this->ms2Plugins->loadMap('msProductData', $xpdo_meta_map['msProductData']);