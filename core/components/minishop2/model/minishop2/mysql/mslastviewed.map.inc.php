<?php
$xpdo_meta_map['msLastviewed'] = array(
	'package' => 'minishop2',
	'version' => '1.1',
	'table' => 'ms_lastviewed',
	'extends' => 'xPDOSimpleObject',
	'fields' => array(
		'user_id' => 0,
		'cookie_id' => '',
		'product_id'=>0,
		'date' => NULL

	),
	'fieldMeta' => array(
		'user_id' => array(
			'dbtype' => 'int',
			'precision' => '10',
			'attributes' => 'unsigned',
			'phptype' => 'integer',
			'null' => true,
			'default' => 0,
		),
        'cookie_id' => array(
            'dbtype' => 'varchar',
            'precision' => '255',
            'phptype' => 'string',
            'null' => true,
        ),

		'product_id' => array(
			'dbtype' => 'int',
			'precision' => '10',
			'attributes' => 'unsigned',
			'phptype' => 'integer',
			'null' => true,
			'default' => 0,
		),
        'date' => array (
            'dbtype' => 'datetime',
            'phptype' => 'datetime',
            'null' => true,
            'default' => '0000-00-00 00:00:00',
        )
	),
);
