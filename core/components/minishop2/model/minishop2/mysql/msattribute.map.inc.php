<?php
$xpdo_meta_map['msAttribute'] = array(
    'package' => 'minishop2',
    'version' => '1.1',
    'table' => 'ms2_attributes',
    'extends' => 'xPDOSimpleObject',
    'fields' =>
        array(
            'name' => NULL,
            'name_ua' => NULL,
            'attribute_group' => 0,
            'key' => NULL,
            'old_id' => 0,
            'sef' => 0,
            'meta_title' => '',
            'h1' => '',
            'meta_description' => '',
            'meta_title_ua' => '',
            'h1_ua' => '',
            'meta_description_ua' => '',

            'sort' => 0,
            'sort_related' => 0,
        ),
    'fieldMeta' =>
        array(
            'name' =>
                array(
                    'dbtype' => 'varchar',
                    'precision' => '100',
                    'phptype' => 'string',
                    'null' => false,
                ),
            'name_ua' =>
                array(
                    'dbtype' => 'varchar',
                    'precision' => '100',
                    'phptype' => 'string',
                    'null' => false,
                ),
            'key' =>
                array(
                    'dbtype' => 'varchar',
                    'precision' => '255',
                    'phptype' => 'string',
                    'null' => true,
                ),

            'meta_title' =>
                array(
                    'dbtype' => 'varchar',
                    'precision' => '255',
                    'phptype' => 'string',
                    'null' => true,
                ),
            'meta_description' =>
                array(
                    'dbtype' => 'varchar',
                    'precision' => '255',
                    'phptype' => 'string',
                    'null' => true,
                ),
            'h1' =>
                array(
                    'dbtype' => 'varchar',
                    'precision' => '255',
                    'phptype' => 'string',
                    'null' => true,
                ),

            'meta_title_ua' =>
                array(
                    'dbtype' => 'varchar',
                    'precision' => '255',
                    'phptype' => 'string',
                    'null' => true,
                ),
            'meta_description_ua' =>
                array(
                    'dbtype' => 'varchar',
                    'precision' => '255',
                    'phptype' => 'string',
                    'null' => true,
                ),
            'h1_ua' =>
                array(
                    'dbtype' => 'varchar',
                    'precision' => '255',
                    'phptype' => 'string',
                    'null' => true,
                ),
            'sef' =>
                array(
                    'dbtype' => 'int',
                    'precision' => '10',
                    'attributes' => 'unsigned',
                    'phptype' => 'integer',
                    'null' => true,
                    'default' => 0,
                ),

            'old_id' =>
                array(
                    'dbtype' => 'int',
                    'precision' => '10',
                    'phptype' => 'integer',
                    'null' => false,
                    'default' => 0,
                    'index' => 'index',
                ),
            'attribute_group' =>
                array(
                    'dbtype' => 'int',
                    'precision' => '10',
                    'attributes' => 'unsigned',
                    'phptype' => 'integer',
                    'null' => true,
                    'default' => 0,
                ),
            'sort' =>
                array(
                    'dbtype' => 'int',
                    'precision' => '10',
                    'attributes' => 'unsigned',
                    'phptype' => 'integer',
                    'null' => true,
                    'default' => 0,
                ),
            'sort_related' =>
                array(
                    'dbtype' => 'int',
                    'precision' => '10',
                    'attributes' => 'unsigned',
                    'phptype' => 'integer',
                    'null' => true,
                    'default' => 0,
                ),

        ),
    'aggregates' =>
        array(
            'Products' =>
                array(
                    'class' => 'msProduct',
                    'local' => 'id',
                    'foreign' => 'attribute',
                    'cardinality' => 'many',
                    'owner' => 'foreign',
                ),
        ),
);
