<?php
$xpdo_meta_map['msSuppliers'] = array(
	'package' => 'minishop2',
	'version' => '1.1',
	'table' => 'ms2_suppliers',
	'extends' => 'xPDOSimpleObject',
	'fields' => array(
		'id' => 0,
		'name' => '',
		'code' => '',
		'status' => 0
	),
	'fieldMeta' => array(
		'id' => array(
			'dbtype' => 'int',
			'precision' => '10',
			'attributes' => 'unsigned',
			'phptype' => 'integer',
			'null' => true,
			'default' => 0,
		),
		'status' => array(
			'dbtype' => 'int',
			'precision' => '10',
			'attributes' => 'unsigned',
			'phptype' => 'integer',
			'null' => true,
			'default' => 1,
		),
		'name' => array(
			'dbtype' => 'varchar',
			'precision' => '255',
			'phptype' => 'string',
			'null' => false,
		),
        'code' => array(
            'dbtype' => 'varchar',
            'precision' => '255',
            'phptype' => 'string',
            'null' => false,
        )
	),
);
