<?php
$xpdo_meta_map['msPickup'] = array(
	'package' => 'minishop2',
	'version' => '1.1',
	'table' => 'ms2_pickup',
	'extends' => 'xPDOSimpleObject',
	'fields' => array(
		//'id' => 0,
		'status' => 1,
		'name' => NULL,
		'address' => NULL,

	),
	'fieldMeta' => array(
		'status' => array(
			'dbtype' => 'int',
			'precision' => '10',
			'attributes' => 'unsigned',
			'phptype' => 'integer',
			'null' => true,
			'default' => 0,
		),

        'name' => array(
			'dbtype' => 'varchar',
			'precision' => '255',
			'phptype' => 'string',
			'null' => false,
		),

        'address' => array(
            'dbtype' => 'varchar',
            'precision' => '255',
            'phptype' => 'string',
            'null' => false,
        ),

	),
);
