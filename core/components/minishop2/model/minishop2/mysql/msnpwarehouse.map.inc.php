<?php
$xpdo_meta_map['msNpWarehouse'] = array(
	'package' => 'minishop2',
	'version' => '1.1',
	'table' => 'ms2_np_warehouse',
	'extends' => 'xPDOSimpleObject',
	'fields' => array(
        'id' => NULL,
		'Ref' => NULL,
		'DescriptionRu' => NULL,
        'Description' => NULL,
		'CityRef' => 0,
        'Number' => NULL,
        'updated'=>NULL,
        'created'=>NULL,
        'TypeOfWarehouse' => NULL,
        'TotalMaxWeightAllowed' => NULL,
        'Longitude' => NULL,
        'Latitude' => NULL,
        'Schedule' => NULL,
        'WarehouseStatus' => NULL,
        'CategoryOfWarehouse' => NULL,
        'PostalCodeUA' => NULL
    ),
	'fieldMeta' => array(
        'id' => array(
            'dbtype' => 'varchar',
            'precision' => '255',
            'phptype' => 'string',
            'null' => false,
        ),
		'Ref' => array(
			'dbtype' => 'varchar',
			'precision' => '255',
			'phptype' => 'string',
			'null' => false,
		),
		'DescriptionRu' => array(
            'dbtype' => 'varchar',
            'precision' => '255',
            'phptype' => 'string',
            'null' => false,
		),
        'Description' => array(
            'dbtype' => 'varchar',
            'precision' => '255',
            'phptype' => 'string',
            'null' => false,
        ),
		'CityRef' => array(
			'dbtype' => 'varchar',
			'precision' => '255',
			'phptype' => 'string',
			'null' => false,
		),
		'Number' => array(
			'dbtype' => 'varchar',
			'precision' => '255',
			'phptype' => 'string',
			'null' => false,
		),

        'TypeOfWarehouse' => array(
            'dbtype' => 'varchar',
            'precision' => '255',
            'phptype' => 'string',
            'null' => false,
        ),
        'TotalMaxWeightAllowed' => array(
            'dbtype' => 'tinyint',
            'precision' => '1',
            'attributes' => 'unsigned',
            'phptype' => 'integer',
            'null' => true,
            'default' => 0,
        ),
        'Longitude' => array(
            'dbtype' => 'varchar',
            'precision' => '255',
            'phptype' => 'string',
            'null' => false,
        ),
        'Latitude' => array(
            'dbtype' => 'varchar',
            'precision' => '255',
            'phptype' => 'string',
            'null' => false,
        ),
        'Schedule' => array(
            'dbtype' => 'text',
            'phptype' => 'json',
            'null' => true,
        ),
        'WarehouseStatus' => array(
            'dbtype' => 'varchar',
            'precision' => '255',
            'phptype' => 'string',
            'null' => false,
        ),
        'CategoryOfWarehouse' => array(
            'dbtype' => 'varchar',
            'precision' => '255',
            'phptype' => 'string',
            'null' => false,
        ),
        'PostalCodeUA' => array(
            'dbtype' => 'varchar',
            'precision' => '255',
            'phptype' => 'string',
            'null' => false,
        ),

        'created' =>
            array(
                'dbtype' => 'datetime',
                'phptype' => 'datetime',
                'null' => true,
                'default' => '0000-00-00 00:00:00',
            ),
        'updated' =>
            array(
                'dbtype' => 'datetime',
                'phptype' => 'datetime',
                'null' => true,
                'default' => '0000-00-00 00:00:00',
            ),

    ),
);
