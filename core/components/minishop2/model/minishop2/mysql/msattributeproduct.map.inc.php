<?php
$xpdo_meta_map['msAttributeProduct'] = array(
    'package' => 'minishop2',
    'version' => '1.1',
    'table' => 'ms2_attributes_product',
    'extends' => 'xPDOSimpleObject',
    'fields' =>
        array(
            'key_attribute' => '',
            'attribute_id' => NULL,
            'product_id' => NULL,
            //'name' => NULL,
            //'attribute_group' => 0,
            'attr_group' => 0,
            'old_value' => NULL,
            'group_name' => NULL
        ),
    'fieldMeta' =>
        array(
            'attribute_id' =>
                array(
                    'dbtype' => 'int',
                    'precision' => '10',
                    'attributes' => 'unsigned',
                    'phptype' => 'integer',
                    'null' => true,
                    'default' => 0,
                ),
            'key_attribute' =>
                array(
                    'dbtype' => 'varchar',
                    'precision' => '100',
                    'phptype' => 'string',
                    'null' => false,
                ),
            'product_id' =>
                array(
                    'dbtype' => 'int',
                    'precision' => '10',
                    'attributes' => 'unsigned',
                    'phptype' => 'integer',
                    'null' => true,
                    'default' => 0,
                ),

            'group_name' =>
                array(
                    'dbtype' => 'varchar',
                    'precision' => '100',
                    'phptype' => 'string',
                    'null' => true,
                ),
            'old_value' =>
                array(
                    'dbtype' => 'varchar',
                    'precision' => '100',
                    'phptype' => 'string',
                    'null' => true,
                ),

            'attr_group' =>
                array(
                    'dbtype' => 'int',
                    'precision' => '10',
                    'attributes' => 'unsigned',
                    'phptype' => 'integer',
                    'null' => true,
                    'default' => 0,
                ),

        ),

    'indexes' =>
        array(
            'product_id' =>
                array(
                    'alias' => 'product_id',
                    'primary' => false,
                    'unique' => false,
                    'type' => 'BTREE',
                    'columns' =>
                        array(
                            'product_id' =>
                                array(
                                    'length' => '',
                                    'collation' => 'A',
                                    'null' => false,
                                ),
                        ),
                ),
            'attribute_id' =>
                array(
                    'alias' => 'attribute_id',
                    'primary' => false,
                    'unique' => false,
                    'type' => 'BTREE',
                    'columns' =>
                        array(
                            'attribute_id' =>
                                array(
                                    'length' => '',
                                    'collation' => 'A',
                                    'null' => false,
                                ),
                        ),
                ),

        ),


);
