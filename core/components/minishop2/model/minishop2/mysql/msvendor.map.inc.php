<?php
$xpdo_meta_map['msVendor'] = array(
    'package' => 'minishop2',
    'version' => '1.1',
    'table' => 'ms2_vendors',
    'extends' => 'xPDOSimpleObject',
    'fields' =>
        array(
            'name' => NULL,
            'name_ua' => NULL,
            'resource' => 0,
            'country' => NULL,
            'country_ua' => NULL,
            'logo' => NULL,
            'address' => NULL,
            'phone' => NULL,
            'url' => NULL,
            'fax' => NULL,
            'email' => NULL,
            'description' => NULL,
            'description_ua' => NULL,
            'properties' => NULL,
            'name_ru' => NULL,
            'sort' => 0,
            'showinmain' => 0,
            'top' => 0,
            'new' => 0,
            'po_nilishiy' => 0,
            'post' => '',
            'discount_prom'=>'',
            'show_icon_delivery'=>0,
            'show_icon_cachback'=>0,
            'from_amount_cachback'=>'999',
            'dop_text'=>'',
            'dop_text_ua'=>'',


        ),
    'fieldMeta' =>
        array(
            'name' =>
                array(
                    'dbtype' => 'varchar',
                    'precision' => '100',
                    'phptype' => 'string',
                    'null' => false,
                ),
            'name_ua' =>
                array(
                    'dbtype' => 'varchar',
                    'precision' => '100',
                    'phptype' => 'string',
                    'null' => false,
                ),
            'url' =>
                array(
                    'dbtype' => 'varchar',
                    'precision' => '100',
                    'phptype' => 'string',
                    'null' => false,
                ),
            'resource' =>
                array(
                    'dbtype' => 'int',
                    'precision' => '10',
                    'attributes' => 'unsigned',
                    'phptype' => 'integer',
                    'null' => true,
                    'default' => 0,
                ),
            'country' =>
                array(
                    'dbtype' => 'varchar',
                    'precision' => '100',
                    'phptype' => 'string',
                    'null' => true,
                ),
            'country_ua' =>
                array(
                    'dbtype' => 'varchar',
                    'precision' => '100',
                    'phptype' => 'string',
                    'null' => true,
                ),
            'logo' =>
                array(
                    'dbtype' => 'varchar',
                    'precision' => '255',
                    'phptype' => 'string',
                    'null' => true,
                ),
            'address' =>
                array(
                    'dbtype' => 'text',
                    'phptype' => 'string',
                    'null' => true,
                ),
            'phone' =>
                array(
                    'dbtype' => 'varchar',
                    'precision' => '20',
                    'phptype' => 'string',
                    'null' => true,
                ),
            'fax' =>
                array(
                    'dbtype' => 'varchar',
                    'precision' => '20',
                    'phptype' => 'string',
                    'null' => true,
                ),
            'email' =>
                array(
                    'dbtype' => 'varchar',
                    'precision' => '255',
                    'phptype' => 'string',
                    'null' => true,
                ),
            'post' =>
                array(
                    'dbtype' => 'varchar',
                    'precision' => '255',
                    'phptype' => 'string',
                    'null' => true,
                ),
            'description' =>
                array(
                    'dbtype' => 'text',
                    'phptype' => 'string',
                    'null' => true,
                ),
            'description_ua' =>
                array(
                    'dbtype' => 'text',
                    'phptype' => 'string',
                    'null' => true,
                ),
            'properties' =>
                array(
                    'dbtype' => 'text',
                    'phptype' => 'json',
                    'null' => true,
                ),

            'top' => array(
                'dbtype' => 'int',
                'precision' => '1',
                'attributes' => 'unsigned',
                'phptype' => 'boolean',//
                'null' => false,
                'default' => 0,
            ),
            'new' => array(
                'dbtype' => 'int',
                'precision' => '1',
                'attributes' => 'unsigned',
                'phptype' => 'boolean',//
                'null' => false,
                'default' => 0,
            ),

            'po_nilishiy' => array(
                'dbtype' => 'int',
                'precision' => '1',
                'attributes' => 'unsigned',
                'phptype' => 'boolean',//
                'null' => true,
                'default' => 0,
            ),
            'showinmain' => array(
                'dbtype' => 'int',
                'precision' => '1',
                'attributes' => 'unsigned',
                'phptype' => 'boolean',//
                'null' => true,
                'default' => 0,
            ),
            'name_ru' =>
                array(
                    'dbtype' => 'varchar',
                    'precision' => '200',
                    'phptype' => 'string',
                    'null' => true,
                ),
            'sort' => array(
                'dbtype' => 'int',
                'precision' => '10',
                'attributes' => 'unsigned',
                'phptype' => 'integer',
                'null' => true,
                'default' => 0,
            ),

            'show_icon_delivery' => array(
                'dbtype' => 'int',
                'precision' => '1',
                'attributes' => 'unsigned',
                'phptype' => 'boolean',//
                'null' => true,
                'default' => 1,
            ),
            'show_icon_cachback' => array(
                'dbtype' => 'int',
                'precision' => '1',
                'attributes' => 'unsigned',
                'phptype' => 'boolean',//
                'null' => true,
                'default' => 1,
            ),
            'from_amount_cachback' =>
                array(
                    'dbtype' => 'text',
                    'phptype' => 'string',
                    'null' => true,
                ),

            'dop_text' =>
                array(
                    'dbtype' => 'text',
                    'phptype' => 'string',
                    'null' => true,
                ),
            'dop_text_ua' =>
                array(
                    'dbtype' => 'text',
                    'phptype' => 'string',
                    'null' => true,
                ),
            'discount_prom' => array(
                'dbtype' => 'int',
                'precision' => '10',
                'attributes' => 'unsigned',
                'phptype' => 'integer',
                'null' => true,
                'default' => 0,
            )
        ),
    'aggregates' =>
        array(
            'Products' =>
                array(
                    'class' => 'msProduct',
                    'local' => 'id',
                    'foreign' => 'vendor',
                    'cardinality' => 'many',
                    'owner' => 'foreign',
                ),
            'Resource' =>
                array(
                    'class' => 'modResource',
                    'local' => 'resource',
                    'foreign' => 'id',
                    'cardinality' => 'one',
                    'owner' => 'local',
                ),
        ),
);
