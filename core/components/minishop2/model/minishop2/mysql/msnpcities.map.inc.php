<?php
$xpdo_meta_map['msNpCities'] = array(
    'package' => 'minishop2',
    'version' => '1.1',
    'table' => 'ms2_np_cities',
    'extends' => 'xPDOSimpleObject',
    'fields' => array(
        //'id' => 0,
        //'id' => '',
        'DescriptionRu' => NULL,
        'Description' => NULL,
        'Ref' => '',
        'Area' => 0,
        'AreaDescriptionRu' => '',
        'created' => '0000-00-00 00:00:00',
        'updated' => '0000-00-00 00:00:00',
        'SettlementTypeDescriptionRu' => '',
        'CityID' => '',
        'warehouse' => 1,
        'kyrier' => 0,
        'samov' => 0,
        'poshtomat' => 1
    ),
    'fieldMeta' => array(

        'samov' => array(
            'dbtype' => 'int',
            'precision' => '10',
            'breeds' => 'unsigned',
            'phptype' => 'integer',
            'null' => true,
            'default' => 0,
        ),
        'poshtomat' => array(
            'dbtype' => 'int',
            'precision' => '10',
            'breeds' => 'unsigned',
            'phptype' => 'integer',
            'null' => true,
            'default' => 1,
        ),
        'kyrier' => array(
            'dbtype' => 'int',
            'precision' => '10',
            'breeds' => 'unsigned',
            'phptype' => 'integer',
            'null' => true,
            'default' => 0,
        ),

        'warehouse' => array(
            'dbtype' => 'int',
            'precision' => '10',
            'breeds' => 'unsigned',
            'phptype' => 'integer',
            'null' => true,
            'default' => 1,
        ),
        'CityID' => array(
            'dbtype' => 'int',
            'precision' => '10',
            'breeds' => 'unsigned',
            'phptype' => 'integer',
            'null' => true,
            'default' => 1,
        ),
        'SettlementTypeDescriptionRu' => array(
            'dbtype' => 'varchar',
            'precision' => '255',
            'phptype' => 'string',
            'null' => false,
        ),
        'id' => array(
            'dbtype' => 'varchar',
            'precision' => '255',
            'phptype' => 'string',
            'null' => false,
        ),
        'Ref' => array(
            'dbtype' => 'varchar',
            'precision' => '255',
            'phptype' => 'string',
            'null' => false,
        ),
        'DescriptionRu' => array(
            'dbtype' => 'varchar',
            'precision' => '255',
            'phptype' => 'string',
            'null' => false,
        ),
        'Description' => array(
            'dbtype' => 'varchar',
            'precision' => '255',
            'phptype' => 'string',
            'null' => false,
        ),
        'Area' => array(
            'dbtype' => 'varchar',
            'precision' => '255',
            'phptype' => 'string',
            'null' => false,
        ),
        'AreaDescriptionRu' => array(
            'dbtype' => 'varchar',
            'precision' => '255',
            'phptype' => 'string',
            'null' => false,
        ),

        'created' =>
            array(
                'dbtype' => 'datetime',
                'phptype' => 'datetime',
                'null' => true,
                'default' => '0000-00-00 00:00:00',
            ),
        'updated' =>
            array(
                'dbtype' => 'datetime',
                'phptype' => 'datetime',
                'null' => true,
                'default' => '0000-00-00 00:00:00',
            ),

    ),
);
