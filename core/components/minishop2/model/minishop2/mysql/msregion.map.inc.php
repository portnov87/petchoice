<?php
$xpdo_meta_map['msRegion'] = array(
	'package' => 'minishop2',
	'version' => '1.1',
	'table' => 'ms2_region',
	'extends' => 'xPDOSimpleObject',
	'fields' => array(
		'name' => NULL,
	),
	'fieldMeta' => array(
		'name' => array(
			'dbtype' => 'varchar',
			'precision' => '255',
			'phptype' => 'string',
			'null' => false,
		),
	),
);
