<?php
$xpdo_meta_map['msAttributeGroup'] = array(
    'package' => 'minishop2',
    'version' => '1.1',
    'table' => 'ms2_attribute_groups',
    'extends' => 'xPDOSimpleObject',
    'fields' =>
        array(
            'name' => NULL,
            'name_ua' => NULL,
            'old_id' => NULL,
            'key' => NULL,
            'sort' => 0,
            'sort_related' => 0,
        ),
    'fieldMeta' =>
        array(
            'name' =>
                array(
                    'dbtype' => 'varchar',
                    'precision' => '100',
                    'phptype' => 'string',
                    'null' => false,
                ),
            'name_ua' =>
                array(
                    'dbtype' => 'varchar',
                    'precision' => '256',
                    'phptype' => 'string',
                    'null' => false,
                ),
            'key' =>
                array(
                    'dbtype' => 'varchar',
                    'precision' => '255',
                    'phptype' => 'string',
                    'null' => false,
                ),
            'old_id' =>
                array(
                    'dbtype' => 'int',
                    'precision' => '10',
                    'phptype' => 'integer',
                    'null' => false,
                    'default' => 0,
                    'index' => 'index',
                ),
            'sort' =>
                array(
                    'dbtype' => 'int',
                    'precision' => '10',
                    'phptype' => 'integer',
                    'null' => false,
                    'default' => 0,
                ),
            'sort_related' =>
                array(
                    'dbtype' => 'int',
                    'precision' => '10',
                    'phptype' => 'integer',
                    'null' => false,
                    'default' => 0,
                ),


        ),
    'aggregates' =>
        array(
            'Products' =>
                array(
                    'class' => 'msProduct',
                    'local' => 'id',
                    'foreign' => 'attribute',
                    'cardinality' => 'many',
                    'owner' => 'foreign',
                ),
        ),
);
