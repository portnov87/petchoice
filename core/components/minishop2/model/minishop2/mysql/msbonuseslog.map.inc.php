<?php
$xpdo_meta_map['msBonusesLog'] = array(
    'package' => 'minishop2',
    'version' => '1.1',
    'table' => 'ms_user_bonuses',
    'extends' => 'xPDOSimpleObject',
    'fields' =>
        array(
            'user_id' => 0,
            'order_id' => 0,
            'manager_id' => 0,
            'ticket_id'=>0,
            'created' => '0000-00-00 00:00:00',
            'amount' => '',
            'reason'=>'',
        ),
    'fieldMeta' =>
        array(
            'user_id' =>
                array(
                    'dbtype' => 'int',
                    'precision' => '10',
                    'attributes' => 'unsigned',
                    'phptype' => 'integer',
                    'null' => false,
                    'default' => 0,
                ),
            'manager_id' =>
                array(
                    'dbtype' => 'int',
                    'precision' => '10',
                    'attributes' => 'unsigned',
                    'phptype' => 'integer',
                    'null' => false,
                    'default' => 0,
                ),
            'order_id' =>
                array(
                    'dbtype' => 'int',
                    'precision' => '10',
                    'attributes' => 'unsigned',
                    'phptype' => 'integer',
                    'null' => false,
                    'default' => 0,
                ),
            'ticket_id' =>
                array(
                    'dbtype' => 'int',
                    'precision' => '10',
                    'attributes' => 'unsigned',
                    'phptype' => 'integer',
                    'null' => false,
                    'default' => 0,
                ),

            'created' =>
                array(
                    'dbtype' => 'datetime',
                    'phptype' => 'datetime',
                    'null' => true,
                    'default' => '0000-00-00 00:00:00',
                ),
            'amount' =>
                array(
                    'dbtype' => 'varchar',
                    'precision' => '100',
                    'phptype' => 'string',
                    'null' => false,
                    'default' => '',
                ),
            'reason' =>
                array(
                    'dbtype' => 'varchar',
                    'precision' => '255',
                    'phptype' => 'string',
                    'null' => false,
                    'default' => '',
                ),

        ),
    'aggregates' =>
        array(
            'User' =>
                array(
                    'class' => 'modUser',
                    'local' => 'user_id',
                    'foreign' => 'id',
                    'owner' => 'foreign',
                    'cardinality' => 'one',
                ),
            'UserProfile' =>
                array(
                    'class' => 'modUserProfile',
                    'local' => 'user_id',
                    'foreign' => 'internalKey',
                    'owner' => 'foreign',
                    'cardinality' => 'one',
                ),
            'CustomerProfile' =>
                array(
                    'class' => 'msCustomerProfile',
                    'local' => 'user_id',
                    'foreign' => 'internalKey',
                    'owner' => 'foreign',
                    'cardinality' => 'one',
                ),
            'Order' =>
                array(
                    'class' => 'msOrder',
                    'local' => 'order_id',
                    'foreign' => 'id',
                    'owner' => 'foreign',
                    'cardinality' => 'one',
                ),
        ),
);
