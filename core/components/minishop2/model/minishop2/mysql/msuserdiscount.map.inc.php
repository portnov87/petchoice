<?php
$xpdo_meta_map['msUserDiscount'] = array(
	'package' => 'minishop2',
	'version' => '1.1',
	'table' => 'ms2_user_discount',
	'extends' => 'xPDOSimpleObject',
	'fields' => array(
		'percent' => 0,
		'total' => 0,
	),
	'fieldMeta' => array(
		'percent' => array(
			'dbtype' => 'int',
			'precision' => '10',
			'attributes' => 'unsigned',
			'phptype' => 'integer',
			'null' => true,
			'default' => 0,
		),
		'total' => array(
			'dbtype' => 'int',
			'precision' => '10',
			'attributes' => 'unsigned',
			'phptype' => 'integer',
			'null' => true,
			'default' => 0,
		),
	),
);
