<?php

interface msDeliveryInterface
{

    /**
     * Returns an additional cost depending on the method of delivery
     *
     * @param msOrderInterface $order
     * @param msDelivery $delivery
     * @param float $cost
     *
     * @return float|integer
     */
    public function getCost(msOrderInterface $order, msDelivery $delivery, $cost = 0);

    /**
     * Returns failure response
     *
     * @param string $message
     * @param array $data
     * @param array $placeholders
     *
     * @return array|string
     */
    public function getWarehouses($city_id, $poshtomat=false);

    /**
     * Returns failure response
     *
     * @param string $message
     * @param array $data
     * @param array $placeholders
     *
     * @return array|string
     */
    public function error($message = '', $data = array(), $placeholders = array());

    /**
     * Returns success response
     *
     * @param string $message
     * @param array $data
     * @param array $placeholders
     *
     * @return array|string
     */
    public function success($message = '', $data = array(), $placeholders = array());
}

class msDeliveryHandler implements msDeliveryInterface
{
    /** @var modX $modx */
    public $modx;
    public $cityOdessa;
    public $cityKiev;


    public $city = 0;

    public function setCity($city)
    {
        $this->city = $city;
        return $this->city;
    }

    /** @var miniShop2 $ms2 */
    public $ms2;

    /**
     * @param xPDOObject $object
     * @param array $config
     */
    function __construct(xPDOObject $object, $config = array())
    {
        $this->modx = &$object->xpdo;
        $this->ms2 = &$object->xpdo->getService('minishop2');

        $this->cityOdessa='db5c88d0-391c-11dd-90d9-001a92567626';
        $this->cityKiev='8d5a980d-391c-11dd-90d9-001a92567626';

    }

    /** @inheritdoc} */
    public function getCost(msOrderInterface $order, msDelivery $delivery, $cost = 0)
    {
        $cart = $this->ms2->cart->status();
        $weight_price = $delivery->get('weight_price');
        //$distance_price = $delivery->get('distance_price');

        $cart_weight = $cart['total_weight'];
        $cost += $weight_price * $cart_weight;

        $add_price = $delivery->get('price');
        if (preg_match('/%$/', $add_price)) {
            $add_price = str_replace('%', '', $add_price);
            $add_price = $cost / 100 * $add_price;
        }
        $cost += $add_price;
        return $cost;
    }

    /**
     * Returns an additional cost depending on the method of delivery
     *
     * @param msOrderInterface|msOrderHandler $order
     * @param float $cost Current cost of order
     *
     * @return array|string
     */
    public function getWarehouses($city_id, $poshtomat=false)
    {

        return 0;
    }

    /** @inheritdoc} */
    public function error($message = '', $data = array(), $placeholders = array())
    {
        if (!is_object($this->ms2)) {
            $this->ms2 = $this->modx->getService('minishop2');
        }
        return $this->ms2->error($message, $data, $placeholders);
    }

    /** @inheritdoc} */
    public function success($message = '', $data = array(), $placeholders = array())
    {
        if (!is_object($this->ms2)) {
            $this->ms2 = $this->modx->getService('minishop2');
        }
        return $this->ms2->success($message, $data, $placeholders);
    }
}