<?php

interface msCartInterface
{

    /**
     * Initializes cart to context
     * Here you can load custom javascript or styles
     *
     * @param string $ctx Context for initialization
     *
     * @return boolean
     */
    public function initialize($ctx = 'web');

    /**
     * Adds product to cart
     *
     * @param integer $id Id of MODX resource. It must be an msProduct descendant
     * @param integer $count .A number of product exemplars
     * @param array $options Additional options of the product: color, size etc.
     *
     * @return array|string $response
     */
    public function add($id, $count = 1, $options = array(), $optionId=false);

    /**
     * Removes product from cart
     *
     * @param string $key The unique key of cart item
     *
     * @return array|string $response
     */
    public function remove($key);

    /**
     * Changes products count in cart
     *
     * @param string $key The unique key of cart item
     * @param integer $count .A number of product exemplars
     *
     * @return array|string $response
     */
    public function change($key, $count);

    /**
     * Cleans the cart
     *
     * @return array|string $response
     */
    public function clean();

    /**
     * Returns the cart status: number of items, weight, price.
     *
     * @param array $data Additional data to return with status
     *
     * @return array $status
     */
    public function status($data = array());

    /**
     * Returns the cart items
     *
     * @return array $cart
     */
    public function get();

    /**
     * Set all the cart items by one array
     *
     * @param array $cart
     *
     * @return void
     */
    public function set($cart = array());

}

class msCartHandler implements msCartInterface
{
    /* @var modX $modx */
    public $modx;
    /* @var array $cart */
    protected $cart;

    /* @inheritdoc} */
    function __construct(miniShop2 &$ms2, array $config = array())
    {
        //unset($_SESSION['minishop2']['cart']);
        //unset($_SESSION['minishop2']['cart_total']);
        //
        $this->ms2 = &$ms2;
        $this->modx = &$ms2->modx;
        $this->config = array_merge(array(
            'cart' => &$_SESSION['minishop2']['cart']
        , 'max_count' => $this->modx->getOption('ms2_cart_max_count', null, 1000, true)
        , 'allow_deleted' => false
        , 'allow_unpublished' => false,
        ), $config);

        $this->cart = &$this->config['cart'];
//        echo '<pre>minishop2 cart';
//        print_r($this->cart);
//        echo '</pre>';
//        die();

        $this->modx->lexicon->load('minishop2:cart');
        $this->modx->lexicon->load('poylang:site');
        //$this->clean();
        $this->setprices();
        if (empty($this->cart) || !is_array($this->cart)) {
            $this->cart = array();
        }
    }

    /* @inheritdoc} */
    public function initialize($ctx = 'web')
    {
        return true;
    }

    /* @inheritdoc} */
    public function add($id, $count = 1, $options = array(),$optionId=false)
    {


        if (isset($options['size'])) {
            //$options['size']=str_replace('.',',',$options['size']);
        }

        if (empty($id) || !is_numeric($id)) {
            return $this->error('ms2_cart_add_err_id');
        }
        $count = intval($count);
        $platnodostavka = '';
        $filter = array('id' => $id);
        if (!$this->config['allow_deleted']) {
            $filter['deleted'] = 0;
        }
        if (!$this->config['allow_unpublished']) {
            $filter['published'] = 1;
        }
        $language=(isset($options['language'])?$options['language']:'');
        if (empty($language))
            $language='ru';



        if ($language == 'ua') {
            $linkorder='/ua/order';

            $languagePoly= $this->modx->getObject('PolylangLanguage', array(
                'active' => 1,
                'culture_key' => 'ua'
            ));

            $polylang = $this->modx->getService('polylang', 'Polylang');
            if ($polylang) {
                $tools = $polylang->getTools();
                $tools->setLanguage($languagePoly);

            }
        }else {
            $linkorder='/order';


        }

//        $polylang = $modx->getService('polylang', 'Polylang');
//        $polyLangTools = $polylang->getTools();
//        $language = false;
//        $language = $polyLangTools->detectLanguage(true);



        /* @var msProduct $product */
        if ($product = $this->modx->getObject('modResource', $filter)) {
            if (!($product instanceof msProduct)) {
                return $this->error('ms2_cart_add_err_product', $this->status());
            }
            if ($count > $this->config['max_count'] || $count <= 0) {
                return $this->error('ms2_cart_add_err_count', $this->status(), array('count' => $count));
            }
            $tvs = $product->getMany('TemplateVarResources');
            foreach ($tvs as $k => $tv) {
                $tvs[$k] = $tv->toArray();
            }

            if ($options['newprice'])
                $_for_key_price = $product->getPrice(array('newprice' => $options['newprice']));
            else
                $_for_key_price = $product->getPrice();

            $response = $this->ms2->invokeEvent('msOnBeforeAddToCart', array(
                'product' => $product,
                'count' => $count,
                'options' => $options,
                'option_id'=>$optionId,
                'cart' => $this,
            ));
            ////echo $product->getPrice();
            if ($options['newprice'])
                $price = $product->getPrice(array('newprice' => $options['newprice']));
            else
                $price = $product->getPrice();

//            echo '<pre>$options1';
//            print_r($options);
//            echo '</pre>';
//            echo '$price1='.$price."<br/><br/>";

            if (!($response['success'])) {
                return $this->error($response['message']);
            }

            $oldprice = $price;
            $weight = $product->getWeight(); // And weight by "ms2_weight_snippet"
            $count = $response['data']['count'];
            $options = $response['data']['options'];
            $price = $this->ms2->formatPrice($price);
//$_for_key_price
            $key = md5($id . $optionId . $price . $weight . ($this->modx->toJSON($options)));
//            echo $key.' $id='.$id.' $optionId='.$optionId.' $price='.$price.' $weight='.$weight;
//            echo '<pre>$options';
//            print_r($options);
//            echo '</pre>';


            $dis = $dis_loylnost=0;

            $actionOption=$product->get('actionOption');

            if ((!$product->get('actiondis')) || (!$product->get('actiondis') != '')) {
                if ($isAuthenticated = $this->modx->user->isAuthenticated()) {
                    $profile = $this->modx->user->Profile->toArray();
                    //$group_manager = (in_array(7, $this->modx->user->getUserGroups()));//$this->modx->user->isMember('Manager');// проверяем группа менеджер или нет
                    $group_manager = (in_array(7, $this->modx->user->getUserGroups()) || in_array(10, $this->modx->user->getUserGroups()));
                    if (!$group_manager) // скидку сделаем только для не менеджеров
                    {
                        if ($profile['extended']['discount']) {
                            $dis_loylnost = $profile['extended']['discount'];
                            //$dis_loylnost=0;// временно
                            $price = (float)$price - (float)($dis_loylnost / 100) * $price;

                        }
                    }

                }
            }else{
                $dis=$product->get('actiondis');
            }

            $nodostavka = false;
            $where = array(
                'contentid' => $id
            , 'tmplvarid' => 76
            );
            $tv = $this->modx->getObject('modTemplateVarResource', $where);
            if ($tv) $nodostavka = $tv->get('value');
            if ($nodostavka == 'yes') {

                if (($dis_loylnost > 5)&&($actionOption==0)) {
                    $price = (float)$oldprice - (float)(5 / 100) * $oldprice;

                }

            }



            foreach ($tvs as $tv) {
                if ($tv['tmplvarid'] == 76) {
                    if ($tv['value'] == 'yes') $platnodostavka = 'yes';
                }
            }
            if (array_key_exists($key, $this->cart)) {
                return $this->change($key, $this->cart[$key]['count'] + $count);
            } else {

                $product_ids_cart = [];
                $this->cart[$key] = array(
                    'id' => $id
                , 'price' => $price
                , 'option_id' => $optionId
                , 'weight' => $weight
                , 'count' => $count
                , 'options' => $options
                , 'action' => $product->get('actiondis')
                , 'actionOption'=>$actionOption
                , 'plusone' => $product->get('plusone')
                , 'plusonecondition' => $product->get('plusonecondition')
                , 'notchange' => 0
                , 'oldprice' => $product->get('oldprice')
                , 'plus' => $product->get('plusone')
                , 'parent' => ''
                , 'platnodostavka' => $platnodostavka//$tvs['16957']['value']//$platnodostavka
                , 'ctx' => $this->modx->context->get('key')
                );
                $eSputnikProducts= $eFbProducts=[];
                $eSputnikProductsArray=[];
                foreach ($this->cart as $cart_items) {
                    $product_ids_cart[] = $cart_items['id'];

                    $eSputnikProducts[]="{
                          'productKey': '".$cart_items['option_id']."',
                          'price': '".$cart_items['price']."',
                          'quantity': '".$cart_items['count']."',
                          'currency': 'UAH'                          
                      }";

                    $eSputnikProductsArray[]=[
                        'productKey'=> $cart_items['option_id'],
                          'price'=>$cart_items['price'],
                          'quantity'=>$cart_items['count'],
                          'currency'=>'UAH'
                    ];


                    $eFbProducts[]="{
                      id: '".$cart_items['option_id']."',
                      quantity: ".$cart_items['count']." 
                    }";


                }

                $amount = $price * $count;
                $amount = 0;
                $count_products = 0;
                foreach ($this->cart as $cart) {
                    $count_products = $count_products + $cart['count'];
                    $amount = $amount + ($cart['count'] * $cart['price']);
                }


                $product_id = $product->get('id');

                $response = $this->ms2->invokeEvent('msOnAddToCart', array('key' => $key, 'cart' => $this));
                if (!$response['success']) {
                    return $this->error($response['message']);
                }
                $tpl = 'tpl/modaL_cart.tpl';


                $product_array = $product->toArray();
                $core_path = $this->modx->getOption('core_path', null) . 'components/modxsmarty/';
                $template_dir = $this->modx->getOption('modxSmarty.template_dir');
                $template = $this->modx->getOption('modxSmarty.template');
                $cache_dir = $this->modx->getOption('modxSmarty.cache_dir');


                if (!$compile_dir = $this->modx->getOption('modxSmarty.compile_dir')) {
                    $compile_dir = "{$core_path}compiled/";
                }
                $config = array(
                    'template_dir' => $template_dir . "{$template}/",
                    'compile_dir' => $compile_dir,
                    'cache_dir' => $cache_dir,
                    'caching' => $this->modx->getOption('modxSmarty.caching'),
                    'cache_lifetime' => $this->modx->getOption('modxSmarty.cache_lifetime'),
                );


                $lexicon = $this->modx->getService('lexicon','modLexicon');

                $smarty = $this->modx->getService('smarty', 'modxSmarty', MODX_CORE_PATH . 'components/modxsmarty/model/modxSmarty/', $config);

                $templates = array();

                $_compile_dir = "{$template}/";
                if ($pre_template = $this->modx->getOption('modxSmarty.pre_template', null, false)) {
                    $templates['prepend'] = $template_dir . "{$pre_template}/";
                    $this->modx->smarty->assign('pre_template', $pre_template);
                    $this->modx->smarty->assign('pre_template_url', $this->modx->getOption('modxSite.template_url') . $pre_template . '/');
                    $_compile_dir .= "{$pre_template}/";
                }

                $_compile_dir .= $this->modx->context->key . "/";

                $templates['main'] = $template_dir . "{$template}/";
                $smarty->setTemplateDir($templates);
                $smarty->setCompileDir($config['compile_dir'] . $_compile_dir);

                $smarty->inheritance_merge_compiled_includes = false;

                $plugins_dir = array(
                    $core_path . 'smarty_plugins',
                );

                $this->modx->smarty->addPluginsDir($plugins_dir);

                $session_id = $_COOKIE['PHPSESSID'];

                if ($this->modx->user->isAuthenticated()) {
                    $user_id = $this->modx->user->get('id');
                    $q = $this->modx->newQuery('msLastviewed', array('user_id:=' => $user_id));
                    $q->sortby('date', 'DESC');
                    $q->select('`msLastviewed`.`product_id`');
                    $q->limit(5);

                    $q->prepare();
                    $q->stmt->execute();
                    $_lastviewed = $q->stmt->fetchAll(PDO::FETCH_ASSOC);

                    foreach ($_lastviewed as $i) {
                        $newar[] = $i['product_id'];
                    }

                } else {
                    $q = $this->modx->newQuery('msLastviewed', array('cookie_id:=' => $session_id));
                    $q->sortby('date', 'DESC');
                    $q->select('`msLastviewed`.`product_id`');
                    $q->limit(5);

                    $q->prepare();
                    $q->stmt->execute();
                    $_lastviewed = $q->stmt->fetchAll(PDO::FETCH_ASSOC);

                    foreach ($_lastviewed as $i) {
                        if (!in_array($i['product_id'], $product_ids_cart))
                            $newar[] = $i['product_id'];
                    }
                }

                $productmodel = $this->modx->newObject('msProduct');

                $ids = $newar;



                $this->modx->smarty->assign('linkorder', $linkorder.' '.$language);


                $this->modx->smarty->assign('polylang_site_modal_amount_label', $this->modx->lexicon('polylang_site_modal_amount_label'));
                $this->modx->smarty->assign('polylang_site_modal_qty_label', $this->modx->lexicon('polylang_site_modal_qty_label'));

                $this->modx->smarty->assign('polylang_site_modalcart_related', $this->modx->lexicon('polylang_site_modalcart_related'));
                $this->modx->smarty->assign('polylang_site_modalcart_action',$this->modx->lexicon('polylang_site_modalcart_action'));
                $this->modx->smarty->assign('polylang_site_checkout_continue', $this->modx->lexicon('polylang_site_checkout_continue'));
                $this->modx->smarty->assign('polylang_site_product_button_buy', $this->modx->lexicon('polylang_site_product_button_buy'));
                $this->modx->smarty->assign('polylang_site_product_button_askqty',$this->modx->lexicon('polylang_site_product_button_askqty'));
                $this->modx->smarty->assign('polylang_site_product_button_noproduct', $this->modx->lexicon('polylang_site_product_button_noproduct'));
                $this->modx->smarty->assign('polylang_site_checkout_save_product', $this->modx->lexicon('polylang_site_checkout_save_product'));
                $this->modx->smarty->assign('polylang_site_checkout_label', $this->modx->lexicon('polylang_site_checkout_label'));

                $this->modx->smarty->assign('polylang_site_product_label_topprd', $this->modx->lexicon('polylang_site_product_label_topprd'));

                $this->modx->smarty->assign('polylang_site_product_label_newpr', $this->modx->lexicon('polylang_site_product_label_newpr'));
                $this->modx->smarty->assign('polylang_site_product_label_orderpr', $this->modx->lexicon('polylang_site_product_label_orderpr'));
                $this->modx->smarty->assign('polylang_site_product_label_action', $this->modx->lexicon('polylang_site_product_label_action'));
                $this->modx->smarty->assign('polylang_site_product_label_skidka', $this->modx->lexicon('polylang_site_product_label_skidka'));

                $this->modx->smarty->assign('polylang_site_product_button_askqty', $this->modx->lexicon('polylang_site_product_button_askqty'));

                $this->modx->smarty->assign('polylang_site_product_button_buy', $this->modx->lexicon('polylang_site_product_button_buy'));
                $this->modx->smarty->assign('polylang_site_product_button_noproduct', $this->modx->lexicon('polylang_site_product_button_noproduct'));


                if (empty($ids)) {

                    $this->modx->smarty->assign('title', $this->modx->lexicon('polylang_site_common_menu_action'));//'Акции');
                    $this->modx->smarty->assign('rows', []);
                    $this->modx->smarty->assign('ecommerce', []);
                    $viewed_products = $this->modx->smarty->fetch('tpl/related/related_cartmodal.tpl');

                    $this->modx->smarty->assign('viewed_products', $viewed_products);

                } else {
                    $limit = 5;
                    $_ds_str = '';
                    $ids_str = [];
                    foreach ($ids as $i) {
                        $ids_str[] = "'" . $i . "'";
                    }
                    $_ds_str = implode(',', $ids_str);
                    $_ids = implode(',', $ids);


                    $config = array(
                        'resources' => $_ids
                    , 'parents' => 0
                    , 'sortby' => 'FIELD(`msProduct`.`id`, ' . $_ds_str . ')'
                    , 'sortdir' => 'ASC'
                    , 'limit' => $limit
                    , 'tpl' => 'tpl.catalogRecent'
                    , 'includeThumbs' => '150x223'
                    , 'type' => 'recent'
                    , 'return' => 'data'

                    );

                    $result = $this->modx->runSnippet('msProducts', $config);

                    $rows_viewed = $result['rows'];


                    if (!empty($rows_viewed)) {

                        $this->modx->smarty->assign('title', $this->modx->lexicon('polylang_site_see_later'));
                        //'Вы недавно просматривали'
                        $this->modx->smarty->assign('rows', $rows_viewed);

                        $viewed_products = $this->modx->smarty->fetch('tpl/related/related_cartmodal.tpl');
                    } else {
                        $viewed_products = '';
                    }

                    $this->modx->smarty->assign('viewed_products', $viewed_products);
                }


                $this->modx->smarty->assign('modx', $this->modx);
                $this->modx->smarty->assign('template_url', '/core/components/modxsite/templates/default/');

                $this->modx->smarty->assign('count_text_products', $count_products . ' ' . $this->ms2->plural_form($count_products, ['товар', 'товара', 'товаров']));

                $this->modx->smarty->assign('count_product', $count);
                $this->modx->smarty->assign('amount', $amount);

                $categories = $product->getTreeParent($product_array['id']);
                $categories_ar = array_reverse($categories);
                $category_name = implode(' - ', $categories_ar);

                $price = $this->ms2->formatPrice($product_array['price']);

                $this->modx->smarty->assign('category_name', $category_name);
                $this->modx->smarty->assign('weight', $this->cart[$key]['options']['size']);
                $this->modx->smarty->assign('vendorname', $product_array['vendor.name']);
                $this->modx->smarty->assign('key', $key);
                $this->modx->smarty->assign('id_1c', $optionId);


                if ($language == 'ua') {

                    $_sql = 'SELECT 
                    IF(polylang_content.pagetitle!="",polylang_content.pagetitle,modx_site_content.pagetitle) as pagetitleLang                    
                    FROM `modx_site_content` 
                    LEFT JOIN modx_polylang_content as polylang_content ON (modx_site_content.id=polylang_content.content_id and polylang_content.culture_key="ua") 
                    WHERE `modx_site_content`.`id` ="' . $product_array['id'] . '" LIMIT 1';

                    $q = $this->modx->prepare($_sql);
                    $q->execute();
                    $res = $q->fetchAll(PDO::FETCH_ASSOC);


                    $idcategory = false;
                    foreach ($res as $v) {
                        if (!empty($v['pagetitleLang'])) {
                            $product_array['pagetitle'] = $v['pagetitleLang'];
                            break;
                        }
                    }
                }


                $this->modx->smarty->assign('product', $product_array);
                $this->modx->smarty->assign('price', $price);

                $this->modx->smarty->assign('cart', $this->cart[$key]);

                $guest_key=$this->getGuid();//$_COOKIES['GUID'];
                $eSputnik="eS('sendEvent', 'StatusCart', {
                       'StatusCart': [
                           ".implode(', ',$eSputnikProducts)."
                       ],
                       'GUID': '".$guest_key."'
                    });";

                $eSputnik.="fbq('track', 'AddToCart', {
                  value: $price, 
                  currency: 'UAH',
                  content_type: 'product',
                  contents: [".implode(', ',$eFbProducts)."]
                });
                    ";

                $this->modx->smarty->assign('eSputnik', $eSputnik);

                //$this->modx->smarty->assign('eSputnikProducts', $eSputnikProductsArray);



                if ($language == 'ua') {
                    $linkorder='/ua/order';
                }else {
                    $linkorder='/order';
                }
                $this->modx->smarty->assign('linkorder', $linkorder);


                $this->modx->smarty->assign('polylang_site_modal_amount_label', $this->modx->lexicon('polylang_site_modal_amount_label'));
                $this->modx->smarty->assign('polylang_site_modal_qty_label', $this->modx->lexicon('polylang_site_modal_qty_label'));

                $this->modx->smarty->assign('polylang_site_modalcart_related', $this->modx->lexicon('polylang_site_modalcart_related'));
                $this->modx->smarty->assign('polylang_site_modalcart_action',$this->modx->lexicon('polylang_site_modalcart_action'));
                $this->modx->smarty->assign('polylang_site_checkout_continue', $this->modx->lexicon('polylang_site_checkout_continue'));
                $this->modx->smarty->assign('polylang_site_product_button_buy', $this->modx->lexicon('polylang_site_product_button_buy'));
                $this->modx->smarty->assign('polylang_site_product_button_askqty',$this->modx->lexicon('polylang_site_product_button_askqty'));
                $this->modx->smarty->assign('polylang_site_product_button_noproduct', $this->modx->lexicon('polylang_site_product_button_noproduct'));
                $this->modx->smarty->assign('polylang_site_checkout_save_product', $this->modx->lexicon('polylang_site_checkout_save_product'));
                $this->modx->smarty->assign('polylang_site_checkout_label', $this->modx->lexicon('polylang_site_checkout_label'));

                $this->modx->smarty->assign('polylang_site_product_label_topprd', $this->modx->lexicon('polylang_site_product_label_topprd'));

                $this->modx->smarty->assign('polylang_site_product_label_newpr', $this->modx->lexicon('polylang_site_product_label_newpr'));
                $this->modx->smarty->assign('polylang_site_product_label_orderpr', $this->modx->lexicon('polylang_site_product_label_orderpr'));
                $this->modx->smarty->assign('polylang_site_product_label_action', $this->modx->lexicon('polylang_site_product_label_action'));
                $this->modx->smarty->assign('polylang_site_product_label_skidka', $this->modx->lexicon('polylang_site_product_label_skidka'));

                $this->modx->smarty->assign('polylang_site_product_button_askqty', $this->modx->lexicon('polylang_site_product_button_askqty'));

                $this->modx->smarty->assign('polylang_site_product_button_buy', $this->modx->lexicon('polylang_site_product_button_buy'));
                $this->modx->smarty->assign('polylang_site_product_button_noproduct', $this->modx->lexicon('polylang_site_product_button_noproduct'));




                $product_in_cart = $this->modx->smarty->fetch($tpl);

                return $this->success('ms2_cart_add_success', $this->status(array(
                    'key' => $key,
                    'count' => $count,
                    'guest_key'=>$guest_key,
                    'eSputnikProducts'=>$eSputnikProductsArray,
                    'cart' => $product_in_cart
                )), array('count' => $count));
            }
        }

        return $this->error('ms2_cart_add_err_nf', $this->status());
    }

    public function getGuid($record=false)
    {
        /*if (!isset($_COOKIE['GUID'])){
            $guest_key=hash('md5',time().'petchoice');
            setcookie('GUID', $guest_key, time() + (86400 * 365), '/');
        }elseif(empty($_COOKIE['GUID']))
        {
            $guest_key=hash('md5',time().'petchoice');
            setcookie('GUID', $guest_key, time() + (86400 * 365), '/');
        }else{
            $guest_key=$_COOKIE['GUID'];
        }*/
        if ($record)
        {
            return $_SESSION['GUID'];
        }
        $guest_key=hash('md5',time().'petchoice');
        $_SESSION['GUID']=$guest_key;
        //setcookie('GUID', $guest_key, time() + (86400 * 365), '/');
        return $_SESSION['GUID'];

    }

    /* @inheritdoc} */
    public function remove($key)
    {
        if (array_key_exists($key, $this->cart)) {
            $response = $this->ms2->invokeEvent('msOnBeforeRemoveFromCart', array('key' => $key, 'cart' => $this));
            if (!$response['success']) {
                return $this->error($response['message']);
            }
            $idproduct = $this->cart[$key]['id'];
            $options = $this->cart[$key]['options'];
            unset($this->cart[$key]);

            $response = $this->ms2->invokeEvent('msOnRemoveFromCart', array('key' => $key, 'cart' => $this));
            if (!$response['success']) {
                return $this->error($response['message']);
            }
            $keydelete = md5($idproduct . (float)'0.01' . '0' . ($this->modx->toJSON($options)));

            return $this->success('ms2_cart_remove_success', $this->status(array('guid'=>true,'keydelete' => $keydelete)));
        } else {
            return $this->error('ms2_cart_remove_error');
        }
    }

    /* @inheritdoc} */
    public function change($key, $count)
    {


        if (array_key_exists($key, $this->cart)) {
            if ($count <= 0) {
                return $this->remove($key);
            } else if ($count > $this->config['max_count']) {
                return $this->error('ms2_cart_add_err_count', $this->status(), array('count' => $count));
            } else {
                $response = $this->ms2->invokeEvent('msOnBeforeChangeInCart', array('key' => $key, 'count' => $count, 'cart' => $this));
                if (!$response['success']) {
                    return $this->error($response['message']);
                }

                $count = $response['data']['count'];
                $this->cart[$key]['count'] = $count;

                $idproduct = $this->cart[$key]['id'];

                $where = array(
                    'contentid' => $idproduct
                , 'tmplvarid' => 76
                );
                $tv = $this->modx->getObject('modTemplateVarResource', $where);
                $nodostavka = $tv->get('value');
                if ($nodostavka == 'yes') {
                    $discount = 0;
                    if ($isAuthenticated = $this->modx->user->isAuthenticated()) {

                        $userID = $this->modx->user->get('id');
                        $user = $this->modx->getObject('modUser', $userID);
                        $profile = $user->getOne('Profile');
                        $extended = $profile->get('extended');
                        $discount = $extended['discount'];
                        //$discount =0; //временно
                    }


//                    echo '<pre>'.$key;
//                    print_r($this->cart);
//                    echo '</pre>';

                    if (
                        (empty($this->cart[$key]['actionOption'])||($this->cart[$key]['actionOption']==0))
                        &&
                        (($this->cart[$key]['actiondis'] > 5) || ($this->cart[$key]['action'] > 5) || (($discount != 0 && ($discount > 5))))
                    ) {
                        $this->cart[$key]['action'] = 5;
                        $this->cart[$key]['actiondis'] = 5;
                        $this->cart[$key]['price'] = $this->cart[$key]['oldprice'] - 0.05 * $this->cart[$key]['oldprice'];
                    }
//                    echo '<pre>'.$key;
//                    print_r($this->cart);
//                    echo '</pre>';
//                    if ($this->cart[$key]['action'] <= 5)
//                    {
//                        if  ($discount != 0 && ($discount > 5)){
//                            //if (($discount != 0 && ($discount > 5))) {
//                            $this->cart[$key]['action'] = 5;
//                            $this->cart[$key]['actiondis'] = 5;
//                            $this->cart[$key]['price'] = $this->cart[$key]['oldprice'] - 0.05 * $this->cart[$key]['oldprice'];
//                        }
//                    }else{
//                        if  ($discount != 0 && ($discount > 5)){
//                            //if (($discount != 0 && ($discount > 5))) {
//                            $this->cart[$key]['action'] = 5;
//                            $this->cart[$key]['actiondis'] = 5;
//                            $this->cart[$key]['price'] = $this->cart[$key]['oldprice'] - 0.05 * $this->cart[$key]['oldprice'];
//                        }
//                    }
                }

                //$count;
                $response = $this->ms2->invokeEvent('msOnChangeInCart', array('key' => $key, 'count' => $count, 'cart' => $this));

                $datastatus = array('key' => $key, 'count' => $count);
                if (isset($_SESSION['minishop2']['promocode'])) {

                    $st = $this->ms2->order->promocode(array('promocode' => $_SESSION['minishop2']['promocode']['code']));

                    $st1 = json_decode($st);

                    $datastatus['promocode'] = $_SESSION['minishop2']['promocode'];

                    $datastatus['message_promocode'] = $st1->message;

                }
                if (!$response['success']) {
                    return $this->error($response['message']);
                }
            }

            $datastatus['guid']=true;




            return $this->success('ms2_cart_change_success', $this->status($datastatus), array('count' => $count));
        } else {
            return $this->error('ms2_cart_change_error', $this->status(array()));
        }
    }

    /* @inheritdoc} */
    public function clean()
    {
        $response = $this->ms2->invokeEvent('msOnBeforeEmptyCart', array('cart' => $this));
        if (!$response['success']) {
            return $this->error($response['message']);
        }

        foreach ($this->cart as $key => $item) {
            if (empty($item['ctx']) || $item['ctx'] == $this->modx->context->key) {
                unset($this->cart[$key]);
            }
        }
        unset($_SESSION['minishop2']['promocode']);
        unset($_SESSION['minishop2']['cart_total']);
        $response = $this->ms2->invokeEvent('msOnEmptyCart', array('cart' => $this));
        if (!$response['success']) {
            return $this->error($response['message']);
        }

        return $this->success('ms2_cart_clean_success', $this->status(['guid'=>true]));
    }

    /* @inheritdoc} */
    public function status($data = array())
    {
        //unset($_SESSION['minishop2']['promocode']);
        if ($isAuthenticated = $this->modx->user->isAuthenticated()) {
            $profile = $this->modx->user->Profile->toArray();

        }
        $eSputnik_datal_products=[];
        $platnay = 0;
        //$data['discount_loyalnost']=0;// временно

        $status = array(
            'total_count' => 0
        , 'total_cost' => 0
        , 'total_weight' => 0,
        );
        $keys = array();
        foreach ($this->cart as $i => $item) {
            $keys[] = $i;
        }
        $categoriespromo = array();
        // промокод адо узнаь какая категория
        if (isset($data['promocode'])) {
            $promocodeid = $data['promocode']['id'];
            $q = $this->modx->newQuery('PromocodeCategory');
            $q->where(array('promocode' => $promocodeid));
            $q->prepare();
            $sql = $q->toSQL();
            $query = $this->modx->prepare($sql);
            $query->execute();
            $promocategory = $query->fetchAll(PDO::FETCH_ASSOC);

            foreach ($promocategory as $cat) {
                $categoriespromo[] = $cat['PromocodeCategory_category'];
            }
        }
        $_SESSION['minishop2']['cart_total']['economy'] = 0;
        $plus = 1;
        $items_abc = $items_summa = 0;
        if (($data['promocode']['summa'] > 0) && ($data['promocode']['summa'] != '')) {
            foreach ($this->cart as $i => $item) {
                if (isset($data['promocode'])) {


                    $product_par = $this->modx->getObject('modResource', array('id' => $item['id']));
                    $parent = '';
                    if ($product_par) $parent = $product_par->get('parent');

                    $zena = $item['price'];
                    if ((in_array((int)$parent, $categoriespromo)) || (in_array(10, $categoriespromo))) {
                        $price1 = $item['count'] * ((float)$item['oldprice']);//-(float)$loyalnost);
                        $items_summa = $items_abc + $price1;
                    }
                }

            }
        }

        $_eSputnik_datal_products=[];
        $_SESSION['minishop2']['cart_total']['economy'] = 0;
        $economytotal = $economytotal1 = 0;
        $countall = 0;
        foreach ($this->cart as $i => $item) {
            if (empty($item['ctx']) || $item['ctx'] == $this->modx->context->key) {

                $ispromo = false;
                $idproduct = $item['id'];
                $item['price'] = str_replace(',', '.', $item['price']);
                $status['total_count'] += $item['count'];
                $item['newaction'] = '-1';

                $price = $item['price'];
                //echo '$price='.$price." before<br/>";
                if ($price) {
                    $newprice = 0;
                    if ($item['plusone'] != 1) {
                        if (isset($data['promocode'])) {

                            if (count($categoriespromo) > 0) {

                                $product_par = $this->modx->getObject('modResource', array('id' => $item['id']));
                                $parent = '';
                                if ($product_par) $parent = $product_par->get('parent');

                                $zena = $item['price'];

                                if ((in_array((int)$parent, $categoriespromo)) || (in_array(10, $categoriespromo))) {
                                    //надо применить промокод

                                    if (strpos($data['promocode']['discount'], '%') !== false) {
                                        $discountpromostr = $data['promocode']['discount'];
                                        $discountpromoarray = explode('%', $discountpromostr);
                                        $economypr = ((float)$item['oldprice'] * ($data['promocode']['discount'] / 100));
                                        $_SESSION['minishop2']['cart_total']['successpromocode'] = true;
                                        $_SESSION['minishop2']['promocode'] = $data['promocode'];
                                        $ispromo = true;

                                        $econom = $item['oldprice'] - $zena;
                                        //теерь ндао посчитать экономию с промокодом+лояльность
                                        // определим лояльность
                                        $loyalnost = 0;
                                        if ($isAuthenticated = $this->modx->user->isAuthenticated()) {
                                            if ($profile['extended']['discount']) {
                                                $loyalnost = (float)($profile['extended']['discount'] / 100) * $item['oldprice'];
                                                $loyalnostpercent = $profile['extended']['discount'];
                                                //$loyalnost=$loyalnostpercent=0;// временно
                                            }

                                        } elseif (isset($data['discount_loyalnost'])) {
                                            if ($data['discount_loyalnost'] > 0) {
                                                $loyalnost = (float)($data['discount_loyalnost'] / 100) * $item['oldprice'];
                                                $loyalnostpercent = $data['discount_loyalnost'];
                                                //$loyalnost=$loyalnostpercent=0;// временно
                                            }

                                        }


                                        $newprice = $item['oldprice'];
                                        if ($data['promocode']['removeloylnost'] == 1) {

                                            // скидка по товару
                                            $discount = 0;
                                            if (isset($data['discount']) && ($data['discount'] > 0)) $discount = (float)($data['discount'] / 100) * $item['oldprice'];
                                            if ((isset($item['action'])) && ($item['action'] != '') && ($item['action'] != 0)) {
                                                $discountpercent = $item['action'];
                                                $discount = (float)($discountpercent / 100) * $item['oldprice'];
                                                $newprice = $zena;
                                            }
                                            // скидка по промокоду
                                            $discountpromo = $discountpromoarray[0];//$data['promocode']['discount'];

                                            //$economypr1
                                            $economydisloal = (float)$loyalnostpercent + (float)$discountpromo;//$discountpromo;//$economypr;

                                            if ($discountpercent >= $economydisloal) {
                                                //$loyalnost=$discount;//$economy
                                                $economy = $discountpromo = 0;
                                                $item['newaction'] = $loyalnostpercent;
                                            } else {
                                                $economy = $discountpromo;
                                                $item['newaction'] = $discountpercent;//$discountpromo;
                                            }

                                            if ($item['platnodostavka'] == 'yes') {
                                                $economy = 0;
                                                $loyalnost = 5;
                                            }

                                        } else {// не суммируем скидки, считаем максимальную из 3х


                                            // скидка по товару
                                            $discount = $discountpercent = 0;
                                            if (isset($data['discount']) && ($data['discount'] > 0)) $discount = (float)($data['discount'] / 100) * $item['oldprice'];
                                            if ((isset($item['action'])) && ($item['action'] != '') && ($item['action'] != 0)) {
                                                $discountpercent = $item['action'];
                                                $discount = (float)($discountpercent / 100) * $item['oldprice'];
                                                $newprice = $zena;
                                            }
                                            // скидка по промокоду
                                            $discountpromo = $discountpromoarray[0];//$data['promocode']['discount'];


                                            $economypercent = array((int)$loyalnostpercent, (int)$discountpromo, (int)$discountpercent);
                                            $economy = 0;
                                            $economy = max($economypercent);
                                            //$economy = (int)$economy;


                                            if ($item['platnodostavka'] == 'yes') {

                                                $economy = 0;
                                                $loyalnost = 5;
                                            }
                                            if ($economy != $discountpromo) {
                                                /*$economy = 0;
                                                if ($discountpercent != 0)
                                                    $item['newaction'] = $discountpercent;
                                                elseif ($economy != $loyalnostpercent) {
                                                    $item['newaction'] = 0;
                                                } else $item['newaction'] = $loyalnostpercent;*/
                                            } else {
                                                //$item['newaction'] = 0;
                                                //$item['newaction'] = (int)$item['newaction'];
                                            }
                                            if ($economy > 0) {

                                                $item['newaction'] = $economy;
                                            }

                                            //$item['newaction'] = $economy;//(int)$item['newaction'];


                                            /**/

                                            if (isset($data['discount']) && ($data['discount'] > 0)) $item['newaction'] = $data['discount'];

                                        }
                                        $economypr = 0;
                                        $olpr = $item['oldprice'] - $loyalnost;
                                        $economypr = $discountpromo;
                                        $pr1 = $item['oldprice'] - $loyalnost;

                                        if (($data['promocode']['summa'] > 0) && ($data['promocode']['summa'] != '')) {
                                            if ($items_summa >= $data['promocode']['summa']) {
                                                if ($economy > 0) {
                                                    if ($data['promocode']['removeloylnost'] == 1) {
                                                        $newprice = round($item['oldprice'] - $loyalnost, 2);
                                                    } else $newprice = $item['oldprice'];
                                                    $economyabsolute = ((float)$newprice * ($economy / 100));
                                                    $economytotal = $economyabsolute * $item['count'];


                                                    $economytotal1 += $economytotal;
                                                }

                                            } else {
                                                $_SESSION['minishop2']['cart_total']['error'] = 'Не выполнены условия по минимальной цене';
                                            }
                                        } else {
                                            if ($economy > 0) {
                                                if ($data['promocode']['removeloylnost'] == 1) {
                                                    $newprice = round($item['oldprice'] - $loyalnost, 2);
                                                } else $newprice = $item['oldprice'];

                                                $economyabsolute = ((float)$newprice * ($economy / 100));
                                                $economytotal = $economyabsolute * $item['count'];

                                                $economytotal1 += $economytotal;
                                            }
                                        }

                                        if ($newprice > 0) $price = $newprice;
                                    } else {

                                        if (!$item['action']) {//действует абсолютный промокод елси на товар нет акционой скидки, только лояльность
                                            if (($data['promocode']['summa'] > 0) && ($data['promocode']['summa'] != '')) {

                                                if ($isAuthenticated = $this->modx->user->isAuthenticated()) {
                                                    if ($profile['extended']['discount']) {
                                                        $loyalnost = (float)($profile['extended']['discount'] / 100) * $item['oldprice'];
                                                    }
                                                } else $loyalnost = 0;

                                                //$loyalnost = 0;// временно

                                                if (isset($data['discount']) && ($data['discount'] > 0)) {
                                                    //$loyalnost = (float)($data['discount'] / 100) * $item['oldprice'];
                                                }
                                                $price1 = $item['count'] * ((float)$item['oldprice'] - (float)$loyalnost);
                                                $items_abc = $items_abc + $price1;
                                                if ($items_abc >= $data['promocode']['summa']) {
                                                    $i = floor($items_abc / $data['promocode']['summa']);
                                                    $economy_abc = $i * $data['promocode']['discount'];

                                                    $_SESSION['minishop2']['cart_total']['successpromocode'] = true;
                                                    $_SESSION['minishop2']['promocode'] = $data['promocode'];
                                                    $economy = $economy_abc;
                                                    $economytotal = $economy * $item['count'];

                                                    $ispromo = true;
                                                    unset($_SESSION['minishop2']['cart_total']['error']);
                                                } else {
                                                    $_SESSION['minishop2']['cart_total']['error'] = 'Не выполнены условия по минимальной цене';
                                                }
                                            } else {
                                                $economy = $data['promocode']['discount'];
                                                $economypr = $data['promocode']['discount'];
                                                $ispromo = true;
                                            }
                                        }

                                    }

                                } else unset($_SESSION['minishop2']['cart_total']['successpromocode']);

                            }

                        } elseif (isset($data['discount_loyalnost'])) {

                            if ($data['discount_loyalnost'] > 0) {
                                $discount_loyalnost=$data['discount_loyalnost'];

                                if ($item['actionOption']==0) {
                                    if ($item['platnodostavka'] == 'yes') {
                                        $economy = 0;
                                        $discount_loyalnost = 5;
                                    }
                                    $loyalnost = (float)($discount_loyalnost / 100) * $item['oldprice'];
                                    $loyalnostpercent = $discount_loyalnost;

                                    //$loyalnost=$discount_loyalnost=$loyalnostpercent=0;// временно

                                    $item['newaction'] = $loyalnostpercent;
                                    //$newprice = round($item['oldprice'] - $loyalnost, 2);временно
                                    $newprice = round($item['oldprice'], 2);
                                    if ($newprice > 0) $price = $newprice;
                                }
                            }

                        }
                        if ($economy > 0) {
                            $_SESSION['minishop2']['promocode'] = $data['promocode'];
                            $_SESSION['minishop2']['cart_total']['successpromocode'] = true;
                        }

                    }

                    $totalitem = $price * $item['count'];
                    $status['total_cost'] += round($totalitem, 2);

                    $status['total_weight'] += $item['weight'] * $item['count'];
                    $it = $totalitem;

                    $htmlimage = '';
                    $keypodarok = '';
                    if ($i == $data['key']) {

                        if ($item['plusone'] == 1) {
                            if (($item['plus'] == 1) && ($data['count'] == $item['plusonecondition'])) {
                                $filter = array('id' => $item['id']);
                                $product_title = '';
                                $image = '';
                                if ($product = $this->modx->getObject('modResource', $filter)) {
                                    if (($product instanceof msProduct)) {
                                        $image = $product->get('image');
                                        $product_title = $product->get('pagetitle');
                                        $image = $this->modx->runSnippet('pthumb', array(
                                            'options' => 'w=150&h=223&far=1',
                                            'input' => $product->get('image'),
                                            'useResizer' => 1
                                        ));
                                    }
                                }

                                $article = $product->get('article');
                                if ($article != '') {
                                    $article = '<div class="small text-left grey">
														<span class="text-nowrap">Код товара:</span>
														<br>
													  ' . $article . '
													  </div>';
                                }
                                $price1 = $this->modx->runSnippet('msPriceDelimiter', array(
                                    'delimiter' => '.',
                                    'price' => (float)'0.01',
                                    'action' => 'left'
                                ));
                                $price2 = $this->modx->runSnippet('msPriceDelimiter', array(
                                    'delimiter' => '.',
                                    'price' => (float)'0.01',
                                    'action' => 'right'
                                ));
                                $cost1 = $this->modx->runSnippet('msPriceDelimiter', array(
                                    'delimiter' => '.',
                                    'price' => (float)'0.01',
                                    'action' => 'right'
                                ));
                                $cost1 = $this->modx->runSnippet('msPriceDelimiter', array(
                                    'delimiter' => '.',
                                    'price' => (float)'0.01',
                                    'action' => 'right'
                                ));

                                $keypodarok = md5($item['id'] . (float)'0.01' . '0' . ($this->modx->toJSON($item['options'])));

                                $plus = 0;

                                $htmlimage = '<div class="product-option basket-item" data-product-key="' . $keypodarok . '">
												  <button type="button" data-content="Удалить продукт из корзины" data-trigger="hover" data-toggle="popover" data-action="cart/remove" class="close" data-original-title="" title=""></button>
												  <div class="row">
													<div class="col-xs-2 text-center">
													  <img alt="' . $product_title . '" src="' . $image . '">						  
													  ' . $article . '
													</div>
													<div class="col-xs-10">
													  <div class="product-item-title"><span class="red">В подарок! </span>' . $product_title . ' <span class="grey">' . $item['options']['size'] . '</span></div>
													  <div class="row">
														<div class="col-xs-3">
														  <label class="control-label">Цена:</label>
														  <div class="product-price">
														  <b>' . $price1 . '</b> 
														  <small class="small text-top">' . $price2 . '</small>
														  </div>
														</div>
														<div class="col-xs-3">
														  <label class="control-label">Количество</label>
														  <div class="count-box">
															 <a class="icon minus-countoff" onclick="return false" href="#"></a>
															<input type="text" value="1" disabled="disabled" max-legth="4" class="form-control custom-inpt count-products">
															<a onclick="return false" class="icon plus-countoff" href="#"></a>
														  </div>
														</div>
														<div class="col-xs-2">
															<label class="control-label">Скидка</label>
															
															<div class="product-price"><b>0</b>%</div>
														</div>
														<div class="col-xs-4 text-right">
														  <label class="control-label">Сумма</label>
														  <div class="price-large"><b>
														  ' . $cost1 . '</b> 
														  <sup class="small">' . $cost2 . '</sup>
														  </div>
														</div>
													  </div>
													</div>

												  </div>
		
												</div>';

                            } else {
                                $plus = 0;
                                $keypodarok = md5($item['id'] . (float)'0.01' . '0' . ($this->modx->toJSON($item['options'])));
                            }
                        }
                    }


                    $prom = false;
                    if ((in_array((int)$parent, $categoriespromo)) || (in_array(10, $categoriespromo))) {
                        $this->cart[$i]['newaction'] = $item['newaction'];
                        $this->cart[$i]['newprice'] = $newprice;//item['newprice'];

                        $this->cart[$i]['promocode'] = 1;
                        $prom = 1;
                        $price = round($item['count'] * $price, 2);//round($price,2);
                    } else {
                        unset($this->cart[$i]['newprice']);
                        unset($this->cart[$i]['newaction']);
                        //$price = false; временно

                    }

                    $skidka = 0;
                    if ($ispromo) {
                        $skidka = $economypr;
                        if ($economy > 0) {
                            $skidka = $economy;
                        }
                        if ($item['platnodostavka'] == 'yes') {
                            if ($skidka > 5) {
                                $skidka = 5;
                                $item['newaction'] = 0;
                                $price = $item['oldprice'];
                                $this->cart[$i]['price'] = $price;
                            }
                        }

                        $this->cart[$i]['skidka1'] = $skidka;
                    }

                    //echo $item['oldprice'].' skidka1='.$skidka.' newprice='.$price.' newaction='.$item['newaction'].' <br/> ';
//                    echo '<pre>';
//                    print_r(array(
//                        'key' => $i,
//                        'count' => $item['count'],
//                        'actionOption'=> $item['actionOption'],
//                        'option_id'=>$item['option_id'],
//                        'oldprice' => $item['oldprice'],
//                        'skidka1' => $skidka,
//                        'cost' => round($it, 2),
//                        'title' => $product_title,
//                        'image' => $image,
//                        'plus' => $plus,
//                        'newprice' => $price,
//                        'keynew' => $keypodarok,
//                        'htmlimage' => $htmlimage,
//                        'newaction' => $item['newaction'],
//                        'platnodostavka' => $item['platnodostavka'],///$platnay,
//                        'plusonecondition' => $item['plusonecondition'],
//                        'weight' => $item['weight'] * $item['count'],
//                    ));
//                    echo '</pre>';

//                    $totalitem = $price * $item['count'];
//                    $status['total_cost'] += round($totalitem, 2);
//
//                    $status['total_weight'] += $item['weight'] * $item['count'];
//                    $it = $totalitem;
//echo '$it = '.$it.' $price='.$price.' oldprice='.$item['oldprice']."<br/><br/>";
//                    if (empty($skidka)&&(empty($item['newaction']))&&($item['newaction']<=0)&&($skidka<=0))
//                    {
                        $price=$item['oldprice'];
                    //}
                    $status['items'][] = array(
                        'key' => $i,
                        'count' => $item['count'],
                        'actionOption'=> $item['actionOption'],
                        'option_id'=>$item['option_id'],
                        'oldprice' => $item['oldprice'],
                        'skidka1' => $skidka,
                        'cost' => round($it, 2),
                        'title' => $product_title,
                        'image' => $image,
                        'plus' => $plus,
                        'newprice' => $price,
                        'keynew' => $keypodarok,
                        'htmlimage' => $htmlimage,
                        'newaction' => $item['newaction'],
                        'platnodostavka' => $item['platnodostavka'],///$platnay,
                        'plusonecondition' => $item['plusonecondition'],
                        'weight' => $item['weight'] * $item['count'],
                        'discount' => 0
                    );

                    $productKey=$idproduct.'_'.
                    $eSputnik_datal_products[]="{
                          'productKey': '".$item['option_id']."',
                          'price': '".$price."',
                          'quantity': '".$item['count']."',
                          'currency': 'UAH'
                          
                      }";

                    $_eSputnik_datal_products[]=[
                        'productKey'=> $item['option_id'],
                          'price'=> $price,
                          'quantity'=>$item['count'],
                          'currency'=>'UAH'
                    ];
//                    "{
//                          'productKey': '".$item['option_id']."',
//                          'price': '".$price."',
//                          'quantity': '".$item['count']."',
//                          'currency': 'UAH'
//
//                      }";

                }

                $countall = $countall + $item['count'];
            }
        }
        $_SESSION['minishop2']['cart_total']['economy'] = $economytotal1;


        if ($_SESSION['minishop2']['cart_total']['economy'] != 0) $economy = $_SESSION['minishop2']['cart_total']['economy'];

        if ($isAuthenticated = $this->modx->user->isAuthenticated()) {
            $profile = $this->modx->user->Profile->toArray();
        }

        $cityid = '';
        $delivery = '';
        $payment = '';
        if (isset($_POST['cityid']) && ($_POST['cityid'] != '')) $cityid = $_POST['cityid'];
        if (isset($_POST['delivery']) && ($_POST['delivery'] != '')) $delivery = $_POST['delivery'];
        if (isset($_POST['payment']) && ($_POST['payment'] != '')) $payment = $_POST['payment'];

        $status['total_cost_withdelivery'] = $status['total_cost'];
        $order = $this->ms2->order->get();
        if (isset($data['promocode'])) {

            if ((!isset($_SESSION['minishop2']['cart_total']['total_cost_old'])) || ($_SESSION['minishop2']['cart_total']['total_cost_old'] == 0))
                $_SESSION['minishop2']['cart_total']['total_cost_old'] = $status['total_cost'];
            if ((!isset($_SESSION['minishop2']['cart_total']['total_cost_withdelivery_old'])) || ($_SESSION['minishop2']['cart_total']['total_cost_withdelivery_old'] == 0))
                $_SESSION['minishop2']['cart_total']['total_cost_withdelivery_old'] = $status['total_cost_withdelivery'];
            if (isset($_SESSION['minishop2']['cart_total']['successpromocode'])) {// если скидку уже применили - не применять повторно для данного товара
                $_SESSION['minishop2']['cart_total']['total_cost_withdelivery'] = $status['total_cost_withdelivery'];
                $_SESSION['minishop2']['cart_total']['total_cost'] = $status['total_cost'];

                if (isset($data['promocode']) && (count($categoriespromo) == 0)) {
                    $_SESSION['minishop2']['promocode'] = $data['promocode'];
                    $_SESSION['minishop2']['cart_total']['successpromocode'] = true;
                    if (isset($data['promocode'])) {//}&&($data['promocode']['summa']!=0)){
                        $discount = $data['promocode']['discount'];
                        if ($this->promocode_cart($data['promocode'])) $this->use_promocode($discount);
                        $status['total_cost_withdelivery'] = $_SESSION['minishop2']['cart_total']['total_cost_withdelivery_old'];

                        $status['successpromocode'] = $_SESSION['minishop2']['cart_total']['successpromocode'];

                    } elseif (isset($_SESSION['minishop2']['cart_total']['successpromocode'])) {
                    }
                }
                if (isset($_SESSION['minishop2']['cart_total']['successpromocode'])) {

                    $status['successpromocode'] = $_SESSION['minishop2']['cart_total']['successpromocode'];

                    if ($_SESSION['minishop2']['cart_total']['economy'] > 0) {
                        $status['economy'] = $economy;
                        unset($_SESSION['minishop2']['cart_total']['error']);
                    } elseif (isset($_SESSION['minishop2']['cart_total']['economy']) && ($_SESSION['minishop2']['cart_total']['economy'] != 0)) {
                        $status['economy'] = $_SESSION['minishop2']['cart_total']['economy'];
                        unset($_SESSION['minishop2']['cart_total']['error']);
                    } else {
                        $status['economy'] = false;
                        if (isset($data['promocode']) && (count($categoriespromo) > 0)) {
                            if (!isset($_SESSION['minishop2']['cart_total']['error'])) $_SESSION['minishop2']['cart_total']['error'] = 'Промо-код суммируется только с постоянной скидкой клиента';
                        }
                    }

                    if ($status['economy'] !== false) {
                        $status['economy'] = round($status['economy'], 2);
                    }
                    if (($data['promocode']['summa'] > 0) && ($status['total_cost'] < $data['promocode']['summa'])) $status['economy'] = false;
                }
            }
        }
        if (isset($data['promocode'])) {
            if ($status['economy'] != '') {

                $status['total_cost_withdelivery'] = $status['total_cost_withdelivery'] - $status['economy'];//$_SESSION['minishop2']['cart_total']['economy'];
                $status['total_cost'] = $status['total_cost'] - $status['economy'];//$_SESSION['minishop2']['cart_total']['economy'];
            }
        }
        if (($delivery != '') && ($cityid != '')) {
            $myclassModelPath = MODX_CORE_PATH . 'components/minishop2/model/minishop2/';
            $this->modx->getService('msDelivery', 'msDelivery', $myclassModelPath);

            //необходимо добавить товары с учётом галочки Платная доставка
            $platnay = 0;
            foreach ($status['items'] as $it) {
                if ($it['platnodostavka'] == 'yes') $platnay += $it['cost'];
            }


            $costdel = $this->modx->msDelivery->getcostdelivery($delivery, $cityid, $status['total_cost'], $platnay);

            if (isset($costdel['cost_delivery']) && ($costdel['cost_delivery'] != '')) {
                $status['cost_delivery'] = $costdel['cost_delivery'];
                $status['total_cost_withdelivery'] = $status['cost_delivery'] + $status['total_cost'];
            }
            if ($costdel['infodelivery']) $status['infodelivery'] = $costdel['infodelivery'];
            if ($costdel['descdelivery']) $status['descdelivery'] = $costdel['descdelivery'];

        }
        if ($status['cost_delivery'] == '') {
            if (($delivery == 2) || ($delivery == 3) || ($delivery == 5)) {
                $status['cost_delivery'] = $costdel['descdelivery'];
            }
            if (($delivery == 2) || ($delivery == 3) || ($delivery == 5)) {
                //$status['descdelivery']=' ';
                //$status['cost_delivery']='';
            }
        } elseif ($status['cost_delivery'] == 0) {

            $status['cost_delivery'] = 'Бесплатно';

            if (($delivery == 2) || ($delivery == 3) || ($delivery == 5)) {
                $status['descdelivery'] = ' ';

                if ($payment == 6) $status['descdelivery'] = 'Внимание! Перевод денег осуществляется за Ваш счет';
            }
        }

        $status['total_cost_withdelivery'] = $this->ms2->formatPrice(round($status['total_cost_withdelivery'], 0));

        $eSputnikCart='';
        $guest_key='';
        if (isset($data['guid'])) {
            $guest_key = $this->getGuid(true);//$_COOKIES['GUID'];
//        else
//            $guest_key = $this->getGuid();


            $eSputnikCart = "eS('sendEvent', 'StatusCart', {
               'StatusCart': [
                   " . implode(', ', $eSputnik_datal_products) . "
               ],
               'GUID': '" . $guest_key . "'
            });";


        }

        $status['guest_key']=$guest_key;
        $status['eSputnikProducts']=$_eSputnik_datal_products;


        $status['eSputnik']=$eSputnikCart;
        $status['total_cost'] = $this->ms2->formatPrice(round($status['total_cost'], 0));
        if ($status['economy'] != '') {
            $status['economy'] = $this->ms2->formatPrice($status['economy']);
        }


        $status['count_text_products'] = $countall . ' ' . $this->ms2->plural_form($countall, ['товар', 'товара', 'товаров']);

        return array_merge($data, $status);
    }

    public function promocode_cart($promocode)
    {

        $object_promocode = $this->modx->getObject('PromocodeItem', array(
            'code' => $promocode,
        ));
        if ($object_promocode) {
            if ($object_promocode->get('status') == 0) {
                $promous = false;

            } else {

                $term = $object_promocode->get('term');
                $discount = $object_promocode->get('discount');
                $summa = $object_promocode->get('summa');
                $category = $object_promocode->get('category');
                $total_cost = $cart['total_cost'];


                if (($term == '0000-00-00 00:00:00') || ($term == '1970-01-01 00:00:00') || ($term == NULL) || (date('Y-m-d H:i:s') <= $term)) {


                    if (($summa > 0) && ($summa != '')) {
                        if (($total_cost >= $summa) && ($category == 10)) {

                        } elseif ($category == 10) {
                            $promous = false;
                        } elseif (($total_cost >= $summa) && ($category != 0)) {// если применение к категории

                        } elseif (($cart['total_cost'] >= $summa) && ($category == 0)) {

                        } else {
                            $promous = false;
                        }

                    } else {
                        $response = $this->use_promocode($object_promocode->toArray());//$object_promocode);//$promocode,$discount);

                    }
                } else {
                    $promous = false;
                }
            }
        } else {
            $promous = false;
        }


        return $promous;
    }

    public function use_promocode($discount)
    {
        if (strpos($discount, '%') !== false) {
            $discount1 = explode('%', $discount);
            $discount1 = $discount[0];

            $economy = ($_SESSION['minishop2']['cart_total']['total_cost_old'] * ($discount1 / 100));
            $_SESSION['minishop2']['cart_total']['total_cost_withdelivery'] = $_SESSION['minishop2']['cart_total']['total_cost_withdelivery_old'] - $economy;

            $_SESSION['minishop2']['cart_total']['total_cost'] = $_SESSION['minishop2']['cart_total']['total_cost_old'] - $economy;
        } else {
            $economy = $discount;
            $_SESSION['minishop2']['cart_total']['total_cost_withdelivery'] = round($_SESSION['minishop2']['cart_total']['total_cost_withdelivery_old'] - $discount);
            $_SESSION['minishop2']['cart_total']['total_cost'] = round($_SESSION['minishop2']['cart_total']['total_cost_old'] - $discount);
        }

        $_SESSION['minishop2']['cart_total']['economy'] = $economy;
        $_SESSION['minishop2']['cart_total']['successpromocode'] = true;

    }

    public function setprices()
    {
        if ($isAuthenticated = $this->modx->user->isAuthenticated()) {
            //$group_manager = (in_array(7, $this->modx->user->getUserGroups()));//$this->modx->user->isMember('Manager');// проверяем группа менеджер или нет
            $group_manager = (in_array(7, $this->modx->user->getUserGroups()) || in_array(10, $this->modx->user->getUserGroups()));
            if ($group_manager) // скидку сделаем только для не менеджеров
            {

                /*echo '<pre>eee';
                print_R($_SESSION['user_for_order']);
                echo '</pre>';*/

                $cart = $this->get();
                $cartnew = array();
                if (isset($_SESSION['user_for_order'])) {
                    if (isset($_SESSION['user_for_order']['extended']['discount']) && ($_SESSION['user_for_order']['extended']['discount'] != ''))
                        $discount = $_SESSION['user_for_order']['extended']['discount'];

                    //$discount=0;// временно

                    foreach ($cart as $key => $item) {
                        /*echo '<pre>$item';
                        print_R($item);
                        echo '</pre>';*/
                        $cartnew[$key] = $item;
                        $price = $item['price'];
                        $oldprice = round($item['oldprice'], 2);
                        //$price=$discount
                        if ($item['action'] == '') {
                            if ((isset($discount)) && ($discount) && ($discount != '')) {
                                //$price = $oldprice - ($discount / 100) * $oldprice;
                                $price = (float)$price;
                            }
                            $cartnew[$key]['price'] = $price;
                        } else {
                            if ($discount > $item['action']) $item['action'] = $discount;

                            //$price = round($oldprice - ($item['action'] / 100) * $oldprice, 2);
                            $price = (float)$price;
                            $cartnew[$key]['price'] = $price;
                        }
                    }
                    $this->set($cartnew);
                }
            }
        }
    }

    /* @inheritdoc} */
    public function get()
    {
        $cart = array();
        foreach ($this->cart as $key => $item) {
            if (empty($item['ctx']) || $item['ctx'] == $this->modx->context->key) {
                $cart[$key] = $item;
            }
        }
        return $cart;
    }

    /* @inheritdoc} */
    public function set($cart = array())
    {
        $this->cart = $cart;
    }

    /**
     * Shorthand for MS2 error method
     *
     * @param string $message
     * @param array $data
     * @param array $placeholders
     *
     * @return array|string
     */
    public function error($message = '', $data = array(), $placeholders = array())
    {
        return $this->ms2->error($message, $data, $placeholders);
    }

    /**
     * Shorthand for MS2 success method
     *
     * @param string $message
     * @param array $data
     * @param array $placeholders
     *
     * @return array|string
     */
    public function success($message = '', $data = array(), $placeholders = array())
    {
        return $this->ms2->success($message, $data, $placeholders);
    }

}