<?php
/**
 * PolylangContent Russian Lexicon Entries for Polylang
 *
 * @package polylang
 * @subpackage lexicon
 */
$_lang['polylang_content_tab_localization'] = 'Локализация';
$_lang['polylang_content_tab_polylangcontent'] = 'Документ';
$_lang['polylang_content_tab_polylangproduct'] = 'Товар miniShop2';
$_lang['polylang_content_tab_options'] = 'Опции товара';
$_lang['polylang_content_tab_tvs'] = 'Дополнительные поля (TV)';
$_lang['polylang_content_header_culture_key'] = 'Язык';
$_lang['polylang_content_header_pagetitle'] = 'Заголовок';
$_lang['polylang_content_header_seotitle'] = 'SEO Заголовок';
$_lang['polylang_content_header_keywords'] = 'SEO Ключевые слова';
$_lang['polylang_content_header_longtitle'] = 'Расширенный заголовок';
$_lang['polylang_content_header_description'] = 'Описание';
$_lang['polylang_content_header_menutitle'] = 'Заголовок меню';
$_lang['polylang_content_header_introtext'] = 'Аннотация';
$_lang['polylang_content_header_content'] = 'Содержимое';
$_lang['polylang_content_header_active'] = 'Активный';
$_lang['polylang_content_header_editedby'] = 'Редактор';
$_lang['polylang_content_header_editedon'] = 'Отредактирована';
$_lang['polylang_content_header_actions'] = 'Действия';
$_lang['polylang_content_label_culture_key'] = 'Язык';
$_lang['polylang_content_label_culture_key_help'] = '';
$_lang['polylang_content_label_pagetitle'] = 'Заголовок';
$_lang['polylang_content_label_pagetitle_help'] = '';
$_lang['polylang_content_label_seotitle'] = 'SEO Заголовок';
$_lang['polylang_content_label_seotitle_help'] = '';
$_lang['polylang_content_label_description'] = 'Описание';
$_lang['polylang_content_label_description_help'] = '';
$_lang['polylang_content_label_keywords'] = 'SEO Ключевые слова';
$_lang['polylang_content_label_keywords_help'] = '';
$_lang['polylang_content_label_longtitle'] = 'Расширенный заголовок';
$_lang['polylang_content_label_longtitle_help'] = '';
$_lang['polylang_content_label_menutitle'] = 'Заголовок меню';
$_lang['polylang_content_label_menutitle_help'] = '';
$_lang['polylang_content_label_introtext'] = 'Аннотация';
$_lang['polylang_content_label_introtext_help'] = '';
$_lang['polylang_content_label_content'] = 'Содержимое';
$_lang['polylang_content_label_content_help'] = '';
$_lang['polylang_content_label_active'] = 'Активный';
$_lang['polylang_content_label_active_help'] = '';
$_lang['polylang_content_btn_create'] = 'Добавить локализацию';
$_lang['polylang_content_btn_generate'] = 'Добавить и перевести недостающие локализации';
$_lang['polylang_content_btn_translate_all'] = 'Перевести все поля';
$_lang['polylang_btn_copy'] = 'Копировать';
$_lang['polylang_btn_copy_field_content'] = 'Скопировать содержимое поля';
$_lang['polylang_content_copy_help'] = 'Выберите язык, в который следует скопировать данные из локализации. Если выбранный язык еще не имеет локализации, то она будет создана автоматически.';
$_lang['polylang_content_field_content_copy_help'] = 'Выберите язык, в который следует скопировать контент из поля.';
$_lang['polylang_content_only_dependent_languages'] = 'Показать только зависимые языки';
$_lang['polylang_content_autocreate_dependent_languages'] = 'Автоматически создать для зависимых языков';
$_lang['polylang_content_autoupdate_dependent_languages'] = 'Автоматически обновить у зависимых языков';
$_lang['polylang_content_menu_update'] = 'Редактировать локализацию';
$_lang['polylang_content_menu_copy'] = 'Скопировать локализацию';
$_lang['polylang_content_menu_edit_lexicon'] = 'Редактировать лексиконы';
$_lang['polylang_content_menu_enable'] = 'Включить локализацию';
$_lang['polylang_content_menu_multiple_enable'] = 'Включить выбранные локализации';
$_lang['polylang_content_menu_disable'] = 'Отключить локализацию';
$_lang['polylang_content_menu_multiple_disable'] = 'Отключить выбранные локализации';
$_lang['polylang_content_menu_remove'] = 'Удалить локализацию';
$_lang['polylang_content_menu_multiple_remove'] = 'Удалить выбранные локализации';
$_lang['polylang_content_title_win_create'] = 'Добавить локализацию';
$_lang['polylang_content_title_win_update'] = 'Редактировать локализацию';
$_lang['polylang_content_title_win_copy'] = 'Копирование локализации';
$_lang['polylang_content_title_win_field_content_copy'] = 'Копирование содержимого поля';
$_lang['polylang_content_title_win_remove'] = 'Удалить локализацию';
$_lang['polylang_content_confirm_remove'] = 'Вы уверены, что хотите удалить эту локализацию?';
$_lang['polylang_content_confirm_multiple_remove'] = 'Вы уверены, что хотите удалить эти локализации?';
$_lang['polylang_content_err_nf_resource'] = 'Не найден ресурс с ID [[+id]]';
$_lang['polylang_content_err_nf_Localization'] = 'Не найден ресурс с ID [[+id]]';
$_lang['polylang_content_err_save_copy'] = 'Не удалось сохранить копию локализации для ID [[+id]].';
$_lang['polylang_content_err_save_copy_content_field'] = 'Не удалось сохранить контент поля [[+field]] для языка [[+lang]] локализации ID [[+id]].';