<?php
/**
 * PolylangLanguageGroup Russian Lexicon Entries for Polylang
 *
 * @package polylang
 * @subpackage lexicon
 */
$_lang['polylang_polylanglanguagegroup_header_name'] = 'Название';
$_lang['polylang_polylanglanguagegroup_label_name'] = 'Название';
$_lang['polylang_polylanglanguagegroup_label_name_help'] = '';
$_lang['polylang_polylanglanguagegroup_tab'] = 'Языковые группы';
$_lang['polylang_polylanglanguagegroup_header_actions'] = 'Действия';
$_lang['polylang_polylanglanguagegroup_btn_create'] = 'Создать группу';
$_lang['polylang_polylanglanguagegroup_menu_update'] = 'Редактировать группу';
$_lang['polylang_polylanglanguagegroup_menu_remove'] = 'Удалить группу';
$_lang['polylang_polylanglanguagegroup_menu_multiple_remove'] = 'Удалить группы';
$_lang['polylang_polylanglanguagegroup_title_win_create'] = 'Создать группу';
$_lang['polylang_polylanglanguagegroup_title_win_update'] = 'Редактировать группу';
$_lang['polylang_polylanglanguagegroup_title_win_remove'] = 'Удалить группу';
$_lang['polylang_polylanglanguagegroup_confirm_remove'] = 'Вы уверены, что хотите удалить эту группу?';
$_lang['polylang_polylanglanguagegroup_confirm_multiple_remove'] = 'Вы уверены, что хотите удалить эти группы?';