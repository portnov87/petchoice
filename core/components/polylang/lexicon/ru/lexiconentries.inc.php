<?php
/**
 * LexiconEntries Russian Lexicon Entries for Polylang
 *
 * @package polylang
 * @subpackage lexicon
 */
$_lang['polylang_lexiconentries_header_name'] = 'Название';
$_lang['polylang_lexiconentries_header_value'] = 'Значение';
$_lang['polylang_lexiconentries_header_language'] = 'Язык';
$_lang['polylang_lexiconentries_header_editedon'] = 'Изменен';
$_lang['polylang_lexiconentries_header_actions'] = 'Действия';
$_lang['polylang_lexiconentries_label_name'] = 'Название';
$_lang['polylang_lexiconentries_label_name_help'] = '';
$_lang['polylang_lexiconentries_label_value'] = 'Значение';
$_lang['polylang_lexiconentries_label_value_help'] = '';
$_lang['polylang_lexiconentries_label_language'] = 'Язык';
$_lang['polylang_lexiconentries_label_language_help'] = '';
$_lang['polylang_lexiconentries_label_editedon'] = 'Изменен';
$_lang['polylang_lexiconentries_label_editedon_help'] = '';
$_lang['polylang_lexiconentries_page_title'] = 'Управление лексиконами темы "site"';
$_lang['polylang_lexiconentries_tab'] = 'Управление лексиконами';
$_lang['polylang_lexiconentries_intro_msg'] = 'Здесь вы можете управлять всеми записями словаря в  пространстве имён "polylang" и теме "site". <strong>Каждая запись словаря будет доступна через плейсхолдер [[%name]]</strong>.<br>Дважды нажмите на любой параметр записи, чтобы изменить его.';
$_lang['polylang_lexiconentries_clone_lexicon_help'] = 'Выберите язык, в который следует скопировать лексиконы.';
$_lang['polylang_lexiconentries_translate_lexicon_help'] = 'Выберите язык, для которого следует сделать перевод лексиконов.';
$_lang['polylang_lexiconentries_only_dependent_languages'] = 'Показать только зависимые языки';
$_lang['polylang_lexiconentries_only_independent_languages'] = 'Показать только не независимые языки';
$_lang['polylang_lexiconentries_btn_create'] = 'Создать лексикон';
$_lang['polylang_lexiconentries_btn_clone'] = 'Клонировать';
$_lang['polylang_lexiconentries_btn_translate'] = 'Перевести';
$_lang['polylang_lexiconentries_tbar_btn_translate'] = 'Перевести все лексиконы';
$_lang['polylang_lexiconentries_tbar_btn_clone'] = 'Клонировать все лексиконы в другой язык';
$_lang['polylang_lexiconentries_menu_update'] = 'Редактировать лексикон';
$_lang['polylang_lexiconentries_menu_clone'] = 'Клонировать лексикон в другой язык';
$_lang['polylang_lexiconentries_menu_multiple_clone'] = 'Клонировать лексиконы в другой язык';
$_lang['polylang_lexiconentries_menu_translate'] = 'Перевести лексикон';
$_lang['polylang_lexiconentries_menu_multiple_translate'] = 'Перевести лексиконы';
$_lang['polylang_lexiconentries_menu_remove'] = 'Удалить лексикон';
$_lang['polylang_lexiconentries_menu_multiple_remove'] = 'Удалить лексиконы';
$_lang['polylang_lexiconentries_title_win_create'] = 'Создать лексикон';
$_lang['polylang_lexiconentries_title_win_update'] = 'Редактировать лексикон';
$_lang['polylang_lexiconentries_title_win_clone'] = 'Клонирование';
$_lang['polylang_lexiconentries_title_win_translate'] = 'Перевод';
$_lang['polylang_lexiconentries_title_win_remove'] = 'Удалить лексикон';
$_lang['polylang_lexiconentries_confirm_remove'] = 'Вы уверены, что хотите удалить этот лексикон?';
$_lang['polylang_lexiconentries_confirm_multiple_remove'] = 'Вы уверены, что хотите удалить эти лексиконы?';
$_lang['polylang_lexiconentries_err_save'] = 'Произошла ошибка при попытке сохранить запись словаря.';