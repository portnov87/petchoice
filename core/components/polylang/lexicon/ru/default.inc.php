<?php
/**
 * Default Russian Lexicon Entries for Polylang
 *
 * @package polylang
 * @subpackage lexicon
 */

include_once 'setting.inc.php';
include_once 'translator.inc.php';
include_once 'properties.inc.php';
include_once 'lexiconentries.inc.php';
include_once 'polylanglanguagegroup.inc.php';
include_once 'polylangtvtmplvars.inc.php';
include_once 'polylangcontent.inc.php';
include_once 'polylangfield.inc.php';
include_once 'polylanglanguage.inc.php';

$_lang['polylang_menu_polylang'] = 'Polylang';
$_lang['polylang_menu_polylang_desc'] = 'Локализации сайта v2.0.0';
$_lang['polylang_menu_settings'] = 'Настройки';
$_lang['polylang_menu_settings_desc'] = '';
$_lang['polylang_menu_lexicon'] = 'Лексиконы';
$_lang['polylang_menu_lexicon_desc'] = 'Управление лексиконами темы "site"';
$_lang['polylang_tv_inject_fieldset'] = 'Polylang';
$_lang['polylang_tv_enabled'] = 'TV доступно в локализациях';
$_lang['polylang_tv_enabled_desc'] = 'Дополнительное поле доступно для редактирования в локализациях';
$_lang['polylang_tv_translate'] = 'TV доступно для перевода в локализациях';
$_lang['polylang_tv_translate_desc'] = 'Дополнительное поле доступно для перевода в локализациях';
$_lang['polylang_option_inject_fieldset'] = 'Polylang';
$_lang['polylang_option_enabled'] = 'Опция доступна в локализациях';
$_lang['polylang_option_enabled_desc'] = 'Опция доступна для редактирования в локализациях';
$_lang['polylang_option_translate'] = 'Опция доступна для перевода в локализациях';
$_lang['polylang_option_translate_desc'] = 'Опция доступна для перевода в локализациях';
$_lang['polylang_filter'] = 'Фильтр';
$_lang['polylang_filter_reset'] = 'Сброс';
$_lang['polylang_filter_submit'] = 'Отправить';
$_lang['polylang_filter_language_group'] = 'Языковая группа:';
$_lang['polylang_filter_language_parent_type'] = 'Родительский язык:';
$_lang['polylang_language_parent_type_1'] = 'Есть';
$_lang['polylang_language_parent_type_2'] = 'Нет';

