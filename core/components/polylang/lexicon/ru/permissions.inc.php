<?php
/**
 * Permissions Russian Lexicon Entries for Polylang
 *
 * @package polylang
 * @subpackage lexicon
 */

$_lang['polylang_menu_settings'] = 'Показывать в меню пункт «Настройки».';
$_lang['polylang_menu_lexicon'] = 'Показывать в меню пункт «Управление лексиконами темы "site"».';

$_lang['polylang_language_view'] = 'Разрешает просмотр языков';
$_lang['polylang_language_list'] = 'Разрешает вывод списка языков';
$_lang['polylang_language_save'] = 'Разрешает создание\изменение языков';
$_lang['polylang_language_clone'] = 'Разрешает клонирование языков';
$_lang['polylang_language_remove'] = 'Разрешает удаление языков';

$_lang['polylang_language_group_view'] = 'Разрешает просмотр языковых групп';
$_lang['polylang_language_group_list'] = 'Разрешает вывод списка языковых групп';
$_lang['polylang_language_group_save'] = 'Разрешает создание\изменение языковых групп';
$_lang['polylang_language_group_remove'] = 'Разрешает удаление языковых групп';

$_lang['polylang_lexicon_view'] = 'Разрешает просмотр лексиконов';
$_lang['polylang_lexicon_list'] = 'Разрешает вывод списка лексиконов';
$_lang['polylang_lexicon_save'] = 'Разрешает создание\изменение лексиконов';
$_lang['polylang_lexicon_clone'] = 'Разрешает клонирование лексиконов';
$_lang['polylang_lexicon_translate'] = 'Разрешает перевод лексиконов';
$_lang['polylang_lexicon_remove'] = 'Разрешает удаление лексиконов';

$_lang['polylang_field_view'] = 'Разрешает просмотр полей';
$_lang['polylang_field_list'] = 'Разрешает вывод списка полей';
$_lang['polylang_field_save'] = 'Разрешает создание\изменение полей';
$_lang['polylang_field_remove'] = 'Разрешает удаление языковых полей';

$_lang['polylang_localization_view'] = 'Разрешает просмотр локализаций';
$_lang['polylang_localization_list'] = 'Разрешает вывод списка локализаций';
$_lang['polylang_localization_save'] = 'Разрешает создание\изменение локализаций';
$_lang['polylang_localization_clone'] = 'Разрешает клонирование локализаций';
$_lang['polylang_localization_generate'] = 'Разрешает генерирование локализаций';
$_lang['polylang_localization_remove'] = 'Разрешает удаление локализаций';