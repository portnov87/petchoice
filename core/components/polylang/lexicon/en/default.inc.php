<?php
/**
 * Default English Lexicon Entries for Polylang
 *
 * @package polylang
 * @subpackage lexicon
 */

include_once 'setting.inc.php';
include_once 'translator.inc.php';
include_once 'properties.inc.php';
include_once 'lexiconentries.inc.php';
include_once 'polylanglanguagegroup.inc.php';
include_once 'polylangtvtmplvars.inc.php';
include_once 'polylangcontent.inc.php';
include_once 'polylangfield.inc.php';
include_once 'polylanglanguage.inc.php';

$_lang['polylang_menu_polylang'] = 'Polylang';
$_lang['polylang_menu_polylang_desc'] = 'Site localization v2.0.0';
$_lang['polylang_menu_settings'] = 'Settings';
$_lang['polylang_menu_settings_desc'] = '';
$_lang['polylang_menu_lexicon'] = 'Lexicons';
$_lang['polylang_menu_lexicon_desc'] = 'Lexicon management of the "site" topic';
$_lang['polylang_tv_inject_fieldset'] = 'Polylang';
$_lang['polylang_tv_enabled'] = 'TV is available in localizations';
$_lang['polylang_tv_enabled_desc'] = 'An additional field is available for editing in localizations.';
$_lang['polylang_tv_translate'] = 'TV is available for translation in localizations';
$_lang['polylang_tv_translate_desc'] = 'An additional field is available for translation in localizations';
$_lang['polylang_option_inject_fieldset'] = 'Polylang';
$_lang['polylang_option_enabled'] = 'The option is available in localizations.';
$_lang['polylang_option_enabled_desc'] = 'The option is available for editing in localizations.';
$_lang['polylang_option_translate'] = 'The option is available for translation in localizations.';
$_lang['polylang_option_translate_desc'] = 'The option is available for translation in localizations.';
$_lang['polylang_filter'] = 'Filter';
$_lang['polylang_filter_reset'] = 'Reset';
$_lang['polylang_filter_submit'] = 'Submit';
$_lang['polylang_filter_language_group'] = 'Language group:';
$_lang['polylang_filter_language_parent_type'] = 'Parent language:';
$_lang['polylang_language_parent_type_1'] = 'Yes';
$_lang['polylang_language_parent_type_2'] = 'No';

