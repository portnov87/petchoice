<?php
/**
 * LexiconEntries English Lexicon Entries for Polylang
 *
 * @package polylang
 * @subpackage lexicon
 */
$_lang['polylang_lexiconentries_header_name'] = 'Name';
$_lang['polylang_lexiconentries_header_value'] = 'Value';
$_lang['polylang_lexiconentries_header_language'] = 'Language';
$_lang['polylang_lexiconentries_header_editedon'] = 'Modified';
$_lang['polylang_lexiconentries_header_actions'] = 'Actions';
$_lang['polylang_lexiconentries_label_name'] = 'Name';
$_lang['polylang_lexiconentries_label_name_help'] = '';
$_lang['polylang_lexiconentries_label_value'] = 'Value';
$_lang['polylang_lexiconentries_label_value_help'] = '';
$_lang['polylang_lexiconentries_label_language'] = 'Language';
$_lang['polylang_lexiconentries_label_language_help'] = '';
$_lang['polylang_lexiconentries_label_editedon'] = 'Modified';
$_lang['polylang_lexiconentries_label_editedon_help'] = '';
$_lang['polylang_lexiconentries_page_title'] = 'Lexicon management of the "site" topic';
$_lang['polylang_lexiconentries_tab'] = 'Lexicon management';
$_lang['polylang_lexiconentries_intro_msg'] = 'Here you can manage all dictionary entries in the "polylang" namespace and subject "site". <strong>Each dictionary entry will be accessible through the [[%name]] </strong>placeholder.<br> Double click on any entry parameter to change it.';
$_lang['polylang_lexiconentries_clone_lexicon_help'] = 'Select the language to copy the lexicons to.';
$_lang['polylang_lexiconentries_translate_lexicon_help'] = 'Select the language for which you want to translate lexicons.';
$_lang['polylang_lexiconentries_only_dependent_languages'] = 'Show only dependent languages';
$_lang['polylang_lexiconentries_only_independent_languages'] = 'Show only non-independent languages';
$_lang['polylang_lexiconentries_btn_create'] = 'Create a lexicon';
$_lang['polylang_lexiconentries_btn_clone'] = 'Clone';
$_lang['polylang_lexiconentries_btn_translate'] = 'Translate';
$_lang['polylang_lexiconentries_tbar_btn_translate'] = 'Translate all lexicons';
$_lang['polylang_lexiconentries_tbar_btn_clone'] = 'Clone all lexicons into another language';
$_lang['polylang_lexiconentries_menu_update'] = 'Edit lexicon';
$_lang['polylang_lexiconentries_menu_clone'] = 'Clone lexicon into another language';
$_lang['polylang_lexiconentries_menu_multiple_clone'] = 'Clone lexicons into another language';
$_lang['polylang_lexiconentries_menu_translate'] = 'Translate lexicon';
$_lang['polylang_lexiconentries_menu_multiple_translate'] = 'Translate lexicons';
$_lang['polylang_lexiconentries_menu_remove'] = 'Remove lexicon';
$_lang['polylang_lexiconentries_menu_multiple_remove'] = 'Remove lexicons';
$_lang['polylang_lexiconentries_title_win_create'] = 'Create lexicon';
$_lang['polylang_lexiconentries_title_win_update'] = 'Edit lexicon';
$_lang['polylang_lexiconentries_title_win_clone'] = 'Cloning';
$_lang['polylang_lexiconentries_title_win_translate'] = 'Translation';
$_lang['polylang_lexiconentries_title_win_remove'] = 'Remove lexicon';
$_lang['polylang_lexiconentries_confirm_remove'] = 'Are you sure you want to delete this lexicon?';
$_lang['polylang_lexiconentries_confirm_multiple_remove'] = 'Are you sure you want to remove these lexicons?';
$_lang['polylang_lexiconentries_err_save'] = 'An error occurred while trying to save the dictionary entry.';