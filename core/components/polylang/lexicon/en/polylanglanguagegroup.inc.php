<?php
/**
 * PolylangLanguageGroup English Lexicon Entries for Polylang
 *
 * @package polylang
 * @subpackage lexicon
 */
$_lang['polylang_polylanglanguagegroup_header_name'] = 'Name';
$_lang['polylang_polylanglanguagegroup_label_name'] = 'Name';
$_lang['polylang_polylanglanguagegroup_label_name_help'] = '';
$_lang['polylang_polylanglanguagegroup_tab'] = 'Language groups';
$_lang['polylang_polylanglanguagegroup_header_actions'] = 'Actions';
$_lang['polylang_polylanglanguagegroup_btn_create'] = 'Create group';
$_lang['polylang_polylanglanguagegroup_menu_update'] = 'Edit group';
$_lang['polylang_polylanglanguagegroup_menu_remove'] = 'Remove group';
$_lang['polylang_polylanglanguagegroup_menu_multiple_remove'] = 'Remove groups';
$_lang['polylang_polylanglanguagegroup_title_win_create'] = 'Create group';
$_lang['polylang_polylanglanguagegroup_title_win_update'] = 'Edit groups';
$_lang['polylang_polylanglanguagegroup_title_win_remove'] = 'Remove group';
$_lang['polylang_polylanglanguagegroup_confirm_remove'] = 'Are you sure you want to delete this group?';
$_lang['polylang_polylanglanguagegroup_confirm_multiple_remove'] = 'Are you sure you want to delete these groups?';