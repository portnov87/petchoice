<?php
/**
 * Permissions English Lexicon Entries for Polylang
 *
 * @package polylang
 * @subpackage lexicon
 */

$_lang['polylang_menu_settings'] = 'Show the "Settings" item in the menu.';
$_lang['polylang_menu_lexicon'] = 'Show the item "Site lexicon management" in the menu.';

$_lang['polylang_language_view'] = 'Permission to view languages';
$_lang['polylang_language_list'] = 'Permission to list of languages';
$_lang['polylang_language_save'] = 'Permission to save\update languages';
$_lang['polylang_language_clone'] = 'Permission to clone languages';
$_lang['polylang_language_remove'] = 'Permission to remove languages';

$_lang['polylang_language_group_view'] = 'Permission to view language groups';
$_lang['polylang_language_group_list'] = 'Permission to list of language groups';
$_lang['polylang_language_group_save'] = 'Permission to save\update language groups';
$_lang['polylang_language_group_remove'] = 'Permission to remove language groups';

$_lang['polylang_lexicon_view'] = 'Permission to view lexicons';
$_lang['polylang_lexicon_list'] = 'Permission to list of lexicons';
$_lang['polylang_lexicon_save'] = 'Permission to save\update lexicons';
$_lang['polylang_lexicon_clone'] = 'Permission to clone lexicons';
$_lang['polylang_lexicon_translate'] = 'Permission to translation lexicons';
$_lang['polylang_lexicon_remove'] = 'Permission to remove lexicons';

$_lang['polylang_field_view'] = 'Permission to view fields';
$_lang['polylang_field_list'] = 'Permission to list of fields';
$_lang['polylang_field_save'] = 'Permission to save\update fields';
$_lang['polylang_field_remove'] = 'Permission to remove fields';

$_lang['polylang_localization_view'] = 'Permission to view localizations';
$_lang['polylang_localization_list'] = 'Permission to list of localizations';
$_lang['polylang_localization_save'] = 'Permission to save\update localizations';
$_lang['polylang_localization_clone'] = 'Permission to clone localizations';
$_lang['polylang_localization_generate'] = 'Permission to generate localizations';
$_lang['polylang_localization_remove'] = 'Permission to remove localizations';