<?php
/**
 * Copyright 2016 Google Inc. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

namespace Google\Cloud\Core;

use Google\Cloud\Core\Exception\NotFoundException;

/**
 * Provides shared functionality for REST service implementations.
 */
trait RestTrait
{
    use ArrayTrait;
    use JsonTrait;
    use WhitelistTrait;

    /**
     * @var RequestBuilder Builds PSR7 requests from a service definition.
     */
    private $requestBuilder;

    /**
     * @var RequestWrapper Wrapper used to handle sending requests to the
     * JSON API.
     */
    private $requestWrapper;

    /**
     * Sets the request builder.
     *
     * @param RequestBuilder $requestBuilder Builds PSR7 requests from a service
     *        definition.
     */
    public function setRequestBuilder(RequestBuilder $requestBuilder)
    {
        $this->requestBuilder = $requestBuilder;
    }

    /**
     * Sets the request wrapper.
     *
     * @param RequestWrapper $requestWrapper Wrapper used to handle sending
     *        requests to the JSON API.
     */
    public function setRequestWrapper(RequestWrapper $requestWrapper)
    {
        $this->requestWrapper = $requestWrapper;
    }

    /**
     * Get the RequestWrapper.
     *
     * @return RequestWrapper|null
     */
    public function requestWrapper()
    {
        return $this->requestWrapper;
    }

    /**
     * Delivers a request built from the service definition.
     *
     * @param string $resource The resource type used for the request.
     * @param string $method The method used for the request.
     * @param array $options [optional] Options used to build out the request.
     * @param array $whitelisted [optional]
     * @return array
     */
    public function send($resource, $method, array $options = [], $whitelisted = false)
    {
        $requestOptions = $this->pluckArray([
            'restOptions',
            'retries',
            'requestTimeout'
        ], $options);


        try {
            echo '<pre>$requestOptions '.$method;
            print_r($requestOptions);
            echo '</pre>';
            echo '<pre>$options ';
            print_r($options);
            echo '</pre>';


            $urlString=[];
            foreach ($options as $key=>$value)
            {
                if ($key!='key')
                    $urlString[]=$key.'='.$value;
            }

//            $url='https://translation.googleapis.com/language/translate/v2';//?'.implode('&',$urlString);
//            $body=$this->sendRequest($url,$options);

//            echo '<pre>';
//            print_r($body);
//            echo '</pre>';die();
            $body = $this->requestWrapper->send(
                $this->requestBuilder->build($resource, $method, $options),
                $requestOptions
            );

            return json_decode(
                $body->getBody(),
                true
            );
        } catch (NotFoundException $e) {
            echo '<pre>$e';
            print_r($e);
            echo '</pre>';
            if ($whitelisted) {
                throw $this->modifyWhitelistedError($e);
            }

            throw $e;
        }
    }

    private function sendRequest($url,$post_array)
    {
       /* $projectId='adept-comfort-328310';
        $serviceAccountPath='/Users/portnovvit/www/sites/petchoice/wwwroot/adept-comfort-328310-3c568aaae3bf.json';
        # Explicitly use service account credentials by specifying the private key
        # file.
        $config = [
            'keyFilePath' => $serviceAccountPath,
            'projectId' => $projectId,
        ];
        echo 'eeeee';
        $storage = new \Google\Cloud\Storage\StorageClient($config);
        echo '<pre>$storage';
        print_r($storage);
        echo '</pre>';
        die();*/
        $apiKey='3c568aaae3bf279cf89af4e50ae9a3a648b47af4';
        $url = 'https://www.googleapis.com/language/translate/v2?key=' . $apiKey . '&q=' . rawurlencode('hello') . '&source=en&target=fr';
        $handle = curl_init($url);
        curl_setopt($handle, CURLOPT_RETURNTRANSFER, true);

        curl_setopt($handle, CURLOPT_HEADER, 0);
        $header = [
            //$headers['Authorization'] = 'Bearer ' . $this->getToken();
            /*'Authorization: Bearer {
  "type": "service_account",
  "project_id": "adept-comfort-328310",
  "private_key_id": "3c568aaae3bf279cf89af4e50ae9a3a648b47af4",
  "private_key": "-----BEGIN PRIVATE KEY-----\nMIIEvgIBADANBgkqhkiG9w0BAQEFAASCBKgwggSkAgEAAoIBAQCwKeT76QAuhpyf\nRgCH+mP+YWlLHEJlLIS02YI8lyqoZDJBpi+HGkQPFp8eXqAf32dJ7d7UYEQjsb3j\nuuQOPO+9J+lpS57XRB5TLd3qYYQQ6zNj34pE+SUyFOmgLUHpLOD7iJQCAjJqwMjX\nYV9ag2FkvMPpqckiLt0WLbPpQqnMB9Y1ZA4Bp/xdFX15gQXYfl9dKbI0DjE8IB4L\ntsguM5rMOYs67RcqC75NA4Lm8rBEEdDKc707H32Jp/CdX+YU4wc0wCxsXqlnmof8\nikY3MsSgD34uKgq8XnknntGqBQu7Wr6tHrpYb8s8+hh3/09J8U77nYl9bZk+nn2r\n4dOKzHApAgMBAAECggEAAhMe5xfu5cgLEdXNYsyVnzopdyDS6AXMCE85eUTfKT2I\n2VIU5DabJOxYx7md5tIxkWLTntUFaGrgTXghgHNs8AwyHl3YII6Ppt4hlhrmPHFw\nrlVEd8cmuNNwju+Jg66rdaWS91UW9T/mUvcKxvJI1DAO70e4c9vGbyDu/1PG09LM\nzKJjEnQNyVwvRdNI+/2nZ9ltZ85f2Ny4S8YyrSE4tDSrVklRaZ7VgaUeb6qKNNJb\naEXW5L7yp0zxGIaQX5wQ45NkeYT1lnkFlz3PDFpfTRMhmq5d73jklZwe7PKfpYNv\nBG7k6VelY92IWApAW8v2nxYgNzF3xZpTGSLhZZSigQKBgQDbe8IYR6s3M3XuvxdF\nfFewlYzBY2BmIkk2bVf24Mj1GVfnoidpg+PqdP5Qi4n1WqkWKN6OJeQouGsPa/m6\nphb3B1lQiP/Tb/APqVdpq+kz07XAZTNi4XQpDdWuQtqEvMa4XeJ412IMFGMlrMnv\n2LP3W3gg3PTKtoATxF2+74FpIQKBgQDNeQ88CjYucY03RcD9ktUDhbhGXajgv1MQ\nzc1krgYmFQt4tudHEgjwfFM5gQ/+Scu21D9j2MDPSjactllSrdYDlPUv8UR8tp7l\nP8YHxj3n4ZmyIMHTMUOD872StqRVFjYYJ1pTASw9P2P3acYvDoOTFwag2di6fz33\nizS+RdH+CQKBgQDLNsfcASd2oa/0kHs0e88ZES5Us/VCO0Z8HTtPuop13LQ2JD3H\nhTUWFKKO4tOmFv3tJYQCKvUX+sAtdDSxayxiJiy800vCcabcaVck4OW3dbKXY1vz\nlnkdtdB9WyrFsZ9wfoS8i/cc5hdyf4wtBQCD6u3Lx6jis5zcMHj32vt/4QKBgByg\nsFGyLohcA8veqRvrWEhsWhdbt0rQ/VbLBcHwSSUHK5j/CswpJBu1hrbtV3gbegmY\nGtjje0GmWmONOHa/h6DqIRsz51Hk89CHCSVOJpPnJ83cqISvSXPaAuCtF8pPFOxa\nl0gXUr2Z1mjDUPx/bi+RNrg0gfDCB7U0rP2RpsdZAoGBAKCJTgNE6ajwgwOWPujA\nlneWEz7ukWHutqr3VjsYpCBXWwtfJa5NMhWa1d7iU85o7OSUEEuUsF/E2ghCSadL\nxgn/cfCq1pSgB4b8XUE35ijSAu6zljkaUexDB81kK129flq/J5imml52Xj8INDr6\npDnFvCBFiQiIofnBRqGs1aoJ\n-----END PRIVATE KEY-----\n",
  "client_email": "petchoice@adept-comfort-328310.iam.gserviceaccount.com",
  "client_id": "113544588780837116362",
  "auth_uri": "https://accounts.google.com/o/oauth2/auth",
  "token_uri": "https://oauth2.googleapis.com/token",
  "auth_provider_x509_cert_url": "https://www.googleapis.com/oauth2/v1/certs",
  "client_x509_cert_url": "https://www.googleapis.com/robot/v1/metadata/x509/petchoice%40adept-comfort-328310.iam.gserviceaccount.com"
}
',*/
           // 'Authorization: Bearer $(gcloud auth application-default print-access-token)',
            'Content-Type: application/json; charset=utf-8'
        ];
        curl_setopt($handle, CURLOPT_HTTPHEADER, $header);

        /*if($post_array) {
            curl_setopt($handle, CURLOPT_POST, 1 );
            curl_setopt($handle, CURLOPT_POSTFIELDS, $post_array );
        }*/
        $response = curl_exec($handle);

        $http_status = curl_getinfo($handle);//, CURLINFO_HTTP_CODE);
        $curl_errno = curl_errno($handle);



        if (curl_errno($handle)) {
            $error_msg = curl_error($ch);
        }

        echo '<pre>$http_status';
        print_r($http_status);
        echo '</pre>';
        echo '<pre>$curl_errno';
        print_r($curl_errno);
        echo '</pre>';
        echo '<pre>$error_msg';
        print_r($error_msg);
        echo '</pre>';

        //$responseDecoded = json_decode($response, true);
        curl_close($handle);
        return $response;

        /*$google_res = $responseDecoded['data']['translations'][0]['translatedText'];

        print_r($response);

        $url = $this->urlApi . $action . '/';


        if ($ch = curl_init($url)) {

            curl_setopt($ch, CURLOPT_HEADER, 0);
            $header = [
                'Content-Type: application/json'
            ];
            curl_setopt($ch, CURLOPT_HTTPHEADER, $header);


            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 30);

            if (count($postData) > 0) {
                curl_setopt($ch, CURLOPT_POST, 1);
                curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'POST');
                $postdata = json_encode($postData);
                //echo '$postdata duty='.$postdata.'<br/><br/>';
                curl_setopt($ch, CURLOPT_POSTFIELDS, $postdata);
                //curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($postData));

            } else curl_setopt($ch, CURLOPT_POST, 0);


            $response = curl_exec($ch);
            $http_status = curl_getinfo($ch);//, CURLINFO_HTTP_CODE);
            $curl_errno = curl_errno($ch);


            $res = curl_exec($ch);

            if (curl_errno($ch)) {
                $error_msg = curl_error($ch);
            }

            curl_close($ch);
            $res1 = json_decode($res);
            return $res1;
        }
        return false;*/

    }



    /**
     * Return a custom API endpoint in the proper format, or default if none provided.
     *
     * @param string $default
     * @param array $config
     * @return string
     */
    private function getApiEndpoint($default, array $config)
    {
        $res = isset($config['apiEndpoint'])
            ? $config['apiEndpoint']
            : $default;

        if (substr($res, -1) !== '/') {
            $res = $res . '/';
        }

        if (strpos($res, '//') === false) {
            $res = 'https://' . $res;
        }

        return $res;
    }
}
