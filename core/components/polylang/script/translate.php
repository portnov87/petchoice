<?php
/**
 * Polylang
 * @package polylang
 * @var modX $modx
 * @var Polylang $polylang
 * @var PolylangTools $tools
 */

define('MODX_API_MODE', true);
require_once dirname(dirname(dirname(dirname(dirname(__FILE__))))) . '/index.php';

//error_reporting(E_ALL | E_STRICT);
//ini_set('display_errors', true);
//ini_set('error_reporting', E_ALL);
//ini_set('display_errors', 1);
//ini_set('display_startup_errors', 1);


$modx->getService('error', 'error.modError');
$modx->setLogLevel(modX::LOG_LEVEL_ERROR);
$modx->setLogTarget('FILE');
$modx->switchContext('mgr');


$polylang = $modx->getService('polylang', 'Polylang');


//$polylang = $modx->getService('polylang', 'Polylang');
//$tools = $polylang->getTools();
//echo 'after polylang';

$wait = 2000; // microseconds время задержки между обращением к API переводчика
$overwrite = false; // нужно ли делать перевод для уже существующих лексиконов с учетом опции polylang_disallow_translation_completed_field



$classKeys = array(
    //'modDocument',    // обычные ресурсы Modx
    //'Article'
    //'msCategory', // категории miniShop2
    'msProduct', // товар miniShop2
);


$q = $modx->newQuery('modResource');

//$q->innerJoin('modUserProfile', 'modUserProfile', 'modUser.id=modUserProfile.internalKey');

$q->select($modx->getSelectColumns('modResource', 'modResource', ''));//array('*')));
$q->where(array(
    'class_key:IN' => $classKeys,
    //'id'=>'15624'
    //'parent:IN'=>array(15,57)
));

$total = 0;


if ($q->prepare() && $q->stmt->execute()) {

    while ($valueContent = $q->stmt->fetch()){//PDO::FETCH_COLUMN)) {

       // $id
        $id=$valueContent['id'];
//        echo '<pre>';
//        print_r($valueContent);
//        echo '</pre>';
//        die();
        $pagetitle=$valueContent['pagetitle'];
        $longtitle=$valueContent['longtitle'];
        $description=$valueContent['description'];
        $introtext=$valueContent['introtext'];
        $content=$valueContent['content'];
        /** @var modProcessorResponse $response */
        $response = $polylang->runProcessor('mgr/polylangcontent/generate', array(
            'rid' => $id,
            'translate' => true,
            'overwrite' => $overwrite,
        ));

        if ($response->isError()) {
            $err = $response->getMessage();
            $modx->log(modX::LOG_LEVEL_ERROR, $err);
            exit('Error! ' . $err);
        }



//        $tmplvarid=4;
//        $_q = $modx->newQuery('modResource');
//        $_q->select('value');//$modx->getSelectColumns('modResource', 'modResource', '', array('id')));
//        $_q->where(array(
//            'contentid:=' => $id,
//            'tmplvarid:=' => $tmplvarid
//        ));
//        if ($_q->prepare() && $_q->stmt->execute()) {
//
//            while ($_value = $_q->stmt->fetch(PDO::FETCH_COLUMN)) {
//

                $_qPolyTv = $modx->newQuery('PolylangContent');
                $_qPolyTv->select('id');//$modx->getSelectColumns('modResource', 'modResource', '', array('id')));
                $_qPolyTv->where(array(
                    'content_id:=' => $id,
                    'culture_key:=' => 'ua',

                    //'tmplvarid:=' => $tmplvarid
                ));
                $_qPolyTv->prepare();
                $_qPolyTv->stmt->execute();
                $polylangTv_result = $_qPolyTv->stmt->fetchAll(PDO::FETCH_ASSOC);

                if (count($polylangTv_result) > 0) {

                } else {

                    $handler = curl_init();

                    $valuesTranslate=[
                        'pagetitle'=>$pagetitle,
                        'longtitle'=>$longtitle,
                        'description'=>$description,
                        'introtext'=>$introtext,
                        'content'=>$content

                    ];
                    $valuesTranslateResult=[];
                    foreach ($valuesTranslate as $_key=>$_value) {
                        $parametersArray = [];
                        $parametersArray['text'] = $_value;
                        $parametersArray['lang'] = 'ru-uk';
                        $parametersArray['options'] = 0;
                        $parametersArray['key'] = 'trnsl.1.1.20211112T085520Z.d975fd53dffc49ed.ef5e9450bea1163d67d1929ab9b036d063b6ed37';
                        $parametersArray['format'] = 'html';
                        $url = 'https://translate.yandex.net/api/v1.5/tr.json/translate?' . http_build_query($parametersArray);


                        curl_setopt($handler, CURLOPT_URL, $url);
                        curl_setopt($handler, CURLOPT_RETURNTRANSFER, 1);
                        $remoteResult = curl_exec($handler);


                        $result = json_decode($remoteResult, true);

                        $valuesTranslateResult[$_key]=$_value;
                        if (isset($result['text'])) {

                            $valuesTranslateResult[$_key]=$result['text'];
//
//                        echo '<pre>parametersArray';
//                        print_r($parametersArray);
//                        echo '</pre>';

//                        echo '<pre>result ';//.$tmplvarid. ' '.$id;
//                        print_r($result);
//                        echo '</pre>';
//                        die();
                        }else{
                            echo '<pre>result ';//.$tmplvarid. ' '.$id;
                            print_r($result);
                            echo '</pre>';
                            //die();
                        }
                    }

                    if (count($valuesTranslateResult)>0) {
//                        echo '<pre>$valuesTranslateResult';
//                        print_r($valuesTranslateResult);
//                        echo '</pre>';
//
//                        echo '<pre>$valuesTranslate';
//                        print_r($valuesTranslate);
//                        echo '</pre>';
                        $_PolyTv = $modx->newObject('PolylangContent');
                        $_PolyTv->set('culture_key', 'ua');
                        $_PolyTv->set('content_id', $id);
                        $_PolyTv->set('pagetitle', $valuesTranslateResult['pagetitle'][0]);
                        $_PolyTv->set('longtitle', $valuesTranslateResult['longtitle'][0]);
                        $_PolyTv->set('description', $valuesTranslateResult['description'][0]);
                        $_PolyTv->set('introtext', $valuesTranslateResult['introtext'][0]);
                        $_PolyTv->set('content', $valuesTranslateResult['content'][0]);
                        //$_PolyTv->set('value', $result['text'][0]);
                        $_PolyTv->save();
                    }

                }




//
//            }
//        }




        $tmplvarid=4;
        $_q = $modx->newQuery('modTemplateVarResource');
        $_q->select('value');//$modx->getSelectColumns('modResource', 'modResource', '', array('id')));
        $_q->where(array(
            'contentid:=' => $id,
            'tmplvarid:=' => $tmplvarid
        ));
        if ($_q->prepare() && $_q->stmt->execute()) {

            while ($_value = $_q->stmt->fetch(PDO::FETCH_COLUMN)) {


                $_qPolyTv = $modx->newQuery('PolylangTv');
                $_qPolyTv->select('value');//$modx->getSelectColumns('modResource', 'modResource', '', array('id')));
                $_qPolyTv->where(array(
                    'content_id:=' => $id,
                    'culture_key:=' => 'ua',
                    'tmplvarid:=' => $tmplvarid
                ));
                $_qPolyTv->prepare();
                $_qPolyTv->stmt->execute();
                $polylangTv_result = $_qPolyTv->stmt->fetchAll(PDO::FETCH_ASSOC);

                if (count($polylangTv_result) > 0) {

                } else {

                    $handler = curl_init();

                    $parametersArray = [];
                    $parametersArray['text'] = $_value;
                    $parametersArray['lang'] = 'ru-uk';
                    $parametersArray['options'] = 0;
                    $parametersArray['key'] = 'trnsl.1.1.20211112T085520Z.d975fd53dffc49ed.ef5e9450bea1163d67d1929ab9b036d063b6ed37';
                    $parametersArray['format'] = 'html';
                    $url = 'https://translate.yandex.net/api/v1.5/tr.json/translate?' . http_build_query($parametersArray);


                    curl_setopt($handler, CURLOPT_URL, $url);
                    curl_setopt($handler, CURLOPT_RETURNTRANSFER, 1);
                    $remoteResult = curl_exec($handler);


                    $result = json_decode($remoteResult, true);


                    if (isset($result['text'])) {
                        $_PolyTv = $modx->newObject('PolylangTv');
                        $_PolyTv->set('culture_key', 'ua');
                        $_PolyTv->set('content_id', $id);
                        $_PolyTv->set('tmplvarid', $tmplvarid);
                        $_PolyTv->set('value', $result['text'][0]);
                        $_PolyTv->save();
//                        echo '<pre>parametersArray';
//                        print_r($parametersArray);
//                        echo '</pre>';
//
//                        echo '<pre>result '.$tmplvarid. ' '.$id;
//                        print_r($result);
//                        echo '</pre>';
//                        die();
                    }

                }




            }
        }


        $tmplvarid=3;
        $_q = $modx->newQuery('modTemplateVarResource');
        $_q->select('value');//$modx->getSelectColumns('modResource', 'modResource', '', array('id')));
        $_q->where(array(
            'contentid:=' => $id,
            'tmplvarid:=' => $tmplvarid
        ));
        if ($_q->prepare() && $_q->stmt->execute()) {

            while ($_value = $_q->stmt->fetch(PDO::FETCH_COLUMN)) {


                $_qPolyTv = $modx->newQuery('PolylangTv');
                $_qPolyTv->select('value');//$modx->getSelectColumns('modResource', 'modResource', '', array('id')));
                $_qPolyTv->where(array(
                    'content_id:=' => $id,
                    'culture_key:=' => 'ua',
                    'tmplvarid:=' => $tmplvarid
                ));
                $_qPolyTv->prepare();
                $_qPolyTv->stmt->execute();
                $polylangTv_result = $_qPolyTv->stmt->fetchAll(PDO::FETCH_ASSOC);

                if (count($polylangTv_result) > 0) {

                } else {

                    $handler = curl_init();

                    $parametersArray = [];
                    $parametersArray['text'] = $_value;
                    $parametersArray['lang'] = 'ru-uk';
                    $parametersArray['options'] = 0;
                    $parametersArray['key'] = 'trnsl.1.1.20211112T085520Z.d975fd53dffc49ed.ef5e9450bea1163d67d1929ab9b036d063b6ed37';
                    $parametersArray['format'] = 'html';
                    $url = 'https://translate.yandex.net/api/v1.5/tr.json/translate?' . http_build_query($parametersArray);


                    curl_setopt($handler, CURLOPT_URL, $url);
                    curl_setopt($handler, CURLOPT_RETURNTRANSFER, 1);
                    $remoteResult = curl_exec($handler);


                    $result = json_decode($remoteResult, true);


                    if (isset($result['text'])) {
                        $_PolyTv = $modx->newObject('PolylangTv');
                        $_PolyTv->set('culture_key', 'ua');
                        $_PolyTv->set('content_id', $id);
                        $_PolyTv->set('tmplvarid', $tmplvarid);
                        $_PolyTv->set('value', $result['text'][0]);
                        $_PolyTv->save();

                    }

                }




            }
        }


        $tmplvarid=16;
        $_q = $modx->newQuery('modTemplateVarResource');
        $_q->select('value');//$modx->getSelectColumns('modResource', 'modResource', '', array('id')));
        $_q->where(array(
            'contentid:=' => $id,
            'tmplvarid:=' => $tmplvarid
        ));
        if ($_q->prepare() && $_q->stmt->execute()) {

            while ($_value = $_q->stmt->fetch(PDO::FETCH_COLUMN)) {


                $_qPolyTv = $modx->newQuery('PolylangTv');
                $_qPolyTv->select('value');//$modx->getSelectColumns('modResource', 'modResource', '', array('id')));
                $_qPolyTv->where(array(
                    'content_id:=' => $id,
                    'culture_key:=' => 'ua',
                    'tmplvarid:=' => $tmplvarid
                ));
                $_qPolyTv->prepare();
                $_qPolyTv->stmt->execute();
                $polylangTv_result = $_qPolyTv->stmt->fetchAll(PDO::FETCH_ASSOC);

                if (count($polylangTv_result) > 0) {

                } else {

                    $handler = curl_init();

                    $parametersArray = [];
                    $parametersArray['text'] = $_value;
                    $parametersArray['lang'] = 'ru-uk';
                    $parametersArray['options'] = 0;
                    $parametersArray['key'] = 'trnsl.1.1.20211112T085520Z.d975fd53dffc49ed.ef5e9450bea1163d67d1929ab9b036d063b6ed37';
                    $parametersArray['format'] = 'html';
                    $url = 'https://translate.yandex.net/api/v1.5/tr.json/translate?' . http_build_query($parametersArray);


                    curl_setopt($handler, CURLOPT_URL, $url);
                    curl_setopt($handler, CURLOPT_RETURNTRANSFER, 1);
                    $remoteResult = curl_exec($handler);


                    $result = json_decode($remoteResult, true);


                    if (isset($result['text'])) {
                        $_PolyTv = $modx->newObject('PolylangTv');
                        $_PolyTv->set('culture_key', 'ua');
                        $_PolyTv->set('content_id', $id);
                        $_PolyTv->set('tmplvarid', $tmplvarid);
                        $_PolyTv->set('value', $result['text'][0]);
                        $_PolyTv->save();

                    }

                }




            }
        }



        $tmplvarid=21;
        $_q = $modx->newQuery('modTemplateVarResource');
        $_q->select('value');//$modx->getSelectColumns('modResource', 'modResource', '', array('id')));
        $_q->where(array(
            'contentid:=' => $id,
            'tmplvarid:=' => $tmplvarid
        ));
        if ($_q->prepare() && $_q->stmt->execute()) {

            while ($_value = $_q->stmt->fetch(PDO::FETCH_COLUMN)) {


                $_qPolyTv = $modx->newQuery('PolylangTv');
                $_qPolyTv->select('value');//$modx->getSelectColumns('modResource', 'modResource', '', array('id')));
                $_qPolyTv->where(array(
                    'content_id:=' => $id,
                    'culture_key:=' => 'ua',
                    'tmplvarid:=' => $tmplvarid
                ));
                $_qPolyTv->prepare();
                $_qPolyTv->stmt->execute();
                $polylangTv_result = $_qPolyTv->stmt->fetchAll(PDO::FETCH_ASSOC);

                if (count($polylangTv_result) > 0) {

                } else {

                    $handler = curl_init();

                    $parametersArray = [];
                    $parametersArray['text'] = $_value;
                    $parametersArray['lang'] = 'ru-uk';
                    $parametersArray['options'] = 0;
                    $parametersArray['key'] = 'trnsl.1.1.20211112T085520Z.d975fd53dffc49ed.ef5e9450bea1163d67d1929ab9b036d063b6ed37';
                    $parametersArray['format'] = 'html';
                    $url = 'https://translate.yandex.net/api/v1.5/tr.json/translate?' . http_build_query($parametersArray);


                    curl_setopt($handler, CURLOPT_URL, $url);
                    curl_setopt($handler, CURLOPT_RETURNTRANSFER, 1);
                    $remoteResult = curl_exec($handler);


                    $result = json_decode($remoteResult, true);


                    if (isset($result['text'])) {
                        $_PolyTv = $modx->newObject('PolylangTv');
                        $_PolyTv->set('culture_key', 'ua');
                        $_PolyTv->set('content_id', $id);
                        $_PolyTv->set('tmplvarid', $tmplvarid);
                        $_PolyTv->set('value', $result['text'][0]);
                        $_PolyTv->save();

                    }

                }




            }
        }



        $total++;
        usleep($wait);


    }
}

function getTranslateUkr($textRus)
{
    $handler = curl_init();
    $parametersArray = [];
    $parametersArray['text'] = $textRus;
    $parametersArray['lang'] = 'ru-uk';
    $parametersArray['options'] = 0;
    $parametersArray['key'] = 'trnsl.1.1.20211112T085520Z.d975fd53dffc49ed.ef5e9450bea1163d67d1929ab9b036d063b6ed37';
    $parametersArray['format'] = 'html';
    $url = 'https://translate.yandex.net/api/v1.5/tr.json/translate?' . http_build_query($parametersArray);


    curl_setopt($handler, CURLOPT_URL, $url);
    curl_setopt($handler, CURLOPT_RETURNTRANSFER, 1);
    $remoteResult = curl_exec($handler);


    $result = json_decode($remoteResult, true);
    return $result;
}

echo "Done! Total translate: {$total}\n";