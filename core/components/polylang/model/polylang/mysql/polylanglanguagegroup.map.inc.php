<?php
$xpdo_meta_map['PolylangLanguageGroup']= array (
  'package' => 'polylang',
  'version' => '1.1',
  'table' => 'polylang_language_group',
  'extends' => 'xPDOSimpleObject',
  'tableMeta' => 
  array (
    'engine' => 'InnoDB',
  ),
  'fields' => 
  array (
    'name' => NULL,
  ),
  'fieldMeta' => 
  array (
    'name' => 
    array (
      'dbtype' => 'varchar',
      'precision' => '255',
      'phptype' => 'string',
      'null' => false,
    ),
  ),
  'aggregates' => 
  array (
    'LanguageGroupMember' => 
    array (
      'class' => 'PolylangLanguageGroupMember',
      'local' => 'id',
      'foreign' => 'group_id',
      'cardinality' => 'many',
      'owner' => 'local',
    ),
  ),
);
