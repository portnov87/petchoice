<?php
$xpdo_meta_map['PolylangLanguage']= array (
  'package' => 'polylang',
  'version' => '1.1',
  'table' => 'polylang_language',
  'extends' => 'xPDOSimpleObject',
  'tableMeta' => 
  array (
    'engine' => 'InnoDB',
  ),
  'fields' => 
  array (
    'currency_id' => 0,
    'name' => NULL,
    'description' => NULL,
    'culture_key' => NULL,
    'site_url' => NULL,
    'parent' => 0,
    'rank' => 0,
    'rank_translation' => 0,
    'active' => 1,
  ),
  'fieldMeta' => 
  array (
    'currency_id' => 
    array (
      'dbtype' => 'int',
      'precision' => '11',
      'phptype' => 'integer',
      'null' => false,
      'default' => 0,
    ),
    'name' => 
    array (
      'dbtype' => 'varchar',
      'precision' => '100',
      'phptype' => 'string',
      'null' => false,
    ),
    'description' => 
    array (
      'dbtype' => 'varchar',
      'precision' => '255',
      'phptype' => 'string',
      'null' => true,
    ),
    'culture_key' => 
    array (
      'dbtype' => 'varchar',
      'precision' => '100',
      'phptype' => 'string',
      'null' => false,
      'index' => 'index',
    ),
    'site_url' => 
    array (
      'dbtype' => 'varchar',
      'precision' => '100',
      'phptype' => 'string',
      'null' => false,
    ),
    'parent' => 
    array (
      'dbtype' => 'int',
      'precision' => '11',
      'phptype' => 'integer',
      'null' => true,
      'default' => 0,
    ),
    'rank' => 
    array (
      'dbtype' => 'int',
      'precision' => '11',
      'phptype' => 'integer',
      'null' => false,
      'default' => 0,
      'index' => 'index',
    ),
    'rank_translation' => 
    array (
      'dbtype' => 'int',
      'precision' => '11',
      'phptype' => 'integer',
      'null' => false,
      'default' => 0,
      'index' => 'index',
    ),
    'active' => 
    array (
      'dbtype' => 'tinyint',
      'precision' => '1',
      'phptype' => 'boolean',
      'null' => false,
      'default' => 1,
      'index' => 'index',
    ),
  ),
  'indexes' => 
  array (
    'active' => 
    array (
      'alias' => 'active',
      'primary' => false,
      'unique' => false,
      'type' => 'BTREE',
      'columns' => 
      array (
        'active' => 
        array (
          'length' => '',
          'collation' => 'A',
          'null' => false,
        ),
      ),
    ),
    'rank' => 
    array (
      'alias' => 'rank',
      'primary' => false,
      'unique' => false,
      'type' => 'BTREE',
      'columns' => 
      array (
        'rank' => 
        array (
          'length' => '',
          'collation' => 'A',
          'null' => false,
        ),
      ),
    ),
    'rank_translation' => 
    array (
      'alias' => 'rank_translation',
      'primary' => false,
      'unique' => false,
      'type' => 'BTREE',
      'columns' => 
      array (
        'rank_translation' => 
        array (
          'length' => '',
          'collation' => 'A',
          'null' => false,
        ),
      ),
    ),
    'culture_key' => 
    array (
      'alias' => 'culture_key',
      'primary' => false,
      'unique' => false,
      'type' => 'BTREE',
      'columns' => 
      array (
        'culture_key' => 
        array (
          'length' => '',
          'collation' => 'A',
          'null' => false,
        ),
      ),
    ),
  ),
  'composites' => 
  array (
    'LanguageGroupMember' => 
    array (
      'class' => 'PolylangLanguageGroupMember',
      'local' => 'id',
      'foreign' => 'language_id',
      'cardinality' => 'many',
      'owner' => 'local',
    ),
  ),
  'aggregates' => 
  array (
    'MultiCurrency' => 
    array (
      'class' => 'MultiCurrency',
      'local' => 'currency_id',
      'foreign' => 'id',
      'cardinality' => 'one',
      'owner' => 'foreign',
    ),
    'PolylangContent' => 
    array (
      'class' => 'PolylangContent',
      'local' => 'culture_key',
      'foreign' => 'culture_key',
      'cardinality' => 'many',
      'owner' => 'local',
    ),
    'PolylangTvTmplvars' => 
    array (
      'class' => 'PolylangTvTmplvars',
      'local' => 'culture_key',
      'foreign' => 'culture_key',
      'cardinality' => 'many',
      'owner' => 'local',
    ),
    'PolylangTv' => 
    array (
      'class' => 'PolylangTv',
      'local' => 'culture_key',
      'foreign' => 'culture_key',
      'cardinality' => 'many',
      'owner' => 'local',
    ),
    'PolylangProductOption' => 
    array (
      'class' => 'PolylangProductOption',
      'local' => 'culture_key',
      'foreign' => 'culture_key',
      'cardinality' => 'many',
      'owner' => 'local',
    ),
    'PolylangProduct' => 
    array (
      'class' => 'PolylangProduct',
      'local' => 'culture_key',
      'foreign' => 'culture_key',
      'cardinality' => 'many',
      'owner' => 'local',
    ),
  ),
);
