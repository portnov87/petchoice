<?php
$xpdo_meta_map['PolylangLanguageGroupMember']= array (
  'package' => 'polylang',
  'version' => '1.1',
  'table' => 'polylang_language_group_member',
  'extends' => 'xPDOObject',
  'tableMeta' => 
  array (
    'engine' => 'InnoDB',
  ),
  'fields' => 
  array (
    'language_id' => NULL,
    'group_id' => NULL,
  ),
  'fieldMeta' => 
  array (
    'language_id' => 
    array (
      'dbtype' => 'int',
      'precision' => '11',
      'attributes' => 'unsigned',
      'phptype' => 'integer',
      'null' => false,
      'index' => 'pk',
    ),
    'group_id' => 
    array (
      'dbtype' => 'int',
      'precision' => '11',
      'attributes' => 'unsigned',
      'phptype' => 'integer',
      'null' => false,
      'index' => 'pk',
    ),
  ),
  'indexes' => 
  array (
    'PRIMARY' => 
    array (
      'alias' => 'PRIMARY',
      'primary' => true,
      'unique' => true,
      'type' => 'BTREE',
      'columns' => 
      array (
        'language_id' => 
        array (
          'length' => '',
          'collation' => 'A',
          'null' => false,
        ),
        'group_id' => 
        array (
          'length' => '',
          'collation' => 'A',
          'null' => false,
        ),
      ),
    ),
  ),
  'aggregates' => 
  array (
    'LanguageGroup' => 
    array (
      'class' => 'PolylangLanguageGroup',
      'local' => 'group_id',
      'foreign' => 'id',
      'cardinality' => 'one',
      'owner' => 'foreign',
    ),
    'Language' => 
    array (
      'class' => 'PolylangLanguage',
      'local' => 'language_id',
      'foreign' => 'id',
      'cardinality' => 'one',
      'owner' => 'foreign',
    ),
  ),
);
