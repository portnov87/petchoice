<?php

$xpdo_meta_map = array (
  'xPDOSimpleObject' => 
  array (
    0 => 'PolylangContent',
    1 => 'PolylangField',
    2 => 'PolylangLanguage',
    3 => 'PolylangLanguageGroup',
    4 => 'PolylangProduct',
    5 => 'PolylangTv',
    6 => 'PolylangTvTmplvars',
  ),
  'xPDOObject' => 
  array (
    0 => 'PolylangLanguageGroupMember',
    1 => 'PolylangProductOption',
  ),
);