<?php

class PolylangLanguageToggleProcessor extends modObjectProcessor
{

    public function process()
    {
        /** @var  Polylang $polylang */
        $polylang = $this->modx->getService('polylang', 'Polylang');
        $id = $this->getProperty('id', 0);
        $language = $this->modx->getObject('PolylangLanguage', $id);
        if ($language) {
            $this->modx->setOption('cultureKey',$language->get('culture_key'));
            $polylang->getTools()->invokeEvent('OnTogglePolylangLanguage', array(
                'tools' => $polylang->getTools(),
                'language' => $language,
            ));
        } else {
            $this->modx->log(modX::LOG_LEVEL_ERROR, 'Language not found by ID:' . $id);
        }
        return $polylang->success();
    }

}

return 'PolylangLanguageToggleProcessor';