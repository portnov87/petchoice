<?php

class PolylangPolylangLanguageGetProcessor extends modObjectGetProcessor
{
    public $classKey = 'PolylangLanguage';
    public $languageTopics = array('polylang:default');
    public $permission = 'polylang_language_view';
    /** @var Polylang $polylang */
    public $polylang;

    public function initialize()
    {
        if (!$this->modx->hasPermission($this->permission)) {
            return $this->modx->lexicon('access_denied');
        }
        $this->polylang = $this->modx->getService('polylang', 'Polylang');
        return parent::initialize();
    }

    /**
     * Return the response
     * @return array
     */
    public function cleanup()
    {
        $data = $this->object->toArray();
        $languageGroup = $this->polylang->getTools()->getLanguageGroupField($data['id']);
        $data['language_group'] = implode(',', $languageGroup);
        return $this->success('', $data);
    }

}

return 'PolylangPolylangLanguageGetProcessor';