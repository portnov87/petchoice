<?php

class PolylangPolylangLanguageUpdateProcessor extends modObjectUpdateProcessor
{
    public $classKey = 'PolylangLanguage';
    public $languageTopics = array('polylang:default');
    public $beforeSaveEvent = 'OnBeforeSavePolylangLanguage';
    public $afterSaveEvent = 'OnSavePolylangLanguage';
    public $permission = 'polylang_language_save';
    /** @var Polylang $polylang */
    public $polylang;
    protected $oldCultureKey;

    public function initialize()
    {
        if (!$this->modx->hasPermission($this->permission)) {
            return $this->modx->lexicon('access_denied');
        }
        $this->polylang = $this->modx->getService('polylang', 'Polylang');
        return parent::initialize();
    }

    public function beforeSet()
    {
        $parent = $this->getProperty('parent', 0);
        if ($parent && $parent == $this->object->get('id')) {
            $this->unsetProperty('parent');
        }
        return parent::beforeSet();
    }

    public function beforeSave()
    {
        $id = $this->object->get('id');
        $key = $this->getProperty('culture_key', '');
        $siteUrl = $this->getProperty('site_url', '');
        $this->oldCultureKey = $this->object->get('culture_key');
        if ($this->modx->getCount($this->classKey, array('culture_key' => $key, 'site_url' => $siteUrl, 'id:!=' => $id))) {
            $this->addFieldError('culture_key', $this->modx->lexicon('polylang_err_culture_key_exists'));
        }
        return !$this->hasErrors();
    }

    public function afterSave()
    {
        $saved = parent::afterSave();
        if ($saved) {
            $id = $this->object->get('id');
            $key = $this->object->get('culture_key');
            if ($this->oldCultureKey != $key) {
                $list = array('PolylangResource', 'PolylangProduct', 'PolylangTv');
                foreach ($list as $className) {
                    $q = $this->modx->newQuery($className);
                    $q->command('UPDATE');
                    $q->query['set']['culture_key'] = array(
                        'value' => $key,
                        'type' => true,
                    );
                    $q->where(array('culture_key' => $this->oldCultureKey));
                    if ($q->prepare()) {
                        if (!$q->stmt->execute()) {
                            $this->modx->log(modX::LOG_LEVEL_ERROR, print_r($q->stmt->errorInfo(), true) . ' SQL: ' . $q->toSQL());
                        }
                    }
                }
            }
            $languageGroup = $this->getProperty('language_group');
            if ($languageGroup) {
                $this->polylang->getTools()->addLanguageInGroups($id, $languageGroup);
            }
        }
        return $saved;
    }


}

return 'PolylangPolylangLanguageUpdateProcessor';