<?php

class PolylangPolylangLanguageGetListProcessor extends modObjectGetListProcessor
{
    public $languageTopics = array('polylang:default');
    public $classKey = 'PolylangLanguage';
    public $defaultSortField = 'rank';
    public $defaultSortDirection = 'ASC';
    public $permission = 'polylang_language_list';
    /** @var Polylang $polylang */
    public $polylang;
    /** @var PolylangTools $tools */
    public $tools = null;

    public function initialize()
    {
        if (!$this->modx->hasPermission($this->permission)) {
            return $this->modx->lexicon('access_denied');
        }
        $this->polylang = $this->modx->getService('polylang', 'Polylang');
        $this->tools = $this->polylang->getTools();
        return parent::initialize();
    }

    public function prepareQueryBeforeCount(xPDOQuery $c)
    {
        $query = $this->getProperty('query');
        $combo = $this->getProperty('combo');
        $onlyRoot = $this->getProperty('onlyRoot', 0);
        $onlyActive = $this->getProperty('onlyActive', 0);
        $languageGroup = $this->getProperty('language_group');
        $languageParentType = $this->getProperty('language_parent_type');
        $defaultLanguage = $this->tools->getDefaultLanguage();
        $excludeDefaultLanguage = $this->getProperty('excludeDefaultLanguage', 0);
        $exclude = $this->getProperty('exclude', '');
        $exclude = $this->tools->explodeAndClean($exclude);

        $c->leftJoin('PolylangLanguageGroupMember', 'LanguageGroupMember', '`LanguageGroupMember`.`language_id` = `PolylangLanguage`.`id`');

        if ($combo && $excludeDefaultLanguage) {
            $c->where(array(
                '`culture_key`:!=' => $defaultLanguage,
            ));
        }
        if (!empty($query)) {
            $c->where(array(
                '`name`:LIKE' => '%' . $query . '%',
                'OR:`description`:LIKE' => '%' . $query . '%',
                'OR:`culture_key`:LIKE' => '%' . $query . '%'
            ));
        }
        if ($onlyActive) {
            $c->where(array(
                '`active`' => 1,
            ));
        }
        if ($onlyRoot) {
            $c->where(array(
                '`parent`' => 0,
            ));
        }
        if ($exclude) {
            $c->where(array(
                '`culture_key`:NOT IN' => $exclude
            ));
        }
        if (!empty($languageGroup)) {
            $c->where(array(
                '`LanguageGroupMember`.`group_id`' => $languageGroup
            ));

        }
        if (!empty($languageParentType)) {
            $operator = $languageParentType == 1 ? ':!=' : '';
            $c->where(array(
                "`parent`{$operator}" => 0
            ));
        }
        if ($combo) {
            $c->groupby('`culture_key`');
        }

        return $c;
    }

    public function prepareRow(xPDOObject $object)
    {
        $data = $object->toArray();
        $languageGroup = $this->polylang->getTools()->getLanguageGroupField($data['id'], 'name');
        $data['language_group_name'] = implode(',', $languageGroup);
        if (!$this->getProperty('combo')) {
            $data['actions'] = array(
                array(
                    'cls' => array(
                        'menu' => 'green',
                        'button' => 'green',
                    ),
                    'icon' => 'icon icon-edit',
                    'title' => $this->modx->lexicon('polylang_menu_update'),
                    'action' => 'updateItem',
                    'button' => true,
                    'menu' => true,
                ), array(
                    'cls' => array(
                        'menu' => 'blue',
                        'button' => 'blue',
                    ),
                    'icon' => 'icon icon-clone',
                    'title' => $this->modx->lexicon('polylang_menu_clone'),
                    'multiple' => $this->modx->lexicon('polylang_menu_clone'),
                    'action' => 'cloneItem',
                    'button' => true,
                    'menu' => true,
                ), array(
                    'cls' => array(
                        'menu' => 'purple',
                        'button' => 'purple',
                    ),
                    'icon' => 'icon icon-pencil',
                    'title' => $this->modx->lexicon('polylang_menu_edit_lexicon'),
                    'action' => 'editLexicon',
                    'button' => true,
                    'menu' => true,
                )
            );
            if (!$data['active']) {
                $data['actions'][] = array(
                    'cls' => array(
                        'menu' => 'yellow',
                        'button' => 'yellow',
                    ),
                    'icon' => 'icon icon-power-off',
                    'title' => $this->modx->lexicon('polylang_menu_enable'),
                    'multiple' => $this->modx->lexicon('polylang_menu_multiple_enable'),
                    'action' => 'enableItem',
                    'button' => true,
                    'menu' => true,
                );
            } else {
                $data['actions'][] = array(
                    'cls' => array(
                        'menu' => 'gray',
                        'button' => 'gray',
                    ),
                    'icon' => 'icon icon-power-off',
                    'title' => $this->modx->lexicon('polylang_menu_disable'),
                    'multiple' => $this->modx->lexicon('polylang_menu_multiple_disable'),
                    'action' => 'disableItem',
                    'button' => true,
                    'menu' => true,
                );
            }
        }
        $data['actions'][] = array(
            'cls' => array(
                'menu' => 'red',
                'button' => 'red',
            ),
            'icon' => 'icon icon-trash-o',
            'title' => $this->modx->lexicon('polylang_menu_remove'),
            'multiple' => $this->modx->lexicon('polylang_menu_multiple_remove'),
            'action' => 'removeItem',
            'button' => true,
            'menu' => true,
        );

        return $data;
    }
}

return 'PolylangPolylangLanguageGetListProcessor';