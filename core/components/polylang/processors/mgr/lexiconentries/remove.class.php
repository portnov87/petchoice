<?php

class PolylangLexiconEntriesRemoveProcessor extends modObjectRemoveProcessor
{
    public $classKey = 'LexiconEntries';
    public $languageTopics = array('polylang:default');
    public $permission = 'polylang_lexicon_remove';
    /** @var Polylang $polylang */
    public $polylang;

    public function initialize()
    {
        if (!$this->modx->hasPermission($this->permission)) {
            return $this->modx->lexicon('access_denied');
        }
        // $this->polylang = $this->modx->getService('polylang', 'Polylang');
        return parent::initialize();
    }

}

return 'PolylangLexiconEntriesRemoveProcessor';