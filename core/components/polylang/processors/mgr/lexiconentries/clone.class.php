<?php

class PolylangLexiconEntriesCloneProcessor extends modProcessor
{
    public $classKey = 'LexiconEntries';
    public $permission = 'polylang_lexicon_clone';
    /** @var Polylang $polylang */
    public $polylang;
    /** @var PolylangTools $tools */
    public $tools;

    public function initialize()
    {
        if (!$this->modx->hasPermission($this->permission)) {
            return $this->modx->lexicon('access_denied');
        }
        $initialized = parent::initialize();
        if ($initialized) {
            $this->polylang = $this->modx->getService('polylang', 'Polylang');
            $this->tools = $this->polylang->getTools();
            $this->setDefaultProperties(array(
                'namespace' => 'polylang',
                'topic' => 'site',
            ));
        }
        return $initialized;
    }

    public function getLanguageTopics()
    {
        return array('polylang:default');
    }

    public function process()
    {
        $lexicons = array();
        $data = $this->getData();
        $all = $this->getProperty('all');
        $languages = $this->getProperty('languages');
        $skipExisting = $this->getProperty('skip_existing',true);
        if (!$all) {
            $lexicons = $this->getProperty('lexicons');
            $lexicons = $this->tools->fromJSON($lexicons, array());
            if (empty($lexicons)) {
                return $this->failure();
            }
        }
        if ($data) {
            foreach ($data as $item) {
                if ($skipExisting && $lexicons && !in_array($item['name'], $lexicons)) {
                    continue;
                }
                $lexicon = array(
                    'name' => $item['name'],
                    'value' => $item['value'],
                    'topic' => $item['topic'],
                    'namespace' => $item['namespace'],
                );
                foreach ($languages as $key => $val) {
                    if (!$val) continue;
                    $lexicon['language'] = $key;
                    $this->copy($lexicon);
                }
            }
        }
        return $this->success();
    }

    /**
     * @param array $lexicon
     */
    public function copy(array $lexicon)
    {
        $entry = $this->modx->getObject('modLexiconEntry', $lexicon);
        if ($entry) {
            $entry->set('editedon', date('Y-m-d H:i:s'));
        } else {
            $entry = $this->modx->newObject('modLexiconEntry', $lexicon);
            $entry->set('createdon', date('Y-m-d H:i:s'));
        }
        if ($entry->save()) {
            $entry->clearCache();
        } else {
            $this->modx->log(modX::LOG_LEVEL_ERROR, 'Error save lexicon:' . print_r($lexicon, 1));
        }
    }

    /**
     * @return array
     */
    public function getData()
    {
        $data = array();
        $language = $this->getProperty('language');
        /** @var modProcessorResponse $response */
        $response = $this->polylang->runProcessor('mgr/lexiconentries/getlist', array(
            'limit' => 10000,
            'language' => $language,
        ));
        if ($response->isError()) {
            $this->modx->log(modX::LOG_LEVEL_ERROR, $response->getMessage());
        } else {
            $response = $this->tools->fromJSON($response->getResponse(), array());
            if ($response && !empty($response['results'])) {
                $data = $response['results'];
            }
        }
        return $data;
    }

}

return 'PolylangLexiconEntriesCloneProcessor';