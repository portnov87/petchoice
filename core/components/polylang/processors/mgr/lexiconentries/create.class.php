<?php

class PolylangLexiconEntriesCreateProcessor extends modObjectCreateProcessor
{
    public $classKey = 'modLexiconEntry';
    public $languageTopics = array('polylang:default');
    public $permission = 'polylang_lexicon_save';
    /** @var Polylang $polylang */
    public $polylang;

    public function initialize()
    {
        if (!$this->modx->hasPermission($this->permission)) {
            return $this->modx->lexicon('access_denied');
        }
        // $this->polylang = $this->modx->getService('polylang', 'Polylang');
        $this->setDefaultProperties(array(
            'namespace' => 'polylang',
            'topic' => 'site',
            'editedon' => date('Y-m-d h:i:s')
        ));
        return parent::initialize();
    }

    /**
     * @return bool
     */
    public function alreadyExists()
    {
        return $this->modx->getCount($this->classKey, array(
                'name' => $this->getProperty('name'),
                'namespace' => $this->getProperty('namespace'),
                'language' => $this->getProperty('language'),
                'topic' => $this->getProperty('topic'),
            )) > 0;
    }

}

return 'PolylangLexiconEntriesCreateProcessor';