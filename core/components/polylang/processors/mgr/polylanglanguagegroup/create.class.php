<?php

class PolylangPolylangLanguageGroupCreateProcessor extends modObjectCreateProcessor
{
    public $classKey = 'PolylangLanguageGroup';
    public $languageTopics = array('polylang:default');
    public $permission = 'polylang_language_group_save';
    /** @var Polylang $polylang */
    public $polylang;

    public function initialize()
    {
        if (!$this->modx->hasPermission($this->permission)) {
            return $this->modx->lexicon('access_denied');
        }
        // $this->polylang = $this->modx->getService('polylang', 'Polylang');
        return parent::initialize();
    }
}

return 'PolylangPolylangLanguageGroupCreateProcessor';