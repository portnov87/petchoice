<?php

class PolylangPolylangLanguageGroupRemoveProcessor extends modObjectRemoveProcessor
{
    public $classKey = 'PolylangLanguageGroup';
    public $languageTopics = array('polylang:default');
    public $permission = 'polylang_language_group_remove';
    /** @var Polylang $polylang */
    public $polylang;

    public function initialize()
    {
        if (!$this->modx->hasPermission($this->permission)) {
            return $this->modx->lexicon('access_denied');
        }
        // $this->polylang = $this->modx->getService('polylang', 'Polylang');
        return parent::initialize();
    }

    public function afterRemove()
    {

        /*$sql = "UPDATE {$this->modx->getTableName($this->classKey)} SET `rank`=`rank`-1 WHERE `rank`>{$this->object->get('rank')}
        // AND  parent_id = {$this->object->get('parent_id')
        ";
        $this->modx->exec($sql);*/

        return parent::afterRemove();
    }

}

return 'PolylangPolylangLanguageGroupRemoveProcessor';