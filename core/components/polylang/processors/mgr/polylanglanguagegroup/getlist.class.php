<?php

class PolylangPolylangLanguageGroupGetListProcessor extends modObjectGetListProcessor
{
    public $languageTopics = array('polylang:default');
    public $classKey = 'PolylangLanguageGroup';
    public $defaultSortField = 'id';
    public $defaultSortDirection = 'ASC';
    public $permission = 'polylang_language_group_list';
    /** @var Polylang $polylang */
    public $polylang;

    public function initialize()
    {
        if (!$this->modx->hasPermission($this->permission)) {
            return $this->modx->lexicon('access_denied');
        }
        // $this->polylang = $this->modx->getService('polylang', 'Polylang');
        return parent::initialize();
    }

    public function prepareQueryBeforeCount(xPDOQuery $c)
    {
        $query = $this->getProperty('query');

        if (!empty($query)) {
            $c->where(array(
                'name:LIKE' => '%' . $query . '%',
            ));
        }
        return $c;
    }

    public function prepareRow(xPDOObject $object)
    {
        $data = $object->toArray();

        if (!$this->getProperty('combo')) {
            $data['actions'] = array(
                array(
                    'cls' => array(
                        'menu' => 'green',
                        'button' => 'green',
                    ),
                    'icon' => 'icon icon-edit',
                    'title' => $this->modx->lexicon('polylang_polylanglanguagegroup_menu_update'),
                    'action' => 'updateItem',
                    'button' => true,
                    'menu' => true,
                ),
                array(
                    'cls' => array(
                        'menu' => 'red',
                        'button' => 'red',
                    ),
                    'icon' => 'icon icon-trash-o',
                    'title' => $this->modx->lexicon('polylang_polylanglanguagegroup_menu_remove'),
                    'multiple' => $this->modx->lexicon('polylang_polylanglanguagegroup_menu_multiple_remove'),
                    'action' => 'removeItem',
                    'button' => true,
                    'menu' => true,
                ),
            );
        }

        return $data;
    }
}

return 'PolylangPolylangLanguageGroupGetListProcessor';