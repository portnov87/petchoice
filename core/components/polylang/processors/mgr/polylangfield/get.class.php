<?php

class PolylangPolylangFieldGetProcessor extends modObjectGetProcessor
{
    public $classKey = 'PolylangField';
    public $languageTopics = array('polylang:default');
    public $permission = 'polylang_field_view';
    /** @var Polylang $polylang */
    public $polylang;

    public function initialize()
    {
        if (!$this->modx->hasPermission($this->permission)) {
            return $this->modx->lexicon('access_denied');
        }
        // $this->polylang = $this->modx->getService('polylang', 'Polylang');
        return parent::initialize();
    }

}

return 'PolylangPolylangFieldGetProcessor';