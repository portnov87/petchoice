<?php

class PolylangPolylangFieldCreateProcessor extends modObjectCreateProcessor
{
    public $classKey = 'PolylangField';
    public $languageTopics = array('polylang:default');
    public $permission = 'polylang_field_save';
    /** @var Polylang $polylang */
    public $polylang;

    public function initialize()
    {
        if (!$this->modx->hasPermission($this->permission)) {
            return $this->modx->lexicon('access_denied');
        }
        $this->polylang = $this->modx->getService('polylang', 'Polylang');
        return parent::initialize();
    }


    public function afterSave()
    {
        $cacheKey = $this->polylang->getTools()->getCacheKey($this->object->getCacheKey());
        $this->modx->cacheManager->delete($cacheKey);
        $this->modx->cacheManager->refresh(array('resource' => array()));
        return parent::afterSave();
    }
}

return 'PolylangPolylangFieldCreateProcessor';