<?php

class PolylangPolylangContentGetListProcessor extends modObjectGetListProcessor
{
    public $languageTopics = array('polylang:default');
    public $classKey = 'PolylangContent';
    public $defaultSortField = 'Language.rank';
    public $defaultSortDirection = 'ASC';
    public $permission = 'polylang_localization_list';
    /** @var Polylang $polylang */
    public $polylang;
    /** @var int $contentId */
    protected $contentId = 0;
    /** @var int $totalLanguages */
    protected $totalLanguages = 0;
    /** @var int $contentLanguages */
    protected $contentLanguages = 0;

    public function initialize()
    {
        if (!$this->modx->hasPermission($this->permission)) {
            return $this->modx->lexicon('access_denied');
        }
        $this->polylang = $this->modx->getService('polylang', 'Polylang');
        $this->contentId = $this->getProperty('content_id', 0);
        return parent::initialize();
    }

    public function prepareQueryBeforeCount(xPDOQuery $c)
    {
        $query = $this->getProperty('query');
        $languageGroup = $this->getProperty('language_group');
        $languageParentType = $this->getProperty('language_parent_type');
        $c->leftJoin('PolylangLanguage', 'Language', '`Language`.`culture_key` = `PolylangContent`.`culture_key`');
        $c->leftJoin('PolylangLanguageGroupMember', 'LanguageGroupMember', '`LanguageGroupMember`.`language_id` = `Language`.`id`');
        $c->leftJoin('modUser', 'User', "`User`.`id` = `{$this->classKey}`.`editedby`");
        $c->select($this->modx->getSelectColumns($this->classKey, $this->classKey));
        $c->select($this->modx->getSelectColumns('modUser', 'User', '', array('username')));
        $c->select($this->modx->getSelectColumns('PolylangLanguage', 'Language', 'language_', array('id', 'name')));

        $c->where(array(
            $this->classKey . '.`content_id`' => $this->contentId
        ));

        if (!empty($query)) {
            $c->where(array(
                '`Language`.`name`:LIKE' => "%{$query}%",
                'OR:`Language`.`culture_key`:LIKE' => '%' . $query . '%',
            ));
        }
        if (!empty($languageGroup)) {
            $c->where(array(
                '`LanguageGroupMember`.`group_id`' => $languageGroup
            ));

        }
        if (!empty($languageParentType)) {
            $operator = $languageParentType == 1 ? ':!=' : '';
            $c->where(array(
                "`Language`.`parent`{$operator}" => 0
            ));
        }
        return $c;
    }

    public function prepareRow(xPDOObject $object)
    {
        $data = $object->toArray();

        if (!$this->getProperty('combo')) {
            $data['actions'] = array(
                array(
                    'cls' => array(
                        'menu' => 'green',
                        'button' => 'green',
                    ),
                    'icon' => 'icon icon-edit',
                    'title' => $this->modx->lexicon('polylang_content_menu_update'),
                    'action' => 'updateItem',
                    'button' => true,
                    'menu' => true,
                ), array(
                    'cls' => array(
                        'menu' => 'blue',
                        'button' => 'blue',
                    ),
                    'icon' => 'icon icon-files-o',
                    'title' => $this->modx->lexicon('polylang_content_menu_copy'),
                    'action' => 'copyItem',
                    'button' => true,
                    'menu' => true,
                ), array(
                    'cls' => array(
                        'menu' => 'purple',
                        'button' => 'purple',
                    ),
                    'icon' => 'icon icon-pencil',
                    'title' => $this->modx->lexicon('polylang_content_menu_edit_lexicon'),
                    'action' => 'editLexicon',
                    'button' => false,
                    'menu' => true,
                )
            );
            if (!$data['active']) {
                $data['actions'][] = array(
                    'cls' => array(
                        'menu' => 'yellow',
                        'button' => 'yellow',
                    ),
                    'icon' => 'icon icon-power-off',
                    'title' => $this->modx->lexicon('polylang_content_menu_enable'),
                    'multiple' => $this->modx->lexicon('polylang_content_menu_multiple_enable'),
                    'action' => 'enableItem',
                    'button' => true,
                    'menu' => true,
                );
            } else {
                $data['actions'][] = array(
                    'cls' => array(
                        'menu' => 'gray',
                        'button' => 'gray',
                    ),
                    'icon' => 'icon icon-power-off',
                    'title' => $this->modx->lexicon('polylang_content_menu_disable'),
                    'multiple' => $this->modx->lexicon('polylang_content_menu_multiple_disable'),
                    'action' => 'disableItem',
                    'button' => true,
                    'menu' => true,
                );
            }
            $data['actions'][] = array(
                'cls' => array(
                    'menu' => 'red',
                    'button' => 'red',
                ),
                'icon' => 'icon icon-trash-o',
                'title' => $this->modx->lexicon('polylang_content_menu_remove'),
                'multiple' => $this->modx->lexicon('polylang_content_menu_multiple_remove'),
                'action' => 'removeItem',
                'button' => true,
                'menu' => true,
            );

        }

        return $data;
    }
}

return 'PolylangPolylangContentGetListProcessor';