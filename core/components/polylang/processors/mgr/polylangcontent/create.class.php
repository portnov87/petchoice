<?php

class PolylangPolylangContentCreateProcessor extends modObjectCreateProcessor
{
    public $classKey = 'PolylangContent';
    public $languageTopics = array('polylang:default');
    public $beforeSaveEvent = 'OnBeforeSavePolylangContent';
    public $afterSaveEvent = 'OnSavePolylangContent';
    public $permission = 'polylang_localization_save';
    /** @var Polylang $polylang */
    public $polylang;

    public function initialize()
    {
        if (!$this->modx->hasPermission($this->permission)) {
            return $this->modx->lexicon('access_denied');
        }
        $this->polylang = $this->modx->getService('polylang', 'Polylang');
        return parent::initialize();
    }

    /**
     * @return array|string
     */
    public function beforeSet()
    {
        $canSet = parent::beforeSet();
        if ($canSet) {
            $properties = array('foreign_properties' => array());
            $classes = $this->polylang->getTools()->getContentClasses();
            $classes['tvpolylang'] = 'PolylangContent';
            $data = $this->getProperties();
            $contentId = $this->modx->getOption('polylangcontent_content_id', $data);
            $cultureKey = $this->modx->getOption('polylangcontent_culture_key', $data);
            foreach ($data as $key => $value) {
                if (preg_match('/^([a-zA-Z0-9]+)(?=_)/', $key, $match)) {
                    $classKey = $match[1];
                    if (isset($classes[$classKey])) {
                        $class = $classes[$classKey];
                        $prefix = $this->modx->call($class, 'getFieldPrefix', array($classKey));
                        $key = $prefix . str_replace("{$classKey}_", '', $key);
                        if ($class == $this->classKey) {
                            $properties[$key] = $value;
                        } else {
                            if (!isset($properties['foreign_properties'][$class])) {
                                $properties['foreign_properties'][$class] = array(
                                    'content_id' => $contentId,
                                    'culture_key' => $cultureKey,
                                );
                            }
                            $properties['foreign_properties'][$class][$key] = $value;
                        }
                    }
                }
            }
            $this->setProperties($properties);
        }
        return $canSet;
    }

    public function afterSave()
    {
        $canSave = parent::afterSave();
        if ($canSave) {
            $properties = $this->getProperty('foreign_properties', array());
            $autocreateDependentLanguages = $this->getProperty('dependent_languages', 0);
            if ($properties) {
                foreach ($properties as $class => $data) {
                    /** @var xPDOSimpleObject $o */
                    $o = $this->modx->newObject($class);
                    $o->fromArray($data);
                    if (!$o->save()) {
                        $this->modx->log(modX::LOG_LEVEL_ERROR, "Error save data for class {$class}. Data:\n" . print_r($data, 1));
                    }
                }
            }
            if ($autocreateDependentLanguages) {
                $cultureKey = $this->object->get('culture_key');
                $dependentLanguages = $this->polylang->getTools()->getDependentLanguages($cultureKey);
                if ($dependentLanguages) {
                    $this->setProperty('dependent_languages', 0);
                    $errors = array();
                    foreach ($dependentLanguages as $language) {
                        $localization = $this->modx->getObject($this->classKey, array(
                            'content_id' => $this->object->get('content_id'),
                            'culture_key' => $language['culture_key']
                        ));
                        if (!$localization) {
                            $this->setProperty('polylangcontent_culture_key', $language['culture_key']);
                            /** @var modProcessorResponse $response */
                            $response = $this->polylang->runProcessor('mgr/polylangcontent/create', $this->getProperties());
                            if ($response->isError()) {
                                $errors[] = $response->getMessage();
                            }
                        }
                    }
                    if (!empty($errors)) {
                        $this->modx->log(modX::LOG_LEVEL_ERROR, print_r($errors, 1));
                        $canSave = false;
                    }
                }
            }
        }
        return $canSave;
    }


}

return 'PolylangPolylangContentCreateProcessor';