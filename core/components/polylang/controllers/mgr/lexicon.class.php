<?php
if (!class_exists('PolylangManagerController')) {
    require_once dirname(dirname(__FILE__)) . '/manager.class.php';
}


class PolylangMgrLexiconManagerController extends PolylangManagerController
{

    public function loadCustomCssJs()
    {
        $language = empty($_GET['language']) ? '' : $_GET['language'];
        $this->addJavascript($this->polylang->config['jsUrl'] . 'mgr/lexiconentries/lexiconentries.grid.js');
        $this->addJavascript($this->polylang->config['jsUrl'] . 'mgr/lexiconentries/lexiconentries.panel.js');
        $this->addHtml('<script type="text/javascript">
        // <![CDATA[
        Ext.onReady(function() {
            MODx.add({
              xtype: "polylang-panel-lexiconentries",
              language:"' . $language . '"
              });
        });
        // ]]>
        </script>');
        $this->modx->invokeEvent('polylangOnManagerCustomCssJs', array('controller' => &$this, 'page' => 'lexicons'));
    }

    public function getPageTitle()
    {
        return $this->modx->lexicon('polylang_lexiconentries_page_title');
    }
}