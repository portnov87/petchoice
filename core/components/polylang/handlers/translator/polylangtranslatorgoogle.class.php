<?php

use Google\Cloud\Translate\V2\TranslateClient;

class PolylangTranslatorGoogle extends PolylangTranslatorHandler
{
    /** @var Translator|null $translator */
    public $translator = null;

    public function __construct(Polylang &$polylang, $config = array())
    {
        parent::__construct($polylang, $config);
        $key = $this->modx->getOption('polylang_translate_google_key', $config, '', true);
        $referer = 'https://petchoice.ua';//MODX_URL_SCHEME . MODX_HTTP_HOST;
        if ($key) {

            $this->translator = new TranslateClient(array(
                'key' => $key,
               /* 'keyFile'=>'{
  "type": "service_account",
  "project_id": "adept-comfort-328310",
  "private_key_id": "3c568aaae3bf279cf89af4e50ae9a3a648b47af4",
  "private_key": "-----BEGIN PRIVATE KEY-----\nMIIEvgIBADANBgkqhkiG9w0BAQEFAASCBKgwggSkAgEAAoIBAQCwKeT76QAuhpyf\nRgCH+mP+YWlLHEJlLIS02YI8lyqoZDJBpi+HGkQPFp8eXqAf32dJ7d7UYEQjsb3j\nuuQOPO+9J+lpS57XRB5TLd3qYYQQ6zNj34pE+SUyFOmgLUHpLOD7iJQCAjJqwMjX\nYV9ag2FkvMPpqckiLt0WLbPpQqnMB9Y1ZA4Bp/xdFX15gQXYfl9dKbI0DjE8IB4L\ntsguM5rMOYs67RcqC75NA4Lm8rBEEdDKc707H32Jp/CdX+YU4wc0wCxsXqlnmof8\nikY3MsSgD34uKgq8XnknntGqBQu7Wr6tHrpYb8s8+hh3/09J8U77nYl9bZk+nn2r\n4dOKzHApAgMBAAECggEAAhMe5xfu5cgLEdXNYsyVnzopdyDS6AXMCE85eUTfKT2I\n2VIU5DabJOxYx7md5tIxkWLTntUFaGrgTXghgHNs8AwyHl3YII6Ppt4hlhrmPHFw\nrlVEd8cmuNNwju+Jg66rdaWS91UW9T/mUvcKxvJI1DAO70e4c9vGbyDu/1PG09LM\nzKJjEnQNyVwvRdNI+/2nZ9ltZ85f2Ny4S8YyrSE4tDSrVklRaZ7VgaUeb6qKNNJb\naEXW5L7yp0zxGIaQX5wQ45NkeYT1lnkFlz3PDFpfTRMhmq5d73jklZwe7PKfpYNv\nBG7k6VelY92IWApAW8v2nxYgNzF3xZpTGSLhZZSigQKBgQDbe8IYR6s3M3XuvxdF\nfFewlYzBY2BmIkk2bVf24Mj1GVfnoidpg+PqdP5Qi4n1WqkWKN6OJeQouGsPa/m6\nphb3B1lQiP/Tb/APqVdpq+kz07XAZTNi4XQpDdWuQtqEvMa4XeJ412IMFGMlrMnv\n2LP3W3gg3PTKtoATxF2+74FpIQKBgQDNeQ88CjYucY03RcD9ktUDhbhGXajgv1MQ\nzc1krgYmFQt4tudHEgjwfFM5gQ/+Scu21D9j2MDPSjactllSrdYDlPUv8UR8tp7l\nP8YHxj3n4ZmyIMHTMUOD872StqRVFjYYJ1pTASw9P2P3acYvDoOTFwag2di6fz33\nizS+RdH+CQKBgQDLNsfcASd2oa/0kHs0e88ZES5Us/VCO0Z8HTtPuop13LQ2JD3H\nhTUWFKKO4tOmFv3tJYQCKvUX+sAtdDSxayxiJiy800vCcabcaVck4OW3dbKXY1vz\nlnkdtdB9WyrFsZ9wfoS8i/cc5hdyf4wtBQCD6u3Lx6jis5zcMHj32vt/4QKBgByg\nsFGyLohcA8veqRvrWEhsWhdbt0rQ/VbLBcHwSSUHK5j/CswpJBu1hrbtV3gbegmY\nGtjje0GmWmONOHa/h6DqIRsz51Hk89CHCSVOJpPnJ83cqISvSXPaAuCtF8pPFOxa\nl0gXUr2Z1mjDUPx/bi+RNrg0gfDCB7U0rP2RpsdZAoGBAKCJTgNE6ajwgwOWPujA\nlneWEz7ukWHutqr3VjsYpCBXWwtfJa5NMhWa1d7iU85o7OSUEEuUsF/E2ghCSadL\nxgn/cfCq1pSgB4b8XUE35ijSAu6zljkaUexDB81kK129flq/J5imml52Xj8INDr6\npDnFvCBFiQiIofnBRqGs1aoJ\n-----END PRIVATE KEY-----\n",
  "client_email": "petchoice@adept-comfort-328310.iam.gserviceaccount.com",
  "client_id": "113544588780837116362",
  "auth_uri": "https://accounts.google.com/o/oauth2/auth",
  "token_uri": "https://oauth2.googleapis.com/token",
  "auth_provider_x509_cert_url": "https://www.googleapis.com/oauth2/v1/certs",
  "client_x509_cert_url": "https://www.googleapis.com/robot/v1/metadata/x509/petchoice%40adept-comfort-328310.iam.gserviceaccount.com"
}',*/
                'restOptions' => array(
                    'headers' => array(
                        'referer' => $referer
                    )
                )
            ));
        } else {
            $this->modx->log(modX::LOG_LEVEL_ERROR, 'Not set google API key!');
        }
    }

   ///Users/portnovvit/www/sites/translate/adept-comfort-328310-0afcee3c68f2.json

    /**
     * @param string $text
     * @param string $from
     * @param string $to
     * @param array $options
     *
     * @return string|false
     */
    public function translate($text, $from, $to, array $options = array())
    {
        if (!$this->translator) return false;
        try {
            if ($text) {
                $from = $this->prepareLanguageCode($from);
                $to = $this->prepareLanguageCode($to);

                $result = $this->translator->translate($text, array('source' => $from, 'target' => $to));
//                echo '<pre>$result translate';
//                print_r($result);
//                echo '</pre>';
                $text = isset($result['text']) ? $result['text'] : $text;
                if ($this->isPostProcessingTranslation) {
                    $text = $this->postProcessingTranslation($text);
                }
            }
        } catch (Exception $e) {
            $this->modx->log(modX::LOG_LEVEL_ERROR, $e->getMessage());
            return false;
        }
        return $text;
    }

    /**
     * @return bool
     */
    public function isInitialized()
    {
        return $this->translator !== null;
    }
}
