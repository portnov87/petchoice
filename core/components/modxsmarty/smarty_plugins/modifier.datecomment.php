<?php



/**
* Модификатор date: unix_timestamp => дата
*
* param string $string
* param string $format
* return string
*/

function declension($count, $forms, $lang = null) {
	global $smarty,$modx;
		
		$forms =$modx->fromJSON($forms);

		if ($lang == 'ru') {
			$mod100 = $count % 100;
			switch ($count%10) {
				case 1:
					if ($mod100 == 11) {$text = $forms[2];}
					else {$text = $forms[0];}
					break;
				case 2:
				case 3:
				case 4:
					if (($mod100 > 10) && ($mod100 < 20)) {$text = $forms[2];}
					else {$text = $forms[1];}
					break;
				case 5:
				case 6:
				case 7:
				case 8:
				case 9:
				case 0:
				default: $text = $forms[2];
			}
		}
		else {
			if ($count == 1) {
				$text = $forms[0];
			}
			else {
				$text = $forms[1];
			}
		}
		return $text;

	}

function smarty_modifier_datecomment($date, $dateFormat='d F Y')
{//echo $date;//date('Y-m-d H:i',$date);
	global $smarty,$modx;
	//, modX &$modx
	$dateNow= 10;
	$dateDay = 'day H:i';
	$dateMinutes = 59;
	$dateHours= 10;
	
	$ticket_date_minutes_back_less = 'меньше минуты назад';
$ticket_date_hours_back = '["[[+hours]] час назад","[[+hours]] часа назад","[[+hours]] часов назад"]';
$ticket_date_hours_back_less = 'меньше часа назад';
$ticket_date_now= 'Только что';
$ticket_date_today = 'Сегодня в';
$ticket_date_yesterday = 'Вчера в';
$ticket_date_tomorrow = 'Завтра в';

   $date = strtotime($date);//preg_match('/^\d+$/',$date) ?  $date : strtotime($date);
   
		$dateFormat = !empty($dateFormat) ? $dateFormat : 'd F Y';
		$current = time();
		//echo $date.' '.$current.' ';
		$delta = $current - $date;

		if ($dateNow) {
			if ($delta < $dateNow) {return $ticket_date_now;}
		}

		if ($dateMinutes) {
			$minutes = round(($delta) / 60);
			//echo $delta.' '.$minutes.' '.$dateMinutes;
			if ($minutes < $dateMinutes) {
				if ($minutes > 0) {//return;
				$ticket_date_minutes_back = '["'.$minutes.' минута назад","'.$minutes.' минуты назад","'.$minutes.' минут назад"]';
					return declension($minutes, $ticket_date_minutes_back);
				}
				else {
					return $ticket_date_minutes_back_less;
				}
			}
		}
//echo date('Y-m-d H:i',$date);//, mktime(0, 0, 0, date('m')  , date('d')-1, date('Y')) ).' ';
		if ($dateHours) {
			$hours = round(($delta) / 3600);
			if ($hours < $dateHours) {
				if ($hours > 0) {
					return $hours.' '.declension($hours, $ticket_date_hours_back);//$modx->lexicon('ticket_date_hours_back',array('hours' => $hours)));
				}
				else {
					return $modx->lexicon('ticket_date_hours_back_less');
				}
			}
		}

		if ($dateDay) {
			switch(date('Y-m-d', $date)) {
				case date('Y-m-d'):
					$day = $ticket_date_today;
					break;
				case date('Y-m-d', mktime(0, 0, 0, date('m')  , date('d')-1, date('Y')) ):
					$day = $ticket_date_yesterday;
					break;
				case date('Y-m-d', mktime(0, 0, 0, date('m')  , date('d')+1, date('Y')) ):
					$day = $ticket_date_tomorrow;
					break;
				default: $day = null;
			}
			if($day) {
				$format = str_replace("day",preg_replace("#(\w{1})#",'\\\${1}',$day),$dateDay);
				return date($format,$date);
			}
		}
 //$modx = & $smarty->modx;
		$m = date("n", $date);
		$month_arr = $modx->fromJSON('["января","февраля","марта","апреля","мая","июня","июля","августа","сентября","октября","ноября","декабря"]');//$modx->lexicon('ticket_date_months'));
		$month = $month_arr[$m - 1];

		$format = preg_replace("~(?<!\\\\)F~U", preg_replace('~(\w{1})~u','\\\${1}', $month), $dateFormat);
		/**/
		//echo $dateFormat.' '.$month;

		return date($format ,$date);
}


/*
function smarty_modifier_spell($num, $one, $two, $many) {
    if ($num%10==1 && $num%100!=11){
        return $one;
    }
    elseif($num%10>=2 && $num%10<=4 && ($num%100<10 || $num%100>=20)){
        return $two;
    }
    else{
        return $many;
    }
}*/
?>
