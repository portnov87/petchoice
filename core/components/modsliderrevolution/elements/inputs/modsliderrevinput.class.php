<?php

class modSliderRevInput extends cbBaseInput
{
    public $defaultIcon = 'modsliderrev';
    public $defaultTpl = '[[modSliderRevolution? &slider=`[[+alias]]`]]';
    /** @var ModSliderRev $modSliderRev */
    public $modSliderRev;

    /**
     * modSliderRevInput constructor.
     * @param ContentBlocks $contentBlocks
     * @param array $options
     */
    public function __construct(ContentBlocks $contentBlocks, array $options = array())
    {
        parent::__construct($contentBlocks, $options);
        $this->modSliderRev = $this->modx->getService('modSliderRev ', 'ModSliderRev', $this->modx->getOption('modsliderrevolution.core_path', null, $this->modx->getOption('core_path') . 'components/modsliderrevolution/') . 'model/modsliderrevolution/', array());
        $this->modx->lexicon->load('modsliderrevolution:contentblocks');
    }

    public function getFieldProperties()
    {
        return array(
            array(
                'key' => 'thumb_size',
                'fieldLabel' => $this->modx->lexicon('modsliderrevolution.property.thumb_size'),
                'description' => $this->modx->lexicon('modsliderrevolution.property.thumb_size_desc'),
                'xtype' => 'contentblocks-combo-gallery-thumbsize',
                'default' => 'medium'
            ),
           /* array(
                'key' => 'cover',
                'fieldLabel' => $this->modx->lexicon('modsliderrevolution.property.cover'),
                'description' => $this->modx->lexicon('modsliderrevolution.property.cover_desc'),
                'xtype' => 'contentblocks-combo-boolean',
                'default' => 0
            )*/
        );
    }

    public function getName()
    {
        return $this->modx->lexicon('modsliderrevolution.input_name');
    }

    public function getDescription()
    {
        return $this->modx->lexicon('modsliderrevolution.input_description');
    }

    public function getLexiconTopics()
    {
        return array(
            'modsliderrevolution:contentblocks',
            'modsliderrevolution:sliders',
        );
    }

    /**
     * @return array
     */
    public function getJavaScripts()
    {
        return array(
            $this->modSliderRev->config['jsUrl'] . '/mgr/contentblocks/modsliderrevinput.js',
        );
    }
    /**
     * @return array
     */
    public function getTemplates()
    {
        $tpls = array();
        $corePath = $this->modSliderRev->config['templatesPath'];
        $template = file_get_contents($corePath . 'contentblocks/inputs/modsliderrevinput.tpl');
        $tpls[] = $this->contentBlocks->wrapInputTpl('modsliderrevinput', $template);
        return $tpls;
    }
}