<?php
/**
 * modSliderRevolution
 * @package modsliderrevolution
 * @var modX $modx
 * @var array $scriptProperties
 */

/** @var ModSliderRev $modSliderRev */
$modSliderRev = $modx->getService('modSliderRev ', 'ModSliderRev', $modx->getOption('modsliderrevolution.core_path', null, $modx->getOption('core_path') . 'components/modsliderrevolution/') . 'model/modsliderrevolution/', array());
$fenomParser = filter_var($modx->getOption('pdotools_fenom_parser', null, 0, true), FILTER_VALIDATE_BOOLEAN);
define('MODX_SLIDER_REV_BASE_URL', $modSliderRev->getSliderBaseUrl());
define('MODX_FENOM_PARSER', $fenomParser);

if (trim($modx->getOption('server_protocol')) === 'https') {
    $_SERVER['HTTPS'] = 'on';
}

include $modSliderRev->config['revSliderPath'] . 'embed.php';

$modx->regClientScript(RevSliderEmbedder::jsIncludes(false, false));
$modx->regClientHTMLBlock(RevSliderEmbedder::cssIncludes(false));

return $modSliderRev->prepareContentForFenom(RevSliderEmbedder::putRevSlider($slider, false));

