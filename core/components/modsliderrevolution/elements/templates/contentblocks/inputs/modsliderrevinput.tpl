<div class="contentblocks-field contentblocks-field-modsliderrevinput">
    <input type="hidden" class="modsliderrev_id"/>
    <input type="hidden" class="modsliderrev_alias"/>
    <div class="contentblocks-field-actions">
        <a href="javascript:void(0);" class="contentblocks-field-modsliderrevinput-update">{%=_('modsliderrevolution.btn.update')%}</a>
        <a href="javascript:void(0);" class="contentblocks-field-modsliderrevinput-refresh">{%=_('modsliderrevolution.btn.refresh')%}</a>
        <a href="javascript:void(0);" class="contentblocks-field-modsliderrevinput-remove">&times; {%=_('modsliderrevolution.btn.remove')%}</a>
    </div>
    <label>{%=o.name%}</label>
    <div class="contentblocks-field-modsliderrevinput-actions" >
        <a href="javascript:void(0);" class="big contentblocks-field-button contentblocks-field-modsliderrevinput-add">{%=_('modsliderrevolution.btn.add')%}</a>
    </div>
    <div class="contentblocks-field-modsliderrevinput-preview"></div>
</div>
