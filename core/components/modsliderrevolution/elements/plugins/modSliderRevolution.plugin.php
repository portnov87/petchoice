<?php
/**
 * @var modX $modx
 * @var array $scriptProperties
 */

/** @var ModSliderRev $modSliderRev */
$modSliderRev = $modx->getService('modSliderRev ', 'ModSliderRev', $modx->getOption('modsliderrevolution.core_path', null, $modx->getOption('core_path') . 'components/modsliderrevolution/') . 'model/modsliderrevolution/', array());

switch ($modx->event->name) {
    case 'OnRichTextEditorRegister':
        $modSliderRev->editorBtnRegister();
        break;
    case 'OnDocFormPrerender':
        $modSliderRev->loadControllerEditorBtnJsCss($modx->controller);
        break;
    case 'OnTempFormPrerender':
    case 'OnChunkFormPrerender':
        $modSliderRev->loadControllerEditorBtnJsCss($modx->controller, false);
        break;
    case 'ContentBlocks_RegisterInputs':
        if ($class = $modx->loadClass('inputs.modSliderRevInput', $modSliderRev->config['corePath'] . 'elements/', true, true)) {
            $instance = new $class($contentBlocks);
            $modx->event->output(array(
                'modsliderrevinput' => $instance,
            ));
        }
        break;

}