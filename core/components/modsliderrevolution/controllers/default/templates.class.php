<?php
require_once dirname(dirname(dirname(__FILE__))) . '/index.class.php';

class ControllersMgrTemplatesManagerController extends ModSliderRevMainController {
    public static function getDefaultController() {
        return 'templates';
    }
}

class modSliderRevolutionTemplatesManagerController extends ModSliderRevMainController {
    public function process(array $scriptProperties = array()) {

    }
    public function loadCustomCssJs() {
        $mgrUrl = $this->modx->getOption('manager_url',null,MODX_MANAGER_URL);

        $this->addJavascript($this->modSliderRev->config['jsUrl'].'mgr/misc/strftime-min-1.3.js');
        $this->addJavascript($this->modSliderRev->config['jsUrl'].'mgr/misc/search.combo.js');
        $this->addJavascript($this->modSliderRev->config['jsUrl'].'mgr/misc/default.grid.js');
        $this->addJavascript($this->modSliderRev->config['jsUrl'].'mgr/widgets/filter.combo.js');
        $this->addJavascript($this->modSliderRev->config['jsUrl'].'mgr/widgets/templates.view.js');
        $this->addJavascript($this->modSliderRev->config['jsUrl'].'mgr/widgets/revslidermodtemplates.panel.js');
        $this->addLastJavascript($this->modSliderRev->config['jsUrl'].'mgr/sections/revslidermodtemplates.js');

        /*$this->addJavascript($this->modSliderRev->config['jsUrl'].'mgr/modsliderrev.js');
         $this->addHtml('<script type="text/javascript">
        Ext.onReady(function() {
            ModSliderRev.config = '.$this->modx->toJSON($this->modSliderRev->config).';
        });
        </script>');*/

        $this->addCss($this->modSliderRev->config['cssUrl'] . 'mgr/main.css');
        $this->addCss($this->modSliderRev->config['cssUrl'] . 'mgr/bootstrap.buttons.css');
        $this->addCss($this->modSliderRev->config['assetsUrl'] . 'vendor/fontawesome/css/font-awesome.min.css');


    }
    public function getLanguageTopics() {
        return array('modsliderrevolution:default','modsliderrevolution:revslidermodtemplates');
    }
    public function getPageTitle() {
        return $this->modx->lexicon('modsliderrevolution.page.revslidermodtemplates_title');
    }
    public function getTemplateFile() {
        return $this->modSliderRev->config['templatesPath'].'mgr/revslidermodtemplates.tpl';
    }
}