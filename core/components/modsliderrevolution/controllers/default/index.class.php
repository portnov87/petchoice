<?php
require_once dirname(dirname(dirname(__FILE__))) . '/index.class.php';

class modSliderRevolutionIndexManagerController extends ModSliderRevMainController
{

    public function loadCustomCssJs()
    {


        $this->addCss($this->modSliderRev->config['cssUrl'] . 'mgr/main.css');
        // $this->addCss($this->modSliderRev->config['cssUrl'] . 'mgr/bootstrap.buttons.css');
        //$this->addCss($this->modSliderRev->config['assetsUrl'] . 'vendor/fontawesome/css/font-awesome.min.css');

    }

    public function getLanguageTopics()
    {
        return array('modsliderrevolution:default', 'modsliderrevolution:sliders');
    }

    public function getPageTitle()
    {
        return $this->modx->lexicon('modsliderrevolution.sliders.page.title');
    }

    /**
     * @return string
     */
    public function getInjectContent()
    {
        $url = $this->modSliderRev->getSliderBaseUrl();
        if (isset($_REQUEST['id']) && !empty($_REQUEST['id'])) {
           // $url .= "index.php?page=revslider&view=slider&id=" . (int)$_REQUEST['id'];
            $url .= "index.php?page=revslider&view=slide&id=new&slider=" . (int)$_REQUEST['id'];
        }
        return '<iframe class="modsliderrev-iframe" src="' . $url . '" width="100%" height="100%" scrolling="yes"  frameborder="0" _onload="this.style.height= (document.body.clientHeight -55) +\'px\';"></iframe>';
    }


}