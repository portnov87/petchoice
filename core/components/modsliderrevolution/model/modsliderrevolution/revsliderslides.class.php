<?php

class RevsliderSlides extends xPDOSimpleObject
{

    public $assetPath;
    public $assetsUrl;
    public $basePath;
    public $baseUrl;
    public $imagePath;
    public $imageUrl;
    public $pluginUrl;

    /**
     * RevsliderSlides constructor.
     */
    public function __construct(xPDO & $xpdo)
    {
        parent::__construct($xpdo);

        $this->assetPath = $this->xpdo->getOption('modsliderrevolution.assets_path', null, $this->xpdo->getOption('assets_path', null, MODX_ASSETS_PATH) . 'components/modsliderrevolution/');
        $this->assetsUrl = $this->xpdo->getOption('modsliderrevolution.assets_url', null, $this->xpdo->getOption('assets_url') . 'components/modsliderrevolution/');
        $this->basePath = $this->assetPath . 'vendor/revslider/';
        $this->baseUrl = $this->assetsUrl . 'vendor/revslider/';
        $this->imagePath = $this->basePath . 'media/';
        $this->imageUrl = $this->baseUrl . 'media/';
        $this->pluginUrl = $this->baseUrl . 'revslider/';


    }

    /**
     * @param string $sliderType
     * @return array
     */
    public function getImageAttributes($sliderType = '')
    {

        $bgType = $this->getParam('background_type', 'transparent');
        $bgColor = $this->getParam('slide_bg_color', 'transparent');

        $bgFit = $this->getParam('bg_fit', 'cover');
        $bgFitX = intval($this->getParam('bg_fit_x', '100'));
        $bgFitY = intval($this->getParam('bg_fit_y', '100'));

        $bgPosition = $this->getParam('bg_position', 'center top');
        $bgPositionX = intval($this->getParam('bg_position_x', '0'));
        $bgPositionY = intval($this->getParam('bg_position_y', '0'));

        $bgRepeat = $this->getParam('bg_repeat', 'no-repeat');

        $bgStyle = ' ';
        if ($bgFit == 'percentage') {
            $bgStyle .= "background-size: " . $bgFitX . '% ' . $bgFitY . '%;';
        } else {
            $bgStyle .= "background-size: " . $bgFit . ";";
        }
        if ($bgPosition == 'percentage') {
            $bgStyle .= "background-position: " . $bgPositionX . '% ' . $bgPositionY . '%;';
        } else {
            $bgStyle .= "background-position: " . $bgPosition . ";";
        }
        $bgStyle .= "background-repeat: " . $bgRepeat . ";";

        $thumb = '';
        $thumb_on = $this->getParam('thumb_for_admin', 'off');

        switch ($sliderType) {
            case 'gallery':
                $imageID = $this->getParam('image_id');
                if (empty($imageID)) {
                    $thumb = $this->getParam('image');

                    $imgID = $this->getImageIdByUrl($thumb);
                    if ($imgID !== false) {
                        $thumb = $this->getUrlAttachmentImage($imgID);
                    }
                } else {
                    $thumb = $this->getUrlAttachmentImage($imageID);
                }

                if ($thumb_on == 'on') {
                    $thumb = $this->getParam('slide_thumb', '');
                }

                break;
            case 'posts':
                $thumb = $this->pluginUrl . 'public/assets/assets/sources/post.png';
                $bgStyle = 'background-size: cover;';
                break;
            case 'woocommerce':
                $thumb = $this->pluginUrl . 'public/assets/assets/sources/wc.png';
                $bgStyle = 'background-size: cover;';
                break;
            case 'facebook':
                $thumb = $this->pluginUrl . 'public/assets/assets/sources/fb.png';
                $bgStyle = 'background-size: cover;';
                break;
            case 'twitter':
                $thumb = $this->pluginUrl . 'public/assets/assets/sources/tw.png';
                $bgStyle = 'background-size: cover;';
                break;
            case 'instagram':
                $thumb = $this->pluginUrl . 'public/assets/assets/sources/ig.png';
                $bgStyle = 'background-size: cover;';
                break;
            case 'flickr':
                $thumb = $this->pluginUrl . 'public/assets/assets/sources/fr.png';
                $bgStyle = 'background-size: cover;';
                break;
            case 'youtube':
                $thumb = $this->pluginUrl . 'public/assets/assets/sources/yt.png';
                $bgStyle = 'background-size: cover;';
                break;
            case 'vimeo':
                $thumb = $this->pluginUrl . 'public/assets/assets/sources/vm.png';
                $bgStyle = 'background-size: cover;';
                break;
        }

        if ($thumb == '') $thumb = $this->getParam('image');


        $bgExtraClass = '';
        $dataUrlImageForView = $thumb;
        $bgFullStyle = $bgStyle;

        if ($bgType == "solid") {
            if ($thumb_on == 'off') {
                $bgFullStyle = 'background-color:' . $bgColor . ';';
                $dataUrlImageForView = '';
            } else {
                $bgFullStyle = 'background-size: cover;';
            }
        }

        if ($bgType == "trans" || $bgType == "transparent") {
            $dataUrlImageForView = '';
            $bgExtraClass = 'mini-transparent';
            $bgFullStyle = 'background-size: inherit; background-repeat: repeat;';

        }

        if (!empty($dataUrlImageForView)) {
            $bgFullStyle .= "background-image:url({$dataUrlImageForView});";
        }

        return array(
            'thumb_url' => $dataUrlImageForView,
            'thumb_class' => $bgExtraClass,
            'thumb_style' => $bgFullStyle
        );

    }

    /**
     * @param string $key
     * @param null $default
     * @return mixed
     */
    public function getParam($key = '', $default = null)
    {
        return $this->xpdo->getOption($key, $this->get('params'), $default);
    }

    /**
     * @param $imageUrl
     * @return int|mixed
     */
    public function getImageIdByUrl($imageUrl)
    {
        $attachmentId = 0;
        if ('' == $imageUrl || !$imageUrl) return $attachmentId;

        $classKey = 'RevsliderImages';
        $q = $this->xpdo->newQuery('RevsliderImages');
        $q->select($this->xpdo->getSelectColumns($classKey, $classKey, '', array('id')));
        $q->where(array('url' => $imageUrl));

        if ($q->prepare() && $q->stmt->execute()) {
            $attachmentId = $q->stmt->fetch(PDO::FETCH_COLUMN);
        }

        return $attachmentId;
    }


    /**
     * @param int $id
     * @return mixed|string
     */
    public function getImageUrlById($id = 0)
    {
        $url = '';
        $classKey = 'RevsliderImages';
        $q = $this->xpdo->newQuery('RevsliderImages');
        $q->select($this->xpdo->getSelectColumns($classKey, $classKey, '', array('url')));
        $q->where(array('id' => $id));

        if ($q->prepare() && $q->stmt->execute()) {
            $url = $q->stmt->fetch(PDO::FETCH_COLUMN);
        }

        return $url;
    }


    /**
     * @param int $thumbId
     * @return string
     */
    public function getUrlAttachmentImage($thumbId = 0)
    {
        $imageUrl = $this->getImageUrlById($thumbId);
        return $this->imageUrl . 'thumb/300x200_' . $imageUrl;

    }


}