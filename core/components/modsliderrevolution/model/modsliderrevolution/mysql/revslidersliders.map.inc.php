<?php
$xpdo_meta_map['RevsliderSliders']= array (
  'package' => 'modsliderrevolution',
  'version' => '1.1',
  'table' => 'revslider_sliders',
  'extends' => 'xPDOSimpleObject',
  'tableMeta' => 
  array (
    'engine' => 'MyISAM',
  ),
  'fields' => 
  array (
    'title' => NULL,
    'alias' => NULL,
    'params' => NULL,
    'settings' => NULL,
    'type' => '',
  ),
  'fieldMeta' => 
  array (
    'title' => 
    array (
      'dbtype' => 'tinytext',
      'phptype' => 'string',
      'null' => false,
    ),
    'alias' => 
    array (
      'dbtype' => 'tinytext',
      'phptype' => 'string',
      'null' => true,
    ),
    'params' => 
    array (
      'dbtype' => 'longtext',
      'phptype' => 'json',
      'null' => false,
    ),
    'settings' => 
    array (
      'dbtype' => 'text',
      'phptype' => 'string',
      'null' => true,
    ),
    'type' => 
    array (
      'dbtype' => 'varchar',
      'precision' => '191',
      'phptype' => 'string',
      'null' => false,
      'default' => '',
    ),
  ),
  'indexes' => 
  array (
    'id' => 
    array (
      'alias' => 'id',
      'primary' => false,
      'unique' => true,
      'type' => 'BTREE',
      'columns' => 
      array (
        'id' => 
        array (
          'length' => '',
          'collation' => 'A',
          'null' => false,
        ),
      ),
    ),
  ),
  'composites' => 
  array (
    'RevsliderSlides' => 
    array (
      'class' => 'RevsliderSlides',
      'local' => 'id',
      'foreign' => 'slider_id',
      'cardinality' => 'many',
      'owner' => 'local',
    ),
  ),
);
