<?php
$xpdo_meta_map['RevsliderModFilterTemplate']= array (
  'package' => 'modsliderrevolution',
  'version' => '1.1',
  'table' => 'revslider_mod_filter_template',
  'extends' => 'xPDOSimpleObject',
  'tableMeta' => 
  array (
    'engine' => 'MyISAM',
  ),
  'fields' => 
  array (
    'filter_id' => NULL,
    'template_id' => NULL,
  ),
  'fieldMeta' => 
  array (
    'filter_id' => 
    array (
      'dbtype' => 'int',
      'precision' => '11',
      'phptype' => 'integer',
      'null' => false,
      'index' => 'index',
    ),
    'template_id' => 
    array (
      'dbtype' => 'int',
      'precision' => '11',
      'phptype' => 'integer',
      'null' => false,
    ),
  ),
  'indexes' => 
  array (
    'filter_id' => 
    array (
      'alias' => 'filter_id',
      'primary' => false,
      'unique' => false,
      'type' => 'BTREE',
      'columns' => 
      array (
        'filter_id' => 
        array (
          'length' => '',
          'collation' => 'A',
          'null' => false,
        ),
        'template_id' => 
        array (
          'length' => '',
          'collation' => 'A',
          'null' => false,
        ),
      ),
    ),
  ),
  'aggregates' => 
  array (
    'RevsliderModFilters' => 
    array (
      'class' => 'RevsliderModFilters',
      'local' => 'filter_id',
      'foreign' => 'id',
      'cardinality' => 'one',
      'owner' => 'foreign',
    ),
    'RevsliderModTemplates' => 
    array (
      'class' => 'RevsliderModTemplates',
      'local' => 'template_id',
      'foreign' => 'id',
      'cardinality' => 'one',
      'owner' => 'foreign',
    ),
  ),
);
