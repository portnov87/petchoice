<?php
$xpdo_meta_map['RevsliderOptions']= array (
  'package' => 'modsliderrevolution',
  'version' => '1.1',
  'table' => 'revslider_options',
  'extends' => 'xPDOSimpleObject',
  'tableMeta' => 
  array (
    'engine' => 'MyISAM',
  ),
  'fields' => 
  array (
    'handle' => NULL,
    'option' => NULL,
  ),
  'fieldMeta' => 
  array (
    'handle' => 
    array (
      'dbtype' => 'varchar',
      'precision' => '100',
      'phptype' => 'string',
      'null' => false,
    ),
    'option' => 
    array (
      'dbtype' => 'longtext',
      'phptype' => 'string',
      'null' => false,
    ),
  ),
);
