<?php
$xpdo_meta_map['RevsliderSlides']= array (
  'package' => 'modsliderrevolution',
  'version' => '1.1',
  'table' => 'revslider_slides',
  'extends' => 'xPDOSimpleObject',
  'tableMeta' => 
  array (
    'engine' => 'MyISAM',
  ),
  'fields' => 
  array (
    'slider_id' => NULL,
    'slide_order' => NULL,
    'params' => NULL,
    'layers' => NULL,
    'settings' => NULL,
  ),
  'fieldMeta' => 
  array (
    'slider_id' => 
    array (
      'dbtype' => 'int',
      'precision' => '9',
      'phptype' => 'integer',
      'null' => false,
    ),
    'slide_order' => 
    array (
      'dbtype' => 'int',
      'precision' => '11',
      'phptype' => 'integer',
      'null' => false,
    ),
    'params' => 
    array (
      'dbtype' => 'longtext',
      'phptype' => 'json',
      'null' => false,
    ),
    'layers' => 
    array (
      'dbtype' => 'longtext',
      'phptype' => 'string',
      'null' => false,
    ),
    'settings' => 
    array (
      'dbtype' => 'text',
      'phptype' => 'json',
      'null' => false,
    ),
  ),
  'indexes' => 
  array (
    'id' => 
    array (
      'alias' => 'id',
      'primary' => false,
      'unique' => true,
      'type' => 'BTREE',
      'columns' => 
      array (
        'id' => 
        array (
          'length' => '',
          'collation' => 'A',
          'null' => false,
        ),
      ),
    ),
  ),
  'aggregates' => 
  array (
    'RevsliderSliders' => 
    array (
      'class' => 'RevsliderSliders',
      'local' => 'slider_id',
      'foreign' => 'id',
      'cardinality' => 'one',
      'owner' => 'foreign',
    ),
  ),
);
