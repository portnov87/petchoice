<?php
$xpdo_meta_map['RevsliderModFilters']= array (
  'package' => 'modsliderrevolution',
  'version' => '1.1',
  'table' => 'revslider_mod_filters',
  'extends' => 'xPDOSimpleObject',
  'tableMeta' => 
  array (
    'engine' => 'MyISAM',
  ),
  'fields' => 
  array (
    'key' => NULL,
    'rank' => 0,
  ),
  'fieldMeta' => 
  array (
    'key' => 
    array (
      'dbtype' => 'varchar',
      'precision' => '255',
      'phptype' => 'string',
      'null' => false,
    ),
    'rank' => 
    array (
      'dbtype' => 'int',
      'precision' => '11',
      'phptype' => 'integer',
      'null' => false,
      'default' => 0,
    ),
  ),
  'composites' => 
  array (
    'RevsliderModFilterTemplate' => 
    array (
      'class' => 'RevsliderModFilterTemplate',
      'local' => 'id',
      'foreign' => 'filter_id',
      'cardinality' => 'many',
      'owner' => 'local',
    ),
  ),
);
