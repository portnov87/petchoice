<?php
$xpdo_meta_map['RevsliderModTemplates']= array (
  'package' => 'modsliderrevolution',
  'version' => '1.1',
  'table' => 'revslider_mod_templates',
  'extends' => 'xPDOSimpleObject',
  'tableMeta' => 
  array (
    'engine' => 'MyISAM',
  ),
  'fields' => 
  array (
    'title' => NULL,
    'description' => NULL,
    'preview' => NULL,
    'img' => NULL,
    'zip' => NULL,
  ),
  'fieldMeta' => 
  array (
    'title' => 
    array (
      'dbtype' => 'text',
      'phptype' => 'string',
      'null' => false,
    ),
    'description' => 
    array (
      'dbtype' => 'text',
      'phptype' => 'string',
      'null' => false,
    ),
    'preview' => 
    array (
      'dbtype' => 'text',
      'phptype' => 'string',
      'null' => false,
    ),
    'img' => 
    array (
      'dbtype' => 'text',
      'phptype' => 'string',
      'null' => false,
    ),
    'zip' => 
    array (
      'dbtype' => 'text',
      'phptype' => 'string',
      'null' => false,
    ),
  ),
  'composites' => 
  array (
    'RevsliderModFilterTemplate' => 
    array (
      'class' => 'RevsliderModFilterTemplate',
      'local' => 'id',
      'foreign' => 'template_id',
      'cardinality' => 'many',
      'owner' => 'local',
    ),
  ),
);
