<?php
$xpdo_meta_map['RevsliderTransients']= array (
  'package' => 'modsliderrevolution',
  'version' => '1.1',
  'table' => 'revslider_transients',
  'extends' => 'xPDOSimpleObject',
  'tableMeta' => 
  array (
    'engine' => 'MyISAM',
  ),
  'fields' => 
  array (
    'handle' => NULL,
    'expires' => NULL,
    'value' => NULL,
  ),
  'fieldMeta' => 
  array (
    'handle' => 
    array (
      'dbtype' => 'varchar',
      'precision' => '200',
      'phptype' => 'string',
      'null' => false,
    ),
    'expires' => 
    array (
      'dbtype' => 'timestamp',
      'phptype' => 'timestamp',
      'null' => true,
    ),
    'value' => 
    array (
      'dbtype' => 'longtext',
      'phptype' => 'string',
      'null' => false,
    ),
  ),
);
