<?php
$xpdo_meta_map['RevsliderImages']= array (
  'package' => 'modsliderrevolution',
  'version' => '1.1',
  'table' => 'revslider_images',
  'extends' => 'xPDOSimpleObject',
  'tableMeta' => 
  array (
    'engine' => 'MyISAM',
  ),
  'fields' => 
  array (
    'url' => NULL,
  ),
  'fieldMeta' => 
  array (
    'url' => 
    array (
      'dbtype' => 'varchar',
      'precision' => '250',
      'phptype' => 'string',
      'null' => true,
    ),
  ),
);
