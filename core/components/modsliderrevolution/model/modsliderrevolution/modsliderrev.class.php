<?php

/**
 * MODx ModSliderRev Class
 *
 * @package modsliderrevolution
 */
class ModSliderRev
{

    const version = '1.0.0-beta';
    /** @var modX $modx */
    public $modx;
    /** @var array $config */
    public $config = array();
    /** @var pdoTools $pdoTools */
    public $pdoTools;
    public $namespace = 'modsliderrevolution';

    /**
     * ModSliderRev constructor.
     * @param modX $modx
     * @param array $config
     */
    function __construct(modX &$modx, array $config = array())
    {
        $this->modx =& $modx;
        $this->modx->lexicon->load('modsliderrevolution:default');
        $corePath = $modx->getOption('modsliderrevolution.core_path', $config, $modx->getOption('core_path', null, MODX_CORE_PATH) . 'components/modsliderrevolution/');
        $assetsUrl = $modx->getOption('modsliderrevolution.assets_url', $config, $modx->getOption('assets_url') . 'components/modsliderrevolution/');
        $assetsPath = $modx->getOption('modsliderrevolution.assets_path', $config, $modx->getOption('assets_path', null, MODX_ASSETS_PATH) . 'components/modsliderrevolution/');
        $this->config = array_merge(array(
            'chunksPath' => $corePath . 'elements/chunks/',
            'controllersPath' => $corePath . 'controllers/',
            'corePath' => $corePath,
            'assetsUrl' => $assetsUrl,
            'assetsPath' => $assetsPath,
            'revSliderPath' => $assetsPath . 'vendor/revslider/',
            'revSliderUrl' => $assetsUrl . 'vendor/revslider/',
            'modelPath' => $corePath . 'model/',
            'processorsPath' => $corePath . 'processors/',
            'jsUrl' => $assetsUrl . 'js/',
            'cssUrl' => $assetsUrl . 'css/',
            'templatesPath' => $corePath . 'elements/templates/',
            'connectorUrl' => $assetsUrl . 'connector.php',
            'connector_url' => $assetsUrl . 'connector.php',
            'actionUrl' => $assetsUrl . 'action.php',
            'managerUrl' => $this->modx->config['manager_url']
        ), $config);
        $this->modx->addPackage('modsliderrevolution', $this->config['modelPath']);
        // $this->checkStat();
    }


    public function initialize()
    {
        /* $this->getPdoTools();
         $this->pdoTools->setConfig($this->config);*/
    }

    /**
     * @return pdoTools
     */
    public function getPdoToolsInstance()
    {
        if (!is_object($this->pdoTools) || !($this->pdoTools instanceof pdoTools)) {
            $this->pdoTools = $this->modx->getService('pdoFetch');
        }

        return $this->pdoTools;
    }

    /**
     * Shorthand for original modX::invokeEvent() method with some useful additions.
     *
     * @param $eventName
     * @param array $params
     * @param $glue
     *
     * @return array
     */
    public function invokeEvent($eventName, array $params = array(), $glue = '<br/>')
    {
        if (isset($this->modx->event->returnedValues)) {
            $this->modx->event->returnedValues = null;
        }

        $response = $this->modx->invokeEvent($eventName, $params);
        if (is_array($response) && count($response) > 1) {
            foreach ($response as $k => $v) {
                if (empty($v)) {
                    unset($response[$k]);
                }
            }
        }

        $message = is_array($response) ? implode($glue, $response) : trim((string)$response);
        if (isset($this->modx->event->returnedValues) && is_array($this->modx->event->returnedValues)) {
            $params = array_merge($params, $this->modx->event->returnedValues);
        }

        return array(
            'success' => empty($message),
            'message' => $message,
            'data' => $params,
        );
    }

    /**
     * @param string $service
     * @return bool
     */
    public function isExistService($service = '')
    {
        $service = strtolower($service);

        return file_exists(MODX_CORE_PATH . 'components/' . $service . '/model/' . $service . '/');
    }

    /**
     * @param string $key
     * @param string $value
     * @param bool $clearCache
     * @return bool
     */
    public function setOption($key, $value, $clearCache = true)
    {
        if (!$setting = $this->modx->getObject('modSystemSetting', $key)) {
            $setting = $this->modx->newObject('modSystemSetting');
            $setting->set('namespace', $this->namespace);
        }
        $setting->set('value', $value);
        if ($setting->save()) {
            $this->modx->config[$key] = $value;
            if ($clearCache) {
                $this->modx->cacheManager->refresh(array('system_settings' => array()));
            }
            return true;
        }
        return false;
    }

    /**
     * @param string $path
     * @return mixed|string
     */
    public function preparePath($path = '')
    {
        $path = str_replace(array(
            '[[+assets_path]]',
            '[[+base_path]]',
            '[[+core_path]]',
            '[[+mgr_path]]',
        ), array(
            $this->modx->getOption('assets_path', null, MODX_BASE_PATH . 'assets/'),
            $this->modx->getOption('base_path', null, MODX_BASE_PATH),
            $this->modx->getOption('core_path', null, MODX_CORE_PATH),
            $this->modx->getOption('manager_path', null, MODX_MANAGER_PATH),
        ), $path);
        return $path;
    }

    /**
     * @param $array
     * @param string $delimiter
     * @return array
     */
    public function explodeAndClean($array, $delimiter = ',')
    {
        $array = explode($delimiter, $array);
        $array = array_map('trim', $array);
        $array = array_keys(array_flip($array));
        $array = array_filter($array);

        return $array;
    }


    /**
     * @param $array
     * @param string $delimiter
     * @return array|string
     */
    public function cleanAndImplode($array, $delimiter = ',')
    {
        $array = array_map('trim', $array);
        $array = array_keys(array_flip($array));
        $array = array_filter($array);
        $array = implode($delimiter, $array);

        return $array;
    }

    /**
     * @param array $array
     * @return array
     */
    public function cleanArray(array $array = array())
    {
        $array = array_map('trim', $array);
        $array = array_filter($array);
        $array = array_keys(array_flip($array));

        return $array;
    }


    /**
     * @param $needle
     * @param array $array
     * @param bool $all
     * @return array
     */
    public function removeArrayByValue($needle, $array = array(), $all = true)
    {
        if (!$all) {
            if (FALSE !== $key = array_search($needle, $array)) unset($array[$key]);
            return $array;
        }
        foreach (array_keys($array, $needle) as $key) {
            unset($array[$key]);
        }
        return $array;
    }

    /**
     * @param modResource $resource
     * @return bool
     */
    public function isWorkingTemplates(modResource $resource)
    {
        $templates = $this->explodeAndClean($this->modx->getOption('modsliderrevolution.working_templates', null, ''));
        return empty(array_diff($templates, array(''))) || in_array($resource->get('template'), $templates);
    }

    /**
     * @param modManagerController $controller
     * @param bool $isDoc
     */
    public function loadControllerEditorBtnJsCss(modManagerController &$controller, $isDoc = true)
    {

        $use = false;
        $useEditor = $this->modx->getOption('use_editor');
        $whichEditor = $this->modx->getOption('which_editor');
        $btnInCodeEditor = $this->modx->getOption('modsliderrevolution.btn_in_code_editor');
        $whichElementEditor = $this->modx->getOption('which_element_editor');

        if ($useEditor && $isDoc) {
            switch ($whichEditor) {
                case 'TinyMCE':
                    $use = true;
                    $controller->addJavascript($this->config['jsUrl'] . 'mgr/inject/TinyMCE/plugin.js');
                    break;
                case 'CKEditor':
                    $use = true;
                    $controller->addJavascript($this->config['jsUrl'] . 'mgr/inject/CKEditor/plugin.js');
                    break;
                case 'Ace':
                    $use = true;
                    $controller->addLastJavascript($this->config['jsUrl'] . 'mgr/inject/Ace/plugin.js');
                    break;
            }
        }

        if ($btnInCodeEditor) {
            switch ($whichElementEditor) {
                case 'Ace':
                    $use = true;
                    $controller->addLastJavascript($this->config['jsUrl'] . 'mgr/inject/Ace/plugin.js');
                    break;
            }
        }


        $this->modx->controller->addLexiconTopic('modsliderrevolution:sliders');

        $this->modx->controller->addJavascript($this->config['jsUrl'] . 'mgr/modsliderrev.js');
        $this->modx->controller->addJavascript($this->config['jsUrl'] . 'mgr/misc/strftime-min-1.3.js');
        $this->modx->controller->addJavascript($this->config['jsUrl'] . 'mgr/misc/search.combo.js');
        $this->modx->controller->addJavascript($this->config['jsUrl'] . 'mgr/widgets/sliders.view.js');

        $this->modx->controller->addHtml('<script type="text/javascript">
                Ext.onReady(function() {
                    ModSliderRev.config = ' . $this->modx->toJSON($this->config) . ';
                });
                </script>');

        $controller->addCss($this->config['cssUrl'] . 'mgr/main.css');
        $controller->addCss($this->config['cssUrl'] . 'mgr/bootstrap.buttons.css');
        $controller->addCss($this->config['assetsUrl'] . 'vendor/fontawesome/css/font-awesome.min.css');


    }

    /**
     * @param string $whichEditor
     */
    public function editorBtnRegister()
    {
        if (empty($this->modx->getOption('use_editor'))) return;

        $whichEditor = $this->modx->getOption('which_editor');
        switch ($whichEditor) {
            case 'TinyMCE':
                $plugins = $this->explodeAndClean($this->modx->getOption('tiny.custom_plugins'));
                if (in_array('modsliderrev', $plugins)) return;
                $plugins[] = 'modsliderrev';
                $plugins = $this->cleanAndImplode($plugins);
                $this->setOption('tiny.custom_plugins', $plugins);
                $buttons = $this->explodeAndClean($this->modx->getOption('tiny.custom_buttons1'));
                $buttons[] = 'modsliderrev';
                $buttons = $this->cleanAndImplode($buttons);
                $this->setOption('tiny.custom_buttons1', $buttons);
                break;
            case 'CKEditor':
                $plugins = $this->explodeAndClean($this->modx->getOption('ckeditor.extra_plugins'));
                if (in_array('modsliderrev', $plugins)) return;
                $plugins[] = 'modsliderrev';
                $plugins = $this->cleanAndImplode($plugins);
                $this->setOption('ckeditor.extra_plugins', $plugins);
                break;
        }
    }


    /**
     * @return string
     */
    public function getSliderLangCode()
    {
        switch ($this->modx->getOption('manager_language', 'en')) {
            case 'ru':
                return 'ru_RU';
            case 'de':
                return 'de_DE';
            case 'fr_FR ':
                return 'fr';
            case 'hu':
                return 'hu_HU';
            case 'it':
                return 'it_IT';
            case 'pl':
                return 'pl_PL';
            case 'zh':
                return 'pt_BR';
            default :
                return 'en_US';
        }
    }

    /**
     * @return mixed
     */
    public function getSliderBaseUrl()
    {
        return MODX_URL_SCHEME . MODX_HTTP_HOST . $this->config['revSliderUrl'];
    }

    /**
     * @param $file
     * @param $path
     * @return bool|int
     */
    public function unzip($file, $path)
    {
        global $amode;
        // added by Raymond
        $r = substr($path, strlen($path) - 1, 1);
        if ($r != "\\" && $r != "/") $path .= "/";
        if (!extension_loaded('zip')) {
            if (strtoupper(substr(PHP_OS, 0, 3) == 'WIN')) {
                if (!@dl('php_zip.dll')) return 0;
            } else {
                if (!@dl('zip.so')) return 0;
            }
        }
        // end mod
        $zip = zip_open($file);
        if ($zip) {
            $old_umask = umask(0);
            while ($zip_entry = zip_read($zip)) {
                if (zip_entry_filesize($zip_entry) > 0) {
                    // str_replace must be used under windows to convert "/" into "\"
                    $complete_path = $path . str_replace('/', '\\', dirname(zip_entry_name($zip_entry)));
                    $complete_name = $path . str_replace('/', '\\', zip_entry_name($zip_entry));
                    if (!file_exists($complete_path)) {
                        $tmp = '';
                        foreach (explode('\\', $complete_path) AS $k) {
                            $tmp .= $k . '\\';
                            if (!file_exists($tmp)) {
                                @mkdir($tmp, $amode);
                            }
                        }
                    }
                    if (zip_entry_open($zip, $zip_entry, "r")) {
                        $fd = fopen($complete_name, 'w');
                        fwrite($fd, zip_entry_read($zip_entry, zip_entry_filesize($zip_entry)));
                        fclose($fd);
                        zip_entry_close($zip_entry);
                    }
                }
            }
            umask($old_umask);
            zip_close($zip);
            return true;
        }
        zip_close($zip);
    }

    /**
     * @param string $content
     * @return string
     */
    public function prepareContentForFenom($content)
    {
        $fenomParser = filter_var($this->modx->getOption('pdotools_fenom_parser', null, 0, true), FILTER_VALIDATE_BOOLEAN);
        return $fenomParser ? "\n{ignore}\n{$content}\n{/ignore}\n" : $content;
    }

    protected function checkStat()
    {
        $key = strtolower(__CLASS__);
        /** @var modDbRegister $registry */
        $registry = $this->modx->getService('registry', 'registry.modRegistry')
            ->getRegister('user', 'registry.modDbRegister');
        $registry->connect();
        $registry->subscribe('/modstore/' . md5($key));
        if ($res = $registry->read(array('poll_limit' => 1, 'remove_read' => false))) {
            return;
        }
        $c = $this->modx->newQuery('transport.modTransportProvider', array('service_url:LIKE' => '%modstore%'));
        $c->select('username,api_key');
        /** @var modRest $rest */
        $rest = $this->modx->getService('modRest', 'rest.modRest', '', array(
            'baseUrl' => 'https://modstore.pro/extras',
            'suppressSuffix' => true,
            'timeout' => 1,
            'connectTimeout' => 1,
        ));
        if ($rest) {
            $level = $this->modx->getLogLevel();
            $this->modx->setLogLevel(modX::LOG_LEVEL_FATAL);
            $rest->post('stat', array(
                'package' => $key,
                'version' => $this::version,
                'keys' => $c->prepare() && $c->stmt->execute()
                    ? $c->stmt->fetchAll(PDO::FETCH_ASSOC)
                    : array(),
                'uuid' => $this->modx->uuid,
                'database' => $this->modx->config['dbtype'],
                'revolution_version' => $this->modx->version['code_name'] . '-' . $this->modx->version['full_version'],
                'supports' => $this->modx->version['code_name'] . '-' . $this->modx->version['full_version'],
                'http_host' => $this->modx->getOption('http_host'),
                'php_version' => XPDO_PHP_VERSION,
                'language' => $this->modx->getOption('manager_language'),
            ));
            $this->modx->setLogLevel($level);
        }
        $registry->subscribe('/modstore/');
        $registry->send('/modstore/', array(md5($key) => true), array('ttl' => 3600 * 24));
    }
}