<?php

class RevsliderSliders extends xPDOSimpleObject
{


    /**
     * @return array
     */
    public function getThumbAttributes()
    {
        $attr = array();
        if ($slide = $this->getFirstSlide()) {
            $sliderType = $this->getType();
            $attr = $slide->getImageAttributes($sliderType);
        }
        return $attr;
    }

    /**
     * @return null|RevsliderSlides
     */
    public function getFirstSlide()
    {

        $q = $this->xpdo->newQuery('RevsliderSlides');
        $q->where(array(
            'slider_id' => $this->get('id'),
            'slide_order' => 1,
        ));

        return $this->xpdo->getObject('RevsliderSlides', $q);

    }

    /**
     * @return string
     */
    public function getType()
    {
        $isFromPosts = $this->isFromPosts();
        $isFromStream = $this->isFromStream();
        $sliderType = 'gallery';
        if ($isFromPosts == true) {
            $sliderType = 'posts';
            if ($this->getParam('source_type', 'gallery') == 'woocommerce') {
                $sliderType = 'woocommerce';
            }
        } elseif ($isFromStream !== false) {
            switch ($isFromStream) {
                case 'facebook':
                    $sliderType = 'facebook';
                    break;
                case 'twitter':
                    $sliderType = 'twitter';
                    break;
                case 'instagram':
                    $sliderType = 'instagram';
                    break;
                case 'flickr':
                    $sliderType = 'flickr';
                    break;
                case 'youtube':
                    $sliderType = 'youtube';
                    break;
                case 'vimeo':
                    $sliderType = 'vimeo';
                    break;

            }
        }

        return $sliderType;

    }

    /**
     * @return bool
     */
    public function isFromPosts()
    {
        $sourceType = $this->getParam('source_type', 'gallery');
        if ($sourceType == "posts" ||
            $sourceType == "specific_posts" ||
            $sourceType == "current_post" ||
            $sourceType == "woocommerce"
        ) {
            return true;
        }

        return false;
    }

    /**
     * @return bool|string
     */
    public function isFromStream()
    {
        $sourceType = $this->getParam('source_type', 'gallery');
        if (
            $sourceType != "posts" &&
            $sourceType != "specific_posts" &&
            $sourceType != "current_post" &&
            $sourceType != "woocommerce" &&
            $sourceType != "gallery"
        ) {
            return ($sourceType);
        }
        return false;
    }

    /**
     * @param string $key
     * @param null $default
     * @return mixed
     */
    public function getParam($key = '', $default = null)
    {
        return $this->xpdo->getOption($key, $this->get('params'), $default);
    }


}