<?php

class RevsliderModTemplatesGetProcessor extends modObjectGetProcessor
{
    public $classKey = 'RevsliderModTemplates';
    public $languageTopics = array('modsliderrevolution:default','modsliderrevolution:revslidermodtemplates');
    /** @var ModSliderRev $modsliderrev  */
    public $modsliderrev ;

    public function initialize()
{
    // $this->modsliderrev = $this->modx->getService('modsliderrevolution', 'ModSliderRev');
    return parent::initialize();
}

}

return 'RevsliderModTemplatesGetProcessor';