<?php
class RevsliderModTemplatesCreateProcessor extends modObjectCreateProcessor {
    public $classKey = 'RevsliderModTemplates';
    public $languageTopics = array('modsliderrevolution:default','modsliderrevolution:revslidermodtemplates');
    /** @var ModSliderRev $modsliderrev  */
    public $modsliderrev ;

    public function initialize()
    {
        // $this->modsliderrev = $this->modx->getService('modsliderrevolution', 'ModSliderRev');
        return parent::initialize();
    }

    public function beforeSet() {
        // $this->setCheckbox('enable');
        // $parentId = $this->getProperty('parent_id');
        // $rank = $this->modx->getCount($this->classKey ,array('parent_id'=>$parentId)) + 1;
        // $this->setProperty('rank', $rank);
        return parent::beforeSet();
    }
}
return 'RevsliderModTemplatesCreateProcessor';