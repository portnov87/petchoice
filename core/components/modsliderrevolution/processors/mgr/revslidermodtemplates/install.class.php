<?php

class RevsliderModTemplatesInstallProcessor extends modObjectGetProcessor
{
    public $classKey = 'RevsliderModTemplates';
    public $languageTopics = array('modsliderrevolution:default', 'modsliderrevolution:revslidermodtemplates');
    /** @var ModSliderRev $modSliderRev */
    public $modSliderRev;

    public function initialize()
    {
        $this->modSliderRev = $this->modx->getService('modsliderrevolution', 'ModSliderRev');
        return parent::initialize();
    }

    /**
     * Return the response
     * @return array
     */
    public function cleanup()
    {
        $arr = $this->object->toArray();
        if ($this->install($arr['zip'])) {
            return $this->success($this->modx->lexicon('modsliderrevolution.success.install_template'), $arr);
        } else {
            $this->modx->log(modX::LOG_LEVEL_ERROR, 'Error install slider template: ' . $arr['zip']);
            return $this->failure($this->modx->lexicon('modsliderrevolution.err.install_template'));
        }
    }

    /**
     * @param string $zip
     * @return bool
     */
    private function install($zip = '')
    {
        $tmpPath = $this->modSliderRev->config['assetsPath'] . 'tmp/';
        if ($content = file_get_contents($zip)) {
            $file = $tmpPath . basename($zip);
            if (file_put_contents($file, $content)) {
                return $this->sendFile($file);
            }
        }
        return false;
    }

    /**
     * @param string $file
     * @return bool
     */
    private function sendFile($file = '')
    {
        $url = $this->modSliderRev->getSliderBaseUrl() . 'index.php?c=admin&m=ajax';
        $params = array(
            'action' => 'revslider_ajax_action',
            'client_action' => 'import_slider_slidersview',
            'nonce' => '',
            'update_animations' => true,
            'update_navigations' => true,
            'page-creation' => false,
            'import_file' => curl_file_create($file),
        );
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_FAILONERROR, 1);
        curl_setopt($ch, CURLOPT_ENCODING, 'gzip, deflate');
        curl_setopt($ch, CURLOPT_TIMEOUT, 30);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $params);
        curl_setopt($ch, CURLOPT_COOKIE, session_name() . "={$_COOKIE[session_name()]};");

        /* curl_setopt($ch, CURLOPT_HEADER, 1);
         curl_setopt($ch, CURLINFO_HEADER_OUT, true);*/

        $output = curl_exec($ch);
        $code = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        $error = curl_errno($ch);
        $message = curl_error($ch);

        curl_close($ch);

        $result = array(
            'response' => array('code' => $code, 'message' => $message, 'error' => $error),
            'body' => $output
        );
        if ($code == 200 && !empty(stripos($output, 'location.href'))) {
            return true;
        } else {
            $this->modx->log(modX::LOG_LEVEL_ERROR, "Error send file slider template. Info:\n" . print_r($result, 1));
            return false;
        }
    }

}

return 'RevsliderModTemplatesInstallProcessor';