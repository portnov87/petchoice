<?php

class RevsliderModTemplatesGetListProcessor extends modObjectGetListProcessor
{
    public $languageTopics = array('modsliderrevolution:default', 'modsliderrevolution:revslidermodtemplates');
    public $classKey = 'RevsliderModTemplates';
    public $defaultSortField = 'id';
    public $defaultSortDirection = 'ASC';
    public $checkListPermission = true;
    /** @var ModSliderRev $modsliderrev */
    public $modsliderrev;

    public function initialize()
    {
        // $this->modsliderrev = $this->modx->getService('modsliderrevolution', 'ModSliderRev');
        return parent::initialize();
    }

    public function beforeQuery()
    {
        if ($this->getProperty('combo')) {
            $this->setProperty('limit', 0);
        }

        return parent::beforeQuery();
    }

    public function prepareQueryBeforeCount(xPDOQuery $c)
    {
        $query = $this->getProperty('query');
        $filter = $this->getProperty('filter');
        $filter = empty($filter) ? 1 : $filter;

        $c->leftJoin('RevsliderModFilterTemplate', 'FilterTemplate', 'FilterTemplate.template_id = RevsliderModTemplates.id');

        $c->where(array('FilterTemplate.filter_id' => $filter));

        if (!empty($query)) {
            $c->where(array(
                'title:LIKE' => '%' . $query . '%',
                'OR:description:LIKE' => "%{$query}%",
            ));
        }

        return $c;
    }

    public function prepareRow(xPDOObject $object)
    {
        $data = $object->toArray();
        return $data;
    }
}

return 'RevsliderModTemplatesGetListProcessor';