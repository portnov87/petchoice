<?php

class RevsliderModTemplatesMultipleProcessor extends modProcessor
{

    /** @var ModSliderRev $modsliderrev  */
    public $modsliderrev ;

    public function initialize()
{
    $this->modsliderrev = $this->modx->getService('modsliderrevolution', 'ModSliderRev');
    return parent::initialize();
}

    /**
     * @return array|string
     */
    public function process()
    {

        if (!$method = $this->getProperty('method', false)) {
            return $this->failure();
        }
        $ids = json_decode($this->getProperty('ids'), true);
        if (empty($ids)) {
            return $this->success();
        }

        foreach ($ids as $id) {
            $this->modx->error->reset();
            /** @var modProcessorResponse $response */
            $response = $this->modx->runProcessor(
                'mgr/revslidermodtemplates/' . $method,
                array('id' => $id),
                array('processors_path' => $this->modsliderrev->config['processorsPath'])
            );
            if ($response->isError()) {
                return $response->getResponse();
            }
        }

        return $this->success();
    }

}

return 'RevsliderModTemplatesMultipleProcessor';