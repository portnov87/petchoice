<?php
class RevsliderModTemplatesUpdateProcessor extends modObjectUpdateProcessor {
    public $classKey = 'RevsliderModTemplates';
    public $languageTopics = array('modsliderrevolution:default', 'modsliderrevolution:revslidermodtemplates');
    /** @var ModSliderRev $modsliderrev  */
    public $modsliderrev ;

    public function initialize()
    {
        // $this->modsliderrev = $this->modx->getService('modsliderrevolution', 'ModSliderRev');
        return parent::initialize();
    }

    public function beforeSet() {
        //$this->setCheckbox('enable');
        return parent::beforeSet();
    }

}
return 'RevsliderModTemplatesUpdateProcessor';