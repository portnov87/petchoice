<?php
class RevsliderModTemplatesRemoveProcessor extends modObjectRemoveProcessor {
    public $classKey = 'RevsliderModTemplates';
    public $languageTopics = array('modsliderrevolution:default','modsliderrevolution:revslidermodtemplates');
    /** @var ModSliderRev $modsliderrev  */
    public $modsliderrev ;

    public function initialize()
    {
        // $this->modsliderrev = $this->modx->getService('modsliderrevolution', 'ModSliderRev');
        return parent::initialize();
    }

    public function afterRemove()
    {

        /*$sql = "UPDATE {$this->modx->getTableName($this->classKey)} SET `rank`=`rank`-1 WHERE `rank`>{$this->object->get('rank')}
        // AND  parent_id = {$this->object->get('parent_id')
        ";
        $this->modx->exec($sql);*/

        return parent::afterRemove();
    }

}
return 'RevsliderModTemplatesRemoveProcessor';