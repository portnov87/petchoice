<?php

class RevsliderSlidersGetListProcessor extends modObjectGetListProcessor
{
    public $languageTopics = array('modsliderrevolution:default', 'modsliderrevolution:sliders');
    public $classKey = 'RevsliderSliders';
    public $defaultSortField = 'id';
    public $defaultSortDirection = 'ASC';
    public $checkListPermission = true;
    /** @var ModSliderRev $modsliderrev */
    public $modsliderrev;

    public function initialize()
    {
        // $this->modsliderrev = $this->modx->getService('modsliderrevolution', 'ModSliderRev');
        return parent::initialize();
    }

    public function beforeQuery()
    {
        if ($this->getProperty('combo')) {
            $this->setProperty('limit', 0);
        }

        return parent::beforeQuery();
    }

    public function prepareQueryBeforeCount(xPDOQuery $c)
    {
        $query = $this->getProperty('query');

        if (!empty($query)) {
            $c->where(array(
                'title:LIKE' => '%' . $query . '%',
                'OR:alias:LIKE' => "%{$query}%",
            ));
        }

        $c->where(array(
            'type:!='=> 'template'
        ));

        return $c;
    }

    public function prepareRow(xPDOObject $object)
    {
        $data = $object->toArray();
        if ($attr = $object->getThumbAttributes()) {
            $data = array_merge($data, $attr);
        }

        $data['actions'] = array(
            array(
                'cls' => array(
                    'menu' => '',
                    'button' => '',
                ),
                'icon' => 'icon icon-edit',
                'title' => $this->modx->lexicon('modsliderrevolution.sliders.menu.update'),
                'action' => 'update',
                'button' => false,
                'menu' => true,
            ),
            array(
                'cls' => array(
                    'menu' => 'green',
                    'button' => 'green',
                ),
                'icon' => 'icon icon-code',
                'title' => $this->modx->lexicon('modsliderrevolution.sliders.menu.insert'),
                'multiple' => $this->modx->lexicon('modsliderrevolution.sliders.menu..multiple_insert'),
                'action' => 'insert',
                'button' => false,
                'menu' => true,
            ),
        );

        return $data;
    }


}

return 'RevsliderSlidersGetListProcessor';