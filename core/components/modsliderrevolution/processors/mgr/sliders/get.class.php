<?php

class RevsliderSlidersGetProcessor extends modObjectGetProcessor
{
    public $languageTopics = array('modsliderrevolution:default', 'modsliderrevolution:sliders');
    public $classKey = 'RevsliderSliders';
    public $checkListPermission = true;
    /** @var ModSliderRev $modsliderrev */
    public $modsliderrev;

    public function initialize()
    {
        // $this->modsliderrev = $this->modx->getService('modsliderrevolution', 'ModSliderRev');
        return parent::initialize();
    }

    public function cleanup()
    {
        $data = $this->object->toArray();
        if ($attr = $this->object->getThumbAttributes()) {
            $data = array_merge($data, $attr);
        }
        return $this->success('', $data);
    }
}

return 'RevsliderSlidersGetProcessor';