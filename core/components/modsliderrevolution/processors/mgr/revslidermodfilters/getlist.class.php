<?php

class RevsliderModFiltersGetListProcessor extends modObjectGetListProcessor
{
    public $languageTopics = array('modsliderrevolution:default', 'modsliderrevolution:revslidermodfilters');
    public $classKey = 'RevsliderModFilters';
    public $defaultSortField = 'id';
    public $defaultSortDirection = 'ASC';
    public $checkListPermission = true;
    /** @var ModSliderRev $modsliderrev */
    public $modsliderrev;

    public function initialize()
    {
        // $this->modsliderrev = $this->modx->getService('modsliderrevolution', 'ModSliderRev');
        return parent::initialize();
    }

    public function prepareRow(xPDOObject $object)
    {
        $data = $object->toArray();
        $data['name'] = $this->modx->lexicon('modsliderrevolution.' . $data['key']);
        return $data;
    }
}

return 'RevsliderModFiltersGetListProcessor';