<?php
/**
 * Setting Russian Lexicon Entries for modSliderRevolution
 *
 * @package modsliderrevolution
 * @subpackage lexicon
 */

$_lang['modsliderrevolution_prop_tpl'] = 'Чанк оформления';
$_lang['modsliderrevolution_prop_frontendCss'] = 'Стиль фронтенда';
$_lang['modsliderrevolution_prop_frontendJs'] = 'Скрипт фронтенда';