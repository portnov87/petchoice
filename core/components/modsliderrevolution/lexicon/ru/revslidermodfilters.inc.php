<?php
/**
 * RevsliderModFilters Russian Lexicon Entries for modSliderRevolution
 *
 * @package modsliderrevolution
 * @subpackage lexicon
 */
$_lang['modsliderrevolution.filterall'] = 'All Sliders';
$_lang['modsliderrevolution.filter-full-width-2'] = 'Full-Width';
$_lang['modsliderrevolution.filter-boxed'] = 'Auto-Size';
$_lang['modsliderrevolution.filter-carousel'] = 'Carousel';
$_lang['modsliderrevolution.filter-front-page'] = 'Front Page';
$_lang['modsliderrevolution.filter-full-screen-2'] = 'Full-Screen';
$_lang['modsliderrevolution.filter-hero-scene'] = 'Hero Scene';
$_lang['modsliderrevolution.filter-menu-navigation'] = 'Menu Navigation';
$_lang['modsliderrevolution.filter-parallax'] = 'Parallax';
$_lang['modsliderrevolution.filter-scroll-navigation'] = 'Scroll Navigation';
$_lang['modsliderrevolution.filter-social-stream'] = 'Social Stream';
$_lang['modsliderrevolution.filter-special-content'] = 'Special Content';
$_lang['modsliderrevolution.filter-tabs'] = 'Tabs';
$_lang['modsliderrevolution.filter-themepunch'] = 'ThemePunch';
$_lang['modsliderrevolution.filter-timer'] = 'Timer';
$_lang['modsliderrevolution.filter-html5-video'] = 'HTML5 Video';
$_lang['modsliderrevolution.filter-vimeo-video'] = 'Vimeo Video';
$_lang['modsliderrevolution.filter-youtube-video'] = 'YouTube Video';
$_lang['modsliderrevolution.filter-empty'] = 'Нет данных';
