<?php
/**
 * Setting Russian Lexicon Entries for modSliderRevolution
 *
 * @package modsliderrevolution
 * @subpackage lexicon
 */
$_lang['modsliderrevolution.setting_basic'] = 'Основные настройки';
$_lang['setting_modsliderrevolution.enable_widgets'] = 'Показывать виджеты';
$_lang['setting_modsliderrevolution.enable_widgets_desc'] = '';
$_lang['setting_modsliderrevolution.btn_in_code_editor'] = 'Добавить кнопку в редактор кода';
$_lang['setting_modsliderrevolution.btn_in_code_editor_desc'] = 'В редактор кода будет добавлена кнопка вставки слайдера';