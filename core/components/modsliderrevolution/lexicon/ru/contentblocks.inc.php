<?php
/**
 * ContentBlocks Russian Lexicon Entries for modSliderRevolution
 *
 * @package modsliderrevolution
 * @subpackage lexicon
 */
$_lang['modsliderrevolution.input_name'] = 'Slider Revolution';
$_lang['modsliderrevolution.input_description'] = '';
$_lang['modsliderrevolution.btn.add'] = 'Добавить слайдер';
$_lang['modsliderrevolution.btn.update'] = 'Редактировать слайдер';
$_lang['modsliderrevolution.btn.refresh'] = 'Обновить слайдер';
$_lang['modsliderrevolution.btn.remove'] = 'Удалить слайдер';
$_lang['modsliderrevolution.property.cover'] = 'Только обложка';
$_lang['modsliderrevolution.property.cover_desc'] = '';
$_lang['modsliderrevolution.property.thumb_size'] = 'Размер слайдера';
$_lang['modsliderrevolution.property.thumb_size_desc'] = '';