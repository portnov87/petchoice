<?php
/**
 * Default Russian Lexicon Entries for modSliderRevolution
 *
 * @package modsliderrevolution
 * @subpackage lexicon
 */
$_lang['modsliderrevolution'] = 'modSliderRevolution';
$_lang['modsliderrevolution.cmenu.sliders'] = 'Slider Revolution';
$_lang['modsliderrevolution.cmenu.sliders_desc'] = '';
$_lang['modsliderrevolution.cmenu.revslidermodtemplates'] = 'Шаблоны слайдеров';
$_lang['modsliderrevolution.cmenu.revslidermodtemplates_desc'] = '';