<?php
/**
 * Sliders Russian Lexicon Entries for modSliderRevolution
 *
 * @package modsliderrevolution
 * @subpackage lexicon
 */
$_lang['modsliderrevolution.sliders.page.title'] = 'modSliderRevolution';
$_lang['modsliderrevolution.sliders.win.title'] = 'Вставка слайдера';
$_lang['modsliderrevolution.sliders.btn.rtf'] = 'Slider Revolution';
$_lang['modsliderrevolution.sliders.btn.insert'] = 'Вставить выбранный слайдер';
$_lang['modsliderrevolution.sliders.btn.editor'] = 'Редактор слайдеров';
$_lang['modsliderrevolution.sliders.menu.update'] = 'Редактировать слайдер';
$_lang['modsliderrevolution.sliders.menu.insert'] = 'Вставить слайдер';
$_lang['modsliderrevolution.sliders.menu..multiple_insert'] = 'Редактор слайдеров';
$_lang['modsliderrevolution.sliders.msg.view_empty'] = 'У вас еще нет не одного слайдера';