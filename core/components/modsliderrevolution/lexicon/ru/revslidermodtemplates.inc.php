<?php
/**
 * RevsliderModTemplates Russian Lexicon Entries for modSliderRevolution
 *
 * @package modsliderrevolution
 * @subpackage lexicon
 */

$_lang['modsliderrevolution.label_title'] = 'title';
$_lang['modsliderrevolution.label_title_help'] = '';
$_lang['modsliderrevolution.label_description'] = 'description';
$_lang['modsliderrevolution.label_description_help'] = '';
$_lang['modsliderrevolution.label_preview'] = 'preview';
$_lang['modsliderrevolution.label_preview_help'] = '';
$_lang['modsliderrevolution.label_img'] = 'img';
$_lang['modsliderrevolution.label_img_help'] = '';
$_lang['modsliderrevolution.label_zip'] = 'zip';
$_lang['modsliderrevolution.label_zip_help'] = '';
$_lang['modsliderrevolution.page.revslidermodtemplates_title'] = 'Шаблоны слайдеров';
$_lang['modsliderrevolution.tab.revslidermodtemplates'] = 'Шаблоны слайдеров';
$_lang['modsliderrevolution.btn.preview'] = 'Demo';
$_lang['modsliderrevolution.btn.install'] = 'Установить';
$_lang['modsliderrevolution.btn.download'] = 'Скачать';
$_lang['modsliderrevolution.btn.editor'] = 'Редактор слайдеров';
$_lang['modsliderrevolution.success.install_template'] = 'Шаблон слайдер успешно установлен!';
$_lang['modsliderrevolution.err.install_template'] = 'При установки шаблона слайдера произошла ошибка!';