<?php
/**
 * Setting English Lexicon Entries for modSliderRevolution
 *
 * @package modsliderrevolution
 * @subpackage lexicon
 */
$_lang['modsliderrevolution.setting_basic'] = 'Basic settings';
$_lang['setting_modsliderrevolution.enable_widgets'] = 'Enable show widgets';
$_lang['setting_modsliderrevolution.enable_widgets_desc'] = '';
$_lang['setting_modsliderrevolution.btn_in_code_editor'] = 'Add button to the code editor';
$_lang['setting_modsliderrevolution.btn_in_code_editor_desc'] = 'The button for inserting the slider will be added to the code editor';