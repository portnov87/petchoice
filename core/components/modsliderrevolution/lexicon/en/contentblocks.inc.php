<?php
/**
 * ContentBlocks English Lexicon Entries for modSliderRevolution
 *
 * @package modsliderrevolution
 * @subpackage lexicon
 */
$_lang['modsliderrevolution.input_name'] = 'Slider Revolution';
$_lang['modsliderrevolution.input_description'] = '';
$_lang['modsliderrevolution.btn.add'] = 'Add slider';
$_lang['modsliderrevolution.btn.update'] = 'Edit slider';
$_lang['modsliderrevolution.btn.refresh'] = 'Refresh slider';
$_lang['modsliderrevolution.btn.remove'] = 'Delete slider';
$_lang['modsliderrevolution.property.cover'] = 'Cover only';
$_lang['modsliderrevolution.property.cover_desc'] = '';
$_lang['modsliderrevolution.property.thumb_size'] = 'Slider size';
$_lang['modsliderrevolution.property.thumb_size_desc'] = '';