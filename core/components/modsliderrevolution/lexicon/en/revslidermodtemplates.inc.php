<?php
/**
 * RevsliderModTemplates English Lexicon Entries for modSliderRevolution
 *
 * @package modsliderrevolution
 * @subpackage lexicon
 */
$_lang['modsliderrevolution.label_title'] = 'title';
$_lang['modsliderrevolution.label_title_help'] = '';
$_lang['modsliderrevolution.label_description'] = 'description';
$_lang['modsliderrevolution.label_description_help'] = '';
$_lang['modsliderrevolution.label_preview'] = 'preview';
$_lang['modsliderrevolution.label_preview_help'] = '';
$_lang['modsliderrevolution.label_img'] = 'img';
$_lang['modsliderrevolution.label_img_help'] = '';
$_lang['modsliderrevolution.label_zip'] = 'zip';
$_lang['modsliderrevolution.label_zip_help'] = '';
$_lang['modsliderrevolution.page.revslidermodtemplates_title'] = 'Slider templates';
$_lang['modsliderrevolution.tab.revslidermodtemplates'] = 'Slider templates';
$_lang['modsliderrevolution.btn.preview'] = 'Demo';
$_lang['modsliderrevolution.btn.install'] = 'Install';
$_lang['modsliderrevolution.btn.download'] = 'Download';
$_lang['modsliderrevolution.btn.editor'] = 'Sliders editor ';
$_lang['modsliderrevolution.success.install_template'] = 'The slider template was successfully installed!';
$_lang['modsliderrevolution.err.install_template'] = 'When installing a slider template, an error occurred!';