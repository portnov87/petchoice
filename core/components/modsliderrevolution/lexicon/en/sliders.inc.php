<?php
/**
 * Sliders Russian Lexicon Entries for modSliderRevolution
 *
 * @package modsliderrevolution
 * @subpackage lexicon
 */

$_lang['modsliderrevolution.sliders.page.title'] = 'modSliderRevolution';
$_lang['modsliderrevolution.sliders.win.title'] = 'Inserting a slider';
$_lang['modsliderrevolution.sliders.btn.rtf'] = 'Slider Revolution';
$_lang['modsliderrevolution.sliders.btn.insert'] = 'Insert selected slider';
$_lang['modsliderrevolution.sliders.btn.editor'] = 'Slider editor';
$_lang['modsliderrevolution.sliders.menu.update'] = 'Edit slider';
$_lang['modsliderrevolution.sliders.menu.insert'] = 'Insert slider';
$_lang['modsliderrevolution.sliders.menu..multiple_insert'] = '';
$_lang['modsliderrevolution.sliders.msg.view_empty'] = 'You do not yet have one slider';