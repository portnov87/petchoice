<?php
/**
 * Setting English Lexicon Entries for modSliderRevolution
 *
 * @package modsliderrevolution
 * @subpackage lexicon
 */

$_lang['modsliderrevolution_prop_tpl'] = 'Chunk for template';
$_lang['modsliderrevolution_prop_frontendCss'] = 'Frontend style';