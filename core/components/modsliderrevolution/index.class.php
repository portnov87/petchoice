<?php
/**
 * @package modsliderrevolution
 * @subpackage controllers
 */
require_once dirname(__FILE__) . '/model/modsliderrevolution/modsliderrev.class.php';

class IndexManagerController extends modExtraManagerController
{
    public static function getDefaultController()
    {
        return 'index';
    }
}


abstract class ModSliderRevMainController extends modExtraManagerController
{
    /** @var ModSliderRev $modSliderRev */
    public $modSliderRev;

    public static function getInstance(modX &$modx, $className, array $config = array())
    {
        $action = call_user_func(array($className, 'getDefaultController'));
        if (isset($_REQUEST['a'])) {
            $action = str_replace(array('../', './', '.', '-', '@'), '', $_REQUEST['a']);
        }
        $className = self::getControllerClassName($action, $config['namespace']);
        $classPath = $config['namespace_path'] . 'controllers/default/' . $action . '.class.php';
        require_once $classPath;
        /** @var modManagerController $controller */
        $controller = new $className($modx, $config);
        return $controller;
    }

    public function initialize()
    {
        $this->modSliderRev = new ModSliderRev($this->modx);
        $this->modSliderRev->initialize();
        $this->addJavascript($this->modSliderRev->config['jsUrl'] . 'mgr/modsliderrev.js');
        $this->addHtml('<script type="text/javascript">
        Ext.onReady(function() {
            ModSliderRev.config = ' . $this->modx->toJSON($this->modSliderRev->config) . ';
        });
        </script>');
        return parent::initialize();
    }

    public function getLanguageTopics()
    {
        return array('modsliderrevolution:default');
    }

    public function checkPermissions()
    {
        return true;
    }

    /**
     * Get the page footer for the controller.
     * @param string $content
     * @return string
     */
    public function getFooter()
    {
        return $this->getInjectContent() . parent::getFooter();
    }


    /**
     * @return string
     */
    public function getInjectContent()
    {
        return '';
    }

}