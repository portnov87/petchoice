<?php

/**
 * Remove an Items
 */
class PromocodeItemRemoveProcessor extends modObjectProcessor {
	public $objectType = 'PromocodeItem';
	public $classKey = 'PromocodeItem';
	public $languageTopics = array('modextra');
	//public $permission = 'remove';


	/**
	 * @return array|string
	 */
	public function process() {
		//if (!$this->checkPermissions()) {
		//	return $this->failure($this->modx->lexicon('access_denied'));
		//}

		$id = $this->modx->fromJSON($this->getProperty('id'));
		if (empty($id)) {
			return $this->failure($this->modx->lexicon('modextra_item_err_ns'));
		}

		//foreach ($ids as $id) {
			/** @var PromocodeItem $object */
			if (!$object = $this->modx->getObject($this->classKey, $id)) {
				return $this->failure($this->modx->lexicon('modextra_item_err_nf'));
			}

			$object->remove();
		//}

		return $this->success();
	}

}

return 'PromocodeItemRemoveProcessor';