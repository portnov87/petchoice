<?php

/**
 * Enable an Item
 */
class PromocodeItemEnableProcessor extends modObjectProcessor {
	public $objectType = 'PromocodeItem';
	public $classKey = 'PromocodeItem';
	public $languageTopics = array('modextra');
	//public $permission = 'save';


	/**
	 * @return array|string
	 */
	public function process() {
		if (!$this->checkPermissions()) {
			return $this->failure($this->modx->lexicon('access_denied'));
		}

		$ids = $this->modx->fromJSON($this->getProperty('ids'));
		if (empty($ids)) {
			return $this->failure($this->modx->lexicon('modextra_item_err_ns'));
		}

		foreach ($ids as $id) {
			/** @var PromocodeItem $object */
			if (!$object = $this->modx->getObject($this->classKey, $id)) {
				return $this->failure($this->modx->lexicon('modextra_item_err_nf'));
			}

			$object->set('status', 0);
			$object->save();
		}

		return $this->success();
	}

}

return 'PromocodeItemEnableProcessor';
