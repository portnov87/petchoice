<?php

/**
 * Get an Item
 */
class PromocodeItemGetProcessor extends modObjectGetProcessor {
	public $objectType = 'PromocodeItem';
	public $classKey = 'PromocodeItem';
	public $languageTopics = array('modextra:default');
	//public $permission = 'view';


	/**
	 * We doing special check of permission
	 * because of our objects is not an instances of modAccessibleObject
	 *
	 * @return mixed
	 */
	public function process() {
		if (!$this->checkPermissions()) {
			return $this->failure($this->modx->lexicon('access_denied'));
		}
echo '<pre>';
print_r($this->object);
echo '</pre>';die();
		return parent::process();
	}

}

return 'PromocodeItemGetProcessor';