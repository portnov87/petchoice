<?php
/**
 * modExtra
 *
 * Copyright 2010 by Shaun McCormick <shaun+modextra@modx.com>
 *
 * modExtra is free software; you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 2 of the License, or (at your option) any later
 * version.
 *
 * modExtra is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * modExtra; if not, write to the Free Software Foundation, Inc., 59 Temple
 * Place, Suite 330, Boston, MA 02111-1307 USA
 *
 * @package modextra
 */
/**
 * Create an Item
 * 
 * @package modextra
 * @subpackage processors
 *//*
$alreadyExists = $modx->getObject('promocode',array(
    'code' => $_POST['code'],
));
if ($alreadyExists) {
    $modx->error->addField('name',$modx->lexicon('modextra.item_err_ae'));
}

if ($modx->error->hasError()) {
    return $modx->error->failure();
}*/


//надо сгенерировать промокоды
/*
echo '<pre>';
print_R($_POST);
echo '</pre>';*/
//$modx->setDebug(true);
function validate($modx)
{
	//if ($code=='') 
	$code=rand();
	$item = $modx->getObject('PromocodeItem', array('code'=>$code));
	if (!$item) return $code;
	else validate($modx);
}
if ($_POST['type']=='dinamic')
{
	if(isset($_POST['status'])) {
		if($_POST['status']==true) $status=1;
		else $status=0;
	}else $status=1;
			$created=date('Y-m-d');
			if(isset($_POST['term'])) $term=date('Y-m-d',strtotime($_POST['term']));
			else $term='';
	//$arraypost=array();//discount
	if ($_POST['code']==''){		
		for ($i = 1; $i <= $_POST['count']; $i++) {			
			// надо проверить есть ли такой код
			$code=validate($modx);
			
			
			$arraypost=array(
				'code'=>$code,
				'name'=>$_POST['name'],
				'term'=>$term,
				'category'=>$_POST['category'],
				'type'=>$_POST['type'],
				'status'=>$status,
				'discount'=>$_POST['discount'],
				'summa'=>(float)$_POST['summa'],
				'created'=>$created
			);
			
			
			$item = $modx->newObject('PromocodeItem',$arraypost);
			
			if ($item->save() == false) {
				return $modx->error->failure($modx->lexicon('modextra.item_err_save'));
			}else{
				if(isset($_POST['category']))
				{
					$item_category = $modx->newObject('PromocodeCategory',array('category'=>$_POST['category'],'promocode'=>$item->get('id')));
					$item_category->save();
				}
			}
		}
	}
	else{
		
		$arraypost=array(
			'code'=>$_POST['code'],
			'name'=>$_POST['name'],
			'term'=>$term,
			'category'=>$_POST['category'],
			'type'=>$_POST['type'],
			'status'=>$status,
			'discount'=>$_POST['discount'],			
			'summa'=>$_POST['summa']
		);
		$item = $modx->newObject('PromocodeItem',$arraypost);
		
		//$item->fromArray($arraypost);
		//$item->save();
		if ($item->save() == false) {
				return $modx->error->failure($modx->lexicon('modextra.item_err_save'));
			}
			else{
				if(isset($_POST['category']))
				{
					$item_category = $modx->newObject('PromocodeCategory',array('category'=>$_POST['category'],'promocode'=>$item->get('id')));
					$item_category->save();
				}
			}
	}

	
}else{ // статика т.е. только сформировать один код указаный
	if ($_POST['status']=='true') $status=1; 
	else $status=0;
	$created=date('Y-m-d');
	if(isset($_POST['term'])) $term=date('Y-m-d',strtotime($_POST['term']));
	else $term='0000-00-00';
			
	if (($_POST['code']=='')||(!isset($_POST['code']))) $code=validate($modx);
	else $code=$_POST['code'];
			
	$arraypost=array(
			'code'=>$code,
			'name'=>$_POST['name'],
			'term'=>$term,
			'category'=>$_POST['category'],
			'type'=>$_POST['type'],
			'status'=>$status,
			'discount'=>$_POST['discount'],
			'summa'=>$_POST['summa'],
			'created'=>$created,
		);
	$item = $modx->newObject('PromocodeItem',$arraypost);
	//$item->fromArray($arraypost);
	if ($item->save() == false) {
		return $modx->error->failure($modx->lexicon('modextra.item_err_save'));
	}else{
		if(isset($_POST['category']))
				{
					
					$item_category = $modx->newObject('PromocodeCategory');
					$item_category->set('category',(int)$_POST['category']);
					$item_category->set('promocode',(int)$item->get('id'));
					//,array('category'=>(int)$_POST['category'],'promocode'=>(int)$item->get('id')));
					$item_category->save();
				}
	}
}
die();


return $modx->error->success('',$item);