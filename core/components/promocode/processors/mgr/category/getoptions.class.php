<?php

class msCategoryGetOptionsProcessor extends modObjectProcessor {
	public $classKey = 'msCategory';


	/** {@inheritDoc} */
	public function process() {
		$query = trim($this->getProperty('query'));
		$limit = trim($this->getProperty('limit', 10));
		$key = $this->getProperty('key');

		$c = $this->modx->newQuery('msCategory');
		$c->sortby('pagetitle','ASC');
		$c->select('pagetitle as value');
		$c->groupby('pagetitle');
		//$c->where(array('key' => $key));
		$c->limit($limit);
		if (!empty($query)) {
			$c->where(array('pagetitle:LIKE' => "%{$query}%"));
		}
		$found = false;
		if ($c->prepare() && $c->stmt->execute()) {
			$res = $c->stmt->fetchAll(PDO::FETCH_ASSOC);
			foreach ($res as $v) {
				if ($v['pagetitle'] == $query) {
					$found = true;
				}
			}
		}
		else {$res = array();}

		if (!$found && !empty($query)) {
			$res = array_merge_recursive(array(array('value' => $query)), $res);
		}

		return $this->outputArray($res);
	}

}

return 'msCategoryGetOptionsProcessor';