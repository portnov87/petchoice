<?php
/**
 * promocode
 *
 * Copyright 2010 by Shaun McCormick <shaun+promocode@modx.com>
 *
 * promocode is free software; you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 2 of the License, or (at your option) any later
 * version.
 *
 * promocode is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * promocode; if not, write to the Free Software Foundation, Inc., 59 Temple
 * Place, Suite 330, Boston, MA 02111-1307 USA
 *
 * @package promocode
 */
require_once dirname(__FILE__) . '/model/promocode/promocode.class.php';
//require_once dirname(__FILE__) . '/model/promocode/mysql/promocode.class.php';
/**
 * @package promocode
 */
 
 

abstract class promocodeBaseManagerController extends modExtraManagerController {
    
    public $promocode;
    public function initialize() {
        $this->promocode = new promocode($this->modx);

        $this->addCss($this->promocode->config['cssUrl'].'mgr.css');
        $this->addJavascript($this->promocode->config['jsUrl'].'mgr/promocode.js');
        $this->addHtml('<script type="text/javascript">
        Ext.onReady(function() {
            promocode.config = '.$this->modx->toJSON($this->promocode->config).';
            promocode.config.connector_url = "'.$this->promocode->config['connectorUrl'].'";
        });
        </script>');
        return parent::initialize();
    }
    public function getLanguageTopics() {
        return array('promocode:default');
    }
    public function checkPermissions() { return true;}
}
class IndexManagerController extends promocodeBaseManagerController {
    public static function getDefaultController() { return 'index'; }
}