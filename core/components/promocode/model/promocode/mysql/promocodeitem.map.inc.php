<?php
$xpdo_meta_map['PromocodeItem']= array (
  'package' => 'promocode',
  'version' => '1.1',
  'table' => 'promo_promocode',
  'extends' => 'xPDOSimpleObject',
  'fields' => 
  array (
    'code' => NULL,
    'name' => NULL,
    'type' => NULL,
    'term' => NULL,
    'category' => NULL,
    'discount' => NULL,
    'created' => NULL,
    'date_active' => NULL,
    'status' => NULL,
    'order_id' => NULL,
    'summa' => NULL,
	'order_num'=> NULL,
	'email'=> NULL,
	'removeloylnost'=>NULL
  ),
  'fieldMeta' => 
  array (
    'code' => 
    array (
      'dbtype' => 'varchar',
      'precision' => '255',
      'phptype' => 'string',
      'null' => false,
    ),
	
    'name' => 
    array (
      'dbtype' => 'varchar',
      'precision' => '255',
      'phptype' => 'string',
      'null' => false,
    ),
    'type' => 
    array (
      'dbtype' => 'varchar',
      'precision' => '50',
      'phptype' => 'string',
      'null' => false,
    ),
    'term' => 
    array (
        'dbtype' => 'datetime',
      'phptype' => 'datetime',
      'null' => true,
      'default' => '0000-00-00 00:00:00',
    ),
    'category' => 
    array (
      'dbtype' => 'int',
      'precision' => '11',
      'phptype' => 'integer',
      'null' => false,
    ),
	'email' => 
    array (
      'dbtype' => 'int',
      'precision' => '11',
      'phptype' => 'integer',
      'null' => false,
    ),	
    'discount' => 
    array (
      'dbtype' => 'varchar',
      'precision' => '50',
      'phptype' => 'string',
      'null' => false,
    ),
    'created' => 
    array (
     
	   'dbtype' => 'datetime',
      'phptype' => 'datetime',
      'null' => true,
      'default' => '0000-00-00 00:00:00',
    ),
    'date_active' => 
    array (
        'dbtype' => 'datetime',
      'phptype' => 'datetime',
      'null' => true,
      'default' => '0000-00-00 00:00:00',
    ),
    'status' => 
    array (
      'dbtype' => 'int',
      'precision' => '2',
      'phptype' => 'integer',
      'null' => false,
    ),
	'removeloylnost' => 
    array (
      'dbtype' => 'int',
      'precision' => '2',
      'phptype' => 'integer',
      'null' => false,
	  'default' => 0,
    ),
	
    'order_id' => 
    array (
      'dbtype' => 'int',
      'precision' => '11',
      'phptype' => 'integer',
      'null' => false,
    ),
    'summa' => 
    array (
      'dbtype' => 'float',
      'phptype' => 'float',
      'null' => false,
    ),
	  'order_num' => 
    array (
      'dbtype' => 'varchar',
      'precision' => '50',
      'phptype' => 'string',
      'null' => false,
    ),
	
  ),
  
  'composites' => 
  array (   
    'Categories' => 
    array (
      'class' => 'PromocodeCategory',
      'local' => 'id',
      'foreign' => 'promocode',
      'cardinality' => 'many',
      'owner' => 'local',
    ),
  ),
  'aggregates' => 
  array (
    'Category' => 
    array (
      'class' => 'msCategory',
      'local' => 'category',
      'foreign' => 'id',
      'cardinality' => 'one',
      'owner' => 'foreign',
    ),
  ),
  
);
