<?php
$xpdo_meta_map['PromocodeCategory']= array (
  'package' => 'promocode',
  'version' => '1.1',
  'table' => 'promo_promocode_category',
  'extends' => 'xPDOSimpleObject',//'xPDOObject',
  'fields' => 
  array (
    'promocode' => NULL,
    'category' => NULL,
  ),
  'fieldMeta' => 
  array (
    'promocode' => 
    array (
      'dbtype' => 'int',
      'precision' => '10',
      'attributes' => 'unsigned',
      'phptype' => 'integer',
      'null' => false,
      'index' => 'pk',
    ),
    'category' => 
    array (
      'dbtype' => 'int',
      'precision' => '10',
      'attributes' => 'unsigned',
      'phptype' => 'integer',
      'null' => false,
      'index' => 'pk',
    ),
  ), 
);


