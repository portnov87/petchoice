<?php
require_once dirname(dirname(__FILE__)) . '/index.class.php';
/**
 * promocode
 *
 * Copyright 2010 by Shaun McCormick <shaun+modextra@modx.com>
 *
 * promocode is free software; you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 2 of the License, or (at your option) any later
 * version.
 *
 * promocode is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * promocode; if not, write to the Free Software Foundation, Inc., 59 Temple
 * Place, Suite 330, Boston, MA 02111-1307 USA
 *
 * @package modextra
 */
/**
 * Loads the home page.
 *
 * @package modextra
 * @subpackage controllers
 */
class promocodeIndexManagerController extends promocodeBaseManagerController {
    public function process(array $scriptProperties = array()) {

    }
    public function getPageTitle() { return $this->modx->lexicon('promocode'); }
    public function loadCustomCssJs() {
		
		$mgrUrl = $this->modx->getOption('manager_url', null, MODX_MANAGER_URL);
        $this->addJavascript($this->promocode->config['jsUrl'].'mgr/widgets/items.grid.js');
        $this->addJavascript($this->promocode->config['jsUrl'].'mgr/widgets/home.panel.js');
        $this->addLastJavascript($this->promocode->config['jsUrl'].'mgr/sections/home.js');
		$this->addLastJavascript($this->promocode->config['jsUrl'].'mgr/widgets/category.tree.js');
		
		
		$minishopAssetsUrl = $this->modx->getOption('minishop2.assets_url', null, $this->modx->getOption('assets_url', null, MODX_ASSETS_URL) . 'components/minishop2/');
		$connectorUrl = $minishopAssetsUrl . 'connector.php';
		$minishopJsUrl = $minishopAssetsUrl . 'js/mgr/';
		$minishopImgUrl = $minishopAssetsUrl . 'img/mgr/';
		
		
		$this->addJavascript($mgrUrl . 'assets/modext/util/datetime.js');
		$this->addJavascript($mgrUrl . 'assets/modext/widgets/element/modx.panel.tv.renders.js');
		$this->addJavascript($mgrUrl . 'assets/modext/widgets/resource/modx.grid.resource.security.local.js');
		//$this->addJavascript($minishopJsUrl . 'product/product.tv.js');
		$this->addJavascript($mgrUrl . 'assets/modext/widgets/resource/modx.panel.resource.js');
		$this->addJavascript($mgrUrl . 'assets/modext/sections/resource/create.js');
		//$this->addJavascript($minishopJsUrl . 'minishop2.js');
		$this->addJavascript($minishopJsUrl . 'misc/ms2.combo.js');
		$this->addJavascript($minishopJsUrl . 'misc/ms2.utils.js');
		
		
		$this->addHtml('
		<script type="text/javascript">
		// <![CDATA[
		miniShop2.config = {
			assets_url: "' . $minishopAssetsUrl . '"
			,connector_url: "' . $connectorUrl . '"
			,show_comments: false
			,logo_small: "' . $minishopImgUrl . 'ms2_small.png"
			,main_fields: ' . json_encode($product_main_fields) . '
			,extra_fields: ' . json_encode($product_extra_fields) . '
			,vertical_tabs: ' . $this->modx->getOption('ms2_product_vertical_tabs', null, true) . '
			,product_tab_extra: ' . $this->modx->getOption('ms2_product_tab_extra', null, true) . '
			,product_tab_gallery: ' . $this->modx->getOption('ms2_product_tab_gallery', null, true) . '
			,product_tab_links: ' . $this->modx->getOption('ms2_product_tab_links', null, true) . '
			,data_fields: ' . json_encode($product_data_fields) . '
			,additional_fields: []
		}
		
		// ]]>
		</script>');
		
		//$this->addLastJavascript($minishopJsUrl . 'product/category.tree.js');
		//$this->addLastJavascript($minishopJsUrl . 'product/links.grid.js');
		//$this->addLastJavascript($minishopJsUrl . 'product/product.common.js');
		//$this->addLastJavascript($minishopJsUrl . 'product/create.js');

		
    }
    public function getTemplateFile() { 
	/*echo '<pre>';
	print_r($this->promocode->config);
	echo '</pre>';
	echo $this->promocode->config['templatesPath'].'home.tpl';die();*/
	return $this->promocode->config['templatesPath'].'home.tpl'; }
}