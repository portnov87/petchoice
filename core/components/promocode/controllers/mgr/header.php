<?php

$modx->regClientCSS($promocode->config['cssUrl'].'mgr.css');
$modx->regClientStartupScript($promocode->config['jsUrl'].'mgr/promocode.js');
$modx->regClientStartupHTMLBlock('<script type="text/javascript">
Ext.onReady(function() {
    promocode.config = '.$modx->toJSON($promocode->config).';
    promocode.config.connector_url = "'.$promocode->config['connectorUrl'].'";
    promocode.action = "'.(!empty($_REQUEST['a']) ? $_REQUEST['a'] : 0).'";
});
</script>');

return '';