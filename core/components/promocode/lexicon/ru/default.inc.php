<?php
/**
 * modExtra
 *
 * Copyright 2010 by Shaun McCormick <shaun+modextra@modx.com>
 *
 * modExtra is free software; you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 2 of the License, or (at your option) any later
 * version.
 *
 * modExtra is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * modExtra; if not, write to the Free Software Foundation, Inc., 59 Temple
 * Place, Suite 330, Boston, MA 02111-1307 USA
 *
 * @package modextra
 */
/**
 * Default English Lexicon Entries for modExtra
 *
 * @package modextra
 * @subpackage lexicon
 */
$_lang['modextra'] = 'Промокоды';
$_lang['modextra.menu_desc'] = 'A sample Extra to develop from.';
$_lang['modextra.items'] = 'Промокоды';
$_lang['code'] = 'Промокод';
$_lang['created'] = 'Дата создания';
$_lang['status'] = 'Активен';
$_lang['date_active'] = 'Дата активации';
$_lang['order_id'] = 'Заказ';
$_lang['order_num'] = 'Номер заказа';
$_lang['discount'] = 'Скидка';
$_lang['term'] = 'Срок действия (до какого действует)';
$_lang['category'] = 'Категория';
$_lang['summa'] = 'Сумма при которой действует код';
$_lang['count']='Количество';


$_lang['modextra.item_create'] = 'Создать правило для кода';

$_lang['modextra.item_err_ae'] = 'An Item already exists with that name.';
$_lang['modextra.item_err_nf'] = 'Item not found.';
$_lang['modextra.item_err_ns'] = 'Item not specified.';
$_lang['modextra.item_err_remove'] = 'An error occurred while trying to remove the Item.';
$_lang['modextra.item_err_save'] = 'An error occurred while trying to save the Item.';
$_lang['modextra.item_remove'] = 'Удалить код';
$_lang['modextra.item_remove_confirm'] = 'Are you sure you want to remove this Item?';
$_lang['modextra.item_update'] = 'Обновить';
$_lang['modextra.intro_msg'] = 'Управление';