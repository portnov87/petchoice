<?php

require_once dirname(dirname(__FILE__)) . '/resourcescontroller.php';

class shopStocks extends shopResourcesRestController
{
    public $allowedMethods = array('post');
    public $classKey = 'msProduct';

    public function initialize()
    {
        parent::initialize();
        $this->whereCondition['class_key'] = 'msProduct';

    }


    public function post()
    {

        $headers=$_SERVER;
        if (isset($headers['REMOTE_ADDR']))
        {
            if ($headers['REMOTE_ADDR']=='45.87.90.20'){
                $result = [
                    'message' => 'Ошибка. НЕт доступа для вашего IP',
                    'success' => true
                ];

                return $this->success('', $result);
            }
        }


        $postData=$this->getProperty('balance');
        if ($postData) {
            if (count($postData)>0) {


               /* $filePath='/var/www/admin/www/petchoice.com.ua/core/cache/logs/stocks.log';
                $file= @fopen($filePath, 'a+');
                $contentLog='';
                foreach ($headers as $key=> $value) {
                    $contentLog .= $key.' = '.$value."\r\n";
                }
                $written = fwrite($file, $contentLog);
               */


                foreach ($postData as $_data) {
                    $product_id = $_data['product_id'];
                    $optionId = $_data['option_id'];


//                    $contentLog=date('Y-m-d H:i').' product_id='.$product_id.' optionId='.$optionId.' availability_1='.$_data['availability_1'].' availability_2='.$_data['availability_2'].' availability_3='.$_data['availability_3']."\r\n";
//                    $written= fwrite($file, $contentLog);


                    $availability_1 = (isset($_data['availability_1'])?$_data['availability_1']:null);
                    $availability_2 = (isset($_data['availability_2'])?$_data['availability_2']:null);//$_data['availability_2'];
                    $availability_3 = (isset($_data['availability_3'])?$_data['availability_3']:null);//$_data['availability_3'];


                    $q = $this->modx->newQuery('msProduct');
                    $q->select(array(
                        'msProduct.*'
                    ));
                    //$q->innerJoin('modTemplateVarResource', 'TV', 'msProduct.id = TV.contentid AND TV.tmplvarid= 1');
                    $q->where(array('msProduct.id' => $product_id));
                    $q->prepare();
                    $q->stmt->execute();
                    $product = $q->stmt->fetch(PDO::FETCH_ASSOC);

                    if ($product){//count(($product)>0) {
                        $q = $this->modx->newQuery('msProduct');
                        $q->select(array(
                            'TV.*'
                        ));
                        $q->innerJoin('modTemplateVarResource', 'TV', 'msProduct.id = TV.contentid AND TV.tmplvarid= 1');
                        $q->where(array('TV.contentid' => $product_id));
                        $q->prepare();
                        $q->stmt->execute();
                        $options_result = $q->stmt->fetch(PDO::FETCH_ASSOC);
                        $characteristics = $options = [];
                        if ($options_result) {

                            if ($options_result['value'] != '') {
                                $options = json_decode($options_result['value'], true);

                                $optionsNew = $options;
                                foreach ($options as $key => $option) {
                                    if (isset($option['id_1c'])) {
                                        if ($option['id_1c'] == $optionId) {

                                            $optionsNew[$key]['availability_1'] = (!is_null($availability_1)?$availability_1:$option['availability_1']);
                                            $optionsNew[$key]['availability_2'] = (!is_null($availability_2)?$availability_2:$option['availability_2']);//$availability_2;
                                            $optionsNew[$key]['availability_3'] = (!is_null($availability_3)?$availability_3:$option['availability_3']);//$availability_3;


//                                            $optionsNew[$key]['availability_1'] = (!is_null($availability_1)?$availability_1:$optionsNew[$key]['availability_1']);
//                                            $optionsNew[$key]['availability_2'] = (!is_null($availability_2)?$availability_2:$optionsNew[$key]['availability_2']);//$availability_2;
//                                            $optionsNew[$key]['availability_3'] = (!is_null($availability_3)?$availability_3:$optionsNew[$key]['availability_3']);//$availability_3;
                                            break;
                                        }
                                    }
                                }
                                $new_optionsForSave = json_encode($optionsNew);

                                $tv = $this->modx->getObject('modTemplateVarResource', array('contentid' => $product_id, 'tmplvarid' => 1));
                                if ($tv) {
                                    $tv->set('value', $new_optionsForSave);
                                    $tv->save();
                                }

                            }
                        }

                    } else {

                        continue;

                    }


                }

//                $written= fwrite($file, "\r\n\r\n\r\n");
//
//
//                @fclose($file);


                $result = [
                    'message' => 'Успех',
                    'success' => true
                ];

                return $this->success('', $result);
            }else{
                $result = [
                    'message' => 'Ошибка. Нет массива данных',
                    'success' => true
                ];

                return $this->success('', $result);
            }
        }else{
            $result = [
                'message' => 'Ошибка. Нет данных',
                'success' => true
            ];

            return $this->success('', $result);
        }


    }

}