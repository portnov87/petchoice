<?php

class shopPets extends shopBaseRestController
{
    public $allowedMethods = array('get', 'post','put');


    public function getList()
    {
        $userId = $this->getProperty('user_id', '');

        if (!empty($userId)) {


            $select = [];
            //$select['modUser'] = '*';//'id, pagetitle, alias ,menuindex, description';

            $q = $this->modx->newQuery('msPet');
            $q->select(array(
                'msPet.*'
            ));

            $q->where(['user_id:=' => $userId]);

            $q->prepare();
            $q->stmt->execute();
            $collection = $q->stmt->fetchAll(PDO::FETCH_ASSOC);

            $total = count($collection);

            return $this->collection($collection, $total);
        }

        $result = [
            'message' => 'Ошибка. Не задан user_id',
            'success' => false
        ];

        return $this->success('', $result);
    }

    public function put()
    {
        $petId = $this->getProperty('id');

        $petData = $this->getProperty('data');
        $pet = $this->modx->getObject('msPet', $petId);
        if ($pet) {


//            echo '<pre>$petData';
//            print_r($petData);
//            echo '</pre>';
//            echo $petId;
//            die();
            if (isset($petData['name']))
                $pet->set('name', $petData['name']);

            if (isset($petData['breed']))
                $pet->set('breed', $petData['breed']);

            if (isset($petData['type']))
                $pet->set('type', $petData['type']);

            if (isset($petData['genre']))
                $pet->set('genre', $petData['genre']);

            if (isset($petData['genre']))
                $pet->set('genre', $petData['genre']);

            if (isset($petData['bday']))
                $pet->set('bday', $petData['bday']);

            if (isset($petData['bmonth']))
                $pet->set('bmonth', $petData['bmonth']);

            if (isset($petData['byear']))
                $pet->set('byear', $petData['byear']);

            if (isset($petData['comment']))
                $pet->set('comment', $petData['comment']);



            $date=date('Y-m-d H:i:s');

            $pet->set('updated', $date);

            if ($pet->save())
            {
                $result = [
                    'message' => 'Питомец обновился',
                    'success' => true
                ];

                return $this->success('', $result);
            }

        }



        $result = [
            'message' => 'Ошибка',
            'success' => false
        ];

        return $this->success('', $result);

    }


    public function post()
    {
        $userId = $this->getProperty('user_id', false);
        if (!$userId) {
            $result = [
                'message' => 'Ошибка. Укажите user_id',
                'success' => false
            ];

            return $this->success('', $result);
        }


        $petData = $this->getProperty('data');


        $petNew = $this->modx->newObject('msPet');

        if (count($petData) > 0) {

            $petNew->set('user_id', $userId);


            if (isset($petData['breed']))
                $petNew->set('breed', $petData['breed']);

            if (isset($petData['type']))
                $petNew->set('type', $petData['type']);

            if (isset($petData['name']))
                $petNew->set('name', $petData['name']);

            if (isset($petData['comment']))
                $petNew->set('comment', $petData['comment']);

            if (isset($petData['bday']))
                $petNew->set('bday', $petData['bday']);

            if (isset($petData['bmonth']))
                $petNew->set('bmonth', $petData['bmonth']);

            if (isset($petData['byear']))
                $petNew->set('byear', $petData['byear']);

            if (isset($petData['genre']))
                $petNew->set('genre', $petData['genre']);


            $date=date('Y-m-d H:i:s');
            $petNew->set('created', $date);
            $petNew->set('photo', '');
            $petNew->set('updated', $date);
            $petNew->set('add_myself', 0);

            if ($petNew->save())
            {
                $result = [
                    'message' => 'Питомец создался',
                    'id'=>$petNew->get('id'),
                    'success' => true
                ];

                return $this->success('', $result);
            }


        } else {
            $result = [
                'message' => 'Ошибка. Укажите data',
                'success' => false
            ];

            return $this->success('', $result);
        }


        $result = [
            'message' => 'Ошибка',
            'success' => false
        ];

        return $this->success('', $result);

    }


    public function read($id)
    {
        if (empty($id)) {
            return $this->failure($this->modx->lexicon('rest.err_field_ns', array(
                'field' => $this->primaryKeyField,
            )));
        }
        $select = [];
        //$select['modUser'] = '*';//'id, pagetitle, alias ,menuindex, description';

        $this->whereCondition['id'] = $id;

        unset($this->whereCondition['deleted']);
        unset($this->whereCondition['published']);

        $q = $this->modx->newQuery('msPet');
        $q->select(array(
            'msPet.*'
        ));

        $q->where($this->whereCondition);

        $q->prepare();
        $q->stmt->execute();
        $status = $q->stmt->fetch(PDO::FETCH_ASSOC);


        return $this->success('', $status);

    }


}