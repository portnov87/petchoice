<?php

class shopPaymentstatus extends shopBaseRestController
{
    public $allowedMethods = array('get');

    /**
     * @return array|void
     */
    public function getList()
    {
        $limit = 100;
        $select = [];
        $q = $this->modx->newQuery('msPaymentStatus');
        $q->select(array(
            'msPaymentStatus.id',
            'msPaymentStatus.name',
            'msPaymentStatus.description',
            'msPaymentStatus.active'
        ));
        //$q->where($this->whereCondition);
        $q->limit($limit);
        $q->prepare();
        $q->stmt->execute();
        $collection = $q->stmt->fetchAll(PDO::FETCH_ASSOC);
        $total = count($collection);

        return $this->collection($collection, $total);
    }

    /**
     * @param $id
     */
    public function read($id)
    {
        if (empty($id)) {
            return $this->failure($this->modx->lexicon('rest.err_field_ns', array(
                'field' => $this->primaryKeyField,
            )));
        }
        $select = [];
        $this->whereCondition['id'] = $id;

        unset($this->whereCondition['deleted']);
        unset($this->whereCondition['published']);

        $q = $this->modx->newQuery('msPaymentStatus');
        $q->select(array(
            'msPaymentStatus.id',
            'msPaymentStatus.name',
            'msPaymentStatus.description',
            'msPaymentStatus.active'
        ));

        $q->where($this->whereCondition);

        $q->prepare();
        $q->stmt->execute();
        $status = $q->stmt->fetch(PDO::FETCH_ASSOC);

        return $this->success('', $status);

    }



    public function post()
    {
        $orderId = $this->getProperty('order_id');
        $statusId = $this->getProperty('status_id');
        if ($orderId&&$statusId) {
            $order = $this->modx->getObject('msOrder', array('id' => $orderId));
            if ($order) {
                //echo $order->get('id').' '.$order->get('payment_status')."<br/>";
                $order->set('payment_status',$statusId);

                if ($order->save())
                {
                    $result = [
                        'message' => 'Статус оплаты обновился',
                        'success' => true
                    ];

                    return $this->success('', $result);
                }
            }
        }

        $result = [
            'message' => 'Ошибка',
            'success' => false
        ];

        return $this->success('', $result);

    }
}