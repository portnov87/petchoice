<?php

require_once dirname(dirname(__FILE__)) . '/resourcescontroller.php';

class shopCategories extends shopResourcesRestController
{

    public $classKey = 'msCategory';

    public function initialize()
    {
        parent::initialize();
        $this->whereCondition['class_key'] = 'msCategory';

    }


    public function getList()
    {

        $limit=20;
        if ($this->getProperty('limit'))
            $limit=$this->getProperty('limit');

        $parentId=10;
        if ($this->getProperty('parent')) {
            if ($this->getProperty('parent')>0)
                $parentId = $this->getProperty('parent');
        }


        $this->whereCondition['id:NOT IN'] = [2950,12430,12908,12909,12911];//2950


        $this->whereCondition['parent:='] = $parentId;


        if ($limit>100)
            $limit=100;

        $offset=0;
        if ($this->getProperty('offset'))
            $offset=$this->getProperty('offset');


        $collection = $this->pdo->getCollection(
            $this->classKey,
            $this->whereCondition,
            array(
//                'loadModels' => 'ms2gallery',
//                'leftJoin' => [
//                    "image" => [
//                        "class" => "msResourceFile",
//                        "alias" => "image",
//                        "on" => "image.resource_id = msCategory.id"// AND image.path LIKE '%/small/%'
//                    ]
//                ],

                'select' => array(
                    'msCategory' => 'id, pagetitle, alias ,menuindex, parent,published,createdon,editedon,uri',
                    //"image" => "image.url as image"
                ),
                'sortby' => 'menuindex',
                'sortdir' => 'asc',
                'limit' => $limit,
                'offset'=>$offset
            )
        );

        $total = count($collection);
        $collectionNew=$collection;
        foreach ($collection as $key => $cat) {
            $children = $this->modx->getChildIds($cat['id'], 1, array('context' => 'web'));
            if ($collectionNew[$key]['parent']==10)
                $collectionNew[$key]['parent']=0;

            $collectionNew[$key]['products_count'] = count($children);
        }

        $collection=$collectionNew;

        return $this->collection($collection, $total);
    }


    public function read($id)
    {
        if (empty($id)) {
            return $this->failure($this->modx->lexicon('rest.err_field_ns', array(
                'field' => $this->primaryKeyField,
            )));
        }

        $this->whereCondition['id'] = $id;

        $category = $this->pdo->getArray(
            $this->classKey,
            $this->whereCondition,
            array(
                'select' => array(
                    'msCategory' => 'id, pagetitle, alias'
                ),
            )
        );
        if (empty($category)) {
            return $this->failure($this->modx->lexicon('rest.err_obj_nf', array(
                'class_key' => $this->classKey,
            )));
        }
        $children = $this->modx->getChildIds($category['id'], 1, array('context' => 'web'));
        $category['products_count'] = count($children);

        $afterRead = $this->afterRead($category);

        if ($afterRead !== true && $afterRead !== null) {
            return $this->failure($afterRead === false ? $this->errorMessage : $afterRead);
        }

        return $this->success('', $category);

    }
}