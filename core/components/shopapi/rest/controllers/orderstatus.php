<?php

class shopOrderstatus extends shopBaseRestController
{
    public $allowedMethods = array('get','post');

    public function getList()
    {

            $limit = 100;




        $select = [];
        //$select['modUser'] = '*';//'id, pagetitle, alias ,menuindex, description';

        $q = $this->modx->newQuery('msOrderStatus');
        $q->select(array(
            'msOrderStatus.id',
            'msOrderStatus.name',
            'msOrderStatus.description',
            'msOrderStatus.color',
            'msOrderStatus.active',
            /*[id] => 2
            [name] => Доставлен
    [description] => Заказ закрывается
    [color] => 008000
            [email_user] => 0
            [email_manager] => 0
            [subject_user] => [[%ms2_email_subject_paid_user]]
            [subject_manager] => [[%ms2_email_subject_paid_manager]]
            [body_user] => 29
            [body_manager] => 30
            [active] => 1
            [final] => 1
            [fixed] => 0
            [rank] => 8
            [editable] => 0*/
        ));

        //$q->where($this->whereCondition);
        $q->limit($limit);
        $q->prepare();
        $q->stmt->execute();
        $collection = $q->stmt->fetchAll(PDO::FETCH_ASSOC);
//        echo '<pre>$collection';
//        print_r($collection);
//        echo '</pre>';
//die();

        $total = count($collection);

        return $this->collection($collection, $total);
    }



    public function post()
    {
        $orderId = $this->getProperty('order_id');
        $statusId = $this->getProperty('status_id');
        $ms2 = $this->modx->getService('minishop2');
        if ($orderId&&$statusId) {
            $order = $this->modx->getObject('msOrder', array('id' => $orderId));
            if ($order) {
                //echo $order->get('id').' '.$order->get('payment_status')."<br/>";
                //$order->set('status',$statusId);


                if (($statusId==2)&&($order->get('status_id')!=$statusId)) {
                    $order->setFinishPaymentStatus();

                    $user_id = $order->get('user_id');
                    $user = $this->modx->getObject('modUser', $user_id);


                    $miniShop2 = $this->modx->getService('minishop2');
                    $miniShop2->initialize($this->modx->context->key);

                    $miniShop2->charge_bonuses($order);
                    $count_orders_close = (int)$user->get('count_orders_close');
                    $count_orders_close = $count_orders_close + 1;
                    $user->set('count_orders_close', $count_orders_close);


                    $summa_close_orders = 0;

                    $profile = $user->getOne('Profile');
                    $q = $this->modx->newQuery('msOrder');
                    $q->where(array('msOrder.status' => 2, 'msOrder.user_id:=' => $user_id));
                    $q->select(array('msOrder.id', 'msOrder.cart_cost'));
                    $ids = [];
                    $total = 0;
                    if ($q->prepare() && $q->stmt->execute()) {
                        $orders = $q->stmt->fetchAll(PDO::FETCH_ASSOC);
                        foreach ($orders as $order_i) {
                            $ids[] = $order_i['id'];
                            $total += $order_i['cart_cost'];
                        }
                    }
                    $user->set('summa_close_orders', $total);

                    if ($user->get('registr') == '1') {

                        $extended = $profile->get('extended');
                        $user_discount = $extended['discount'];

                        $q = $this->modx->newQuery('msUserDiscount', array('percent:>' => $user_discount));
                        $q->sortby('percent', 'ASC');
                        $q->select('`msUserDiscount`.`percent`,`msUserDiscount`.`total`');
                        $q->limit(1);
                        $next_discount_total = 0;
                        $next_discount = 0;
                        if ($q->prepare() && $q->stmt->execute()) {
                            while ($row = $q->stmt->fetch(PDO::FETCH_ASSOC)) {
                                $next_discount = $row['percent'];
                                $next_discount_total = $row['total'];
                            }
                        }

                        if ($total >= $next_discount_total) {
                            if ($user_discount < $next_discount) {


                                $extended = $profile->get('extended');
                                $extended['discount'] = $next_discount;
                                $profile->set('extended', $extended);

                                $profile->save();

                            }
                        }

                    }
                }





                ///$ms2->changeOrderStatus($orderId, $statusId, 0, true);

                if ($order->save())
                {
                     $result = [
                        'message' => 'Статус обновился',
                        'success' => true
                    ];

                    return $this->success('', $result);
                }
            }
        }

        $result = [
            'message' => 'Ошибка',
            'success' => false
        ];

        return $this->success('', $result);

    }



    public function read($id)
    {
        if (empty($id)) {
            return $this->failure($this->modx->lexicon('rest.err_field_ns', array(
                'field' => $this->primaryKeyField,
            )));
        }
        $select = [];
        //$select['modUser'] = '*';//'id, pagetitle, alias ,menuindex, description';

        $this->whereCondition['id'] = $id;

        unset($this->whereCondition['deleted']);
        unset($this->whereCondition['published']);

        $q = $this->modx->newQuery('msOrderStatus');
        $q->select(array(
            'msOrderStatus.id',
            'msOrderStatus.name',
            'msOrderStatus.description',
            'msOrderStatus.color',
            'msOrderStatus.active',

        ));

        $q->where($this->whereCondition);

        $q->prepare();
        $q->stmt->execute();
        $status = $q->stmt->fetch(PDO::FETCH_ASSOC);


        return $this->success('', $status);

    }




}