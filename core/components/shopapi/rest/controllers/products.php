<?php

require_once dirname(dirname(__FILE__)) . '/resourcescontroller.php';

class shopProducts extends shopResourcesRestController
{
    public $classKey = 'msProduct';

    public function initialize()
    {
        parent::initialize();
        $this->whereCondition['class_key'] = 'msProduct';

        //echo 'date_update'.$this->getProperty('date_update');
        //die();
        //?date_update=2020-09-15T13:45:30
        if ($this->getProperty('date_begin')) {
            $this->whereCondition['msProduct.createdon:>='] = strtotime($this->getProperty('date_begin'));
        }
        if ($this->getProperty('date_update')) {

            $this->whereCondition['msProduct.editedon:>='] = strtotime($this->getProperty('date_update'));
        }
        if ($this->getProperty('parent')) {

            //$this->whereCondition['msProduct.parent:>='] = $this->getProperty('parent');
        }
        //13967


        if ($this->getProperty('tag')) {
            $this->whereCondition['Tags.value:LIKE'] = $this->getProperty('tag');
        }
    }

    public function getList()
    {



        $leftJoin = [];
        $leftJoin["Data"] = [
            "class" => "msProductData",
            "alias" => "Data",
        ];


        if ($this->getProperty('tag')) {
            $leftJoin["Tags"] = [
                'class' => 'msProductOption',
                'alias' => 'Tags',
                'on' => 'msProduct.id = Tags.product_id'
            ];
        }


        $select = [];
        $select['msProduct'] = 'id, pagetitle, alias ,menuindex, description,parent, uri';
        $select['Data'] = 'article,date_new, helloween, markdown, newyear, black_friday, february, hits, price_kg, day_cat, day_dog, askidka, 
        price, old_price, thumb, new, popular, favorite, avability';

        if ($this->getProperty('tag')) {
            $select['Tags'] = 'Tags.value as tag';
        }
        if ($this->getProperty('sortBy')) {
            $sort='msProduct.'.$this->getProperty('sortBy');//createdon';
        }
        else
            $sort='msProduct.createdon';




        if ($this->getProperty('sort')) {
            $sortType=$this->getProperty('sort');
        }
        else
            $sortType = 'desc';

        $headers=$_SERVER;
        $filePath='/var/www/admin/www/petchoice.com.ua/core/cache/logs/products.log';
        $file= @fopen($filePath, 'a+');
        $contentLog='';
        foreach ($headers as $key=> $value) {
            $contentLog .= $key.' = '.$value."\r\n";
        }
        $written = fwrite($file, $contentLog);



        $limit=20;
        $offset=0;
        if ($this->getProperty('limit'))
            $limit=$this->getProperty('limit');

        if ($this->getProperty('offset'))
            $offset=$this->getProperty('offset');



        if ($limit>100)
            $limit=100;


        $collection = $this->pdo->getCollection(
            $this->classKey,
            $this->whereCondition,
            array(
                'leftJoin' => $leftJoin,
                'select' => $select,
                'sortby' => $sort,
                'sortdir' => $sortType,
                'limit'=>$limit,
                'offset'=>$offset
            )
        );

        $total = count($collection);

        foreach ($collection as $key => $cat) {
            $collection[$key]['thumb'] = 'https://petchoice.ua' . $cat['thumb'];
            //$collection[$key]['measure'] = $this->getMeasureTitle($cat['measure']);
        }


        $collection[$key]['attributes']=[];

        foreach ($collection as $key => $product) {

            $product_id=$product['id'];


            $getAttributes=false;

            if ($this->getProperty('attributes')) {
                if ($this->getProperty('attributes')=='1')
                    $getAttributes=true;
            }

            $getCharacteristics=false;

            if ($this->getProperty('characteristics')) {
                if ($this->getProperty('characteristics')=='1')
                    $getCharacteristics=true;
            }

            $getOptions=true;

            if ($this->getProperty('options')) {
                if ($this->getProperty('options')=='0')
                    $getOptions=false;
            }


            if ($getOptions) {

                $q = $this->modx->newQuery('msProduct');
                $q->select(array(
                    'TV.*'
                ));
                $q->innerJoin('modTemplateVarResource', 'TV', 'msProduct.id = TV.contentid AND TV.tmplvarid= 1');
                $q->where(array('TV.contentid' => $product_id));
                $q->prepare();
                $q->stmt->execute();
                $options_result = $q->stmt->fetch(PDO::FETCH_ASSOC);
                $characteristics = $options = [];
                if ($options_result) {

                    if ($options_result['value'] != '') {
                        $contentLog="\r\n\r\n".date('Y-m-d H:i')."before\r\n\r\n".$options_result['value']."\r\nafter\r\n";//.date('Y-m-d H:i')."\r\n\r\n".json_encode($collection);
                        $options = json_decode($options_result['value'], true);

                        $new_options=$_new_options=[];

                        $globalid1c=true;

                        foreach ($options as $_key=>$option)
                        {

                            $new_option=$option;
                            $_new_option=$option;
                            //$new_options=$_new_option=[];

                            $id1c=true;
                            $MIGX_id=$option['MIGX_id'];
                            if (isset($option['id_1c']))
                            {
                                if (empty($option['id_1c']))
                                {
                                    $id1c=false;
                                    $globalid1c=false;
                                }
                            }else {
                                $id1c=false;
                                $globalid1c=false;
                            }


                            if (!$id1c)
                            {
                                $id1c=$product_id.$MIGX_id.time();
                                $new_option['id_1c']=$id1c;
                                $_new_option['id_1c']=$id1c;
                            }



                            if (!empty($new_option['markdown_text']))
                            {

                                $markdown_textLabel=str_replace(['[[%',']]'],'',$new_option['markdown_text']);

                                $markdown_text= $this->modx->lexicon($markdown_textLabel);
                                if (!empty($markdown_text)) {
                                    $_new_option['markdown_text'] = $markdown_text;
                                }
                            }

                            if (!empty($new_option['text_action']))
                            {

                                $text_actionLabel=str_replace(['[[%',']]'],'',$new_option['text_action']);

                                $text_action= $this->modx->lexicon($text_actionLabel);

                                //if (!empty($text_action)&&($text_action!=$text_actionLabel)) {
                                if (!empty($text_action)) {
                                    $_new_option['text_action'] = $text_action;
                                }
                            }
                            $new_options[]=$new_option;

                            $_new_options[]=$_new_option;


                            //$new_options[]=$new_option;

                        }

                        if (!$globalid1c) {
                            $options=$new_options;
                            $new_optionsForSave = json_encode($new_options);

                            $tv = $this->modx->getObject('modTemplateVarResource', array('contentid' => $product_id, 'tmplvarid' => 1));
                            if ($tv) {
                                $tv->set('value', $new_optionsForSave);
                                $tv->save();
                            }
                        }


                    }
                }

                $collection[$key]['options'] = $options;//$_new_options;
            }




            if ($getCharacteristics) {
                $q = $this->modx->newQuery('msProduct');
                $q->select(array(
                    'TV.value',
                    'TemplateVar.name',
                    'TemplateVar.caption'
                ));
                $q->innerJoin('modTemplateVarResource', 'TV', 'msProduct.id = TV.contentid AND TV.tmplvarid!= 1');
                $q->innerJoin('modTemplateVar', 'TemplateVar', 'TV.tmplvarid=TemplateVar.id');


                $q->where(array('TV.contentid' => $product_id));
                $q->prepare();
                $q->stmt->execute();
                $characteristics_result = $q->stmt->fetchAll(PDO::FETCH_ASSOC);

                $collection[$key]['characteristics'] = $characteristics_result;
            }


            if ($getAttributes) {
                $q = $this->modx->newQuery('msAttributeProduct');
                $q->select(array(
                        'group_id' => 'attributegroup.id',
                        'group_name' => 'attributegroup.name',
                        'attr_id' => 'attribute.id',
                        'attr_name' => 'attribute.name'
                    )
                );
                $q->join('msAttribute','attribute','JOIN',['msAttributeProduct.attribute_id=attribute.id']);
                $q->join('msAttributeGroup','attributegroup','JOIN',['msAttributeProduct.attr_group=attributegroup.id']);

                $q->where(array(
                    'msAttributeProduct.product_id:=' => $product_id
                ));
                $q->limit(100);
                $q->prepare();
                //$sql = $q->toSQL();
                $q->stmt->execute();
                $attributes = $q->stmt->fetchAll(PDO::FETCH_ASSOC);


                $collection[$key]['attributes'] = $attributes;
            }

            $vendorArray=[];

            $q = $this->modx->newQuery('msVendor');
            $q->select(array(
                'msVendor.id',
                'msVendor.name'
            ));
            $q->innerJoin('msProductData', 'product', 'msVendor.id = product.vendor');

            $q->where(array('product.id' => $product_id));
            $q->prepare();
            $q->stmt->execute();



            $vendor_result = $q->stmt->fetch(PDO::FETCH_ASSOC);

            if ($vendor_result)
                $vendor_result['apiLink']='https://petchoice.ua/rest/vendors/id/'.$vendor_result['id'];



            $collection[$key]['vendor'] = $vendor_result;


            $parentId=$product['parent'];
            //730

            $q = $this->modx->newQuery('msCategory');
            $q->select(array(
                'msCategory.id',
                'msCategory.pagetitle'
            ));
            //$q->innerJoin('msCategoryMember', 'productCat', 'msCategory.id = productCat.category_id');

            $q->where(array('msCategory.id' => $parentId));
            $q->prepare();
            $q->stmt->execute();

            $categoryMain = $q->stmt->fetchAll(PDO::FETCH_ASSOC);



            $q = $this->modx->newQuery('msCategory');
            $q->select(array(
                'msCategory.id',
                'msCategory.pagetitle'
            ));
            $q->innerJoin('msCategoryMember', 'productCat', 'msCategory.id = productCat.category_id');

            $q->where(array('productCat.product_id' => $product_id));
            $q->prepare();
            $q->stmt->execute();

            $categories_result = $q->stmt->fetchAll(PDO::FETCH_ASSOC);
            //$cats=array_merge($categoryMain,$categories_result);

            if ($categoryMain)
            {
                foreach ($categoryMain as $_key=>$ven){
                    $catId=$ven['id'];
                    //$categories_result_new[$_key]['apiLink']='https://petchoice.ua/rest/categories/id/'.$catId;
                    $catPr=[];
                    $catPr=$ven;
                    $catPr['apiLink']='https://petchoice.ua/rest/categories/id/'.$catId;
                    //$categories[$catId]['apiLink']='https://petchoice.ua/rest/categories/id/'.$catId;
                    $categories[]=$catPr;//ven;

                }
            }


            $collection[$key]['url']='https://petchoice.ua/'.$collection[$key]['uri'];
            $collection[$key]['categories'] = $categories;


        }



        $contentLog=$contentLog.date('Y-m-d H:i')."\r\n\r\n".json_encode($collection);//' product_id='.$product_id.' optionId='.$optionId.' availability_1='.$_data['availability_1'].' availability_2='.$_data['availability_2'].' availability_3='.$_data['availability_3']."\r\n";
        $written= fwrite($file, $contentLog."\r\n\r\n");

        @fclose($file);

        return $this->collection($collection, $total);
    }

    public function read($id)
    {

        $headers=$_SERVER;

        //$lexicon = $this->modx->getService('lexicon','modLexicon');
        //$this->modx->lexicon->load('minishop2:cart');
        $this->modx->lexicon->load('ua:poylang:site');
        //echo $this->modx->context->get('key');

        if (empty($id)) {
            return $this->failure($this->modx->lexicon('rest.err_field_ns', array(
                'field' => $this->primaryKeyField,
            )));
        }

        $this->whereCondition['id'] = $id;

        $product = $this->pdo->getArray(
            $this->classKey,
            $this->whereCondition,
            array(
                'leftJoin' => [
                    "Data" => [
                        "class" => "msProductData",
                        "alias" => "Data",
                    ]
                ],
                'select' => array(
                    'msProduct' => 'id, pagetitle, alias ,menuindex, description, parent, uri',
                    'Data' => 'article,date_new, helloween, markdown, newyear, black_friday, february, hits, price_kg, day_cat, day_dog, askidka, 
        price, old_price, thumb, new, popular, favorite, avability',

                ),
            )
        );


        if (empty($product)) {
            return $this->failure($this->modx->lexicon('rest.err_obj_nf', array(
                'class_key' => $this->classKey,
            )));
        }else{

            $headers=$_SERVER;
            $filePath='/var/www/admin/www/petchoice.com.ua/core/cache/logs/product.log';
            $file= @fopen($filePath, 'a+');
            $contentLog='';
            foreach ($headers as $key=> $value) {
                $contentLog .= $key.' = '.$value."\r\n";
            }
            $written = fwrite($file, $contentLog);

            $product_id=$product['id'];


            $getAttributes=true;

            if ($this->getProperty('attributes')) {
                if ($this->getProperty('attributes')=='1')
                    $getAttributes=true;
            }

            $getCharacteristics=true;

            if ($this->getProperty('characteristics')) {
                if ($this->getProperty('characteristics')=='1')
                    $getCharacteristics=true;
            }

            $getOptions=true;

            if ($this->getProperty('options')) {
                if ($this->getProperty('options')=='0')
                    $getOptions=false;
            }


            if ($getOptions) {

                $q = $this->modx->newQuery('msProduct');
                $q->select(array(
                    'TV.*'
                ));
                $q->innerJoin('modTemplateVarResource', 'TV', 'msProduct.id = TV.contentid AND TV.tmplvarid= 1');
                $q->where(array('TV.contentid' => $product_id));
                $q->prepare();
                $q->stmt->execute();
                $options_result = $q->stmt->fetch(PDO::FETCH_ASSOC);
                $characteristics = $options = [];
                if ($options_result) {

                    if ($options_result['value'] != '') {
                        $options = json_decode($options_result['value'], true);

                        $new_options=$_new_option=[];

                        $globalid1c=true;

                        foreach ($options as $_key=>$option)
                        {

                            $new_option=$option;
                            $_new_option=$option;
                            $id1c=true;
                            $MIGX_id=$option['MIGX_id'];
                            if (isset($option['id_1c']))
                            {
                                if (empty($option['id_1c']))
                                {
                               // if ($option['id_1c']==''){
                                    $id1c=false;
                                    $globalid1c=false;
                                }
                            }else {
                                $id1c=false;
                                $globalid1c=false;
                            }


                            if (!$id1c)
                            {
                                $id1c=$product_id.$MIGX_id.time();
                                $new_option['id_1c']=$id1c;
                                $_new_option['id_1c']=$id1c;
                            }


                            if (!empty($new_option['markdown_text']))
                            {

                                $markdown_textLabel=str_replace(['[[%',']]'],'',$new_option['markdown_text']);

                                $markdown_text= $this->modx->lexicon($markdown_textLabel);
                                if (!empty($markdown_text)) {
                                    $_new_option['markdown_text'] = $markdown_text;
                                }
                            }

                            if (!empty($new_option['text_action']))
                            {

                                $text_actionLabel=str_replace(['[[%',']]'],'',$new_option['text_action']);

                                $text_action= $this->modx->lexicon($text_actionLabel);

                                //if (!empty($text_action)&&($text_action!=$text_actionLabel)) {
                                if (!empty($text_action)) {
                                    $_new_option['text_action'] = $text_action;
                                }
                            }
                            $new_options[]=$new_option;

                            $_new_options[]=$_new_option;

                        }


                        if (!$globalid1c) {
                            $options=$new_options;
                            $new_optionsForSave = json_encode($new_options);

                            $tv = $this->modx->getObject('modTemplateVarResource', array('contentid' => $product_id, 'tmplvarid' => 1));
                            if ($tv) {
                                $tv->set('value', $new_optionsForSave);
                                $tv->save();
                            }
                        }


                    }
                }

                $product['options'] = $_new_options;//$options;
            }


            if ($getCharacteristics) {
                $q = $this->modx->newQuery('msProduct');
                $q->select(array(
                    'TV.value',
                    'TemplateVar.name',
                    'TemplateVar.caption'
                ));
                $q->innerJoin('modTemplateVarResource', 'TV', 'msProduct.id = TV.contentid AND TV.tmplvarid!= 1');
                $q->innerJoin('modTemplateVar', 'TemplateVar', 'TV.tmplvarid=TemplateVar.id');


                $q->where(array('TV.contentid' => $product_id));
                $q->prepare();
                $q->stmt->execute();
                $characteristics_result = $q->stmt->fetchAll(PDO::FETCH_ASSOC);

                $product['characteristics'] = $characteristics_result;
            }


            if ($getAttributes) {
                $q = $this->modx->newQuery('msAttributeProduct');
                $q->select(array(
                        'group_id' => 'attributegroup.id',
                        'group_name' => 'attributegroup.name',
                        'attr_id' => 'attribute.id',
                        'attr_name' => 'attribute.name'
                    )
                );
                $q->join('msAttribute','attribute','JOIN',['msAttributeProduct.attribute_id=attribute.id']);
                $q->join('msAttributeGroup','attributegroup','JOIN',['msAttributeProduct.attr_group=attributegroup.id']);

                $q->where(array(
                    'msAttributeProduct.product_id:=' => $product_id
                ));
                $q->limit(100);
                $q->prepare();
                //$sql = $q->toSQL();
                $q->stmt->execute();
                $attributes = $q->stmt->fetchAll(PDO::FETCH_ASSOC);


                $product['attributes'] = $attributes;
            }

            $vendorArray=[];

            $q = $this->modx->newQuery('msVendor');
            $q->select(array(
                'msVendor.id',
                'msVendor.name'
            ));
            $q->innerJoin('msProductData', 'product', 'msVendor.id = product.vendor');

            $q->where(array('product.id' => $product_id));
            $q->prepare();
            $q->stmt->execute();



            $vendor_result = $q->stmt->fetch(PDO::FETCH_ASSOC);

            if ($vendor_result)
                $vendor_result['apiLink']='https://petchoice.ua/rest/vendors/id/'.$vendor_result['id'];



            $product['vendor'] = $vendor_result;

            $q = $this->modx->newQuery('msCategory');
            $q->select(array(
                'msCategory.id',
                'msCategory.pagetitle'
            ));
            $q->innerJoin('msCategoryMember', 'productCat', 'msCategory.id = productCat.category_id');

            $q->where(array('productCat.product_id' => $product_id));
            $q->prepare();
            $q->stmt->execute();

            $categories_result = $q->stmt->fetchAll(PDO::FETCH_ASSOC);
            $categories_result_new=$categories_result;
            if ($categories_result)
            {
                foreach ($categories_result as $key=>$ven){
                    $catId=$ven['id'];
                    $categories_result_new[$key]['apiLink']='https://petchoice.ua/rest/categories/id/'.$catId;
                }
            }
            $categories_result=$categories_result_new;


            $parentId=$product['parent'];
            //730

            $q = $this->modx->newQuery('msCategory');
            $q->select(array(
                'msCategory.id',
                'msCategory.pagetitle'
            ));
            //$q->innerJoin('msCategoryMember', 'productCat', 'msCategory.id = productCat.category_id');

            $q->where(array('msCategory.id' => $parentId));
            $q->prepare();
            $q->stmt->execute();

            $categoryMain = $q->stmt->fetchAll(PDO::FETCH_ASSOC);
            $categories=[];
            if ($categoryMain)
            {
                foreach ($categoryMain as $_key=>$ven){
                    $catId=$ven['id'];
                    //$categories_result_new[$_key]['apiLink']='https://petchoice.ua/rest/categories/id/'.$catId;
                    $catPr=[];
                    $catPr=$ven;
                    $catPr['apiLink']='https://petchoice.ua/rest/categories/id/'.$catId;
                    //$categories[$catId]['apiLink']='https://petchoice.ua/rest/categories/id/'.$catId;
                    $categories[]=$catPr;//ven;

                }
            }

            $product['categories'] = $categories;//$categories_result;


        }

        $product['url']='https://petchoice.ua/'.$product['uri'];
        $product['thumb'] = 'https://petchoice.ua' . $product['thumb'];
        //$product['measure'] = $this->getMeasureTitle($product['measure']);


        $contentLog=date('Y-m-d H:i')." get one product \r\n\r\n".json_encode($product)."<br/>";//' product_id='.$product_id.' optionId='.$optionId.' availability_1='.$_data['availability_1'].' availability_2='.$_data['availability_2'].' availability_3='.$_data['availability_3']."\r\n";
        $written= fwrite($file, $contentLog);

        $written= fwrite($file, "\r\n\r\n\r\n");
        @fclose($file);

        return $this->success('', $product);

    }

    private function getMeasureTitle($measure)
    {
        switch ($measure) {
            case'1':
                return 'кг.';
                break;
            case'2':
                return 'шт.';
                break;
            case'3':
                return 'уп.';
                break;
            case'4':
                return '8 шт.';
                break;
        }
    }
}