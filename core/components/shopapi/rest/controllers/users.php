<?php

require_once dirname(dirname(__FILE__)) . '/resourcescontroller.php';

class shopUsers extends shopResourcesRestController
{
    public $classKey = 'modUser';
    public $allowedMethods = array('put', 'get','post');


    public function initialize()
    {
        parent::initialize();
        $this->whereCondition = [];
        //$this->whereCondition['class_key'] = 'modUser';
        /*
                if ($this->getProperty('date_begin')) {
                    $this->whereCondition['msProduct.createdon:>='] = strtotime($this->getProperty('date_begin'));
                }

                if ($this->getProperty('date_update')) {
                     $this->whereCondition['msProduct.editedon:>='] = strtotime($this->getProperty('date_update'));
                }


                if ($this->getProperty('tag')) {
                    $this->whereCondition['Tags.value:LIKE'] = $this->getProperty('tag');
                }*/
    }

    public function post()
    {
        $userIdPost = $this->getProperty('user_id', '');
        $userPostData = $this->getProperty('data', []);

        if ($userPostData) {
            if (count($userPostData) > 0) {

                if (!empty($userIdPost)) {
                    $userObject = $this->modx->getObject('modUser', $userIdPost);
                    if ($userObject) {
                        $profileUserObject = $userObject->getOne('Profile');
                        if ($profileUserObject) {

                            $extended = $profileUserObject->get('extended');
                            $newextended = $extended;


                            if (isset($userPostData['fullname']))
                                $profileUserObject->set('fullname', $userPostData['fullname']);//Имя

                            if (isset($userPostData['email']))
                                $profileUserObject->set('email', $userPostData['email']);//email

                            if (isset($userPostData['mobilephone']))
                                $profileUserObject->set('mobilephone', $userPostData['mobilephone']);//дополнительный телефон

                            if (isset($userPostData['city_name'])) {
                                $profileUserObject->set('city', $userPostData['city_name']);

                                $newextended['city'] = $userPostData['city_name'];
                            }

                            if (isset($userPostData['lastname'])) {
                                $profileUserObject->set('lastname', $userPostData['lastname']);//Фамилия
                                $newextended['lastname'] = $userPostData['lastname'];
                            }

                            if (isset($userPostData['fathername']))
                                $newextended['fathername'] = $userPostData['fathername'];


                            if (isset($userPostData['comment']))
                                $profileUserObject->set('comment', $userPostData['comment']);//Комментарий


                            if (isset($userPostData['city_id'])) {
                                $newextended['city_id_np'] = $userPostData['city_id'];
                                $newextended['city_id'] = $userPostData['city_id'];
                            }


                            if (isset($userPostData['street_id'])) {
                                $newextended['street_id'] = $userPostData['street_id'];
                                $newextended['street_np_ref'] = $userPostData['street_id'];

                                if (isset($userPostData['street'])) {
                                    $newextended['street_np'] = $userPostData['street'];
                                }
                            } else if (isset($userPostData['street'])) {

                                $newextended['street'] = $userPostData['street'];
                            }



//                            if (isset($userPostData['street_np_ref'])) {
//                                $newextended['street_np_ref'] = $userPostData['street_np_ref'];
//                                $newextended['street_id'] = $userPostData['street_np_ref'];
//                            }

//                                if (isset($userPostData['city_id'])) {
//                                    $newextended['city_id'] = $userPostData['city_id'];
//                                }


                            if (isset($userPostData['street_id_np']))
                                $newextended['street_id_np'] = $userPostData['street_id_np'];

                            if (isset($userPostData['floor']))
                                $newextended['floor'] = $userPostData['floor'];

                            if (isset($userPostData['parade']))
                                $newextended['parade'] = $userPostData['parade'];

                            if (isset($userPostData['doorphone']))
                                $newextended['doorphone'] = $userPostData['doorphone'];

                            //if (isset($userPostData['housing']))


                            if (isset($userPostData['house'])) {
                                $newextended['house'] = $userPostData['house'];
                                $newextended['housing'] = $userPostData['house'];
                            }

                            if (isset($userPostData['room']))
                                $newextended['room'] = $userPostData['room'];


                            if (isset($userPostData['warehouse_name'])) {
                                $newextended['warehouse_name'] = $userPostData['warehouse_name'];
                            }

                            if (isset($userPostData['warehouse_id'])) {
                                $newextended['warehouse_id'] = $userPostData['warehouse_id'];
                                $newextended['warehouse'] = $userPostData['warehouse_id'];
                            }


                            $extended = $this->modx->toJSON($newextended);

                            $profileUserObject->set('extended', $extended);
                            if ($profileUserObject->save())
                            {
                                $result = [
                                    'message' => 'Пользователь обновился',
                                    'success' => true
                                ];

                                return $this->success('', $result);
                            }
                        }
                    }
                    $userObject->save();

                    return $this->failure($this->modx->lexicon('rest.err_field_ns', array(
                        'field' => 'phone',//$this->primaryKeyField,
                    )));
                }

            } else {
                return $this->failure($this->modx->lexicon('rest.err_field_ns', array(
                    'field' => 'phone',//$this->primaryKeyField,
                )));
            }

        }

        return $this->failure($this->modx->lexicon('Ошибка сохранения пользователя'));
    }


    public function put()
    {
        $phone = $this->getProperty('phone');

        if ($phone) {

            $q = $this->modx->newQuery('modUser');
            $q->select(array(
                'modUser.id',
                'modUser.username',
                'modUser.active',
                'modUser.registr_date',
                'modUser.subscribe',
                'modUser.subscribe_date',
                'modUser.registr',
                'modUser.created',
                'modUser.last_order',
                'modUser.total_orders',
                'modUser.count_orders_close',
                'modUser.summa_close_orders',
                'modUser.summa_all_orders',
                'modUser.bonuses',
                'modUser.date_clear_bonuses',
                'Profile.fullname',
                'Profile.email',
                'Profile.phone',
                'Profile.mobilephone',
                'Profile.blocked',
                'Profile.address',
                'Profile.country',
                'Profile.city',
                'Profile.state',
                'Profile.comment',
                'Profile.lastname',
                'Profile.extended'
                //'Profile.*'
            ));

            $q->where('Profile.mobilephone="' . $phone . '" OR Profile.phone="' . $phone . '"');
            $q->innerJoin('modUserProfile', 'Profile', 'Profile.internalKey = modUser.id');

            $q->limit(1);
            $q->prepare();


            $q->stmt->execute();
            $user = $q->stmt->fetch(PDO::FETCH_ASSOC);

            if ($user) {
                $userNew = $user;
                //foreach ($collection as $key => $col) {

                $extended = $userNew['extended'];
                $extendedArray = json_decode($extended, true);
                unset($userNew['extended']);
                $userNew['extended'] = [];
                $userNew['extended']['discount'] = (isset($extendedArray['discount']) ? $extendedArray['discount'] : false);
                $userNew['extended']['city_id'] = (isset($extendedArray['city_id']) ? $extendedArray['city_id'] : false);//$extendedArray['city_id'];
                $userNew['extended']['delivery'] = (isset($extendedArray['delivery']) ? $extendedArray['delivery'] : false);//$extendedArray['delivery'];
                $userNew['extended']['warehouse'] = (isset($extendedArray['warehouse']) ? $extendedArray['warehouse'] : false);//$extendedArray['warehouse'];
                $userNew['extended']['street'] = (isset($extendedArray['street']) ? $extendedArray['street'] : false);//$extendedArray['street'];
                $userNew['extended']['house'] = (isset($extendedArray['house']) ? $extendedArray['house'] : false);//$extendedArray['house'];
                $userNew['extended']['room'] = (isset($extendedArray['room']) ? $extendedArray['room'] : false);//$extendedArray['room'];
                $userNew['extended']['housing'] = (isset($extendedArray['housing']) ? $extendedArray['housing'] : false);//$extendedArray['housing'];
                $userNew['extended']['fathername'] = (isset($extendedArray['fathername']) ? $extendedArray['fathername'] : false);//$extendedArray['housing'];


                $userNew['extended']['warehouse_name'] = (isset($extendedArray['warehouse_name']) ? $extendedArray['warehouse_name'] : false);

                $userNew['extended']['street_id_np'] = (isset($extendedArray['street_id_np']) ? $extendedArray['street_id_np'] : false);

                //city_id_np

                $userNew['extended']['parade'] = (isset($extendedArray['parade']) ? $extendedArray['parade'] : false);//$extendedArray['parade'];
                $userNew['extended']['doorphone'] = (isset($extendedArray['doorphone']) ? $extendedArray['doorphone'] : false);//$extendedArray['doorphone'];
                $userNew['extended']['floor'] = (isset($extendedArray['floor']) ? $extendedArray['floor'] : false);//$extendedArray['floor'];
                //$collectionNew[$key]['payment']=$extendedArray['payment'];
                //$collectionNew[$key]['payment']=$extendedArray['payment'];


                $user = $userNew;
//                $userObject = $this->modx->getObject('modUserProfile', array('internalKey' => $user['id']));
//
//                $userObject->set('extended', $userNew['extended']);
//
//                if ($userObject->save()) {

                    return $this->success('', $user);
//                }else{
//
//                }
            } else {
                return $this->failure($this->modx->lexicon('rest.err_field_ns', array(
                    'field' => 'phone',//$this->primaryKeyField,
                )));
            }


        } else {
            return $this->failure($this->modx->lexicon('rest.err_field_ns', array(
                'field' => 'phone',//$this->primaryKeyField,
            )));
        }

    }

    public function getList()
    {
        $leftJoin = [];
        $leftJoin["Profile"] = [
            "class" => "modUserProfile",
            "alias" => "Profile",
        ];

        $limit = 20;
        $offset = 0;
        if ($this->getProperty('limit'))
            $limit = $this->getProperty('limit');

        if ($limit > 100)
            $limit = 100;

        $select = [];
        //$select['modUser'] = '*';//'id, pagetitle, alias ,menuindex, description';


        if ($this->getProperty('offset'))
            $offset = $this->getProperty('offset');


        $q = $this->modx->newQuery('modUser');
        $q->select(array(
//            'modUser.*',
            'modUser.id',
            'modUser.username',
            'modUser.active',
            'modUser.registr_date',
            'modUser.subscribe',
            'modUser.subscribe_date',
            'modUser.registr',
            'modUser.created',
            'modUser.last_order',
            'modUser.total_orders',
            'modUser.count_orders_close',
            'modUser.summa_close_orders',
            'modUser.summa_all_orders',
            'modUser.bonuses',
            'modUser.date_clear_bonuses',
            'Profile.fullname',
            'Profile.email',
            'Profile.phone',
            'Profile.mobilephone',
            'Profile.blocked',
            'Profile.address',
            'Profile.country',
            'Profile.city',
            'Profile.state',
            'Profile.comment',
            'Profile.lastname',
            'Profile.extended'
            //'Profile.*'
        ));

        if ($this->getProperty('created')) {
            $this->whereCondition['modUser.created:>='] = str_replace('T', ' ', $this->getProperty('created'));
        }

        if ($this->getProperty('registr_date')) {
            $this->whereCondition['modUser.registr_date:>='] = str_replace('T', ' ', $this->getProperty('registr_date'));
        }


        $q->where($this->whereCondition);


        $q->innerJoin('modUserProfile', 'Profile', 'Profile.internalKey = modUser.id');

        $q->limit($limit, $offset);
        $q->prepare();
        $q->stmt->execute();
        $collection = $q->stmt->fetchAll(PDO::FETCH_ASSOC);

        //echo $q->toSql();

        $collectionNew = $collection;
        foreach ($collection as $key => $col) {

            $extended = $collectionNew[$key]['extended'];
            $extendedArray = json_decode($extended, true);
            $collectionNew[$key]['discount'] = (isset($extendedArray['discount']) ? $extendedArray['discount'] : false);
            $collectionNew[$key]['city_id'] = (isset($extendedArray['city_id']) ? $extendedArray['city_id'] : false);//$extendedArray['city_id'];
            $collectionNew[$key]['delivery'] = (isset($extendedArray['delivery']) ? $extendedArray['delivery'] : false);//$extendedArray['delivery'];
            $collectionNew[$key]['warehouse'] = (isset($extendedArray['warehouse']) ? $extendedArray['warehouse'] : false);//$extendedArray['warehouse'];
            $collectionNew[$key]['street'] = (isset($extendedArray['street']) ? $extendedArray['street'] : false);//$extendedArray['street'];
            $collectionNew[$key]['house'] = (isset($extendedArray['house']) ? $extendedArray['house'] : false);//$extendedArray['house'];
            $collectionNew[$key]['room'] = (isset($extendedArray['room']) ? $extendedArray['room'] : false);//$extendedArray['room'];
            $collectionNew[$key]['housing'] = (isset($extendedArray['housing']) ? $extendedArray['housing'] : false);//$extendedArray['housing'];
            $collectionNew[$key]['fathername'] = (isset($extendedArray['fathername']) ? $extendedArray['fathername'] : false);//$extendedArray['housing'];
            $collectionNew[$key]['street_id_np'] = (isset($extendedArray['street_id_np']) ? $extendedArray['street_id_np'] : false);//$extendedArray['housing'];


            $collectionNew[$key]['parade'] = (isset($extendedArray['parade']) ? $extendedArray['parade'] : false);//$extendedArray['parade'];
            $collectionNew[$key]['doorphone'] = (isset($extendedArray['doorphone']) ? $extendedArray['doorphone'] : false);//$extendedArray['doorphone'];
            $collectionNew[$key]['floor'] = (isset($extendedArray['floor']) ? $extendedArray['floor'] : false);//$extendedArray['floor'];
            //$collectionNew[$key]['payment']=$extendedArray['payment'];
            //$collectionNew[$key]['payment']=$extendedArray['payment'];

            unset($collectionNew[$key]['extended']);
        }


        $collection = $collectionNew;

//        $collection = $this->pdo->getCollection(
//            'modUser',//$this->classKey,
//            $this->whereCondition,
//            [
//                'limit'=>10
//            ]
//        );

        $total = count($collection);

//        echo '<pre>';
//        print_r($collection);
//        echo '</pre>';
//        die();


        return $this->collection($collection, $total);
    }

    public function read($id)
    {
        if (empty($id)) {
            return $this->failure($this->modx->lexicon('rest.err_field_ns', array(
                'field' => $this->primaryKeyField,
            )));
        }


        $q = $this->modx->newQuery('modUser');
        $q->select(array(
            'modUser.id',
            'modUser.username',
            'modUser.active',
            'modUser.registr_date',
            'modUser.subscribe',
            'modUser.subscribe_date',
            'modUser.registr',
            'modUser.created',
            'modUser.last_order',
            'modUser.total_orders',
            'modUser.count_orders_close',
            'modUser.summa_close_orders',
            'modUser.summa_all_orders',
            'modUser.bonuses',
            'modUser.date_clear_bonuses',
            'Profile.fullname',
            'Profile.email',
            'Profile.phone',
            'Profile.mobilephone',
            'Profile.blocked',
            'Profile.address',
            'Profile.country',
            'Profile.city',
            'Profile.state',
            'Profile.comment',
            'Profile.lastname',
            'Profile.extended'
            //'Profile.*'
        ));

        $q->where('modUser.id=' . $id);
        $q->innerJoin('modUserProfile', 'Profile', 'Profile.internalKey = modUser.id');

        $q->limit(1);
        $q->prepare();

        $q->stmt->execute();
        $user = $q->stmt->fetch(PDO::FETCH_ASSOC);

        if ($user) {
            $userNew = $user;
            //foreach ($collection as $key => $col) {

            $extended = $userNew['extended'];
            $extendedArray = json_decode($extended, true);
            unset($userNew['extended']);
            $userNew['extended'] = [];
            $userNew['extended']['discount'] = (isset($extendedArray['discount']) ? $extendedArray['discount'] : false);
            $userNew['extended']['city_id'] = (isset($extendedArray['city_id']) ? $extendedArray['city_id'] : false);//$extendedArray['city_id'];
            $userNew['extended']['delivery'] = (isset($extendedArray['delivery']) ? $extendedArray['delivery'] : false);//$extendedArray['delivery'];
            $userNew['extended']['warehouse'] = (isset($extendedArray['warehouse']) ? $extendedArray['warehouse'] : false);//$extendedArray['warehouse'];
            $userNew['extended']['street'] = (isset($extendedArray['street']) ? $extendedArray['street'] : false);//$extendedArray['street'];
            $userNew['extended']['house'] = (isset($extendedArray['house']) ? $extendedArray['house'] : false);//$extendedArray['house'];
            $userNew['extended']['room'] = (isset($extendedArray['room']) ? $extendedArray['room'] : false);//$extendedArray['room'];
            $userNew['extended']['housing'] = (isset($extendedArray['housing']) ? $extendedArray['housing'] : false);//$extendedArray['housing'];
            $userNew['extended']['fathername'] = (isset($extendedArray['fathername']) ? $extendedArray['fathername'] : false);//$extendedArray['housing'];

            $userNew['extended']['street_id_np'] = (isset($extendedArray['street_id_np']) ? $extendedArray['street_id_np'] : false);//$extendedArray['parade'];

            $userNew['extended']['parade'] = (isset($extendedArray['parade']) ? $extendedArray['parade'] : false);//$extendedArray['parade'];
            $userNew['extended']['doorphone'] = (isset($extendedArray['doorphone']) ? $extendedArray['doorphone'] : false);//$extendedArray['doorphone'];
            $userNew['extended']['floor'] = (isset($extendedArray['floor']) ? $extendedArray['floor'] : false);//$extendedArray['floor'];
            //$collectionNew[$key]['payment']=$extendedArray['payment'];
            //$collectionNew[$key]['payment']=$extendedArray['payment'];


            $user = $userNew;
        }


        return $this->success('', $user);

    }


}