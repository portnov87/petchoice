<?php
//error_reporting(E_ALL | E_STRICT);
//ini_set('display_errors', false);
//ini_set('error_reporting', E_ALL);
//ini_set('display_errors', 1);
//ini_set('display_startup_errors', 1);
class shopOrder extends shopBaseRestController
{
    public $allowedMethods = array('post', 'get');

    public function getList()
    {

        if ($this->getProperty('date_begin')) {
            $this->whereCondition['msOrder.createdon:>='] = str_replace('T', ' ', $this->getProperty('date_begin'));
        }
        if ($this->getProperty('date_update')) {

            $this->whereCondition['msOrder.updatedon:>='] = str_replace('T', ' ', $this->getProperty('date_update'));
        }


        //echo $this->getProperty('date_begin').' '.strtotime($this->getProperty('date_begin'))."\r\n\r\n". strtotime('2020-12-01 00:00:00');
        if ($this->getProperty('user_id')) {
            $this->whereCondition['msOrder.user_id:>='] = $this->getProperty('user_id');
        }

        $sort = 'createdon';

        $sortType = 'DESC';
        if ($this->getProperty('sort')) {

            $sort = $this->getProperty('sort');//'createdon';
            $sortType = 'DESC';
            //$this->whereCondition['msOrder.updatedon:>='] = strtotime($this->getProperty('date_update'));
        }
        if ($this->getProperty('sortType'))
            $sortType = $this->getProperty('sortType');


        $limit = 20;
        if ($this->getProperty('limit'))
            $limit = $this->getProperty('limit');

        if ($limit > 100)
            $limit = 100;
        $offset = 0;


        if ($this->getProperty('offset'))
            $offset = $this->getProperty('offset');


        $select = [];
        //$select['modUser'] = '*';//'id, pagetitle, alias ,menuindex, description';

        $q = $this->modx->newQuery('msOrder');
        $q->select(array(
            'msOrder.*',
            'msOrder.id',
            'msOrder.user_id',
            'msOrder.createdon',
            'msOrder.updatedon',
            'msOrder.date_shipping',
            'msOrder.time_shipping_from',
            'msOrder.time_shipping_to',
            'msOrder.num',
            'msOrder.cost',
            'msOrder.cart_cost',
            'msOrder.delivery_cost',
            'msOrder.status',
            'msOrder.payment_status',
            'msOrder.delivery',
            'msOrder.payment',
            'msOrder.address',
            'msOrder.comment',
            'msOrder.promocode_id',
            'msOrder.promocode',
            'msOrder.with_bonuses',
            'msOrder.bonuses',
            'msOrder.apply_bonuses',
            'msOrder.date_apply_bonuses',
            'msOrder.cost_without_bonuses',
            'msOrder.discount',
            'msOrder.number_ttn',
            'msOrder.id_1c',
            'msOrder.date_create_ttn',
            'msOrder.date_shipping_np',
            'msOrder.cost_shipping_np',
            'msOrder.status_np',
            'msOrder.date_update_status',
            'msOrder.city_id_np',
            'msOrder.pickup_id',
            'msOrder.version',
            'msOrder.kkm'
        ));

        //$q->leftJoin('msOrderAddress', 'OrderAddress', 'OrderAddress.id = msOrder.id');

        $q->where($this->whereCondition);
        $q->limit($limit, $offset);


        $q->sortby($sort, $sortType);

        $q->prepare();
        $q->stmt->execute();

        $collection = $q->stmt->fetchAll(PDO::FETCH_ASSOC);

        $collectionNew = $collection;
        foreach ($collection as $key => $col) {

            $orderId = $col['id'];
            $addressId = $col['address'];
            $q = $this->modx->newQuery('msOrderAddress');
            $q->select(array(
                'msOrderAddress.user_id',
                'msOrderAddress.receiver',
                'msOrderAddress.phone',
                'msOrderAddress.city',
                'msOrderAddress.street',
                'msOrderAddress.building',
                'msOrderAddress.room',
                'msOrderAddress.comment',
                'msOrderAddress.lastname',
                'msOrderAddress.warehouse',
                'msOrderAddress.warehouse_id',
                'msOrderAddress.city_id_np',
                'msOrderAddress.street_id_np',
                'msOrderAddress.housing',
                'msOrderAddress.parade',
                'msOrderAddress.doorphone',
                'msOrderAddress.floor',
                'msOrderAddress.fathername'

            ));
            $q->where('msOrderAddress.id=' . $addressId);
            //$q->where(array('msOrderAddress.id' => $addressId));
            $q->prepare();
            $q->stmt->execute();
            $address = $q->stmt->fetch(PDO::FETCH_ASSOC);

            if ($address)
                $collectionNew[$key]['address_data'] = $address;

            $productsNew = [];
            $qPr = $this->modx->newQuery('msOrderProduct');
            $qPr->select(array(
                //'msOrderProduct.*'
                'msOrderProduct.id',
                'msOrderProduct.product_id',
                'msOrderProduct.name',
                'msOrderProduct.count',
                'msOrderProduct.price',
                'msOrderProduct.cost',
                'msOrderProduct.options',
                'msOrderProduct.cost_old',
                'msOrderProduct.price_old',
                'msOrderProduct.skidka',
                'msOrderProduct.discount',
                'msOrderProduct.stock',
                'msOrderProduct.id_1c'
            ));
            $qPr->where('msOrderProduct.order_id=' . $orderId);
            $qPr->prepare();
            $qPr->stmt->execute();
            $products = $qPr->stmt->fetchAll(PDO::FETCH_ASSOC);

            $promoId = $col['promocode_id'];
            $productsNew = $products;
            $cost_without_promo = $cost_with_promo = $amountPromo = 0;
            $cost_old = 0;
            foreach ($products as $_key => $pr) {
                $options = json_decode($pr['options'], true);

                $cost_old = $cost_old + $pr['cost_old'];
                if ($promoId > 0) {
                    if ($pr['discount'] > 0) {
                        $amountPromoItem = ($pr['cost_old'] * ($pr['discount'] / 100));
                        $amountPromo = $amountPromo + $amountPromoItem;

                        $productsNew[$_key]['promocode_amount'] = $amountPromoItem;
                        $productsNew[$_key]['promocode_percent'] = $pr['discount'];
                    }
                }

                $productsNew[$_key]['options'] = $options;
            }

            $cost_without_promo = $cost_old;

            if ($promoId > 0) {
                $promocodeModel = $this->modx->getObject('PromocodeItem', $promoId);

                if ($promocodeModel) {

                    $promocodePercent = $promocodeModel->get('discount');

                    $collectionNew[$key]['promocode_amount'] = $amountPromo;
                    $collectionNew[$key]['promocode_percent'] = $promocodePercent;
                }
            }

            $collectionNew[$key]['products'] = $productsNew;

        }


        $collection = $collectionNew;
        $total = count($collection);

        return $this->collection($collection, $total);
    }


    public function post()
    {

        $inputData = json_decode(file_get_contents('php://input'), true);


        $orderId = (isset($inputData['order_id']) ? $inputData['order_id'] : false);


        if (isset($inputData['data_order'])) {
            $dataOrder = $inputData['data_order'];

            if ($orderId) {
                $order = $this->modx->getObject('msOrder', array('id' => $orderId));
                if ($order) {
                    if (isset($dataOrder['deleteProducts'])) {
                        foreach ($dataOrder['deleteProducts'] as $delProduct) {
                            $orderProduct = $this->modx->getObject('msOrderProduct', (int)$delProduct);

                            if ($orderProduct) {
                                $orderProduct->remove();
                            }
                        }
                    }
                }else{
                    $result = [
                        'message' => 'Ошибка. Заказ с ID  ' . $orderId . " не существует",
                        'success' => false
                    ];
                    return $this->failure('', $result);

                }
            } elseif (isset($dataOrder['id_1c'])) {
//                echo '<pre>$dataOrder';
//                print_r($dataOrder);
//                echo '</pre>';
                $order = $this->modx->getObject('msOrder', array('id_1c' => $dataOrder['id_1c']));
                if (!$order)
                    $order = $this->modx->newObject('msOrder');
                else {
                    $result = [
                        'message' => 'Ошибка. Заказ с ID 1C ' . $dataOrder['id_1c'] . " уже существует",
                        'success' => false
                    ];
                    return $this->failure('', $result);
                }
            } else
                $order = $this->modx->newObject('msOrder');


            if ($order) {
                if (!empty($order->get('id')))
                    $createObject = false;
                else
                    $createObject = true;


                if (isset($dataOrder['products'])) {
                    if (!$createObject) {
                        $products = $order->getMany('Products');
                        $productsData=$dataOrder['products'];
                        foreach ($productsData as $product) {

                            if (isset($product['id_1c'])){
                                $orderProduct = $this->modx->getObject('msOrderProduct', array('id_1c' => $product['id_1c'], 'order_id' => $orderId));
                                if (!$orderProduct) {

                                    $orderProduct = $this->modx->newObject('msOrderProduct');
                                    $orderProduct->set('order_id', $orderId);
                                }
                            }
                            /*elseif (isset($product['id'])) {

                                $orderProduct = $this->modx->getObject('msOrderProduct', array('id' => $product['id'], 'order_id' => $orderId));
                                if (!$orderProduct) {

                                    $orderProduct = $this->modx->newObject('msOrderProduct');
                                    $orderProduct->set('order_id', $orderId);
                                }
                            }*/
                            else {
                                $orderProduct = $this->modx->newObject('msOrderProduct');
                                $orderProduct->set('order_id', $orderId);
                            }

                            if (isset($product['product_id']))
                                $orderProduct->set('product_id', $product['product_id']);
                            if (isset($product['name']))
                                $orderProduct->set('name', $product['name']);
                            if (isset($product['count']))
                                $orderProduct->set('count', $product['count']);
                            if (isset($product['price']))
                                $orderProduct->set('price', $product['price']);
                            if (isset($product['cost']))
                                $orderProduct->set('cost', $product['cost']);

                            if (isset($product['options']))
                                $orderProduct->set('options', $product['options']);

                            if (isset($product['cost_old']))
                                $orderProduct->set('cost_old', $product['cost_old']);

                            if (isset($product['price_old']))
                                $orderProduct->set('price_old', $product['price_old']);

                            if (isset($product['skidka']))
                                $orderProduct->set('skidka', $product['skidka']);

                            if (isset($product['discount']))
                                $orderProduct->set('discount', $product['discount']);

                            if (isset($product['stock']))
                                $orderProduct->set('stock', $product['stock']);

                            if (isset($product['id_1c']))
                                $orderProduct->set('id_1c', $product['id_1c']);

                            $orderProduct->save();
                        }


                    } else {


                        foreach ($dataOrder['products'] as $v) {

                            if ($tmp = $this->modx->getObject('msProduct', $v['product_id'])) {
                                $name = $tmp->get('pagetitle');
                            } else {
                                $name = '';
                            }

                            $_id_1cOption = $v['id_1c'];


                            //$optionsArray=json_decode($options,true);

                            //$old_pr=$v['price'];

                            /* @var msOrderProduct $product */
                            $product = $this->modx->newObject('msOrderProduct');
                            $cost = $v['price'] * $v['count'];


                            $_dataProduct = array_merge($v, array(
                                'product_id' => $v['product_id']
                            , 'price_old' => round($v['oldprice'], 2)
                            , 'discount' => round($v['skidka1'], 2)
                            , 'name' => $name
                            , 'cost' => $cost
                            , 'id_1c' => $_id_1cOption
                            , 'cost_old' => round($v['oldprice'] * $v['count'], 2)
                            ));


                            $product->fromArray($_dataProduct);
                            $products[] = $product;
                        }


                        $order->addMany($products);

                    }


                }

                if (isset($dataOrder['address_data'])) {
                    $addressData = $dataOrder['address_data'];
                    if (count($addressData) > 0) {
                        if (!$createObject) {


                            $address = $order->getOne('Address');
                            if ($address) {
                                if (isset($addressData['receiver']))
                                    $address->set('receiver', $addressData['receiver']);

                                if (isset($addressData['phone']))
                                    $address->set('phone', $addressData['phone']);

                                if (isset($addressData['city']))
                                    $address->set('city', $addressData['city']);
                                if (isset($addressData['street']))
                                    $address->set('street', $addressData['street']);
                                if (isset($addressData['building']))
                                    $address->set('building', $addressData['building']);
                                if (isset($addressData['room']))
                                    $address->set('room', $addressData['room']);

                                if (isset($addressData['comment']))
                                    $address->set('comment', $addressData['comment']);

                                if (isset($addressData['lastname']))
                                    $address->set('lastname', $addressData['lastname']);
                                if (isset($addressData['warehouse']))
                                    $address->set('warehouse', $addressData['warehouse']);
                                if (isset($addressData['warehouse_id']))
                                    $address->set('warehouse_id', $addressData['warehouse_id']);

                                if (isset($addressData['street_id_np']))
                                    $address->set('street_id_np', $addressData['street_id_np']);
                                if (isset($addressData['city_id_np']))
                                    $address->set('city_id_np', $addressData['city_id_np']);

                                if (isset($addressData['housing']))
                                    $address->set('housing', $addressData['housing']);

                                if (isset($addressData['parade']))
                                    $address->set('parade', $addressData['parade']);
                                if (isset($addressData['doorphone']))
                                    $address->set('doorphone', $addressData['doorphone']);

                                if (isset($addressData['fathername']))
                                    $address->set('fathername', $addressData['fathername']);

                                if (isset($addressData['floor']))
                                    $address->set('floor', $addressData['floor']);

                                $address->save();
                            }
                        } else {


                            $address = $this->modx->newObject('msOrderAddress');

                            if (isset($addressData['receiver']))
                                $addressDataForSave['receiver'] = $addressData['receiver'];
                            //$address->set('receiver', $addressData['receiver']);

                            if (isset($addressData['phone']))
                                $addressDataForSave['phone'] = $addressData['phone'];
                            //$address->set('phone', $addressData['phone']);

                            if (isset($addressData['city']))
                                $addressDataForSave['city'] = $addressData['city'];
                            //$address->set('city', $addressData['city']);
                            if (isset($addressData['street']))
                                $addressDataForSave['street'] = $addressData['street'];
                            //$address->set('street', $addressData['street']);
                            if (isset($addressData['building']))
                                $addressDataForSave['building'] = $addressData['building'];
                            //$address->set('building', $addressData['building']);
                            if (isset($addressData['room']))
                                $addressDataForSave['room'] = $addressData['room'];
                            //$address->set('room', $addressData['room']);

                            if (isset($addressData['comment']))
                                $addressDataForSave['comment'] = $addressData['comment'];
                            //$address->set('comment', $addressData['comment']);

                            if (isset($addressData['lastname']))
                                $addressDataForSave['lastname'] = $addressData['lastname'];
                            //$address->set('lastname', $addressData['lastname']);
                            if (isset($addressData['warehouse']))
                                $addressDataForSave['warehouse'] = $addressData['warehouse'];
                            //$address->set('warehouse', $addressData['warehouse']);
                            if (isset($addressData['warehouse_id']))
                                $addressDataForSave['warehouse_id'] = $addressData['warehouse_id'];
                            //$address->set('warehouse_id', $addressData['warehouse_id']);

                            if (isset($addressData['city_id_np']))
                                $addressDataForSave['city_id_np'] = $addressData['city_id_np'];
                            //$address->set('city_id_np', $addressData['city_id_np']);

                            if (isset($addressData['street_id_np']))
                                $addressDataForSave['street_id_np'] = $addressData['street_id_np'];
                            //$address->set('street_id_np', $addressData['street_id_np']);

                            if (isset($addressData['housing']))
                                $addressDataForSave['housing'] = $addressData['housing'];
                            //$address->set('housing', $addressData['housing']);

                            if (isset($addressData['parade']))
                                $addressDataForSave['parade'] = $addressData['parade'];
                            //$address->set('parade', $addressData['parade']);
                            if (isset($addressData['doorphone']))
                                $addressDataForSave['doorphone'] = $addressData['doorphone'];
                            //$address->set('doorphone', $addressData['doorphone']);
                            if (isset($addressData['floor']))
                                $addressDataForSave['floor'] = $addressData['floor'];

                            if (isset($addressData['fathername']))
                                $addressDataForSave['fathername'] = $addressData['fathername'];

                            //$address->set('floor', $addressData['floor']);


                            if (isset($addressData['user_id']))
                                $addressDataForSave['user_id'] = $addressData['user_id'];
                            //$address->set('user_id', $addressData['user_id']);
                            else {
                                if (!isset($dataOrder['user_id'])) {
                                    $result = [
                                        'message' => 'Ошибка создания. Отсутствует user_id',
                                        'success' => false
                                    ];
                                    return $this->failure('', $result);
                                } else {
                                    $addressDataForSave['user_id'] = $dataOrder['user_id'];
                                    //$address->set('user_id', $dataOrder['user_id']);
                                }
                            }


                            $created = date('Y-m-d H:i:s');
                            //$addressDataForSave['createdon'] = $created;

                            $addressDataForSave['updatedon'] = $created;

                            $address->fromArray($addressDataForSave);

                            $order->addOne($address);


                        }
                    }
                }


                if (!isset($dataOrder['user_id'])) {
                    $result = [
                        'message' => 'Ошибка создания. Отсутствует user_id',
                        'success' => false
                    ];
                    return $this->failure('', $result);
                }
                $orderData['user_id'] = $dataOrder['user_id'];

                if (isset($dataOrder['date_shipping']))
                    $orderData['date_shipping'] = $dataOrder['date_shipping'];

                if (isset($dataOrder['time_shipping_from']))
                    $orderData['time_shipping_from'] = $dataOrder['time_shipping_from'];





                if (isset($dataOrder['time_shipping_to']))
                    $orderData['time_shipping_to'] = $dataOrder['time_shipping_to'];
                //$order->set('time_shipping_to', $dataOrder['time_shipping_to']);

                if (isset($dataOrder['cost']))
                    $orderData['cost'] = $dataOrder['cost'];
                //$order->set('cost', $dataOrder['cost']);

                if (isset($dataOrder['cart_cost']))
                    $orderData['cart_cost'] = $dataOrder['cart_cost'];
                //$order->set('cart_cost', $dataOrder['cart_cost']);

                if (isset($dataOrder['delivery_cost']))
                    $orderData['delivery_cost'] = $dataOrder['delivery_cost'];

                if (isset($dataOrder['pickup_id']))
                    $orderData['pickup_id'] = $dataOrder['pickup_id'];
                //$order->set('delivery_cost', $dataOrder['delivery_cost']);

                if (isset($dataOrder['delivery']))
                    $orderData['delivery'] = $dataOrder['delivery'];

                if (isset($dataOrder['num']))
                    $orderData['num'] = $dataOrder['num'];
                //$order->set('delivery', $dataOrder['delivery']);

                if (isset($dataOrder['payment']))
                    $orderData['payment'] = $dataOrder['payment'];
                //$order->set('payment', $dataOrder['payment']);


                if (isset($dataOrder['status']))
                    $orderData['status'] = $dataOrder['status'];
                //$order->set('status', $dataOrder['status']);

                $orderData['kkm'] = 0;
                if (isset($dataOrder['kkm'])) {
                    if ($dataOrder['kkm']=='1') {
                        $orderData['kkm'] = $dataOrder['kkm'];
                        $orderData['status'] = 2;
                        $orderData['num'] = $orderData['num'] . 'kkm';
                    }

                }




                if (isset($dataOrder['promocode_id']))
                    $orderData['promocode_id'] = $dataOrder['promocode_id'];
                //$order->set('promocode_id', $dataOrder['promocode_id']);

                if (isset($dataOrder['promocode']))
                    $orderData['promocode'] = $dataOrder['promocode'];
                //$order->set('promocode', $dataOrder['promocode']);

                if (isset($dataOrder['with_bonuses']))
                    $orderData['with_bonuses'] = $dataOrder['with_bonuses'];
                //$order->set('with_bonuses', $dataOrder['with_bonuses']);

                if (isset($dataOrder['bonuses']))
                    $orderData['bonuses'] = $dataOrder['bonuses'];
                //$order->set('bonuses', $dataOrder['bonuses']);

                if (isset($dataOrder['apply_bonuses']))
                    $orderData['apply_bonuses'] = $dataOrder['apply_bonuses'];
                //$order->set('apply_bonuses', $dataOrder['apply_bonuses']);

                if (isset($dataOrder['date_apply_bonuses']))
                    $orderData['date_apply_bonuses'] = $dataOrder['date_apply_bonuses'];
                //$order->set('date_apply_bonuses', $dataOrder['date_apply_bonuses']);

                if (isset($dataOrder['cost_without_bonuses']))
                    $orderData['cost_without_bonuses'] = $dataOrder['cost_without_bonuses'];
                //$order->set('cost_without_bonuses', $dataOrder['cost_without_bonuses']);

                if (isset($dataOrder['discount']))
                    $orderData['discount'] = $dataOrder['discount'];
                //$order->set('discount', $dataOrder['discount']);

                if (isset($dataOrder['number_ttn']))
                    $orderData['number_ttn'] = $dataOrder['number_ttn'];
                //$order->set('number_ttn', $dataOrder['number_ttn']);

                if (isset($dataOrder['date_create_ttn']))
                    $orderData['date_create_ttn'] = $dataOrder['date_create_ttn'];
                //$order->set('date_create_ttn', $dataOrder['date_create_ttn']);

                if (isset($dataOrder['date_shipping_np']))
                    $orderData['date_shipping_np'] = $dataOrder['date_shipping_np'];
                //$order->set('date_shipping_np', $dataOrder['date_shipping_np']);

                if (isset($dataOrder['cost_shipping_np']))
                    $orderData['cost_shipping_np'] = $dataOrder['cost_shipping_np'];
                //$order->set('cost_shipping_np', $dataOrder['cost_shipping_np']);

                if (isset($dataOrder['status_np']))
                    $orderData['status_np'] = $dataOrder['status_np'];
                //$order->set('status_np', $dataOrder['status_np']);

                if (isset($dataOrder['city_id_np']))
                    $orderData['city_id_np'] = $dataOrder['city_id_np'];


                if (isset($dataOrder['pickup_id']))
                    $orderData['pickup_id'] = $dataOrder['pickup_id'];
                //$order->set('pickup_id', $dataOrder['pickup_id']);

                if (isset($dataOrder['payment_status']))
                    $orderData['payment_status'] = $dataOrder['payment_status'];
                //$order->set('payment_status', $dataOrder['payment_status']);

                if (isset($dataOrder['ref_np_ttn']))
                    $orderData['ref_np_ttn'] = $dataOrder['ref_np_ttn'];

                if (isset($dataOrder['date_update_status']))
                    $orderData['date_update_status'] = $dataOrder['date_update_status'];


                if (isset($dataOrder['id_1c']))
                    $orderData['id_1c'] = $dataOrder['id_1c'];
                else {
                    $result = [
                        'message' => 'Ошибка. Не указан ID 1C',
                        'success' => false
                    ];
                    return $this->failure('', $result);
                }


                $created = date('Y-m-d H:i:s');
              //  $orderData['createdon'] = $created;
                $orderData['updatedon'] = $created;


                $order->fromArray($orderData);

                if ($order->save()) {

                    //changeOrderStatus
                    if ($createObject) {


                       // if ($order->get('status')==)
                        $order->setFinishPaymentStatus();

                        $user_id = $order->get('user_id');
                        $user = $this->modx->getObject('modUser', $user_id);


                        $miniShop2 = $this->modx->getService('minishop2');
                        $miniShop2->initialize($this->modx->context->key);

                        $miniShop2->charge_bonuses($order);
                        $count_orders_close = (int)$user->get('count_orders_close');
                        $count_orders_close = $count_orders_close + 1;
                        $user->set('count_orders_close', $count_orders_close);


                        $summa_close_orders = 0;

                        $profile = $user->getOne('Profile');
                        $q = $this->modx->newQuery('msOrder');
                        $q->where(array('msOrder.status' => 2, 'msOrder.user_id:=' => $user_id));
                        $q->select(array('msOrder.id', 'msOrder.cart_cost'));
                        $ids = [];
                        $total = 0;
                        if ($q->prepare() && $q->stmt->execute()) {
                            $orders = $q->stmt->fetchAll(PDO::FETCH_ASSOC);
                            foreach ($orders as $order_i) {
                                $ids[] = $order_i['id'];
                                $total += $order_i['cart_cost'];
                            }
                        }
                        $user->set('summa_close_orders', $total);

                        if ($user->get('registr') == '1') {

                            $extended = $profile->get('extended');
                            $user_discount = $extended['discount'];

                            $q = $this->modx->newQuery('msUserDiscount', array('percent:>' => $user_discount));
                            $q->sortby('percent', 'ASC');
                            $q->select('`msUserDiscount`.`percent`,`msUserDiscount`.`total`');
                            $q->limit(1);
                            $next_discount_total = 0;
                            $next_discount = 0;
                            if ($q->prepare() && $q->stmt->execute()) {
                                while ($row = $q->stmt->fetch(PDO::FETCH_ASSOC)) {
                                    $next_discount = $row['percent'];
                                    $next_discount_total = $row['total'];
                                }
                            }

                            if ($total >= $next_discount_total) {
                                if ($user_discount < $next_discount) {


                                    $extended = $profile->get('extended');
                                    $extended['discount'] = $next_discount;
                                    $profile->set('extended', $extended);

                                    $profile->save();

                                }
                            }

                        }



                        $action = '1c_chnage';
                        $message = 'Создан через 1с';
                        //$orderProduct = $this->modx->newObject('msOrderProduct');

                        $created = date('Y-m-d H:i:s');
                        $logData = array(
                            'order_id' => $order->get('id')
                        , 'user_id' => $order->get('user_id')
                        , 'timestamp' => $created
                        , 'action' => $action
                        , 'entry' => 0
                        , 'message' => $message
                        );
                        $_log = $this->modx->newObject('msOrderLog');//, $logData);
                        $_log->set('order_id', $order->get('id'));
                        //echo $order->get('id');
                        $_log->set('user_id', $order->get('user_id'));
                        $_log->set('timestamp', $created);
                        $_log->set('action', $action);
                        $_log->set('entry', 0);
                        $_log->set('message', $message);
//                        $_log->set('ip',$this->modx->request->getClientIp());


                        if ($_log->save()) {
                            $result = [
                                'message' => 'Заказ создался',
                                'order_id' => $order->get('id'),
                                'success' => true
                            ];
                        } else {
                            $result = [
                                'message' => 'Ошибка сохранения',
                                'success' => false
                            ];
                            return $this->failure('', $result);
                        }
                    } else {
                        $action = '1c_chnage';
                        $message = 'Изменен через 1с';
                        //$orderProduct = $this->modx->newObject('msOrderProduct');

                        $created = date('Y-m-d H:i:s');
                        $logData = array(
                            'order_id' => $order->get('id')
                        , 'user_id' => $order->get('user_id')
                        , 'timestamp' => $created
                        , 'action' => $action
                        , 'entry' => 0
                        , 'message' => $message
                        );
                        $_log = $this->modx->newObject('msOrderLog');//, $logData);
                        $_log->set('order_id', $order->get('id'));
                        //echo $order->get('id');
                        $_log->set('user_id', $order->get('user_id'));
                        $_log->set('timestamp', $created);
                        $_log->set('action', $action);
                        $_log->set('entry', 0);
                        $_log->set('message', $message);
//                        $_log->set('ip',$this->modx->request->getClientIp());


                        if ($_log->save()) {
                            $result = [
                                'message' => 'Заказ обновился',
                                'success' => true
                            ];
                        } else {
                            $result = [
                                'message' => 'Ошибка сохранения',
                                'success' => false
                            ];
                            return $this->failure('', $result);
                        }
                    }

                    //return $this->success('', $result);
                } else {
                    $result = [
                        'message' => 'Ошибка сохранения',
                        'success' => false
                    ];
                    return $this->failure('', $result);
                }
            }
        } else {
            $result = [
                'message' => 'Ошибка. Не указан data_order',
                'success' => false
            ];
        }

        return $this->success('', $result);
//        } else {
//            $result = [
//                'message' => 'Нет order_id',
//                'success' => false
//            ];
//            return $this->failure('', $result);
//        }

    }


    public function read($id)
    {
        if (empty($id)) {
            return $this->failure($this->modx->lexicon('rest.err_field_ns', array(
                'field' => $this->primaryKeyField,
            )));
        }

        $this->whereCondition['id'] = $id;


        $q = $this->modx->newQuery('msOrder');
        $q->select(array(
            'msOrder.*',
        ));


        $q->where($this->whereCondition);
        $q->prepare();
        $q->stmt->execute();
        $order = $q->stmt->fetch(PDO::FETCH_ASSOC);


        $orderId = $order['id'];
        $addressId = $order['address'];
        $q = $this->modx->newQuery('msOrderAddress');
        $q->select(array(
            'msOrderAddress.user_id',
                'msOrderAddress.receiver',
                'msOrderAddress.phone',
                'msOrderAddress.city',
                'msOrderAddress.street',
                'msOrderAddress.building',
                'msOrderAddress.room',
                'msOrderAddress.comment',
                'msOrderAddress.lastname',
                'msOrderAddress.warehouse',
                'msOrderAddress.warehouse_id',
                'msOrderAddress.city_id_np',
                'msOrderAddress.street_id_np',
                'msOrderAddress.housing',
                'msOrderAddress.parade',
                'msOrderAddress.doorphone',
                'msOrderAddress.fathername',
                'msOrderAddress.floor'
        ));
        $q->where('msOrderAddress.id=' . $addressId);
        //$q->where(array('msOrderAddress.id' => $addressId));
        $q->prepare();
        $q->stmt->execute();
        $address = $q->stmt->fetch(PDO::FETCH_ASSOC);

//            echo '<pre>'.$orderId.' '.$addressId;
//            print_r($address);
//            echo '</pre>';
//            die();
        if ($address)
            $order['address_data'] = $address;

        $productsNew = [];
        $qPr = $this->modx->newQuery('msOrderProduct');
        $qPr->select(array(
            'msOrderProduct.id',
            'msOrderProduct.product_id',
            'msOrderProduct.name',
            'msOrderProduct.count',
            'msOrderProduct.price',
            'msOrderProduct.cost',
            'msOrderProduct.options',
            'msOrderProduct.cost_old',
            'msOrderProduct.price_old',
            'msOrderProduct.skidka',
            'msOrderProduct.discount',
            'msOrderProduct.stock',
            'msOrderProduct.id_1c'
        ));
        $qPr->where('msOrderProduct.order_id=' . $orderId);
        $qPr->prepare();
        $qPr->stmt->execute();
        $products = $qPr->stmt->fetchAll(PDO::FETCH_ASSOC);


        $promoId = $order['promocode_id'];
        $productsNew = $products;
        $cost_without_promo = $cost_with_promo = $amountPromo = 0;
        $cost_old = 0;


        $cost_without_promo = $cost_old;

        if ($promoId > 0) {
            $promocodeModel = $this->modx->getObject('PromocodeItem', $promoId);

            if ($promocodeModel) {

                $promocodePercent = $promocodeModel->get('discount');


                $order['promocode_percent'] = $promocodePercent;
            }
        }


        $productsNew = $products;
        foreach ($products as $_key => $pr) {
            $options = json_decode($pr['options'], true);
            $cost_old = $cost_old + $pr['cost_old'];
            if ($promoId > 0) {
                if ($pr['discount'] > 0) {
                    $amountPromoItem = round(($pr['cost_old'] * ($pr['discount'] / 100)), 2);
                    $amountPromo = $amountPromo + $amountPromoItem;

                    $productsNew[$_key]['promocode_amount'] = $amountPromoItem;
                    $productsNew[$_key]['promocode_percent'] = $pr['discount'];
                }
            }

            $productsNew[$_key]['options'] = $options;
        }


        if ($amountPromo > 0)
            $order['promocode_amount'] = $amountPromo;


        $order['products'] = $productsNew;


        return $this->success('', $order);

    }


}