<?php

require_once dirname(dirname(__FILE__)) . '/resourcescontroller.php';

class shopVendors extends shopResourcesRestController
{

    public $classKey = 'msVendor';

    public function initialize()
    {
        parent::initialize();

    }


    public function getList()
    {


        $q = $this->modx->newQuery('msVendor');
        $q->select(array(
            'msVendor.*',
        ));

        $limit = 20;
        $offset=0;
        if ($this->getProperty('limit'))
            $limit = $this->getProperty('limit');

        if ($limit > 100)
            $limit = 100;

        if ($this->getProperty('offset'))
            $offset=$this->getProperty('offset');


        //$q->where($this->whereCondition);
        $q->limit($limit,$offset);
        $q->prepare();
        $q->stmt->execute();
        $collection = $q->stmt->fetchAll(PDO::FETCH_ASSOC);
//        echo '<pre>$collection';
//        print_r($collection);
//        echo '</pre>';
//
//
//        die();


//        $collection = $this->pdo->getCollection(
//            $this->classKey,
//            $this->whereCondition
//
//        );

        $total = count($collection);

    /*    foreach ($collection as $key => $cat) {
            $collection[$key]['image'] = 'https://petchoice.ua' . $cat['image'];
//            $children = $this->modx->getChildIds($cat['id'], 1, array('context' => 'web'));
  //          $collection[$key]['products_count'] = count($children);
        }
*/

        return $this->collection($collection, $total);
    }


    public function read($id)
    {
        if (empty($id)) {
            return $this->failure($this->modx->lexicon('rest.err_field_ns', array(
                'field' => $this->primaryKeyField,
            )));
        }

        $this->whereCondition['id'] = $id;

        unset($this->whereCondition['deleted']);
        unset($this->whereCondition['published']);
        $q = $this->modx->newQuery('msVendor');
        $q->select(array(
            'msVendor.*',
        ));



        $q->where($this->whereCondition);
        $q->prepare();
        $q->stmt->execute();
        $vendor = $q->stmt->fetch(PDO::FETCH_ASSOC);
//        echo '<pre>$vendor'.$q->toSql();
//        print_r($vendor);
//        echo '</pre>';
//        echo '<pre>$vendor'.$q->toSql();
//        print_r($this->whereCondition);
//        echo '</pre>';
//        die();

        return $this->success('', $vendor);



        //$category = $this->pdo->getArray(
//            $this->classKey,
//            $this->whereCondition,
//            array(
//                'select' => array(
//                    'msVendor' => '*',//'id, name, country, url,post, description'
//                ),
//            )
//        );
//        if (empty($category)) {
//            return $this->failure($this->modx->lexicon('rest.err_obj_nf', array(
//                'class_key' => $this->classKey,
//            )));
//        }
//
//        $afterRead = $this->afterRead($category);
//
//        if ($afterRead !== true && $afterRead !== null) {
//            return $this->failure($afterRead === false ? $this->errorMessage : $afterRead);
//        }
//
//        return $this->success('', $category);

    }
}