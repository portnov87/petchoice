<?php

class shopPetstype extends shopBaseRestController
{
    public $allowedMethods = array('get', 'post');



    public function getList()
    {
        $select = [];
        $q = $this->modx->newQuery('msPetType');
        $q->select(array(
            'msPetType.*'
        ));

        //$q->where(['id:=' => $userId]);

        $q->prepare();
        $q->stmt->execute();
        $collection = $q->stmt->fetchAll(PDO::FETCH_ASSOC);

        $total = count($collection);

        return $this->collection($collection, $total);
    }




    public function read($id)
    {
        if (empty($id)) {
            return $this->failure($this->modx->lexicon('rest.err_field_ns', array(
                'field' => $this->primaryKeyField,
            )));
        }
        $select = [];
        //$select['modUser'] = '*';//'id, pagetitle, alias ,menuindex, description';

        $this->whereCondition['id'] = $id;

        unset($this->whereCondition['deleted']);
        unset($this->whereCondition['published']);

        $q = $this->modx->newQuery('msPetType');
        $q->select(array(
            'msPetType.*'
        ));

        $q->where($this->whereCondition);

        $q->prepare();
        $q->stmt->execute();
        $status = $q->stmt->fetch(PDO::FETCH_ASSOC);


        return $this->success('', $status);

    }


}