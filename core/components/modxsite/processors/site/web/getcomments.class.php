<?php

class modSiteWebGetcommentsProcessor extends modProcessor
{


    public function initialize()
    {

        $this->setDefaultProperties(array(
            'sort' => "RAND()",
            'dir' => 'ASC',
            //'showhidden'        => false,
            //'showunpublished'   => false,
            //'getPage'           => false,
            'limit' => 1,
            'idcontent' => '463'
        ));


        return parent::initialize();
    }

    public function process()
    {

        $output = '';
        $idcontent = $this->getProperty('idcontent');
        //$items=$this->getComments($idcontent);
        /*echo '<pre>';
        print_r($comments);
        echo '</pre>';*/
        //die();
        // get menu items
        if (!$items = $this->getComments($idcontent)) {
            return;
        }
        /*
        // prepare menu items
        $items = $this->prepareMenu($items);
        */
        return array(
            'success' => true,
            'message' => '',
            'object' => $items,
        );
    }

    public function getComments($incontent)
    {

        $limit = $this->getProperty('limit');


        $Tickets = $this->modx->getService('tickets', 'Tickets', $this->modx->getOption('tickets.core_path', null, $this->modx->getOption('core_path') . 'components/tickets/') . 'model/tickets/');
        $Tickets->initialize($this->modx->context->key);


        // Joining tables
        //if ($action == 'comments') {
        $class = 'TicketComment';

        $innerJoin = array();

        // пол года
        $date = date('Y-m-d', strtotime('-200 days'));
        $datenow = date('Y-m-d');
        //echo $date;

        $q = $this->modx->newQuery($class);


        $q->leftJoin('modUser', 'User', '`User`.`id` = `TicketComment`.`createdby`');
        $q->leftJoin('modUserProfile', 'Profile', '`Profile`.`internalKey` = `TicketComment`.`createdby`');

        $q->leftJoin('TicketThread', 'Thread', '`TicketComment`.`thread` = `Thread`.`id` AND `Thread`.`deleted` = 0');


        $select = array(
            'TicketComment.text',// as text',
            'TicketComment.id as commentid',
            'TicketComment.date_ago',// as date_ago',
            'Profile.avatar',// as avatar',
            'Profile.fullname',// as fullname',


            //'TicketComment.createdon:<='=>$datenow,

        );


        $where['TicketComment.createdon:>='] = $date;
        $where['TicketComment.deleted'] = 0;
        $where['TicketComment.published'] = 1;
        $where['TicketComment.parent'] = 0;
        $where['TicketComment.show_home'] = 1;

        $where['TicketComment.thread'] = $incontent;

        //$q->select($select);
        $q->select(array(
            'TicketComment.text',//,
            'TicketComment.id as commentid',
            'TicketComment.createdby',
            'TicketComment.createdon',
            'TicketComment.name',
            'Profile.fullname',//avatar',
            'Profile.photo as avatar',
            //'User.*'
            //'Profile.fullname',
        ));
        $q->where($where);


        $q->limit($limit);
        $q->sortby('RAND()');

        if ($q->prepare() && $q->stmt->execute()) {
            while ($row = $q->stmt->fetch(PDO::FETCH_ASSOC)) {


                $row['date_ago'] = $Tickets->dateFormat($row['createdon']);//date('Y-m-d',$row['createdon']);//$Tickets->dateFormat($row['createdon']);

                if ($row['avatar'] == '') $row['avatar'] = '/assets/images/no_photo.png';
                if (empty($row['createdby'])) {
                    $row['fullname'] = $row['name'];
                    $row['guest'] = 1;
                }


                $result[] = $row;
            }
        }

        /*echo '<pre>$result666';
        print_r($result);
        echo '</pre>';*/
        return $result;
    }

}

return 'modSiteWebGetcommentsProcessor';
