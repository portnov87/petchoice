<?php

class modSiteWebProductsGetOptionsListProcessor extends modProcessor
{

    protected $activeIDs = array();     // ID of active parents

    public function initialize()
    {

        $this->setDefaultProperties(array(
            'product_id' => false,      // Menu id
            'dostavka' => '',
            'cacheable' => false,
            'sortBy' => 'price',
            'sortOrder' => 'ASC'
        ));

        return parent::initialize();
    }


    public function process()
    {

        $output = '';


        if (!empty($this->modx->resource)) {//} AND $this->modx->resource instanceOf msProduct){
            $resource = $this->modx->resource;

            $items = $this->getOptions($resource->id);


        } else $items = false;

        return array(
            'success' => true,
            'message' => '',
            'object' => $items,
        );
    }

    public function sortprice($a, $b)
    {
        if ((float)$a['price_supplier'] === (float)$b['price_supplier']) return 0;
        return (float)$a['price_supplier'] > (float)$b['price_supplier'] ? 1 : -1;
    }

    public function getOptions($productid)
    {
        $action_product = 0;
        $action_product_value = 0;
        //echo $productid;
        $dostavka = $this->getProperty('dostavka');
        $miniShop2 = $this->modx->getService('minishop2');
        $docid = $productid;
        $tv_id = 1;


        if (!$this->modx->loadClass('msProduct', MODX_CORE_PATH . 'components/minishop2/model/minishop2/', false, true)) {
            return false;
        }
        $product = new msProduct($this->modx);


        $pid = $idcat = $this->modx->resource->parent;
        $dataproduct = $this->modx->resource->Data;
//        echo '<pre>$dataproduct';
//        print_r($dataproduct);
//        echo '</pre>';
//        die();


        $article = '';
        $toorder = $vendor = $new = $popular = false;


        $toorder = $dataproduct->toorder;



        $day_cat = $dataproduct->day_cat;
        $day_dog = $dataproduct->day_dog;
        $day_safeanimals = $dataproduct->day_safeanimals;

        $newyear = $dataproduct->newyear;
        $black_friday = $dataproduct->black_friday;

        $den_shivotnyh= $dataproduct->den_shivotnyh;
        $helloween = $dataproduct->helloween;
        $markdown = $dataproduct->markdown;
        $february = $dataproduct->february;


        $article = $dataproduct->article;
        $vendor = $dataproduct->vendor;
        $popular = $dataproduct->popular;
        $new = $dataproduct->new;
        $action_product = $dataproduct->adiscount;
        //https://petchoice.ua/koshkam/suxoj-korm/cat-chow-sterilized-dlya-sterilizovannyix-koshek

        $valaction = $product->getvalueaction($docid, $idcat);
        //echo '$valaction'.$valaction;

        //получаем инфу про акцию продукта
        // Получаем TV по ID документа
        $q = $this->modx->newQuery('modTemplateVar');
        $q->innerJoin('modTemplateVarResource', 'v', 'v.tmplvarid = modTemplateVar.id');
        $q->where(array(
            'v.contentid' => $docid,
            'modTemplateVar.id' => $tv_id,
        ));


        $q->limit(1);
        $q->select(array('modTemplateVar.*', 'v.value'));
        $value = array();
        $array = array();
        if ($q->prepare() && $q->stmt->execute()) {
            while ($tv = $q->stmt->fetch(PDO::FETCH_ASSOC)) {
                $value = $tv['value'];

            }
        }
        // Получаем массив из JSON-строки
        if (count($value) > 0) $array = $this->modx->fromJSON($value);

        // Пробегаемся по всему массиву и фильтруем по дате
        $data = array();
        $now = time();
        $i = 0;
        $limit = count($array);


        $data = $array;
        $valueproduct = '';
        $i = 1;
        $j = 0;
        $action = 0;

        $highPrice = 0;
        $lowPrice = 0;
        $rowop = array();

        $ecomerc = '';
        $namecategory_ec = '';
        $vendorname = '';

        $sql = "SELECT name,url,country FROM `modx_ms2_vendors` WHERE `id` ='$vendor' LIMIT 1";
        $q = $this->modx->prepare($sql);
        $q->execute();
        $res = $q->fetchAll(PDO::FETCH_ASSOC);

        foreach ($res as $v) $vendorname = $v['name'];
        $category_name = '';
        $cat_model = $this->modx->getObject('msCategory', $this->modx->resource->parent);
        if ($cat_model) {
            $category = $cat_model->get('pagetitle');
            $namecategory_ec = $category;
            $cat_model_parent = $this->modx->getObject('msCategory', $cat_model->get('parent'));
            if ($cat_model_parent) {
                $namecategory_ec = $category . ' - ' . $cat_model_parent->get('pagetitle');
                //$category_name = $category . '/' . $cat_model_parent->get('pagetitle');
            }

        }

        $all_bonuses = $sizes_pr = [];
        $action_product_value_array = [];
        $action_hide=0;
        $esputnikId=false;
        $esputnikPrice=0;
        $esputnikStock=0;
        $eFbProducts=[];
        foreach ($data as $key => $r) {
            $action = 0;
            $show = true;
            //echo 'disable_optio'.$r['disable_option'];
            if (isset($r['disable_option'])) {
                if ($r['disable_option'] == 1) {
                    $show = false;
                }
            }
            if ($show) {
                if ($r['action'] != '') {
                    $action = 1;
                    //$action_product=1;
                    $action_product_value_array[] = $r['action'];
                    //$action_product_value = $r['action'];
                } else {
                    if ($action == 0) {
                        if ($valaction != '') {
                            $action = 1;
                            //$action_product=1;
                            $action_product_value_array[] = $valaction;//$r['action'];
                            // $action_product_value = $valaction;

                        } else {
                            $action = 0;
                        }
                    }
                }


                if ($r['plusone'] == 1) $action_product = 1;
                //$output='<div class="product-photo-lbl action-lbl"><b>АКЦИЯ</b></div>';


                $r['valaction'] = $valaction;
                $r['idcategory'] = $idcat;
                $priceall = $product->getAllPrice($r);

                $price = $priceall['price'];
                $oldprice = $priceall['oldprice'];

                if ($j == 0) {
                    $price_prev = $price;
                    $weight_prev = $r['weight'];
                    $weight_prefix_prev = $r['weight_prefix'];
                }

                if ($dostavka == 'yes') {
                    $dostavka1 = 0; //echo $price.'pr ';
                } else {
                    $free = $this->modx->getOption('free_delivery_3');

                    if ($price >= $free) $dostavka1 = 1;//'<div class="col-xs-3 equal-height" style=""><span style="" class="icon free-icon"></span></div>';
                    else $dostavka1 = 0;
                }


                $pr_kg = '';
                $_w = trim($r['weight']);
                $_w = str_replace(',', '.', $_w);
                if (is_numeric($_w)) {
                    if ($r['weight_prefix'] == 'гр') {
                        $weight = round($r['weight'] / 1000, 2);
                        $pr_kg = $price / $weight;
                    } elseif ($r['weight_prefix'] == 'кг') {
                        $weight = str_replace(',', '.', $r['weight']);
                        $pr_kg = $price / $weight;
                    }

                }



                $r['price'] = str_replace(',', '.', $r['price']);
                $price = str_replace(',', '.', $price);

                $prices[] = $price;
                $r['price'] = $miniShop2->formatPrice($r['price']);
                $price = $miniShop2->formatPrice($price);
                //data-product-vendor="{$attr.product_vendor}"
                //data-product-category="{$attr.category_ec}"
                /*echo "<pre>";
                print_r($r);
                echo "</pre>";*/
                $availability_1 = 0;
                $availability_2 = 0;
                $availability_3=0;
                if (isset($r['availability_1'])) {
                    if ($r['availability_1'] > 0)
                        $availability_1 = $r['availability_1'];
                }
                if (isset($r['availability_2'])) {
                    if ($r['availability_2'] > 0)
                        $availability_2 = $r['availability_2'];
                }

                if (isset($r['availability_3'])) {
                    if ($r['availability_3'] > 0)
                        $availability_3 = $r['availability_3'];
                }
                $bonuses = 0;
                if (isset($r['bonuses']) && ($r['bonuses'] > 0)) {
                    $bonuses = $r['bonuses'];
                    $all_bonuses[] = $bonuses;
                    $sizes_pr[$bonuses] = $r['weight'] . ' ' . $r['weight_prefix'];
                }


                if (!$esputnikId) {
                    $esputnikId = $docid.'_'.$r['MIGX_id'];//$r['id_1c'];
                    $esputnikPrice=$price;
                    if (($availability_1>0)||($availability_2>0)||($availability_2>0))
                    {
                        $esputnikStock=1;
                    }
                }
                $rowop[] = array(
                    'id' => $r['MIGX_id'],
                    'idproduct' => $docid,
                    'markdown'=>$r['markdown'],
                    'by_weight'=>$r['by_weight'],
                    'markdown_text'=>$r['markdown_text'],
                    'availability_1' => $availability_1,
                    'availability_2' => $availability_2,
                    'availability_3' => $availability_3,

                    'price_kg' => round($pr_kg),
                    'nameproduct' => $this->modx->resource->pagetitle,
                    'articleproduct' => $article,//$r['artpost'],
                    'idx' => $i,
                    'price_prev' => (float)$price_prev,
                    'weight_prev' => $weight_prev,
                    'weight_prefix_prev' => $weight_prefix_prev,
                    'instock' => $r['instock'],
                    'weight' => $r['weight'],
                    'id_1c' => $r['id_1c'],
                    'weight_prefix' => $r['weight_prefix'],
                    'price_supplier' => $r['price_supplier'],
                    'currency_supplier' => $r['currency_supplier'],
                    'markup' => $r['markup'],
                    'action' => $action,
                    'action_number' => $r['action'],

                    'action_hide' => (isset($r['action_hide'])?$r['action_hide']:false),
                    //'dostavka'=>$dostavka,
                    'price' => $price,
                    'product_vendor' => $vendorname,
                    'category_ec' => $namecategory_ec,
                    'oldprice' => $oldprice,
                    'dostavka' => $dostavka1,
                    'bonuses' => $bonuses
                );
                $eFbProducts[]="{
                              id: '".$r['id_1c']."',
                              quantity: 1 
                            }";

                if (isset($r['action_hide']))
                    $action_hide=$r['action_hide'];
                $j++;
                $i++;
            }

        }
//        echo '<pre>';
//                print_r($rowop);
//                echo '</pre>';
//                die();

        $min_price = '';
        foreach ($rowop as $it) {
            if (($it['price'] != '') && ($it['price'] != '0')) {
                $min_price = $it['price'];
                break;
            }
        }

        $pagetitle = addslashes($this->modx->resource->pagetitle);
        //replace("'", '"', $this->modx->resource->pagetitle);
        $vendorname = addslashes($vendorname);
        //str_replace("'", '"', $vendorname);

        $ecomerc .= "dataLayer.push({
                                                      'ecommerce': {
                                                        'detail': {                                                          
                                                          'products': [{
                                                            'name': '" . $pagetitle . "',         // Name or ID is required.
                                                            'id': '" . $this->modx->resource->id . "',
                                                            'price': '" . $min_price . "',
                                                            'brand': '" . $vendorname . "',
                                                            'category': '" . $namecategory_ec . "',
                                                            'variant': ''
                                                           }]
                                                         }
                                                       },
                                  'event': 'gtm-ee-event',
                                'gtm-ee-event-category': 'Enhanced Ecommerce',
 'gtm-ee-event-action': 'Product Details',
 'gtm-ee-event-non-interaction': 'True',


                                });\r\n";

        $published=$this->modx->resource->published;
        if ($esputnikId) {
            $eSputnik = "eS('sendEvent', 'ProductPage', {
                'ProductPage': {
                    'productKey': '" . ($esputnikId ? $esputnikId : $article) . "',
                    'price': '" . $min_price . "',
                    'isInStock': $published
                }});";

            $eSputnik .= "fbq('track', 'ViewContent', {
                          content_type: 'product',
                          content_category: '$namecategory_ec',
                          content_name: '$pagetitle', // наименование товара
                          contents: [".implode(', ',$eFbProducts)."]
                        });";
//
//
//eS('sendEvent', 'ProductPage', {
//                'ProductPage': {
//                    'productKey': '" . ($esputnikId ? $esputnikId : $article) . "',
//                    'price': '" . $min_price . "',
//                    'isInStock': $published
//                }});";
        }


        function sortprice($a, $b)
        {
            if ((float)$a['price'] === (float)$b['price']) return 0;
            return (float)$a['price'] > (float)$b['price'] ? 1 : -1;
        }

        uasort($rowop, 'sortprice');


        $data = $array;
        $i = 1;
        $j = 0;
        $data1 = $rowop;
        $k1 = '';
        $outstock = 0;
        foreach ($data1 as $key => $r) {
            if ($r['instock'] == 0) {
                $k1 = $data1[$key];
                unset($data1[$key]);
                if ($k1 != '') $data1[] = $k1;
                $outstock++;
            }

        }

        $i = 1;
        foreach ($data1 as $key => $d) {
            $data1[$key]['num'] = $i;
            $i++;
        }

        $rowop = $data1;

        foreach ($rowop as $row) $outputattr[] = $row;

        $lowPrice = min($prices);
        $highPrice = max($prices);
        $outputoffers = '
					<span itemprop="offers" itemscope itemtype="http://schema.org/AggregateOffer">
						<meta itemprop="lowPrice" content="' . $lowPrice . '"/>
						<meta itemprop="highPrice" content="' . $highPrice . '"/>
						<meta itemprop="priceCurrency" content="UAH" />
					  </span>';

        $output['outputattr'] = $outputattr;
        $output['outputoffers'] = $outputoffers;
        $output['article'] = $article;
        $output['vendor'] = $vendor;
        $output['action_hide'] =$action_hide;

        $output['toorder'] = $toorder;




        $output['day_cat'] = $day_cat;
        $output['day_dog'] = $day_dog;
        $output['day_safeanimals'] = $day_safeanimals;

        $output['newyear'] = $newyear;
        $output['february'] = $february;
        $output['helloween'] = $helloween;

        $output['markdown'] = $markdown;
        $output['black_friday'] = $black_friday;

        $output['den_shivotnyh'] = $den_shivotnyh;

        $output['popular'] = $popular;
        $output['new'] = $new;

        $output['bonuses'] = 0;
        $output['size_bonuses'] = 0;

        if (isset($sizes_pr[max($all_bonuses)])) {
            $output['bonuses'] = max($all_bonuses);
            $output['size_bonuses'] = $sizes_pr[max($all_bonuses)];
        }
        $output['ecomerc'] = $ecomerc;

        $output['eSputnik'] = $eSputnik;
        //$output['infoproduct']=$infoproduct;
        //$output['dostavka']=$dostavka;

        $output['action_product'] = $action_product;

        $action_product_value = max($action_product_value_array);
        $output['action_product_value'] = $action_product_value;
        $output['maxpercentorder'] = $this->modx->getOption('limit_bonuses');
//        echo '<div style="display:none;"><pre>';
//        print_r($output);
//        echo '</pre></div>';
        return $output;
    }


}

return 'modSiteWebProductsGetOptionsListProcessor';
