<?php

class modSiteWebProductsGetProductsProcessor extends modProcessor
{

    protected $activeIDs = array();     // ID of active parents

    public function initialize()
    {

        $this->setDefaultProperties(array(
            'id' => 'menu',      // Menu id
            'cacheable' => false,
            'parents' => $this->modx->resource->id,
            'level' => 1,
            'sortBy' => 'menuindex',
            'sortOrder' => 'ASC',
            'levelClass' => '',
            'activeClass' => 'active',
            'ignoreHidden' => false,
            'showUnpublished' => false,
            'adiscount' => 0,
            'list'=>'',
            'limit' => 8
        ));

        return parent::initialize();
    }

    public function process()
    {

        $output = '';

        // get active parents
        if (!empty($this->modx->resource) AND $this->modx->resource instanceOf modResource) {
            $resource = $this->modx->resource;
            $this->activeIDs[] = $resource->id;

            while ($resource = $resource->getOne('Parent')) {
                $this->activeIDs[] = $resource->id;
            }
        }
        $products = $this->getMenuItems();

        // get menu items
        if (!$items = $this->getMenuItems()) {
            return;
        }
        /*
        // prepare menu items
        $items = $this->prepareMenu($items);
        */
        return array(
            'success' => true,
            'message' => '',
            'object' => $items,
        );
    }

    public function getMenuItems()
    {
        $items = array();

        $startId = $this->getProperty('startId');
        $level = $this->getProperty('level');
        $cacheable = $this->getProperty('cacheable');
        $id = $this->getProperty('id', 'menu');



        $polylang = $this->modx->getService('polylang', 'Polylang');
        $polyLangTools = $polylang->getTools();
        $language = false;
        $language = $polyLangTools->detectLanguage();
        $langCode='';
        if ($language->get('culture_key') == 'ua') {
            $langCode='ua';
        }

        $cacheKey = $this->modx->context->key . "/{$id}/{$startId}";

        if ($cacheable) {
            if ($fromCache = $this->modx->cacheManager->get($cacheKey)) {
                return $fromCache;
            }
        }

        if ($items = $this->getItems($startId, $level,$langCode)) {
            if ($cacheable) {
                $this->modx->cacheManager->set($cacheKey, $items);
            }
        }

        return $items;
    }

    protected function getItems($parent, $level,$langCode='')
    {
        $level--;
        $items = array();
        $class = 'msProduct';
        $q = $this->modx->newQuery('msProduct');

        $q->leftJoin('msProductData', 'Data', 'Data.id=msProduct.id');
        $q->leftJoin('msVendor', 'Vendor', 'Data.vendor = Vendor.id');

        if ($langCode=='ua')
        {
            $q->leftJoin('PolylangContent', 'polylang_content', '`polylang_content`.`content_id`=`msProduct`.`id` AND polylang_content.culture_key="ua"');
        }


        $thumb = '150x223';


        $where = $this->getDefaultConditions();
        $where1 = $this->getProperty('where');
        if ($where1['Data.adiscount'] == 1) {
            $q->where(array('Data.askidka:OR' => 1, 'Data.adiscount' => 1));
            $q->sortby('RAND()');
        } elseif ($where1['Data.new'] == 1) {
            $q->where(array('Data.new' => 1,'msProduct.published'=>1));
            $q->sortby('RAND()');
        }
        elseif (isset($where1['Vendor.id'])) {
            $q->where(array('Vendor.id' => $where1['Vendor.id']));

            $q->sortby('Data.hits', 'DESC');
        }

        if ($langCode=='ua') {
            //'msProduct.pagetitle',//
            $q->select(array(
                'msProduct.id',
                'msProduct.parent',
                'polylang_content.pagetitle',
                'msProduct.longtitle', 'msProduct.description',
                'Data.article',
                'msProduct.menutitle', 'msProduct.link_attributes',
                'msProduct.uri', 'msProduct.alias',
                'Data.adiscount',
                'Data.popular',
                'Data.new',
                'Vendor.name',
                'msProduct.parent',
                "Data.image"
            ));
        }else {

            $q->select(array('msProduct.id', 'msProduct.parent', 'msProduct.pagetitle',
                'msProduct.longtitle', 'msProduct.description',
                'Data.article',
                'msProduct.menutitle', 'msProduct.link_attributes',
                'msProduct.uri', 'msProduct.alias',
                'Data.adiscount',
                'Data.popular',
                'Data.new',
                'Vendor.name',
                'msProduct.parent',
                //"$thumb.url as image"
                "Data.image"
            ));
        }
        if (!function_exists('cmp1')) {
            function cmp1($a, $b)
            {
                return strnatcmp($a["price"], $b["price"]);

            }

        }
        $miniShop2 = $this->modx->getService('minishop2');
        $miniShop2->initialize($this->modx->context->key);

        $limit = $this->getProperty('limit');
        $q->limit($limit);
        $productmodel = $this->modx->newObject('msProduct');
       // $q->sortby('RAND()');

        if (($_SERVER['HTTP_HOST']=='m.petchoice.pp.ua')||($_SERVER['HTTP_HOST'] == 'm.petchoice.ua'))
            $limitoption = 5;
        else $limitoption = 3;

       // echo 'toSql'.$q->toSql();



        if ($q->prepare() && $q->stmt->execute()) {

            $ecomerc="";
            /*dataLayer.ecommerce.push({'impressions':[";"dataLayer.push({
                     'ecommerce': {
                       'currencyCode': 'UAH',
                       'impressions': [";
*/

            $num=1;
            while ($row = $q->stmt->fetch(PDO::FETCH_ASSOC)) {


                $idcat = $docid = $row['id'];
                $idcat = $row['parent'];
                $valaction = $productmodel->getvalueaction($row['id'], $idcat);

                // Получаем TV по ID документа
                $q3 = $this->modx->newQuery('modTemplateVar');
                $tv_id = 1;
                $q3->innerJoin('modTemplateVarResource', 'v', 'v.tmplvarid = modTemplateVar.id');
                $q3->where(array(
                    'v.contentid' => $row['id'],
                    'modTemplateVar.id' => $tv_id,
                ));
                $q3->limit(1);
                $q3->select(array('modTemplateVar.*', 'v.value'));
                $action = '';
                $actions=[];
                if ($q3->prepare() && $q3->stmt->execute()) {
                    while ($tv = $q3->stmt->fetch(PDO::FETCH_ASSOC)) {
                        $valueoptions = $tv['value'];
                        $arrayoptions = $this->modx->fromJSON($valueoptions);
                        usort($arrayoptions, 'cmp2');

                        $showAction=true;
                        if (isset($arrayoptions[0]['disable_option'])) {
                            if ($arrayoptions[0]['disable_option'] == 1) {
                                $showAction = false;
                            }
                        }

                        if ($showAction){
                            if ($arrayoptions[0]['action']) $actions[] = $arrayoptions[0]['action'];
                        }
                    }
                }

                if (count($actions)>0) $action=max($actions);
                else $action='';

                if ($action == '') {
                    //if ($valaction != '') $action = $valaction;
                }
                $valuactionproduct = $action;




                $outputattr = $outputtabs = array();
//echo  '<div style="display::none;"><pre>';print_r($arrayoptions);echo '</pre></div>';
                foreach ($arrayoptions as $keyopt => $ropt) {

                    $showAction=true;
                    if (isset($ropt['disable_option'])) {
                        if ($ropt['disable_option'] == 1) {
                            $showAction = false;
                        }
                    }

                    if ($showAction) {
                        $valuactionproduct = 0;
                        if ($ropt['action'] != '') {
                            $action = 1;
                            $valuactionproduct = $ropt['action'];
                        } else {
                            if ($valaction != '') $action = 1;
                            else $action = 0;
                        }

                        $ropt['valaction'] = $valaction;
                        $ropt['idcategory'] = $idcat;
                        $priceall = $productmodel->getAllPrice($ropt);
                        $price = $priceall['price'];
                        $oldprice = $priceall['oldprice'];
                        $arrayoptions[$keyopt]['price'] = $price;
                        $arrayoptions[$keyopt]['action'] = $action;
                        $arrayoptions[$keyopt]['action_number'] = $valuactionproduct;
                        $arrayoptions[$keyopt]['oldprice'] = $oldprice;
                        $arrayoptions[$keyopt]['availability_1'] = $arrayoptions[$keyopt]['availability_2'] = $arrayoptions[$keyopt]['availability_3'] = 0;
                        if (isset($ropt['availability_1'])) $arrayoptions[$keyopt]['availability_1'] = $ropt['availability_1'];
                        if (isset($ropt['availability_2'])) $arrayoptions[$keyopt]['availability_2'] = $ropt['availability_2'];
                        if (isset($ropt['availability_3'])) $arrayoptions[$keyopt]['availability_3'] = $ropt['availability_3'];
                    }
                }
                usort($arrayoptions, 'cmp1');

                $dataopt = $arrayoptions;
                $i = 1;
                $j = 0;
                $data1opt = $dataopt;
                $k1 = '';

                $outstock = 0;
                foreach ($data1opt as $keyopt => $ropt) {
                    if ($ropt['instock'] == 0) {
                        $k1 = $data1opt[$keyopt];
                        unset($data1opt[$keyopt]);
                        if ($k1 != '') $data1opt[] = $k1;
                        $outstock++;
                    }

                }


                $dataopt = $data1opt;

                if ($outstock == count($data1opt)) {
                    $data2 = array();
                    $data2[] = array_shift($data1opt);//[0];

                    $data1opt = $data2;
                }

                $categories=$productmodel->getTreeParent($row['id']);
                $categories_ar=array_reverse($categories);
                $category_name=implode(' - ',$categories_ar);
                $brand=addslashes($row['name']);
                $name=addslashes($row['pagetitle']);

                $row['brand']=$brand;
                $row['category']=$category_name;
                $row['name']=$name;

                $row['position']=$num;
                $action_hide=0;
                $cartkey=$weight_prefix='';
                $size=$firstPrice=$option_id=0;
                foreach ($dataopt as $keyopt => $ropt) {
                    //echo 'disable_optio'.$ropt['disable_option'].'<br/>';
                    $show = true;
                    if (isset($ropt['disable_option'])) {
                        if ($ropt['disable_option'] == 1) {
                            $show = false;
                        }
                    }

                    if ($show) {
                        $price = $ropt['price'];
                        if ($j == 0) {
                            $price_prev = $price;//$r['price'];
                            $weight_prev = $ropt['weight'];
                            $weight_prefix_prev = $ropt['weight_prefix'];
                        }


                        $ropt['price'] = str_replace(',', '.', $ropt['price']);
                        $ropt['price'] = str_replace(' ', '', $ropt['price']);
                        $price = str_replace(',', '.', $price);
                        $price = str_replace(' ', '', $price);


                        $ropt['price'] = $miniShop2->formatPrice($ropt['price']);
                        $price = $miniShop2->formatPrice($price);
                        if ($firstPrice==0) {
                            $firstPrice = $ropt['price'];
                            $option_id=$ropt['id_1c'];
                            $firstSize=$ropt['weight'].' '.$ropt['weight_prefix'];
                        }

                        $outputattr[] = array(
                            'id' => $ropt['MIGX_id'],
                            'idproduct' => $row['id'],//$idproduct,
                            'idx' => $i,
                            'availability_1' => (isset($ropt['availability_1']) ? $ropt['availability_1'] : '0'),
                            'availability_2' => (isset($ropt['availability_2']) ? $ropt['availability_2'] : '0'),
                            'availability_3' => (isset($ropt['availability_3']) ? $ropt['availability_3'] : '0'),
                            'brand' => $brand,
                            'category_ec' => $category_name,
                            'price_prev' => $price_prev,
                            'productname' => $row['pagetitle'],
                            'articleproduct' => $row['article'],
                            'weight_prev' => $weight_prev,
                            'weight_prefix_prev' => $weight_prefix_prev,
                            'instock' => $ropt['instock'],
                            'weight' => $ropt['weight'],
                            'id_1c' => $ropt['id_1c'],
                            'weight_prefix' => $ropt['weight_prefix'],
                            'price_supplier' => $ropt['price_supplier'],
                            'currency_supplier' => $ropt['currency_supplier'],
                            'markup' => $ropt['markup'],
                            'action' => $ropt['action'],
                            'action_number' => $ropt['action_number'],
                            'action_hide' => $ropt['action_hide'],
                            'price' => $price,
                            'oldprice' => $ropt['oldprice'],
                        );
                        $outputtabs[] = array(
                            'idproduct' => $row['id'],
                            'idx' => $i,
                            'availability_1' => (isset($ropt['availability_1']) ? $ropt['availability_1'] : '0'),
                            'availability_2' => (isset($ropt['availability_2']) ? $ropt['availability_2'] : '0'),
                            'availability_3' => (isset($ropt['availability_3']) ? $ropt['availability_3'] : '0'),
                            'brand' => $brand,
                            'category_ec' => $category_name,
                            'productname' => $row['pagetitle'],
                            'articleproduct' => $row['article'],
                            'price_prev' => $price_prev,
                            'weight_prev' => $weight_prev,
                            'weight_prefix_prev' => $weight_prefix_prev,
                            'instock' => $ropt['instock'],
                            'id_1c' => $ropt['id_1c'],
                            'weight' => $ropt['weight'],
                            'weight_prefix' => $ropt['weight_prefix'],
                            'price_supplier' => $ropt['price_supplier'],
                            'currency_supplier' => $ropt['currency_supplier'],
                            'markup' => $ropt['markup'],
                            'action' => $ropt['action'],
                            'action_number' => $ropt['action_number'],
                            'action_hide' => $ropt['action_hide'],
                            'price' => $price,
                            'oldprice' => $ropt['oldprice'],
                        );
                        if ($ropt['action_hide']=='1')
                            $action_hide=1;

                        $j++;
                        $i++;
                        if ($j == $limitoption) break;
                    }
                }

                //end action

                $row['action'] =$action;

                $row['type'] = 'action';
                $row['action_hide'] = $action_hide;

//
//                $row['brand']=$brand;
//                $row['category']=$category_name;
//                $row['name']=$name;

                $row['firstPrice']=$firstPrice;
                $row['option_id']=$option_id;

                $row['firstSize'] = $firstSize;
                //$row['key']=$cartkey;


                $row['options'] = array('outputattr' => $outputattr, 'outputtabs' => $outputtabs);
                $resultrat = "";
                $countcom = "";

                $sql = "SELECT C.* FROM modx_tickets_comments C LEFT JOIN modx_tickets_threads D ON C.thread=D.id WHERE C.published=1 AND C.deleted=0 AND D.resource =  '" . $row['id'] . "'";

                $icon = '';
                $q1 = $this->modx->prepare($sql);
                $q1->execute(array(0));
                $arr = $q1->fetchAll(PDO::FETCH_ASSOC);

                if (count($arr)) {
                    $all = array();
                    foreach ($arr as $arr_one) {
                        if ($arr_one['rating'] != 0) $all[] = $arr_one['rating'];
                    }
                    $middle = number_format(round(array_sum($all) / count($all)), 1, '.', '');
                    if (count($all) == 0) {
                        $resultrat = "";
                        $countcom = "";
                    } else {

                        $resultrat = '';
                        $middle1 = floor($middle);
                        if ($middle != 0) {
                            $rat = '';
                            for ($d = 1; $d <= $middle; $d++) {
                                $rat .= '<span class="icon star-' . $icon . 'icon star-' . $icon . 'icon-check"></span>';
                            }
                            $noact = 5 - $middle1;
                            for ($d = 1; $d <= $noact; $d++) {
                                $rat .= '<span class="icon star-' . $icon . 'icon"></span>';
                            }
                            $resultrat = $rat;
                        }
                        $countcom = count($all);//"("..")";
                    }
                }


                $row['rating'] = $resultrat;
                $row['countcom'] = $countcom;
                if ($row['image'] == '') $row['image'] = '/assets/images/no-image.png';
                $row['isaction'] = $row['adiscount'];

                $row['action'] = $action;


                $row['valueaction'] = $valuactionproduct;
                $min_price='';
                foreach ($dataopt as $it)
                {
                    if (($it['price']!='')&&($it['price']!='0')) {
                        $min_price = $it['price'];
                        break;
                    }
                }

                $row['min_price']=$min_price;
                $row['list']=$this->getProperty('list');

                if ($min_price!='') {
                    $min_price=str_replace(',','.',$min_price);
                    $ecomerc .= "
                        {
                          'name': '" . $name . "',
                          'id': '" . $row['id'] . "',
                          'price': " . $min_price . ",
                          'brand': '" . $brand . "',
                          'category': '" . $category_name . "',
                          'variant': '',
                          'list': '" . $this->getProperty('list') . "',
                          'position': " . $num . "
                        },
                    ";
                }

                $items[] = $row;
                $num++;
            }

        }
//        echo '<pre>$items';
//        print_r($items);
//        echo '</pre>';
//        die();

        return ['list'=>$items,'ecomerc'=>$ecomerc];
    }

    protected function prepareMenu(array & $items, $currentlevel = 1)
    {
        $levelClass = $this->getProperty('levelClass');
        $activeClass = $this->getProperty('activeClass');

        foreach ($items as &$item) {

            $cls = array();

            if ($levelClass) {
                $cls[] = "{$levelClass}{$currentlevel}";
            }

            $item['linktext'] = ($item['menutitle'] ? $item['menutitle'] : $item['pagetitle']);

            if (in_array($item['id'], $this->activeIDs)) {
                if ($activeClass) {
                    $cls[] = $activeClass;
                }
            }

            $item['cls'] = implode(" ", $cls);

            if ($item['childs']) {
                $item['childs'] = $this->prepareMenu($item['childs'], $currentlevel + 1);
            }
        }


        return $items;
    }

    protected function getDefaultConditions()
    {
        $where = array(
            'msProduct.deleted' => 0,
        );

        if (!$this->getProperty('showUnpublished')) {
            $where['msProduct.published'] = true;
        }


        if (!$this->getProperty('ignoreHidden')) {
            $where['msProduct.hidemenu'] = false;
        }

        if ($_where = $this->getProperty('where')) {
            $where = array_merge($where, $_where);
        }
        return $where;
    }
}

return 'modSiteWebProductsGetProductsProcessor';
