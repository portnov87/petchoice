<?php
class modSiteWebMenuGetCatalogMenuProcessor extends modProcessor{
    
    protected $activeIDs = array();     // ID of active parents

    protected $language=false;
    public function initialize(){
        
        $this->setDefaultProperties(array(
            'id'                => 'menu',      // Menu id
            'cacheable'         => true,//false,
            'startId'           => $this->modx->resource->id,   
            'level'             => 1,
            'sortBy'            => 'menuindex',
            'sortOrder'         => 'ASC',
            'levelClass'        => '',
            'activeClass'       => 'active',
            'ignoreHidden'        => false,
            'showUnpublished'   => false,
        ));
        
        return parent::initialize();
    }
    
    public function process() {
		
        $output = '';

        $polylang = $this->modx->getService('polylang', 'Polylang');
        $polyLangTools = $polylang->getTools();
        $language = false;
        $this->language = $polyLangTools->detectLanguage();



        // get active parents
        if(!empty($this->modx->resource) AND $this->modx->resource instanceOf modResource){
            $resource = $this->modx->resource;
            $this->activeIDs[] = $resource->id;
            
            while($resource = $resource->getOne('Parent')){
                $this->activeIDs[] = $resource->id;
            }
        }
        
        // get menu items
        if(!$items = $this->getMenuItems()){
            return;
        }
        
        // prepare menu items
        $items = $this->prepareMenu($items);
        
        return array(
            'success'   => true,
            'message'   => '',
            'object'     => $items,
        );
    }
    
    public function getMenuItems(){
        $items = array();
        
        $startId = $this->getProperty('startId');
        $level = $this->getProperty('level');
        $cacheable = $this->getProperty('cacheable');
        $id = $this->getProperty('id', 'menu');
        $cacheKey = $this->modx->context->key."/{$id}/{$startId}";
        
        if($cacheable){
            if($fromCache = $this->modx->cacheManager->get($cacheKey)){
                return $fromCache;
            }
        }
            
        //else
        if($items = $this->getItems($startId, $level)){
            if($cacheable){
                $this->modx->cacheManager->set($cacheKey, $items);
            }
        }
        
        return $items;
    }

    protected function getItems($parent, $level){
        $level--;
        $items = array();
        $q = $this->modx->newQuery('modResource');
        
        $where = $this->getDefaultConditions();
        
        $where['parent'] = $parent;
        
        $q->where($where);
        if ($this->language)
        {
            if ($this->language->get('culture_key')=='ua') {
                $q->leftJoin('PolylangContent', 'polylang_content', "modResource.id=polylang_content.content_id and polylang_content.culture_key='".$this->language->get('culture_key')."'");
                $q->select(array(
                    'modResource.id', 'modResource.parent',
                    'IF(polylang_content.pagetitle!="",polylang_content.pagetitle,modResource.pagetitle) as pagetitleLang',
                    'IF(polylang_content.longtitle!="",polylang_content.longtitle,modResource.longtitle) as longtitleLang',
                    'IF(polylang_content.description!="",polylang_content.description,modResource.description) as descriptionLang',
                    //'modResource.description',

                    'modResource.menutitle',
                    'modResource.link_attributes', 'modResource.uri', 'modResource.alias'

                ));
            }else{
                $q->select(array(
                    'modResource.id', 'modResource.parent',
                    'modResource.pagetitle as pagetitleLang',
                    'modResource.longtitle as longtitleLang',
                    'modResource.description as descriptionLang',
                    //'modResource.description',

                    'modResource.menutitle',
                    'modResource.link_attributes', 'modResource.uri', 'modResource.alias'

                ));
            }
        }else {

            $q->select(array(
                'modResource.id', 'modResource.parent',
                'modResource.pagetitle as pagetitleLang',
                'modResource.longtitle as longtitleLang',
                'modResource.description as descriptionLang',
                //'modResource.description',

                'modResource.menutitle',
                'modResource.link_attributes', 'modResource.uri', 'modResource.alias'

            ));
        }
        $q->sortby($this->getProperty('sortBy'), $this->getProperty('sortOrder'));
        if($q->prepare() && $q->stmt->execute()){
//            echo $q->toSQL();
//            die();
            while($row = $q->stmt->fetch(PDO::FETCH_ASSOC)){
                if($level>0){
                    $row['childs'] = $this->getItems($row['id'], $level);
                }
                else{
                    $row['childs'] = array();
                }
                $items[$row['id']] = $row;
            }
        }
//        echo '<pre>';
//        print_r($items);
//        echo '</pre>';
//        die();
		
        return $items;
    }
    
    protected function prepareMenu(array & $items, $currentlevel=1){
        $levelClass = $this->getProperty('levelClass');
        $activeClass = $this->getProperty('activeClass');
        
        foreach($items as &$item){
            
            $cls = array();
            
            if($levelClass){
                $cls[] = "{$levelClass}{$currentlevel}";
            }
            
            $item['linktext'] = ($item['menutitle'] ? $item['menutitle'] : $item['pagetitleLang']);
            
            if(in_array($item['id'], $this->activeIDs)){
                if($activeClass){
                    $cls[] = $activeClass;   
                }
            }
            
            $item['cls'] = implode(" ", $cls);
            
            if($item['childs']){
                $item['childs'] = $this->prepareMenu($item['childs'], $currentlevel+1);
            }
        }
        
		
        return $items;
    }
    
    protected function getDefaultConditions(){
        $where = array(
            'deleted'   => 0,
        );
        
        if(!$this->getProperty('showUnpublished')){
            $where['published'] = true;
        }
        
        if(!$this->getProperty('ignoreHidden')){
            $where['hidemenu'] = false;
        }
        
        if($_where = $this->getProperty('where')){
            $where = array_merge($where, $_where);
        }
        return $where;
    }
}
return 'modSiteWebMenuGetCatalogMenuProcessor';
