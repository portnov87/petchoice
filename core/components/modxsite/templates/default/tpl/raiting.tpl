{extends file="layout.tpl"}

{block name=content}
    <div class="container">
    <!--  <script>
        {literal}
        dataLayer.push({
            'ecommerce': {
                'currencyCode': 'UAH',

            },'event': 'gtm-ee-event',
            'gtm-ee-event-category': 'Enhanced Ecommerce',
            'gtm-ee-event-action': 'Product Impressions',
            'gtm-ee-event-non-interaction': 'True',
        });
        //'impressions': []
        {/literal}
    </script>-->
    {literal}
        <script>
            jQuery(document).ready(function () {
                jQuery(document).on('click', '.open_modal_vote',function () {
                    
                    href=jQuery(this).data('href');
                    product_id=jQuery(this).data('product_id');
                    
                    jQuery('#submitRaitingModal').modal('show');
                    jQuery('#submitRaitingModal').find('.fb-like').data('href',href);
jQuery('#submitRaitingModal').find('.shared_facebook_modal').data('href',href);
jQuery('#submitRaitingModal').find('.shared_facebook_modal').data('product_id',product_id);
                    return false;
                });
                
                
                
                jQuery(document).on('click','.shared_facebook_modal',function(){
                
                
                _href=jQuery(this).data('href');
                
                product_id=jQuery(this).data('product_id');
                FB.ui({
                    method: 'share',
                    href: _href//'https://petchoice.ua/{/literal}{link id={field name=id}}{literal}',
                }, function(response){
                    $.ajax({
                        type: "POST",
                        url: '/assets/components/minishop2/raiting_product.php',
                        data: 'product_id='+product_id+'&type=shared',
                    }).done(function (data) {

                        //data = $.parseJSON(data);
                        //console.log(data);
                    });
                });
                return false;
            });
            });
        </script>
    {/literal}

    {snippet name=BreadCrumb params="scheme=`full`&containerTpl=`breadcrumbOuterTpl`&currentCrumbTpl=`breadcrumbCurrentTpl`&linkCrumbTpl=`breadcrumbLinkTpl`&homeCrumbTpl=`breadcrumbLinkTpl`"}


    <div id="rating-tabs" class="tabs main-tab rating-tabs" role="tabpanel">
    
     <div id="help-raiting-popover" class="hidden">
        <div class="popover-body">        
            <p><b>Проголосуйте за свой выбор</b></p>
            <ul class="list-unstyled green-bullet">
                <li>
                    Поделиться в Facebook +2 балла к рейтингу
                </li>
                <li>
                    Отзыв с оценкой (списком):
                    <ul>
                        <li>1 звезда: -2 балла</li>
                        <li>2 звезды: -1 балл</li>
                        <li>3 звезды: 0 баллов</li>
                        <li>4 звезды: +1 балл</li>
                        <li>5 звезд: +2 балла</li>
                    </ul>
                </li>
            </ul>
        </div>
    </div>
    
    
        <div class="right-tab-help anim-hover">
            <a  data-toggle="popover" data-popover-content="#help-raiting-popover" data-placement="left" style="border-bottom: 1px dashed #476dc7;color:#476dc7; padding:0px;text-decoration:none;" href="javascript:void(0)">Как это работает?</a>
        </div>
        <ul class="nav nav-tabs" role="tablist">
            <li class="{$active_all}">
                <a href="/rejting-kormov">
                    Все товары
                </a>
            </li>
            <li class="{$active_cats}">
                <a href="/rejting-kormov/dlya-koshek">
                    Для кошек
                </a>
            </li>
            <li class="{$active_dogs}">
                <a href="/rejting-kormov/dlya-sobak">
                    Для собак
                </a>
            </li>
        </ul>
        <br><br>
        <div class="tab-content">

            <!--ms|price_kg,ms|vendor:vendors,ms|adiscount,ms|askidka'-->

            {assign var=params value=[
            "element"=>'msProducts'
            ,"where"=>[
            "class_key"=>"msProduct"
            ]
            ,"filters"=>''
            ,"includeTVs"=>'0'
            ,"includeMS"=>'1'
            ,"includeMSList"=>'price,new,toorder,favorite,popular,size,color'
            ,"paginator"=>'getPage'
            ,"tplOuter"=>'filterTpl'
            ,"tplFilter.outer.default"=>'filterGroupTpl'
            ,"tplFilter.row.default"=>'filterRowTpl'
            ,"tplFilter.outer.ms|price"=>'filterPriceOuterTpl'
            ,"tplFilter.outer.ms|price_kg"=>'filterPriceOuterTpl'
            ,"tplFilter.row.ms|price"=>'filterPriceTpl'
            ,"tplFilter.row.ms|price_kg"=>'filterPriceTpl'
            ,"tplFilter.outer.tv|product_options"=>'filterPriceOuterTpl'
            ,"tplFilter.row.tv|product_options"=>'filterPriceTpl'
            ,"tplFilter.outer.ms|vendor"=>'filterVendorOuterTpl'
            ,"tpls"=>'productRowTpl'
            ,"limit"=>"24"
            ,"raiting"=>'1'
            ]}

            {snippet name=mFilter2 params=$params}



            {assign var=pars value=[
            "element"=>'msProducts'
            ,"where"=>[
            "class_key"=>"msProduct"
            ]
            ,"filters"=>''
            ,"includeTVs"=>'0'
            ,"includeMS"=>'1'
            ,"includeMSList"=>'price,new,toorder,favorite,popular,size,color'
            ,"paginator"=>'getPage'
            ,"tplOuter"=>'filterTpl'
            ,"tplFilter.outer.default"=>'filterGroupTpl'
            ,"tplFilter.row.default"=>'filterRowTpl'
            ,"tplFilter.outer.ms|price"=>'filterPriceOuterTpl'
            ,"tplFilter.outer.ms|price_kg"=>'filterPriceOuterTpl'
            ,"tplFilter.row.ms|price"=>'filterPriceTpl'
            ,"tplFilter.row.ms|price_kg"=>'filterPriceTpl'
            ,"tplFilter.outer.tv|product_options"=>'filterPriceOuterTpl'
            ,"tplFilter.row.tv|product_options"=>'filterPriceTpl'
            ,"tplFilter.outer.ms|vendor"=>'filterVendorOuterTpl'
            ,"tpls"=>'productRowTpl'
            ,"limit"=>"24"
            ,"raiting"=>'1'
            ]}


            {snippet name=metaprevnext  params=$pars}

        </div>
    </div>


    <!--
    <div class="row title-page equal-height-wrapper" style="min-width:100%!important;">
      <div class="col-xs-7 equal-height">
        <h1>{snippet name=metah1 params="h1={field name=pagetitle}"}</h1>  
      </div>
      <div id="mse2_sort" class="col-xs-5 equal-height text-right filters">
        <span>Отображать по:</span>
        <a href="#" class="sort filter-link {if (($mse2_sort=='hits')&&($mse2_sortdir=='asc'))}up-sort active{/if} {if (($mse2_sort=='hits')&&($mse2_sortdir=='desc'))}down-sort active{/if}" data-sort="hits" data-dir="{if (($mse2_sort=='hits')&&($mse2_sortdir=='asc'))}desc{/if} {if (($mse2_sort=='hits')&&($mse2_sortdir=='desc'))}asc{/if}" data-default="asc">популярности</a>
    	<a href="#" class="sort filter-link {if (($mse2_sort=='pagetitle')&&($mse2_sortdir=='asc'))}up-sort active{/if} {if (($mse2_sort=='pagetitle')&&($mse2_sortdir=='desc'))}down-sort active{/if}" data-sort="pagetitle" data-dir="{if (($mse2_sort=='pagetitle')&&($mse2_sortdir=='asc'))}desc{/if} {if (($mse2_sort=='pagetitle')&&($mse2_sortdir=='desc'))}asc{/if}" data-default="desc">алфавиту</a>
        
      </div>
    </div>
рейтинг
{literal}
{/literal}-->


    </div>{literal}


{/literal}

{literal}
    <style>
        #submitRaitingModal ul {
            list-style: none;
            padding-left: 0px;
            margin-left: 0px;
        }

        #submitRaitingModal ul li {
            padding-left: 0px;
            margin-left: 0px;
            margin-bottom: 20px;
        }
    </style>
    <div class="modal fade" id="submitRaitingModal" tabindex="-1" role="dialog" aria-labelledby="basketModalLabel"
    aria-hidden="true">
    <div class="modal-dialog">
    <div class="modal-content">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"></button>
        <h3 style="margin-right:30px;font-size:28px;font-family: "Roboto Condensed", sans-serif;font-weight: 400;line-height: 1.1;color: inherit;color:black;"  class="modal-title greener"><b>Поддержите свой выбор </b></h3>
    </div>
    <div class="modal-body">
    <!--<b>
        Вы можете
    </b>-->
    <ul>
   <li>
    <span style="color: #8fae3b;">Рассказать друзьям <span
                class="grey">(2 балла)</span>
                            </span>
                            <!--Нажать Нравится <span class="grey">(1 балл)</span> или -->
    <div style="margin-top:15px;">

    <!--<div class="fb-like" data-href=""
    data-layout="button_count" data-action="like" data-size="large"
    data-show-faces="false" data-share="false"></div>-->
    <button data-href="" data-product_id="" style="display:inline-block;background-color: #4267b2;padding: 4px 15px;border: 1px solid #4267b2;"
            class="shared_facebook_modal btn grey-btn pull-right11">Поделиться
    </button>
    <!--
    <div class="fb-like" data-href="" data-layout="button_count" data-action="like"
                                     data-size="large" data-show-faces="true" data-share="true"></div>-->
    </div>
    </li>
    
     <li>
    <span style="color: #8fae3b;">Поставить Like <span
                class="grey">(1 балл)</span>
                            </span>
                            <!--Нажать Нравится <span class="grey">(1 балл)</span> или -->
    <div style="margin-top:15px;">

    <div class="fb-like" data-href=""
    data-layout="button_count" data-action="like" data-size="large"
    data-show-faces="false" data-share="false"></div>
    
    <!--
    <div class="fb-like" data-href="" data-layout="button_count" data-action="like"
                                     data-size="large" data-show-faces="true" data-share="true"></div>-->
    </div>
    </li>
    <li>
    <span  style="color: #8fae3b;">Или оставьте отзыв о товаре</span>
    <div class="popup_forma_review" style="margin-top:15px;">
{/literal}
    {if (!$modx->user->isAuthenticated('web'))}
        <br/>
        <p>Для того, чтобы добавить отзыв, нужно</p>
        <p>
            <button class="btn green-btn" data-toggle="modal" data-target="#boxregistr" id="registrcomment">
                ЗАРЕГИСТИРОВАТЬСЯ
            </button>
        </p>
        <p>
            или
        </p>
        <button class="btn grey-btn" data-toggle="modal" data-target="#boxlogin">АВТОРИЗОВАТЬСЯ</button>
        <br>
        <br>
    {else}
        <form id="comment-form" action="" method="post">
            <!--<p class="h3 green"><b>Написать отзыв о товаре</b></p>-->
            <input type="hidden" name="thread" value="resource-"/>
            <input type="hidden" name="parent" value="0"/>
            <input type="hidden" name="id" value="0"/>
            <div class="br-widget">
                <select name="rating" id="rating1" class="stars-rating">
                    <option value="1">1</option>
                    <option value="2">2</option>
                    <option value="3">3</option>
                    <option value="4">4</option>
                    <option value="5" selected>5</option>
                </select>

            </div>
            <br>
            <textarea name="text" id="comment-editor" class="custom-inpt" rows="6"></textarea>
            <br><br>

            <button type="submit" class="btn grey-btn text-uppercase pull-right submit">Отправить</button>
        </form>
    {/if}
{literal}
    </div>
    </li>

    </ul>


    </div>
    <div class="modal-footer">

        <button type="button" class="btn link-btn light" data-dismiss="modal">Перейти на сайт</button>
    </div>
    </div>
    </div>
    </div>
{/literal}

{/block}