
{snippet name=msCartKey params="id=`{$attr.idproduct}`&option_id=`{$attr.id_1c}`&weight=``&size=`{$attr.weight} {$attr.weight_prefix}`&price=`{$attr.price}`" assign=key}
{snippet name=inCartCount params="key=`{$key}`&price=`{$attr.price}`" assign=incart}

<div role="tabpanel" class="product-option tab-pane {if ($attr.idx==1)}active{/if}" id="{$product.type}{$attr.idproduct}-pane{$attr.idx}" data-product-key="{$key}">


	{if ($attr.instock==0)}
	<div class="product-item-tab-content clearfix text-center">
			<span class="grey">[[%polylang_site_product_button_noproduct]]</span>
			<br>
			<a href="#" class="askpr1" id="status{$key}" data-article="{$attr.articleproduct}"  data-product="{$attr.productname}, {$attr.weight} {$attr.weight_prefix}" data-id="{$key}" data-toggle="modal" data-target="#boxStatus">[[%polylang_site_product_button_askqty]]</a>

			 
    </div>
	{/if}
	{if ($attr.instock==1)}
		<div class="product-item-tab-content clearfix card-button">
				<button type="button" class="btn pay-product-btn orange-btn pull-right text-uppercase add-cart {if ($incart>0)}hidden{/if}"
						data-product-type="catalog" data-product-vendor="{$attr.brand}"
                                        data-product-category="{$attr.category_ec}"
                                        data-product-price="{$attr.price}"
                                        data-option-id="{$attr.id_1c}"
					  					data-product-name="{$attr.productname}"
                                        data-action="cart/add" data-count="1" data-size="{$attr.weight} {$attr.weight_prefix}" data-product-id="{$attr.idproduct}">[[%polylang_site_product_button_buy]]</button>
				<div class="count-box pull-right {if ($incart==0)} hidden {/if}">
				  <a href="#" class="icon minus-count"></a>
				  <input data-product-type="catalog" data-product-vendor="{$attr.brand}"
                                                  data-product-category="{$attr.category_ec}"
                                                  data-option-id="{$attr.id_1c}"
                                                  data-product-price="{$attr.price}"
                                                  data-product-size="{$attr.weight} {$attr.weight_prefix}"
					  data-product-name="{$attr.productname}" class="form-control custom-inpt count-products" type="text" value="{$incart}" />
				  <a href="#" class="icon plus-count"></a>
				</div>
				{if ($attr.action==1)}
					<span class="product-price pull-left">
					{snippet name=msPriceDelimiter params="delimiter=`,`&price=`{$attr.price}`&action=`left`"}
					
					<span class="small text-top">
					{snippet name=msPriceDelimiter params="delimiter=`,`&price=`{$attr.price}`&action=`right`"}
					
					</span></span>
					
				{/if}
				{if ($attr.action==0)}
					<span class="product-price pull-left">			
					{snippet name=msPriceDelimiter params="delimiter=`,`&price=`{$attr.price}`&action=`left`"}			
					<span class="small text-top">
					{snippet name=msPriceDelimiter params="delimiter=`,`&price=`{$attr.price}`&action=`right`"}			
					</span></span>
				{/if}
		</div>
	{/if}
</div>




