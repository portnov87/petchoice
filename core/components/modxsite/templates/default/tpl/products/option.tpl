
{snippet name=msCartKey params="id=`{$attr.idproduct}`&option_id=`{$attr.id_1c}`&weight=``&size=`{$attr.weight} {$attr.weight_prefix}`&price=`{$attr.price}`" assign=key}


<div itemprop="offers" itemscope itemtype="http://schema.org/Offer" class="card-costs-item product-option" data-product-key="{$key}">

 <meta itemprop="priceCurrency" content="UAH" />
    <div class="row equal-height-wrapper" style="position:relative;"><!--padding:0px;float:left!important;width:110px;margin-left:0px!important;margin-right:0px;padding-left:0px!important;-->
      <div class="{if ($attr.dostavka>0)}col-xs-3{else}col-xs-6{/if} equal-height" style="" >
        <span class="product-card-price weight">{$attr.weight} <small>{$attr.weight_prefix}</small></span>
        
	
        
        {if ($attr.markdown_text!='')}
            
            <br/>
    			<span class="grey small">{$attr.markdown_text}</span>            
        {else}
    		{if ($attr.num>1)}		
    		    {snippet name=price_economy params="weight=`{$attr.weight}`&weight_prefix=`{$attr.weight_prefix}`&prevweightprefix=`{$attr.weight_prefix_prev}`&prevweight=`{$attr.weight_prev}`&old=`{$attr.price_prev}`&new=`{$attr.price}`"}            
    		{/if}
        {/if}
		  <br/>
          {if (($modx->user->isAuthenticated())&&(in_array(7, $modx->user->getUserGroups())))}
              {if (($attr.availability_1!='0')||($attr.availability_2!='0')||($attr.availability_3!='0'))}
				  <span class="grey small">наличие: {$attr.availability_1}/{$attr.availability_2}/{$attr.availability_3}</span>
              {/if}
		  {/if}
        
      </div>
     {if ($attr.dostavka>0)}
	   {if ($attr.markdown)}  
         <div class="col-xs-1 equal-height" style="">
       
         <span style="" class="icon markdown-icon"></span>
         
         </div>
         {/if}    
         
         {if ($attr.by_weight)}
         <div class="col-xs-1 equal-height" style="">
         <span style="" class="icon by_weight-icon"></span>
         
         </div>
         {/if}    
         
         <div class="col-xs-3 equal-height" style=""><span style="" class="icon free-icon"></span></div>
         
	 {else}
        
         {if ($attr.markdown)}
         <div class="col-xs-1 equal-height" style="">
         <span style="" class="icon markdown-icon"></span>
         
         </div>
         {/if}    
         
         {if ($attr.by_weight)}
         <div class="col-xs-3 equal-height" style="">
         
         <span style="" class="icon by_weight-icon"></span>
         
         </div>    
         {/if}    
         
     {/if}
     
     
     
     
     
    
    {if ($attr.instock==0)}
		<div class="col-xs-6 equal-height text-right" >
     
			<div class="card-price inline casper_price" style="position:relative">
            
            {if (($attr.action==1)&&($attr.action_number>0))}
     <div class=" equal-height" style="text-align:right;position:absolute;top: 30px;right: 80px;height:auto!important;min-height:auto!important;"><span style="margin-left:5px;padding:5px 5px; background-color: #a0489c;color:#fff;text-align:right;">-{$attr.action_number}%</span></div>    
     {/if}



                {if ($attr.action==1)}

					<span class="product-card-price-old">
				 {snippet name=msPriceDelimiter params="delimiter=`,`&price=`{$attr.oldprice}`&action=`left`"}
						<span class="text-top">{snippet name=msPriceDelimiter params="delimiter=`,`&price=`{$attr.oldprice}`&action=`right`"}</span></span>
					<span class="product-card-price">
					{snippet name=msPriceDelimiter params="delimiter=`,`&price=`{$attr.price}`&action=`left`"}
						<span class="text-top">
					{snippet name=msPriceDelimiter params="delimiter=`,`&price=`{$attr.price}`&action=`right`"}
					</span></span>
                {else}
					<span class="product-card-price">
				 {snippet name=msPriceDelimiter params="delimiter=`,`&price=`{$attr.price}`&action=`left`"}
						<span class="text-top">
				 {snippet name=msPriceDelimiter params="delimiter=`,`&price=`{$attr.price}`&action=`right`"}
				 </span></span>
                {/if}



                {if (($modx->resource->parent==15)||($modx->resource->parent==19)||($modx->resource->parent==59)||($modx->resource->parent==57))}
                    {if ($attr.price_kg!='')}<br><span class="grey small" style="float:right;">{$attr.price_kg}  грн / кг</span>{/if}
                {/if}

				<meta itemprop="price" content="{$attr.price}">
			</div>



			<a href="#" class="askpr" id="status{$attr.id}" data-article="{$attr.articleproduct}" data-product="{$attr.nameproduct}, {$attr.weight} {$attr.weight_prefix}" data-id="{$attr.id}" data-toggle="modal" data-target="#boxStatus">Сообщить о наличии?</a>
                  </div>
                  <link itemprop="availability" href="http://schema.org/OutStock"/>
	{/if}
        
		 {if ($attr.instock==1)}
		 
			  <link itemprop="availability" href="http://schema.org/InStock"/>
			   <div class="col-xs-6 equal-height text-right">
               

				 <div class="card-price inline"  style="position:relative;">
				 
            {if (($attr.action==1)&&($attr.action_number>0))}
     <div class=" equal-height" style="text-align:right;position:absolute;top: 30px;right: 80px;height:auto!important;min-height:auto!important;"><span style="margin-left:5px;padding:5px 5px; background-color: #a0489c;color:#fff;text-align:right;">-{$attr.action_number}%</span></div>    
     {/if}


				 {if ($attr.action==1)}
				 
				 <span class="product-card-price-old">
				 {snippet name=msPriceDelimiter params="delimiter=`,`&price=`{$attr.oldprice}`&action=`left`"}
				 <span class="text-top">{snippet name=msPriceDelimiter params="delimiter=`,`&price=`{$attr.oldprice}`&action=`right`"}</span></span>
				 	<span class="product-card-price">
					{snippet name=msPriceDelimiter params="delimiter=`,`&price=`{$attr.price}`&action=`left`"}					
					<span class="text-top">
					{snippet name=msPriceDelimiter params="delimiter=`,`&price=`{$attr.price}`&action=`right`"}
					</span></span>
				 {else}
				 <span class="product-card-price">
				 {snippet name=msPriceDelimiter params="delimiter=`,`&price=`{$attr.price}`&action=`left`"}
				 <span class="text-top">
				 {snippet name=msPriceDelimiter params="delimiter=`,`&price=`{$attr.price}`&action=`right`"}
				 </span></span>
				 {/if}
				 
				 
				 
					{if (($modx->resource->parent==15)||($modx->resource->parent==19)||($modx->resource->parent==59)||($modx->resource->parent==57))}
					{if ($attr.price_kg!='')}<br><span class="grey small" style="float:right;">{$attr.price_kg}  грн / кг</span>{/if}
					{/if}
					
					 <meta itemprop="price" content="{$attr.price}">
					</div>
				  <div class="card-button inline">
					  <button type="button"
							  data-product-type="page"
                                                  data-product-vendor="{$attr.product_vendor}"
                                                  data-product-category="{$attr.category_ec}"
                                                  data-product-price="{$attr.price}"
                                                  data-option-id="{$attr.id_1c}"
					  data-product-name="{$attr.nameproduct}" data-product-id="{$attr.idproduct}"
							  data-size="{$attr.weight} {$attr.weight_prefix}" data-count="1" data-action="cart/add"
					  class="btn pay-btn orange-btn pull-right text-uppercase product add-cart">[[%polylang_site_product_button_buy]]</button>
					  
					  <div class="count-box pull-right hidden">
						<span class="small grey">кол-во</span>
						<a href="#" class="icon minus-count"></a>
						
						{snippet name=msCartKey params="id=`{$attr.idproduct}`&option_id=`{$attr.id_1c}`&weight=``&size=`{$attr.weight} {$attr.weight_prefix}`&price=`{$attr.price}`" assign=msCartKey}
						{snippet name=inCartCount params="key=`{$msCartKey}`" assign=inCartCount}
						
						
						<input name="sub-product-count{$attr.idx}" data-product-type="page"
                                                       data-product-vendor="{$attr.product_vendor}"
                                                  data-product-category="{$attr.category_ec}"
                                                  data-product-price="{$attr.price}"
                                                  data-option-id="{$attr.id_1c}"
					  data-product-name="{$attr.nameproduct}"
                                                       data-product-size="{$attr.weight} {$attr.weight_prefix}"
                                                       class="form-control custom-inpt count-products" type="text" min="1" value="{$inCartCount}" />
						<a href="#" class="icon plus-count"></a>
					   
					  </div>
					</div>
					
				</div>
		 {/if}
          
        
          
      
    </div>
</div>