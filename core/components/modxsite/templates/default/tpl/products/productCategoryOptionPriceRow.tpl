{snippet name=msCartKey params="id=`{$attr.idproduct}`&option_id=`{$attr.id_1c}`&weight=``&size=`{$attr.weight} {$attr.weight_prefix}`&price=`{$attr.price}`" assign=key}

{snippet name=inCartCount params="key=`{$key}`&price=`{$attr.price}`" assign=incart}


  <li class="product-option" id="{$product.type}{$attr.idproduct}-pane{$attr.idx}" data-product-key="{$key}">

                                    <span>{$attr.weight} {$attr.weight_prefix}</span>
                                    <div class="card-button">
                                        <!--<span class="grey">Товар закончился</span>
                                        <br>-->
                                        {if ($attr.instock==1)}
                                        <button type="button" data-product-type="catalog"
                                                class="btn pay-product-btn orange-btn text-uppercase add-cart  {if ($incart>0)}hidden{/if}"
                                                data-action="cart/add" data-count="1" data-size="{$attr.weight} {$attr.weight_prefix}" 
                                            data-product-vendor="{$attr.product_vendor}"
                                            data-option-id="{$attr.id_1c}"
                                                  data-product-category="{$attr.category_ec}"
                                                  data-product-price="{$attr.price}"
        			                        data-product-name="{$attr.productname}"
                                            data-product-id="{$attr.idproduct}">[[%polylang_site_product_button_buy]]
                                        </button>
                                        
                                        <div class="count-box pull-right {if ($incart==0)} hidden {/if}">
                                            <a href="#" class="icon minus-count"></a>
                                            <input data-product-type="catalog" data-action="cart/add" data-count="1" data-size="{$attr.weight} {$attr.weight_prefix}" 
                                                    data-product-vendor="{$attr.product_vendor}"
                                                  data-product-category="{$attr.category_ec}"
                                                  data-product-price="{$attr.price}"
                                                  data-option-id="{$attr.id_1c}"
    				                                data-product-name="{$attr.productname}"
                                                    data-product-id="{$attr.idproduct}"
                                                   class="form-control custom-inpt count-products" type="text"
                                                   value="{$incart}">
                                            <a href="#" class="icon plus-count"></a>
                                        </div>
                                        
                                        {/if}
                                        
                                        {if ($attr.instock==0)}
                                        <a href="#" class="askpr" id="status{$key}" data-article="{$attr.articleproduct}"  data-product="{$attr.productname}, {$attr.weight} {$attr.weight_prefix}" data-id="{$key}" data-toggle="modal" data-target="#boxStatus">Сообщить о наличии?</a>
                                        {/if}
                                        
                                        
                                      
                                    {if (($attr.action==1)&&($attr.action_hide!=1))}
                                                 
                                                                    
                                                                    <div class="product-price">
                                                                {snippet name=msPriceDelimiter params="delimiter=`,`&price=`{$attr.price}`&action=`left`"}
                    
                                                                <div class="small text-top">
                                                                    {snippet name=msPriceDelimiter params="delimiter=`,`&price=`{$attr.price}`&action=`right`"}
                                                                </div>
                                                            </div>
                                                            <div class="product-price-old">
                                                                {snippet name=msPriceDelimiter params="delimiter=`,`&price=`{$attr.oldprice}`&action=`left`"}
                                                                <div class="small text-top">
                                                                    {snippet name=msPriceDelimiter params="delimiter=`,`&price=`{$attr.oldprice}`&action=`right`"}            
                                                                </div>
                                                            </div>
                                                                 
                                                        
                    				{/if}
				{if (($attr.action_hide==1)||($attr.action==0))}
                                    {if ($attr.instock==0)}
                                    
                                        {if ($attr.action_hide==1)}
                                        <div class="card-price inline casper_price">
                                                <div class="product-card-price">
                                                    {snippet name=msPriceDelimiter params="delimiter=`,`&price=`{$attr.oldprice}`&action=`left`"}            
                                                    <div class="text-top">
                                                        {snippet name=msPriceDelimiter params="delimiter=`,`&price=`{$attr.oldprice}`&action=`right`"}            
                                                    </div>
                                                </div>
                                            </div>
                                        {else}
                                        <div class="card-price inline casper_price">
                                                <div class="product-card-price">
                                                    {snippet name=msPriceDelimiter params="delimiter=`,`&price=`{$attr.price}`&action=`left`"}            
                                                    <div class="text-top">
                                                        {snippet name=msPriceDelimiter params="delimiter=`,`&price=`{$attr.price}`&action=`right`"}            
                                                    </div>
                                                </div>
                                            </div>
                                        {/if}
                                         
                                    {else}
                                    
                                     {if ($attr.action_hide==1)}
                                      <div class="product-price">
                                            {snippet name=msPriceDelimiter params="delimiter=`,`&price=`{$attr.oldprice}`&action=`left`"}            
                                            <div class="small text-top">
                                                {snippet name=msPriceDelimiter params="delimiter=`,`&price=`{$attr.oldprice}`&action=`right`"}    		
                                            </div>
                                        </div>
                                     {else}
                                      <div class="product-price">
                                            {snippet name=msPriceDelimiter params="delimiter=`,`&price=`{$attr.price}`&action=`left`"}            
                                            <div class="small text-top">
                                                {snippet name=msPriceDelimiter params="delimiter=`,`&price=`{$attr.price}`&action=`right`"}    		
                                            </div>
                                        </div>
                                     {/if}
                                     
                                       
                                    {/if}
                 
				{/if}
                
                
                                          
                                    </div>
                                    {if (($modx->user->isAuthenticated())&&((in_array(10, $modx->user->getUserGroups()))||(in_array(7, $modx->user->getUserGroups()))))}
                                    <sub>                                    
                                        Склад: {$attr.availability_1} шт / Магазин: {$attr.availability_2} шт / Экватор: {$attr.availability_3} шт
                                    </sub>
                                    {else}
                                    <sub class="price_economy" style="margin-bottom: 5px;">
                                      {if ($attr.markdown_text!='')}
                                            	  <!--<span class="grey small">-->
                                                  {$attr.markdown_text}
                                                      <!--
                                                  </span> -->           
                                        {else}
                                    		    {snippet name=price_economy params="catalog=1&weight=`{$attr.weight}`&weight_prefix=`{$attr.weight_prefix}`&prevweightprefix=`{$attr.weight_prefix_prev}`&prevweight=`{$attr.weight_prev}`&old=`{$attr.price_prev}`&new=`{$attr.price}`"}
                                        {/if}
                                    
                            		
                            		
                                    </sub>
                                    {/if}
                                </li>
                                
             