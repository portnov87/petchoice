<div class="product-item big-item ms2_product" data-list="{$product.list}" data-name="{$product.name}" data-brand="{$product.brand}"
     data-category="{$product.category}" data-price="{$product.min_price}" data-position="{$product.position}" id="list_product_{$product.id}">
    <form class="ms2_form" method="post">
        <input type="hidden" name="product_id" value="{$product.id}" />
            <div class="product-item-img">
              <a href="{link id={$product.id}}" data-type="list_product" data-id="{$product.id}">
    		  
			  {snippet name=pthumb params="input=`{$product.image}`&options=`w=150&h=223&far=1`&useResizer=`1`" assign=preview}
			  <img src="{$preview}" alt="{$pagetitle}" /></a>
			  
			   {if ($product.info_action!='')}
			  <div class="rowaction_info">{$product.info_action}</div>
			  {/if}
			  
              <div class="product-item-lbls {if ($product.black_friday==1)} event-sales {/if}">
			  {if ($product.popular==1)}
			  <div class="product-info-lbl top-sales-lbl">[[%polylang_site_product_label_topprd]]</div>
			  {/if}
			  
			  {if ($product.new==1)}
			  <div class="product-photo-lbl new-product-lbl">[[%polylang_site_product_label_newpr]]</div>
			  {/if}
			 
			  
			  {if ($product.toorder==1)}
				<div class="product-photo-lbl toorder-lbl">[[%polylang_site_product_label_orderpr]]</div>
			{/if}
            
			  {if ($product.isaction==1)}
			  <div class="product-photo-lbl action-lbl"><b>[[%polylang_site_product_actionblock]]</b></div>
			  {/if}
			  {if ($product.helloween==1)}
              <div class="product-info-helloween"></div>
              {/if}
			  {if ($product.action!='')}
              
              
              {if ($product.action_hide!=1)}
			  <div class="product-info-lbl discount-lbl {if ($product.black_friday==1)} product-info-black_friday {/if}{if ($product.newyear==1)} product-info-newyear {/if}">[[%polylang_site_product_label_skidka]] <b>-{$product.action}%</b></div>
              {/if}
			  {/if}
              
              </div>

                {if (($product.bonuses!='')&&($product.bonuses>0))}
                <div class="cashback cb_dt  cb_dt_catalog">
                    <div class="cashback_balloon">
                        <span>{$product.bonuses}</span>
                    </div>
                    <div class="cashback_info">
                        <div>
                            Возвращаем <span>{$product.bonuses} грн</span> на бонусный счет при покупке <span>{$product.size_bonuses}</span><br>
                            Оплачивайте бонусами до {$product.maxpercentorder}% от стоимости заказа
                        </div>
                    </div>
                </div>
                {/if}


            </div>
            <div class="product-item-title">
                <a href="{link id={$product.id}}" data-type="list_product" data-id="{$product.id}">{$product.pagetitle}</a>
            </div>
            
			{if ($product.article!='')}
				<div class="product-item-code small"><i>Код товара: {$product.article}</i></div>
			{/if}
            <div class="product-item-text">              
			  {$product.introtext}
            </div>
			{if ($product.rating!='')}
            
				<div class="product-item-stars">
					
					{$product.rating}
					{if ($product.countcom!='')}
					<span class="icon small-use-icon"></span>
				  <span class="count text-bottom">{$product.countcom}</span>
					{/if}
				 
				</div>
			{/if}
			
			
            <ul class="product-list" id="productTab{$product.id}">
			{include file='tpl/products/optionsCategory.tpl'}
            </ul>
            <a class="btn_product_page" href="{link id={$product.id}}">[[%polylang_site_common_detail]]</a>
            </form>



          </div>