<li>
<div class="product-item small-item ms2_product" data-list="{$item.list}" data-name="{$item.name}" data-brand="{$item.brand}" data-category="{$item.category}" data-price="{$item.min_price}" data-position="{$item.position}" id="action_product_{$item.id}">
	<form class="ms2_form" method="post">
		<input type="hidden" name="product_id" value="{$item.id}" />
	  <div class="product-item-img">
	    {snippet name=pthumb params="input=`{$item.image}`&options=`w=150&h=223&far=1`&useResizer=`1`" assign=preview}               

	  
		<a data-id="{$item.id}" data-type="action_product" href="/{link id={$item.id}}"><img src="{$preview}" alt="{$item.pagetitle}" /></a>
		
		<div class="product-item-lbls">
		{if ($item.popular==1)}
			<div class="product-info-lbl top-sales-lbl"><b>ТОП</b> продаж</div>
		{/if}
		
		
		{if ($item.new==1)}
			<div class="product-info-lbl new-product-lbl"><b>НОВИНКА</b></div>
		{/if}
		{*{snippet name=isaction params="id={$item.id}"}*}
		{if ($item.isaction==1)}
		<div class="product-photo-lbl action-lbl"><b>АКЦИЯ</b></div>
		{/if}
		
		{if ($item.valueaction!='')}
		<div class="product-photo-lbl discount-lbl">СКИДКА<br/><b>-{$item.valueaction}%</b></div>
		{/if}
		
		
		
		</div>
	  </div>
	  <div class="product-item-title"><a data-id="{$item.id}" data-type="action_product" href="/{link id={$item.id}}">{$item.pagetitle}</a></div>
	  
	  
	  {if ($item.rating!='')}
			<div class="product-item-stars">
					{$item.rating}
					{if ($item.countcom!='')}
						<span class="icon small-use-icon"></span>
						  <span class="count text-bottom">{$item.countcom}</span>
					{/if}					
				</div>
	  {/if}
	  
	  <div class="tabs product-item-tabs" role="tabpanel">
	  {assign var=product value=$item}
	  {include file='tpl/products/optionsAction.tpl'}
		
	  </div>
	  
	  </form>
</div>
</li>