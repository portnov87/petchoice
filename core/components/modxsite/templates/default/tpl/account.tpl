{extends file="layout.tpl"}

{block name=content}
{literal}

[[!notAuthRedirect]]

      <div class="container">
          <div class="page-personal">
            [[BreadCrumb? &scheme=`full` &containerTpl=`breadcrumbOuterTpl` &currentCrumbTpl=`breadcrumbCurrentTpl` &linkCrumbTpl=`breadcrumbLinkTpl` &homeCrumbTpl=`breadcrumbLinkTpl`]]
            <div class="h1">[[%polylang_site_account_hello]], <span class="turquoise">[[+fullname]]</span></div>
            <br>
            <div class="row" role="tabpanel">
              <div class="col-xs-4">
                <div class="vertical-tabs">
                  <ul class="tab-list" role="tablist">
                    <li role="presentation" class="active"><a href="#profile" aria-controls="profile" role="tab" data-toggle="tab">[[%polylang_site_account_personal]]</a></li>
                    <li role="presentation"><a href="#myPets" aria-controls="myPets" role="tab" data-toggle="tab">[[%polylang_site_account_pets]]</a></li>
                    <li role="presentation"><a href="#transaction" aria-controls="transaction" role="tab" data-toggle="tab">[[%polylang_site_account_history]]</a></li>
                    <li role="presentation"><a href="#payment" aria-controls="payment" role="tab" data-toggle="tab">[[%polylang_site_account_bill]]</a></li>
                  </ul>
                  <div class="row">
                    <div class="col-xs-4">
                      <img src="/assets/images/puppy_4.png" alt="">
                    </div>
                    <div class="col-xs-8">
                      <br>
                      <br>
                      <p>
                        <a href="" data-toggle="modal" data-target="#feedbackModal">[[%polylang_site_form_review]]</a>
                      </p>
                      <p class="grey small">
                          [[%polylang_site_account_aboutourwork_review]]
                      </p>
					  
					  {/literal}
					  
					 {if ($modx->user->count_orders_close>=2)} 
					 <!-- <style>

    .social_reviews_block {
        display: inline-block;
        width: 100%;
        height: auto;
        list-style-type: none;
        padding: 0 !important;
        position: relative;
        left: -10px;

    }
    .social_reviews_block li{
        display: inline-block;
        width: 33.33%;
        height: 40px;
        float: left;
    }
    .social_reviews_block li > a{
        width: 80%;
        position: relative;
        height: inherit;
        display: block;
        border: none !important;
        text-decoration: none !important;
        padding: 0 !important;
        margin: auto !important;
    }
    .social_reviews_block li a > img{
        display: block;
        width: 100%;
        height: auto;
    }
    .social_reviews_block li > a:hover div{
        display: inline-block;
    }
    .social_reviews_block li > a > div{
        display: none;
        left: 0;
        bottom:45px;
        z-index: 10;
        position: absolute;
        width: auto;
        max-width: 300px;
        padding: 15px;
        background: #ffffff;
        -webkit-border-radius:5px;
        -moz-border-radius:5px;
        border-radius:5px;
        border: 1px solid #e8ecf0;
        -webkit-box-shadow:     0 5px 7px 0 rgba(0,0,0,0.2);
        -moz-box-shadow:        0 5px 7px 0 rgba(0,0,0,0.2);
        box-shadow:             0 5px 7px 0 rgba(0,0,0,0.2);
    }
    .social_reviews_block li > a > div:before{
        content: " ";
        position: absolute;
        z-index: 2;
        bottom:-5px;
        left: 20%;
        display: block;
        background: #ffffff;
        width: 10px;
        height: 10px;
        border-bottom: 1px solid #e8ecf0;
        border-left: 1px solid #e8ecf0;
        -webkit-transform: rotate(-45deg);
        -moz-transform: rotate(-45deg);
        -ms-transform: rotate(-45deg);
        -o-transform: rotate(-45deg);
        transform: rotate(-45deg);
    }
    .social_reviews_block li > a > div > span{
        display: block;
        max-width: 100%;
        overflow: hidden;
        margin-bottom: 15px;
        border-bottom: 1px dashed #c5d4e4;
    }
    .social_reviews_block li > a > div > span > img{
        width: 100%;
        height: auto;
    }
    .social_reviews_block li > a > div > ul{
        display: inline-block;
        width: 100%;
        list-style-type: none;
        padding: 0 !important;

    }
    .social_reviews_block li > a > div > ul > li{
        display: inline-block !important;
        width: 100% !important;
        margin-bottom: 10px !important;
        font-size: 12px !important;
        line-height: 14px !important;
        color:#838fa3 !important;
        float: none;
        clear: both;
        height: auto !important;
    }


</style>
<ul class="social_reviews_block">
    <li>
        <a target="_blank" href="https://market.yandex.ua/shop/335020/reviews/" title="">
            <img src="/assets/images/account/icn_sr_01.png" alt="market.yandex.ua" />
            <div>
                <span>
                    <img src="/assets/images/account/icn_pcp.png" alt="Royal Canin Maxi Junior (15 кг)" />
                </span>
                <ul>
                    <li>- оставьте отзыв в 2-х указанных сервисах, каждый не менее 150 символов и желательно уникальные</li>
                    <li>- сообщите об этом на sales@petchoice.com.ua (желательно прикрепить скриншот)</li>
                    <li>- получите скидку <strong>10%</strong> на любой товар</li>
                </ul>
            </div>
        </a>
    </li>
    <li>
        <a target="_blank" href="http://hotline.ua/yp/27099/reviews/" title="">
            <img src="/assets/images/account/icn_sr_02.png" alt="hotline.ua" />
            <div>
                <span>
                    <img src="/assets/images/account/icn_pcp.png" alt="Royal Canin Maxi Junior (15 кг)" />
                </span>
                <ul>
                    <li>- оставьте отзыв в 2-х указанных сервисах, каждый не менее 150 символов и желательно уникальные</li>
                    <li>- сообщите об этом на sales@petchoice.com.ua (желательно прикрепить скриншот)</li>
                    <li>- получите скидку <strong>10%</strong> на любой товар</li>
                </ul>
            </div>
        </a>
    </li>
    <li>
        <a target="_blank" href="http://price.ua/firm54945.html#review" title="">
            <img src="/assets/images/account/icn_sr_03.png" alt="price.ua" />
            <div>
                <span>
                    <img src="/assets/images/account/icn_pcp.png" alt="Royal Canin Maxi Junior (15 кг)" />
                </span>
                <ul>
                    <li>- оставьте отзыв в 2-х указанных сервисах, каждый не менее 150 символов и желательно уникальные</li>
                    <li>- сообщите об этом на sales@petchoice.com.ua (желательно прикрепить скриншот)</li>
                    <li>- получите скидку <strong>10%</strong> на любой товар</li>
                </ul>
            </div>
        </a>
    </li>
    <li>
        <a target="_blank" href="https://www.facebook.com/pg/petchoice.com.ua/reviews/" title="">
            <img src="/assets/images/account/icn_sr_04.png" alt="facebook" />
            <div>
                <span>
                    <img src="/assets/images/account/icn_pcp.png" alt="Royal Canin Maxi Junior (15 кг)" />
                </span>
                <ul>
                    <li>- оставьте отзыв в 2-х указанных сервисах, каждый не менее 150 символов и желательно уникальные</li>
                    <li>- сообщите об этом на sales@petchoice.com.ua (желательно прикрепить скриншот)</li>
                    <li>- получите скидку <strong>10%</strong> на любой товар</li>
                </ul>
            </div>
        </a>
    </li>
    <li>
        <a target="_blank" href="https://www.google.com.ua/maps/place/%D0%97%D0%BE%D0%BE%D0%BC%D0%B0%D0%B3%D0%B0%D0%B7%D0%B8%D0%BD+PetChoice/@46.493232,30.7176353,17z/data=!3m1!4b1!4m5!3m4!1s0x40c631f73265280b:0x4bf82acf322dd7e1!8m2!3d46.493232!4d30.719824" title="">
            <img src="/assets/images/account/icn_sr_05.png" alt="market.yandex.ua" />
            <div>
                <span>
                    <img src="/assets/images/account/icn_pcp.png" alt="Royal Canin Maxi Junior (15 кг)" />
                </span>
                <ul>
                    <li>- оставьте отзыв в 2-х указанных сервисах, каждый не менее 150 символов и желательно уникальные</li>
                    <li>- сообщите об этом на sales@petchoice.com.ua (желательно прикрепить скриншот)</li>
                    <li>- получите скидку <strong>10%</strong> на любой товар</li>
                </ul>
            </div>
        </a>
    </li>
    <li>
        <a target="_blank" href="https://yandex.ua/maps/org/petchoice/1497651513/?add-review&lang=ru" title="">
            <img src="/assets/images/account/icn_sr_06.png" alt="yandex maps" />
            <div>
                <span>
                    <img src="/assets/images/account/icn_pcp.png" alt="Royal Canin Maxi Junior (15 кг)" />
                </span>
                <ul>
                    <li>- оставьте отзыв в 2-х указанных сервисах, каждый не менее 150 символов и желательно уникальные</li>
                    <li>- сообщите об этом на sales@petchoice.com.ua (желательно прикрепить скриншот)</li>
                    <li>- получите скидку <strong>10%</strong> на любой товар</li>
                </ul>
            </div>
        </a>
    </li>
</ul>-->
{/if}
{literal}
                    </div>
                  </div>
                </div>
              </div>
              <div class="col-xs-8">
                <div class="tab-content">
                  <div role="tabpanel" class="tab-pane active" id="profile">
                      [[!OfficeProfile? 
                      &tplProfile=`tpl.UpdateProfileForm`
                      &avatarParams=`{"w":160,"h":160,"zc":1,"bg":"ffffff","f":"jpg"}`
                      &profileFields=`username:50,email:50,
                      fullname:50,extended[lastname]:50,phone:12,
                      state,photo,specifiedpassword,extended[room],extended[house],extended[street],extended[housing],
						  extended[parade],extended[doorphone],extended[floor],
                      confirmpassword,extended[payment],extended[delivery],extended[city_id],extended[warehouse],city`
                      ]]
                  </div>
                  <div role="tabpanel" class="tab-pane" id="myPets">
                      <div id="petlist">
                      <p class="info-msg">[[%polylang_site_label_addpets]]</p>
                      [[!getPet? &tpl=`tpl.UpdatePetForm`]]
                      </div>
                      <button class="btn green-btn" data-toggle="modal" data-target="#petModal">[[%polylang_site_account_add_pet]] <i class="glyphicon glyphicon-plus"></i></button>
                    <div class="modal fade" id="petModal" tabindex="-1" role="dialog" aria-labelledby="thanksCallModalLabel" aria-hidden="true">
                    <div class="modal-dialog">
                      <div class="modal-content">
                        <div class="modal-header">
                          <button type="button" class="close" data-dismiss="modal" aria-label="Close"></button>
                          <h3 class="modal-title greener" id="thanksCallModalLabel"></h3>
                        </div>
                        <div class="modal-body">
                        [[!AjaxForm?
                        	&snippet=`addPet`
                        	&form=`tpl.AddPetForm`
                        	&validate=`breed:required,type:required,name:required,genre:required`
                        ]]
                        </div>
                        <div class="modal-footer">
                        </div>
                      </div>
                    </div>
                    </div>
                  </div>
                  <div role="tabpanel" class="tab-pane" id="transaction">
                    [[!msPurchaseHistory? &limit=`12`]]
                  </div>
                  <div role="tabpanel" class="tab-pane" id="payment">
                    [[!msGetUserOrders? &tpl=`userOrdersRowTpl` &emptyTpl=`tpl.userOrdersEmpty`]]
                  </div>
                </div>
              </div>
            </div>
          </div>
      </div>
        [[$tpl.feedbackModal]]
		{/literal}
{/block}