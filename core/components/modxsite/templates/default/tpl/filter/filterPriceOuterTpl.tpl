

<fieldset class="for-item range-item" id="mse2_ms|price">
	<p><b>Цена, грн/кг:</b></p>
	<div class="show_hide">
		
		<div class="formCost row">
		
		<div class="col-xs-6">
                <input type="text" name="price_kg" id="price_kg_0" value="{$pricemin}" class="custom-inpt inline minCost" readonly/>
              </div>
              <div class="col-xs-6">
                <input type="text" name="price_kg" id="price_kg_1" value="{$pricemax}" class="custom-inpt inline maxCost" readonly/>
              </div>
	
		</div>
		<span id="pricekg_min" style="display:none;">{$min}</span>
		<span id="pricekg_max" style="display:none;" >{$max}</span>
		<div class="sliderCont">
			<div class="mse2_number_slider" data-idx=""></div>
		</div>
	</div>
</fieldset>

<!--
<div id="mse2_{$table}{$delimeter}{$filter}" class="for-item range-item">
 <p><b>Цена (грн):</b></p>

 <div class="row">
              <div class="col-xs-6">
                <input type="text" name="{$table}{$delimeter}{$filter}_min" id="amount1" value="{$pricemin}" class="custom-inpt inline" readonly >
              </div>
              <div class="col-xs-6">
                <input type="text" name="{$table}{$delimeter}{$filter}_max"  id="amount2" value="{$pricemax}" class="custom-inpt inline" readonly >
              </div>
            </div>
			<button class="filterprice">ok</button>
			<input type="hidden" name="pricerange" value=""/>

            <div id="slider-range"></div>

</div>


{literal}

<script>
//$(document).ready(function(){



/* Slider range */
$(function () {
  'use strict';

  $( '#slider-range' ).slider({
    range: true,
   min: {/literal}{$min}{literal},
    max: {/literal}{$max}{literal},
    values: [ {/literal}{$pricemin}, {$pricemax}{literal} ],
    slide: function( event, ui ) {
      $( '#amount1' ).val( ui.values[ 0 ] );
      $( '#amount2' ).val( ui.values[ 1 ] );
    }
  });
  $( '#amount1' ).val( $( '#slider-range' ).slider( 'values', 0 ) );
  $( '#amount2' ).val( $( '#slider-range' ).slider( 'values', 1 ) );
});

/*
$( '#slider-range' ).slider({
    range: true,
    min: '{/literal}{$min}{literal}',
    max: '{/literal}{$max}{literal}',
    values: [ '{/literal}{$min}', '{$max}{literal}' ],
    slide: function( event, ui ) {
      $( '#amount1' ).val( ui.values[ 0 ] );
      $( '#amount2' ).val( ui.values[ 1 ] );
    }
  });
  $( '#amount1' ).val( $( '#slider-range' ).slider( 'values', 0 ) );
  $( '#amount2' ).val( $( '#slider-range' ).slider( 'values', 1 ) );
});*/
</script>
{/literal}
-->