{if ($label!='')}
	{if ($filter=='adiscount')}
		{$rows}<br/>
	{else}

		<div class="form-group mse2_{$filter} ">
			<label class="control-label">{$label}:</label>
			<div class="custom-select-container">
				<select id="mse2_attributes_{$filter}"
						name="{$filter}"
						class="attributes_raiting form-control replaced"
						data-error="это поле обязательное"
						aria-invalid="false">

					<option value="">-Выбрать-</option>
					{$rows}
				</select>
			</div>
			<div class="help-block with-errors"></div>
		</div>


	{/if}
{/if}


