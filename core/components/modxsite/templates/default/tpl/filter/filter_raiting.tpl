

<div>
    <div id="mse2_mfilter" class="row msearch2">
		<div class="col-xs-3">
			{if ($filters!='')}
				{nocache}
					{block name=filter nocache}
						<div class="grey-sidebar">
							<p class="h3 green"><b>{$labelfilter}</b></p>
							{if ($filtershow==1)}
								<div id="mse2_selected_wrapper" class="for-item">
									<div id="mse2_selected"  class="raiting_selected">
										<p><b>[[%polylang_site_your_choice]]:</b></p>
										<span class="spanselected"></span>
										<a data-url="{$pageurl}" class="delete-filters"  href="#">[[%polylang_site_reset_filters]]</a>
									</div>
								</div>
								<form action="" method="post" id="mse2_filters">
									{$filters}
								</form>
							{else}
								<div>{$filters}</div>
							{/if}
						</div>
					{/block}
				{/nocache}
            {else}
                <div class="grey-sidebar">
    						<p class="h3 green"><b>{$labelfilter}</b></p>
							    <div class="form-group ">
                        			<label class="control-label">Мой питомец:</label>
                        			<div class="custom-select-container">
                        				<select id="mse2_attributes_type_pet"
                        						name="type_pet"
                        						class="attributes_raiting1 form-control replaced change_type_pet"
                        						>
                                                <option value="">--</option>
                        					<option value="/rejting-kormov/dlya-koshek">Кошка</option>
                                            <option value="/rejting-kormov/dlya-sobak">Собака</option>
                        				</select>
                        			</div>
                        			<div class="help-block with-errors"></div>
                        		</div>
						</div>
			{/if}
			{nocache}
				{block name=recent nocache}
					{snippet name=view_recent params="action=`returnViewed`"}
				{/block}
			{/nocache}

			<!-- FILERS FORM END -->

		</div>
		<div class="col-xs-9">
			{nocache}
				{block name=filterres nocache}



					{if ($banner!='')}
						{$banner}
					{/if}

					<div id="mse2_results">
						{$results}
					</div>
					<div class="page-buttons clearfix">

						{if ($limit2>0)}
							<button id="showmore" type="button" class="btn green-btn text-uppercase pull-left">Показать ещё <span class="count">{$limit2}</span> &nbsp;<span class="icon down-white-icon"></span></button>
						{else}
							<button style="display:none;" id="showmore" type="button" class="btn green-btn text-uppercase pull-left">Показать ещё <span class="count">{$limit2}</span> &nbsp;<span class="icon down-white-icon"></span></button>
						{/if}

						{literal}
							<nav class="pull-right">
								<ul id="mse2_pagination" class="pagination">

									[[!+page.nav]]
								</ul>
							</nav>
						{/literal}
					</div>
				{/block}
			{/nocache}

			{literal}
		
			{/literal}
			<div class="row text-part">
			{if ($showseotext>0)}
				{field name=seo_text}
			{/if}


			{literal}   {/literal}
			</div>
		</div>

	</div>


</div>



