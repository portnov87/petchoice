{extends file="layout.tpl"}

{block name=content}
{literal}
     <div class="container">
        <div class="page-404">
          <div class="breadcrumbs small">
            <a href="#">Главная</a>&nbsp;&nbsp;<span class="grey-black">&gt;</span>&nbsp;&nbsp;<span>Страница не найдена</span>
          </div>
          <div class="row">
              <div class="col-xs-7 text-center">
                  <h1 class="red">404</h1>
                  <p class="grey">
                    ТАКОЙ СТРАНИЦЫ НЕ СУЩЕСТВУЕТ
                  </p>
              </div>
              <div class="col-xs-5">
                  <img src="/assets/images/cat_1.png" alt="">
              </div>
          </div>
        </div>
      </div>

	  {/literal}
	  {/block}
