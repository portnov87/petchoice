<div class="brands">
          <h2 class="h1 text-left">Бренды</h2>

          <div class="brands-slider slider">
            <ul class="bxslider brands-bx">
              
						{foreach $brands as $item} 						
							{snippet name=pthumb params="input=`{$item.logo}`&options=`w=172&h=172&far=1`&useResizer=`1`" assign=preview}               							
							<li><a href="{$item.url}"><img style="max-width:172px;" src="{$preview}" alt="{$item.name}" /></a> </li>
						{/foreach}
						
			  
            </ul>
          </div>

          <a href="/brands" class="btn grey-btn">Просмотреть все бренды </a>
        </div>
		
		
		