
<div class="same-products">
      <h3 class="h1">Похожие товары</h3>
      <div class="product-slider slider">
        <ul class="bxslider slider-actions">
          {foreach $rows as $row}   
                {assign var=product value=$row}
						{include file='tpl/related/productRelated.tpl'}
	      {/foreach}
        </ul>
      </div>
</div>

<script>
    {literal}
    dataLayer.push({
        'ecommerce': {
            'currencyCode': 'UAH',
            'impressions': [{/literal}{$ecommerce}{literal}]
        },'event': 'gtm-ee-event',
        'gtm-ee-event-category': 'Enhanced Ecommerce',
        'gtm-ee-event-action': 'Product Impressions',
        'gtm-ee-event-non-interaction': 'True',
    });
    {/literal}
</script>