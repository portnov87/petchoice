
{if (count($rows)>1)}
<div class="same-products">
      <h3 class="h1">Рекомендованные товары</h3>
      <div class="product-slider slider"  style="width:570px; margin-left:80px;">
        <ul class="bxslider slider-actions">
          {foreach $rows as $row}   
                {assign var=product value=$row}
            			{include file='tpl/related/productRelated.tpl'}
	      {/foreach}
        </ul>
      </div>
</div>

<script>
    {literal}
    dataLayer.push({
        'ecommerce': {
            'currencyCode': 'UAH',
            'impressions': [{/literal}{$ecommerce}{literal}]
        },'event': 'gtm-ee-event',
        'gtm-ee-event-category': 'Enhanced Ecommerce',
        'gtm-ee-event-action': 'Product Impressions',
        'gtm-ee-event-non-interaction': 'True',
    });
    {/literal}
</script>
{else}
<div class="same-products">
      <h3 class="h1">Наши акции</h3>
<div class="product-slider slider" style="width:570px; margin-left:80px;">
                <ul class="bxslider slider-actions" style="width:100%;">    			
				{assign var=params value=[
					"startId"       => 5
					,"level"        => 2
					,"sortBy"       => "menuindex"
					,"levelClass"   => "level"
					,"cacheable"    =>false
                    ,'list'=>'Promotions and discounts'
					,"where"=>[
							"Data.adiscount"=>"1"
						   ]
					,"id"           => "catalog"    
				]}
				{processor action="site/web/products/getproducts" ns="modxsite" params=$params assign=resultaction}
				{assign var=items value=$resultaction.object}
				
				{foreach $items.list as $item}
					{include file='tpl/products/action.tpl'}
				{/foreach}
                </ul>


                
              </div>
</div>
{/if}