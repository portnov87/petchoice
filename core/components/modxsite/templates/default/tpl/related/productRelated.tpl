
<li>
                    <div class="product-item small-item first-child" data-list="{$product.list}" data-name="{$product.name}" data-brand="{$product.brand}" data-category="{$product.category}" data-price="{$product.min_price}" data-position="{$product.position}"
                         id="related_product_{$product.id}">
                      <div class="product-item-img">
					  
					  {snippet name=pthumb params="input=`{$product.image}`&options=`w=120&h=210&far=1`&useResizer=`1`" assign=preview}
                        <a data-id="{$product.id}" data-type='related_product' href="{link id={$product.id}}"><img src="{$preview}"  alt="{$product.pagetitle}" /></a>

                        <div class="product-item-lbls">
                          {if ($product.popular==1)}
							  <div class="product-info-lbl top-sales-lbl"><b>ТОП</b> продаж</div>
							  {/if}
							  
							  {if ($product.new==1)}
							  <div class="product-photo-lbl new-product-lbl"><b>НОВИНКА</b></div>
							  {/if}
				  {if ($product.toorder==1)}
			  <div class="product-photo-lbl toorder-lbl">ПОД ЗАКАЗ</div>
			  {/if}
							  {if ($product.isaction==1)}
										  <div class="product-photo-lbl action-lbl"><b>АКЦИЯ</b></div>
										  {/if}
										  
										  {if ($product.action!='')}
										  <div class="product-info-lbl discount-lbl">СКИДКА <b>-{$product.action}%</b></div>
										  {/if}
                        </div>
                      </div>
                      <div class="product-item-title">
                          <a data-id="{$product.id}" data-type='related_product'  data-list="{$product.list}" href="{link id={$product.id}}">{$product.pagetitle}</a>
                      </div>
                     {if ($product.rating!='')}
								<div class="product-item-stars">
									{$product.rating}
								</div>
					{/if}
                    
                      <div class="tabs product-item-tabs" role="tabpanel">
                             {include file='tpl/products/optionsAction.tpl'}
                      </div>
                    </div>
                  </li>


				  
				  
				  
