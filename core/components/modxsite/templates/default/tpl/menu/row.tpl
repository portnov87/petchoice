<div class="col-xs-2">
								<div class="main-menu-item {if ($item.id==13)}dogs{elseif ($item.id==14)}cats{elseif ($item.id==26)}fishes{elseif ($item.id==27)}birds{elseif ($item.id==28)}mouses{elseif ($item.id==1148)}horek{/if}">
								  <div class="item-nav">
									<span class="icon {if ($item.id==13)}dog-icon{elseif ($item.id==14)}cat-icon{elseif ($item.id==26)}fish-icon{elseif ($item.id==27)}bird-icon{elseif ($item.id==28)}mouse-icon{elseif ($item.id==1148)}horek-icon{/if}"></span>
									{$item.pagetitleLang}
								  </div>
								  <div class="submenu">
									<div class="content-submenu">
									  <span class="menu-pointer"></span>
									  <div class="row">
										
										{snippet name=getCategoryGroup params="id={$item.id}"}
										{if ($item.id==13)}
										<div class="animal-block dog">
										  <img src="/assets/images/dog-submenu.png" alt="Товары для собак" />
										</div>																			
										{elseif ($item.id==14)}
										<div class="animal-block dog">
										  <img src="/assets/images/kitten-menu.png" alt="Товары для котов" />
										</div>
										{elseif ($item.id==26)}										
										<div class="animal-block dog">
										  <img src="/assets/images/fish-menu.png" alt="Товары для рыб" />
										</div>
										{elseif ($item.id==27)}
										<div class="animal-block dog">
										  <img src="/assets/images/parrot-menu.png" alt="Товары для птиц" />
										</div>
										{elseif ($item.id==28)}
										<div class="animal-block dog">
										  <img src="/assets/images/rodent-menu.png" alt="Товары для грызунов" />
										</div>
										{elseif ($item.id==1148)}
										<div class="animal-block dog">
										  <img src="/assets/images/horek.png" alt="Товары для хорьков" />
										</div>
										{/if}
									  </div>
									</div>
								  </div>
								</div>
							  </div>