

<div class="rated_product_container">
    <div>
        <div class="rated_p_img">
        
            {snippet name=pthumb params="input=`{$product.image}`&options=`w=150&h=223&far=1`&useResizer=`1`" assign=preview}
            
            <img style="max-width:100%;height:223px" src="{$preview}" alt="{$pagetitle}" />
        </div>
        <div class="rated_p_description_box">
            <div class="hn_2">
                <h2>
                    <a href="{link id={$product.id}}" data-type="list_product" data-id="{$product.id}">{$product.pagetitle}</a>
                </h2>
            </div>
            <div class="product_code">
                Код продукта: <span>{$product.article}</span>
            </div>
            <div class="rated_p_description_text">
                {$product.introtext}
            </div>
        </div>
        <div class="rated_p_ballot_box">
            <div>
                <div class="all_voted">
                    <div class="product_all_points">
                        Баллы: <span>{$product.count_points}</span>
                    </div>
                    <div class="product_points">
                        <div class="rpp_1">{$product.rating_count}</div>
                        <div class="rpp_2">{$product.countcom}</div>
                        <div class="rpp_3">{$product.count_likes}</div>
                        <div class="rpp_4">{$product.count_shared}</div>
                    </div>
                </div>
                <div class="vote_btn">
                    <a class="open_modal_vote" data-product_id="{$product.id}" data-href="https://petchoice.ua/{link id={$product.id}}" href="javascript:void(0)" target="_blank">Проголосовать</a>
                    <!--data-toggle="modal" data-dismiss="modal"
                                           data-target="#submitRaitingModal"-->
                </div>
            </div>
        </div>
    </div>
    <div>
        <div class="hn_2">
            <div>Характеристики</div>
        </div>
        <div class="characteristics_table_scroll">
            <div class="characteristics_table">
                {if ($product.vendorcountry)}
                <div>
                    <div>Страна производитель</div>
                    <div>{$product.vendorcountry}</div>
                </div>
                {/if}
                {if ($product.brand)}
                <div>
                    <div>Торговая марка</div>
                    <div>{$product.brand}</div>
                </div>
                {/if}
                {if ($product.category_name)}
                <div>
                    <div>Категория</div>
                    <div>{$product.category_name}</div>
                </div>
                {/if}
                {$product.attributes}
            </div>
        </div>

        <div class="product_content">
            {if ($product.protein!='')}
            <span>протеин: {$product.protein};</span>
            {/if}
            {if ($product.shir!='')}
            <span>жир: {$product.shir};</span>
            {/if}
            {if ($product.clutch_bag!='')}
            <span>клетчатка: {$product.clutch_bag};</span>
            {/if}
            {if ($product.zola!='')}
            <span>зола: {$product.zola};</span>
            {/if}
            {if ($product.yglevodi!='')}
            <span>углеводы: {$product.yglevodi};</span>
            {/if}
        </div>

    </div>
</div>