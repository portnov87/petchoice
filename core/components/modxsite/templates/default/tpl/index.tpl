{extends file="layout.tpl"}


{block name=mainpage}
  <script>
    eS('sendEvent', 'MainPage');
  </script>
 <!-- fb widget 
   <div id="fb-root"></div>
    <script>(function(d, s, id) {
      var js, fjs = d.getElementsByTagName(s)[0];
      if (d.getElementById(id)) return;
      js = d.createElement(s); js.id = id;
      js.src = "//connect.facebook.net/ru_RU/sdk.js#xfbml=1&version=v2.0";
      fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));</script>
      end fb -->

       <div class="container">
        <div class="breadcrumbs">
          &nbsp;
        </div>
        
            
        
        <div class="tabs main-tab" role="tabpanel">
          <ul class="nav nav-tabs" role="tablist" id="mainTab">
            <li role="presentation" class="active">
              <a href="#actions-pane" aria-controls="actions-pane" role="tab" data-toggle="tab">[[%polylang_site_main_actiontab]]</a>
            </li>
            <li role="presentation">
              <a href="#new-products-pane" class='newtabmain' aria-controls="new-products-pane" role="tab" data-toggle="tab">[[%polylang_site_main_newtab]]</a>
            </li>
            <li role="presentation">
              <a href="#royal-canin-pane" class='newtabmain' aria-controls="royal-canin-pane" role="tab" data-toggle="tab">Royal Canin</a>
            </li>
          </ul>

          <div class="tab-content">
            <div role="tabpanel" class="tab-pane active" id="actions-pane" aria-labelledby="actions-tab">
            {if ($modx->getOption('show_banner'))}
                {snippet name=modSliderRevolution params="slider=slider_home"}
                <div style="text-align:center;width:100%;"><a href="/discounts" class="btn grey-btn">[[%polylang_site_main_allaction]] </a></div>
            {else}
                  <div class="product-slider slider">
                    <ul class="bxslider slider-actions" style="width:100%;">				
    				{assign var=params value=[
    					"startId"       => 10
    					,"level"        => 2
    					,"sortBy"       => "menuindex"
    					,"levelClass"   => "level"
    					,"cacheable"    =>false
                        ,'list'=>'Promotions and discounts'
    					,"where"=>[
    							"Data.adiscount"=>"1"
    						   ]
    					,"id"           => "catalog"    
    				]}
    				{processor action="site/web/products/getproducts" ns="modxsite" params=$params assign=resultaction}
    				{assign var=items value=$resultaction.object}
    				
    				{foreach $items.list as $item}
    					{include file='tpl/products/action.tpl'}
    				{/foreach}
                    </ul>
    
    
                    <div style="text-align:center;width:100%;"><a href="/discounts" class="btn grey-btn">[[%polylang_site_main_allaction]] </a></div>
                  </div>
              {/if}
            </div>
            
            
            
            <div role="tabpanel" class="tab-pane" id="royal-canin-pane"  aria-labelledby="royal-canin-tab">
              <div class="product-slider slider">
                <ul class="bxslider slider-royal-canin" style="width:100%;">
    				{assign var=paramscanin value=[
						"startId"       => 10
						,"level"        => 2
						,"sortBy"       => "menuindex"
						,"levelClass"   => "level"
						,"cacheable"    =>false
                        ,'list'=>'Royal Canin'
                        ,"where"=>[
    						"Vendor.id"=>"19"
						   ]                      
						,"id"           => "catalog"    					  
					]}
					{processor action="site/web/products/getproducts" ns="modxsite" params=$paramscanin assign=resultcanin}
					{assign var=itemscanin value=$resultcanin.object}					
					{foreach $itemscanin.list as $item}
						{include file='tpl/products/action.tpl'}
					{/foreach}
                </ul>


                <script>
                    {literal}
                    $(document).ready(function (){
                       $('.newtabmain').on('click',function(){
                           dataLayer.push({
                               'ecommerce': {
                                   'currencyCode': 'UAH',
                                   'impressions': [{/literal}{$itemsnew.ecomerc}{literal}]
                               },'event': 'gtm-ee-event',
                               'gtm-ee-event-category': 'Enhanced Ecommerce',
                               'gtm-ee-event-action': 'Product Impressions',
                               'gtm-ee-event-non-interaction': 'True',
                           });
                       });
                    });

                    {/literal}
                </script>
                <script>
                    {literal}
                    dataLayer.push({
                        'ecommerce': {
                            'currencyCode': 'UAH',
                            'impressions': [{/literal}{$items.ecomerc}{literal}]
                        },'event': 'gtm-ee-event',
                        'gtm-ee-event-category': 'Enhanced Ecommerce',
                        'gtm-ee-event-action': 'Product Impressions',
                        'gtm-ee-event-non-interaction': 'True',
                    });
                    {/literal}
                </script>
                <!--<script>

                </script>-->
              </div>
            </div>
            
            

            <div role="tabpanel" class="tab-pane" id="new-products-pane"  aria-labelledby="new-products-tab">
              <div class="product-slider slider">
                <ul class="bxslider slider-new-products" style="width:100%;">
					{assign var=paramsnew value=[
						"startId"       => 10
						,"level"        => 2
						,"sortBy"       => "menuindex"
						,"levelClass"   => "level"
						,"cacheable"    =>false
                    ,'list'=>'New arrivals'
						,"where"=>[
							"Data.new"=>"1"
						   ]						  
					]}
					{processor action="site/web/products/getproducts" ns="modxsite" params=$paramsnew assign=resultnew}
					{assign var=itemsnew value=$resultnew.object}					
					{foreach $itemsnew.list as $item}
						{include file='tpl/products/action.tpl'}
					{/foreach}
                </ul>


                <script>
                    {literal}
                    $(document).ready(function (){
                       $('.newtabmain').on('click',function(){
                           dataLayer.push({
                               'ecommerce': {
                                   'currencyCode': 'UAH',
                                   'impressions': [{/literal}{$itemsnew.ecomerc}{literal}]
                               },'event': 'gtm-ee-event',
                               'gtm-ee-event-category': 'Enhanced Ecommerce',
                               'gtm-ee-event-action': 'Product Impressions',
                               'gtm-ee-event-non-interaction': 'True',
                           });
                       });
                    });

                    {/literal}
                </script>
                <script>
                    {literal}
                    dataLayer.push({
                        'ecommerce': {
                            'currencyCode': 'UAH',
                            'impressions': [{/literal}{$items.ecomerc}{literal}]
                        },'event': 'gtm-ee-event',
                        'gtm-ee-event-category': 'Enhanced Ecommerce',
                        'gtm-ee-event-action': 'Product Impressions',
                        'gtm-ee-event-non-interaction': 'True',
                    });
                    {/literal}
                </script>
                <!--<script>

                </script>-->
              </div>
            </div>
          </div>
        </div>
        


        <div class="row">
          <div class="col-xs-6">
            <h2 class="h1">[[%polylang_site_main_reviews]] <span class="small grey"><a href="/otzyivyi-sajta">[[%polylang_site_main_reviewslabel]] ({snippet name=TicketCount params="resources=463"})</a>
			{*Всего отзывов: {snippet name=TicketCount params="resources=463"}*}</span></h2>
			
			
			{assign var=paramsticket value=[
							"limit"=>"1"
						   ,"idcontent"=>"463"
						   ,"sortby"=>"RAND()"
						   ,"sortdir"=>"ASC"					   
					   ]}
			{processor action="site/web/getcomments" ns="modxsite" params=$paramsticket assign=lastcommentobj}
			{assign var=lastcomment value=$lastcommentobj.object}					
			{foreach $lastcomment as $item}      
						{include file='tpl/comments/lastcomment.tpl'}
					{/foreach}
			
			
          </div>

          <div class="col-xs-6">
            <h2 class="h1">[[%polylang_site_main_anonslabel]]</h2>
            <div class="row social-follows">
              <div class="col-xs-6">
                <iframe width="560" height="315" src="https://www.youtube.com/embed/3aO2YS4UF7E?rel=0&amp;controls=0&amp;showinfo=0"; frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
                <!--<div class="fb-like-box" data-href="https://www.facebook.com/petchoice.com.ua?ref=profile&amp;pnref=lhc" data-width="250" data-height="350" data-colorscheme="light" data-show-faces="false" data-header="false" data-stream="false" data-show-border="false"></div>-->
                
              </div>
              <div class="col-xs-6">
                
                <!--<div id="vk_groups"></div>
				{literal}
                <script type="text/javascript">
                  VK.Widgets.Group("vk_groups", {mode: 0, width: "260", height: "350", color1: 'FFFFFF', color2: '838fa3', color3: 'afc4db'}, 84542368);
                </script>
				{/literal}
                -->
              </div>
            </div>
          </div>
        </div>

        <div class="row follow-part">
          <div class="col-xs-4">
            <p class="title grey">[[%polylang_site_common_subscribe_label]] </p><a class="why-follow" data-toggle="popover" data-popover-content="#follow-popover" data-placement="bottom">[[%polylang_site_main_forwhat]] ?</a>
          </div>
          <div class="col-xs-8">
            <form action="/assets/components/minishop2/subscribe.php" class="formsubscribe" method="post" data-toggle="validator" role="form">
              <div class="form-group inline">
                <span class="inpt-icon name-inpt"></span>
                <input class="form-control custom-inpt" type="text"  name="name" placeholder="Ваше Имя" required>
              </div>
              <div class="form-group inline">
                <span class="inpt-icon email-inpt"></span>
                <input class="form-control custom-inpt" type="email"  name="email" placeholder="Ваш E-mail" required>
              </div>

              <input class="form-control custom-inpt" type="hidden"  name="code" value=''>
              <button type="submit" class="btn green-btn text-uppercase buttonsubscribe" data-type="home" id="buttonsubscribe" data-toggle="modal" data-target="#thanksCallModalLabel">[[%polylang_site_form_suscribe_button]]</button>
              
            </form>
            <div class="messagesubscribe"></div>
            <div class="animal-pic">
              <img src="/assets/images/puppy_3.png" alt="dog" />
            </div>
          </div>
        </div>

        <div class="row benefit-preview">
          [[%polylang_site_main_healthy]]
{*          <div class="col-xs-12">*}
{*            <h2 class="h1">Чем мы можем быть полезны ?</h2>*}
{*          </div>*}

{* <div class="col-xs-3">*}
{*            <div class="helpfully-text">*}
{*              <h3 class="wider-bordered turquoise">Вопрос ветеринару</h3>*}
{*              <p class="small">*}
{*                Если нужен совет профессионала или вас беспокоит состояние питомца или, задайте вопрос здесь*}
{*              </p>*}
{*            </div>*}
{*            <a href="{link id=559}" class="small pull-right">Подробнее</a>*}
{*          </div>*}
{*           <div class="col-xs-3">*}
{*            <div class="helpfully-text">*}
{*              <h3 class="wider-bordered turquoise">Рейтинг кормов</h3>*}
{*              <p class="small">*}
{*                Выбирайте лучшее для своего питомца и голосуйте за свой выбор*}
{*              </p>*}
{*            </div>*}
{*            <a href="{link id=12430}" class="small pull-right">Подробнее</a>*}
{*          </div>*}


{*          <div class="col-xs-3">*}
{*            <div class="helpfully-text">*}
{*              <h3 class="wider-bordered turquoise">Блог</h3>*}
{*              <p class="small">*}
{*                Интересные статьи и советы экспертов и ветеринаров об уходе, кормлении и воспитании животных*}
{*              </p>*}
{*            </div>*}
{*            <a href="{link id=529}" class="small pull-right">Подробнее</a>*}
{*          </div>*}

{*          <div class="col-xs-3">*}
{*            <div class="helpfully-text">*}
{*              <h3 class="wider-bordered greener">Адреса ветеренарных  клиник</h3>*}
{*              <p class="small">*}
{*                Адреса ветеринарных клиник в Одессе и Украине*}
{*              </p>*}
{*            </div>*}
{*            <a href="{link id=476}" class="small pull-right">Подробнее</a>*}
{*          </div>*}
{*         *}
          
          



          
        </div>
		{snippet name="getBrands"}

        <div class="row text-part">

          [[%polylang_site_main_seotext]]
{*          <div class="col-xs-12">*}
{*            <h2 class="h1">Зоомагазин PetChoice</h2>*}
{*          </div>*}

{*          <div class="col-xs-6">*}
{*            <p>*}
{*              Наверняка вы любите животных, если заглянули в интернет-зоомагазин PetChoice. Может быть, вы их содержите, лечите или просто ищете интересный, необычный подарок. В любом случае, правильно сделали, вам у нас понравится, добро пожаловать! *}
{*              </p>*}
{*              <h3 class="greener"><strong>Ваш питомец наверняка одобрил бы онлайн-зоомагазин PetChoice</strong></h3>*}
{*            <p>*}
{*              Магазин не зря называется PetChoice: если бы домашние животные могли сами выбирать для себя зоотовары, они бы обратили на нас внимание. Еще бы, ведь у нас без преувеличения есть ВСЁ для животных, рыб и птиц:*}
{*            </p>*}
{*            <ul class="list-unstyled green-bullet">*}
{*              <li>Корма: сухие, в виде консервов, еда для лечебной диеты, заменители молока и лакомства.</li>*}
{*              <li>Профилактические и лечебные препараты и аксессуары.</li>*}
{*              <li>Средства и приспособления для облегчения ухода за животными: шампуни, жидкости для удаления неприятных запахов, ножницы, триммеры, расчески, туалеты, наполнители для них и т.д.</li>*}
{*              <li>Товары для домашних животных, незаменимые в быту и на прогулках: ошейники, поводки, миски, игрушки, адресники, домики, переноски, аквариумы и оборудование для них, клетки и т. д.</li>*}
{*              <li>Одежда.</li>*}
{*            </ul>*}

{*            <div class="insertion">*}
{*              <div class="insertion-img inline">*}
{*                <img src="/assets/images/puppy_1.png" alt="dog" />*}
{*              </div>*}
{*              <div class="text-line inline">*}
{*                <i>Все товары интернет-магазина зоотоваров сертифицированы, производятся зарекомендовавшими себя производителями: Royal Canin, Pro Plan, Trixie, Hill's, Dog Chow, Cat Chow, 8in1, Bayer, Gourmet и другими.</i>*}
{*              </div>*}
{*            </div>*}
{*            <h3 class="greener"><strong>Больше 2 тысяч товаров для животных – это только половина того, что можно у нас найти</strong></h3>  *}

{*            <p>*}
{*              В PetChoice любят животных, поэтому мы с удовольствием ведем разделы:*}
{*            </p>*}

{*            <ul class="list-unstyled green-bullet">*}
{*              <li><i>Ветеринарные клиники/приюты</i>. Здесь собраны актуальные адреса и контакты ветклиник и приютов Одессы и других украинских городов. Нужна помощь врача или сами хотите помочь животным? Откройте закладки, зайдите на petchoice.com.ua и быстро найдите нужную информацию.</li>*}
{*              <li><i>Вопрос ветеринару</i>. Оставляйте свои вопросы, и практикующий ветеринарный врач поможет бесплатным советом.</li>*}
{*              <li><i>Блог</i>. Здесь публикуются интересные статьи, помогающие разобраться в актуальных вопросах ухода за животными.</li>*}
{*            </ul>*}
{*          </div>*}

{*          <div class="col-xs-6">*}
{*            <h3 class="greener"><strong>Покупки в магазине зоотоваров PetChoice – это еще и большое удовольствие</strong></h3>*}
{*            <p>*}
{*              Без преувеличений. Становясь нашим покупателем, вы получаете:*}
{*            </p>*}
{*            <ul class="list-unstyled green-bullet">*}
{*              <li><strong>Красочный каталог</strong> с удобным поиском и понятной навигацией.</li>*}
{*              <li><strong>Большие фото товаров</strong>. Мы не скрываем то, как выглядит продукция, поэтому часто предоставляем несколько фотографий.</li>*}
{*              <li><strong>Подробные описания зоотоваров</strong>. Если что-то интересует дополнительно, всегда можно спросить у наших менеджеров.</li>*}
{*              <li><strong>Отзывы о товаре</strong>. Общение – это прекрасно, мы уже ждем ваших комментариев.</li>*}
{*              <li><strong>Акции и скидки</strong>. Мы не жадные, и регулярно делаем интересные предложения, помогающие экономить. Дешевый зоомагазин в Украине – это вполне реально.</li>*}
{*              <li><strong>Дисконтная программа</strong>, действующая на постоянной основе: 5% скидки дается уже при <a href="/account/register">регистрации на сайте</a>. Покупая больше, вы получаете большую скидку.</li>*}
{*              <li><strong>Доставка по Украине</strong>. Зоомагазин находится в Одессе, отправки осуществляются быстро, в течение нескольких дней после заказа товар будет у вас. Мы предлагаем и <a href="/usloviya-dostavki-i-oplatyi">бесплатную доставку</a> – по Одессе и всей Украине.</li>*}
{*            </ul>*}
{*          </div>*}
        </div>
      </div>
	  
	  
	  {/block}

