{extends file="layout.tpl"}

{block name=content}

{snippet name=esputnikcategory params="id={field name=id}"}

<style>

.cashback:hover .cashback_info{
    background:none;border:none;
    padding:0px;
}
    .cashback:hover .cashback_info >div{
            border: 1px solid #ff8f00;
    min-width: 250px;
    font-size: 12px;
    line-height: 14px;
    padding: 11px 13px;
    -webkit-border-radius: 3px;
    -moz-border-radius: 3px;
    border-radius: 3px;
    
    position: relative;
    float: left;
    width: auto;
    height: auto;
    background: white;
    
    color: #222222;
    }        
</style>
  <div class="container">
  <!--  <script>
        {literal}
        dataLayer.push({
            'ecommerce': {
                'currencyCode': 'UAH',

            },'event': 'gtm-ee-event',
            'gtm-ee-event-category': 'Enhanced Ecommerce',
            'gtm-ee-event-action': 'Product Impressions',
            'gtm-ee-event-non-interaction': 'True',
        });
        //'impressions': []
        {/literal}
    </script>-->
{snippet name=BreadCrumb params="scheme=`full`&containerTpl=`breadcrumbOuterTpl`&currentCrumbTpl=`breadcrumbCurrentTpl`&linkCrumbTpl=`breadcrumbLinkTpl`&homeCrumbTpl=`breadcrumbLinkTpl`"}

    <div class="row title-page equal-height-wrapper" style="min-width:100%!important;">
      <div class="col-xs-7 equal-height">
        <h1>{snippet name=metah1 params="h1={field name=pagetitle}"}</h1>  
      </div>
      <div id="mse2_sort" class="col-xs-5 equal-height text-right filters">
        <span>[[%polylang_site_category_sort_label]]:</span><!--popular-->
        <a href="#" class="sort filter-link {if (($mse2_sort=='hits')&&($mse2_sortdir=='asc'))}up-sort active{/if} {if (($mse2_sort=='hits')&&($mse2_sortdir=='desc'))}down-sort active{/if}" data-sort="hits" data-dir="{if (($mse2_sort=='hits')&&($mse2_sortdir=='asc'))}desc{/if} {if (($mse2_sort=='hits')&&($mse2_sortdir=='desc'))}asc{/if}" data-default="asc">[[%polylang_site_category_sort_popular]]</a>
		<a href="#" class="sort filter-link {if (($mse2_sort=='pagetitle')&&($mse2_sortdir=='asc'))}up-sort active{/if} {if (($mse2_sort=='pagetitle')&&($mse2_sortdir=='desc'))}down-sort active{/if}" data-sort="pagetitle" data-dir="{if (($mse2_sort=='pagetitle')&&($mse2_sortdir=='asc'))}desc{/if} {if (($mse2_sort=='pagetitle')&&($mse2_sortdir=='desc'))}asc{/if}" data-default="desc">[[%polylang_site_category_sort_alphavit]]</a>
        
      </div>
    </div>

	{assign var=params value=[
		"element"=>'msProducts'
        ,"where"=>[
			"class_key"=>"msProduct"
		]
        ,"filters"=>'ms|price_kg,ms|vendor:vendors,ms|adiscount,ms|askidka,ms|helloween,ms|black_friday,ms|markdown'
		,"includeTVs"=>'0'
		,"includeMS"=>'1'
		,"includeMSList"=>'price,new,toorder,favorite,popular,size,color'
        ,"paginator"=>'getPage'
        ,"tplOuter"=>'filterTpl'
        ,"tplFilter.outer.default"=>'filterGroupTpl'
        ,"tplFilter.row.default"=>'filterRowTpl'
        ,"tplFilter.outer.ms|price"=>'filterPriceOuterTpl'
		,"tplFilter.outer.ms|price_kg"=>'filterPriceOuterTpl'
        ,"tplFilter.row.ms|price"=>'filterPriceTpl'
		,"tplFilter.row.ms|price_kg"=>'filterPriceTpl'
        ,"tplFilter.outer.tv|product_options"=>'filterPriceOuterTpl'
        ,"tplFilter.row.tv|product_options"=>'filterPriceTpl'
        ,"tplFilter.outer.ms|vendor"=>'filterVendorOuterTpl'
        ,"tpls"=>'productRowTpl'
        ,"limit"=>"24"
					]}

	{snippet name=mFilter2 params=$params}



    {assign var=pars value=[
		"element"=>'msProducts'
        ,"where"=>[
			"class_key"=>"msProduct"
		]
        ,"filters"=>'ms|price_kg,ms|vendor:vendors,ms|adiscount,ms|askidka,ms|helloween,ms|black_friday, ms|markdown'
		,"includeTVs"=>'0'
		,"includeMS"=>'1'
		,"includeMSList"=>'price,new,toorder,favorite,popular,size,color'
        ,"paginator"=>'getPage'
        ,"tplOuter"=>'filterTpl'
        ,"tplFilter.outer.default"=>'filterGroupTpl'
        ,"tplFilter.row.default"=>'filterRowTpl'
        ,"tplFilter.outer.ms|price"=>'filterPriceOuterTpl'
		,"tplFilter.outer.ms|price_kg"=>'filterPriceOuterTpl'
        ,"tplFilter.row.ms|price"=>'filterPriceTpl'
		,"tplFilter.row.ms|price_kg"=>'filterPriceTpl'
        ,"tplFilter.outer.tv|product_options"=>'filterPriceOuterTpl'
        ,"tplFilter.row.tv|product_options"=>'filterPriceTpl'
        ,"tplFilter.outer.ms|vendor"=>'filterVendorOuterTpl'
        ,"tpls"=>'productRowTpl'
        ,"limit"=>"24"
					]}


{snippet name=metaprevnext  params=$pars}
{literal}
{/literal}

	
  </div>{literal}


{/literal}

{literal}

{/literal}
	  
	   {/block}