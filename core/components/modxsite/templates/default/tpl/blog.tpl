{extends file="layout.tpl"}

{block name=content}
  <div class="container">    
{snippet name=BreadCrumb params="scheme=`full`&containerTpl=`breadcrumbOuterTpl`&currentCrumbTpl=`breadcrumbCurrentTpl`&linkCrumbTpl=`breadcrumbLinkTpl`&homeCrumbTpl=`breadcrumbLinkTpl`"}
    <div class="row">
        <div class="col-xs-9">
		{literal}       
			[[!pdoPage?
				&parents=`[[*id]]`            
				&ajax=0
				&tpl=`blogRowTpl`
				&scheme=`full`
				&includeContent=`1`
				&includeTVs=`articlefoto`
				&processTVs=`1`
				
			]]
		{/literal}     
		<br style="clear:both;"/>
			[[!+page.nav]]
        </div>
        [[!getsidebar]]
    </div>
  </div>
	  {/block}
