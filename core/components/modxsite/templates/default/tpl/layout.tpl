<!doctype html>
<html class="no-js" prefix="og: https://ogp.me/ns#">
<head>

    <!-- Google Tag Manager -->
    <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
        new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
                j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
                'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
                })(window,document,'script','dataLayer','GTM-W5VBDZ');</script>
    <!-- End Google Tag Manager -->
    <base href="https://petchoice.ua/">
    <meta charset="utf-8">
    <title>{snippet name=metatitle params="title=`{field name=pagetitle}`&seotitle=`{field name=longtitle}`"}</title>
    <meta name="description" content="{snippet name=metadescription params="description=`{field name=description}`"}">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- Place favicon.ico and apple-touch-icon.png in the root directory -->
    <link rel="stylesheet" href="/assets/styles/main_v_3.css">

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script> <!-- 33 KB -->

    <script src="/assets/fancybox/jquery.fancybox.js"></script>
    <link href="/assets/fancybox/jquery.fancybox.css" rel="stylesheet"/>
  
<!--<script src='https://www.google.com/recaptcha/api.js'></script>-->
    {literal}
    <!--<script>
        (function (i, s, o, g, r, a, m) {
            i['GoogleAnalyticsObject'] = r;
            i[r] = i[r] || function () {
                    (i[r].q = i[r].q || []).push(arguments)
                }, i[r].l = 1 * new Date();
            a = s.createElement(o),
                m = s.getElementsByTagName(o)[0];
            a.async = 1;
            a.src = g;
            m.parentNode.insertBefore(a, m)
        })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');

        ga('create', 'UA-67805669-1', 'auto', {'allowLinker': true});

        ga('require', 'linker');

        {/literal}
        {if ($type=='product')}
        {literal}
        ga('set', 'dimension3', '{/literal}{field name=id}{literal}');
        ga('set', 'dimension4', "offerdetail");
        ga('set', 'dimension5', [[+price]]);
        {/literal}
        {/if}
        {if ($type=='home')}
        {literal}
        ga('set', 'dimension3', '');
        ga('set', 'dimension4', "home");
        ga('set', 'dimension5', "");
        {/literal}
        {/if}
        {if ($type=='cart')}
        {literal}
        ga('set', 'dimension3', [[+idsproduct]]);
        ga('set', 'dimension4', "cart");
        ga('set', 'dimension5', "[[+cart_cost]]");
        {/literal}
        {/if}
        {if ($typemarketing=='successorder')}
        {literal}
        ga('set', 'dimension3', [[+idsproduct]]);
        ga('set', 'dimension4', "purchase");
        ga('set', 'dimension5', "[[+cart_cost]]");
        {/literal}
        {/if}
        {if ($type=='other')}
        {literal}
        ga('set', 'dimension3', '');
        ga('set', 'dimension4', "other");
        ga('set', 'dimension5', '');
        {/literal}
        {/if}
        {literal}
        ga('linker:autoLink', ['m.petchoice.ua']);
        ga('send', 'pageview');
    </script>-->
    {/literal}
    {*
    {snippet name=getcanonical}
    {snippet name=robots}
    *}
    {field name=type assign=typedoc}

    {if ($type=='product')}
        <meta property="og:title" content="{field name=pagetitle}"/>
        <meta property="og:type" content="product"/>
        <meta property="og:image" content="https://petchoice.ua/{snippet name=msGallery params="limit=1&main=1"}"/>
        <meta property="og:url" content="{link id={field name=id}}"/>
        <meta property="og:description" content="{field name=introtext}"/>
    {/if}

    {if ($type=='blog')}
        <meta property="og:title" content="{field name=pagetitle}"/>
        <meta property="og:type" content="article"/>
        <meta property="og:image" content="https://petchoice.ua/{field name=image}"/>
        <meta property="og:url" content="/{link id={field name=id}}"/>
        <meta property="og:description" content="{field name=description}"/>
    {/if}


</head>
<body>    {literal}

    <!-- Google Tag Manager (noscript) -->
    <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-W5VBDZ";
                      height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
    <!-- End Google Tag Manager (noscript) -->

    <script type="text/javascript">
        function setCookie(name, value, options) {
            options = options || {};
            var expires = options.expires;

            if (typeof expires == "number" && expires) {
                var d = new Date();
                d.setTime(d.getTime() + expires * 1000);
                expires = options.expires = d;
            }
            if (expires && expires.toUTCString) {
                options.expires = expires.toUTCString();
            }

            value = encodeURIComponent(value);

            var updatedCookie = name + "=" + value;

            for (var propName in options) {
                updatedCookie += "; " + propName;
                var propValue = options[propName];
                if (propValue !== true) {
                    updatedCookie += "=" + propValue;
                }
            }

            document.cookie = updatedCookie;
        }

        // возвращает cookie с именем name, если есть, если нет, то undefined
        function getCookie(name) {
            var matches = document.cookie.match(new RegExp(
                "(?:^|; )" + name.replace(/([\.$?*|{}\(\)\[\]\\\/\+^])/g, '\\$1') + "=([^;]*)"
            ));
            return matches ? decodeURIComponent(matches[1]) : undefined;
        }
    </script>
{/literal}
{snippet name=getmessage}
<!-- Add your site or application content here -->
<div class="wrapper">
    <div class="header">
        <div class="basket-header" style="z-index:99999!important;">
            <div class="container">
                <div class="row">
                    <div class="col-xs-7">
                        <a href="{link id=473}">Доставка и оплата</a>
                        {if (!$modx->user->isAuthenticated())}
                            <a href="/account/register">Войти / Зарегистрироваться</a>
                            <span class="small"><i>(скидки до 10%)</i></span>
                        {else}
                            {nocache}{block name=login nocache}{snippet name=login params="tplType=modChunk&loginTpl=headNotLoggedInTpl&logoutTpl=headLoggedInTpl"}{/block}{/nocache}
                        {/if}
                    </div>

                    <div id="msMiniCart" class="col-xs-5 text-right">
                        {nocache}{block name=minicart nocache}{snippet name=msMiniCart params="tpl=miniCartTpl"}{/block}{/nocache}

                    </div>
                </div>
            </div>
        </div>
        <div class="main-block">
            <div class="container">
                <div class="row">
                    <div class="col-xs-3">

                        <a class="logo-link" href="{config name=site_url}">
                            <div class="hny-logo"></div>
                            <img src="/assets/images/logo.png"
                                 alt="Зомагазин PetChoice - товары для животных с доставкой"/></a>

                    </div>

                    <div class="col-xs-6">
                        <div class="search-element">


                            {assign var=paramsSearch value=[
                            "element"=>"=msProducts"
                            ,"fields"=>"Data.article:3,Data.vendor:2"
                            ,"where"=>[
                            "Data.price:>"=>0
                            ]
                            ]}
                            {snippet as_tag=1 name=mSearchForm params=$paramsSearch}

                        </div>
                    </div>

                    <div class="col-xs-3 text-right">
                        <div class="phone-part">
                            <div class="phone-line">
                                <span class="icon phone-icon"></span>
                                <span class="big">048 700-50-80</span>
                                <div class="arrow-block dropdown">
                                    <a class="icon arrow-phone" id="PhoneList" data-target="#" href=""
                                       data-toggle="dropdown" aria-haspopup="true" role="button"
                                       aria-expanded="false"></a>
                                    <div class="dropdown-menu" role="menu" aria-labelledby="PhoneList">
                                        <span class="dropdown-arrow"></span>
                                        <p>
                                            <span class="icon life-icon"></span>
                                            063 505-50-03
                                        </p>
                                        <p>
                                            <span class="icon kyivstar-icon"></span>
                                            067 841-64-64
                                        </p>
                                        <p>
                                            <span class="icon mts-icon"></span>
                                            095 505-65-95
                                        </p>
                                    </div>
                                </div>
                            </div>
                            <div class="dropdown callback">
                                <a id="CallBack" data-target="#" href="" data-toggle="dropdown" aria-haspopup="true"
                                   role="button" aria-expanded="false">Заказать обратный звонок11</a>
                                <div class="dropdown-menu" role="menu" aria-labelledby="CallBack">
                                    <span class="dropdown-arrow"></span>portnovvit@gmail.com
                                    {assign var=paramsAjaxForm value=[
                                    "snippet"=>"FormIt"
                                    ,"form"=>"callFormTpl"
                                    ,"hooks"=>"email"
                                    ,"emailSubject"=>"Получен заказ обратного звонка"
                                    ,"emailTo"=>"portnovvit@gmail.com"
                                    ,"validate"=>"name:required,phone:required"
                                    ,"validationErrorMessage"=>"В форме содержатся ошибки!"
                                    ,"successMessage"=>"Сообщение успешно отправлено"
                                    ]}

                                    {snippet as_tag=1 name=AjaxForm params=$paramsAjaxForm}
<!--{config name=emailsender},-->
                                </div>
                            </div>
                            <div class="times small">
                                <p>Пн-Пт: 9:00 - 18:00</p>
                                <p>Суббота: 9:00 - 12:00</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="edge-block">
            <div class="container">
                <div class="row">
                    <div class="col-xs-3">
                        <p class="bordered red">Скидки до 10%</p>
                        <div class="bordered grey small">
                            <a href="/account/register" class="none-border">Зарегистрируйтесь</a> и получите скидку 5%
                            на первую покупку
                        </div>
                    </div>

                    <div class="col-xs-3">
                        <p class="bordered green">Доставка по Одессе и Украине</p>
                        <div class="bordered grey small">
                            Бесплатная доставка от 299 грн (Одесса) и 699 грн (Украина)
                        </div>
                    </div>

                    <div class="col-xs-3">
                        <p class="bordered grey-blue">Нам доверяют</p>
                        <div class="bordered grey small">
                            Ценим мнение наших клиентов. Читайте их отзывы <a href="/account/register"
                                                                              class="none-border"
                                                                              target="_blank">здесь</a>
                        </div>
                    </div>

                    <div class="col-xs-3">
                        <p class="bordered blue">Официально</p>
                        <div class="bordered grey small">
                            Сертифицированные товары по выгодным ценам
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="main-menu">
            <div class="container">
                <div class="row">

                    {assign var=params value=[
                    "startId"       => 10
                    ,"level"        => 2
                    ,"sortBy"       => "menuindex"
                    ,"levelClass"   => "level"
                    ,"cacheable"    =>false
                    ,"id"           => "catalog"
                    ]}
                    {processor action="site/web/menu/getcatalogmenu" ns="modxsite" params=$params assign=result}
                    {assign var=items value=$result.object}
                    {include file='tpl/menu/menumain.tpl'}

                </div>
            </div>
        </div>
    </div>
    {* если главная *}
    {if ($modx->resource->id==1)}
        {block name=mainpage}{/block}
    {else}
        {block name=content}{/block}

    {/if}


    <div class="footer">
        <div class="container">
            <div class="row">
                <div class="col-xs-3">
                    <ul class="footer-menu list-unstyled">
                        <li class="footer-menu-item first"><a href="https://petchoice.ua/contacts">Контакты</a></li>
                        <li class="footer-menu-item"><a href="https://petchoice.ua/usloviya-dostavki-i-oplatyi">Условия
                                доставки и оплаты</a></li>
                        <li class="footer-menu-item"><a href="https://petchoice.ua/o-magazine">О магазине</a></li>
                        <li class="footer-menu-item"><a href="https://petchoice.ua/veterinarnyie-kliniki">Ветеринарные
                                клиники</a></li>
                        <li class="footer-menu-item last"><a href="https://petchoice.ua/blog">Блог</a></li>
                        <li class="footer-menu-item"><a href="https://m.petchoice.ua/?type=mobile">Мобильная версия</a>
                        </li>
                    </ul>
                </div>
                <div class="col-xs-3 follow-footer">
                    <p><b>Подписаться на рассылку</b></p>

                    <form action="/assets/components/minishop2/subscribe.php" class="formsubscribe" method="post"
                          data-toggle="validator" role="form">
                        <div class="form-group form-line">
                            <span class="inpt-icon name-inpt"></span>
                            <input class="form-control custom-inpt" name="name" type="text" placeholder="Имя"
                                   data-error="введите ваше имя" required>
                            <div class="help-block with-errors">введите ваше имя</div>
                        </div>
                        <div class="form-group form-line">
                            <span class="inpt-icon email-inpt"></span>
                            <input class="form-control custom-inpt" type="email" name="email"
                                   data-error="введите ваш email" placeholder="E-mail адрес" required>
                            <div class="help-block with-errors">введите ваш email</div>
                        </div>
                        <a class="why-follow small" data-toggle="popover" data-popover-content="#follow-popover"
                           data-placement="top">Зачем мне подписываться ?</a>
                        <button type="submit" class="btn grey-btn pull-right" id="buttonsubscribe" data-toggle="modal"
                                data-target="#thanksCallModalLabel">Подписаться
                        </button>
                    </form>
                    <div class="messagesubscribe"></div>


                </div>

                <div class="col-xs-3">
                    <div class="pay-info">
                        <p><b>Способы оплаты:</b></p>
                        <div class="pay-systems">
                            <span class="icon visa">visa</span>
                            <span class="icon mastercard">mastercard</span>
                            <span class="icon privat24">privat24</span>
                            <span class="icon global">cash</span>
                        </div>
                        <a class="small" href="#" data-toggle="modal" data-target="#payModal">Оплатить заказ</a>
                    </div>
                    <div class="delivery-info">
                        <p><b>Способы доставки:</b></p>
                        <p class="small">Новая Почта, Ин-тайм, курьерская доставка до двери</p>
                    </div>
                </div>

                <div class="col-xs-3 text-right cat-part">
                    <div class="phone-part">
                        <div class="phone-line">
                            <span class="icon phone-icon"></span>
                            <span class="big">048 700-50-80</span>
                        </div>

                        <p>
                            <span class="icon life-icon"></span>
                            063 505-50-03
                        </p>
                        <p>
                            <span class="icon kyivstar-icon"></span>
                            067 841-64-64
                        </p>
                        <p>
                            <span class="icon mts-icon"></span>
                            095 505-65-95
                        </p>
                    </div>


                    <div class="times small">
                        <p>Пн-Пт: 9:00 - 18:00</p>
                        <p>Суббота: 9:00 - 12:00</p>
                    </div>

                    <span class="footer-cat"></span>
                </div>
            </div>
        </div>

        <div class="undo-footer">
            <div class="container">
                <div class="row">
                    <div class="col-xs-8 copyright">
                        &copy; 2016 Интернет-магазин зоотоваров PetChoice
                    </div>

                    <div class="col-xs-4 text-right">
                        <a class="icon vk-icon" target="_blank" href="//vk.com/petchoice">vk</a>
                        <a class="icon fb-icon" target="_blank" href="//www.facebook.com/petchoice.com.ua">fb</a>
                        <!-- not need now <a class="icon ytub-icon" href="#">youtube</a>-->
                    </div>
                </div>
            </div>
        </div>
    </div>


    <!-- follow popover -->
    <div id="follow-popover" class="hidden">
        <div class="popover-body">
            <p><b>Что даёт подписка на рассылку?</b></p>
            <ul class="list-unstyled green-bullet">
                <li>
                    Информация о акциях и скидках
                </li>
                <li>
                    Постоянно обновляемые индивидуальные выгодные предложения
                </li>
                <li>
                    Последние новости о выгодных покупках
                </li>
            </ul>
        </div>
    </div>
    <!-- end follow popover -->


    {if (($type=='other')||($type=='product'))}
        <div class="modal fade" id="boxStatus" tabindex="-1" role="dialog" aria-labelledby="boxStatusModalLabel"
             aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"></button>
                        <h3 class="modal-title greener"><b>Сообщить о наличии 111</b></h3>
                    </div>
                    {snippet name=AjaxForm params="snippet=`ask_stock`&form=`FormAsk.tpl`&hooks=`recaptchav2,email`&emailSubject=`Тестовое сообщение`&emailTo=`portnovvit@gmail.com`&validate=`name:required,g-recaptcha-response:required,phone:required`&validationErrorMessage=`В форме содержатся ошибки!`&successMessage=`Сообщение успешно отправлено`"}

                </div>
            </div>
        </div>
    {/if}

    {if (!$modx->user->isAuthenticated())}
        <div class="modal fade" id="boxlogin" tabindex="-1" role="dialog" aria-labelledby="boxloginModalLabel"
             aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"></button>
                        <h3 class="modal-title greener"><b>Авторизация</b></h3>
                    </div>

                    <form action="/account/login" method="post" class="ajaxloginbox" data-toggle="validator"
                          role="form">
                        <div class="modal-body">

                            <div class="form-group ">
                                <label class="control-label">Телефон</label>
                                <input name="username" type="text" class="form-control custom-inpt" placeholder="+38"
                                       value="+38" required>
                                <div class="help-block small grey">
                                    Укажите номер телефона в формате +380ХХХХХХХХХ без пробелов, дефисов и скобок
                                </div>
                                <div class="help-block with-errors"></div>
                            </div>
                            <div class="form-group">
                                <label class="control-label">Пароль</label>
                                <input name="password" type="password" class="form-control custom-inpt" required>
                                <div class="help-block">
                                    <a href="" data-toggle="modal" data-dismiss="modal"
                                       data-target="#forgotPasswordModal">Забыли пароль?</a>
                                </div>
                            </div>

                        </div>
                        <div class="modal-footer">
                            <input class="returnUrl" type="hidden" name="returnUrl"
                                   value="https://petchoice.ua/account/"/>
                            <input class="loginLoginValue" type="hidden" name="service" value="login"/>
                            <br>
                            <button type="submit" class="btn orange-btn">ВОЙТИ</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <!-- Forgot Password Modal -->
        <div class="modal fade" id="forgotPasswordModal" tabindex="-1" role="dialog" aria-labelledby="basketModalLabel"
             aria-hidden="true">
            <div class="modal-dialog">
                <div id="office-auth-form" class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"></button>
                        <h3 class="modal-title greener"><b>Восстановление пароля</b></h3>
                    </div>
                    <!--<form id="office-auth-login" method="post" data-toggle="validator" role="form">
                        -->
                    <div class="modal-body">

                        {snippet name=officeAuth params="tplLogin=`forgotPasswordFormTpl`&HybridAuth=`0`"}
                    </div>
                    <!--</form>-->
                </div>
            </div>
            <!-- end Forgot Password Modal -->
        </div>
        <!--
 <div class="modal fade" id="recoverySuccessModal" tabindex="-1" role="dialog" aria-labelledby="thanksCallModalLabel" aria-hidden="true">
             <div class="modal-dialog">
               <div class="modal-content">
                 <div class="modal-header">
                   <button type="button" class="close" data-dismiss="modal" aria-label="Close"></button>
                   <h3 class="modal-title greener" id="thanksCallModalLabel"></h3>
                 </div>
                 <div class="modal-body text-center">
                   <br><br>
                   <h3 class="modal-title greener"><b>Спасибо</b></h3>
                   <br>
                   <div class="thanks-text">На Ваш e-mail отправлено письмо с инструкцией по восстановлению пароля</div>
                   <br><br>
                 </div>
                 <div class="modal-footer">
                 </div>
               </div>
             </div>
           </div>
      -->
        <div class="modal fade" id="boxregistr" tabindex="-1" role="dialog" aria-labelledby="boxregistrModalLabel"
             aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"></button>
                        <h3 class="modal-title greener"><b>Регистрация</b></h3>
                    </div>


                    <form action="/account/register" class="ajaxregistrbox" method="post" data-toggle="validator"
                          role="form">
                        <div class="modal-body">
                            <div class="form-group ">
                                <label class="control-label">Имя <sup>*</sup></label>
                                <input name="fullname" type="text" class="form-control custom-inpt" value=""
                                       placeholder="Иван" required>
                                <div class="help-block with-errors"></div>
                            </div>
                            <div class="form-group">
                                <label class="control-label">Фамилия</label>
                                <input name="lastname" type="text" class="form-control custom-inpt" value=""
                                       placeholder="Иванов">
                            </div>
                            <div class="form-group ">
                                <label class="control-label">Телефон <sup>*</sup></label>
                                <input type="text" name="phone" class="form-control custom-inpt" placeholder="+38"
                                       value="+38" required>
                                <div class="help-block small grey">
                                    <ul class="list-unstyled">
                                        <li>Будет использован как логин к Личному кабинету</li>
                                        <li>Укажите номер телефона в формате +380ХХХХХХХХХ без пробелов, дефисов и
                                            скобок
                                        </li>
                                    </ul>
                                </div>
                                <div class="help-block with-errors"></div>
                            </div>
                            <div class="form-group ">
                                <label class="control-label">Email <sup>*</sup></label>
                                <input name="email" type="email" class="form-control custom-inpt" value="" required>
                                <div class="help-block small grey">Никакого спама, обещаем</div>
                                <div class="help-block with-errors"></div>
                            </div>
                            <div class="form-group ">
                                <label class="control-label">Пароль <sup>*</sup></label>
                                <input id="password" name="password" type="password" class="form-control custom-inpt"
                                       value="" required>
                                <div class="help-block small grey">Не менее 6 символов</div>
                                <div class="help-block with-errors"></div>
                            </div>
                            <div class="form-group ">
                                <label class="control-label">Подтвердите пароль <sup>*</sup></label>
                                <input name="password_confirm" type="password" class="form-control custom-inpt" value=""
                                       required>
                                <div class="help-block with-errors"></div>
                            </div>


                        </div>
                        <div class="modal-footer">
                            <input type="hidden" name="registerbtn" value="111"/>
                            <input name="discount" type="hidden" value="5">

                            <button class="btn orange-btn" type="submit" value="account/register" name="registerbtn1"
                                    style="pointer-events: all; cursor: pointer;">Зарегистрироваться
                            </button>
                            <p class="form-control-static inline grey">
                                <sup>*</sup> - поля обязательны для заполнения
                            </p>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    {/if}

    <!-- Pay Modal -->
    <div class="modal fade" id="payModal" tabindex="-1" role="dialog" aria-labelledby="payModalLabel"
         aria-hidden="true">
        <form id="getorder" class="form-horizontal" method="get">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"></button>
                        <h3 class="modal-title greener" id="paytModalLabel"><b>Оплатить заказ</b></h3>
                    </div>
                    <div class="modal-body">
                        <div class="form-group number-pay">
                            <label id="payInpt">Введите номер заказа</label>
                            <input name="msorder" type="text" class="form-control" value="" required/>
                            <span class="icon green-check-icon hidden"></span>
                            <span class="icon red-check-icon hidden"></span>
                        </div>

                        <div class="errorliqpay hide help-block with-errors">
                        </div>

                    </div>
                    <div class="modal-footer hidden">
                        <div class="pull-left">
                            <p class="h3 name"><b></b></p>

                            <ul class="list-inline">
                                <li>
                                    <b class="grey inline" style="vertical-align: bottom;">
                                        <span class="h3">К оплате:</span>
                                    </b>
                                </li>
                                <li>
                    <span class="price-large inline" style="vertical-align: bottom;">
                   <span class="product-price">0 <span class="small text-top">00</span></span>
                    </span>
                                </li>
                            </ul>
                            <ul class="list-inline">
                                <li>
                                    <b class="grey inline" style="vertical-align: bottom;">
                                        <span class="h3">Способ оплаты:</span>
                                    </b>
                                </li>
                                <li>
                                    <span class="payment"></span>
                                </li>
                            </ul>

                            <ul class="list-inline">
                                <li>
                                    <b class="grey inline" style="vertical-align: bottom;">
                                        <span class="h3">Способ доставки:</span>
                                    </b>
                                </li>
                                <li>
                                    <span class="deliveryorderfooter"></span>
                                </li>
                            </ul>
                        </div>

                        <button type="submit" name="ms2_action"
                                class="btn orange-btn text-uppercase pull-right payorder" data-value="">Оплатить
                        </button>
                    </div>
                </div>
            </div>
        </form>
    </div>
    <!-- end Pay Modal -->


    {if (!$modx->user->isAuthenticated())}
        <div class="modal fade" id="subscribeSuccessModal" tabindex="-1" role="dialog"
             aria-labelledby="thanksCallModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"></button>
                        <h3 class="modal-title greener" id="thanksCallModalLabel"></h3>
                        <div class="modal-body text-center">
                            <br><br>
                            <h3 class="modal-title greener"><b>Спасибо</b></h3>
                            <br>
                            <div class="thanks-text">На Ваш e-mail отправлено письмо с инструкцией по восстановлению
                                пароля
                            </div>
                            <br><br>
                        </div>
                        <div class="modal-footer">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    {/if}


</div>


{literal}
    <script>
        jQuery(document).ready(function () {

            //jQuery('.fancybox').fancybox();
        });

    </script>
    <script async type="text/javascript"
            src="/assets/components/msearch2/js/web/lib/jquery-ui-1.10.4.custom.min.js"></script>
{/literal}
<!--[if lt IE 9]>
<script src="/assets/scripts/oldieshim.js"></script>
<![endif]-->

<link href="https://cdnjs.cloudflare.com/ajax/libs/fotorama/4.6.4/fotorama.css" rel="stylesheet">
<link rel="stylesheet" href="/assets/components/msearch2/css/web/redmond/jquery-ui-1.10.4.custom.min.css">
<link rel="stylesheet" href="/assets/styles/custom.css">


<script src="/assets/scripts/vendor.js"></script>


<script src="https://cdnjs.cloudflare.com/ajax/libs/fotorama/4.6.4/fotorama.js"></script> <!-- 16 KB -->
<!--    <script src="/assets/scripts/scripts.js"></script>
    <script src="/assets/components/msearch/js/mfilter.js" type="text/javascript"></script>-->
{literal}<!--Render time: [^t^] <br/>
     Запросы к базе [^q^]<br/>
     Время запросов [^qt^]-->

{/literal}
<!--<script async type="text/javascript" src="/assets/components/msearch2/js/web/lib/jquery-ui-1.10.4.custom.min.js"></script>
  <script type="text/javascript" src="/assets/components/minishop2/js/web/default.js"></script>
<script type="text/javascript" src="/assets/components/msearch2/js/web/default.js"></script>-->

</body>
</html>
