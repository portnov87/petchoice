{extends file="layout.tpl"}

{block name=content}
{literal}
[[!loggedinRedirect]]
      <div class="container">
          <div class="page-discount">
            [[BreadCrumb? 
                &scheme=`full` 
                &containerTpl=`breadcrumbOuterTpl` 
                &currentCrumbTpl=`breadcrumbCurrentTpl` 
                &linkCrumbTpl=`breadcrumbLinkTpl` 
                &homeCrumbTpl=`breadcrumbLinkTpl`
            ]]
            <br><br>
            <div class="row equal-container">
                <div class="col-xs-6 equal-column green-separator">
                    <div class="">
                      [[!Login? 
                          &loginTpl=`loginFormTpl` 
                          &logoutTpl=`logoutTpl`
                          &errTpl=`lgnErrTpl`
                          &loginResourceId=`7`
                      ]]
                      
                        [[!officeAuth?
                        	tplLogin=`forgotPasswordFormTpl`
                        	&HybridAuth=`0`
                        ]]

                    </div>
                    <br>
                </div>
                 <div class="col-xs-6 equal-column blue-separator">
                    <div class="h1">Дисконтная программа</div>
                    <p>Зарегистрируйтесь на сайте и получите скидку 5% уже на первый заказ.</p>
                    <p>А также:</p>
                    <ul class="list-unstyled green-bullet">
                        <li>следите за размером скидки и стоимостью покупок в личном кабинете</li>
    		<li>получайте бонусы</li>
                        <li>оставляйте отзывы о товарах</li>
                        </ul>
                    <div class="h3"><b class="green">Условия  программы</b></div>
                    <p>Скидка по программе лояльности не суммируется с остальными скидками на сайте (если это не указано отдельно)</p>
                    <p><em>На товары ТМ Клуб 4 лапы, DogChow, CatChow, Optimeal и древесные наполнители может устанавливаться скидка не более 5%</em></p>
                    <br>
                    <div class="row">
                      <div class="col-xs-6 discount-options">
                          <!--<div class="row">
                            <div class="col-xs-5">
                                <div class="narrow-stroke">3%</div>
                            </div>
                            <div class="col-xs-7">
                                <span class="h3 grey">Регистрация</span>
                            </div>
                          </div>-->
                          <div class="row">
                            <div class="col-xs-5">
                                <div class="narrow-stroke theme-purple">5%</div>
                            </div>
                            <div class="col-xs-7">
                                <span class="h3 grey">Регистрация</span>
                            </div>
                          </div>
                          <div class="row">
                            <div class="col-xs-5">
                                <div class="narrow-stroke theme-turquoise">7%</div>
                            </div>
                            <div class="col-xs-7">
                                <span class="h2 grey">5 000 <small class="grey">грн</small></span>
                            </div>
                          </div>
                          <div class="row">
                            <div class="col-xs-5">
                                <div class="narrow-stroke theme-orange">10%</div>
                            </div>
                            <div class="col-xs-7">
                                <span class="h2 grey">10 000 <small class="grey">грн</small></span>
                            </div>
                          </div>
                      </div>
                      <div class="col-xs-6">
                        <img src="/assets/images/puppy_5.png" alt="">
                      </div>
                    </div>
                </div>
            </div>
          </div>
      </div>
        <div class="modal fade" id="recoverySuccessModal" tabindex="-1" role="dialog" aria-labelledby="thanksCallModalLabel" aria-hidden="true">
            <div class="modal-dialog">
              <div class="modal-content">
                <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close"></button>
                  <h3 class="modal-title greener" id="thanksCallModalLabel"></h3>
                </div>
                <div class="modal-body text-center">
                  <br><br>
                  <h3 class="modal-title greener"><b>Спасибо</b></h3>
                  <br>
                  <div class="thanks-text">На Ваш e-mail отправлено письмо с инструкцией по восстановлению пароля</div>
                  <br><br>
                </div>
                <div class="modal-footer">
                </div>
              </div>
            </div>
          </div>
		{/literal}
{/block}