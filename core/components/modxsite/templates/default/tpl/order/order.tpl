
<!doctype html>
<html class="no-js">
  <head>

    <script>

        {if (!$modx->user->isAuthenticated())}
        registration='guest';
        {else}
          {if ($modx->user->username=='admin')}

          registration='manager';
          {elseif (($modx->user->isMember('manager5'))||($modx->user->isMember('manager2'))||($modx->user->isMember('manager3'))||( $modx->user->isMember('manager')))}

          registration='manager';

          {else}
          registration='user';
          {/if}

        {/if}


        {if ($type=='product')}
        {literal}

        dataLayer=[{
            'dynx_itemid': '{/literal}{field name=id}{literal}',
            'dynx_pagetype': 'offerdetail',
            'dynx_totalvalue': {/literal}[[+price]]{literal},
            'registration':registration
        }];

        {/literal}
        {/if}
        {if ($type=='home')}
        {literal}
        dataLayer=[{
            'dynx_itemid': '',
            'dynx_pagetype': 'home',
            'dynx_totalvalue': 0,
            'registration':registration
        }];
        {/literal}
        {/if}
        {if ($type=='cart')}
        {literal}
        dataLayer=[{
            'dynx_itemid': '',
            'dynx_pagetype': 'conversionintent',
            'dynx_totalvalue': 0,
            'registration':registration
        }];
        {/literal}
        {/if}
        {if ($typemarketing=='successorder')}
        {literal}
        dataLayer=[{
            'dynx_itemid': '',
            'dynx_pagetype': 'conversion',
            'dynx_totalvalue': "{/literal}[[+cart_cost]]{literal}",
            'registration':registration
        }];
        {/literal}
        {/if}
        {if ($type=='other')}
        {literal}
        dataLayer=[{
            'dynx_itemid': '',
            'dynx_pagetype': 'other',
            'dynx_totalvalue': 0,
            'registration':registration
        }];
        {/literal}
        {/if}
        {literal}
    </script>
    <!-- Google Tag Manager -->
    <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
            new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
            j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
            'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
        })(window,document,'script','dataLayer','GTM-W5VBDZ');</script>
    <!-- End Google Tag Manager -->
    {/literal}
    <meta charset="utf-8">
    <title>[[*pagetitle]]</title>
    <meta name="description" content="[[*description]]">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    <!-- Place favicon.ico and apple-touch-icon.png in the root directory -->
    <link rel="stylesheet" href="/assets/styles/main.css">
    <!-- vk widget -->
  
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script> 
                
  <script src="//cdnjs.cloudflare.com/ajax/libs/modernizr/2.6.2/modernizr.min.js"></script>
    <!--<script src="//cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/2.3.1/js/bootstrap.min.js"></script>-->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>









    <script src="/assets/scripts/jquery.bxslider.js"></script>
<script src="/assets/scripts/jquery.barrating.js"></script>

    <!-- vk widget -->
    <!--<script type="text/javascript" src="//vk.com/js/api/openapi.js?116"></script>-->
    [[!getcanonical]]
    [[!msGetOrder?idsuccess=`[[+GET.msorder]]`]]
  </head>
  <body>
  {literal}
  <!-- Google Tag Manager (noscript) -->
  <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-W5VBDZ";
                    height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
  <!-- End Google Tag Manager (noscript) -->
  {/literal}


  {literal}

    <script>
      !function (t, e, c, n) {
        var s = e.createElement(c);
        s.async = 1, s.src = 'https://statics.esputnik.com/scripts/' + n + '.js';
        var r = e.scripts[0];
        r.parentNode.insertBefore(s, r);
        var f = function () {
          f.c(arguments);
        };
        f.q = [];
        f.c = function () {
          f.q.push(arguments);
        };
        t['eS'] = t['eS'] || f;
      }(window, document, 'script', '9AD2834E688B486E93DCDF7D7D0B4C20');
    </script><script>eS('init');</script>

    <!--[if lt IE 7]>
      <p class="browsehappy">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
    <![endif]-->

    <!-- fb widget -->
    <div id="fb-root"></div>
    <script>(function(d, s, id) {
      var js, fjs = d.getElementsByTagName(s)[0];
      if (d.getElementById(id)) return;
      js = d.createElement(s); js.id = id;
      js.src = "//connect.facebook.net/ru_RU/sdk.js#xfbml=1&version=v2.0";
      fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));</script>
    <!-- end fb -->
    {/literal}



{literal}
    <!-- Add your site or application content here -->
    <div class="wrapper">
      <div class="header goout-header">

        <div class="main-block">
          <div class="container">
            <div class="row">
              <div class="col-xs-3">
                <a class="logo-link" href="[[++site_url]]"><img src="/assets/images/logo.png" alt="[[++site_name]]" /></a>
              </div>
              <div class="col-xs-3">
                [[!PolylangLinks?
                  &'mode=`single`']]
              </div>


              <!--<a href="[[++site_url]]"><div class="hny-logo"></div></a>-->

              <div class="col-xs-3">
                <a href="[[!getreferal]]" class="back-link">Вернутся в магазин</a>
              </div>

              <div class="col-xs-3 text-right">
                <div class="phone-part">
                  <div class="phone-line">
                    <span class="icon phone-icon"></span>
                    <span class="big">048 700-50-80</span>
                    <div class="arrow-block dropdown">
                      <a class="icon arrow-phone" id="PhoneList" data-target="#" href="" data-toggle="dropdown" aria-haspopup="true" role="button" aria-expanded="false"></a>
                      <div class="dropdown-menu" role="menu" aria-labelledby="PhoneList">
                        <span class="dropdown-arrow"></span>
                        <p>
                          <span class="icon life-icon"></span>
                          063 505 50 03
                        </p>
                        <p>
                          <span class="icon kyivstar-icon"></span>
                          067 841 64 64
                        </p>
                        <p>
                          <span class="icon mts-icon"></span>
                          095 505 65 95
                        </p>
                      </div>
                    </div>
                  </div>
                  <div class="dropdown callback">
                    <a id="CallBack" data-target="#"  href=""  data-toggle="dropdown" aria-haspopup="true" role="button" aria-expanded="false">Заказать обратный звонок</a>
                    <div class="dropdown-menu" role="menu" aria-labelledby="CallBack">
                      <span class="dropdown-arrow"></span>
                      <form action="" method="" data-toggle="validator" role="form">
                        <div class="form-group form-line">
                          <span class="inpt-icon name-inpt"></span>
                          <input class="form-control custom-inpt" type="text" placeholder="Имя" required>
                        </div>
                        <div class="form-group form-line">
                          <span class="inpt-icon phone-inpt"></span>
                          <input class="form-control custom-inpt" type="text" maxlength="14" placeholder="Телефон" value="+3 80" required>
                        </div>
                        <button type="submit" class="btn grey-btn text-uppercase">перезвонить мне</button>
                      </form>
                    </div>
                  </div>
                  <div class="times small">
                    <p>Пн-Пт: 9:00 - 18:00</p>
                    <p>Суббота: 9:00 - 15:00</p>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>


      </div>
      <div class="container">
          <div class="page-basket">
            <div class="breadcrumbs">
              &nbsp;
            </div>
            [[!msOrder? 
        		&tplOuter=`orderOuterTpl` 
        		&tplEmpty=`orderEmptyTpl`
        		&tplDelivery=`deliveryTpl`
        		&tplPayment=`paymentTpl`
        		&tplSuccess=`orderSuccessTpl`
        	]]

          </div>
      </div>

      [[$footer]]

{/literal}
{*
      #<div class="col-xs-3" style="padding:0px 2px;">
       # <input type="text" placeholder="Домофон" class="custom-inpt form-control"  data-error="это поле обязательное" name="extended[doorphone]" value="[[+doorphone]]">
#      </div>
      #<div class="col-xs-3" style="padding:0px 2px;">
 #       <input type="text" style="width:75px;" placeholder="Этаж" class="custom-inpt form-control"  data-error="это поле обязательное" name="extended[floor]" value="[[+floor]]">
  #    </div>
  <a href="javascript:void(0)" style="float:right;margin-right:15px;margin-top:10px;" class="showdopkurier" >Дополнительно</a>
  <a href="javascript:void(0)" style="float:right;margin-right:15px;margin-top:10px;" class="hidedopkurier" >Свернуть</a>
*}