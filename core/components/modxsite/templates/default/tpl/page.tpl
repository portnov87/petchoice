{extends file="layout.tpl"}

{block name=content}
{literal}
     
<div class="container">
[[BreadCrumb? &scheme=`full` &containerTpl=`breadcrumbOuterTpl` &currentCrumbTpl=`breadcrumbCurrentTpl` &linkCrumbTpl=`breadcrumbLinkTpl` &homeCrumbTpl=`breadcrumbLinkTpl`]]        
    <div class="row">
		<div class="col-xs-9">
			
			{/literal}
			  <h1>{field name=pagetitle}</h1>
			  {literal}
			<br>
			<div class="row blog-content">            
              <div class="col-xs-9" style="width:100%;">
              [[*content]]
			  </div>                
            </div>
            <br>
            <br>
		</div>
		
		
		
		  <div class="col-xs-3">
          [[getSidebarPage]]
			 

			<!--<div class="follow-social">
			  
			  <div id="vk_groups"></div>
			  <script type="text/javascript">
				VK.Widgets.Group("vk_groups", {mode: 0, width: "260", height: "350", color1: 'FFFFFF', color2: '838fa3', color3: 'afc4db'}, 84542368);
			  </script>

			  <br><br>
			  
			  <div class="fb-like-box" data-href="https://www.facebook.com/petchoice.com.ua?ref=profile&amp;pnref=lhc" data-width="250" data-height="350" data-colorscheme="light" data-show-faces="true" data-header="false" data-stream="false" data-show-border="false"></div>
			</div>-->

			<div class="saw-products">
			  <p class="title-block">[[%polylang_site_main_actiontab]]</p>
				[[getactionRandom]]
			</div>
		  </div>
		
    </div>
</div>
	  {/literal}
	  {/block}
