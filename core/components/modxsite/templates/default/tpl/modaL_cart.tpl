{snippet name=msCartKey params="id=`{$product.id}`&weight=``&size=`{$weight}`&price=`{$price}`" assign=key}
<script>
    {$eSputnik}
</script>
<div class="header_cart">
    <div class="basket-cell-cart">
        <div class="basket-icon"></div>
    </div>
    <div class="count_product">
        <a href="#">{$count_text_products}</a>
    </div>
    <div class="total_amount_cart">на сумму <span>{$amount} грн.</span></div>


</div>
<div class="line_cart"></div>
<div class="product_cart">
    <div class="preview_product_cart">
        {snippet name=pthumb params="input=`{$product.image}`&options=`w=120&h=120&far=1`&useResizer=`1`" assign=preview}
        <img src="{$preview}" alt="{$product.pagetitle}" />
    </div>
    <div class="descr_product_cart">
        <div class="descr_product_cart_code">
            <span class="name_product">{$product.pagetitle} ({$weight})</span>
            <span class="sku_product">{$product.article}</span>
        </div>


        <div class="wrapper_countbox product-option" data-product-key="{$key}">
            <div class="count-box-tb">
                <div class="count-box pull-right11 ">
                    <span class="small grey">кол-во</span>
                    <a href="#" class="icon minus-count"></a>
                    <input name="sub-product-count1" data-product-type="page"
                           data-product-vendor="{$vendorname}"
                           data-product-category="{$category_name}"
                           data-product-price="{$price}"
                           data-product-name="{$product.pagetitle}"
                           data-product-size="{$weight}"
                           class="form-control custom-inpt count-products" type="text" min="1" value="{$count_product}" />
                    <a href="#" class="icon plus-count"></a>
                </div>
            </div>
            <div class="price_product">
                {$price} грн
            </div>
        </div>

    </div>


</div>
<div class="line_cart"></div>
<div class="continue_cart">
    <div class="continue_cart_td">
        <div class="goshopping"><a class="close_modal_cart" href="#">Продолжить покупки</a> <span class="icn_back"></span></div>
        <span class="text_save_product">товар сохранится в корзине</span>
    </div>
    <div class="continue_cart_td go_order_cart_td">
        <a href="/order" class="btn orange-btn btn-lg disabled" style="pointer-events: all; cursor: pointer;">Оформить заказ</a>
    </div>
</div>
<div class="line_cart"></div>

{$viewed_products}
