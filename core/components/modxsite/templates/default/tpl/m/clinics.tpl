
    {extends file="m_layout.tpl"}

{block name=content}
{literal}
[[!redirect2child]]
      <div class="container">
        <div class="page-clinics">
          [[BreadCrumb? &scheme=`full` &containerTpl=`breadcrumbOuterTpl` &currentCrumbTpl=`breadcrumbCurrentTpl` &linkCrumbTpl=`breadcrumbLinkTpl` &homeCrumbTpl=`breadcrumbLinkTpl`]]
          <div class="h1">
            [[*longtitle:default=`[[*pagetitle]]`]]
          </div>
          <br>
          [[pdoMenu?
                &parents=`476`
                &level=`1`
                &tplOuter=`@INLINE <ul class="list-inline"><li>Выберите город:</li>[[+wrapper]]</ul>`
            ]]
          <div class="map-container">
              <script type="text/javascript" charset="utf-8" src="//api-maps.yandex.ru/services/constructor/1.0/js/?sid=bsu7eaypNhZgSDk4WFlCF-wfIgCdGsJ_&width=1170&height=450"></script>
          </div>
          <br>
          <div class="row">
            [[!getImageList?
                &tvname=`clinics`
                &tpl=`clinicRowTpl`
            ]]
            </div>
            <br>
            <br>
          <div class="form-group">
              <button class="btn orange-btn" data-toggle="modal" data-target="#addAddressModal">ДОБАВИТЬ АДРЕС</button>
              <button class="btn grey-btn">СООБЩИТЬ ОБ ОШИБКЕ</button>
          </div>
        </div>
      </div>
    [[!AjaxForm?
        &snippet=`FormIt`
        &form=`addClinicFormTpl`
    	&hooks=`email`
    	&emailSubject=`[[++site_name]] | Запрос на добавление [[*parent:is=`476`:then=`новой клиники`]][[*parent:is=`556`:then=`новой клиники`]]`
    	&emailTo=`[[++emailsender]]`
    	&validate=`name:required,address:required,phone:required,work_time:required,services:required,site:required`
    	&validationErrorMessage=`В форме содержатся ошибки!`
    	&successMessage=`Сообщение успешно отправлено`
    ]]
    
      {/literal}
	  {/block}