{extends file="m_layout.tpl"}

{block name=mainpage}

    <script>
        eS('sendEvent', 'MainPage');
        </script>
{if ($modx->getOption('show_banner'))}
                {snippet name=modSliderRevolution params="slider=slider_home_mobile"}                
            {/if}
            
 <div class="wrapper_link_menu">
            <a href="javascript:void(0);" class="open_menu_catalog"><span>[[%polylang_site_common_menu_catalog]]</span></a>
        </div>
        
        
    <!--<div class="main-page-slider">
        <div class="swiper-container gallery-1">
            <div class="swiper-wrapper">-->
              <!--
              
				-->
               

                <script>
                    {literal}
                    dataLayer.push({
                        'ecommerce': {
                            'currencyCode': 'UAH',
                            'impressions': [{/literal}{$items.ecomerc}{literal}]
                        },'event': 'gtm-ee-event',
                        'gtm-ee-event-category': 'Enhanced Ecommerce',
                        'gtm-ee-event-action': 'Product Impressions',
                        'gtm-ee-event-non-interaction': 'True',
                    });
                    {/literal}
                </script>
                
                <div class="main_products wrapper_brands">
                    <div class="title_block">[[%polylang_site_label_akcii]]</div>
                        {assign var=params value=[
    					"startId"       => 10
    					,"level"        => 2
    					,"sortBy"       => "menuindex"
    					,"levelClass"   => "level"
    					,"cacheable"    =>false
                    ,'list'=>'Promotions and discounts'
    					,"where"=>[
    							"Data.adiscount"=>"1"
    						   ]
    					,"id"           => "catalog"    
    				]}
    				{processor action="site/web/products/getproducts" ns="modxsite" params=$params assign=resultaction}
    				{assign var=items value=$resultaction.object}
    				<div class="product_list">
    				{foreach $items.list as $product}
    					{include file='tpl/m/products/action.tpl'}
    				{/foreach}
                    </div>
                    <div class="show_more">
                        <a href="/discounts">[[%polylang_site_showmore]]</a>
                    </div>
                </div>
                
                 <div class="main_products ">
                    <div class="title_block">[[%polylang_site_newproducts]]</div>
                     {assign var=paramsnew value=[
                     "startId"       => 10
                     ,"level"        => 2
                     ,"sortBy"       => "menuindex"
                     ,"levelClass"   => "level"
                     ,"cacheable"    =>false
                     ,'list'=>'New arrivals'
                     ,"where"=>[
                     "Data.new"=>"1"
                     ]
                     ]}
    				{processor action="site/web/products/getproducts" ns="modxsite" params=$paramsnew assign=resultnew}
    				{assign var=itemsnew value=$resultnew.object}
    				
                    <div class="product_list">
    				{foreach $itemsnew.list as $product}
    					{include file='tpl/m/products/action.tpl'}
    				{/foreach}
                    </div>
                    
                </div>
                
                {* 
                    {assign var=paramsnew value=[
						"startId"       => 10
						,"level"        => 2
						,"sortBy"       => "menuindex"
						,"levelClass"   => "level"
						,"cacheable"    =>false
                    ,'list'=>'New arrivals'
						,"where"=>[
							"Data.new"=>"1"
						   ]						  
					]}
                    
                    
                    {processor action="site/web/products/getproducts" ns="modxsite" params=$paramsnew assign=resultaction}
    				{assign var=items value=$resultaction.object}
    				
    				{foreach $items.list as $product}
    					{include file='tpl/m/products/action.tpl'}
    				{/foreach}
                    *}
                    

                <script>
                    {literal}
                    dataLayer.push({
                        'ecommerce': {
                            'currencyCode': 'UAH',
                            'impressions': [{/literal}{$items.ecomerc}{literal}]
                        },'event': 'gtm-ee-event',
                        'gtm-ee-event-category': 'Enhanced Ecommerce',
                        'gtm-ee-event-action': 'Product Impressions',
                        'gtm-ee-event-non-interaction': 'True',
                    });
                    {/literal}
                </script>

           <!-- </div>
            <div class="swiper-button-next"></div>
            <div class="swiper-button-prev"></div>
        </div>
    </div>-->
    
    
    {snippet name="getBrands" params="tpl=tpl/m/brands/brands_home.tpl"}
    
    
    <!-- windows popup -->
    <div class="windows-popup-container product-quantity-pu">
        <div id="product-quantity-pu" class="window-popup"><span class="btn-close"></span>
            <header class="popup-header">
                <h2>[[%polylang_site_cart_qty_label]]:</h2>
            </header>
            <form class="ms2_form" method="post" onsubmit="dataLayer.push({
'event': 'event-GA',
'eventCategory' : 'product',
'eventAction' : 'buy-product-page'
});">
                <input type="hidden" name="product_id" value=""/>
                <div class="quantity">
                    <input class="input-quantity" type="number" name="countcart" value="1"/>
                    <span class="number-minus"></span>
                    <span class="number-plus"></span>
                </div>
                <div class="btn-orange">
                    <button  type="submit" data-product-vendor=""
                            data-product-category=""
                            data-product-name=""
                            data-count="1"
                            class="pay-btn orange-btn pull-right text-uppercase product add-cart">[[%polylang_site_product_button_buy]]
                    </button>
                </div>
                <input type="hidden" value="" id="idproductcart"/>
<input type="hidden" value="" id="optionId"/>
                <input type="hidden" value="" id="productprice"/>
                <input type="hidden" value="" id="productsize"/>
                <input type="hidden" value="cart/add" id="action"/>
                <input type="hidden" value="" name="key" id="keycart"/>
            </form>

        </div>
    </div>
    
{/block}