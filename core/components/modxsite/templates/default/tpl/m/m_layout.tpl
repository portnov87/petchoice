<!DOCTYPE html>
<html lang="ru">
<head>
<base href="https://m.petchoice.ua/">
{snippet name=m_getcanonical}

    <meta http-equiv="Content-Type" content="text/html;charset=UTF-8" />
    <meta http-equiv="imagetoolbar" content="no" />

    <meta name="format-detection" content="telephone=no" />
    <meta http-equiv="x-rim-auto-match" content="none" />

    <meta name="apple-mobile-web-app-capable" content="yes" />
    <meta name="viewport" content=" width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" />

    <meta charset="utf-8" />
    <title>{snippet name=metatitle params="title={field name=pagetitle}&seotitle=`{field name=longtitle}`"}</title>

    <meta name="keywords" content=" " />
        <meta name="description" content="{snippet name=metadescription params="description=`{field name=description}`"}">

    <link href="//fonts.googleapis.com/css?family=Open+Sans:300,400,700,800&subset=latin,cyrillic-ext,cyrillic" rel="stylesheet" type="text/css">
    <link href="//fonts.googleapis.com/css?family=Open+Sans+Condensed:300,700&subset=latin,cyrillic" rel="stylesheet" type="text/css">

    <link rel="stylesheet" href="/assets/mobile/css/resetcss.css" />
    <link rel="stylesheet" href="/assets/mobile/css/styles.css" />
    <link rel="stylesheet" href="/assets/mobile/css/swiper.min.css" />

    <!-- favicon -->
    <link rel="apple-touch-icon" 	type="text/css" 			href="favicon.png"	/>
    <link rel="shortcut icon" 		type="image/x-icon"			href="favicon.ico"	/>


    <!--<script type="text/javascript" src="/assets/mobile/js/jquery-2.1.4.min.js"></script>-->
	
{literal}
<!-- vk widget -->
    <script async type="text/javascript" src="//vk.com/js/api/openapi.js?116"></script>
{/literal}

 {literal}
        <script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');
  ga('create', 'UA-67805669-1', 'auto', {'allowLinker': true});
ga('require','linker');

  {/literal}
  {if ($type=='product')}
{literal}
 ga('set','dimension3','{/literal}{field name=id}{literal}');
  ga('set','dimension4',"offerdetail");
  ga('set','dimension5',[[+price]]);
  {/literal}
{/if}
{if ($type=='home')}
{literal}
 ga('set','dimension3','');
  ga('set','dimension4',"home");
  ga('set','dimension5',"");
  {/literal}
{/if}
{if ($type=='cart')}
{literal}
 ga('set','dimension3',[[+idsproduct]]);
  ga('set','dimension4',"cart");
  ga('set','dimension5',"[[+cart_cost]]");
  {/literal}
{/if}
{if ($typemarketing=='successorder')}
{literal}
  ga('set','dimension3',[[+idsproduct]]);
  ga('set','dimension4',"purchase");
  ga('set','dimension5',"[[+cart_cost]]");
{/literal}
{/if}
{if ($type=='other')}
{literal}
 ga('set','dimension3','');
  ga('set','dimension4',"other");
  ga('set','dimension5','');
  {/literal}
{/if}
{literal}
ga('linker:autoLink', ['petchoice.ua'] );
  ga('send', 'pageview');
  
  
  



</script>

{/literal}

</head>
<body>
   {literal}
   
<!-- Google Tag Manager -->
<noscript><iframe src="//www.googletagmanager.com/ns.html?id=GTM-W5VBDZ"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'//www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-W5VBDZ');</script>
<!-- End Google Tag Manager -->
   
   <script type="text/javascript">
function setCookie(name, value, options) {
  options = options || {};
  var expires = options.expires;
  if (typeof expires == "number" && expires) {
    var d = new Date();
    d.setTime(d.getTime() + expires * 1000);
    expires = options.expires = d;
  }
  if (expires && expires.toUTCString) {
    options.expires = expires.toUTCString();
  }
  value = encodeURIComponent(value);
  var updatedCookie = name + "=" + value;
  for (var propName in options) {
    updatedCookie += "; " + propName;
    var propValue = options[propName];
    if (propValue !== true) {
      updatedCookie += "=" + propValue;
    }
  }
  document.cookie = updatedCookie;
}
// возвращает cookie с именем name, если есть, если нет, то undefined
function getCookie(name) {
  var matches = document.cookie.match(new RegExp(
    "(?:^|; )" + name.replace(/([\.$?*|{}\(\)\[\]\\\/\+^])/g, '\\$1') + "=([^;]*)"
  ));
  return matches ? decodeURIComponent(matches[1]) : undefined;
}
</script>
 <!-- fb widget -->
   <div id="fb-root"></div>
    <script>(function(d, s, id) {
      var js, fjs = d.getElementsByTagName(s)[0];
      if (d.getElementById(id)) return;
      js = d.createElement(s); js.id = id;
      js.src = "//connect.facebook.net/ru_RU/sdk.js#xfbml=1&version=v2.0";
      fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));</script>
    <!--  end fb -->
   {/literal}
   
   {snippet name=mobilepreloader}



<header class="main-header">



    
	
    <ul>
        <li class="logo">					<a href="{config name=site_url}" title="на главную"></a></li>
        <!--<li class="h-btn-user-room">
            <a href="user_room"></a>
        </li>-->
		[[!Login? &tplType=`modChunk` &loginTpl=`mobilheadNotLoggedInTpl` &logoutTpl=`mobilheadLoggedInTpl`]]

		{nocache}{block name=minicart nocache}{snippet name=msMiniCart_m}{/block}{/nocache}
		
        
        <li class="h-btn-search">			<div title="найти на сайте"></div></li>
		{literal}
		[[!mSearchForm?
			&element=`msProducts`
			&tplForm=`m_tpl.mSearch2.form`
			&fields=`Data.article:3,Data.vendor:2`
			&where=`{"Data.price:>":0}`
		]]
       {/literal}

        <li class="h-btn-menu"><a href="#" title="меню"></a></li>
        <ul class="main-menu">
            <li><a href="/">Главная</a></li>
            <li><a href="/{link id=2060}">Акции</a></li>
            <li><a href="/{link id=475}">О PetChoice</a></li>
            <li><a href="/{link id=473}">Оплата и доставка</a></li>
			<li><a href="/blog">Блог</a></li>
			<li><a href="/brands">Бренды</a></li>
            <li><a href="/{link id=4}">Контакты</a></li>            
            <li><a href="/account">Личный кабинет</a></li>
            <li><a href="https://petchoice.ua/?type=desktop">Полная версия сайта</a></li>

        </ul>
    </ul>
</header>

<div id="main-container">
{snippet name=m_getmessage}
    <footer class="main-footer">

        <ul class="contacts" id="phones-anchor">
            <li>
                ЗАКАЗ И КОНСУЛЬТАЦИЯ ПО ТЕЛЕФОНУ
            </li>
            <li class="main-phone">
                <a href="tel:+380487005080">048 700-50-80</a>
            </li>
            <li class="other-phones">
                <div>
                    <a href="tel:+380635055003">063 505-50-03</a>
                    <a href="tel:+380678416464">067 841-64-64</a>
                    <a href="tel:+380955056595">095 505-65-95</a>                    
                </div>
                <a href="#phones-anchor" class="btn-more-phones">Ещё телефоны</a>
            </li>
            <li class="work-time">
                <span>Пн-Пт: 9:00 - 18:00<br/>
				Сб: 9:00 - 12:00</span>
            </li>
        </ul>

        <ul class="btns-social">
            <li class="soc-icn-fb"><a href="//www.facebook.com/petchoice.ua"></a></li>
            <li class="soc-icn-vk"><a href="//vk.com/petchoice"></a></li>
        </ul>
        <nav class="copyright">
            &copy; 2016 Интернет-магазин зоотоваров PetChoice
        </nav>


    </footer>

   

 {* если главная *}
	  {if ($modx->resource->id==1)}
		{block name=mainpage}{/block}
		
		<div class="main-menu-list">
			 {assign var=params value=[
							"startId"       => 10
							,"level"        => 2
							,"sortBy"       => "menuindex"
							,"levelClass"   => "level"
							,"cacheable"    =>false
							,"id"           => "catalog"    
						]}
						{processor action="site/web/menu/getcatalogmenu" ns="modxsite" params=$params assign=result}
						{assign var=items value=$result.object}
						{include file='tpl/m/menu/menumain.tpl'}
			   <!-- -->
			   
			</div>
		
	  {else}

	  {block name=content}{/block} 
	  
	  {/if}
    
</div> <!-- #main-container -->
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
    <script type="text/javascript" src="/assets/mobile/js/jquery.easings.min.js"></script>
    <script type="text/javascript" src="/assets/mobile/js/swiper.min.js"></script>
    <script type="text/javascript" src="/assets/mobile/js/script.js"></script>
<script src="/assets/scripts/m/m_vendor.js"></script>
    <script src="/assets/scripts/m/scripts.js"></script>
<!--    <script src="/assets/components/msearch/js/mfilter.js" type="text/javascript"></script>
	
	<script type="text/javascript" src="/assets/scripts/img_fit.js"></script>
	-->
	<div class="windows-popup-container forgotpassword">
		<div id="forgotpassword" class="window-popup"><span style="width:30px;" class="btn-close"></span>
			<header class="popup-header" style="padding-right:30px;">
				<h2>Восстановить пароль</h2>
			</header>
			<div id="office-auth-form" class="modal-content">
				{snippet name=officeAuth params="tplLogin=`m_forgotPasswordFormTpl`&HybridAuth=`0`"}
				</div>
			
		</div>
	</div>
	
	
<!--<script type="text/javascript" src="/assets/components/minishop2/js/web/m/default.js"></script>-->
<script type="text/javascript" src="/assets/components/msearch2/js/web/m/default.js"></script>
	
</body>
</html>

