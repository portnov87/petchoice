{extends file="m_layout.tpl"}

{block name=content}


[[!notAuthRedirect]]



<style>
    .action_event .header_action_info{
        margin-top:100px;
    }
    .action_event .body-container, .action_event .product-container,.action_event #main-container{
        padding-top:0px;
    }
    
</style>



[[m_BreadCrumb]]
    <div class="user-room" style="margin-top:30px;">
        <h1 class="user-hello">
            [[%polylang_site_account_hello]], <span class="user-name">{$modx->user->Profile->get('fullname')}</span> !
        </h1>

        <ul class="user-accordion accordion">
            <li class="active">
                <a href="javascript:void(0)">[[%polylang_site_account_bill]]</a>
                <div>				
				[[!m_msGetUserOrders]]                 
                </div>
            </li>
            <li>
                <a href="javascript:void(0)">[[%polylang_site_account_history]]</a>
                <div>
				
				[[!m_msPurchaseHistory? &limit=`10`]]                   

                </div>
            </li>
            <li>
                <a href="javascript:void(0)">[[%polylang_site_account_personal]]</a>
                <div>
				
				{literal}
						[[!OfficeProfile? 
						  &tplProfile=`m_tpl.UpdateProfileForm`
						  &avatarParams=`{"w":160,"h":160,"zc":1,"bg":"ffffff","f":"jpg"}`
						  &profileFields=`username:50,email:50,
						  fullname:50,extended[lastname]:50,phone:12,
						  state,photo,specifiedpassword,extended[room],extended[house],extended[street],
						  confirmpassword,extended[payment],extended[delivery],extended[city_id],extended[warehouse],city`
                      ]]
					  
					 
					  
					  {/literal}
                   
                </div>
            </li>
        </ul>

        <div class="btn-green">
            <a href="/account?service=logout">[[%polylang_site_cmenu_exit]]</a>
        </div>
    </div>


{literal}
	
	
 <!-- Pay Modal paymentform-->
 
 <div class="windows-popup-container paymentform">
		<div id="paymentform" class="window-popup"><span style="width:30px;" class="btn-close"></span>
			<header class="popup-header" style="padding-right:30px;">
				<h2>Оплатить заказ</h2>
			</header>
			<div id="office-pay-form" class="modal-content">
				<form id="getpayment" class="form-horizontal" method="get">
					<div class="form-group number-pay">
						<label id="payInpt">Введите номер заказа</label>
						<input name="msorder" type="text" class="form-control" value="" required />
						<span class="icon green-check-icon hidden"></span>
						<span class="icon red-check-icon hidden"></span>
					  </div>
				
					<div class="errorliqpay hide help-block with-errors">
					</div>
					
					 <div class="modal-footer hidden">
					  <div class="pull-left">
					  <p class="h3 name"><b></b></p>
					  
							  <ul class="list-inline">
								<li>
								<b class="grey inline" style="vertical-align: bottom;">
								<span class="h3">К оплате:</span>
								</b>
								</li>
								<li>
								<span class="price-large inline" style="vertical-align: bottom;">
							   <span class="product-price">0 <span class="small text-top">00</span></span>
								</span>
								</li>
								</ul>
						   <ul class="list-inline">
								<li>
								<b class="grey inline" style="vertical-align: bottom;">
								<span class="h3">[[%polylang_site_order_type_oplata]]:</span>
								</b>
								</li>
								<li>
								<span class="payment"></span>
								</li>
							</ul>
					  
						   <ul class="list-inline">
								<li>
								<b class="grey inline" style="vertical-align: bottom;">
								<span class="h3">[[%polylang_site_order_type_delivery]]:</span>
								</b>
								</li>
								<li>
							  <span class="deliveryorderfooter"></span>
								</li>
							</ul>
					  
					  </div>
					  
					  <button type="submit" name="ms2_action" class="btn orange-btn text-uppercase pull-right payorder" data-value="">[[%polylang_site_common_lable_payment_url]]</button>
					</div>
					
				</form>
				</div>
			
		</div>
	</div>
 
		{/literal}
{/block}