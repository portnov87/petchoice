<script>
    {$ecommerc_datal}
</script>
<ul>
    <li class="form-group">
        <label class="control-label col-xs-4">[[%polylang_site_order_name]]<sup>*</sup>:</label>
        <div class="col-xs-8">
            <input name="receiver" type="text" class="custom-inpt form-control" data-error="введите ваше имя"
                   value="{$form.receiver}" required="" aria-required="true" aria-invalid="false">
            <div class="help-block with-errors"></div>
        </div>
    </li>
    <li class="form-group">
        <label class="control-label col-xs-4">[[%polylang_site_order_lastname]]<sup>*</sup>:</label>
        <div class="col-xs-8">
            <input name="lastname" autocomplete="off" type="text" class="custom-inpt form-control"
                   data-error="[[%polylang_site_insert_lastname]]" value="{$form.lastname}" required="" aria-required="true">
            <div class="help-block with-errors"></div>
        </div>
    </li>
    <li class="form-group  hide fathername">
        <label class="control-label col-xs-4">[[%polylang_site_order_fathername]]<sup>*</sup>:</label>
        <div class="col-xs-8">
            <input name="fathername" id="fathername"  autocomplete="off" type="text" class="custom-inpt form-control"
                   data-error="введите ваше отчество" value="{$form.fathername}" aria-required="true">
            <div class="help-block with-errors">
                <ul class="list-unstyled"><li>введите ваше отчество</li></ul>
            </div>
        </div>
    </li>

    <li class="form-group">
        <label class="control-label col-xs-4">[[%polylang_site_order_phone]]<sup>*</sup>:</label>
        <div class="col-xs-8"><!--disabled-->
            <input name="phone" type="tel" class="custom-inpt form-control" placeholder="+38"
                   data-error="[[%polylang_site_insert_phone]]" value="{$form.phone}" required="" aria-required="true">
            <div class="help-block with-errors"></div>
        </div>
    </li>
    <li class="form-group">
        <label class="control-label col-xs-4">Email:</label>
        <div class="col-xs-8">
            <input name="email" type="email" class="custom-inpt form-control" data-error="[[%polylang_site_form_email_lable]]"
                   value="{$form.email}" required="" aria-required="true">
            <div class="help-block with-errors"></div>
        </div>
    </li>
    <li class="h3"><b class="green">[[%polylang_site_order_delivery]]</b></li>
    <li class="form-group" style="margin-top:10px;">
        <label class="control-label col-xs-4">[[%polylang_site_order_city]]:</label>
        <div class="col-xs-8">
            <input type="text" id="cityidorder_checkout" autocomplete="off" class="custom-inpt form-control" data-error="это поле обязательное" name="suggest_locality" value="{$form.city}" data-provide="typeahead" required="" aria-required="true">


{*            <input type="text" autocomplete="off" class="custom-inpt form-control" data-error="[[%polylang_site_account_fiedl_requerd]]"*}
{*                   name="suggest_locality" value="{$form.city}" data-provide="typeahead" required=""*}
{*                   aria-required="true">*}
            <div class="help-block with-errors hide">[[%polylang_site_order_nofound_city]]
            </div>
            <input name="city" type="hidden" value="Одесса">
            <input name="extended[city_id]" type="hidden" id="cityidorder" value="{$form.city_id}">

            <div class="help-block small grey payment-descr">[[%polylang_site_input_city]]</div>
        </div>
    </li>
    <li class="form-group deliverieswrap">
        <label class="control-label col-xs-4">[[%polylang_site_order_type_delivery]]:</label>
        <div class="col-xs-8">
            <div class="custom-select-container"><select id="deliveries" name="delivery" class="form-control replaced"
                                                         data-error="[[%polylang_site_account_fiedl_requerd]]" required=""
                                                         aria-required="true">
                    <option value="" disabled="" selected="" style="display:none;">[[%polylang_site_order_choice_list]]</option>
                    {$form.deliveries}
                </select>

            </div>
            <div class="col-xs-8">
                <div class="help-block small grey " id="infodeliveryorder">{$form.infodeliver}</div>
            </div>
            <div class="help-block with-errors"></div>
        </div>
    </li>
    <li id="warehouse-block" class="form-group {if ($form.delivery==1)}hide{/if}">
        <label class="control-label col-xs-4">[[%polylang_site_order_warehouse]]:</label>
        <div class="col-xs-8">
            <div class="custom-select-container">
{*                <select id="warehouses" name="extended[warehouse]" class="form-control replaced"*}
{*                        data-error="[[%polylang_site_account_fiedl_requerd]]">*}

{*                    <option value="0" disabled="" selected="" style="display:none;">[[%polylang_site_order_choice_list]]</option>*}
{*                </select>*}



                <input type="text" id="warehouses" name="extended[warehouse_value]" value='{$form.warehouse_name}' placeholder="[[%polylang_site_order_choice_list]]" />
                <input type="hidden" id="warehouses_id" name="extended[warehouse]" value='{$form.warehouse}' />



            </div>
            <input type="hidden" value="" name="textwarehouse" id="textwarehouse"/>
            <div class="help-block with-errors"></div>
        </div>
    </li>
    <li id="samovivoz-block" class="form-group {if ($form.delivery!=1)}hide{/if}">
        <label class="control-label col-xs-4">[[%polylang_site_order_address_samov]]:</label>
        <div class="col-xs-8">
            <div class="custom-select-container">
              <select id="samovivoz" name="samovivoz" class="form-control" >
                            {$form.samovivoz}
                            </select>
                
            </div>
        </div>
    </li>
    <li id="courier-block" class="form-group hide">
        <label class="control-label col-xs-4">[[%polylang_site_order_address]]:</label>
        <div class="col-xs-8">
                <div class="row">
                        
                              <div class="col-xs-12">
                                <input type="text" placeholder="[[%polylang_site_order_address_street]]" class="custom-inpt form-control"  data-error="[[%polylang_site_account_fiedl_requerd]]" id="street_ext" name="extended[street]" value="{$form.street}">
                                <input type="text" placeholder="[[%polylang_site_order_address_street]]" class="custom-inpt form-control"  data-error="[[%polylang_site_account_fiedl_requerd]]" id="street_np" name="extended[street_np]" value="[[+street_np]]">
                                <input type="hidden" placeholder="[[%polylang_site_order_address_street]]" class="custom-inpt form-control"  data-error="[[%polylang_site_account_fiedl_requerd]]" id="street_np_ref" name="extended[street_np_ref]" value="[[+street_np_ref]]">
                                
                                
                                
                                <div class="help-block small grey show_np">
                                    [[%polylang_site_input_street]]
                                </div>
                              </div>
                              
                            </div>
                         <div class="row">            
                <!--<div class="col-xs-6">
                    <input type="text" placeholder="Улица" class="custom-inpt form-control"
                           data-error="это поле обязательное" name="extended[street]" value="{$form.street}">
                </div>-->
                <div class="col-xs-3" style="margin-bottom:15px;">
                    <input type="text" placeholder="[[%polylang_site_order_address_houses]]" class="custom-inpt form-control"
                           data-error="[[%polylang_site_account_fiedl_requerd]]" name="extended[house]" value="{$form.house}">
                </div>
                <div class="col-xs-3" style="margin-bottom:15px;">
                    <input type="text" placeholder="Кв" class="custom-inpt form-control"
                           data-error="[[%polylang_site_account_fiedl_requerd]]" name="extended[room]" value="{$form.room}">
                </div>
            </div>
        </div>
        <div class="help-block with-errors"></div>
    </li>
    <li class="h3"><b class="green">[[%polylang_site_order_type_oplata]]:</b></li>
    <li class="form-group" style="margin-top:10px;">
        <label class="control-label col-xs-4">[[%polylang_site_order_choice_oplate]]:</label>
        <div class="col-xs-8">
            <div class="custom-select-container">
                <select {if ($form.city=='')}disabled{/if} id="payments" name="payments" class="form-control replaced"
                        data-error="[[%polylang_site_account_fiedl_requerd]]" required="" aria-required="true">
                    <option value="0" disabled="" selected="" style="display:none;">[[%polylang_site_order_choice_list]]</option>
                    {$form.payments}

                </select>
            </div>
            <div class="help-block with-errors"></div>
            <!--<div class="help-block small grey payment-descr hidden" data-payment-id="3">
                Оплата на карту Приват банка
            </div>-->
            <div class="help-block small grey payment-descr" data-payment-id="">
            </div>
        </div>
    </li>
    <li class="form-group">
        <label class="control-label col-xs-4">[[%polylang_site_order_comment]]:</label>
        <div class="col-xs-8">
            <textarea name="comment" id="comment" class="form-control"></textarea>
        </div>
    </li>

</ul>


<!-- ENTER PROMO-CODE -->
<ul class="enter-promocode accordion">
    <li>
        <a>[[%polylang_site_enter_promocode]]</a>
        <div>
            <input name="promocode" type="text" id="promocode" value="" class="form-control"><br/>
            <button value="order/promocode" type="submit"
                    class="btn green-btn text-uppercase pull-left promocodevalidate">[[%polylang_site_submit_bonuse_label]]</button>
            <p id="messagepromocode"></p>

            <br/>
            <div class="promocode-sale">
                <span>[[%polylang_site_order_skidka]]:</span>
                <div class="price-large uah" id="discountsumma">0,00</div>
            </div>
        </div>
    </li>
</ul>
{if (($bonuses!='')&&($bonuses>0))}
<ul class="enter-promocode accordion">
    <li>
        <a>[[%polylang_site_use_bonuse]]</a>
        <div>
            <input name="bonuses" type="hidden" id="bonuses" value="{$bonuses}" class="form-control"><br/>

            <div class="promocode-sale">
                <span>[[%polylang_site_pay_bonuse]]:</span>
                <div class="price-large uah" id="bonuses_span">{$bonuses}</div>
            </div>


            <button value="order/bonuses" type="submit" class="btn green-btn text-uppercase pull-left bonusesvalidate">[[%polylang_site_submit_bonuse_label]]</button>
            <p id="messagebonuses"></p>
            <br/>
            <div class="promocode-sale">
                <span>[[%polylang_site_total_bonuse_label]]:</span>
                <div class="price-large uah" id="user_bonuses_span">{$user_bonuses}</div>
            </div>
        </div>
    </li>
</ul>

{/if}


<ul>
    <li class="form-group">
        <label class="control-label col-xs-4"
               style="float:left;clear:none;width:100%;text-align:center;font-weight:700;">[[%polylang_site_order_delivery]]:</label>
        <div class="col-xs-8"
             style="float:left;margin-left:20px;clear:none;width:100%;text-align:center;padding:0px;margin:0px;">
            {if (($form.cost_delivery!=0)&&($form.cost_delivery!=''))}
                <span class="deliverycost" style="width:100%;text-align:center;"
                      id="summadostavkaorder">{$form.cost_delivery} грн.</span>
            {else}
                <span class="deliverycost" style="width:100%;text-align:center;"
                      id="summadostavkaorder">[[%polylang_site_order_shipping_free]]</span>
            {/if}
        </div>
    </li>
</ul>
<div class="col-xs-8">
    <div id="descdeliveryorder">{$form.descdelivery}</div>
</div>
<div class="orders-total-price">[[%polylang_site_order_label_total]]:<div id="totalcart" class="uah">{$form.order_cost}</div><!--order_cost_withdelivery-->


    <input type="hidden" class="total_cost_order" value="{$form.order_cost}"/>

</div>
<div class="btn-orange" style="margin-bottom:10px;">
    <button name="ms2_action" value="order/submit" type="submit" class="btn orange-btn btn-lg">[[%polylang_site_order_submit]]</button>
    <a href="javascript:void(0)" name="ms2_action_payment" value="order/submitpayment" id="payment_liqpay_submit" type="button" class="btn orange-btn btn-lg hide">Подтвердить и оплатить</a>
    
</div>




<div class="checkout_popup_cities popupcheckout">

    <div id="citiesPopup" class="window-popup">
        <header class="popup-header" style="padding-right:30px;">
            <span style="width:30px;" class="btn-close"></span>
            <h2>Выберите город</h2>
            <input type="text" class="form-control" id="searchCities" value="" placeholder="Поиск города"/>
        </header>
        <div class="modal-content">
            <ul class="cities_list">

            </ul>
            <!-- end Forgot Password Modal -->
        </div>
    </div>

</div>


<div class="warehouse_novoposhta popupcheckout">

    <div id="warehousePopup" class="window-popup">
        <header class="popup-header" style="padding-right:30px;">
            <span style="width:30px;" class="btn-close"></span>
            <h2>Выберите отделение</h2>
            <input type="text" class="form-control" id="searchWarehouseNovaposhta" value="" placeholder="Поиск отделения"/>
        </header>
        <div class="modal-content">
            <ul class="novaposhta_warehouser_list">

            </ul>
            <!-- end Forgot Password Modal -->
        </div>
    </div>

</div>




<div class="poshtomat_novoposhta popupcheckout">

    <div id="poshtomatPopup" class="window-popup">
        <header class="popup-header" style="padding-right:30px;">
            <span style="width:30px;" class="btn-close"></span>
            <h2>Выберите почтомат</h2>
            <input type="text" class="form-control" id="searchPoshtomatNovaposhta" value="" placeholder="Поиск почтомата"/>
        </header>
        <div class="modal-content">
            <ul class="novaposhta_poshtomat_list">

            </ul>
            <!-- end Forgot Password Modal -->
        </div>
    </div>

</div>




<style>
    .popupcheckout .popup-header input{
        width:100%;
        margin:10px 0px;
    }
    .popupcheckout{
        position: fixed;
        display: block;
        top: 48px;
        left: 0;
        width: 100%;
        /* max-width: 320px; */
        height: 100%;
        max-height: 100vh;
        overflow-y: auto;
        overflow-x: hidden !important;
        z-index: 60;
        background: rgba(255,255,255,1.00);
        border: 1px solid #abc1d6;
        box-shadow: 0 2px 15px 0 rgb(0 0 0 / 10%);
        padding-bottom: 50px;
        visibility: hidden;
        opacity: 0.0;
        transform: translateX(320px);
        -moz-transform: translateX(320px);
        -ms-transform: translateX(320px);
        -o-transform: translateX(320px);
        -webkit-transform: translateX(320px);
    }
    .activeWarehouse{
        visibility: visible;
        opacity: 1.0;
        transform: translateX(0);
        -moz-transform: translateX(0);
        -ms-transform: translateX(0);
        -o-transform: translateX(0);
        -webkit-transform: translateX(0);


    }

    .popupcheckout .popup-header{
        padding: 0 55px 15px 15px;
        position: sticky;
        top: 0px;
        height: 120px;
        z-index: 10;
    }
    .novaposhta_poshtomat_list,.checkout_popup_cities .cities_list, .novaposhta_warehouser_list{
        display:block;
    }

    .novaposhta_poshtomat_list li, .checkout_popup_cities .cities_list li, .novaposhta_warehouser_list li{
        display:block;
        width:100%;
        border-bottom:1px solid #ccc;
        padding:10px 10px;
    }
    /*.activeWarehouse{
        display:block;
    }*/
    .activeWarehouse .warehousePopup
    {
        display:block;
    }

    .hidenp{
        display:none;
    }
</style>
			
			
			
			