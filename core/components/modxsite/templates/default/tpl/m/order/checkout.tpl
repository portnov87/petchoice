{extends file="m_layout_order.tpl"}

{block name=content}
{literal}
		
			
    	[[!msCheckoutMobile? 
				&tplmobile=`1`
        		&tplOuter=`orderOuterTpl` 
        		&tplEmpty=`orderEmptyTpl`
        		&tplDelivery=`deliveryTpl`
        		&tplPayment=`paymentTpl`
        		&tplSuccess=`m_orderSuccessTpl`
        	]]
			
		{/literal}	
{/block}
