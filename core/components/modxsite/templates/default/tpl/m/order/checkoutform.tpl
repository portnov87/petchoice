<div class="order-checkout-container">
        <ul class="catalog-tool">
            <li class="btn-back"><a href="/"></a></li>
            <li class="crumbs">
                <span>
                [[%polylang_site_checkout_label]]
                </span>
            </li>

        </ul>
        <div class="order-checkout-block">
            <form id="msOrder" class="ms2_form loading1" method="post" data-toggle="validator" role="form" novalidate="true">
			{$form}
			</form>
        </div>
</div>	