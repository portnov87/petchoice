
{extends file="m_layout_order.tpl"}

{block name=content}


[[!msGetOrder?id=`{$order_id}` &tplmobile=`1`]]


{literal}
    <script type="text/javascript">

        [[+ecomerce]]
    </script>
  <style>
        #main-container h3, #main-container p,#main-container h1,#main-container h2
        {
            line-height:1.2;margin-bottom:15px;
        }
        #main-container .h3{
            font-size:18px;
            margin-bottom:15px;
        }
    </style>

<div id="main-container" style="" class="padding-lr" style="padding-bottom:0px!important;">

<div class="row">
    <div class="col-xs-12">
        <h1 class="green">[[+nameuser]], благодарим за Ваш заказ!</h1>
        <div class="h3">
          <span class="grey">Номер Вашего заказа:</span> №[[+num]]
        </div>
        <p>
          Наши менеджеры свяжутся с Вами в ближайшее время.
          <br>
          Информации о заказе отправлена на ваш электронный адрес.
        </p>
        <div class="h3 green">
          <b>
            Следите за нами в соц. сетях. Узнавайте первыми об акциях и распродажах Pet Choice
          </b>
        </div>
        <br>
        <div class="row">
          <div class="col-xs-6   col-sm-3 col-md-3" style="text-align:center;">
            <div data-show-border="false" data-stream="false" data-header="false" data-show-faces="true" data-colorscheme="light" data-height="350" data-width="250" data-href="https://www.facebook.com/petchoice.com.ua?ref=profile&amp;pnref=lhc" class="fb-like-box"></div>
          </div>
          <div class="col-xs-6   col-sm-3 col-md-3" style="text-align:center;">
          {if !$modInstagram = $_modx->cacheManager->get('mod_instagram')}
                {set $modInstagram = '!modInstagram' | snippet }
                {set $null = $_modx->cacheManager->set('mod_instagram', $modInstagram, 1800)} {* кэш на 30 минут *}
            {/if}
            
            {$modInstagram}
            
          </div>
        </div><br style="clear:both;"/>    
    </div>

</div>

    <script>
        setTimeout(function(){location.href='/'},300000);
    </script>

</div>
{/literal}
{/block}

