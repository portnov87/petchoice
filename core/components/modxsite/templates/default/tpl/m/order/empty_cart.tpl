<div class="wrapper_empty_cart">
    <h1>[[%polylang_site_cart_price_emptycart]]</h1>

    <div class="container_empty_cart">
        <div class="background_img"></div>
        <span>[[%polylang_site_cart_price_emptycartmess]]</span>
    </div>
</div>

<div class="wrapper_link_menu">
    <a href="javascript:void(0);" class="open_menu_catalog"><span>[[%polylang_site_common_menu_catalog]]</span></a>
</div>


<div class="main_products">
    <div class="title_block">[[%polylang_site_canbe_interest]]</div>
    {assign var=params value=[
    "startId"       => 10
    ,"level"        => 2
    ,"sortBy"       => "menuindex"
    ,"levelClass"   => "level"
    ,"cacheable"    =>false
    ,'list'=>'Promotions and discounts'
    ,"id"           => "catalog"
    ]}
    {processor action="site/web/products/getproductsforemptycart" ns="modxsite" params=$params assign=resultaction}
    {assign var=items value=$resultaction.object}

    <div class="product_list">
        {foreach $items.list as $product}
            {include file='tpl/m/products/action.tpl'}
        {/foreach}
    </div>
</div>