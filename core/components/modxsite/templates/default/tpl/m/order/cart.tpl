<script>
    {$ecommerc_datal} 
</script>
<div class="basket-container">

<form id="msOrder" class="ms2_form" novalidate="true" role="form" data-toggle="validator" method="post">
        <ul class="catalog-tool">
            <li class="btn-back"><a href="/"></a></li>
            <li class="crumbs">
                <span>
                [[%polylang_site_cart_label]]
                </span>
            </li>

        </ul>

<ul class="basket-table-list">
{foreach $cart.goods as $good}
<li class="basket-table-block" data-product-key="{$good.key}">
{if (isset($good.actiondis)&&($good.actiondis!=0))}<div data-marker="green" class="sale-marker">{$good.actiondis}%</div>{/if}
                    <span

                            data-product-vendor="{$good.vendorname_ec}"
                            data-product-category="{$good.category_ec}"
                            data-product-price="{$good.price}"
                            data-product-name="{$good.pagetitle_ec}"
                            data-product-id="{$good.id}" data-size="{$good.size}"
                            onclick="removeFromCartMobile('{$good.key}',this);" class="btn-delet-product"></span>
                <ul>
                
    				
					
                    <li class="basket-table-product-img imgfit">
                    <a style="display:block;width: 100%;height: 100px;position: relative;z-index: 9;" href="/{link id={$good.id}}">
                        {snippet name=pthumb params="input=`{$good.image}`&options=`w=100&h=100&far=1`&useResizer=`1`" assign=preview}
                        
                    <img src="{$preview}" alt="{$good.pagetitle}" />
                        </a>
                    </li>
                    <li class="basket-table-product-name">
                        <h2>
                        <a href="/{link id={$good.id}}">
                            {$good.pagetitle}, {$good.size}
                            </a>
                        </h2>
                        {if ({$good.article}!='')}
                            <span class="grey small">
                                [[%polylang_site_cart_code_label]]: {$good.article}
                            </span>
                        {/if}
                    </li>
                </ul>
                <!-- --> <ul class="basket-table-product-tool">
                  <!--<li class="product-type-form">
                        <form action="">
                            <select>
                                <option value="">25-35 см/10 мм шт</option>
                                <option value="" selected>30-40см/15мм шт</option>
                                <option value="">40-50см/15мм шт</option>
                            </select>
                        </form>
                    </li>
                    <li class="icn-free"></li>-->
                    <li class="basket-table-product-sale">
                        <div class="sale">[[%polylang_site_order_skidka]]: {if ($good.actiondis!='')}{$good.actiondis}{else}{$discount}{/if}%
						</div>
                    </li>
                </ul>
                <ul class="basket-table-product-tool2">
                    <li>
                        <span>[[%polylang_site_cart_price_label]]:</span>
                        <div class="product-price uah">
                        {if ({$good.oldprice}!='')}
                                {$good.oldprice}
                            {else}
                            {$good.price}
                            {/if}
                        </div>
                    </li>
                    <li class="quantity">
                        <input class="input-quantity" data-product-vendor="{$good.product_vendor}"
                                                  data-product-category="{$good.category_ec}"
                                                  data-product-price="{$good.price}"                                                  
					  data-product-name="{$good.pagetitle}"
        data-product-id="{$good.id}" data-size="{$good.size}"  type="number" data-key="{$good.key}" value="{$good.count}" />
                        <span class="number-minus" data-key="{$good.key}" ></span>
                        <span class="number-plus" data-key="{$good.key}" ></span>
                    </li>
                    <li>
                        <span>[[%polylang_site_amount_label]]:</span>
                        <div class="product-total-price uah">{$good.cost}</div>
                    </li>
                </ul>
            </li><!-- basket product end -->
{/foreach}
</ul>

        
            
           

        <!-- ENTER PROMO-CODE -->
        <ul class="enter-promocode accordion">
            <li>
                <a>[[%polylang_site_enter_promocode]]</a>
                <div>
                    <input name="promocode" type="text" id="promocode" value="{$promocode_code}" class="form-control"><br />
                    <button value="order/promocode" type="submit" class="btn green-btn text-uppercase pull-left promocodevalidate">[[%polylang_site_order_primenit]]</button>
					
                    <p  id="messagepromocode"></p>

                    <br />
                    <div class="promocode-sale">
                        <span>[[%polylang_site_discount_label]]:</span><div class="price-large uah" id="discountsumma">0,00</div>
                    </div>
                </div>
            </li>
        </ul>
        
        {if ($modx->user->isAuthenticated())}
        {if (($bonuses!='')&&($bonuses>0))}
            <ul class="enter-promocode accordion">
                <li>
                    <a>[[%polylang_site_use_bonuse]]</a>
                    <div>
                        <input name="bonuses" type="hidden" id="bonuses" value="{$bonuses}" class="form-control"><br/>
            
                        <div class="promocode-sale">
                            <span>[[%polylang_site_pay_bonuse]]:</span>
                            <div class="price-large uah" id="bonuses_span">{$bonuses}</div>
                        </div>
            
            
                        <button value="order/bonuses" type="submit" class="btn green-btn text-uppercase pull-left bonusesvalidate">[[%polylang_site_submit_bonuse_label]]</button>
                        <p id="messagebonuses"></p>
                        <br/>
                        <div class="promocode-sale">
                            <span>[[%polylang_site_total_bonuse_label]]:</span>
                            <div class="price-large uah" id="user_bonuses_span">{$user_bonuses}</div>
                        </div>
                    </div>
                </li>
            </ul>
            
            {/if}
            {/if}
            

        
        <div class="orders-total-price">
            [[%polylang_site_order_label_total]]: <div class="uah" id="totalcart" >{$cart.total_cost}</div>
        </div>
        <div class="btn-orange">
            <a href="/order">[[%polylang_site_checkout_label]]</a>
        </div>
		<div class="btn-grey" style="margin-top:0px;">                
				<a class="btn small grey-btn btn-lg clickquickorder" style="color:#fff;text-decoration:none;" href="#quickorder">[[%polylang_site_order_quick]]</a>
            </div>
		
	</form>
</div>

	
	
	
<div class="windows-popup-container quickorder">
    <div id="quickorder" class="window-popup" ><span class="btn-close"></span>
        <header class="popup-header">
            <h2>[[%polylang_site_order_quick]]</h2>
        </header>
		<form id="msOrderQuick" style="padding:30px;" class="ms2_form" method="post" data-toggle="validator" role="form">
										<div class="regformorder row">
											<div class="col-xs-11">
												<div class="form-horizontal">
													  <div class="form-group">
														<label class="control-label col-xs-4">[[%polylang_site_order_name]]*:</label>
														<div class="col-xs-8">
														  <input name="receiver" type="text" class="custom-inpt form-control" data-error="[[%polylang_site_insert_lastname]]" value="{$receiver}" required>
														  <div class="help-block with-errors"></div>
														</div>
													  </div>
													  <div class="form-group">
														<label class="control-label col-xs-4">Телефон*:</label>
														<div class="col-xs-8">
														  <input name="phone" autocomplete="off"  type="tel" class="custom-inpt form-control" placeholder="+38"  data-error="[[%polylang_site_insert_phone]]" value="{$phone}" required>
														  <div class="help-block with-errors error-phone"></div>
														</div>
													  </div>
													 
												</div>
											</div>
									  </div>
									 
									 <button name="ms2_action" value="order/submitquick" type="submit" class="btn green-btn btn-lg">[[%polylang_site_order_submit]]</button>
									 
								 </form>
    </div>
</div>
