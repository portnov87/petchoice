{extends file="m_layout.tpl"}

{block name=content}
<style>
    .action_event .header_action_info{
        margin-top:100px;
    }
    .action_event .body-container, .action_event .product-container,.action_event #main-container{
        padding-top:0px;
    }
</style>
{literal}
[[!loggedinRedirect]]
    		<div class="loginregistration-container">
					[[m_BreadCrumb]]

					<br />

					<div class="login-registration-block login-block active">
                    
                    
                        [[!Login?
								&loginTpl=`m_loginFormTpl`
								&logoutTpl=`logoutTpl`
								&errTpl=`lgnErrTpl`
								&loginResourceId=`7`
								]]
                                
                    </div>
                    
                    
                    <div class="login-registration-block registration-block">
                    
                        [[!Register?
								&submitVar=`registerbtn`
								&activationResourceId=`611`
								&activationEmailSubject=`Подтверждение регистрации`
								&successMsg=`Вам нужно подтвердить свою регистрацию.`
								&usernameField=`phone`
								&usergroups=`3:Super User`
								&activation=`0`
								&autoLogin=`1`
								&submittedResourceId=`7`
								&validate=`
								password:required:minLength=^6^,
								password_confirm:password_confirm=^password^,
								fullname:required,
								phone:required,
								email:required:email`
								&placeholderPrefix=`reg.`
								]]
								[[!+error.message]]
								[[!+successMsg]]
								<div class="registerMessage">[[!+reg.error.message]]</div>
								<!--
								onsubmit="dataLayer.push({
								'event': 'event-GA',
								'eventCategory' : 'client',
								'eventAction' : 'registration'
								});"-->
								<form autocomplete="off" onsubmit="dataLayer.push({
								'event': 'event-GA',
								'eventCategory' : 'client',
								'eventAction' : 'registration'
								});" action="/account/register" class="formaregistration1  form_stretchy" method="post" data-toggle="validator" role="form">
									<div class="col-xs-8">
										<div class="form-group ">
											<label class="control-label">Имя <sup>*</sup></label>
											<input name="fullname" type="text" class="form-control custom-inpt" data-error="введите ваше имя" value="[[!+reg.fullname]]" placeholder="Иван" required="" aria-required="true">
											<div class="help-block with-errors hide1"></div>
										</div>
										<div class="form-group">
											<label class="control-label">Фамилия</label>
											<input name="lastname" type="text" class="form-control custom-inpt" data-error="введите вашу фамилия" value="[[!+reg.lastname]]" placeholder="Иванов">
										</div>
										<div class="form-group ">
											<label class="control-label">Телефон <sup>*</sup></label>
											<input type="text" name="phone" class="form-control custom-inpt" placeholder="+38" data-error="введите ваш телефон" value="" required="" aria-required="true">
											<div class="help-block with-errors hide1"><!--введите ваш телефон--></div>
											<div class="help-block small grey">
												<!--<ul class="list-unstyled">
													<li>Будет использован как логин к Личному кабинету</li>
													<li>Укажите номер телефона в формате +380ХХХХХХХХХ без пробелов, дефисов и скобок</li>
												</ul>-->
											</div>
										</div>
										<div class="form-group ">
											<label class="control-label">Email <sup>*</sup></label>
											<input name="email" type="email" autocomplete="off" class="form-control custom-inpt" value="[[!+reg.email]]" data-error="введите вашу почту" required="" aria-required="true">
											<div class="help-block with-errors hide1"><!--введите вашу почту--></div>
											<div class="help-block small grey"></div>
										</div>

										<div class="checkbox">
											<input type="checkbox" name="subscribe_order" id="subscribe_order" value="1" checked="">
											<label for="subscribe_order" class="">
												<span>Хочу получать самые выгодные предложения</span>
											</label>
										</div>
										<div class="form-group ">
											<label class="control-label">Пароль <sup>*</sup></label>
											<input id="password" name="password" autocomplete="off" type="password" class="form-control custom-inpt" value="" data-error="введите пароль" required="" aria-required="true">
											<div class="help-block with-errors hide1"><!--введите пароль--></div>
											<div class="help-block small grey">Не менее 6 символов</div>
										</div>
										<div class="form-group ">
											<label class="control-label">Подтвердите пароль <sup>*</sup></label>
											<input name="password_confirm" id="password_confirm" type="password" class="form-control custom-inpt" data-error="подтвердите пароль" value="" required="" aria-required="true">
											<div class="help-block with-errors hide1"><!--подтвердите пароль--></div>
										</div>
										<div class="form-group">
											<sup>*</sup> <span class="help-block">- поля обязательны для заполнения</span>
										</div>
									</div>
									<input name="discount" type="hidden" value="5"><input class="hide" type="text" name="code" autocomplete="off" value="">
									<input name="registerbtn" type="hidden" value="Зарегистрироваться">
									<!--<input type="submit" value="Зарегистрироваться" name="registerbtn1" class="submit-btn">-->
                                    <input style="" type="submit" value="Зарегистрироваться" name="registerbtn1" class="submit-btn button orange" />
                                    <a href="javascript:void(0);" class="link_login button whitefon">Есть пользователь</a>
								</form>

					<!--	<div class="tabs-btn">
							<div data-tab-btn="1" class="active">Войти</div><div data-tab-btn="2">Зарегистрироваться</div>
						</div>
						<ul class="tabs">
							<li data-tab="1" class="login-form-container">
							
							</li>


							<li data-tab="2" class="registration-form-container">
							
							</li>
						</ul>-->
                        </div>
					




			</div> <!-- #main-container -->





		{/literal}
{/block}