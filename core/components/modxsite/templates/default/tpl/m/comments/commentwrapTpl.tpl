<div class="reviews-container" style="">
			{foreach $commentsThread.comments as $comment}
                            <div class="review">
                                <div class="review-name">{$comment.name}</div>
								<div class="review-user-pets">
								{snippet name=m_getPetCom params="tpl=`m_tpl.PetComment`&pet=`$comment.pet`&pet_type=`$comment.pet_type`&userId=`$comment.createdby`"}
                               </div>
								
                                <div class="review-rating" itemprop="reviewRating" itemscope="" itemtype="http://schema.org/Rating">
                                    <div class="stars-comment" itemprop="ratingValue">
										{$comment.ratingtext}
                                        
                                    </div>
                                    <meta itemprop="worstRating" content="1">
                                    <meta itemprop="bestRating" content="5">
                                </div>
                                <div class="review-text">
                                    {$comment.text}
                                </div>
                                <div class="review-date">{$comment.created|datecomment:"d F Y"}</div>
                            </div>
			{/foreach}
                            
                        </div>

						{if ($commentsThread.auth==1)}
                        <div class="review-form-container">
						
                            <form id="comment-form" action="" method="post" novalidate="novalidate">
                                <p class="h3 green"><b>[[%polylang_site_product_review_write]]</b></p>
                                <input type="hidden" name="thread" value="resource-{$modx->resource->id}">
                                <input type="hidden" name="parent" value="0">
                                <input type="hidden" name="id" value="0">
                                <div class="br-widget">
                                    <select name="rating" id="rating1" class="stars-rating hide">
                                        <option value="1">1</option>
                                        <option value="2">2</option>
                                        <option value="3">3</option>
                                        <option value="4" selected="">4</option>
                                        <option value="5">5</option>
                                    </select>
                                    
                                </div>

                                <textarea name="text" id="comment-editor" class="custom-inpt" rows="6"></textarea>

                                <button type="submit" class="btn green-btn text-uppercase pull-right submit">[[%polylang_site_submit]]</button>
                            </form>
                        </div>
						{else}
						
						
                        <div class="review-form-inlogin">
                        [[%polylang_site_review_new]]                            
                        </div>
						{/if}