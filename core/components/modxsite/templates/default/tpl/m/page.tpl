{extends file="m_layout.tpl"}

{block name=content}
{literal}

<style>
    .action_event .header_action_info{
        margin-top:100px;
    }
    .action_event .product-container,.action_event #main-container{
        padding-top:0px;
    }
    
</style>


<div class="body-container">
[[m_BreadCrumb]]
      <div class="text-block col-xs-9 padding-lr"  >
        
        <div class="row blog-content">
         {/literal}
			  <h1 style="padding:0px 0px;">{field name=pagetitle}</h1>
			  {literal}
          <div class="col-xs-9" style="width:100%;">
            [[*content]]
            <div class="socials">
              <div class="pluso" data-background="transparent" data-options="medium,square,line,horizontal,nocounter,theme=04" data-services="vkontakte,odnoklassniki,facebook,twitter,google" data-user="280379110"></div>
            </div>
          </div>
        </div>

      
      </div>
	  <br style="clear:both"/>
</div>{/literal}

{/block}
