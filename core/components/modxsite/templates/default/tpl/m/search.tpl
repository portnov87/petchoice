{extends file="m_layout.tpl"}

{block name=content}


<style>
    .action_event .header_action_info{
        margin-top:100px;
    }
    .action_event .body-container, .action_event .product-container,.action_event #main-container{
        padding-top:0px;
    }
    
</style>



<div class="big-prevs-container">
        
		[[m_BreadCrumb? &filter=`1`]]
		
        <!--<div id="sort-by-form">			
				<form action="">
					<label for="sort-by">Сортировать по:</label>
					<div id="mse2_sort">
						<select id="sort-by" >
							<option value="" selected class="sort filter-link [[+mse2_sort:is=`ms|hits:asc`:then=`up-sort active`:elseis=`ms|hits:desc`:then=`down-sort active`:else=`down-sort active`]]" data-sort="ms|hits" data-dir="[[+mse2_sort:is=`ms|hits:asc`:then=`asc`]][[+mse2_sort:is=`ms|hits:desc`:then=`desc`]]" data-default="asc">популярности</option>
							<option value="" class="sort filter-link [[+mse2_sort:is=`ms_product|pagetitle:asc`:then=`up-sort active`]] [[+mse2_sort:is=`ms_product|pagetitle:desc`:then=`down-sort active`]] " data-sort="ms_product|pagetitle" data-dir="[[+mse2_sort:is=`ms_product|pagetitle:asc`:then=`asc`]][[+mse2_sort:is=`ms_product|pagetitle:desc`:then=`desc`]]" data-default="asc">алфавиту</option>
							
						</select>
						
					</div>
				</form>
        </div>-->
		{literal}	
	[[!searchyandex]]	
	{/literal}
	
	</div><!-- paginatorpage.big-prevs-container -->
	

<!-- windows popup -->
<div class="windows-popup-container product-quantity-pu">
    <div id="product-quantity-pu" class="window-popup"><span class="btn-close"></span>
        <header class="popup-header">
            <h2>Количество:</h2>
        </header>
		<form class="ms2_form" method="post" onsubmit="dataLayer.push({
	'event': 'event-GA',
	'eventCategory' : 'product',
	'eventAction' : 'buy-preview'
	});">
                <input type="hidden" name="product_id" value="" />
        <div class="quantity">
            <input class="input-quantity" type="number" name="countcart" value="1" />
            <span class="number-minus"></span>
            <span class="number-plus"></span>
        </div>
        <div class="btn-orange">
            <button type="submit" class="pay-btn orange-btn pull-right text-uppercase product add-cart">[[%polylang_site_product_button_buy]]</button>
        </div>
		<input type="hidden" value="" id="idproductcart"/>
		<input type="hidden" value="" id="productsize"/>
		<input type="hidden" value="cart/add" id="action"/>
		<input type="hidden" value="" name="key" id="keycart"/>
		</form>
		
    </div>
</div>



<div class="windows-popup-container boxStatus">
    <div id="boxStatus" class="window-popup"><span class="btn-close"></span>
        <header class="popup-header">
            <h2>Сообщить о наличии:</h2>
        </header>
		
		{literal}
		 [[!AjaxForm?
                	&snippet=`ask_stock`
                	&form=`m_FormAsk.tpl`
                	&hooks=`email`
                	&emailSubject=`Тестовое сообщение`
                	&emailTo=`portnovvit@gmail.com`
                	&validate=`name:required,phone:required`
                	&validationErrorMessage=`В форме содержатся ошибки!`
                	&successMessage=`Сообщение успешно отправлено`
                ]]
		{/literal}
    </div>
</div>




	  
	   {/block}