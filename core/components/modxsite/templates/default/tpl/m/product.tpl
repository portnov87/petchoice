{extends file="m_layout.tpl"}

{block name=content}
    {snippet name=esputnikproduct params="id={field name=id}"}

    <style>
        .action_event .header_action_info {
            margin-top: 100px;
        }

        .action_event #main-container {
            padding-top: 0px;
        }
    </style>
    {snippet name=view_recent}
    {snippet name=HPCount}
    {snippet name=HitsPage params="saveTv=`true`"}


    <div class="container" style="width:100%;padding:0px;">

    {field name=info_action assign=infoaction}
    {field name=infoproduct assign=infoproduct}

    {field name=nodostavka assign=nodostavka}
    {assign var=params value=[
    "dostavka"       => {$nodostavka}

    ]}


    {processor action="site/web/products/getoptionslist" ns="modxsite" params=$params assign=resultproduct}
    <script>
        {$resultproduct.object.ecomerc}
    </script>
    {assign var=article value={$resultproduct.object.article}}
    {assign var=vendor value={$resultproduct.object.vendor}}

    {assign var=popular value={$resultproduct.object.popular}}
    {assign var=new value={$resultproduct.object.new}}


    {field name=product_tab_consist assign=product_tab_consist}
    {field name=product_tab_feeding assign=product_tab_feeding}
    {field name=product_tab_use assign=product_tab_use}
    <div class="product-container">

        {snippet name=m_BreadCrumb}


        <div class="product-block" style="margin-top: 0px;">
            <div class="product-name">
                <h1>
                    {field name=pagetitle}
                </h1>
            </div>
            {if ({$article}!='')}
                <div class="product-code">
                    [[%polylang_site_product_label_code]]: <span>{$article}</span>
                </div>
            {/if}

            <div class="product-photo-container" style="margin-bottom:15px;">
                {field name=actionproduct assign =discount}

                {if ($discount!='')}
                    {snippet name=m_isaction params="id=`{field name=id}`"}
                {/if}
                {if ($new==1)}
                    <span data-marker="blue2" class="marker">
                    [[%polylang_site_product_label_newpr]]
                </span>
                {elseif ({$popular}==1)}
                    <span data-marker="green" class="marker">[[%polylang_site_product_label_topprd]]</span>
                {else}
                {/if}

                {if ($resultproduct.object.action_product_value!=0)}
                {if ($resultproduct.object.black_friday == 1)}
{*                    <span data-marker="red" style="background:black;    color: #fff;*}
{*    top: 50px;*}
{*    width: 80px;*}
{*    padding: 5px;*}
{*    height: 50px;*}
{*    text-align: center;*}
{*    text-transform: uppercase;" class="prev-sale-marker">[[%polylang_site_product_label_friday]]</span>*}

{*                    <div class="product-photo-lbl discount-lbl {if ($resultproduct.object.black_friday==1)} product-info-black_friday {/if}{if ($resultproduct.object.newyear==1)} product-info-newyear {/if}">*}

{*                        [[%polylang_site_product_label_friday]]<br/><b></b>*}
{*                    </div>*}
                {/if}
                {/if}


                {if ($resultproduct.object.action_product==1)}
                    <span data-marker="red" style="top:40px;" class="prev-sale-marker">АКЦИЯ</span>
                {/if}

                {if ($resultproduct.object.toorder==1)}
                    <span data-marker="red" class="prev-sale-marker">ПОД ЗАКАЗ</span>
                {/if}




                {snippet name=m_isaction params="id=`{field name=id}`"}


                <div class="product-photo">

                    {snippet name=msGallery params="tplOuter=`m_galleryThumbOuterTpl`&tplRow=`m_galleryThumbRowTpl`"}

                </div>
                {if (($resultproduct.object.bonuses!='')&&($resultproduct.object.bonuses>0))}
                    <div class="cashback cb_mobile">
                        <div class="cashback_balloon">
                            <span>{$resultproduct.object.bonuses}</span>
                        </div>
                        <div class="cashback_info">
                            [[%polylang_site_product_bonus_info? &bonuses={$resultproduct.object.bonuses}
                            &size_bonuses={$resultproduct.object.size_bonuses}
                            &maxpercentorder={$resultproduct.object.maxpercentorder}]]

                            <!--    Возвращаем <span>{$resultproduct.object.bonuses} грн</span> на бонусный счет при покупке
                            <span>{$resultproduct.object.size_bonuses}</span><br>
                            Оплачивайте бонусами до {$resultproduct.object.maxpercentorder}% от стоимости заказа-->
                        </div>
                    </div>
                {/if}
            </div>


            {if ($infoproduct!='')}
                <div class="product-sale-info">
                    <span>{$infoproduct}</span>
                </div>
            {else}

            {/if}
            {if ($infoaction!='')}
                <div class="product-sale-info">
                    <span>
                       {$infoaction}
                    </span>
                </div>
            {/if}

            {if ($nodostavka=='yes')}
                <div class="product-other-info">
                    <span>
                        Доставка на отделение Новая Почта или Интайм осуществляется за счет покупателя
                    </span>
                </div>
            {else}
                {if (($resultproduct.object.bonuses!='')&&($resultproduct.object.bonuses>0))}
                    <div class="product-other-info">
                        <span>

[[%polylang_site_product_bonus_info? &bonuses={$resultproduct.object.bonuses} &size_bonuses={$resultproduct.object.size_bonuses} &maxpercentorder={$resultproduct.object.maxpercentorder}]]
                            <!--Возвращаем <span>{$resultproduct.object.bonuses} грн</span> на бонусный счет при покупке
                            <span>{$resultproduct.object.size_bonuses}</span><br>
                            Оплачивайте бонусами до {$resultproduct.object.maxpercentorder}% от стоимости заказа
-->
                        </span>
                    </div>
                {/if}
            {/if}


            <form class="ms2_form" method="post">
                <input type="hidden" name="product_id" value="{field name=id}"/>

                <ul class="product-buy-choice">
                    {foreach $resultproduct.object.outputattr as $attr}
                        {include file='tpl/m/products/option.tpl'}
                    {/foreach}
                </ul>
            </form>
            <div class="product-brief">
                <h2>[[%polylang_site_product_label_haracteristic]]:</h2>

                [[%polylang_site_product_label_country]]: {snippet name=msVendor params="vendor=`{$vendor}`&returnOption=`country`&tpl=`vendorCountryTpl`"}
                <br/>
                [[%polylang_site_product_label_brand]]: {snippet name=msVendor params="vendor=`{$vendor}`&returnOption=`name`&tpl=`vendorNameTpl`"}
                <br/>
                [[%polylang_site_product_label_cat]]: {field name=parent assign=parent}
                {snippet name=getResourceField params="id=`{$parent}`"}<br/>

                {snippet name=msAllOptions}

            </div>


            <ul class="product-info-accordion accordion">

                <li class="active"><a href="#">[[%polylang_site_product_description]]</a>
                    <div>
                        {field name=content}
                    </div>
                </li>
                {if ($product_tab_consist!='')}
                    <li class="composition-table">
                        <a href="#">[[%polylang_site_product_sostav]]</a>
                        <div>
                            {$product_tab_consist}
                        </div>
                    </li>
                {/if}
                {if ($product_tab_feeding!='')}
                    <li class="composition-table">
                        <a href="#">[[%polylang_site_product_norm_korm]]</a>
                        <div>
                            {$product_tab_feeding}
                        </div>
                    </li>
                {/if}

                {if ($product_tab_use!='')}
                    <li class="composition-table">
                        <a href="#">[[%polylang_site_product_why_use]]</a>
                        <div>
                            {$product_tab_use}
                        </div>
                    </li>
                {/if}

                <li>
                    <a href="#">[[%polylang_site_product_review_label]]

                        <div class="rating_product">
                            <div class="product-item-stars text-right" itemprop="aggregateRating" itemscope
                                 itemtype="http://schema.org/AggregateRating">
                                <div class="product-item-stars-wrapper">
                                    <div class="product-item-stars-grid">
                                        {$rating}
                                    </div>
                                    <div style="margin-top:-5px;">
                                        <span class="icon small-use-icon"></span>
                                        <span class="count text-bottom"><span class='tabcountsreview'>(<span
                                                        itemprop='ratingCount'>{$countcomments}</span>)</span></span>
                                    </div>

                                </div>

                                <meta content="{$ratingValue}" itemprop="ratingValue"/>
                                <meta content="1" itemprop="worstRating"/>
                                <meta itemprop="reviewCount" content="{$countcomments}"/>
                                <meta content="5" itemprop="bestRating"/>

                            </div>
                        </div>
                    </a>
                    <div>

                        <!--
                			  <div id="comments" class="comments reviews-container">
                					{foreach $commentslimit as $comment}
                					<div itemprop="review" itemscope itemtype="http://schema.org/Review"  class="review user-comment1 col-xs-12 ticket-comment" id="comment-{$comment.id}" data-parent="0" data-newparent="0" data-id="{$comment.id}">
                							<p>
                							  <span class="review-name" itemprop="author" itemscope itemtype="http://schema.org/Person">
                							  <b itemprop="name">{$comment.name}</b>
                							  </span>
                							  
                							  {if ($comment.pet!='')}<br><em class="grey small review-user-pets">{$comment.pet} {$comment.pet_type}	</em>{/if}
                							</p>
                							 
                							{if ($comment.rat!='')}
                							<div itemprop="reviewRating" itemscope itemtype="http://schema.org/Rating">
                								<div class="review-rating stars-comment" >{$comment.rat}</div>
                                                <meta  itemprop="ratingValue" content = "{$comment.rating}">
                									<meta itemprop="worstRating" content = "1">
                									<meta itemprop="bestRating" content = "5">
                							</div>
                							{/if}							 
                							<p  itemprop="reviewBody" class="review-text">
                							  {$comment.text}
                							</p>
                							<div class="row">
                							  <div class="col-xs-8">							  
                								<p>
                								  <em class="small grey review-date" itemprop="datePublished" content="{$comment.created}">
                                                       {$comment.created|datecomment:"d F Y"}  {*{$comment.created|date_format:"d F Y"}*}</em>
                
                								  
                								</p>
                							  </div>							  
                							</div>
                					</div>
                					{/foreach}
                				</div>
                					
                				{if ($countcomments>3)}<a class="seeallcom" data-toggle="tab" role="tab" aria-controls="comments-products-pane" href="#comments-products-pane" aria-expanded="false">[[%polylang_site_product_seeall_review]] ({$countcomments})</a>{/if}
                -->

                        {snippet name=m_TicketComments params="enableCaptcha=`0`&nameproduct=`{field name=pagetitle}`"}
                    </div>
                </li>
            </ul>
        </div>

    </div>
    <!-- .big-prevs-container -->




    <!-- windows popup -->
    <div class="windows-popup-container product-quantity-pu">
        <div id="product-quantity-pu" class="window-popup"><span class="btn-close"></span>
            <header class="popup-header">
                <h2>[[%polylang_site_cart_qty_label]]</h2>
            </header>
            <form class="ms2_form" method="post" onsubmit="dataLayer.push({
'event': 'event-GA',
'eventCategory' : 'product',
'eventAction' : 'buy-product-page'
});">
                <input type="hidden" name="product_id" value="{field name=id}"/>
                <div class="quantity">
                    <input class="input-quantity" type="number" name="countcart" value="1"/>
                    <span class="number-minus"></span>
                    <span class="number-plus"></span>
                </div>
                <div class="btn-orange">
                    <button type="submit" data-product-vendor="{$vendorname_ec}"
                            data-product-category="{$namecategory_ec}"
                            data-product-name="{$productname_ec}"
                            data-count="1"
                            class="pay-btn orange-btn pull-right text-uppercase product add-cart">
                        [[%polylang_site_product_button_basket]]
                    </button>
                </div>
                <input type="hidden" value="" id="idproductcart"/>
                <input type="hidden" value="" id="optionId"/>
                <input type="hidden" value="" id="productprice"/>
                <input type="hidden" value="" id="productsize"/>
                <input type="hidden" value="cart/add" id="action"/>
                <input type="hidden" value="" name="key" id="keycart"/>
            </form>

        </div>
    </div>
    <div class="windows-popup-container boxStatus">
        <div id="boxStatus" class="window-popup"><span class="btn-close"></span>
            <header class="popup-header">
                <h2 style="margin-top:0px;">Сообщить о наличии:</h2>
            </header>

            {literal}
                [[!AjaxForm?
                &snippet=`FormIt`
                &form=`m_FormAsk.tpl`
                &hooks=`recaptchav2,email`
                &emailSubject=`Запрос о наличии`
                &emailTo=`[[++emailsender]]`
                &validate=`name:required,phone:required,g-recaptcha-response:required`
                &validationErrorMessage=`В форме содержатся ошибки!`
                &successMessage=`Сообщение успешно отправлено`
                ]]
            {/literal}
        </div>
    </div>
{/block}