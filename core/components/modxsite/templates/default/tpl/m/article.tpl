{extends file="m_layout.tpl"}

{block name=content}
{literal}

  
<style>
    .action_event .header_action_info{
        margin-top:100px;
    }
    .action_event .body-container, .action_event .product-container,.action_event #main-container{
        padding-top:0px;
    }
    
</style>



<div class="body-container">
[[m_BreadCrumb]]
      <div class="text-block col-xs-9 padding-lr"  >
        
        
         {/literal}
			  <h1>{field name=pagetitle}</h1>
			  {literal}
			  
			   [[*content]]
			  
 <div class="fb-commnets">
          <div id="disqus_thread"></div>
          <script type="text/javascript">
            /* * * CONFIGURATION VARIABLES: EDIT BEFORE PASTING INTO YOUR WEBPAGE * * */
            var disqus_shortname = 'petchoice'; // required: replace example with your forum shortname

            /* * * DON'T EDIT BELOW THIS LINE * * */
            (function() {
              var dsq = document.createElement('script'); dsq.type = 'text/javascript'; dsq.async = true;
              dsq.src = '//' + disqus_shortname + '.disqus.com/embed.js';
              (document.getElementsByTagName('head')[0] || document.getElementsByTagName('body')[0]).appendChild(dsq);
            })();
          </script>
          <noscript>Please enable JavaScript to view the <a href="https://disqus.com/?ref_noscript">comments powered by Disqus.</a></noscript>

        </div>
      
      </div>
	  <br style="clear:both"/>
</div>
{/literal}

{/block}
