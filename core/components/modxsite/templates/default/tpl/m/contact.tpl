{extends file="m_layout.tpl"}

{block name=content}

<style>
    .action_event .header_action_info{
        margin-top:100px;
    }
    .action_event .body-container, .action_event .product-container,.action_event #main-container{
        padding-top:0px;
    }
    
</style>


	{literal}
	<div class="body-container">
      
        <div class="page-contact">
          [[m_BreadCrumb]]
          <div class="text-block padding-lr" itemscope itemtype="http://schema.org/Organization">
          <div class="row equal-container">
              <div class="col-xs-6 equal-column blue-separator">
                  <div class="h1">Наши контакты</div>
                 
                 
                  <div class="row" style="padding:15px 15px 0px 15px;">
                    <!--<div class="col-xs-7">-->
                        <div class="h3 green" style="margin-bottom:15px"><b>Контактные телефоны</b></div>
                        <p>
                          <span class="icon phone-icon"></span>&nbsp;&nbsp;&nbsp;
                          <span class="h3" itemprop="telephone">(048) 700-50-80</span>
                        </p>
                        <p>
                          <span class="icon life-icon"></span>
                          <span class="h3" itemprop="telephone">(063) 505-50-03</span>
                        </p>
                        <p>
                          <span class="icon kyivstar-icon"></span>
                          <span class="h3" itemprop="telephone">(067) 841-64-64</span>
                        </p>
                        <p>
                          <span class="icon mts-icon"></span>
                          <span class="h3" itemprop="telephone">(095) 505-65-95</span>
                        </p>
                   
                  </div>
                  
                    <div class="h3 green" style="margin-bottom:15px"><b>График работы (интернет-магазин)</b></div>
                  <p itemprop="address" itemscope itemtype="http://schema.org/PostalAddress">
                    <span class="h3"  itemscope itemtype="http://schema.org/LocalBusiness">
                      
    					<time itemprop="openingHours" datetime="Mo-Fr 09:00-18:00">Пн-Пт: 9:00 - 18:00</time><br />
<time itemprop="openingHours" datetime="Sa-Su 9:00-15:00">Суббота: 9:00 - 15:00</time>
                      
                    </span>
                  </p>
                  
                  <br>
                  
             
                  
                   <div class="h3 green"><b>Адрес магазина (Приморский р-н)</b></div>
                  <p itemprop="address" itemscope itemtype="http://schema.org/PostalAddress">
                    <span class="h3"  itemprop="streetAddress">
                      [[-*address:nl2br]]
                     <span itemprop="addressCountry">Украина</span>, <span itemprop="addressLocality">г.Одесса</span>, 
    				 <span itemprop="streetAddress">Картамышевская 40/1</span>, 
                        Индекс: <span itemprop="postalCode">65091</span>
                    </span>
                  </p>
             
                  <p itemprop="address" itemscope itemtype="http://schema.org/PostalAddress">
                    <span class="h3"  itemscope itemtype="http://schema.org/LocalBusiness">
                      
						<time itemprop="openingHours" datetime="Mo-Fr 09:00-20:00">Пн-Пт: 9:00 - 20:00</time><br />
<time itemprop="openingHours" datetime="Sa-Su 10:00-19:00">Сб-Вс: 10:00 - 19:00</time>
                      
                    </span>
                  </p>
                  
                  <br>
                  
                   <div class="h3 green"><b>Адрес магазина (Суворовский р-н)</b></div>
                  <p itemprop="address" itemscope itemtype="http://schema.org/PostalAddress">
                    <span class="h3"  itemprop="streetAddress">
                      [[-*address:nl2br]]
                     <span itemprop="addressCountry">Украина</span>, <span itemprop="addressLocality">г.Одесса</span>, 
    				 <span itemprop="streetAddress">Высоцкого 12</span>, 
                        Индекс: <span itemprop="postalCode">65086</span>
                    </span>
                  </p>
           
                  <p itemprop="address" itemscope itemtype="http://schema.org/PostalAddress">
                    <span class="h3"  itemscope itemtype="http://schema.org/LocalBusiness">
                      
						<time itemprop="openingHours" datetime="Mo-Su 10:00-20:00">Пн-Вс: 10:00 - 20:00</time>
                      
                    </span>
                  </p>
                  
                  <br>

                 
                  <br>
                  
              </div>
              <div class="col-xs-6 equal-column green-separator">
                  <div class="h1">Написать нам</div>
                  [[!+fi.validation_error_message:notempty=`<p>[[!+fi.validation_error_message]]</p>`]]
                  [[!AjaxForm?
                    	&snippet=`FormIt`
                    	&form=`contactFormTpl`
                    	    &hooks=`recaptchav2,email`
                    	&emailSubject=`[[++site_name]] | Получено новое сообщение...`
                    	&emailTo=`[[++emailsender]]`
                    	&validate=`email:required,message:required,g-recaptcha-response:required`
                    	&validationErrorMessage=`В форме содержатся ошибки!`
                    	&successMessage=`Сообщение успешно отправлено`
                    ]]
              </div>
          </div>
          
          </div>
          
        </div>
		<br style="clear:both;"/>
      </div>
{/literal}

{/block}