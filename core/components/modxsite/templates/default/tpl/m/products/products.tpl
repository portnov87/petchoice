
{if (count($products)>0)}
<div class="main_products">
    <div class="product_list">    
        {foreach $products as $product}
    
            {include file='tpl/m/products/productRowTpl.tpl'}
        	{* {if (($showleshebniy==1)&&($product.showleshebniy==1))}
        		{$htmlbanner}
        	{/if} *}
        {/foreach}    
    </div>


</div>


    <script>
        {literal}
        dataLayer.push({
            'ecommerce': {
                'currencyCode': 'UAH',
                'impressions': [{/literal}{$ecommerce}{literal}]
            },'event': 'gtm-ee-event',
            'gtm-ee-event-category': 'Enhanced Ecommerce',
            'gtm-ee-event-action': 'Product Impressions',
            'gtm-ee-event-non-interaction': 'True',
        });
        {/literal}
    </script>

{else}
<p>[[%polylang_site_category_no_found]]</p>
{/if}
