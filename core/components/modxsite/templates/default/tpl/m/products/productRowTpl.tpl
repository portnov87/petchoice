
 <div class="big-prev-block" data-list="{$product.list}" data-name="{$product.name}" data-brand="{$product.brand}" data-category="{$product.category}" data-price="{$product.min_price}" data-position="{$product.position}" id="list_product_{$product.id}">
        
    <div class="preview-container">
        <input type="hidden" name="product_id" value="{$product.id}" />              
                            {snippet name=m_isaction params="id={$product.id}"}
                        
                        <div class="prev-image">
    					    {snippet name=pthumb params="input=`{$product.image}`&options=`w=140&h=180&far=0`&useResizer=`0`&zc=1" assign=preview}               
                            <a data-type="action_product" data-id="{$product.id}" href="{snippet name=PolylangMakeUrl params="id=`{$product.id}`"}">
                                <img src="{$preview}" alt="{$product.pagetitle}">
                            </a>
                        </div>
                        <div class="prev-name">
                            <a data-type="action_product" data-id="{$product.id}" href="{snippet name=PolylangMakeUrl params="id=`{$product.id}`"}">
                              {$product.pagetitle}
                            </a>
                        </div>
                        
                
                        <div class="product-item-code small">
                              [[%polylang_site_product_label_code]]: {$product.article}
                        </div>
                            
                        <div class="rating_product">
                           {if ($product.rating!='')}
                            	<div class="product-item-stars">
                    			    {$product.rating}
                                </div>    
                    			{if ($product.countcom!='')}
                    				<span class="icon small-use-icon"></span>
                    				<span class="count text-bottom">{$product.countcom}</span>
                    			{/if}
                    	  {/if}
                        </div>                        
                        {include file='tpl/m/products/optionsAction.tpl'}                        
    </div>
</div>