{snippet name=msCartKey params="id=`{$attr.idproduct}`&option_id=`{$attr.id_1c}`&weight=``&size=`{$attr.weight} {$attr.weight_prefix}`&price=`{$attr.price}`" assign=key}
{snippet name=msCartKey params="id=`{$attr.idproduct}`&option_id=`{$attr.id_1c}`&weight=``&size=`{$attr.weight} {$attr.weight_prefix}`&price=`{$attr.price}`" assign=msCartKey}


<li>
    <div class="weight_wrap">
        <div class="buy-type">{$attr.weight} {$attr.weight_prefix}</div>

        
            {if ($attr.markdown_text!='')}
                <div class="sale-from-type">
                    <span class="grey small">{$attr.markdown_text}</span>
                </div>
            {else}
                {if (($modx->resource->parent==15)||($modx->resource->parent==19)||($modx->resource->parent==59)||($modx->resource->parent==57))}
                    {if ($attr.price_kg!='')}<div class="sale-from-type"><span class="grey small" style="float:right;">{$attr.price_kg}  грн / кг</span></div>{/if}
                {/if}
            {/if}
        
    </div>

<!--    <div class="buy-type">{$attr.weight} {$attr.weight_prefix}</div>
 style="margin-left:100px;"-->

    <div class="icon_wrap">
        {if ($attr.markdown)}
            <div class="equal-height"><span style="" class="icon markdown-icon"></span></div>
        {/if}
        {if ($attr.by_weight)}
            <div class="equal-height"><span style="" class="icon by_weight-icon"></span></div>
        {/if}
        {if ($attr.dostavka>0)}
            <div class=" equal-height"><span style="" class="icon free-icon"></span></div><!--col-xs-3-->

        {/if}

    <!--style="margin-left:90px;"-->
        <!-- style="margin-left:5px;margin-right:5px;padding:2px 1px; background-color: #a0489c;color:#fff;font-size:13px;"-->
        {if (($attr.action==1)&&($attr.action_number>0))}
            <div class="col-xs-2 equal-height action_number" >
                <span>-{$attr.action_number}%</span>
            </div>
        {/if}

    </div>

    {if ($attr.instock==0)}
        <div class="product-buy-wrapper product-none">
            <div class="wrapper_price_product_one">
                    <div class="price uah">{$attr.price}</div>
            </div>
            <a href="#boxStatus" class="no_store_button" data-article="{$attr.articleproduct}"
               data-product="{$attr.nameproduct}, {$attr.weight} {$attr.weight_prefix}" data-id="{$attr.id}">[[%polylang_site_product_button_askqty]]</a>
        </div>
    {else}
        <div class="product-buy-wrapper">
            {if ($attr.action==1)}
                <div class="wrapper_price_product_one oldpr">
                    <div class="old-price uah">{$attr.oldprice}</div>
                    <div class="price uah">{$attr.price}</div>
                    <!--float:right;clear:none;margin-top:8px;-->
                </div>
            {else}
                <div class="wrapper_price_product_one">
                    <div class="price uah">{$attr.price}</div>
                    <!--float:right;clear:none;-->
                </div>
            {/if}

            <div class="btn-buy">
                {snippet name=inCartCount params="key=`{$msCartKey}`" assign=inCartCount}
                <input name="sub-product-count{$attr.idx}" class="form-control custom-inpt count-products" type="hidden" min="1"
                       value="{$inCartCount}"/>
                <button data-product-type="page"
                        data-product-vendor="{$attr.product_vendor}"
                        data-product-category="{$attr.category_ec}"
                        data-product-price="{$attr.price}"
                        data-option-id="{$attr.id_1c}"
                        data-product-name="{$attr.nameproduct}"
                        data-product-id="{$attr.idproduct}" data-size="{$attr.weight} {$attr.weight_prefix}"
                        data-key="{$msCartKey}" data-count="1" data-action="cart/add" href="#product-quantity-pu" type="button"
                        class="btn-product-buy ">[[%polylang_site_product_button_buy]]
                </button>
            </div>
        </div>
    {/if}
</li>
           
           
		  