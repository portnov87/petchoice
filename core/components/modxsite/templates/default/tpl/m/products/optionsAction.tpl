<div class="all_options" style="{if (count($product.options.outputtabs)==1)} grid-template-columns: 1fr; {/if}">
{if (count($product.options.outputtabs)>1)}
    <span class="prev_nav"></span>
    {/if}
    <ul class="nav nav-tabs"  role="tablist" id="productTab{$product.id}">
			{foreach $product.options.outputtabs as $tab}
			    {include file='tpl/m/products/productActionOptionWeightRow.tpl'}
			{/foreach}
    </ul>
    {if (count($product.options.outputtabs)>1)}
    <span class="next_nav"></span>
    {/if}
</div>

    {foreach $product.options.outputtabs as $tab} 
    
            <div id="category_product_{$product.id}_{$tab.id_1c}" class="category_option_block" {if ($tab.idx===1)}  style="display:block"{/if}>
            
            
                    {snippet name=msCartKey params="id=`{$product.id}`&option_id=`{$tab.id_1c}`&weight=``&size=`{$tab.weight} {$tab.weight_prefix}`&price=`{$tab.price}`" assign=key}
                    {if ($tab.instock==0)}
                        <div class="product-buy-wrapper product-none">                           
                            <a href="#boxStatus" class="no_store_button" data-article="{$tab.articleproduct}"
                               data-product="{$tab.nameproduct}, {$tab.weight} {$tab.weight_prefix}" data-id="{$tab.id}">[[%polylang_site_product_button_askqty]]</a>
                        </div>
                    {else}
                    
                     <div class="prev-buy-btn">
                                
                                    <a data-product-type="page" 
                                    data-product-vendor="{$product.brand}" 
                                    data-product-category="{$product.category}" 
                                    data-product-price="{$product.price}" 
                                    data-option-id="{$tab.id_1c}" 
                                    data-product-name="{$product.name}" 
                                    data-product-id="{$product.id}" 
                                    data-size="{$tab.weight} {$tab.weight_prefix}" 
                                    data-key="{$key}" 
                                    data-count="1" 
                                    data-action="cart/add" href="#product-quantity-pu" type="button" class="btn-product-buy">[[%polylang_site_product_button_basket]]</a>
                                
                                
                                    <!--<a data-id="{$product.id}" data-type="action_product"   href="{snippet name=PolylangMakeUrl params="id=`{$product.id}`"}">[[%polylang_site_product_button_buy]]</a>-->
                    </div>
                    {/if}
            </div>
    {/foreach}