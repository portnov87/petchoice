
{if (count($recomproducts)>0)}
    <p style="padding: 15px;line-height: 1.5;font-weight:700;">Рекомендовані товари:</p>

<div class="main_products"><div class="product_list">
    {foreach $recomproducts as $product}
        {include file='tpl/m/products/productRowTpl.tpl'}
    {/foreach}
    </div>
</div>
    <br/>
{/if}
<p style="padding: 15px;line-height: 1.5;">[[%polylang_site_searchcategory_label]]':
    {$categories}
</p>
{if (count($products)>0)}
<div class="main_products"><div class="product_list">
<!--<ul class="big-prevs-list">-->
{foreach $products as $product}
	{include file='tpl/m/products/productRowTpl.tpl'}
	{if (($showleshebniy==1)&&($product.showleshebniy==1))}
		{$htmlbanner}
	{/if}
{/foreach}
<!--</ul>-->
</div>
</div>
{else}
<p>[[%polylang_site_category_no_found]]</p>
{/if}
