<div class="title_block">
    [[%polylang_site_main_lable_brand]]
</div>
<div class="wrapper_brands">
    <div class="swiper-container logos_galerey">
        <div class=" swiper-wrapper logos-table" style="">
        
        
            {foreach $brands as $brand}
                {snippet name=pthumb params="input=`{$brand.logo}`&options=`w=140&h=140&far=1`&useResizer=`1`" assign=preview}
                <div class="swiper-slide">
                    <a href="{$brand.url}"><img src="{$preview}" alt="{$brand.name}" /></a>
                </div>
            {/foreach}
        </div>
        <div class="swiper-button-next"></div>
        <div class="swiper-button-prev"></div>

    </div>
    <div class="show_more">
        <a href="{$brandsurl}">[[%polylang_site_main_lable_all_brand]]</a>
    </div>
</div>