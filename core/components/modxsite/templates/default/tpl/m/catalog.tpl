{extends file="m_layout.tpl"}

{block name=content}
    {snippet name=esputnikcategory params="id={field name=id}"}
<style>
    .action_event .header_action_info{
        margin-top:100px;
    }
    .action_event .product-container,.action_event #main-container{
        padding-top:0px;
    }
    
    /*.big-prevs-container {
        padding-top:100px;
}*/
</style>

<div class="big-prevs-container">
        
    	[[m_BreadCrumb? &filter=`1`]]
		
        <div class="wrapper_sort">
            <div class="block_sort">
                <a href="#sort-pu">
                [[+mse2_sort:is=`pagetitle`:then=`[[%polylang_site_category_sort_alphavit]]`]]
                [[+mse2_sort:is=`hits`:then=`[[%polylang_site_category_sort_popular]]`]]
                [[+mse2_sort:is=`adiscount`:then=`[[%polylang_site_category_sort_adiscount]]`]]
                </a>
            </div>
            <div class="block_filter btn-filters">
                <a href="#filters-pu">[[%polylang_site_label_filters]]</a>
            </div>
            
        </div>
        <h1>{$title}</h1>
        
        <!--<div id="sort-by-form">
        
				<form action="">
					<label for="sort-by">[[%polylang_site_category_sort_label]]:</label>
					<div id="mse2_sort">
						<select id="sort-by" >
							<option value="" selected class="sort filter-link [[+mse2_sort:is=`ms|hits:asc`:then=`up-sort active`:elseis=`ms|hits:desc`:then=`down-sort active`:else=`down-sort active`]]" data-sort="ms|hits" data-dir="[[+mse2_sort:is=`ms|hits:asc`:then=`asc`]][[+mse2_sort:is=`ms|hits:desc`:then=`desc`]]" data-default="desc">[[%polylang_site_category_sort_popular]]</option>
							
							<option value="" class="sort filter-link [[+mse2_sort:is=`ms_product|pagetitle:asc`:then=`up-sort active`]] [[+mse2_sort:is=`ms_product|pagetitle:desc`:then=`down-sort active`]] " data-sort="ms_product|pagetitle" data-dir="[[+mse2_sort:is=`ms_product|pagetitle:asc`:then=`asc`]][[+mse2_sort:is=`ms_product|pagetitle:desc`:then=`desc`]]" data-default="asc">[[%polylang_site_category_sort_alphavit]]</option>
		
							
						</select>
						
					</div>
				</form>
        </div>-->
		
		{assign var=params value=[	
		"element"=>'msProducts'
        ,"where"=>[
			"class_key"=>"msProduct"
		]
        ,"filters"=>'
            tv|product_options:number,
            ms|vendor:vendors,
            ms|adiscount,
            ms|askidka,ms|helloween,
            ms|black_friday,
            ms|markdown'
        ,"paginator"=>'getPage'
        ,"tplOuter"=>'filterTpl'
        ,"tplFilter.outer.default"=>'filterGroupTpl'
        ,"tplFilter.row.default"=>'filterRowTpl'
        ,"tplFilter.outer.ms|price"=>'filterPriceOuterTpl'
        ,"tplFilter.row.ms|price"=>'filterPriceTpl'
        ,"tplFilter.outer.tv|product_options"=>'filterPriceOuterTpl'
        ,"tplFilter.row.tv|product_options"=>'filterPriceTpl'
        ,"tplFilter.outer.ms|vendor"=>'filterVendorOuterTpl'
        ,"tpls"=>'m_productRowTpl'
        ,"limit"=>"20"					  
					]}
					
		{snippet name=mFilter2_m params=$params}
	
	</div><!-- paginatorpage.big-prevs-container -->
	
<div class="windows-popup-container catalogfilter">
</div>
<!-- windows popup -->
<div class="windows-popup-container product-quantity-pu">
    <div id="product-quantity-pu" class="window-popup"><span class="btn-close"></span>
        <header class="popup-header">
            <h2>[[%polylang_site_cart_qty_label]]</h2>
        </header>
		<form class="ms2_form" method="post" onsubmit="dataLayer.push({
	'event': 'event-GA',
	'eventCategory' : 'product',
	'eventAction' : 'buy-preview'
	});">
                <input type="hidden" name="product_id" value="" />
        <div class="quantity">
            <input class="input-quantity" type="number" name="countcart" value="1" />
            <span class="number-minus"></span>
            <span class="number-plus"></span>
        </div>
        <div class="btn-orange">
            <button type="submit"  class="pay-btn orange-btn pull-right text-uppercase product add-cart">[[%polylang_site_product_button_basket]]</button>
        </div>
		<input type="hidden" value="" id="idproductcart"/>
		<input type="hidden" value="" id="productsize"/>
        <input type="hidden" value="" id="optionId"/>
            <input type="hidden" value="" id="product_price"/>
            <input type="hidden" value="" id="product_category"/>
            <input type="hidden" value="" id="product_vendor"/>
            <input type="hidden" value="" id="product_name"/>
		<input type="hidden" value="cart/add" id="action"/>
		<input type="hidden" value="" name="key" id="keycart"/>
		</form>
		
    </div>
</div>



<div class="windows-popup-container boxStatus">
    <div id="boxStatus" class="window-popup"><span class="btn-close"></span>
        <header class="popup-header">
            <h2 style="margin-top:0px;">[[%polylang_site_product_button_askqty]]:</h2>
        </header>
		
		{literal}
		 [[!AjaxForm?
                	&snippet=`ask_stock`
                	&form=`m_FormAsk.tpl`
                	&hooks=`recaptchav2,email`
                	&emailSubject=`Запрос о наличии`
                	&emailTo=`[[++emailsender]]`
                	&validate=`name:required,phone:required,g-recaptcha-response:required`
                	&validationErrorMessage=`В форме содержатся ошибки!`
                	&successMessage=`Сообщение успешно отправлено`
                ]]
		{/literal}
    </div>
</div>



{literal}
{/literal}

	  
	   {/block}