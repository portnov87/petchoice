<div class="product-item small-item ms2_product"  data-list="{$product.list}" data-name="{$product.name}" data-brand="{$product.brand}" data-category="{$product.category}" data-price="{$product.min_price}" data-position="{$product.position}" id="recent_product_{$product.id}">
<input type="hidden" name="product_id" value="{$product.id}" />
<div class="product-item-img">

{snippet name=pthumb params="input=`{$product.image}`&options=`w=150&h=223&far=1`&useResizer=`1`" assign=preview}

  <a data-id="{$product.id}" data-type="recent_product" href="{link id={$product.id}}"><img src="{$preview}"  alt="{$product.pagetitle}" /></a>
  
  <div class="product-item-lbls">
  
  
  
    {if ($product.popular==1)}
			  <div class="product-info-lbl top-sales-lbl"><b>ТОП</b> продаж</div>
			  {/if}
			  
			  {if ($product.new==1)}
			  <div class="product-photo-lbl new-product-lbl"><b>НОВИНКА</b></div>
			  {/if}
  {if ($product.toorder==1)}
			  <div class="product-photo-lbl toorder-lbl">ПОД ЗАКАЗ</div>
			  {/if}
  {if ($product.isaction==1)}
			  <div class="product-photo-lbl action-lbl"><b>АКЦИЯ</b></div>
			  {/if}
			  
			  {if ($product.action!='')}
			  <div class="product-info-lbl discount-lbl">СКИДКА <b>-{$product.action}%</b></div>
			  {/if}
    
  </div>
</div>
<div class="product-item-title"><a data-type="recent_product" data-id="{$product.id}" href="{link id={$product.id}}">{$product.pagetitle}</a></div>
{if ($product.rating!='')}
			<div class="product-item-stars">
                {$product.rating}
            </div>
{/if}
			
<div class="tabs product-item-tabs" role="tabpanel">
 
 
 
	  {include file='tpl/products/optionsAction.tpl'}
                
</div>
</div>
