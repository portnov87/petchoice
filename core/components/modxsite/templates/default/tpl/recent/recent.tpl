<div class="saw-products">
  <p class="title-block">Недавно просмотренные</p>
  
  {foreach $rows as $row}   
{assign var=product value=$row}  
						{include file='tpl/recent/productRecent.tpl'}
	{/foreach}
</div>
{literal}
<script>

    dataLayer.push({
        'ecommerce': {
            'currencyCode': 'UAH',
            'impressions': [{/literal}{$ecommerce}{literal}]
        },'event': 'gtm-ee-event',
        'gtm-ee-event-category': 'Enhanced Ecommerce',
        'gtm-ee-event-action': 'Product Impressions',
        'gtm-ee-event-non-interaction': 'True',
    });
</script>
{/literal}
