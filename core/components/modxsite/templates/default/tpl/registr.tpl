{extends file="layout.tpl"}

{block name=content}

    <div class="container">
        <div class="page-discount">
            {snippet name=BreadCrumb params="scheme=`full`&containerTpl=`breadcrumbOuterTpl`&currentCrumbTpl=`breadcrumbCurrentTpl`&linkCrumbTpl=`breadcrumbLinkTpl`&homeCrumbTpl=`breadcrumbLinkTpl`"}
            <br/>
            <br/>
            <div class="row equal-container">
                <div class="col-xs-6 equal-column green-separator">
                    <div class="">
                {literal} [[!Register?
                    &submitVar=`registerbtn`
                    &activationResourceId=`611`
                    &activationEmailSubject=`Подтверждение регистрации`
                    &successMsg=`Вам нужно подтвердить свою регистрацию.`
                    &usernameField=`phone`
                    &usergroups=`3:Super User`
                    &activation=`0`
                    &autoLogin=`1`
                    &submittedResourceId=`7`
                    &validate=`
                    password:required:minLength=^6^,
                    password_confirm:password_confirm=^password^,
                    fullname:required,
                    phone:required,
                    email:required:email`
                    &placeholderPrefix=`reg.`
                    ]]
                    <div class="">
                        <div class="h1">[[%polylang_site_registr_new]] <small class="inline"><a href="[[~8]]">[[%polylang_site_registr_already]]</a></small>
                        </div>
                        <br>
                        [[!+error.message]]
                        [[!+successMsg]]
                        <div class="registerMessage">[[!+reg.error.message]]</div>
                        <form autocomplete="off" action="[[~[[*id]]]]" onsubmit="ga('send', 'event', 'client', 'registration');"
                              class="formaregistration" method="post" data-toggle="validator" role="form">
                            <div class="row">
                                <div class="col-xs-8">
                                    <div class="form-group [[!+reg.error.fullname:notempty=`has-error`]]">
                                        <label class="control-label">[[%polylang_site_order_name]] <sup>*</sup></label>
                                        <input name="fullname" type="text" class="form-control custom-inpt"
                                               data-error="[[%polylang_site_form_name_lable]]" value="[[!+reg.fullname]]"
                                               placeholder="Иван" required>
                                        <div class="help-block with-errors hide1">[[%polylang_site_form_name_lable]]</div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label">[[%polylang_site_order_lastname]]</label>
                                        <input name="lastname" type="text" class="form-control custom-inpt"
                                               data-error="введите вашу фамилия" value="[[!+reg.lastname]]" placeholder="Иванов">
                                    </div>
                                    <div class="form-group [[!+reg.error.phone:notempty=`has-error`]]">
                                        <label class="control-label">[[%polylang_site_order_phone]] <sup>*</sup></label>
                                        <input type="text" name="phone" class="form-control custom-inpt" placeholder="+38"
                                               data-error="введите ваш телефон" value="" required>
                                        <div class="help-block with-errors hide1"><!--введите ваш телефон-->[[!+reg.error.phone]]</div>
                                        <div class="help-block small grey">
                                            <ul class="list-unstyled">
                                                [[%polylang_site_registr_login]]

                                            </ul>
                                        </div>


                                    </div>
                                    <div class="form-group [[!+reg.error.email:notempty=`has-error`]]">
                                        <label class="control-label">Email <sup>*</sup></label>
                                        <input name="email" type="email" autocomplete="off" class="form-control custom-inpt"
                                               value="[[-!+reg.email]]" data-error="введите вашу почту" required>
                                        <div class="help-block with-errors hide1"><!--введите вашу почту-->[[!+reg.error.email]]</div>
                                        <div class="help-block small grey">[[%polylang_site_registr_nospam]]</div>

                                    </div>
                                    <input name="code" type="text" autocomplete="off" class="codeh" value="">
                                    <div class="checkbox">
                                        <input type="checkbox" name="subscribe" id="subscribe" value="1" checked/>
                                        <label for="subscribe" class="">
                                            <span>[[%polylang_site_registr_proposal]]</span>
                                        </label>
                                    </div>
                                    <div class="form-group [[!+reg.error.password:notempty=`has-error`]]">
                                        <label class="control-label">Пароль <sup>*</sup></label>
                                        <input id="password" name="password" autocomplete="off" type="password"
                                               class="form-control custom-inpt" value="[[-!+reg.password]]" data-error="введите пароль"
                                               required>
                                        <div class="help-block with-errors hide1"><!--введите пароль-->[[!+reg.error.password]]</div>
                                        <div class="help-block small grey">[[%polylang_site_registr_symbol]]</div>
                                    </div>
                                    <div class="form-group [[!+reg.error.password_confirm:notempty=`has-error`]]">
                                        <label class="control-label">[[%polylang_site_registr_confpass]] <sup>*</sup></label>
                                        <input name="password_confirm" type="password" class="form-control custom-inpt"
                                               data-error="[[%polylang_site_registr_confpass]]" value="[[-!+reg.password_confirm]]"
                                               required>
                                        <div class="help-block with-errors hide1"><!--подтвердите пароль-->
                                            [[!+reg.error.password_confirm]]
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <input name="discount" type="hidden" value="5">
                            <input type="submit" value="Зарегистрироваться" name="registerbtn" class="btn orange-btn"/>
                            <p class="form-control-static inline grey">
                                <sup>*</sup> - [[%polylang_site_registr_necessarily]]
                            </p>
                        </form>
                    </div>
                    <!-- Forgot Password Modal -->
                    <div class="modal fade" id="forgotPasswordModal" tabindex="-1" role="dialog" aria-labelledby="basketModalLabel"
                         aria-hidden="true">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"></button>
                                    <h3 class="modal-title greener"><b>[[%polylang_site_registr_restore]]</b></h3>
                                </div>
                                <form action="" data-toggle="validator" role="form">
                                    <div class="modal-body">
                                        <div class="form-group">
                                            <label class="control-label">Email <sup>*</sup></label>
                                            <input type="email" class="form-control custom-inpt" required>
                                        </div>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="submit" class="btn orange-btn">[[%polylang_site_submit]]</button>
                                        <button type="button" class="btn link-btn light" data-dismiss="modal">
                                            [[%polylang_site_registr_cancel]]
                                        </button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                    <!-- end Forgot Password Modal -->

                    </div>
                    <br>
                </div>
                <div class="col-xs-6 equal-column blue-separator">
                    [[%polylang_site_registr_info]]
                </div>
            </div>
        </div>
    </div>
{/literal}

{/block}