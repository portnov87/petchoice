{extends file="layout.tpl"}

{block name=content}
{literal}
      <div class="container">
        <div class="page-contact">
          [[BreadCrumb? &scheme=`full` &containerTpl=`breadcrumbOuterTpl` &currentCrumbTpl=`breadcrumbCurrentTpl` &linkCrumbTpl=`breadcrumbLinkTpl` &homeCrumbTpl=`breadcrumbLinkTpl`]]
          <div itemscope itemtype="http://schema.org/Organization">
          <div class="row equal-container">
              <div class="col-xs-7 equal-column blue-separator">

                  [[%polylang_site_contact_text]]
              </div>
              <div class="col-xs-5 equal-column green-separator">
                  <div class="h1">[[%polylang_site_contact_ustext]]</div>
                  [[!+fi.validation_error_message:notempty=`<p>[[!+fi.validation_error_message]]</p>`]]
                  [[!AjaxForm?
                    	&snippet=`FormIt`
                    	&form=`contactFormTpl`
                    	&hooks=`recaptchav2,email`
                    	&emailSubject=`[[++site_name]] | Получено новое сообщение...`
                    	&emailTo=`[[++emailsender]]`
                    	&validate=`email:required,message:required,g-recaptcha-response:required`
                    	&validationErrorMessage=`В форме содержатся ошибки!`
                    	&successMessage=`Сообщение успешно отправлено`
                    ]]
                    <!--recaptchav2,-->
              </div>
          </div>
          
          </div>
          
        </div>
      </div>
	  {/literal}
	  {/block}
