{extends file="layout.tpl"}

{block name=content}

{snippet name=esputnikproduct params="id={field name=id}"}
{snippet name=view_recent}


{literal}

    {/literal}

  <div class="container">

{snippet name=BreadCrumb params="scheme=`full`&containerTpl=`breadcrumbOuterTpl`&currentCrumbTpl=`breadcrumbCurrentTpl`&linkCrumbTpl=`breadcrumbLinkTpl`&homeCrumbTpl=`breadcrumbLinkTpl`"}

{field name=infoproduct assign=infoproduct}

	  {field name=info_action assign=infoaction}
{field name=nodostavka assign=nodostavka}
{assign var=params value=["dostavka" => {$nodostavka}]}

{processor action="site/web/products/getoptionslist" ns="modxsite" params=$params assign=resultproduct}
<script>
    {$resultproduct.object.ecomerc}
</script>
<div  itemscope itemtype="http://schema.org/Product">
    <div class="row" >
      <div class="col-xs-9">
        <h1 itemprop="name">{field name=pagetitle}</h1>
         <meta content="/{link id={field name=id}}" itemprop="url" />
		 
		 
		 {assign var=article value={$resultproduct.object.article}}
		 {assign var=vendor value={$resultproduct.object.vendor}}
		 
		 {assign var=popular value={$resultproduct.object.popular}}
		 {assign var=new value={$resultproduct.object.new}}
			 {if ({$article}!='')}
			 <span class="h3 grey">[[%polylang_site_product_label_code]]:
							  <span  itemprop="sku">{$article}</span>
						   </span>
			{/if}
        
        <div class="row product-card-content">
          <div class="col-xs-5 product-photos">
		  {snippet name=msGallery_new}
            <!-- Labels for item photo  -->
            <div class="product-item-lbls" >			
            {if ($resultproduct.object.helloween==1)}
            <div class="product-item-helloween" ></div>{/if}
			{if ({$popular}==1)}<div class="product-photo-lbl top-sales-lbl">[[%polylang_site_product_label_topprd]]</div>{/if}
			{if ($resultproduct.object.action_product==1)}
			<div class="product-photo-lbl action-lbl"><b>[[%polylang_site_product_actionblock]]</b></div>
			{/if}
			
			{if ($resultproduct.object.toorder==1)}
			<div class="product-photo-lbl toorder-lbl">[[%polylang_site_product_label_orderpr]]</div>
			{/if}
			{if ($resultproduct.object.action_product_value!=0)}
			<div class="product-photo-lbl discount-lbl {if ($resultproduct.object.black_friday==1)} product-info-black_friday {/if}{if ($resultproduct.object.newyear==1)} product-info-newyear {/if}">[[%polylang_site_discount_label]]<br/><b>-{$resultproduct.object.action_product_value}%</b></div>
			{/if}
			  {if ($new==1)}<div class="product-photo-lbl new-product-lbl">[[%polylang_site_product_label_newpr]]</div>{/if}
            </div>

    			{if (($resultproduct.object.bonuses!='')&&($resultproduct.object.bonuses>0))}
					  <div class="cashback cb_dt">
						  <div class="cashback_balloon">
							  <span>{$resultproduct.object.bonuses}</span>
						  </div>
						  <div class="cashback_info">
							  <div>
								  [[%polylang_site_product_bonus_info? &bonuses={$resultproduct.object.bonuses}
								  &size_bonuses={$resultproduct.object.size_bonuses}
								  &maxpercentorder={$resultproduct.object.maxpercentorder}]]

		{*						  Возвращаем <span>{$resultproduct.object.bonuses} грн</span> на бонусный счет при покупке <span>{$resultproduct.object.size_bonuses}</span><br>*}
		{*						  Оплачивайте бонусами до {$resultproduct.object.maxpercentorder}% от стоимости заказа*}
							  </div>
						  </div>
					  </div>
                {/if}
          </div>
		  
          <div class="col-xs-7">

				  {if ($infoaction!='')}
				   <div class="info-product-label">
							  <div class="row equal-height-wrapper">
								<div class="col-xs-2 left-part-label text-center equal-height">
								  <span class="h3 red">[[%polylang_site_product_actionblock]]!</span>
								</div>
								<div class="col-xs-10 equal-height">
									{$infoaction}
								</div>
							  </div>
							</div>
				{/if}
		  
               
               {if ($infoproduct!='')}
			   
			   		 <div class="info-product-label">
                      <div class="row equal-height-wrapper">
                        <div class="col-xs-2 left-part-label text-center equal-height">
                          <img src="/assets/images/icon_info.png" alt="information" />
                        </div>
                        <div class="col-xs-10 equal-height">
                          {$infoproduct}
                            {if (($resultproduct.object.bonuses!='')&&($resultproduct.object.bonuses>0))}
								[[%polylang_site_product_bonus_info? &bonuses={$resultproduct.object.bonuses}
								&size_bonuses={$resultproduct.object.size_bonuses}
								&maxpercentorder={$resultproduct.object.maxpercentorder}]]

{*								Возвращаем <span>{$resultproduct.object.bonuses} грн</span> на бонусный счет при покупке <span>{$resultproduct.object.size_bonuses}</span><br>*}
{*								Оплачивайте бонусами до {$resultproduct.object.maxpercentorder}% от стоимости заказа*}
							{/if}
                        </div>
                      </div>
                    </div>
				{else}
                   {if (($resultproduct.object.bonuses!='')&&($resultproduct.object.bonuses>0))}
				   <div class="info-product-label">
					   <div class="row equal-height-wrapper">
						   <div class="col-xs-2 left-part-label text-center equal-height">
							   <img src="/assets/images/icon_info.png" alt="information" />
						   </div>
						   <div class="col-xs-10 equal-height">

							   [[%polylang_site_product_bonus_info? &bonuses={$resultproduct.object.bonuses}
							   &size_bonuses={$resultproduct.object.size_bonuses}
							   &maxpercentorder={$resultproduct.object.maxpercentorder}]]
{*								   Возвращаем <span>{$resultproduct.object.bonuses} грн</span> на бонусный счет при покупке <span>{$resultproduct.object.size_bonuses}</span><br>*}
{*								   Оплачивайте бонусами до {$resultproduct.object.maxpercentorder}% от стоимости заказа*}

						   </div>
					   </div>
				   </div>
                   {/if}
			   {/if}
               {if ($nodostavka=='yes')}                 
                 <div class="info-product-label">
                      <div class="row equal-height-wrapper">
                        <div class="col-xs-2 left-part-label text-center equal-height">
                          <img src="/assets/images/icon_info.png" alt="information" />
                        </div>
                        <div class="col-xs-10 equal-height">
                             <p>{config name="textdostavkaproduct"}</p>
                        </div>
                      </div>
                    </div>                
                {/if}    
            <div class="product-card-costs big-item ms2_product">
			{if (!$modx->user->isAuthenticated('web'))}
				{if (($resultproduct.object.action_product_value==0)&&($resultproduct.object.action_product==0))}

				<a class="discountpoplink" style="text-align:right;float:right;margin-bottom:25px;" data-toggle="popover" data-popover-content="#discount-popover" data-placement="left" data-original-title="" title="">[[%polylang_site_product_discount]]</a>
				{/if}
			{/if}
                <form class="ms2_form" method="post">
                <input type="hidden" name="product_id" value="{field name=id}" />
				{foreach $resultproduct.object.outputattr as $attr}
					{include file='tpl/products/option.tpl'}
				{/foreach}
                </form> 
				{if (!$modx->user->isAuthenticated('web'))}
				  <!-- follow popover -->
				  <div id="discount-popover" class="hidden">
					<div class="popover-body">
						[[%polylang_site_product_discount_popover]]
{*					  <p><b>Зарегистрируйтесь и получите скидку уже на первый заказ</b></p>*}
{*						<ul class="list-unstyled green-bullet">*}
{*						<li>5% регистрация</li>*}
{*						<li>7% сумма покупок 5 000 грн</li>*}
{*						<li>10% сумма покупок 10 000 грн</li>*}
{*						</ul>*}
					</div>
				  </div>
				  <!-- end follow popover -->
	  {/if}
	  
	  
				
				 <div class="product-item-stars text-right" itemprop="aggregateRating" itemscope itemtype="http://schema.org/AggregateRating">
		
   {$rating}
   <span class="icon small-use-icon"></span>
   <span class="count text-bottom"><span class='tabcountsreview'>(<span itemprop='ratingCount'>{$countcomments}</span>)</span></span>
   <meta content="{$ratingValue}" itemprop="ratingValue"/>
   <meta content="1" itemprop="worstRating"/>
   <meta itemprop="reviewCount" content="{$countcomments}"/>
   <meta content="5" itemprop="bestRating"/>

				</div>
				
            </div>
          </div>
        </div>
      </div>
      <div class="col-xs-3">
        <div class="right-side-info">
          <div class="content-right-side-info">
			  [[%polylang_site_product_delivery_text]]

{*            <p class="h3 green"><b>Доставка</b></p>*}
{*            <p>*}
{*              <b>По Украине</b>*}
{*              <br>*}
{*              <span class="small">*}

{*                Новая Почта бесплатно от 999 грн. <br />*}
{*                Курьером Новая Почта бесплатно от 1199 грн.*}
{*                *}
{*              </span>*}
{*            </p>*}
{*            <p>*}
{*              <b>По Одессе</b>*}
{*              <br>*}
{*              <span class="small">Бесплатная курьерская достака до двери при заказе от 499 грн.</span>*}
{*            </p>*}
{*            <p class="h3 green"><b>Оплата</b></p>*}
{*            <div class="pay-small-icons">*}
{*              <img src="/assets/images/visa.png" title="Картой Visa" alt="visa" />*}
{*              <img src="/assets/images/master_card.png" title="Картой MasterCard" alt="master_card" />*}
{*              <img src="/assets/images/privat24.png" title="Приват24" alt="privat24" />*}
{*              <img src="/assets/images/cash.png" title="Оплата наличными/Наложенный платеж" alt="express" />*}
{*            </div>*}
{*           *}
{*            <p class="h3 green"><b>Возврат</b></p>*}
{*            <p class="small">*}
{*              Возврат и обмен в течение 14 дней*}
{*              <br><br>*}
{*              <a href="/{link id=473}" target="_blank">Условия доставки и оплаты</a>*}
{*            </p>*}
          </div>
        </div>
      </div>
    </div>

<br style="clear:both;"/>
    <div class="tabs product-tabs" style="position:relative;" role="tabpanel">
         <!--<div style="position:absolute; top:7px;right:15px;width:200px;height:30px;">{literal}<script type="text/javascript">(function() {
  if (window.pluso)if (typeof window.pluso.start == "function") return;
  if (window.ifpluso==undefined) { window.ifpluso = 1;
    var d = document, s = d.createElement('script'), g = 'getElementsByTagName';
    s.type = 'text/javascript'; s.charset='UTF-8'; s.async = true;
    s.src = ('https:' == window.location.protocol ? 'https' : 'http')  + '://share.pluso.ru/pluso-like.js';
    var h=d[g]('body')[0];
    h.appendChild(s);
  }})();</script>{/literal}
             <div class="pluso" data-background="transparent" data-options="medium,square,line,horizontal,nocounter,theme=04" data-services="vkontakte,odnoklassniki,facebook,twitter,google" data-user="280379110"></div>
         </div>-->
         
         <div style="display:table;z-index:9999;position:absolute; top:5px;right:15px;width:300px;height:30px;">

                    {literal}
                        <div style="display:table-cell;height:40px;vertical-align:middle; text-align:center;">
                        
                             <div class="fb-like" data-href="https://petchoice.ua/{/literal}{link id={field name=id}}{literal}" data-layout="button_count" data-action="like" data-size="large"
                                 data-show-faces="false" data-share="false"></div>
                        
                                 
                                 
                                 <button style="display:inline-block;margin-top:-12px;margin-left:10px;background-color: #4267b2;padding: 4px 15px;border: 1px solid #4267b2;" class="shared_facebook btn grey-btn pull-right11" >Поделиться</button>
                                   <!--<a href=""  class="shared_facebook">Поделиться</a>-->
                        </div>
                    {/literal}

                    <div style="display:table-cell;height:40px;padding:0px 20px;vertical-align:middle;text-align:center;">
                        <a class="why-follow" data-toggle="popover" data-popover-content="#shared-popover"
                           data-placement="left" data-original-title="" title="">
                            </a>
                    </div>
                </div>


		 {field name=product_tab_consist assign=product_tab_consist}
{field name=product_tab_feeding assign=product_tab_feeding}
{field name=product_tab_use assign=product_tab_use}

		 
      <ul class="nav nav-tabs" role="tablist" id="productCardTab">
        <li role="presentation" class="active">
          <a href="#main-product-pane" aria-controls="main-product-pane" role="tab" data-toggle="tab"><img src="/assets/images/icon_info.png" alt="icon" /> [[%polylang_site_product_mainlabel]]</a>
        </li>
        <li role="presentation">
          <a href="#data-products-pane" aria-controls="data-products-pane" role="tab" data-toggle="tab"><img src="/assets/images/icon_note.png" alt="icon" />[[%polylang_site_product_description]]</a>
        </li>
        
		{if ({$product_tab_consist}!='')}
        <li role="presentation">
          <a href="#consist-products-pane" aria-controls="consist-products-pane" role="tab" data-toggle="tab"><img src="/assets/images/icon_faq.png" alt="icon" />[[%polylang_site_product_sostav]]</a>
        </li>
        {/if}
		{if ({$product_tab_feeding}!='')}
        
        <li role="presentation">
          <a href="#norms-products-pane" aria-controls="norms-products-pane" role="tab" data-toggle="tab"><img src="/assets/images/icon_cup.png" alt="icon" />[[%polylang_site_product_norm_korm]]</a>
        </li>
        {/if}
		{if ({$product_tab_use}!='')}        
        <li role="presentation">
          <a href="#use-products-pane" aria-controls="use-products-pane" role="tab" data-toggle="tab"><img src="/assets/images/icon_cup.png" alt="icon" />[[%polylang_site_product_why_use]]</a>
        </li>
        {/if}
        <li role="presentation" class="allcomments">
          <a href="#comments-products-pane" aria-controls="comments-products-pane" role="tab" data-toggle="tab"><img src="/assets/images/icon_star.png" alt="icon" />[[%polylang_site_product_review_label]]
		  <span class="tabcountsreview">({$countcomments})</span></a>
        </li>
      </ul>
	  
      <div class="tab-content">
        <div role="tabpanel" class="tab-pane active" id="main-product-pane" aria-labelledby="actions-tab">
          <div class="row">
            <div class="col-xs-6">
              <p class="h3 green"><b>[[%polylang_site_product_label_haracteristic]]:</b></p>
              <p>
                <span class="grey">[[%polylang_site_product_label_country]]:</span>
				{$vendorcountry}
              </p>
              <p>
                <span class="grey">[[%polylang_site_product_label_brand]]:</span>
				<span itemprop="brand">
				<a href="{$vendorurl}">{$vendorname}</a>
				</span>
              </p>
              <p>
                <span class="grey">[[%polylang_site_product_label_cat]]:</span> <span itemprop="category" >
				{$namecategory}
				</span>
              </p>
              <p>
                <span class="grey"></span>
              </p>
			 {$attributes}
            </div>
			<div  id="comments1" class="col-xs-6">
				{if ($countcomments>0)}<p class="h3 green"><b>[[%polylang_site_main_reviews]]:</b></p>{/if}
			  <div id="comments" class="comments">
					{foreach $commentslimit as $comment}
					<div itemprop="review" itemscope itemtype="http://schema.org/Review"  class="user-comment1 col-xs-12 ticket-comment" id="comment-{$comment.id}" data-parent="0" data-newparent="0" data-id="{$comment.id}">
							<p>
							  <span itemprop="author" itemscope itemtype="http://schema.org/Person">
							  <b itemprop="name">{$comment.name}</b>
							  </span>
							  
							  {if ($comment.pet!='')}<br><em class="grey small">{$comment.pet} {$comment.pet_type}	</em>{/if}
							</p>
							 
							{if ($comment.rat!='')}
							<div itemprop="reviewRating" itemscope itemtype="http://schema.org/Rating">
								<div class="stars-comment" >{$comment.rat}</div>
                                <meta  itemprop="ratingValue" content = "{$comment.rating}">
									<meta itemprop="worstRating" content = "1">
									<meta itemprop="bestRating" content = "5">
							</div>
							{/if}							 
							<p  itemprop="reviewBody">
							  {$comment.text}
							</p>
							<div class="row">
							  <div class="col-xs-8">							  
								<p>
								  <em class="small grey" itemprop="datePublished" content="{$comment.created}">
                                       {$comment.created|datecomment:"d F Y"}  {*{$comment.created|date_format:"d F Y"}*}</em>

								  
								</p>
							  </div>							  
							</div>
					</div>
					{/foreach}
				</div>
					{if ($countcomments>0)}<br/>{/if}
				{if ($countcomments>3)}<a class="seeallcom" data-toggle="tab" role="tab" aria-controls="comments-products-pane" href="#comments-products-pane" aria-expanded="false">Смотреть все ({$countcomments})</a>{/if}


					{if ($countcomments>0)}	<br/>{/if}
					{if (!$modx->user->isAuthenticated('web'))}
								<br/>
						[[%polylang_site_authorization]]
{*								 <p>Для того, чтобы добавить отзыв, нужно</p>*}
{*									<p>*}
{*									  <button class="btn green-btn"  data-toggle="modal" data-target="#boxregistr" id="registrcomment" >ЗАРЕГИСТИРОВАТЬСЯ</button>*}
{*									</p>*}
{*									<p>*}
{*									  или *}
{*									</p>*}
{*									<button class="btn grey-btn"  data-toggle="modal" data-target="#boxlogin">АВТОРИЗОВАТЬСЯ</button>*}
{*									<br>*}
{*									<br>*}
					{else}

							<form id="comment-form" action="" method="post">
							  <p class="h3 green"><b>[[%polylang_site_product_review_write]]</b></p>
							  <p class="h3"><b>[[+fullname]]</b></p>
								<input type="hidden" name="thread" value="resource-{$modx->resource->id}" />
								<input type="hidden" name="parent" value="0" />
								<input type="hidden" name="id" value="0" />
							  <div class="br-widget">
								<select  name="rating" id="rating1" class="stars-rating">
								  <option value="1">1</option>
								  <option value="2">2</option>
								  <option value="3">3</option>
								  <option value="4" selected>4</option>
								  <option value="5">5</option>
								</select>
								
							  </div>
							  <br>
							  <textarea name="text" id="comment-editor" class="custom-inpt" rows="6"></textarea>
							  <br><br>
							  <button type="submit" class="btn grey-btn text-uppercase pull-right submit">[[%polylang_site_submit]]</button>
							  </form>

					{/if}

			</div>            
          </div>
        </div>

        <div role="tabpanel" class="tab-pane" id="data-products-pane" aria-labelledby="actions-tab">
          <div class="row" >
            {field name=content}
          </div>
          <meta content="{field name=introtext}" itemprop="description" />

        </div>
         


		{if ({$product_tab_consist}!='')}
		<div role="tabpanel" class="tab-pane consists-table" id="consist-products-pane" aria-labelledby="actions-tab">
				  {$product_tab_consist}
				</div>
		{/if}

		{if ({$product_tab_feeding}!='')}
		<div role="tabpanel" class="tab-pane norms-table" id="norms-products-pane" aria-labelledby="actions-tab">
				  {$product_tab_feeding}
				</div>
		{/if}
		
		{if ({$product_tab_use}!='')}
		<div role="tabpanel" class="tab-pane norms-table" id="use-products-pane" aria-labelledby="actions-tab">
				  {$product_tab_use}
				</div>
		{/if}

        <div role="tabpanel" class="tab-pane" id="comments-products-pane" aria-labelledby="actions-tab">
          <div class="row">
		  
				<div class="col-xs-6">
					<div id="comments1">
					
						<p class="h3 green"><b>[[%polylang_site_main_reviews]]:</b></p>
						<div id="comments" class="comments">
							{foreach $comments as $comment}
							<div itemprop="review" itemscope itemtype="http://schema.org/Review"  class="user-comment1 col-xs-12 ticket-comment" id="comment-{$comment.id}" data-parent="0" data-newparent="0" data-id="{$comment.id}">
									<p>
									  <span itemprop="author" itemscope itemtype="http://schema.org/Person">
									  <b itemprop="name">{$comment.name}</b>
									  </span><br><em class="grey small">{$comment.pet} {$comment.pet_type}	</em>
									</p>
									 
									{if ($comment.rat!='')}
									<div itemprop="reviewRating" itemscope itemtype="http://schema.org/Rating">
										<div class="stars-comment" >{$comment.rat}</div>
                                        <meta  itemprop="ratingValue" content = "{$comment.rating}">
											<meta itemprop="worstRating" content = "1">
											<meta itemprop="bestRating" content = "5">
									</div>
									{/if}							 
									<p  itemprop="reviewBody">
									  {$comment.text}
									</p>
									<div class="row">
									  <div class="col-xs-8">
									  
										<p>
										  <em class="small grey" itemprop="datePublished" content="{$comment.created}">
										  {$comment.created|datecomment:"d F Y"}{* *}</em>
										</p>
									  </div>
									  
									</div>
							</div>
							{/foreach}
						</div>
					</div>
				</div>
				<div class="col-xs-6">
				
					{if (!$modx->user->isAuthenticated('web'))}
								<br/>
						[[%polylang_site_authorization]]
{*								 <p>Для того, чтобы добавить отзыв, нужно</p>*}
{*									<p>*}
{*									  <button class="btn green-btn"  data-toggle="modal" data-target="#boxregistr" id="registrcomment" >ЗАРЕГИСТИРОВАТЬСЯ</button>*}
{*									</p>*}
{*									<p>*}
{*									  или *}
{*									</p>*}
{*									<button class="btn grey-btn"  data-toggle="modal" data-target="#boxlogin">АВТОРИЗОВАТЬСЯ</button>*}
{*									<br>*}
{*									<br>*}
					{else}

							<form id="comment-form" action="" method="post">
							  <p class="h3 green"><b>Написать отзыв о товаре</b></p>
							  <p class="h3"><b>[[+fullname]]</b></p>
								<input type="hidden" name="thread" value="resource-{$modx->resource->id}" />
								<input type="hidden" name="parent" value="0" />
								<input type="hidden" name="id" value="0" />
							  <div class="br-widget">
								<select  name="rating" id="rating1" class="stars-rating">
								  <option value="1">1</option>
								  <option value="2">2</option>
								  <option value="3">3</option>
								  <option value="4" selected>4</option>
								  <option value="5">5</option>
								</select>
								
							  </div>
							  <br>
							  <textarea name="text" id="comment-editor" class="custom-inpt" rows="6"></textarea>
							  <br><br>
							  <button type="submit" class="btn grey-btn text-uppercase pull-right submit">[[%polylang_site_submit]]</button>
							  </form>

					{/if}

				</div>
		  
          </div>

        </div>
      </div>

    </div>
    </div>
	{snippet name=msRelated params="limit=`6`"}
  </div>
	   {/block}

