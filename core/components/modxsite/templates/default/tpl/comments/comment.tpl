<div itemprop="review" itemscope itemtype="http://schema.org/Review"  class="user-comment1 col-xs-12 ticket-comment" id="comment-{$comment.id}" data-parent="0" data-newparent="0" data-id="{$comment.id}">
							<p>
							  <span itemprop="author" itemscope itemtype="http://schema.org/Person">
							  <b itemprop="name">{$comment.name}</b>
							  </span>
							  
							  {if ($comment.pet!='')}<br><em class="grey small">{$comment.pet} {$comment.pet_type}	</em>{/if}
							</p>
							 
							{if ($comment.rat!='')}
							<div itemprop="reviewRating" itemscope itemtype="http://schema.org/Rating">
								<div class="stars-comment" >{$comment.rat}</div>
                                 
                                 <meta itemprop="ratingValue" content = "{$comment.rating}">
									<meta itemprop="worstRating" content = "1">
									<meta itemprop="bestRating" content = "5">
							</div>
							{/if}							 
							<p  itemprop="reviewBody">
							  {$comment.text}
							</p>
							<div class="row">
							  <div class="col-xs-8">							  
								<p>
								  <em class="small grey" itemprop="datePublished" content="{$comment.created}">
                                        {$comment.created|datecomment:"d F Y"}  {*{$comment.created|date_format:"d F Y"}*}</em>
								  
								  
								</p>
							  </div>							  
							</div>
					</div>