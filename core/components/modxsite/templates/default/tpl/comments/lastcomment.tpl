<div class="row callback-single">
  <div class="col-xs-4">
    <div class="user-photo">
      <img src="{$item.avatar}" alt="user" />
    </div>
  </div>
  <div class="col-xs-8">
    <div class="callback-popup">
      <div class="callback-popup-content clearfix">
        <p class="title greener"><b>{$item.fullname}</b></p>
        <p class="small grey">{$item.date_ago}</p>
        <p class="textcom">
          {$item.text}
        </p>

        {*<a href="javascript:void(0)" data-id="{$item.id}" id="shownextcomment" class=" small pull-right">Прочесть еще отзыв</a>*}
      </div>
    </div>
  </div>
</div>