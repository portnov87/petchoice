{extends file="layout.tpl"}

{block name=content}
{literal}
     

  <div class="container">
          <div class="page-shipping">
            
             [[BreadCrumb? &scheme=`full` &containerTpl=`breadcrumbOuterTpl` &currentCrumbTpl=`breadcrumbCurrentTpl` &linkCrumbTpl=`breadcrumbLinkTpl` &homeCrumbTpl=`breadcrumbLinkTpl`]]
             <h1>[[*pagetitle]]</h1>
            <div class="row equal-container">
            
            
              [[*content]]
                
            </div>
            <br>
            <br>
          </div>
      </div>
      
      
      
	  {/literal}
	  {/block}
