{extends file="layout.tpl"}

{block name=content}


{literal}
  
  <div class="container">
    [[BreadCrumb? &scheme=`full` &containerTpl=`breadcrumbOuterTpl` &currentCrumbTpl=`breadcrumbCurrentTpl` &linkCrumbTpl=`breadcrumbLinkTpl` &homeCrumbTpl=`breadcrumbLinkTpl`]]

    <div class="row title-page equal-height-wrapper" style="width:100%;min-width:100%;">
      <div class="col-xs-7 equal-height">
        <h1>[[*pagetitle]]</h1>
      </div>
      <div id="mse2_sort" class="col-xs-5 equal-height text-right filters">
        <span>[[%polylang_site_category_sort_label]]:</span>
        <!--<a href="#" class="sort filter-link" data-sort="ms|popular" data-dir="[[+mse2_sort:is=`ms|popular:desc`:then=`desc`]]" data-default="desc">популярности</a>
        <a href="#" class="sort filter-link up-sort" data-sort="ms|price" data-dir="[[+mse2_sort:is=`ms|price:desc`:then=`desc`]]" data-default="desc">цене</a>
        <a href="#" class="sort filter-link unactive" data-sort="ms_product|pagetitle" data-dir="[[+mse2_sort:is=`ms_product|pagetitle:desc`:then=`desc`]]" data-default="desc">алфавиту</a>
        -->
        
        <a href="#" class="sort filter-link [[+mse2_sort:is=`ms|hits:asc`:then=`up-sort active`:elseis=`ms|hits:desc`:then=`down-sort active`:else=`down-sort active`]]" data-sort="ms|hits" data-dir="[[+mse2_sort:is=`ms|hits:asc`:then=`asc`]][[+mse2_sort:is=`ms|hits:desc`:then=`desc`]]" data-default="asc">[[%polylang_site_category_sort_popular]]</a>
        <!--<a href="#" class="sort filter-link [[+mse2_sort:is=`ms|price:asc`:then=`up-sort active`]] [[+mse2_sort:is=`ms|price:desc`:then=`down-sort active`]]" data-sort="ms|price" data-dir="[[+mse2_sort:is=`ms|price:asc`:then=`asc`]] [[+mse2_sort:is=`ms|price:desc`:then=`desc`]]" data-default="desc">цене</a>-->
        <a href="#" class="sort filter-link [[+mse2_sort:is=`ms_product|pagetitle:asc`:then=`up-sort active`]] [[+mse2_sort:is=`ms_product|pagetitle:desc`:then=`down-sort active`]] " data-sort="ms_product|pagetitle" data-dir="[[+mse2_sort:is=`ms_product|pagetitle:asc`:then=`asc`]][[+mse2_sort:is=`ms_product|pagetitle:desc`:then=`desc`]]" data-default="desc">[[%polylang_site_category_sort_alphavit]]</a>
      </div>
    </div>
    [[!Discounts]]
   
  </div>
  {/literal}
{/block}