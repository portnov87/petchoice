{extends file="layout.tpl"}

{block name=content}
{literal}
     
  <div class="container">
    [[BreadCrumb? &scheme=`full` &containerTpl=`breadcrumbOuterTpl` &currentCrumbTpl=`breadcrumbCurrentTpl` &linkCrumbTpl=`breadcrumbLinkTpl` &homeCrumbTpl=`breadcrumbLinkTpl`]]

    <div class="row">
      <div class="col-xs-9">
        <h1>[[*pagetitle]]</h1>
        <br>
        <div class="row blog-content">
        <!--col-xs-9-->
          <div class="" style="padding:0px 75px 0px 30px;">
            [[*content]]
            <div class="socials">
              
            
            <p class="greener"><strong>Поделиться с друзьями:</strong></p>
<script type="text/javascript">(function() {
  if (window.pluso)if (typeof window.pluso.start == "function") return;
  if (window.ifpluso==undefined) { window.ifpluso = 1;
    var d = document, s = d.createElement('script'), g = 'getElementsByTagName';
    s.type = 'text/javascript'; s.charset='UTF-8'; s.async = true;
    s.src = ('https:' == window.location.protocol ? 'https' : 'http')  + '://share.pluso.ru/pluso-like.js';
    var h=d[g]('body')[0];
    h.appendChild(s);
  }})();</script>
<div class="pluso" data-background="transparent" data-options="medium,square,line,horizontal,nocounter,theme=04" data-services="vkontakte,odnoklassniki,facebook,twitter,google" data-user="280379110"></div>
              
              
              
              
              
            </div>
            
            
            
          </div>
        </div>

        <div class="row page-nav">
            [[!prevNext? &tpl=`prevnextArticle` &sort=`menuindex` &includeHidden=`1`]]
        </div>

        <div class="fb-commnets">
          <div id="disqus_thread"></div>
          <script type="text/javascript">
            /* * * CONFIGURATION VARIABLES: EDIT BEFORE PASTING INTO YOUR WEBPAGE * * */
            var disqus_shortname = 'petchoice'; // required: replace example with your forum shortname

            /* * * DON'T EDIT BELOW THIS LINE * * */
            (function() {
              var dsq = document.createElement('script'); dsq.type = 'text/javascript'; dsq.async = true;
              dsq.src = '//' + disqus_shortname + '.disqus.com/embed.js';
              (document.getElementsByTagName('head')[0] || document.getElementsByTagName('body')[0]).appendChild(dsq);
            })();
          </script>
          <noscript>Please enable JavaScript to view the <a href="https://disqus.com/?ref_noscript">comments powered by Disqus.</a></noscript>

        </div>
      </div>
      [[!getsidebar]]

    
    </div>

  </div>
	  {/literal}
	  {/block}
