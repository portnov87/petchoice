<!doctype html>
<html class="no-js" prefix="og: https://ogp.me/ns#">
<head>

{*
    {assign var=pars value=[
        "element"=>'msProducts'
        ,"where"=>[
			"class_key"=>"msProduct"
		]
        ,"filters"=>'ms|price_kg,ms|vendor:vendors,ms|adiscount,ms|askidka'
		,"includeTVs"=>'0'
		,"includeMS"=>'1'
		,"includeMSList"=>'price,new,toorder,favorite,popular,size,color'
        ,"paginator"=>'getPage'
        ,"tplOuter"=>'filterTpl'
        ,"tplFilter.outer.default"=>'filterGroupTpl'
        ,"tplFilter.row.default"=>'filterRowTpl'
        ,"tplFilter.outer.ms|price"=>'filterPriceOuterTpl'
		,"tplFilter.outer.ms|price_kg"=>'filterPriceOuterTpl'
        ,"tplFilter.row.ms|price"=>'filterPriceTpl'
		,"tplFilter.row.ms|price_kg"=>'filterPriceTpl'
        ,"tplFilter.outer.tv|prloduct_options"=>'filterPriceOuterTpl'
        ,"tplFilter.row.tv|product_options"=>'filterPriceTpl'
        ,"tplFilter.outer.ms|vendor"=>'filterVendorOuterTpl'
        ,"tpls"=>'productRowTpl'
        ,"limit"=>"24"
					]}


{snippet name=metaprevnext  params=$pars}
*}
{literal}
[[+paginated-prev-link]]
[[+paginated-next-link]]
{/literal}	
{snippet name=alternate}

 
    <script>

    {if (!$modx->user->isAuthenticated())}
        registration='guest';
    {else}
        {if ($modx->user->username=='admin')}

            registration='manager';
        {elseif (($modx->user->isMember('manager5'))||($modx->user->isMember('manager2'))||($modx->user->isMember('manager3'))||( $modx->user->isMember('manager')))}

            registration='manager';

        {else}
            registration='user';
        {/if}

    {/if}


        {if ($type=='product')}
        {literal}

        dataLayer=[{
            'dynx_itemid': '{/literal}{field name=id}{literal}',
            'dynx_pagetype': 'offerdetail',
            'dynx_totalvalue': {/literal}[[+price]]{literal},
            'registration':registration
        }];

        {/literal}
        {/if}
        {if ($type=='home')}
        {literal}
        dataLayer=[{
            'dynx_itemid': '',
            'dynx_pagetype': 'home',
            'dynx_totalvalue': 0,
            'registration':registration
        }];
        {/literal}
        {/if}
        {if ($type=='cart')}
        {literal}
        dataLayer=[{
            'dynx_itemid': '',
            'dynx_pagetype': 'conversionintent',
            'dynx_totalvalue': 0,
            'registration':registration
        }];
        {/literal}
        {/if}
        {if ($typemarketing=='successorder')}
        {literal}
        dataLayer=[{
            'dynx_itemid': '',
            'dynx_pagetype': 'conversion',
            'dynx_totalvalue': "{/literal}[[+cart_cost]]{literal}",
            'registration':registration
        }];
        {/literal}
        {/if}
        {if ($type=='other')}
        {literal}
        dataLayer=[{
            'dynx_itemid': '',
            'dynx_pagetype': 'other',
            'dynx_totalvalue': 0,
            'registration':registration
        }];
        {/literal}
        {/if}
        {literal}
    </script>

    <!-- Google Tag Manager -->
    <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
        new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
                j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
                'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
                })(window,document,'script','dataLayer','GTM-W5VBDZ');</script>
    <!-- End Google Tag Manager -->
    {/literal}
    <base href="https://petchoice.ua/">
    <meta charset="utf-8">
    <title>{snippet name=metatitle params="title=`{field name=pagetitle}`&seotitle=`{field name=longtitle}`"}</title>
    <meta name="description" content="{snippet name=metadescription params="description=`{field name=description}`"}">
    <meta name="viewport" content="width=device-width, initial-scale=1">


<meta name="robots" content="noindex, nofollow"/>
    
    <script  src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script> 
<script  src="//cdnjs.cloudflare.com/ajax/libs/modernizr/2.6.2/modernizr.min.js"></script>
<link  href="https://cdnjs.cloudflare.com/ajax/libs/fotorama/4.6.4/fotorama.css" rel="stylesheet">
    
    {literal}
    
{/literal}

<link rel="stylesheet" href="/assets/fancybox/jquery.fancybox.css"/>
<link rel="stylesheet" href="/assets/components/msearch2/css/web/redmond/jquery-ui-1.10.4.custom.min.css"/>
        <link rel="stylesheet" href="/assets/styles/bootstrap.min.css"/>
        <link rel="stylesheet" href="/assets/components/tickets/css/web/default.css"/>
        <link rel="stylesheet" href="/assets/styles/main_v_3.css?v=2233"/>
        <link rel="stylesheet" href="/assets/styles/main_v_4.css"/>
        
        
        
        {snippet name=PolylangCanonical}
    {*    
    {snippet name=getcanonical}
    {snippet name=robots}
    *}
    {$canonical}
    {$robots}


    {field name=type assign=typedoc}

    {if ($type=='product')}
        <meta property="og:title" content="{field name=pagetitle}"/>
        <meta property="og:type" content="product"/>
        <meta property="og:image" content="https://petchoice.ua{snippet name=msGallery params="limit=1&main=1"}"/>
        <meta property="og:url" content="https://petchoice.ua/{link id={field name=id}}"/>
        <meta property="og:description" content="{field name=introtext}"/>
    {/if}

    {if ($type=='blog')}
        <meta property="og:title" content="{field name=pagetitle}"/>
        <meta property="og:type" content="article"/>
        <meta property="og:image" content="https://petchoice.ua{field name=image}"/>
        <meta property="og:url" content="https://petchoice.ua/{link id={field name=id}}"/>
        <meta property="og:description" content="{field name=description}"/>
    {/if}

    
</head>
<body class=" action_event_1">
  <script>
    !function (t, e, c, n) {
        var s = e.createElement(c);
        s.async = 1, s.src = 'https://statics.esputnik.com/scripts/' + n + '.js';
        var r = e.scripts[0];
        r.parentNode.insertBefore(s, r);
        var f = function () {
            f.c(arguments);
        };
        f.q = [];
        f.c = function () {
            f.q.push(arguments);
        };
        t['eS'] = t['eS'] || f;
    }(window, document, 'script', '9AD2834E688B486E93DCDF7D7D0B4C20');
</script><script>eS('init');</script>   

[[eSputnikCustomerData]]
            
<!--action_event_no-->

<style>
    .action_event .phone-part .phone-line .big, .action_event .phone-part .small
    {
        /*color: #1c3847!important;*/
        color:#000;
    }
    
    .action_event .header .main-block
    {
        background-color: #c62f48!important;
        
    }
    
    .delivery_header span.activelang{
        font-weight:700;
        border-bottom: none;
            display: inline-block;
    margin: 2px 0px 0 0;
 color: #476dc7;
    text-decoration: none;
    background-image:none!important;
    }

</style>
{literal}
<div id="fb-root"></div>
<script async defer crossorigin="anonymous" src="https://connect.facebook.net/ru_RU/sdk.js#xfbml=1&version=v9.0&appId=1450437548564324&autoLogAppEvents=1"; nonce="h7InQuzt"></script>
    
   <!-- <script>
   

        (function(d, s, id) {
            var js, fjs = d.getElementsByTagName(s)[0];
            if (d.getElementById(id)) return;
            js = d.createElement(s); js.id = id;
            js.src = 'https://connect.facebook.net/ru_RU/sdk.js#xfbml=1&version=v3.2&appId=2345507562185950';
            fjs.parentNode.insertBefore(js, fjs);
        }(document, 'script', 'facebook-jssdk'));
        
        </script>-->
        
    <!-- Google Tag Manager (noscript) -->
    <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-W5VBDZ";
                      height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
    <!-- End Google Tag Manager (noscript) -->


{/literal}
{snippet name=getmessage}
<!-- Add your site or application content here -->
<div class="wrapper ">
    <div class="header">
        <div class="basket-header" style="z-index:99999!important;">
            <div class="container">
                <div class="row">
                    <div class="col-xs-9">
                    {snippet name=PolylangLinks params='mode=`single`'}
                    
                        <span class="delivery_header" style="margin-right:20px;"><span class="icon icon_delivery"></span><a href="{snippet name=PolylangMakeUrl  params='id=473'}" >[[%polylang_site_common_menu_delivery]]</a></span>
                        {if (!$modx->user->isAuthenticated())}

                            <a href="{snippet name=PolylangMakeUrl  params='id=9'}">[[%polylang_site_common_menu_login]]</a>
                            <span class="small"><i>([[%polylang_site_common_lable_loylnost]] 10%)</i></span>
                        {else}


                            {nocache}{block name=login nocache}{snippet name=login params="tplType=modChunk&loginTpl=headNotLoggedInTpl&logoutTpl=headLoggedInTpl"}{/block}{/nocache}
                        {/if}
                         <span class="shop_header" style="margin-left:10px;"><span class="icon icon_shop"></span><a href="{snippet name=PolylangMakeUrl  params='id=4'}"  >[[%polylang_site_common_menu_shops]]</a></span>
                         <span class="veterinar_header"  style="margin-left:10px;">
                         <span class="icon icon_veterinar"></span>
                         <a href="{snippet name=PolylangMakeUrl  params='id=559'}" >[[%polylang_site_common_lable_voprosvet]]</a>
                             
                         </span>
                    </div>

                    <div id="msMiniCart" class="col-xs-3 text-right">
                        {nocache}{block name=minicart nocache}{snippet name=msMiniCart params="tpl=miniCartTpl"}{/block}{/nocache}

                    </div>
                </div>
            </div>
        </div>
        <div class="main-block">
            <div class="container">
                <div class="row">
                    <div class="col-xs-3">

                        <a class="logo-link" href="{config name=site_url}">
                            <!--<div class="hny-logo"></div>--><img src="/assets/images/logo.png"
                                                                    alt="Зомагазин Pet Choice - товары для животных с доставкой"/></a>

                    </div>

                    <div class="col-xs-6">
                        <div class="search-element">
                            {assign var=paramsSearch value=[
                            "element"=>"=msProducts"
                            ,"fields"=>"Data.article:3,Data.vendor:2"
                            ,"where"=>[
                            "Data.price:>"=>0
                            ]
                            ]}
                            {snippet as_tag=1 name=mSearchForm params=$paramsSearch}
                        </div>
                    </div>

                    <div class="col-xs-3 text-right">
                        <div class="phone-part">
                            <div class="phone-line">
                                <span class="icon phone-icon"></span>
                                <span class="big">048 700-50-80</span>
                                <div class="arrow-block dropdown">
                                    <a class="icon arrow-phone" id="PhoneList" data-target="#" href=""
                                       data-toggle="dropdown" aria-haspopup="true" role="button"
                                       aria-expanded="false">[[%polylang_site_header_more]]</a>
                                    <div class="dropdown-menu" role="menu" aria-labelledby="PhoneList">
                                        <span class="dropdown-arrow"></span>
                                        <p>
                                            <span class="icon life-icon"></span>
                                            063 505-50-03
                                        </p>
                                        <p>
                                            <span class="icon kyivstar-icon"></span>
                                            067 841-64-64
                                        </p>
                                        <p>
                                            <span class="icon mts-icon"></span>
                                            095 505-65-95
                                        </p>
                                    </div>
                                </div>
                            </div>
                            <div class="dropdown callback">
                                <a id="CallBack" data-target="#" href="" data-toggle="dropdown" aria-haspopup="true"
                                   role="button" aria-expanded="false">[[%polylang_site_common_call]]</a>
                                <div class="dropdown-menu" role="menu" aria-labelledby="CallBack">
                                    <span class="dropdown-arrow"></span>
                                    {assign var=paramsAjaxForm value=[
                                    "snippet"=>"FormIt"
                                    ,"form"=>"callFormTpl"
                                    ,"hooks"=>"email"
                                    ,"emailSubject"=>"Получен заказ обратного звонка"
                                    ,"emailTo"=>{config name=emailsender}
                                    ,"validate"=>"name:required,phone:required"
                                    ,"validationErrorMessage"=>"В форме содержатся ошибки!"
                                    ,"successMessage"=>"Сообщение успешно отправлено"
                                    ]}
                                    {snippet as_tag=1 name=AjaxForm params=$paramsAjaxForm}
                                </div>
                            </div>
                            <div class="times small">
                                <p>[[%polylang_site_common_timespn]]: 9:00 - 19:00</p>
                                <p>[[%polylang_site_common_timessub]]: 9:00 - 16:00</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="edge-block">
            <div class="container">
                <div class="row">
                    <div class="col-xs-3">
                    [[%polylang_site_main_benefits_one]]                        
                    </div>
                    <div class="col-xs-3">
                    [[%polylang_site_main_benefits_two]]                    
                    </div>
                    <div class="col-xs-3">
                    [[%polylang_site_main_benefits_three]]                        
                    </div>
                    <div class="col-xs-3">
                    [[%polylang_site_main_benefits_four]]                        
                    </div>
                </div>
            </div>
        </div>

        <div class="main-menu">
            <div class="container">
                <div class="row">

                    {assign var=params value=[
                    "startId"       => 10
                    ,"level"        => 2
                    ,"sortBy"       => "menuindex"
                    ,"levelClass"   => "level"
                    ,"cacheable"    =>false
                    ,"id"           => "catalog"
                    ]}
                    {processor action="site/web/menu/getcatalogmenu" ns="modxsite" params=$params assign=result}
                    {assign var=items value=$result.object}
                    {include file='tpl/menu/menumain.tpl'}

                </div>
            </div>
        </div>
    </div>
    {* если главная *}
    {if ($modx->resource->id==1)}
        {block name=mainpage}{/block}
    {else}
        {block name=content}{/block}
    {/if}
    <div class="footer">
        <div class="container">
            <div class="row">
                <div class="col-xs-3">
                    <ul class="footer-menu list-unstyled">
                        <li class="footer-menu-item first"><a href="{snippet name=PolylangMakeUrl  params='id=4'}">[[%polylang_site_common_menu_contact]]</a></li>
                        <li class="footer-menu-item"><a href="{snippet name=PolylangMakeUrl  params='id=473'}">[[%polylang_site_common_menu_delivery]]</a></li>
                        <li class="footer-menu-item"><a href="{snippet name=PolylangMakeUrl  params='id=5'}">[[%polylang_site_common_menu_about]]</a></li>
                        <li class="footer-menu-item"><a href="{snippet name=PolylangMakeUrl  params='id=476'}">[[%polylang_site_common_menu_clinik]]</a></li>
                                <li class="footer-menu-item last"><a href="{snippet name=PolylangMakeUrl  params='id=559'}">[[%polylang_site_common_menu_voprosvet]]</a></li>
                                <li class="footer-menu-item last"><a href="{snippet name=PolylangMakeUrl  params='id=12430'}">[[%polylang_site_common_menu_retingkorm]]</a></li>
                        <li class="footer-menu-item last"><a href="{snippet name=PolylangMakeUrl  params='id=529'}">[[%polylang_site_common_menu_blog]]</a></li>
                        <li class="footer-menu-item last"><a href="{snippet name=PolylangMakeUrl  params='id=14752'}">[[%polylang_site_common_menu_oferta]]</a></li>
                        
                        <li class="footer-menu-item"><a href="https://m.petchoice.ua/?type=mobile">[[%polylang_site_common_menu_mobil]]</a>
                        </li>
                    </ul>
                </div>
                <div class="col-xs-3 follow-footer">
                    <p><b>[[%polylang_site_common_subscribe_label]]</b></p>

                    <form action="/assets/components/minishop2/subscribe.php" class="formsubscribe" method="post"
                          data-toggle="validator" role="form">
                        <div class="form-group form-line">
                            <span class="inpt-icon name-inpt"></span>
                            <input class="form-control custom-inpt" name="name" type="text" placeholder="Имя"
                                   data-error="введите ваше имя" required>
                            <div class="help-block with-errors">[[%polylang_site_form_name_lable]]</div>
                        </div>
                        <div class="form-group form-line">
                            <span class="inpt-icon email-inpt"></span>
                            <input class="form-control custom-inpt" type="email" name="email"
                                   data-error="введите ваш email" placeholder="E-mail адрес" required>
                            <div class="help-block with-errors">[[%polylang_site_form_email_lable]]</div>
                        </div>
                        <input class="hide" type="text" name="code" value="">
                        <a class="why-follow small" data-toggle="popover" data-popover-content="#follow-popover"
                           data-placement="top">[[%polylang_site_form_subscribe_why_lable]]</a>
                        <button type="submit" class="btn grey-btn pull-right" data-type="footer" id="buttonsubscribe" data-toggle="modal"
                                data-target="#thanksCallModalLabel">[[%polylang_site_form_suscribe_button]]
                        </button>
                    </form>
                    <div class="messagesubscribe"></div>


                </div>

                <div class="col-xs-3">
                    <div class="pay-info">
                        <p><b>[[%polylang_site_common_lable_payment]]:</b></p>
                        <div class="pay-systems">
                            <span class="icon visa">visa</span>
                            <span class="icon mastercard">mastercard</span>
                            <span class="icon privat24">privat24</span>
                            <span class="icon global">cash</span>
                        </div>
                        <a class="small" href="#" data-toggle="modal" data-target="#payModal">[[%polylang_site_common_lable_payment_url]]</a>
                    </div>
                    <div class="delivery-info">
                        <p><b>[[%polylang_site_common_lable_delivery]]:</b></p>
                        <p class="small">[[%polylang_site_common_valuetext_delivery]]</p>
                    </div>
                </div>

                <div class="col-xs-3 text-right cat-part">
                    <div class="phone-part">
                        <div class="phone-line">
                            <span class="icon phone-icon"></span>
                            <span class="big">048 700-50-80</span>
                        </div>

                        <p>
                            <span class="icon life-icon"></span>
                            063 505-50-03
                        </p>
                        <p>
                            <span class="icon kyivstar-icon"></span>
                            067 841-64-64
                        </p>
                        <p>
                            <span class="icon mts-icon"></span>
                            095 505-65-95
                        </p>
                    </div>


                    <div class="times small">
                        <p>[[%polylang_site_common_timespn]]: 9:00 - 19:00</p>
                        <p>[[%polylang_site_common_timessub]]: 9:00 - 16:00</p>
                    </div>

                    <span class="footer-cat"></span>
                </div>
            </div>
        </div>

        <div class="undo-footer">
            <div class="container">
                <div class="row">
                    <div class="col-xs-8 copyright">
                        &copy; 2020 [[%polylang_site_common_copyright]]
                    </div>

                    <div class="col-xs-4 text-right">
                        <a class="icon in-icon" target="_blank" href="https://www.instagram.com/petchoice_ua/">in</a>
                        <a class="icon fb-icon" target="_blank" href="//www.facebook.com/petchoice.com.ua">fb</a>
                        <!-- not need now <a class="icon ytub-icon" href="#">youtube</a>-->
                    </div>
                </div>
            </div>
        </div>
    </div>


    <!-- follow popover -->
    <div id="follow-popover" class="hidden">
        <div class="popover-body">
        [[%polylang_site_common_why_subscribe]]            
        </div>
    </div>
    <!-- end follow popover -->


    {if (($type=='other')||($type=='product'))}
        <div class="modal fade" id="boxStatus" tabindex="-1" role="dialog" aria-labelledby="boxStatusModalLabel"
             aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"></button>
                        <h3 class="modal-title greener"><b>Сообщить о наличии</b></h3>
                    </div>
                    {snippet name=AjaxForm params="snippet=`FormIt`&form=`FormAsk.tpl`&hooks=`recaptchav2,email`&emailSubject=`Запрос о наличии`&emailTo=`[[++emailsender]]`&validate=`name:required,phone:required,g-recaptcha-response:required`&validationErrorMessage=`В форме содержатся ошибки!`&successMessage=`Сообщение успешно отправлено`"}
<!--ask_stock

-->
                </div>
            </div>
        </div>
    {/if}

    {if (!$modx->user->isAuthenticated())}
        <div class="modal fade" id="boxlogin" tabindex="-1" role="dialog" aria-labelledby="boxloginModalLabel"
             aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"></button>
                        <h3 class="modal-title greener"><b>Авторизация</b></h3>
                    </div>

                    <form action="/account/login" method="post" class="ajaxloginbox" data-toggle="validator"
                          role="form">
                        <div class="modal-body">

                            <div class="form-group ">
                                <label class="control-label">Телефон</label>
                                <input name="username" type="text" class="form-control custom-inpt" placeholder="+38"
                                       value="+38" required>
                                <div class="help-block small grey">
                                    Укажите номер телефона в формате +380ХХХХХХХХХ без пробелов, дефисов и скобок
                                </div>
                                <div class="help-block with-errors"></div>
                            </div>
                            <div class="form-group">
                                <label class="control-label">Пароль</label>
                                <input name="password" type="password" class="form-control custom-inpt" required>
                                <div class="help-block">
                                    <a href="" data-toggle="modal" data-dismiss="modal"
                                       data-target="#forgotPasswordModal">Забыли пароль?</a>
                                </div>
                            </div>

                        </div>
                        <div class="modal-footer">
                            <input class="returnUrl" type="hidden" name="returnUrl"
                                   value="https://petchoice.ua/account/"/>
                            <input class="loginLoginValue" type="hidden" name="service" value="login"/>
                            <br>
                            <button type="submit" class="btn orange-btn">ВОЙТИ</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <!-- Forgot Password Modal -->
        <div class="modal fade" id="forgotPasswordModal" tabindex="-1" role="dialog" aria-labelledby="basketModalLabel"
             aria-hidden="true">
            <div class="modal-dialog">
                <div id="office-auth-form" class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"></button>
                        <h3 class="modal-title greener"><b>Восстановление пароля</b></h3>
                    </div>
                    <!--<form id="office-auth-login" method="post" data-toggle="validator" role="form">
                        -->
                    <div class="modal-body">

                        {snippet name=officeAuth params="tplLogin=`forgotPasswordFormTpl`&HybridAuth=`0`"}
                    </div>
                    <!--</form>-->
                </div>
            </div>
            <!-- end Forgot Password Modal -->
        </div>
        
        <div class="modal fade" id="boxregistr" tabindex="-1" role="dialog" aria-labelledby="boxregistrModalLabel"
             aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"></button>
                        <h3 class="modal-title greener"><b>Регистрация</b></h3>
                    </div>


                    <form action="/account/register" class="ajaxregistrbox" method="post" data-toggle="validator"
                          role="form">
                        <div class="modal-body">
                            <div class="form-group ">
                                <label class="control-label">Имя <sup>*</sup></label>
                                <input name="fullname" type="text" class="form-control custom-inpt" value=""
                                       placeholder="Иван" required>
                                <div class="help-block with-errors"></div>
                            </div>
                            <div class="form-group">
                                <label class="control-label">Фамилия</label>
                                <input name="lastname" type="text" class="form-control custom-inpt" value=""
                                       placeholder="Иванов">
                            </div>
                            <div class="form-group ">
                                <label class="control-label">Телефон <sup>*</sup></label>
                                <input type="text" name="phone" class="form-control custom-inpt" placeholder="+38"
                                       value="+38" required>
                                <div class="help-block small grey">
                                    <ul class="list-unstyled">
                                        <li>Будет использован как логин к Личному кабинету</li>
                                        <li>Укажите номер телефона в формате +380ХХХХХХХХХ без пробелов, дефисов и
                                            скобок
                                        </li>
                                    </ul>
                                </div>
                                <div class="help-block with-errors"></div>
                            </div>
                            <div class="form-group ">
                                <label class="control-label">Email <sup>*</sup></label>
                                <input name="email" type="email" class="form-control custom-inpt" value="" required>
                                <div class="help-block small grey">Никакого спама, обещаем</div>
                                <div class="help-block with-errors"></div>
                            </div>
                            <div class="form-group ">
                                <label class="control-label">Пароль <sup>*</sup></label>
                                <input id="password" name="password" type="password" class="form-control custom-inpt"
                                       value="" required>
                                <div class="help-block small grey">Не менее 6 символов</div>
                                <div class="help-block with-errors"></div>
                            </div>
                            <input name="code" type="text" class="hide" value="">
                            <div class="form-group ">
                                <label class="control-label">Подтвердите пароль <sup>*</sup></label>
                                <input name="password_confirm" type="password" class="form-control custom-inpt" value=""
                                       required>
                                <div class="help-block with-errors"></div>
                            </div>


                        </div>
                        <div class="modal-footer">
                            <input type="hidden" name="registerbtn" value="111"/>
                            <input name="discount" type="hidden" value="5">

                            <button class="btn orange-btn" type="submit" value="account/register" name="registerbtn1"
                                    style="pointer-events: all; cursor: pointer;">Зарегистрироваться
                            </button>
                            <p class="form-control-static inline grey">
                                <sup>*</sup> - поля обязательны для заполнения
                            </p>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    {/if}

    <!-- Pay Modal -->
    <div class="modal fade" id="payModal" tabindex="-1" role="dialog" aria-labelledby="payModalLabel" aria-hidden="true">

            <div class="modal-dialog">

                <div class="modal-content">
                    <form id="getorder" class="form-horizontal" method="get">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"></button>
                        <h3 class="modal-title greener" id="paytModalLabel"><b>Оплатить заказ</b></h3>
                    </div>
                    <div class="modal-body">
                        <div class="form-group number-pay">
                            <label id="payInpt">Введите номер заказа</label>
                            <input name="msorder" type="text" class="form-control" value="" required/>
                            <input name="code" type="text" class="hide" value=""/>
                            <span class="icon green-check-icon hidden"></span>
                            <span class="icon red-check-icon hidden"></span>
                        </div>

                        <div class="errorliqpay hide help-block with-errors">
                        </div>

                    </div>
                    <div class="modal-footer hidden">
                        <div class="pull-left">
                            <p class="h3 name"><b></b></p>

                            <ul class="list-inline">
                                <li>
                                    <b class="grey inline" style="vertical-align: bottom;">
                                        <span class="h3">К оплате:</span>
                                    </b>
                                </li>
                                <li>
                    <span class="price-large inline" style="vertical-align: bottom;">
                   <span class="product-price">0 <span class="small text-top">00</span></span>
                    </span>
                                </li>
                            </ul>
                            <ul class="list-inline">
                                <li>
                                    <b class="grey inline" style="vertical-align: bottom;">
                                        <span class="h3">Способ оплаты:</span>
                                    </b>
                                </li>
                                <li>
                                    <span class="payment"></span>
                                </li>
                            </ul>

                            <ul class="list-inline">
                                <li>
                                    <b class="grey inline" style="vertical-align: bottom;">
                                        <span class="h3">Способ доставки:</span>
                                    </b>
                                </li>
                                <li>
                                    <span class="deliveryorderfooter"></span>
                                </li>
                            </ul>
                        </div>

                        <button type="submit" name="ms2_action"
                                class="btn orange-btn text-uppercase pull-right payorder" data-value="">Оплатить
                        </button>
                    </div>

                </form>
                </div>
            </div>

    </div>
    <!-- end Pay Modal -->


    {if (!$modx->user->isAuthenticated())}
        <div class="modal fade" id="subscribeSuccessModal" tabindex="-1" role="dialog"
             aria-labelledby="thanksCallModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"></button>
                        <h3 class="modal-title greener" id="thanksCallModalLabel"></h3>
                        <div class="modal-body text-center">
                            <br><br>
                            <h3 class="modal-title greener"><b>Спасибо</b></h3>
                            <br>
                            <div class="thanks-text">На Ваш e-mail отправлено письмо с инструкцией по восстановлению
                                пароля
                            </div>
                            <br><br>
                        </div>
                        <div class="modal-footer">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    {/if}





    <!-- Add Modal -->
    <div class="modal fade" id="showMapModal" tabindex="-1" role="dialog" aria-labelledby="showMapModalLabel" aria-hidden="true">
        <div class="modal-dialog" style="" >
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">

                </div>
                <div class="modal-footer">
                    <a href="#" class="inline small back-modal pull-right" data-dismiss="modal">Перейти на сайт</a>
                </div>
            </div>
        </div>
    </div>
    <!-- end Thanks -->

</div>

<script src="https://cdnjs.cloudflare.com/ajax/libs/fotorama/4.6.4/fotorama.js"></script> <!-- 16 KB -->
{literal}<!--Render time: [^t^] <br/>
     Запросы к базе [^q^]<br/>
     Время запросов [^qt^]-->
{/literal}
{literal}
    <script>
        jQuery(document).ready(function(){
            jQuery('.product-item-title a,.product-item-img a').on('click',function(){

                id=jQuery(this).data('id');
                type=$(this).data('type');

                name=jQuery('#'+type+'_'+id).data('name');
                brand=jQuery('#'+type+'_'+id).data('brand');
                price=jQuery('#'+type+'_'+id).data('price');
                category=jQuery('#'+type+'_'+id).data('category');
                list=jQuery('#'+type+'_'+id).data('list');
                position=jQuery('#'+type+'_'+id).data('position');
                dataLayer.push({
                    'event': 'gtm-ee-event',
                    'gtm-ee-event-category': 'Enhanced Ecommerce',
                    'gtm-ee-event-action': 'Product Clicks',
                    'gtm-ee-event-non-interaction': 'False',

                    'ecommerce': {
                        'click': {
                            'actionField': {'list': list},
                            'products': [{
                                'name': name,
                                'id': id,
                                'price': price,
                                'brand': brand,
                                'category': category,
                                'variant': '',
                                'position': position
                            }]

                        }
                    }
                });

                //
               // return false;
            });

            // Vet answer form vis
//===============================================================
            //.btn_answer_vis a
            $(".btn_answer_vis a").on('click', function () {
                //$('.answer-form-wrapper').
                id=$(this).data('parent');
                $('.comment_answer_wr').html('');
                //alert(id);
                clonehtml=jQuery('#answer-form-wrapper').clone();
                clonehtml.find('input[name="parent"]').val(id);

                //.show().appendTo('#comment_answer_'+id);
                $('#comment_answer_'+id).html(clonehtml.html());
            });
            $(document).on('click', ".btn_cancel",function () {
                $(this).parents(".answer_footer_container").find(".answer_footer_form").slideToggle(200);
            })
        })
    </script>
{/literal}

{literal}
 <!-- Add Modal -->
    <div class="modal fade" id="showCartModal" tabindex="-1" role="dialog" aria-labelledby="showCartModalLabel"
         aria-hidden="true">
        <div class="modal-dialog" style="width:780px">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
                    
                </div>

            </div>
        </div>
    </div>
    <!-- end Thanks -->
{/literal}
{if (!$modx->user->isAuthenticated())}

    {literal}


<!-- Add Modal -->
<div class="modal fade" id="addAskregistrModal" tabindex="-1" role="dialog" aria-labelledby="addAskregistrModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"></button>
                <img src="/assets/images/popup_sale.png" alt="Дарим скидку 5%" />
            </div>
            <div class="modal-body">

                [[%polylang_site_popup_regdesktop]]

                <form id="form_reg_popup" action="/account/register" method="post" class="clearfix" data-toggle="validator" role="form">

                    <div class="row">
                        <div class="col-xs-12">
                            <div class="form-group ">
                                <label class="control-label">[[%polylang_site_order_name]]</label>
                                <input class="form-control custom-inpt modal-fullname-inp" name="fullname" type="text" placeholder="[[%polylang_site_order_name]]" required>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="form-group ">
                                <label class="control-label">Телефон</label>
                                <input class="form-control custom-inpt modal-phone-inp" name="phone" type="text" placeholder="+38" required>
                                <div class="help-block with-errors hide"></div>


                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="form-group ">
                                <label class="control-label">E-mail</label>
                                <input class="form-control custom-inpt modal-email-inp" name="email" type="email" placeholder="E-mail" required>
                            </div>
                        </div>
                    </div>
                    <input type="hidden" name="registr_forpopup" value="1"/>
                    <input type="hidden" name="code" value=""/>
                    <input name="discount" type="hidden" value="5">
                    <input name="registerbtn" type="hidden" value="1">

                    <button type="submit" id="registrpopup" name="registerbtn" value='1' class="btn orange-btn pull-right">[[%polylang_site_registr_reg]]</button>
                </form>


            </div>
            <div class="modal-footer">
                <a href="#" class="inline small back-modal pull-right" data-dismiss="modal">Перейти на сайт</a>
            </div>
        </div>
    </div>
</div>
<!-- end Thanks -->
{/literal}
{/if}


{literal}
        <script  src="/assets/scripts/bootstrap.min.js"></script> 
         <script  src="/assets/components/msearch2/js/web/lib/jquery-ui-1.10.4.custom.min.js"></script> 
        <script  src="/assets/fancybox/jquery.fancybox.js"></script> 
        <script  src="/assets/scripts/jquery.bxslider.js"></script> 
        <script  src="/assets/scripts/jquery.barrating.js"></script> 
        <script  src="/assets/scripts/vendor_v4.js"></script>
        <script>
         jQuery(document).ready(function () {
                 
            
        
            $('.shared_facebook').on('click',function(){
                FB.ui({
                    method: 'share',
                    href: 'https://petchoice.ua/{/literal}{link id={field name=id}}{literal}',
                }, function(response){
                    $.ajax({
                        type: "POST",
                        url: '/assets/components/minishop2/raiting_product.php',
                        data: 'product_id={/literal}{field name=id}{literal}&type=shared',
                    }).done(function (data) {

                        //data = $.parseJSON(data);
                        //console.log(data);
                    });
                });
                return false;
            });
            });
            </script>
            
          
{/literal}
</body>
</html>
