<!DOCTYPE html>
<html lang="ru">
<head>
{snippet name=metaprevnext}
<style>
    @font-face{
    font-family:'Glyphicons Halflings';src:url("https://m.petchoice.ua/assets/bower_components/bootstrap-sass-official/assets/fonts/bootstrap/glyphicons-halflings-regular.eot");
src:url("https://m.petchoice.ua/assets/bower_components/bootstrap-sass-official/assets/fonts/bootstrap/glyphicons-halflings-regular.eot?#iefix") format("embedded-opentype"),url("https://m.petchoice.ua/assets/bower_components/bootstrap-sass-official/assets/fonts/bootstrap/glyphicons-halflings-regular.woff2") format("woff2"),url("https://m.petchoice.ua/assets/bower_components/bootstrap-sass-official/assets/fonts/bootstrap/glyphicons-halflings-regular.woff") format("woff"),url("https://m.petchoice.ua/assets/bower_components/bootstrap-sass-official/assets/fonts/bootstrap/glyphicons-halflings-regular.ttf") format("truetype"),url("https://m.petchoice.ua/assets/bower_components/bootstrap-sass-official/assets/fonts/bootstrap/glyphicons-halflings-regular.svg#glyphicons_halflingsregular") format("svg")
}


v
</style>
    <script>

        {if (!$modx->user->isAuthenticated())}
        registration='guest';
        {else}
        {if ($modx->user->username=='admin')}

        registration='manager';
        {elseif (($modx->user->isMember('manager5'))||($modx->user->isMember('manager2'))||($modx->user->isMember('manager3'))||( $modx->user->isMember('manager')))}

        registration='manager';

        {else}
        registration='user';
        {/if}

        {/if}


        {if ($type=='product')}
        {literal}

        dataLayer=[{
            'dynx_itemid': '{/literal}{field name=id}{literal}',
            'dynx_pagetype': 'offerdetail',
            'dynx_totalvalue': {/literal}[[+price]]{literal},
            'registration':registration
        }];

        {/literal}
        {/if}
        {if ($type=='home')}
        {literal}
        dataLayer=[{
            'dynx_itemid': '',
            'dynx_pagetype': 'home',
            'dynx_totalvalue': 0,
            'registration':registration
        }];
        {/literal}
        {/if}
        {if ($type=='cart')}
        {literal}
        dataLayer=[{
            'dynx_itemid': '',
            'dynx_pagetype': 'conversionintent',
            'dynx_totalvalue': 0,
            'registration':registration
        }];
        {/literal}
        {/if}
        {if ($typemarketing=='successorder')}
        {literal}
        dataLayer=[{
            'dynx_itemid': '',
            'dynx_pagetype': 'conversion',
            'dynx_totalvalue': "{/literal}[[+cart_cost]]{literal}",
            'registration':registration
        }];
        {/literal}
        {/if}
        {if ($type=='other')}
        {literal}
        dataLayer=[{
            'dynx_itemid': '',
            'dynx_pagetype': 'other',
            'dynx_totalvalue': 0,
            'registration':registration
        }];
        {/literal}
        {/if}
        {literal}
    </script>
    <!-- Google Tag Manager -->
    <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
            new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
            j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
            'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
        })(window,document,'script','dataLayer','GTM-W5VBDZ');</script>
    <!-- End Google Tag Manager -->
    {/literal}
    {*<base href="https://m.petchoice.ua/">*}
    <base href="https://m.petchoice.ua/">
    
    {snippet name=m_getcanonical}
    
    
    {$robots}

    <meta http-equiv="Content-Type" content="text/html;charset=UTF-8"/>
    <meta http-equiv="imagetoolbar" content="no"/>

    <meta name="format-detection" content="telephone=no"/>
    <meta http-equiv="x-rim-auto-match" content="none"/>

    <meta name="apple-mobile-web-app-capable" content="yes"/>
    <meta name="viewport" content=" width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no"/>

    <meta charset="utf-8"/>
    <title>{snippet name=metatitle params="title={field name=pagetitle}&seotitle=`{field name=longtitle}`"}</title>

    <meta name="keywords" content=" "/>
    <meta name="description" content="{snippet name=metadescription params="description=`{field name=description}`"}">

    <link href="//fonts.googleapis.com/css?family=Open+Sans:300,400,700,800&subset=latin,cyrillic-ext,cyrillic"
          rel="stylesheet" type="text/css">
    <link href="//fonts.googleapis.com/css?family=Open+Sans+Condensed:300,700&subset=latin,cyrillic" rel="stylesheet"
          type="text/css">
          <!--
          <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
          -->
    <!--<link href="/assets/mobile/css/resetcss.css" rel="stylesheet" type="text/css">-->
    
    
    
    <!--<link href="/assets/styles/bootstrap.min.css" rel="stylesheet" type="text/css">         
    <link href="/assets/mobile/css/swiper.min.css" rel="stylesheet" type="text/css">         -->
    
        <!--         -->
        
        
        <link href="/assets/mobile/build/css/libs.min.css?ver=277" rel="stylesheet" type="text/css">
        <!--<link href="/assets/mobile/css/styles_v1.css?ver=2222" rel="stylesheet" type="text/css">-->
        <link href="/assets/mobile/build/css/style.min.css?ver=277" rel="stylesheet" type="text/css">
        <!--
        <link href="/assets/components/tickets/css/web/default.css" rel="stylesheet" type="text/css">         
        -->
          
    {literal}
        <style>
            .modal-dialog{margin-top:80px!important;}
        </style>
        
       <!-- <style>
            .delivery_header span.activelang{
                font-weight:700;
                border-bottom: none;
                    display: inline-block;
            margin: 0px 0px 0 0;
         color: #476dc7;
            text-decoration: none;
            background-image:none!important;
            }
            .main-header > ul > li.polylanglinks{
                left:70px;
                width:70px;
                
                background:none;
                
            }
            .main-header > ul > li.polylanglinks > span
            {
                display:inline;
                float:left;
                width:50%;
                    padding-top: 12px;
            }
        </style>-->
    {/literal}
         
    <!-- favicon -->
    <link rel="apple-touch-icon" type="text/css" href="favicon.png"/>
    <link rel="shortcut icon" type="image/x-icon" href="favicon.ico"/>
    
    {literal}
        <!-- vk widget -->
        <!--<script async type="text/javascript" src="//vk.com/js/api/openapi.js?116"></script>-->
    {/literal}
</head>
<body style="min-width:100%; width:auto!important;" class="action_event_1">
<script>
    !function (t, e, c, n) {
        var s = e.createElement(c);
        s.async = 1, s.src = 'https://statics.esputnik.com/scripts/' + n + '.js';
        var r = e.scripts[0];
        r.parentNode.insertBefore(s, r);
        var f = function () {
            f.c(arguments);
        };
        f.q = [];
        f.c = function () {
            f.q.push(arguments);
        };
        t['eS'] = t['eS'] || f;
    }(window, document, 'script', '9AD2834E688B486E93DCDF7D7D0B4C20');
</script><script>eS('init');</script>   

[[eSputnikCustomerData]]
{literal}

    <!-- Google Tag Manager (noscript) -->
    <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-W5VBDZ";
                      height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
    <!-- End Google Tag Manager (noscript) -->
{/literal}
{literal}
    
     <div id="fb-root"></div>
<script async defer crossorigin="anonymous" src="https://connect.facebook.net/ru_RU/sdk.js#xfbml=1&version=v9.0&appId=1450437548564324&autoLogAppEvents=1"; nonce="h7InQuzt"></script>
    <!--
    <script>
      //$(window).load(function () {
        window.fbAsyncInit = function() {

            FB.init({
                appId: '2345507562185950',
                //authResponse: false,

                cookie: false,
                oauth: false,
                status: true,
                xbfml: true,
                autoLogAppEvents : true,
                //xfbml            : true,
                version          : 'v3.2'
            });

            FB.Event.subscribe("edge.create", function (targetUrl) {
                alert('edge.create');
                console.log('edge.create');
            });
            FB.Event.subscribe("edge.remove", function (targetUrl) {
                alert('edge.remove');
                console.log('edge.remove');
            });
            /*
            FB.Event.subscribe('edge.create',
                function(response) {
                    alert('You liked the URL: ' + response);
                }
            );

            FB.Event.subscribe('edge.remove',
                function(response) {
                    alert('You UNliked the URL: ' + response);
                }
            );*/
           
        };
      //});


        (function(d, s, id) {
            var js, fjs = d.getElementsByTagName(s)[0];
            if (d.getElementById(id)) return;
            js = d.createElement(s); js.id = id;
            js.src = 'https://connect.facebook.net/ru_RU/sdk.js#xfbml=1&version=v3.2&appId=2345507562185950';
            fjs.parentNode.insertBefore(js, fjs);
        }(document, 'script', 'facebook-jssdk'));
        
        </script>-->
<!-- fb widget 
    <div id="fb-root"></div>
    <script>(function (d, s, id) {
            var js, fjs = d.getElementsByTagName(s)[0];
            if (d.getElementById(id)) return;
            js = d.createElement(s);
            js.id = id;
            js.src = "//connect.facebook.net/ru_RU/sdk.js#xfbml=1&version=v2.0";
            fjs.parentNode.insertBefore(js, fjs);
        }(document, 'script', 'facebook-jssdk'));</script>
        -->
    <!--  end fb -->
{/literal}

{snippet name=mobilepreloader}
{if (!$modx->user->isAuthenticated())}
    <script type="text/javascript">
        dataLayer.push({
            'registration': 'guest',
        });
    </script>
{else}
{if ($modx->user->username=='admin')}
    <script type="text/javascript">
        dataLayer.push({
            'registration': 'manager',
        });
    </script>
{elseif (($modx->user->isMember('manager5'))||($modx->user->isMember('manager2'))||($modx->user->isMember('manager3'))||( $modx->user->isMember('manager')))}
    <script type="text/javascript">
        dataLayer.push({
            'registration': 'manager',
        });
    </script>

{else}
    <script type="text/javascript">
        dataLayer.push({
            'registration': 'user',
        });
    </script>
{/if}

.
{/if}
<div  class="header_action header_action_info"></div>
<header class="main-header" style="z-index:10!important;">
    <ul class="main-header__menu">
        <li class="main-header__menu__burger">
        <div class="--svg__burger-before"></div>
        </li>
        <li class="main-header__menu__logo"><a href="{config name=site_url}" title="на главную" class="--svg__logo"></a></li>
        <li class="main-header__menu__search"> {literal}
            [[!mSearchForm?
            &element=`msProducts`
            &tplForm=`m_tpl.mSearch2.form`
            &fields=`Data.article:3,Data.vendor:2`
            &where=`{"Data.price:>":0}`
            ]]
        {/literal}
        </li>
        <li class="main-header__menu__cart">
        {nocache}{block name=minicart nocache}{snippet name=msMiniCart_m}{/block}{/nocache}
        </li>
    </ul>
</header>

 {* *}
{if ($modx->resource->id!=8)}
    [[!Login? &tplType=`modChunk` &loginTpl=`mobilheadNotLoggedInTpl` &logoutTpl=`mobilheadLoggedInTpl`]]
{/if}
        
<div class="wrapper-burger" id="main_menu">
        <div class="main_menu" >
            <div class="main_menu_head">
                <span><img src="/assets/mobile/build/img/logo2_mobile.svg"/></span>
                <span class="close_modal"></span>   
            </div>
            
                {if (!$modx->user->isAuthenticated())}
                <div class="main_menu_account">
                    <div class="account">
                        <span class="icon_account"></span>
                    </div>
                    
                    <div class="main_menu_auth">
                        <a class="link_login" href="javascript:void(0)">[[%polylang_site_loginform_entr]]</a>                    
                    </div>
                    <div class="main_menu_auth">
                        <a class="link_registr" href="javascript:void(0)">[[%polylang_site_registr_new]]</a>
                    </div>
                </div>
                {else}
                    [[!Login? &tplType=`modChunk` &loginTpl=`mobilheadNotLoggedInTpl` &logoutTpl=`m_mobilheadLoggedInTpl`]]
                {/if}
            
            <ul class="main_menu_items">
                <li class="catalog_item"><a href="javascript:void(0);" class="open_menu_catalog">[[%polylang_site_common_menu_catalog]]</a></li>
                <li class="cart_item">{nocache}{block name=minicart nocache}{snippet name=msMiniCart_m params='cart_menu=1'}{/block}{/nocache}</li>
            </ul>
            <div class="main_menu_title">
                [[%polylang_site_lang_text]]
            </div>
            <div class="main_menu_lang">
                {snippet name=PolylangLinks params='mode=`single`'}
            </div>
            
            <div class="main_menu_title">
            [[%polylang_site_label_help]]                
            </div>
            <ul class="main-menu">                
                    <li class="delivery_item"><a href="{snippet name=PolylangMakeUrl  params='id=473'}">[[%polylang_site_common_menu_delivery]]</a></li>
                    <li class="contacts_item"><a href="{snippet name=PolylangMakeUrl  params='id=4'}">[[%polylang_site_common_menu_contact]]</a></li>                    
                    <li class="loylnost_item"><a href="{snippet name=PolylangMakeUrl  params='id=9'}">[[%polylang_site_common_menu_loylnost]]</a></li>                    
                    <li class="actions_item"><a href="{snippet name=PolylangMakeUrl  params='id=2060'}">[[%polylang_site_label_akcii]]</a></li>
                    <li class="brands_item"><a href="/brands">[[%polylang_site_main_lable_brand]]</a></li>
            </ul>
            
            <div class="main_menu_title">
            [[%polylang_site_label_other]]
                
            </div>
            <ul class="main-menu">
                <li class="voprosvet_item"><a href="{snippet name=PolylangMakeUrl  params='id=559'}">[[%polylang_site_common_menu_voprosvet]]</a></li>
                <li class="reviews_item"><a href="{snippet name=PolylangMakeUrl  params='id=755'}">[[%polylang_site_product_review_label]]</a></li>
                <li class="blog_item"><a href="{snippet name=PolylangMakeUrl  params='id=529'}">[[%polylang_site_common_menu_blog]]</a></li>
                <li class="ratingkorm_item"><a href="https://petchoice.ua/">[[%polylang_site_common_menu_fullwww]]</a></li>
                <!--<li class="ratingkorm_item"><a href="{snippet name=PolylangMakeUrl  params='id=12430'}">[[%polylang_site_common_menu_retingkorm]]</a></li>-->
            </ul>
            <div class="main_menu_title">
                [[%polylang_site_common_social]]
            </div>
            <ul class="main_menu_social">
                <li><a class="facebook_link" href="/"><span></span></a></li>
                <li><a class="instagram_link" href="/"><span></span></a></li>
            </ul>
            
            
            {if ($modx->user->isAuthenticated())}
                <a href="/account?service=logout" class="button whitefon logout">[[%polylang_site_cmenu_exit]]</a>
            {/if}
        </div>
</div>




<div class="wrapper-burger" id="autocomplete_popup">
        <div class="main_menu main-menu-list">
        
            <div class="main_menu_head">
                <span>
                {literal}
                [[!mSearchForm?
                    &element=`msProducts`
                    &tplForm=`m_tpl.mSearch2.form_popup`
                    &fields=`Data.article:3,Data.vendor:2`
                    &where=`{"Data.price:>":0}`
                    ]]
                    {/literal}
                </span>
                <span class="close_modal"></span>   
            </div>
            <div id="result_automplete_search">
                <div class="result_category">
                    <span>Категории</span>
                    <div class="category_res">
                        
                    </div>
                    
                    
                </div>
                <div class="result_products">
                    <span>Продукты</span>
                    <div class="products_res">
                        
                    </div>
                </div>
                
            </div>
            <div class="footer_result_search">
                <div class="show_all_result">
                    <a href="javascript:void(0)" class="button orange">Показать все</a>
                </div>
                <a href="javascript:void(0)" class="button whitefon close_autocomplete_popup">Закрыть</a>
            </div>
        </div>
</div>




<div class="wrapper-burger" id="menu_catalog">
        <div class="main_menu main-menu-list">
        
            <div class="main_menu_head">
                <span>Каталог товаров</span>
                <span class="close_modal"></span>   
            </div>
            {assign var=params value=[
            "startId"       => 10
            ,"level"        => 2
            ,"sortBy"       => "menuindex"
            ,"levelClass"   => "level"
            ,"cacheable"    =>false
            ,"id"           => "catalog"
            ]}
            {processor action="site/web/menu/getcatalogmenu" ns="modxsite" params=$params assign=result}
            {assign var=items value=$result.object}
            {include file='tpl/m/menu/menumain.tpl'}

        </div>
        
</div>


<div class="body_container" id="main-container">

   {* если главная *}
    {if ($modx->resource->id==1)}
        
       
        {block name=mainpage}{/block}
    {else}
        {block name=content}{/block}
    {/if}
    

    {snippet name=m_getmessage}
    <footer class="main-footer">
        <ul class="contacts" id="phones-anchor">
            <li>
            [[%polylang_site_common_orderphone]]                
            </li>
            <li class="main-phone">
                <a href="tel:+380487005080">048 700-50-80</a>
            </li>
            <li>
                [[%polylang_site_common_otherphone]]
            </li>
            <li class="other-phones">
                <div class="icons-phones">
                    <a href=""><span class="icon_viber"></span></a>
                    <a href=""><span class="icon_telegram"></span></a>
                </div>
                <ul>
                    <li><a href="tel:+380635055003">063 505-50-03</a></li>
                    <li><a href="tel:+380678416464">067 841-64-64</a></li>
                    <li><a href="tel:+380955056595">095 505-65-95</a></li>
                </ul>
                
            </li>
            <li class="work-time">
                <div>
                    <span>[[%polylang_site_common_timespn]]: 09:00 - 18:00</span>
            		<span>[[%polylang_site_common_timessub]]: 09:00 - 16:00</span>
                </div>
            </li>
        </ul>

        <div class="title_social">
            [[%polylang_site_common_social]]
        </div>
        <ul class="btns-social">
            <li class="soc-icn-fb"><a href="//www.facebook.com/petchoice.com.ua"></a></li>
            <li class="soc-icn-in"><a href="https://www.instagram.com/petchoice_ua/"></a></li>
        </ul>
        <nav class="copyright">
            &copy; 2016 [[%polylang_site_common_copyright]]
        </nav>
    </footer>

    <!-- Add Modal -->
    <div class="modal fade" id="showMapModal" tabindex="-1" role="dialog" aria-labelledby="showMapModalLabel" aria-hidden="true">
        <div class="modal-dialog" style="width:auto;height:500px;" >
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">

                </div>
                <div class="modal-footer">
                    <a href="#" class="inline small back-modal pull-right" data-dismiss="modal">Перейти на сайт</a>
                </div>
            </div>
        </div>
    </div>
    <!-- end Thanks -->
</div> <!-- #main-container -->
<!--<script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
<script type="text/javascript" src="/assets/mobile/js/jquery.easings.min.js"></script>
<script type="text/javascript" src="/assets/mobile/js/swiper.min.js"></script>
<script type="text/javascript" src="/assets/mobile/js/script.js"></script>
<script src="/assets/scripts/m/m_vendor.js"></script>
<script src="/assets/scripts/m/scripts.js"></script>
/assets/components/msearch2/js/web/m/default.js-->
<!--    <script src="/assets/components/msearch/js/mfilter.js" type="text/javascript"></script>
	
	<script type="text/javascript" src="/assets/scripts/img_fit.js"></script>
	-->
    
    
<div class="wrapper-burger" id="forgotpas_profile">
        <div class="main_menu main-menu-list">
        
            <div class="main_menu_head">
                <span>Восстановить пароль</span>
                <span class="close_modal"></span>   
            </div>
        <div id="office-auth-form" class="modal-content">
            {snippet name=officeAuth params="tplLogin=`m_forgotPasswordFormTpl`&HybridAuth=`0`"}
        </div>
    </div>
</div>

<style>
    .btn.link-btn.light
    {
            color: #838fa3;
    background: none;
    border: none;
    box-shadow: none;
    }
    .modal-content{
    border-radius:0px;
         -webkit-box-shadow: none!important;
     box-shadow: none!important;
    
    }
</style>
    {if (!$modx->user->isAuthenticated())}
    
        <div class="modal fade" id="recoverySuccessModal" tabindex="-1" role="dialog" aria-labelledby="thanksCallModalLabel" aria-hidden="true">
            <div class="modal-dialog">
              <div class="modal-content">
                <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close"></button>
                  <h3 class="modal-title greener" id="thanksCallModalLabel"></h3>
                </div>
                <div class="modal-body text-center">
                  <br><br>
                  <h3 class="modal-title greener"><b></b></h3>
                  <br>
                  <div class="thanks-text"></div>
                  <br><br>
                </div>
                <div class="modal-footer">
                </div>
              </div>
            </div>
          </div>
    {/if}




<!--<script type="text/javascript" src="/assets/components/msearch2/js/web/m/default.js"></script>-->
{literal}
<!--<script>
    jQuery(document).ready(function(){
        jQuery('.prev-image a,.prev-name a,.product-item-title a,.product-item-img a,.big-prev-img a,.big-prev-name a, .prev-buy-btn a').on('click',function(){
            id=jQuery(this).data('id');
            type=jQuery(this).data('type');


            name=jQuery('#'+type+'_'+id).data('name');

            brand=jQuery('#'+type+'_'+id).data('brand');
            price=jQuery('#'+type+'_'+id).data('price');
            category=jQuery('#'+type+'_'+id).data('category');
            list=jQuery('#'+type+'_'+id).data('list');
            position=jQuery('#'+type+'_'+id).data('position');
            dataLayer.push({
                'event': 'gtm-ee-event',
                'gtm-ee-event-category': 'Enhanced Ecommerce',
                'gtm-ee-event-action': 'Product Clicks',
                'gtm-ee-event-non-interaction': 'False',

                'ecommerce': {
                    'click': {
                        'actionField': {'list': list},
                        'products': [{
                            'name': name,
                            'id': id,
                            'price': price,
                            'brand': brand,
                            'category': category,
                            'variant': '',
                            'position': position
                        }]

                    }
                }
            });

          //  return false;
        });
    })
</script>-->
{/literal}






{if (!$modx->user->isAuthenticated())}

 <script>
    {if ($smarty.server.REMOTE_ADDR=='188.130.177.18')}
    {literal}
    jQuery(document).ready(function() {
        jQuery("input[name='phone']").mask("+389999999999");

        //jQuery('.popup_w').addClass('active');
        jQuery('#addAskregistrModal').modal('show');
        jQuery('.btn_close_w').on('click',function(){
            jQuery('#addAskregistrModal').modal('hide');
        });
        jQuery('.modal-email-inp, .modal-fullname-inp,.modal-phone-inp').on('click',function(){
            that=this;
            setTimeout(function(){jQuery(that).focus();},1000);
           // var destination = $(this).offset().top +200;
          //  jQuery('body').animrate({ scrollTop: destination }, 100); //1100 - скорость
        });

    });
    {/literal}
    {/if}
    </script>
{literal}


<!--tabindex="-1"-->
    <div class="modal fade" id="addAskregistrModal"  role="dialog" aria-labelledby="addAskregistrModalLabel" aria-hidden="false">
        <div class="modal-dialog" style="width:auto;max-width:400px;">
            <div class="modal-content">
                <!--<div class="modal-header">



                </div>-->
                <div class="modal-body">
                    <!--<button type="button" style="height: 18px;z-index:999!important;" class="close" data-dismiss="modal" aria-label="Close"></button>-->
                    <span style="cursor:pointer" class="btn_close_w"></span>
                    <div>
                        <img src="/assets/images/popup_sale.png" alt="" />
                        <div class="hn6">
                            <h3>
                                Зарегистрируйтесь и покупайте со скидкой:
                            </h3>
                        </div>
                        <ul class="list-unstyled green-bullet modal-bullet">
                            <li>скидка 5% на первую покупку, накопительная до 10%</li>
                            <li>получайте бонусы</li>
                            <li>следите за размером скидки и стоимостью покупок</li>
                        </ul>
                    </div>

                    <form style=" padding:10px;" class="formaregistration3 form_stretchy form_mob_reg "
                          method="post"
                          data-toggle="validator"
                          role="form"
                          novalidate="true" action="/account/register"  id="form_reg_popup" >

                    <div class="row">
                        <div class="col-xs-12">
                            <div class="form-group">
                                <label class="control-label">[[%polylang_site_order_name]]</label>
                                <input class="form-control custom-inpt modal-fullname-inp" name="fullname" type="text" placeholder="Имя" required />
                            </div>
                        </div>
                        <div class="col-xs-12">
                            <div class="form-group ">
                                <label class="control-label">Телефон</label>
                                <input class="form-control custom-inpt modal-phone-inp" name="phone" type="tel" placeholder="+38" required />
                                <div class="help-block with-errors hide"></div>


                            </div>
                        </div>
                        <div class="col-xs-8">
                            <div class="form-group">
                                <label class="control-label">E-mail</label>
                                <input class="form-control custom-inpt modal-email-inp" name="email" type="email" placeholder="E-mail адрес" required />
                            </div>
                        </div>
    <div class="col-xs-8">
                        <input type="hidden" name="registr_forpopup" value="1"/>
                        <input type="hidden" name="code" value=""/>
                        <input name="discount" type="hidden" value="5">
                        <input name="registerbtn" type="hidden" value="1">

                        <button type="submit" id="registrpopup" name="registerbtn" value='1' class="btn orange-btn pull-right">Зарегистрироваться</button>
    </div>
</div>
                    </form>


                </div>

            </div>
        </div>
    </div>

    
{/literal}
{/if}

{literal}
<script src="https://code.jquery.com/jquery-3.6.4.min.js"></script>
<script type="text/javascript" src="/assets/mobile/build/js/libs.min.js?ver=57"></script>

<!--<script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js?ver=4"></script>-->
<!--
<script type="text/javascript" src="/assets/mobile/js/jquery.easings.min.js?ver=4"></script>
<script type="text/javascript" src="/assets/mobile/js/swiper.min.js?ver=5"></script>-->
<script type="text/javascript" src="/assets/mobile/js/script_v2.js?ver=56"></script>

<script type="text/javascript" src="/assets/mobile/build/js/main.min.js?ver=59"></script>

<script src="/assets/scripts_m/m_vendor_v2.js?ver=56"></script>
<script src="/assets/scripts_m/scripts.v2.js?ver=56"></script>

<script src="/assets/components/msearch2/js/web/m/default.js?ver=56"></script>

<style>
.help-block.with-errors ul{
    padding-left:0px;
        margin-left:0px;
        margin-bottom:0px!important;
        padding-bottom:0px!important;
}
    .help-block.with-errors ul li
    {
        padding-left:0px;
        margin-left:0px;
        
            margin-bottom: 0px;
    padding-left: 0px;
    margin-top: 5px;

    }
    .form-group.has-error input{
        border:1px solid #c53048!important;
    }
</style>
<script>
    jQuery(document).ready(function(){
       jQuery(document).on('submit','#login_profile form',function(){
          
          var dataForm=jQuery(this).serialize();          
          var action_url=jQuery(this).attr('action');
          
          console.log('submit again');
          jQuery.ajax({
                type: "POST",
                url: action_url,
                data: dataForm,
                success: function (data) {
                    
                    console.log('formlogin');
                    var formLogin=jQuery(data).find('.login-block form');
                    if (formLogin.length>0){
                        jQuery('#login_profile .wrapper_form_login').html(formLogin);//#login_profile
                        
                        setTimeout(function(){
                            $("input[name='username']").mask("+389999999999");    
                        },500);
                        
                    }else{
                        window.location.href='/account';
                        
                        
                        console.log('test');
                    }
                    
                },
                // complete: function (data) {
                //     //window.location.href='/account';
                //     console.log('complete');
                //     console.log(data);
                // },
                error: function (error)
                {
                    window.location.href='/account';
                    console.log('error');
                    console.log(error);
                }
            });
            
            
          return false;
       });
    });
</script>
{/literal}
</body>
</html>

