[[$header]]
      <div class="container">
        <div class="breadcrumbs">
          &nbsp;
        </div>
        <div class="tabs main-tab" role="tabpanel">
          <ul class="nav nav-tabs" role="tablist" id="mainTab">
            <li role="presentation" class="active">
              <a href="#actions-pane" aria-controls="actions-pane" role="tab" data-toggle="tab">Акционные предложения</a>
            </li>
            <li role="presentation">
              <a href="#new-products-pane" aria-controls="new-products-pane" role="tab" data-toggle="tab">Новые поступления</a>
            </li>
          </ul>

          <div class="tab-content">
            <div role="tabpanel" class="tab-pane active" id="actions-pane" aria-labelledby="actions-tab">
              <div class="product-slider slider">
                <ul class="bxslider slider-actions" style="width:100%;">
                 [[!msProducts? 
                      &parents=`10`
                      &tpl=`actionProductRowTpl` 
                      &where=`["Data.adiscount =1 OR Data.askidka =1"]`
                      &adiscount=`1`
                      &sortby=`RAND()`
                      &limit=`15`
                  ]]
                </ul>   <!-- $q->where(array('`msProductData`.`askidka`'=>1),xPDOQuery::SQL_OR); {"Data.adiscount":"1"}`-->
                <div style="text-align:center;width:100%;"><a href="/discounts" class="btn grey-btn">Просмотреть все акции </a></div>
              </div>
            </div>

            <div role="tabpanel" class="tab-pane" id="new-products-pane"  aria-labelledby="new-products-tab">
              <div class="product-slider slider">
                <ul class="bxslider slider-new-products">
                  [[!msProducts? 
                      &parents=`10`
                      &tpl=`newinProductRowTpl` 
                      &where=`{"Data.new":"1"}`
                      &includeThumbs=`150x223`
                       &sortby=`RAND()`
                      &limit=`15`
                  ]]
                  
                </ul>
              </div>
            </div>
          </div>
        </div>


        <div class="row">
          <div class="col-xs-6">
            <h2 class="h1">Отзывы покупателей <span class="small grey">Всего отзывов: [[!TicketCount? &resources=463]]</span></h2>
            [[!TicketLatest? 
                &limit=`1` 
                &fastMode=`0` 
                &action=`comments` 
                &tpl=`latestCommentTpl` 
                &includeContent=`1` 
                &sortby=`RAND()`
                &sortdir=``
                &commentshop=`1`
            ]]
          </div>

          <div class="col-xs-6">
            <h2 class="h1">Мы в соц-сетях</h2>
            <div class="row social-follows">
              <div class="col-xs-6">
                <!--FB Widget -->
                <div class="fb-like-box" data-href="https://www.facebook.com/petchoice.com.ua?ref=profile&amp;pnref=lhc" data-width="250" data-height="350" data-colorscheme="light" data-show-faces="true" data-header="false" data-stream="false" data-show-border="false"></div>
              </div>
              <div class="col-xs-6">
                <!-- VK Widget -->
                <!--<div id="vk_groups"></div>
                <script type="text/javascript">
                  VK.Widgets.Group("vk_groups", {mode: 0, width: "260", height: "350", color1: 'FFFFFF', color2: '838fa3', color3: 'afc4db'}, 84542368);
                </script>-->
              </div>
            </div>
          </div>
        </div>

        <div class="row follow-part">
          <div class="col-xs-4">
            <p class="title grey">Подпишитесь на рассылку </p><a class="why-follow" data-toggle="popover" data-popover-content="#follow-popover" data-placement="bottom">Зачем ?</a>
          </div>
          <div class="col-xs-8">
            <form action="/assets/components/minishop2/subscribe.php" class="formsubscribe" method="post" data-toggle="validator" role="form">
              <div class="form-group inline">
                <span class="inpt-icon name-inpt"></span>
                <input class="form-control custom-inpt" type="text"  name="name" placeholder="Ваше Имя" required>
              </div>
              <div class="form-group inline">
                <span class="inpt-icon email-inpt"></span>
                <input class="form-control custom-inpt" type="email"  name="email" placeholder="Ваш E-mail" required>
              </div>
              <button type="submit" class="btn green-btn text-uppercase buttonsubscribe"  id="buttonsubscribe" data-toggle="modal" data-target="#thanksCallModalLabel">Подписаться</button>
              
            </form>
            <div class="messagesubscribe"></div>
            <div class="animal-pic">
              <img src="/assets/images/puppy_3.png" alt="dog" />
            </div>
          </div>
        </div>

        <div class="row benefit-preview">
          <div class="col-xs-12">
            <h2 class="h1">Чем мы можем быть полезны ?</h2>
          </div>

          <div class="col-xs-3">
            <div class="helpfully-text">
              <h3 class="wider-bordered turquoise">Блог</h3>
              <p class="small">
                Интересные статьи и советы экспертов и ветеринаров об уходе, кормлении и воспитании животных
              </p>
            </div>
            <a href="[[~529]]" class="small pull-right">Подробнее</a>
          </div>

          <div class="col-xs-3">
            <div class="helpfully-text">
              <h3 class="wider-bordered greener">Адреса ветеренарных  клиник</h3>
              <p class="small">
                Адреса ветеринарных клиник в Одессе и Украине
              </p>
            </div>
            <a href="[[~476]]" class="small pull-right">Подробнее</a>
          </div>

          <!--<div class="col-xs-3">
            <div class="helpfully-text">
              <h3 class="wider-bordered purple">Приюты для животных</h3>
              <p class="small">
                С другой стороны дальнейшее развитие различных форм деятельности
                требуют определения и уточнения позиций, занимаемых участниками
                в отношении
              </p>
            </div>
            <a href="[[~556]]" class="small pull-right">Подробнее</a>
          </div>

          <div class="col-xs-3">
            <div class="helpfully-text">
              <h3 class="wider-bordered light-red">Задать вопрос ветеринару</h3>
              <p class="small">
                С другой стороны дальнейшее развитие различных форм деятельности
                требуют определения и уточнения позиций, занимаемых участниками
                в отношении
              </p>
            </div>
            <a href="[[~559]]" class="small pull-right">Подробнее</a>
          </div>-->
        </div>
        [[getBrands]]
        

        <div class="row text-part">
          <div class="col-xs-12">
            <h2 class="h1">Коротко о «PetChoice»</h2>
          </div>

          <div class="col-xs-6">
            <p>
              Наверняка вы любите животных, если заглянули в интернет-зоомагазин PetChoice. Может быть, вы их содержите, лечите или просто ищете интересный, необычный подарок. В любом случае, правильно сделали, вам у нас понравится, добро пожаловать! 
              </p>
              <h3 class="greener"><strong>Ваш питомец наверняка одобрил бы онлайн-зоомагазин PetChoice</strong></h3>
            <p>
              Магазин не зря называется PetChoice: если бы домашние животные могли сами выбирать для себя зоотовары, они бы обратили на нас внимание. Еще бы, ведь у нас без преувеличения есть ВСЁ для животных, рыб и птиц:
            </p>
            <ul class="list-unstyled green-bullet">
              <li>Корма: сухие, в виде консервов, еда для лечебной диеты, заменители молока и лакомства.</li>
              <li>Профилактические и лечебные препараты и аксессуары.</li>
              <li>Средства и приспособления для облегчения ухода за животными: шампуни, жидкости для удаления неприятных запахов, ножницы, триммеры, расчески, туалеты, наполнители для них и т.д.</li>
              <li>Товары для домашних животных, незаменимые в быту и на прогулках: ошейники, поводки, миски, игрушки, адресники, домики, переноски, аквариумы и оборудование для них, клетки и т. д.</li>
              <li>Одежда.</li>
            </ul>

            <div class="insertion">
              <div class="insertion-img inline">
                <img src="/assets/images/puppy_1.png" alt="dog" />
              </div>
              <div class="text-line inline">
                <i>Все товары интернет-магазина зоотоваров сертифицированы, производятся зарекомендовавшими себя производителями: Royal Canin, Pro Plan, Trixie, Hill's, Dog Chow, Cat Chow, 8in1, Bayer, Gourmet и другими.</i>
              </div>
            </div>
            <h3 class="greener"><strong>Больше 2 тысяч товаров для животных – это только половина того, что можно у нас найти</strong></h3>  

            <p>
              В PetChoice любят животных, поэтому мы с удовольствием ведем разделы:
            </p>

            <ul class="list-unstyled green-bullet">
              <li><i>Ветеринарные клиники/приюты</i>. Здесь собраны актуальные адреса и контакты ветклиник и приютов Одессы и других украинских городов. Нужна помощь врача или сами хотите помочь животным? Откройте закладки, зайдите на petchoice.com.ua и быстро найдите нужную информацию.</li>
              <li><i>Вопрос ветеринару</i>. Оставляйте свои вопросы, и практикующий ветеринарный врач поможет бесплатным советом.</li>
              <li><i>Блог</i>. Здесь публикуются интересные статьи, помогающие разобраться в актуальных вопросах ухода за животными.</li>
            </ul>
          </div>

          <div class="col-xs-6">
            <h3 class="greener"><strong>Покупки в магазине зоотоваров PetChoice – это еще и большое удовольствие</strong></h3>
            <p>
              Без преувеличений. Становясь нашим покупателем, вы получаете:
            </p>
            <ul class="list-unstyled green-bullet">
              <li><strong>Красочный каталог</strong> с удобным поиском и понятной навигацией.</li>
              <li><strong>Большие фото товаров</strong>. Мы не скрываем то, как выглядит продукция, поэтому часто предоставляем несколько фотографий.</li>
              <li><strong>Подробные описания зоотоваров</strong>. Если что-то интересует дополнительно, всегда можно спросить у наших менеджеров.</li>
              <li><strong>Отзывы о товаре</strong>. Общение – это прекрасно, мы уже ждем ваших комментариев.</li>
              <li><strong>Акции и скидки</strong>. Мы не жадные, и регулярно делаем интересные предложения, помогающие экономить. Дешевый зоомагазин в Украине – это вполне реально.</li>
              <li><strong>Дисконтная программа</strong>, действующая на постоянной основе: 3% скидки дается уже при <a href="/account/register">регистрации на сайте</a>. Покупая больше, вы получаете большую скидку.</li>
              <li><strong>Доставка по Украине</strong>. Зоомагазин находится в Одессе, отправки осуществляются быстро, в течение нескольких дней после заказа товар будет у вас. Мы предлагаем и <a href="/usloviya-dostavki-i-oplatyi">бесплатную доставку</a> – по Одессе и всей Украине.</li>
            </ul>
          </div>
        </div>
      </div>
      [[$footer]]