<!DOCTYPE html>
<html lang="ru">
<head>

    <script>

        {if (!$modx->user->isAuthenticated())}
        registration='guest';
        {else}
        {if ($modx->user->username=='admin')}

        registration='manager';
        {elseif (($modx->user->isMember('manager5'))||($modx->user->isMember('manager2'))||($modx->user->isMember('manager3'))||( $modx->user->isMember('manager')))}

        registration='manager';

        {else}
        registration='user';
        {/if}

        {/if}


        {if ($type=='product')}
        {literal}

        dataLayer=[{
            'dynx_itemid': '{/literal}{field name=id}{literal}',
            'dynx_pagetype': 'offerdetail',
            'dynx_totalvalue': {/literal}[[+price]]{literal},
            'registration':registration
        }];

        {/literal}
        {/if}
        {if ($type=='home')}
        {literal}
        dataLayer=[{
            'dynx_itemid': '',
            'dynx_pagetype': 'home',
            'dynx_totalvalue': 0,
            'registration':registration
        }];
        {/literal}
        {/if}
        {if ($type=='cart')}
        {literal}
        dataLayer=[{
            'dynx_itemid': '',
            'dynx_pagetype': 'conversionintent',
            'dynx_totalvalue': 0,
            'registration':registration
        }];
        {/literal}
        {/if}
        {if ($typemarketing=='successorder')}
        {literal}
        dataLayer=[{
            'dynx_itemid': '',
            'dynx_pagetype': 'conversion',
            'dynx_totalvalue': "{/literal}[[+cart_cost]]{literal}",
            'registration':registration
        }];
        {/literal}
        {/if}
        {if ($type=='other')}
        {literal}
        dataLayer=[{
            'dynx_itemid': '',
            'dynx_pagetype': 'other',
            'dynx_totalvalue': 0,
            'registration':registration
        }];
        {/literal}
        {/if}
        {literal}
    </script>
    <!-- Google Tag Manager -->
    <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
            new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
            j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
            'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
        })(window,document,'script','dataLayer','GTM-W5VBDZ');</script>
    <!-- End Google Tag Manager -->
    {/literal}
    <base href="https://m.petchoice.ua/">
    {snippet name=m_getcanonical}

    <meta http-equiv="Content-Type" content="text/html;charset=UTF-8"/>
    <meta http-equiv="imagetoolbar" content="no"/>

    <meta name="format-detection" content="telephone=no"/>
    <meta http-equiv="x-rim-auto-match" content="none"/>

    <meta name="apple-mobile-web-app-capable" content="yes"/>
    <meta name="viewport" content=" width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no"/>

    <meta charset="utf-8"/>
    <title>{snippet name=metatitle params="title={field name=pagetitle}&seotitle=`{field name=longtitle}`"}</title>

    <meta name="keywords" content=" "/>
    <meta name="description" content="{snippet name=metadescription params="description=`{field name=description}`"}">

    <link href="//fonts.googleapis.com/css?family=Open+Sans:300,400,700,800&subset=latin,cyrillic-ext,cyrillic"
          rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Open+Sans+Condensed:300,700&subset=latin,cyrillic"
          rel="stylesheet" type="text/css">
<!--
    <link rel="stylesheet" href="/assets/mobile/css/resetcss.css"/>
    <link rel="stylesheet" href="/assets/mobile/css/styles_v1.css"/>
    <link rel="stylesheet" href="/assets/mobile/css/swiper.min.css"/>
-->
<link href="/assets/mobile/build/css/libs.min.css?ver=238" rel="stylesheet" type="text/css">
        <link href="/assets/mobile/build/css/style.min.css?ver=238" rel="stylesheet" type="text/css">
    <!-- favicon -->
    <link rel="apple-touch-icon" type="text/css" href="favicon.png"/>
    <link rel="shortcut icon" type="image/x-icon" href="favicon.ico"/>


    <!--<script type="text/javascript" src="/assets/mobile/js/jquery-2.1.4.min.js"></script>-->
    <!--
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
    <script type="text/javascript" src="/assets/mobile/js/jquery.easings.min.js"></script>
    <script type="text/javascript" src="/assets/mobile/js/swiper.min.js"></script>
    <script type="text/javascript" src="/assets/mobile/js/script_v2.js"></script>
    -->
    {literal}
    
    
<style>
    .delivery_header span.activelang{
        font-weight:700;
        border-bottom: none;
            display: inline-block;
    margin: 0px 0px 0 0;
 color: #476dc7;
    text-decoration: none;
    background-image:none!important;
    }
    .main-header > ul > li.polylanglinks{
        left:70px;
        width:70px;
        
        background:none;
        
    }
    .main-header > ul > li.polylanglinks > span
    {
        display:inline;
        float:left;
        width:50%;
            padding-top: 12px;
    }


    .popup-header .btn-close{
        position: absolute;
        right: 10px;
        top: 10px;
    }
    .popup-header h2{
        height: 40px;
        padding-top: 15px;
    }
    .popupcheckout{
        top:0px;
    }
</style>
        <!-- vk widget -->
        <!--<script async type="text/javascript" src="//vk.com/js/api/openapi.js?116"></script>-->
    {/literal}




</head>
<body>
{literal}
    <!-- Google Tag Manager (noscript) -->
    <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-W5VBDZ";
                      height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
    <!-- End Google Tag Manager (noscript) -->
{/literal}
{literal}
    <script type="text/javascript">
        function setCookie(name, value, options) {
            options = options || {};
            var expires = options.expires;
            if (typeof expires == "number" && expires) {
                var d = new Date();
                d.setTime(d.getTime() + expires * 1000);
                expires = options.expires = d;
            }
            if (expires && expires.toUTCString) {
                options.expires = expires.toUTCString();
            }
            value = encodeURIComponent(value);
            var updatedCookie = name + "=" + value;
            for (var propName in options) {
                updatedCookie += "; " + propName;
                var propValue = options[propName];
                if (propValue !== true) {
                    updatedCookie += "=" + propValue;
                }
            }
            document.cookie = updatedCookie;
        }
        // возвращает cookie с именем name, если есть, если нет, то undefined
        function getCookie(name) {
            var matches = document.cookie.match(new RegExp(
                "(?:^|; )" + name.replace(/([\.$?*|{}\(\)\[\]\\\/\+^])/g, '\\$1') + "=([^;]*)"
            ));
            return matches ? decodeURIComponent(matches[1]) : undefined;
        }
    </script>
    
    <div id="fb-root"></div>
<script async defer crossorigin="anonymous" src="https://connect.facebook.net/ru_RU/sdk.js#xfbml=1&version=v9.0&appId=1450437548564324&autoLogAppEvents=1"; nonce="h7InQuzt"></script>
    
    <!-- fb widget -->
    <!--<div id="fb-root"></div>
    <script>(function (d, s, id) {
            var js, fjs = d.getElementsByTagName(s)[0];
            if (d.getElementById(id)) return;
            js = d.createElement(s);
            js.id = id;
            js.src = "//connect.facebook.net/ru_RU/sdk.js#xfbml=1&version=v2.0";
            fjs.parentNode.insertBefore(js, fjs);
        }(document, 'script', 'facebook-jssdk'));</script>-->
    <!--  end fb -->

    <script>
        !function (t, e, c, n) {
            var s = e.createElement(c);
            s.async = 1, s.src = 'https://statics.esputnik.com/scripts/' + n + '.js';
            var r = e.scripts[0];
            r.parentNode.insertBefore(s, r);
            var f = function () {
                f.c(arguments);
            };
            f.q = [];
            f.c = function () {
                f.q.push(arguments);
            };
            t['eS'] = t['eS'] || f;
        }(window, document, 'script', '9AD2834E688B486E93DCDF7D7D0B4C20');
    </script><script>eS('init');</script>
{/literal}


{snippet name=mobilepreloader}


<header class="main-header" style="z-index:10!important;">


    <ul class="main-header__menu">
        <li class="main-header__menu__burger">
        <div class="--svg__burger-before"></div>
        </li>
        <li class="main-header__menu__logo"><a href="{config name=site_url}" title="на главную" class="--svg__logo"></a></li>
        <li class="main-header__menu__search"> {literal}
            [[!mSearchForm?
            &element=`msProducts`
            &tplForm=`m_tpl.mSearch2.form`
            &fields=`Data.article:3,Data.vendor:2`
            &where=`{"Data.price:>":0}`
            ]]
        {/literal}
        </li>
        <li class="main-header__menu__cart">
        {nocache}{block name=minicart nocache}{snippet name=msMiniCart_m}{/block}{/nocache}
        </li>        
      
      {* 
        {if ($modx->resource->id!=8)}
        
            [[!Login? &tplType=`modChunk` &loginTpl=`mobilheadNotLoggedInTpl` &logoutTpl=`mobilheadLoggedInTpl`]]
        
        {/if}
        *}

    <!--  
       

        <li class="h-btn-menu"><a href="#" title="меню"></a></li>
        -->
    </ul>
    

   

</header>


{if ($modx->resource->id!=8)}
    [[!Login? &tplType=`modChunk` &loginTpl=`mobilheadNotLoggedInTpl` &logoutTpl=`mobilheadLoggedInTpl`]]
{/if}
        
<div class="wrapper-burger" id="main_menu">
        <div class="main_menu" >
            <div class="main_menu_head">
                <span><img src="/assets/mobile/build/img/logo2_mobile.svg"/></span>
                <span class="close_modal"></span>   
            </div>
            
                {if (!$modx->user->isAuthenticated())}
                <div class="main_menu_account">
                    <div class="account">
                        <span class="icon_account"></span>
                    </div>
                    
                    <div class="main_menu_auth">
                        <a class="link_login" href="javascript:void(0)">[[%polylang_site_loginform_entr]]</a>                    
                    </div>
                    <div class="main_menu_auth">
                        <a class="link_registr" href="javascript:void(0)">[[%polylang_site_registr_new]]</a>
                    </div>
                </div>
                {else}
                    [[!Login? &tplType=`modChunk` &loginTpl=`mobilheadNotLoggedInTpl` &logoutTpl=`m_mobilheadLoggedInTpl`]]
                {/if}
            
            <ul class="main_menu_items">
                <li class="catalog_item"><a href="javascript:void(0);" class="open_menu_catalog">[[%polylang_site_common_site_catalog]]</a></li>
                <li class="cart_item">{nocache}{block name=minicart nocache}{snippet name=msMiniCart_m params='cart_menu=1'}{/block}{/nocache}</li>
            </ul>
            <div class="main_menu_title">
                [[%polylang_site_lang_text]]
            </div>
            <div class="main_menu_lang">
                {snippet name=PolylangLinks params='mode=`single`'}
            </div>
            
            <div class="main_menu_title">
            [[%polylang_site_label_help]]                
            </div>
            <ul class="main-menu">                
                    <li class="delivery_item"><a href="{snippet name=PolylangMakeUrl  params='id=473'}">[[%polylang_site_common_menu_delivery]]</a></li>
                    <li class="contacts_item"><a href="{snippet name=PolylangMakeUrl  params='id=4'}">[[%polylang_site_common_menu_contact]]</a></li>                    
                    <li class="loylnost_item"><a href="{snippet name=PolylangMakeUrl  params='id=7'}">[[%polylang_site_common_menu_loylnost]]</a></li>                    
                    <li class="actions_item"><a href="{snippet name=PolylangMakeUrl  params='id=2060'}">[[%polylang_site_common_menu_action]]</a></li>
                    <li class="brands_item"><a href="/brands">[[%polylang_site_main_lable_brand]]</a></li>
            </ul>
            
            <div class="main_menu_title">
            [[%polylang_site_label_other]]
                
            </div>
            <ul class="main-menu">
                <li class="voprosvet_item"><a href="{snippet name=PolylangMakeUrl  params='id=559'}">[[%polylang_site_common_menu_voprosvet]]</a></li>
                <li class="reviews_item"><a href="{snippet name=PolylangMakeUrl  params='id=755'}">[[%polylang_site_product_review_label]]</a></li>
                <li class="blog_item"><a href="{snippet name=PolylangMakeUrl  params='id=529'}">[[%polylang_site_common_menu_blog]]</a></li>
                <li class="ratingkorm_item"><a href="https://petchoice.ua/">[[%polylang_site_common_menu_fullwww]]</a></li>
                <!--<li class="ratingkorm_item"><a href="{snippet name=PolylangMakeUrl  params='id=12430'}">[[%polylang_site_common_menu_retingkorm]]</a></li>-->
            </ul>
            <div class="main_menu_title">
                [[%polylang_site_common_social]]
            </div>
            <ul class="main_menu_social">
                <li><a class="facebook_link" href="/"><span></span></a></li>
                <li><a class="instagram_link" href="/"><span></span></a></li>
            </ul>
            
            
            {if ($modx->user->isAuthenticated())}
                <a href="/" class="button whitefon logout">[[%polylang_site_cmenu_exit]]</a>
            {/if}
        </div>
</div>

<div class="wrapper-burger" id="menu_catalog">
        <div class="main_menu main-menu-list">
        
            <div class="main_menu_head">
                <span>[[%polylang_site_common_menu_catalog]]</span>
                <span class="close_modal"></span>   
            </div>
            {assign var=params value=[
            "startId"       => 10
            ,"level"        => 2
            ,"sortBy"       => "menuindex"
            ,"levelClass"   => "level"
            ,"cacheable"    =>false
            ,"id"           => "catalog"
            ]}
            {processor action="site/web/menu/getcatalogmenu" ns="modxsite" params=$params assign=result}
            {assign var=items value=$result.object}
            {include file='tpl/m/menu/menumain.tpl'}

        </div>
        
</div>


<div class="body_container" id="main-container">

   
        {block name=content}{/block}


    
    
    {snippet name=m_getmessage}
    <footer class="main-footer">
        <ul class="contacts" id="phones-anchor">
            <li>
            [[%polylang_site_common_orderphone]]                
            </li>
            <li class="main-phone">
                <a href="tel:+380487005080">048 700-50-80</a>
            </li>
            <li>
                [[%polylang_site_common_otherphone]]
            </li>
            <li class="other-phones">
                <div class="icons-phones">
                    <a href=""><span class="icon_viber"></span></a>
                    <a href=""><span class="icon_telegram"></span></a>
                </div>
                <ul>
                    <li><a href="tel:+380635055003">063 505-50-03</a></li>
                    <li><a href="tel:+380678416464">067 841-64-64</a></li>
                    <li><a href="tel:+380955056595">095 505-65-95</a></li>
                </ul>
                
            </li>
            <li class="work-time">
                <div>
                    <span>[[%polylang_site_common_timespn]]: 09:00 - 18:00</span>
            		<span>[[%polylang_site_common_timessub]]: 09:00 - 16:00</span>
                </div>
            </li>
        </ul>

        <div class="title_social">
            [[%polylang_site_we_are_social]]
        </div>
        <ul class="btns-social">
            <li class="soc-icn-fb"><a href="//www.facebook.com/petchoice.com.ua"></a></li>
            <li class="soc-icn-in"><a href="https://www.instagram.com/petchoice_ua/"></a></li>
        </ul>
        <nav class="copyright">
            &copy; 2016 [[%polylang_site_common_copyright]]
        </nav>
    </footer>


   

</div> <!-- #main-container -->
<!--<script src="/assets/scripts/m/m_vendor.js"></script>
<script src="/assets/scripts/m/scripts.js"></script>-->
<script src="/assets/scripts_m/m_vendor_v2.js?ver=77"></script>
<script src="/assets/scripts_m/scripts.v2.js?ver=77"></script>
<!--<script src="/assets/scripts/vendor.js"></script>
    <script src="/assets/scripts/m/scripts.js"></script>
    <script src="/assets/components/msearch/js/mfilter.js" type="text/javascript"></script>-->

<script type="text/javascript" src="/assets/scripts/img_fit.js?ver=77"></script>

<div class="windows-popup-container forgotpassword">
    <div id="forgotpassword" class="window-popup"><span style="width:30px;" class="btn-close"></span>
        <header class="popup-header" style="padding-right:30px;">
            <h2>[[%polylang_site_common_resetpassword]]</h2>
        </header>
        <div id="office-auth-form" class="modal-content">
            {snippet name=officeAuth params="tplLogin=`m_forgotPasswordFormTpl`&HybridAuth=`0`"}
        </div>

    </div>
</div>
<script type="text/javascript" src="/assets/mobile/build/js/libs.min.js?ver=77"></script>
<script type="text/javascript" src="/assets/mobile/build/js/main.min.js?ver=80"></script>
<!--
<script type="text/javascript" src="/assets/components/minishop2/js/web/m/default.js"></script>
<script type="text/javascript" src="/assets/components/msearch2/js/web/m/default.js"></script>
-->

</body>
</html>

