const {
    src,
    dest
} = require('gulp');
const changed = require('gulp-changed');
//const imagemin = import('imagemin');//import('gulp-imagemin');
//const imagemin = import('gulp-imagemin');
const imagemin = import('gulp-imagemin');
const imageminGifsicle = import('imagemin-gifsicle');

const imageminoptipng = import('imagemin-optipng');//imagemin.optipng(),
const imageminsvgo = import('imagemin-svgo');
    //const imagemin.svgo()


const recompress = require('imagemin-jpeg-recompress');
const pngquant = require('imagemin-pngquant');
const bs = require('browser-sync');
//const imageminGifsicle = require('imagemin-gifsicle');
module.exports = function rastr() {
    return src('assets/mobile/src/img/**/*.+(png|jpg|jpeg|gif|svg|ico)')
        .pipe(changed('assets/mobile/build/img'))
        /*.pipe(imagemin({
                interlaced: true,
                progressive: true,
                optimizationLevel: 5,
            },
            [
                recompress({
                    loops: 6,
                    min: 50,
                    max: 90,
                    quality: 'high',
                    use: [pngquant({
                        quality: [0.8, 1],
                        strip: true,
                        speed: 1
                    })],
                }),
                imageminGifsicle,//imagemin.gifsicle(),
                imageminoptipng,//imagemin.optipng(),
                imageminsvgo//imagemin.svgo()
            ], ), )*/
        .pipe(dest('assets/mobile/build/img'))
        .pipe(bs.stream())
}