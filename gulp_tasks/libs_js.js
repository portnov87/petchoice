const plugins = [
    //'node_modules/jquery/dist/jquery.js',
    'node_modules/swiper/swiper-bundle.js',
    'node_modules/bootstrap/dist/js/bootstrap.js',
    'node_modules/jquery-autocomplete/jquery.autocomplete.js'
];
const {
    src,
    dest
} = require('gulp');
const uglify = require('gulp-uglify-es').default;
const concat = require('gulp-concat');
const map = require('gulp-sourcemaps');
//const chalk = require('chalk');

module.exports = function libs_js(done) {
    if (plugins.length > 0)
        return src(plugins)
            .pipe(map.init())
            .pipe(uglify())
            .pipe(concat('libs.min.js'))
            .pipe(map.write('../sourcemaps'))
            .pipe(dest('assets/mobile/build/js/'))
    else {
        return done(console.log('No added JS plugins'));//chalk.redBright(
    }
}