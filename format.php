<?php
if (!function_exists('sorthits')) {
    function sorthits($a, $b)
    {
        return strnatcmp($b["hits"], $a["hits"]);
    }
}
$output_filters = [];
$prosmotri = 0;
/** @var array $scriptProperties */
/** @var pdoFetch $pdoFetch */
$fqn = $modx->getOption('pdoFetch.class', null, 'pdotools.pdofetch', true);
if (!$pdoClass = $modx->loadClass($fqn, '', false, true)) {
    return false;
}
$pdoFetch = new $pdoClass($modx, $scriptProperties);
$pdoFetch->addTime('pdoTools loaded.');
/** @var mSearch2 $mSearch2 */
if (!$modx->loadClass('msearch2', MODX_CORE_PATH . 'components/msearch2/model/msearch2/', false, true)) {
    return false;
}
$mSearch2 = new mSearch2($modx, $scriptProperties, $pdoFetch);
$mSearch2->initialize($modx->context->key);
$savedProperties = array();

if (empty($queryVar)) {
    $queryVar = 'query';
}
if (empty($parentsVar)) {
    $parentsVar = 'parents';
}
if (empty($minQuery)) {
    $minQuery = $modx->getOption('index_min_words_length', null, 3, true);
}
if (empty($classActive)) {
    $classActive = 'active';
}
if (isset($scriptProperties['disableSuggestions'])) {
    $scriptProperties['suggestions'] = empty($scriptProperties['disableSuggestions']);
}
if (empty($toPlaceholders) && !empty($toPlaceholder)) {
    $toPlaceholders = $toPlaceholder;
}
if (empty($plPrefix)) {
    $plPrefix = 'mse2_';
}
if (isset($_REQUEST['limit']) && is_numeric($_REQUEST['limit']) && abs($_REQUEST['limit']) > 0) {
    $limit = abs($_REQUEST['limit']);
} elseif ($limit == '') {
    $limit = 10;
}
if (!isset($outputSeparator)) {
    $outputSeparator = "\n";
}
$fastMode = !empty($fastMode);
// All templates of filters are converted to lowercase
foreach ($scriptProperties as $k => $v) {
    if (strpos($k, 'tplFilter') === 0) {
        $tmp = 'tplFilter.' . strtolower(substr($k, 10));
        if ($tmp != $k) {
            unset($scriptProperties[$k]);
            $scriptProperties[$tmp] = $v;
        }
    }
}

if (isset($scriptProperties['raiting']))
    $raiting_page = $scriptProperties['raiting'];
else $raiting_page = false;

//ms|vendor
$class = 'modResource';
$output = array('filters' => '', 'results' => '', 'total' => 0, 'limit' => $limit);
$ids = $found = $log = $where = array();

// ---------------------- Retrieving ids of resources for filter
$query = !empty($_REQUEST[$queryVar])
    ? htmlspecialchars(strip_tags(trim($_REQUEST[$queryVar])))
    : '';

// Filter by ids
if (!empty($resources)) {
    /* echo '<pre>	$ids';
 print_r($resources);
 echo '</pre>';*/
    $ids = array_map('trim', explode(',', $resources));
} elseif (isset($_REQUEST[$queryVar]) && empty($query)) {
    $output['results'] = $modx->lexicon('mse2_err_no_query');
} elseif (empty($query) && !empty($forceSearch)) {
    $output['results'] = $modx->lexicon('mse2_err_no_query_var');
} elseif (isset($_REQUEST[$queryVar]) && !preg_match('/^[0-9]{2,}$/', $query) && mb_strlen($query, 'UTF-8') < $minQuery) {
    $output['results'] = $modx->lexicon('mse2_err_min_query');
} elseif (isset($_REQUEST[$queryVar])) {
    //echo $plPrefix.$queryVar.' dddd '.$query;
    $modx->setPlaceholder($plPrefix . $queryVar, $query);

    $found = $mSearch2->Search($query);
    /*	echo '<pre>ddd';
        print_r($found);
        echo '</pre>';*/
    //echo '$query'.$query;
    $ids = array_keys($found);
    /**/
    if (!empty($ids)) {
        $tmp = $scriptProperties;
        $tmp['returnIds'] = 1;
        $tmp['resources'] = implode(',', $ids);
        $tmp['parents'] = (int)$scriptProperties['parents'];
        $tmp['limit'] = 0;

        $ids = explode(',', $modx->runSnippet($scriptProperties['element'], $tmp));
        $pdoFetch->addTime('Found ids: "' . implode(',', $ids) . '"');
    }

    if (empty($ids[0])) {

        $output['results'] = 'Извините, по Вашему запросу ничего не найдено';//$modx->lexicon('mse2_err_no_results');
    }
}


//echo $plPrefix.$queryVar;
$modx->setPlaceholder($plPrefix . $queryVar, $query);


//echo $query;
// Has error message - exit
if (!empty($output['results'])) {
    $log = '';
    if ($modx->user->hasSessionContext('mgr') && !empty($showLog)) {
        $log = '<pre class="mFilterLog">' . print_r($pdoFetch->getTime(), 1) . '</pre>';
    }
    if (!empty($toSeparatePlaceholders)) {
        $output['log'] = $log;
        $modx->setPlaceholders($output, $toSeparatePlaceholders);
        return;
    } elseif (!empty($toPlaceholders)) {
        $output['log'] = $log;
        $modx->setPlaceholders($output, $toPlaceholders);
        return;
    } else {
        $output = '<div style="text-align:center;padding:20px 0px">' . $output['results'] . '</div>';// $pdoFetch->getChunk($scriptProperties['tplOuter'], $output, $fastMode);
        //$output = $pdoFetch->getChunk($scriptProperties['tplOuter'], $output, $fastMode);
        //$output .= $log;
        return $output;
    }
}

// ---------------------- Checking resources by status and custom "where" parameter
// Support for specifying property set in the element name
$elementName = $scriptProperties['element'];
$elementSet = array();
if (strpos($elementName, '@') !== false) {
    list($elementName, $elementSet) = explode('@', $elementName);
}
/** @var modSnippet $snippet */

if (!empty($elementName) && $element = $modx->getObject('modSnippet', array('name' => $elementName))) {
    $elementProperties = $element->getProperties();
    $elementPropertySet = !empty($elementSet)
        ? $element->getPropertySet($elementSet)
        : array();
    if (!is_array($elementPropertySet)) {
        $elementPropertySet = array();
    }
    $params = array_merge(
        $elementProperties,
        $elementPropertySet,
        $scriptProperties,
        array(
            'parents' => empty($scriptProperties[$parentsVar]) && !empty($_REQUEST[$parentsVar])
                ? $_REQUEST[$parentsVar]
                : $scriptProperties[$parentsVar],
            'returnIds' => 1,
            'limit' => 0,
        )
    );

    if (!empty($ids)) {
        /*echo '<pre>ids';
        print_r($ids);
        echo '</pre>';*/
        $params['resources'] = implode(',', $ids);
    }

    $params['raiting_page'] = $raiting_page;
    $element->setCacheable(false);
    $tmp = $element->process($params);
    /*if ($_SERVER['REMOTE_ADDR']=='188.130.177.18'){
    echo '<pre>tmp';
    print_r(	$params);
    echo '</pre>';
    }*/
    if (!empty($tmp)) {
        $tmp = explode(',', $tmp);
        $ids = !empty($ids)
            ? array_intersect($ids, $tmp)
            : $tmp;
    } /*echo '<pre>	$ids';
    print_r($ids);
    echo '</pre>';*/
    $pdoFetch->addTime('Fetched ids for building filters: "' . implode(',', $ids) . '" from element "' . $elementName . '"');
} else {
    $modx->log(modX::LOG_LEVEL_ERROR, '[mSearch2] Could not find main snippet with name: "' . $elementName . '"');
    return '';
}


$alias = $modx->context->getOption('request_param_alias', 'q');


$request = $_REQUEST[$alias];
$tmpalias = explode('/', $request);

$tmpaliasar = array_filter($tmpalias);
/*
echo '<pre>'.$elementName;
print_r($tmpaliasar);
echo  '</pre>';*/
$showcategory = false;
$showfilter = true;
$showcat1 = false;
$catalias = false;
if ((count($tmpaliasar) <= 1) && (!in_array('rejting-kormov', $tmpaliasar)))// значит категория первая и надо вывести группы а не фильтры
{
    $showcategory = true;
    $showfilter = false;

    if (count($tmpaliasar) == 1) {
        $catalias = $tmpaliasar[0];
        $showcat1 = true;
    }
}
if (isset($_REQUEST['query'])) {
    //echo 'eeee'.$_REQUEST[$alias];
    $showcategory = true;
    $showfilter = false;
}

// ---------------------- Nothing to filter, exit
if (empty($ids)) {

    if ($modx->user->hasSessionContext('mgr') && !empty($showLog)) {
        $log = '<pre class="mFilterLog">' . print_r($pdoFetch->getTime(), 1) . '</pre>';
    } else {
        $log = '';
    }
    $res = $modx->lexicon('mse2_err_no_results');

    $output = array_merge($output, array(
        'filters' => ''//$modx->lexicon('mse2_err_no_filters')
    , 'results' => $res
    , 'filtershow' => 0
    , 'log' => $log
    ));


    if (!empty($toSeparatePlaceholders)) {
        $modx->setPlaceholders($output, $toSeparatePlaceholders);
        return;
    } elseif (!empty($toPlaceholders)) {
        $modx->setPlaceholders($output, $toPlaceholders);
        return;
    } else {
        $output['results'] .= $log;
        //echo $scriptProperties['tplOuter'];
        return $pdoFetch->getChunk($scriptProperties['tplOuter'], $output, $fastMode);
    }
}
//echo 'rrrr';
// ---------------------- Checking for suggestions processing
// Checking by results count
if (!empty($scriptProperties['suggestionsMaxResults']) && count($ids) > $scriptProperties['suggestionsMaxResults']) {
    $scriptProperties['suggestions'] = false;
    $pdoFetch->addTime('Suggestions disabled by "suggestionsMaxResults" parameter: results count is ' . count($ids) . ', max allowed is ' . $scriptProperties['suggestionsMaxResults']);
} else {
    $pdoFetch->addTime('Total number of results: ' . count($ids));
}

// Then get filters
$pdoFetch->addTime('Getting filters for ids: "' . implode(',', $ids) . '"');
$filters = '';
$count = 0;


$filtercount = array();//
//$filtercount=$mSearch2->getSuggestions($ids, $_REQUEST, $ids);

//echo $modx->resource->id;
$rangeprice = $mSearch2->getRangePrice($modx->resource->id);
if ($_SERVER['REMOTE_ADDR'] == '188.130.177.18') {
    /* echo '<pre>$rangeprice';
 print_R($rangeprice);
 echo '</pre>';*/
}

if (!empty($ids)) {
    $filters = $mSearch2->getFilters($ids, true);
    /**/
    $mayfiltereq = false;
    foreach (array_keys($_GET) as $kf => $vf) {
        //echo $vf.'</br>';
        if (isset($filters['ms|' . $vf])) $mayfiltereq = true;
        if (isset($filters['attributes|' . $vf])) {
            //$_REQUEST[$vf]=$filters['attributes|'.$vf];
            $_GET['attributes|' . $vf] = $_REQUEST['attributes|' . $vf] = $_REQUEST[$vf];
        }
        /*echo '<pre>get';
        print_r($filters['attributes|'.$vf]);
        echo '</pre>';*/
    }
    /*echo '<pre>get';
            print_r($_REQUEST);
            echo '</pre>';
*/
    // And checking by filters count
    if (!empty($filters) && $scriptProperties['suggestions']) {
        foreach ($filters as $tmp) {
            if (!is_array($tmp)) {
                continue;
            }
            $count += count(array_values($tmp));
        }
        if (!empty($scriptProperties['suggestionsMaxFilters']) && $count > $scriptProperties['suggestionsMaxFilters']) {
            $scriptProperties['suggestions'] = false;
            $pdoFetch->addTime('Suggestions disabled by "suggestionsMaxFilters" parameter: filters count is ' . $count . ', max allowed is ' . $scriptProperties['suggestionsMaxFilters']);
        } else {
            $pdoFetch->addTime('Total number of filters: ' . $count);
        }
    }
}


$modx->setPlaceholder($plPrefix . 'filters_count', $count);


// ---------------------- Loading results
$start_sort = implode(',', array_map('trim', explode(',', $scriptProperties['sort'])));
/*
echo '<pre>';
print_r($scriptProperties);
echo '</pre>';*/
//$tmp['sortby'] = 'hits ASC';
$start_limit = !empty($scriptProperties['limit']) ? $scriptProperties['limit'] : 0;
$suggestions = array();
$page = $sort = '';

$scriptProperties['sortby'] = 'hits';
$scriptProperties['sortdir'] = 'desc';
// Support for specifying property set in the paginator name
$paginatorName = $scriptProperties['paginator'];
$paginatorSet = array();
if (strpos($paginatorName, '@') !== false) {
    list($paginatorName, $paginatorSet) = explode('@', $paginatorName);
}

if (!empty($ids)) {
    /* @var modSnippet $paginator */
    if ($paginator = $modx->getObject('modSnippet', array('name' => $paginatorName))) {
        $paginatorProperties = $paginator->getProperties();
        $paginatorPropertySet = !empty($paginatorSet)
            ? $paginator->getPropertySet($paginatorSet)
            : array();
        if (!is_array($paginatorPropertySet)) {
            $paginatorPropertySet = array();
        }
        $paginatorProperties = array_merge(
            $paginatorProperties
            , $paginatorPropertySet
            , $elementPropertySet
            , $scriptProperties
            , array(
                'resources' => implode(',', $ids)
            , 'parents' => '0'
            , 'element' => $elementName
            , 'defaultSort' => $start_sort
            , 'toPlaceholder' => false
            , 'limit' => $limit
            )
        );


        // Switching chunk for rows, if specified
        if (!empty($scriptProperties['tpls'])) {
            $tmp = isset($_REQUEST['tpl']) ? (integer)$_REQUEST['tpl'] : 0;
            $tpls = array_map('trim', explode(',', $scriptProperties['tpls']));
            $paginatorProperties['tpls'] = $tpls;
            if (isset($tpls[$tmp])) {
                $paginatorProperties['tpl'] = $tpls[$tmp];
                $paginatorProperties['tpl_idx'] = $tmp;
            }
        }

        // Trying to save weight of found ids if using mSearch2
        $weight = false;
        if (!empty($found) && strtolower($paginatorName) == 'msearch2') {
            $tmp = array();
            foreach ($ids as $v) {
                $tmp[$v] = isset($found[$v])
                    ? $found[$v]
                    : 0;
            }
            $paginatorProperties['resources'] = $modx->toJSON($tmp);
            $weight = true;
        }

        if (!empty($_REQUEST['sort'])) {
            $sort = $_REQUEST['sort'];
        } elseif (!empty($start_sort)) {
            $sort = $start_sort;
        } else {

            $sortby = !empty($scriptProperties['sortby']) ? $scriptProperties['sortby'] : '';
            if (!empty($sortby)) {
                $sortdir = !empty($scriptProperties['sortdir']) ? $scriptProperties['sortdir'] : 'desc';
                $sort = $sortby;//.$mSearch2->config['method_delimeter'].$sortdir;
            }

        }

        if (!empty($_REQUEST[$paginatorProperties['pageVarKey']])) {
            $page = (int)$_REQUEST[$paginatorProperties['pageVarKey']];
        }
        if (!empty($sort)) {
            $paginatorProperties['sortby'] = $mSearch2->getSortFields($sort, 'DESC');
            $paginatorProperties['sortdir'] = '';
        }

        $paginatorProperties['start_limit'] = $start_limit;
        $savedProperties['paginatorProperties'] = $paginatorProperties;

        // We have a delimeters in $_GET, so need to filter resources


        //if (strpos(implode(array_keys($_GET)), $mSearch2->config['filter_delimeter']) !== false) {
        //	if ($mayfiltereq){

        if (isset($_REQUEST['vendor'])) $_REQUEST['ms|vendor'] = $_REQUEST['vendor'];
        $matched = $mSearch2->Filter($ids, $_REQUEST);

        /*if ($_SERVER['HTTP_CF_CONNECTING_IP']=='78.46.62.217'){
         echo '<pre>dddd';//.$elementName.' '.count($ids);
                print_r($matched);//$order);
                echo '</pre>';
        }*/

        /*
if ($_SERVER['REMOTE_ADDR']=='188.130.177.18'){
 //echo $elementName.' '.count($ids);
                echo '<pre>dddd';//.$elementName.' '.count($ids);
                print_r($matched);//$order);
                echo '</pre>';

                echo '<pre>dddd';
                print_r($ids);
                echo '</pre>';

            }*/
        /*	echo '<pre>request';
            print_r($_REQUEST);
            echo '</pre>';
            echo '<pre>$matched';
            print_r($matched);
            echo '</pre>';*/
        /*
if ($_SERVER['REMOTE_ADDR']=='188.130.177.18'){

echo '<pre>';
print_r(	$matched);
echo '</pre>';
}*/
        $matched = array_intersect($ids, $matched);
        if ($scriptProperties['suggestions']) {
            $suggestions = $mSearch2->getSuggestions($ids, $_REQUEST, $matched);
            $pdoFetch->addTime('Suggestions retrieved.');
        } else $suggestions = $mSearch2->getSuggestions($ids, $_REQUEST, $matched);
        /*	echo '<pre>$suggestions';
            print_r($suggestions);
            echo '</pre>';*/

        // Trying to save weight of found ids again
        if ($weight) {
            $tmp = array();
            foreach ($matched as $v) {
                $tmp[$v] = isset($found[$v]) ? $found[$v] : 0;
            }
            $paginatorProperties['resources'] = $modx->toJSON($tmp);
        } else {
            $paginatorProperties['resources'] = implode(',', $matched);
        }


        //}
        // Saving log
        $log = $pdoFetch->timings;
        $pdoFetch->timings = array();

        //$paginator->setProperties($paginatorProperties);
        $paginator->setCacheable(false);
        $output['results'] = !empty($paginatorProperties['resources'])
            ? $paginator->process($paginatorProperties)
            : $modx->lexicon('mse2_err_no_results');
        $output['total'] = $modx->getPlaceholder($paginatorProperties['totalVar']);

        /*
        echo '<pre>results';
print_r($paginatorProperties);
echo '</pre>';
        echo '<pre>results';
print_r($paginator->process($paginatorProperties));
echo '</pre>';
        */
    } else {
        $modx->log(modX::LOG_LEVEL_ERROR, '[mSearch2] Could not find pagination snippet with name: "' . $paginatorName . '"');
        return '';
    }
} else {

}
/*
echo '<pre>paginator';
print_r($paginatorProperties);
echo '</pre>';*/

$showseotext = 0;
//echo 'page'.$page;
if ($page == '') {
    $showseotext = 1;
    $ostatok = $output['total'] - $limit;
    if ($ostatok > 24) $limit2 = 24;
    else $limit2 = $ostatok;
} elseif ($page > 1) {
    $showseotext = 0;
    $ostatok = $output['total'] - ($page * $limit);
    if ($ostatok > 24) $limit2 = 24;
    else $limit2 = $ostatok;

} else {
    if ($output['total'] >= 24)
        $limit2 = 24;
    else $limit2 = 0;//$output['total'];
}
if ($limit2 < 0) $output['limit2'] = 0;
else $output['limit2'] = $limit2;


//echo $ostatok.' '.$output['total'].' '.$page.' '.$limit;

//echo $output['limit'].' '.$output['total'].' $page '.$page.' '.$limit.' '.$start_limit;
// ----------------------  Loading filters
$pdoFetch->timings = $log;
if (!empty($paginator)) {
    $pdoFetch->addTime('Fired paginator: "' . $paginatorName . '"');
} else {
    $pdoFetch->addTime('Could not find pagination snippet with name: "' . $paginatorName . '"');
}
if (empty($filters)) {
    $pdoFetch->addTime('No filters retrieved');
    $output['filters'] = $modx->lexicon('mse2_err_no_filters');
    if (empty($output['results'])) {
        $output['results'] = $modx->lexicon('mse2_err_no_results');
    }
} else {
    $pdoFetch->addTime('Filters retrieved');
    $request = array();
    foreach ($_GET as $k => $v) {
        $request[$k] = explode($mSearch2->config['values_delimeter'], $v);
    }

    if (isset($_REQUEST['urlbrand']) && ($_REQUEST['urlbrand'] != '')) $pageurl = str_replace('/' . $_REQUEST['urlbrand'], '', $_GET['q']);
    else $pageurl = $_GET['q'];

    $newnames = array();


    /*if ($_SERVER['REMOTE_ADDR']=='188.130.177.18'){
            echo '<pre>sdsdsd';
            print_r($filters);//array($requested, $values, $ids));//array($requested, $values, $ids));
            echo '</pre>';
            }*/


    foreach ($filters as $key => $f) {
        if ($key == 'ms|adiscount') {
            unset($filters[$key][0]);
            $filters[$key][1]['title'] = 'Акции';
        }
        if (is_array($f)) {
            foreach ($f as $key1 => $f1) {
                $newnames[$key] = $f1['caption'];
                if ($key == 'ms|adiscount') $newnames[$key] = 'Акции';
                break;
            }
        }

    }


    if ($showcategory == true) {

        $tplFilter = 'filterCategoryTpl';
        $tplGroup = 'filterCategoryGroupRowTpl';
        $tplRow = 'filterCategoryRowTpl';

        if ($catalias) {
            $aliascategory = $catalias;
        }

        //echo $catalias;
        if (isset($_REQUEST[$alias])) {
            // echo $ids;
            $q = $modx->newQuery('modResource', array('id:IN' => $ids, 'published' => 1));//
            $q->select('id,parent,pagetitle,context_key,menuindex, properties, uri, alias');//,context_key,menuindex, properties, uri
            $q->groupby('parent');
            $parentsid = array();

            $q->sortby('menuindex', 'DESC');
            $parents = [];
            if ($q->prepare() && $q->stmt->execute()) {
                $group = '';
                while ($row = $q->stmt->fetch(PDO::FETCH_ASSOC)) {

                    if ($catalias) {

                        $urlpar = $catalias;

                        $res = $modx->getObject('modResource', $row['parent']);


                        $res1 = $modx->getObject('modResource', $res->get('parent'));

                        if ($res1) {
                            // echo 'parent'.$res->get('parent');
                            if ($res1->get('alias') == $catalias) {
                                $tv1 = $res->getOne('TemplateVarResources', array(
                                    'tmplvarid' => 5,
                                    'contentid' => $res->get('id'),
                                ));
                                if ($tv1) $group = $tv1->get('value');
                                $urlpar = $row['parent'];


                                /*  echo '<pre>parents';
                                  print_r($parents);
                                  echo '</pre>';
                                   echo '<pre>group';
                                  print_r($group);
                                  echo '</pre>';*/
                                $parents[$urlpar]['group'] = $group;
                                $parents[$urlpar]['group1'] = $res1->get('pagetitle');//$group;

                                $parents[$urlpar]['alias'] = $res1->get('alias');//$group;
                                $parents[$urlpar]['id'] = $res->get('id');
                                $parents[$urlpar]['parent'] = $res->get('parent');
                                $parents[$urlpar]['pagetitle'] = $res->get('pagetitle');
                                $parents[$urlpar]['context_key'] = $res->get('context_key');
                                $parents[$urlpar]['menuindex'] = $res->get('menuindex');
                                $parents[$urlpar]['properties'] = $res->get('properties');

                                $uri = $res->get('uri');
                                if ($uri[strlen($uri) - 1] == '/') {
                                    $uri = substr($uri, 0, -1);
                                }
                                if ($query != '') $parents[$urlpar]['uri'] = $uri . '?query=' . $query;//$_REQUEST[$alias];
                                else $parents[$urlpar]['uri'] = $uri;
                            }
                        }
                    } else {

                        $res = $modx->getObject('modResource', $row['parent']);


                        $urlpar = $row['parent'];


                        $res1 = $modx->getObject('modResource', $res->get('parent'));//$row['parent']);//
                        if ($res1->get('alias') == $catalias) {
                            $tv1 = $res->getOne('TemplateVarResources', array(
                                'tmplvarid' => 5,
                                'contentid' => $res->get('id'),
                            ));
                            if ($tv1) $group = $tv1->get('value');
                        }

                        $parents[$urlpar]['group'] = $res1->get('pagetitle');//$group;
                        $parents[$urlpar]['group1'] = $group;//$group;

                        $parents[$urlpar]['alias'] = $res1->get('alias');//$group;
                        $parents[$urlpar]['id'] = $res->get('id');
                        $parents[$urlpar]['parent'] = $res->get('parent');
                        $parents[$urlpar]['pagetitle'] = $res->get('pagetitle');
                        $parents[$urlpar]['context_key'] = $res->get('context_key');
                        $parents[$urlpar]['menuindex'] = $res->get('menuindex');
                        $parents[$urlpar]['properties'] = $res->get('properties');

                        $uri = $res->get('uri');
                        if ($uri[strlen($uri) - 1] == '/') {
                            $uri = substr($uri, 0, -1);
                        }
                        //else echo $uri.' '.$uri[strlen($uri)].'<br/>';
                        //strpos($uri)
                        $parents[$row['parent']]['uri'] = $uri;
                        if ($query != '') $parents[$row['parent']]['uri'] .= '?query=' . $query;//$_REQUEST[$alias];

                    }
                }


            }

        } else {
            $q = $modx->newQuery('modResource', array('parent' => $modx->resource->id, 'published' => 1));//
            $q->select('id,parent,pagetitle,context_key,menuindex, properties, uri');//,context_key,menuindex, properties, uri
            //$q->order('menuindex');
            $parentsid = array();

            $q->sortby('menuindex', 'ASC');

            if ($q->prepare() && $q->stmt->execute()) {
                $group = '';
                while ($row = $q->stmt->fetch(PDO::FETCH_ASSOC)) {
                    $parents[$row['id']] = $row;
                    $res = $modx->getObject('modResource', $row['id']);
                    $tv = $res->getOne('TemplateVarResources', array(
                        'tmplvarid' => 5,
                        'contentid' => $res->get('id'),
                    ));
                    if ($tv) $group = $tv->get('value');
                    $parents[$row['id']]['group'] = $group;

                }
            }
        }
        /*
if ($_SERVER['HTTP_X_REAL_IP']=='188.130.177.18')
{
echo 'eeee';die();

}   */

        $pricesfilter = array();
        $pargr = array();
        foreach ($parents as $par) $pargr[$par['group']][] = $par;

        foreach ($pargr as $key => $par) {
            $rows = '';
            $totalpar = 0;
            foreach ($par as $pa) {

                $total = 0;//modResource

                $ids = array();
                $q2 = $modx->newQuery('msProduct');
                $q2->where(array('class_key' => 'msProduct', 'parent' => $pa['id'], 'published' => 1, 'deleted' => 0));
                $q2->select('`msProduct`.`id`');
                if ($q2->prepare() && $q2->stmt->execute()) {
                    $ids = $q2->stmt->fetchAll(PDO::FETCH_COLUMN);
                }

                $q2 = $modx->newQuery('msProduct');
                $q2->leftJoin('msCategoryMember', 'Member', '`Member`.`product_id` = `msProduct`.`id`');
                $q2->where(array('class_key' => 'msProduct', 'Member.category_id' => $pa['id'], 'published' => 1, 'deleted' => 0));
                $q2->select('`msProduct`.`id`');
                if ($q2->prepare() && $q2->stmt->execute()) {
                    $ids2 = $q2->stmt->fetchAll(PDO::FETCH_COLUMN);
                    if (!empty($ids2)) {
                        $ids = array_unique(array_merge($ids, $ids2));
                    }
                }


                $parentsid = array();

                $total = count($ids);

                if ($total > 0) {
                    $rows .= $pdoFetch->getChunk($tplRow, array(
                        'group' => $key,
                        'subgroup' => $pa['group']//]$v2['group']
                    , 'title' => $pa['pagetitle']
                    , 'value' => $pa['pagetitle']
                    , 'url' => $pa['uri']
                    , 'idx' => $idx++
                    , 'num' => $total
                    ), $fastMode);

                    $totalpar = $totalpar + $total;
                }
            }

            if ($totalpar > 0) {


                $modx->smarty->assign('rows', $rows);
                /* $modx->smarty->assign('subgroup', $key);
                 $modx->smarty->assign('idx', $idx);
                 $modx->smarty->assign('num', $idx);

                 $rogr=$modx->smarty->fetch('tpl/filter/filterCategoryGroupRow.tpl');
                 $rogr;*/

                $rowssubgroup .= $pdoFetch->getChunk($tplGroup, array(
                    'subgroup' => $key,
                    'rows' => $rows
                , 'idx' => $idx++
                , 'num' => ''//$num
                ), $fastMode);


            }

        }

        //echo '$tplFilter'.$tplFilter.'<br/>';
        $outputar .= $pdoFetch->getChunk($tplFilter, array(
            'group' => ''
        , 'rowssubgroup' => $rowssubgroup
        ), $fastMode);


        $output_filters[$filter] = $outputar;


    }


    if ($showfilter) {


        foreach ($filters as $filter => $data) {

            if (empty($data) || !is_array($data)) {
                continue;
            }
            $tplOuter = !empty($scriptProperties['tplFilter.outer.' . $filter]) ? $scriptProperties['tplFilter.outer.' . $filter] : $scriptProperties['tplFilter.outer.default'];
            $tplRow = !empty($scriptProperties['tplFilter.row.' . $filter]) ? $scriptProperties['tplFilter.row.' . $filter] : $scriptProperties['tplFilter.row.default'];
            $tplEmpty = !empty($scriptProperties['tplFilter.empty.' . $filter]) ? $scriptProperties['tplFilter.empty.' . $filter] : '';

            // Caching chunk for quick placeholders
            $pdoFetch->getChunk($tplRow);

            $rows = $has_active = '';
            list($table, $filter2) = explode($mSearch2->config['filter_delimeter'], $filter);
            //echo 'table '.$table.' '.$filter2.'<br/>';
            $idx = 0;
            $co = 0;


            if ($filter2 == 'vendor') {
                $rowsarray = array();

                foreach ($data as $v) {
                    $idspr = implode(',', $v['resources']);
                    $q = $modx->newQuery('msProductData');
                    $q->where(array('id:IN' => $v['resources']));
                    $q->select('id,hits');
                    $rowspr = array();
                    $prosmotri = 0;
                    if ($q->prepare() && $q->stmt->execute()) {
                        while ($row1 = $q->stmt->fetch(PDO::FETCH_ASSOC)) {
                            foreach ($row1 as $k1 => $v1) {
                                if ($k1 == 'id') {
                                    $rowspr[$v1] = $row1['hits'];
                                    $prosmotri = $prosmotri + $row1['hits'];
                                }
                            }
                        }
                    }

                    if (empty($v)) {
                        continue;
                    }
                    $filter3 = explode('|', $filter);
                    $filter4 = $filter3[1];
                    //$checked = isset($request[$filter]) && in_array($v['value'], $request[$filter]) && isset($v['type']) && $v['type'] != 'number';
                    $checked = isset($request[$filter4]) && in_array($v['value'], $request[$filter4]) && isset($v['type']) && $v['type'] != 'number';


                    //	if ($scriptProperties['suggestions']) {
                    if ($checked) {
                        $num = '';
                        $has_active = 'has_active';
                    } else if (isset($suggestions[$filter4][$v['value']])) {
                        $num = $suggestions[$filter4][$v['value']];
                    } else {
                        $num = !empty($v['resources']) ? count($v['resources']) : '';
                    }
                    //	} else {$num = '';}


                    if ((isset($_REQUEST['attribute'])) || ((isset($_REQUEST['vendor'])) && ($_REQUEST['vendor'] != ''))) {

                        $pageurlstr = '';//$v['url'];//.'/';
                        $ur = '';
                    } else {
                        $pageurlstr = $pageurl;
                        $pageurlstr .= '/' . $v['url'];//.'/';
                        $ur = $v['url'];//.'/';
                    }

                    //echo '$tplRow2222'.$tplRow.'<br/>';

                    $modx->smarty->assign('filter', $filter2);
                    $modx->smarty->assign('hits', $prosmotri);
                    $modx->smarty->assign('table', $table);
                    $modx->smarty->assign('title', $v['title']);
                    $modx->smarty->assign('value', $v['value']);
                    $modx->smarty->assign('type', $v['type']);
                    $modx->smarty->assign('checked', $checked ? 'checked' : '');
                    $modx->smarty->assign('selected', $checked ? 'selected' : '');
                    $modx->smarty->assign('disabled', !$checked && empty($num) && $scriptProperties['suggestions'] ? 'disabled' : '');

                    $modx->smarty->assign('urlbrand', $pageurlstr);
                    $modx->smarty->assign('url', $ur);

                    $modx->smarty->assign('idx', $idx++);
                    $modx->smarty->assign('num', $num);
                    $modx->smarty->assign('delimeter', $mSearch2->config['filter_delimeter']);


                    $cont = $modx->smarty->fetch('tpl/filter/' . $tplRow . '.tpl');

                    $rowsarray[] = array(
                        'hits' => $prosmotri,
                        'content' => $cont
                    );

                    $rowsall .= $cont;// $cont;//_all
                    $co++;
                }
                $rows = '';
                usort($rowsarray, 'sorthits');
                foreach ($rowsarray as $r) {
                    $rows .= $r['content'];
                }


            } else {

               /* if ($_SERVER['HTTP_CF_CONNECTING_IP'] == '78.46.62.217') {
                    echo '<pre>filters'.$filter2.' '.$co;
                    print_r($data);
                    echo '</pre>';


                }*/
                foreach ($data as $v) {

                    if (empty($v)) {

                        continue;
                    }



                    $filter3 = explode('|', $filter);
                    $filter4 = $filter3[1];
                    $checked = isset($request[$filter4]) && in_array($v['value'], $request[$filter4]) && isset($v['type']) && $v['type'] != 'number';


                    if ($checked) {
                        $num = '';
                        $has_active = 'has_active';
                    } else if (isset($suggestions[$filter4][$v['value']])) {
                        $num = $suggestions[$filter4][$v['value']];
                    } else {
                        $num = !empty($v['resources']) ? count($v['resources']) : '';
                        //	echo 'num'.
                    }


                    $pageurlstr = '';
                    if (isset($_REQUEST['attribute'])) {

                        $pageurlstr = '';//$v['url'];//.'/';
                        $ur = '';
                    } else {
                        if (isset($v['sef'])) {
                            if ($v['sef'] == 1) {
                                $pageurlstr = $pageurl;
                                $pageurlstr .= '/' . $v['value'];//.'/';
                                //$ur=$v['url'];//.'/';
                            }
                        }

                    }


                    $modx->smarty->assign('filter', $filter2);
                    $modx->smarty->assign('hits', $prosmotri);
                    $modx->smarty->assign('table', $table);
                    $modx->smarty->assign('urlbrand', $pageurlstr);

                    $modx->smarty->assign('title', $v['title']);
                    $modx->smarty->assign('value', $v['value']);
                    if ($filter4 == 'price_kg') $pricesfilter[] = $v['value'];

                    $modx->smarty->assign('type', $v['type']);
                    $modx->smarty->assign('checked', $checked ? 'checked' : '');
                    $modx->smarty->assign('selected', $checked ? 'selected' : '');
                    $modx->smarty->assign('disabled', !$checked && empty($num) && $scriptProperties['suggestions'] ? 'disabled' : '');
                    $modx->smarty->assign('idx', $idx++);
                    $modx->smarty->assign('num', $num);
                    $modx->smarty->assign('delimeter', $mSearch2->config['filter_delimeter']);

                    if ($raiting_page == '1') {
                        $tplRow = 'filterRaitingRowTpl';
                    }

                    $rows .= $modx->smarty->fetch('tpl/filter/' . $tplRow . '.tpl');
                    //echo  'tplRow'.$tplRow.'<br/>';



                   /* if ($_SERVER['HTTP_CF_CONNECTING_IP'] == '78.46.62.217') {
                        echo '<pre>filters'.$filter2.' '.$num;
                        print_r($rows);
                        echo '</pre>';


                    }*/
                    if ((isset($v['resources'])) && (count($v['resources']) > 0)) {
                        $co++;
                    }
                }
            }
            $tpl = empty($rows) ? $tplEmpty : $tplOuter;


            if (!isset($output_filters[$filter])) {
                $output_filters[$filter] = '';
            }

            if ($co > 0) {

                //echo '$tpl'.$tpl;

                if ($filter == 'ms|price_kg') {

                    $min = $rangeprice[0];//round(min($pricesfilter));
                    $max = $rangeprice[1];//ceil(max($pricesfilter));

                    if (isset($_REQUEST['price_kg'])) {
                        $pr_ar = explode(',', $_REQUEST['price_kg']);

                        $pricemin = $pr_ar[0];
                        $pricemax = $pr_ar[1];
                    } else {
                        $pricemin = $min;
                        $pricemax = $max;
                    }

                    $modx->smarty->assign('pricemax', $pricemax);
                    $modx->smarty->assign('pricemin', $pricemin);

                    $modx->smarty->assign('max', $max);
                    $modx->smarty->assign('min', $min);
                }

                $modx->smarty->assign('filter', $filter2);
                $modx->smarty->assign('table', $table);
                $modx->smarty->assign('label', $newnames[$filter]);
                $modx->smarty->assign('rows', $rows);

                $modx->smarty->assign('count', $co);
                $modx->smarty->assign('has_active', $has_active);
                $modx->smarty->assign('delimeter', $mSearch2->config['filter_delimeter']);



                if ($filter == 'ms|price_kg') {
                    switch ($modx->resource->id) {
                        case '15':
                        case '19':
                        case '59':
                        case '57':
                            $output_filters[$filter] = $modx->smarty->fetch('tpl/filter/' . $tpl . '.tpl');
                            break;

                    }


                } else {

                    //echo 'eeee'.$filter.'<br/>';
                    if ($raiting_page == '1') {
                        $tpl = 'filterRaitingGroupTpl';
                    }

                    //echo  '$tpl'.$tpl.'<br/>';
                    $output_filters[$filter] = $modx->smarty->fetch('tpl/filter/' . $tpl . '.tpl');


                }
                //$output['filters_smarty'][$filter]
                /* echo '<pre>'.$filter;
                 print_r($output['filters_smarty'][$filter]);
                 echo '</pre>';
                  */
                /*	$output['filters'][$filter] .= $pdoFetch->getChunk($tpl, array(
                        'filter' => $filter2
                        ,'table' => $table
                        ,'label'=>$newnames[$filter]
                        ,'rows' => $rows
                        ,'count'=>$co
                        ,'has_active' => $has_active
                        ,'delimeter' => $mSearch2->config['filter_delimeter']
                    ), $fastMode);*/
            }

            if ($filter2 == 'vendor') {
                if ($co > 9) {
                    /* $tplOuter1='filterVendorOuterAllTpl';
                    // echo '$tplOuter1'.$tplOuter1.' '.$tpl;
                     $tpl = $tplOuter1;//empty($rows) ? $tplEmpty : $tplOuter;
                     if (!isset($output['filters'][$filter])) {$output['filters'][$filter] = '';}
                     $output['filters'][$filter] .= $pdoFetch->getChunk($tpl, array(
                         'filter' => $filter2
                         ,'table' => $table
                         ,'rows' => $rowsall//$rows
                         ,'has_active' => $has_active
                         ,'delimeter' => $mSearch2->config['filter_delimeter']
                     ), $fastMode);*/
                }
            }


        }
    }
    if (empty($output['filters'])) {
        $output['filters'] = $modx->lexicon('mse2_err_no_filters');
        if (empty($output['results'])) {
            $output['results'] = $modx->lexicon('mse2_err_no_results');
        }

    } else {
        $pdoFetch->addTime('Filters templated');
    }
}


if ($showcategory == true) {
    $output['labelfilter'] = 'Каталог';
    $output['filtershow'] = 0;
} else {
    $output['labelfilter'] = 'Фильтр товаров';
    $output['filtershow'] = 1;
}

$pdoFetch->addTime('Total filter operations: ' . $mSearch2->filter_operations);

// Saving params into cache for ajax requests
$savedProperties['scriptProperties'] = $scriptProperties;
$hash = sha1(serialize($savedProperties));
$_SESSION['mSearch2'][$hash] = $savedProperties;

// Active class for sort links
if (!empty($sort)) {
    $output[$sort] = $classActive;
}
if (isset($paginatorProperties['tpl_idx'])) {
    $output['tpl' . $paginatorProperties['tpl_idx']] = $classActive;
    $output['tpls'] = 1;
}


// Setting values for frontend javascript
$config = array(
    'cssUrl' => $mSearch2->config['cssUrl'] . 'web/',
    'jsUrl' => $mSearch2->config['jsUrl'] . 'web/',
    'actionUrl' => $mSearch2->config['actionUrl'],
    'queryVar' => $mSearch2->config['queryVar'],
    'filter_delimeter' => $mSearch2->config['filter_delimeter'],
    'method_delimeter' => $mSearch2->config['method_delimeter'],
    'values_delimeter' => $mSearch2->config['values_delimeter'],
    'start_sort' => $start_sort,
    'start_limit' => $start_limit,
    'start_page' => 1,
    'start_tpl' => '',
    'sort' => $sort == $start_sort ? '' : $sort,
    'sortdir' => $sortdir,
    'limit' => $limit == $start_limit ? '' : $limit,
    'page' => $page,
    'tpl' => !empty($paginatorProperties['tpl_idx'])
        ? $paginatorProperties['tpl_idx']
        : '',
    'parentsVar' => $parentsVar,
    'key' => $hash,
    'pageId' => !empty($pageId) ? (integer)$pageId : $modx->resource->id,
    $queryVar => isset($_REQUEST[$queryVar]) ? $_REQUEST[$queryVar] : '',
    $parentsVar => isset($_REQUEST[$parentsVar]) ? $_REQUEST[$parentsVar] : '',
);

$output['pageurl'] = $pageurl;
$u = explode('/', $_REQUEST['q']);

$output['page'] = $page;
if ($page > 1) $output['showseotext'] = 0;//$showseotext;
else $output['showseotext'] = 1;
if (count($u) > 2) $output['showseotext'] = 0;//$showseotext;
$modx->regClientStartupScript('
    <script type="text/javascript">
    mse2Config = ' . $modx->toJSON($config) . ';
    </script>', true);
$modx->setPlaceholders($config, $plPrefix);

// Prepare output
$log = '';
if ($modx->user->hasSessionContext('mgr') && !empty($showLog)) {
    $log = '<pre class="mFilterLog">' . print_r($pdoFetch->getTime(), 1) . '</pre>';
}


if (!empty($toSeparatePlaceholders)) {
    $modx->setPlaceholders($output['filters'], $toSeparatePlaceholders);
    $output['log'] = $log;
    if (is_array($output_filters)) {
        $output['filters'] = implode($outputSeparator, $output_filters);//$output['filters']);
    }

    if (empty($output['results'])) {
        $pcre = '/^' . preg_quote($toSeparatePlaceholders) . '(\d+)$/';
        foreach ($modx->placeholders as $k => $v) {
            if (preg_match($pcre, $k)) {
                $output['results'][] = $v;
            }
        }
        $output['results'] = implode($outputSeparator, $output['results']);
    }


    $modx->setPlaceholders($output, $toSeparatePlaceholders);
} else {
    /*
    if ($_SERVER['HTTP_CF_CONNECTING_IP']=='78.46.62.217'){
                    echo '<pre>filters'.$tplRow;
                print_r($output_filters);//array($requested, $values, $ids));//array($requested, $values, $ids));
                echo '</pre>';
                }*/


    if (is_array($output_filters)) {
        $output['filters'] = implode($outputSeparator, $output_filters);//$output['filters']);
    }
    if (!empty($toPlaceholders)) {
        $output['log'] = $log;
        $modx->setPlaceholders($output, $toPlaceholders);
    } else {

        $modx->smarty->assign('banner', $banner);
        $modx->smarty->assign('filters', $output['filters']);
        $modx->smarty->assign('total', $output['total']);
        $modx->smarty->assign('limit', $output['limit']);
        $modx->smarty->assign('limit2', $output['limit2']);
        $modx->smarty->assign('labelfilter', $output['labelfilter']);
        $modx->smarty->assign('filtershow', $output['filtershow']);
        $modx->smarty->assign('hits desc', $output['hits DESC:desc']);

        $modx->smarty->assign('tpl0', $output['tpl0']);
        $modx->smarty->assign('tpls', $output['tpls']);
        $modx->smarty->assign('pageurl', $output['pageurl']);
        $modx->smarty->assign('showseotext', $output['showseotext']);
        $modx->smarty->assign('seo_text', $output['seo_text']);
        $modx->smarty->assign('page', $page);

        $modx->smarty->assign('results', $output['results']);


        $st_nav = $modx->getPlaceholder('page.nav');
        if ($_SERVER['HTTP_CF_CONNECTING_IP'] == '188.130.177.18') {
            //echo 'st_nav'.$st_nav;die();
        }

        if ($raiting_page == '1') {
            // return $modx->smarty->fetch('tpl/filter/filter.tpl');
            return $modx->smarty->fetch('tpl/filter/filter_raiting.tpl');
        } else {
            return $modx->smarty->fetch('tpl/filter/filter.tpl');
        }
        $output .= $log;
        return $output;
    }
}


//$modx->smarty->assign($key, $value);
//return $modx->smarty->fetch($tpl);